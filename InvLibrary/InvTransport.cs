﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;
using Inv.Support;

namespace Inv
{
  public static class TransportFoundation
  {
    public const int SendBufferSize = 65536;
    public const int ReceiveBufferSize = 65536;
  }

  public interface TransportFactory
  {
    TransportConnection JoinConnection();
  }

  public interface TransportConnection
  {
    TransportFlow Flow { get; }
    void Drop();
  }

  public sealed class TransportLink : IDisposable
  {
    public TransportLink(Inv.TransportFactory TransportFactory)
    {
      this.TransportFactory = TransportFactory;
      this.ConnectionPool = new Pool<TransportConnection>(() =>
      {
        var Connection = TransportFactory.JoinConnection(); // join the connection.

        if (JoinEvent != null)
          JoinEvent(Connection);

        return Connection;
      }, C =>
      {
        C.Drop(); // drop the connection.

        if (DropEvent != null)
          DropEvent(C);
      }, Timeout: null, MinCapacity: 0);
      this.FaultedSignal = new AutoResetSignal(true, "TransportLink-Faulted");
      this.EstablishedSignal = new ManualResetSignal(false, "TransportLink-Established");
      this.FaultCriticalSection = new Inv.ExclusiveCriticalSection("Inv.TransportLink-Broker");
      this.ExchangeCriticalSection = new Inv.ReaderWriterCriticalSection("Inv.TransportLink-Exchange");
      ExchangeCriticalSection.NoTimeout();
    }
    public void Dispose()
    {
      ExchangeCriticalSection.Dispose();
      FaultedSignal.Dispose();
      EstablishedSignal.Dispose();
    }

    public bool IsActive { get; private set; }
    public event Action ConnectEvent;
    public event Action DisconnectEvent;
    public event Action ReconnectEvent;
    public event Action<Inv.TransportConnection> JoinEvent;
    public event Action<Inv.TransportConnection> DropEvent;
    public event Action<Exception> FaultBeginEvent;
    public event Action<Exception> FaultContinueEvent;
    public event Action<Exception> FaultEndEvent;

    public void Connect()
    {
      if (!IsActive)
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(ConnectionPool.Count == 0, "ConnectionPool should be empty.");

        var Connection = ConnectionPool.Take();
        try
        {
          if (ConnectEvent != null)
            ConnectEvent();
        }
        catch (Exception Exception)
        {
          Connection.Drop();

          throw Exception.Preserve();
        }
        ConnectionPool.Return(Connection);

        this.IsActive = true;
      }
    }
    public void Disconnect()
    {
      if (IsActive)
      {
        this.IsActive = false;

        if (DisconnectEvent != null)
          DisconnectEvent();

        ConnectionPool.Drain(); // disposes all active connections.

        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(ConnectionPool.Count == 0, "ConnectionPool should be empty.");
      }
    }
    public int ConnectionCount()
    {
      return ConnectionPool.Count;
    }
    public Inv.TransportConnection TakeConnection()
    {
      return ConnectionPool.Take();
    }
    public void ReturnConnection(Inv.TransportConnection Connection)
    {
      Debug.Assert(Connection != null);

      ConnectionPool.Return(Connection);
    }
    public void Exchange(Action<Inv.TransportConnection> Action)
    {
      if (ExchangeCriticalSection.IsAnyLockHeld)
      {
        // NOTE: this is a remote call made from within a call - or during a fault.
        CallConnection(Action);
      }
      else
      {
        Exception FaultException;
        do
        {
          using (ExchangeCriticalSection.EnterReadLock())
          {
            try
            {
              CallConnection(Action);

              FaultException = null;
            }
            catch (Inv.TransportFlowException FlowException)
            {
              // NOTE: a reconnection is required.
              FaultException = FlowException;
            }
            catch (Inv.TransportMarshalException MarshalException)
            {
              // NOTE: transport exceptions are marshalled over the RPC and do not signify a reconnection is required.
              throw MarshalException.Preserve();
            }
          }

          if (FaultException != null)
            FaultRecovery(FaultException);
        }
        while (IsActive && FaultException != null);
      }
    }
    public T Exchange<T>(Func<Inv.TransportConnection, T> Func)
    {
      var Result = default(T);

      Exchange((Action<Inv.TransportConnection>)(Connection => Result = Func(Connection)));

      return Result;
    }
    public void SimulateFault(TimeSpan TimeSpan)
    {
      var StartTime = DateTime.Now;

      void FaultDelay()
      {
        // NOTE: we want the reconnection to be attempted until the end of the simulation time.
        if (StartTime + TimeSpan > DateTime.Now)
          throw new Inv.TransportFlowException("Simulated unable to reconnect fault.");
      }

      Inv.TaskGovernor.RunActivity("InvTransportLink.SimulateFault", () =>
      {
        SimulateReconnectAction = FaultDelay;
        try
        {
          FaultRecovery(new Exception("Simulated remote connection fault."));
        }
        catch
        {
          // ignore exceptions during fault recovery?
        }
        finally
        {
          SimulateReconnectAction = null;
        }
      });
    }

    private void CallConnection(Action<Inv.TransportConnection> Action)
    {
      var Connection = ConnectionPool.Take();
      try
      {
        Interlocked.Increment(ref ActiveConnectionCount);
        try
        {
          Action(Connection);
        }
        finally
        {
          Interlocked.Decrement(ref ActiveConnectionCount);
        }
      }
      finally
      {
        ConnectionPool.Return(Connection);
      }
    }
    private void FaultRecovery(Exception FaultException)
    {
      bool IsFirstFaultThread;

      using (FaultCriticalSection.Lock())
      {
        IsFirstFaultThread = FaultedSignal.WaitOne(0);

        if (IsFirstFaultThread)
          EstablishedSignal.Reset();
      }

      if (!IsFirstFaultThread)
      {
        // Block this thread until reconnection is complete.
        EstablishedSignal.WaitOne();
      }
      else
      {
        using (ExchangeCriticalSection.EnterWriteLock())
        {
          try
          {
            var FaultSignalSet = false;

            try
            {
              if (FaultBeginEvent != null)
                FaultBeginEvent(FaultException);

              var Reconnecting = true;
              while (Reconnecting)
              {
                try
                {
                  if (IsActive)
                  {
                    if (SimulateReconnectAction != null)
                      SimulateReconnectAction();

                    if (IsActive)
                    {
                      ConnectionPool.Drain();
                      ConnectionPool.EnsureCapacity();

                      if (ReconnectEvent != null)
                        ReconnectEvent();
                    }
                  }

                  Reconnecting = false;
                }
                catch (Inv.TransportFlowException ContinueException)
                {
                  if (FaultContinueEvent != null)
                    FaultContinueEvent(ContinueException);

                  // wait one second before trying again.
                  Inv.TaskGovernor.Sleep(1000);

                  Reconnecting = true;
                }
              }

              if (FaultEndEvent != null)
                FaultEndEvent(FaultException);

              FaultSignalSet = true;
              FaultedSignal.Set();
            }
            catch (Exception TerminalException)
            {
              // NOTE: safety measure to ensure the fault management can't completely fail.
              if (!FaultSignalSet)
                FaultedSignal.Set();

              throw TerminalException.Preserve();
            }
          }
          finally
          {
            // Allow subsequent faulting threads to continue.
            EstablishedSignal.Set();
          }
        }
      }
    }

    private readonly Inv.TransportFactory TransportFactory;
    private Inv.Pool<Inv.TransportConnection> ConnectionPool;
    private Inv.ReaderWriterCriticalSection ExchangeCriticalSection;
    private Inv.ExclusiveCriticalSection FaultCriticalSection;
    private AutoResetSignal FaultedSignal;
    private ManualResetSignal EstablishedSignal;
    private Action SimulateReconnectAction;
    private int ActiveConnectionCount;
  }

  public sealed class TransportPacket
  {
    public TransportPacket(byte[] Buffer)
    {
      this.Buffer = Buffer;
    }

    public byte[] Buffer { get; private set; }

    public TransportReader ToReader()
    {
      return new TransportReader(this);
    }
  }

  public sealed class TransportFlow
  {
    public TransportFlow(System.IO.Stream Stream)
      : this(Stream, Stream)
    {
    }
    public TransportFlow(System.IO.Stream InputStream, System.IO.Stream OutputStream)
    {
      this.InputStream = InputStream;
      this.OutputStream = OutputStream;
    }

    public System.IO.Stream InputStream { get; }
    public System.IO.Stream OutputStream { get;}

    public TransportQueue NewQueue(string Name)
    {
      return new TransportQueue(this, Name);
    }

    public void SendPacket(Inv.TransportPacket RoutePacket)
    {
      if (!TrySendPacket(RoutePacket))
        throw new TransportFlowException("Send connection lost.");
    }
    public bool TrySendPacket(Inv.TransportPacket RoutePacket)
    {
      var Buffer = RoutePacket.Buffer;
      return WriteByteArray(Buffer, 0, Buffer.Length);
    }
    public Inv.TransportPacket ReceivePacket()
    {
      var Result = TryReceivePacket();

      if (Result == null)
        throw new TransportFlowException("Receive connection lost.");

      return Result;
    }
    public Inv.TransportPacket TryReceivePacket()
    {
      // TODO: more efficient buffered socket reading?

      var DataBuffer = new byte[4];

      if (!ReadByteArray(DataBuffer, 0, 4))
        return null;

      var DataLength = BitConverter.ToInt32(DataBuffer, 0);

      if (DataLength > 0)
      {
        Array.Resize(ref DataBuffer, DataLength);

        if (!ReadByteArray(DataBuffer, 4, DataLength))
          return null;

        return new Inv.TransportPacket(DataBuffer);
      }
      else
      {
        return null;
      }
    }

    [DebuggerNonUserCode]
    private bool WriteByteArray(byte[] Buffer, int Offset, int Length)
    {
      try
      {
        OutputStream.Write(Buffer, Offset, Length);
      }
      catch
      {
        return false;
      }

      return true;
    }
    [DebuggerNonUserCode]
    private bool ReadByteArray(byte[] Buffer, int Offset, int Length)
    {
      // NOTE: don't break on exception in here as socket disconnections are expected.
      //       downstream code must cope with ReceivePacket returning null.
      try
      {
        var Index = Offset;
        while (Index < Length)
        {
          var Actual = InputStream.Read(Buffer, Index, Length - Index);

          if (Actual == 0)
            return false;

          Index += Actual;
        }
      }
      catch
      {
        return false;
      }

      return true;
    }
  }

  public sealed class TransportFlowException : Exception
  {
    public TransportFlowException(string Message)
      : base(Message)
    {
    }
  }

  public sealed class TransportMarshalException : Exception
  {
    public TransportMarshalException(Exception Exception)
      : base(Exception.Message)
    {
      this.Type = Exception.GetType().FullName;
    }
    public TransportMarshalException(string Type, string Message, string Source, string StackTrace)
      : base(Message)
    {
      this.Type = Type;
      this.SourceField = Source;
      this.StackTraceField = StackTrace;
    }

    public string Type { get; private set; }
    public override string Source
    {
      get { return SourceField; }
    }
    public override string StackTrace
    {
      get { return StackTraceField; }
    }

    private readonly string SourceField;
    private readonly string StackTraceField;
  }

  public sealed class TransportWriter : IDisposable
  {
    public TransportWriter() : this(new MemoryStream())
    {
    }
    public TransportWriter(MemoryStream Stream)
    {
      this.MemoryStream = Stream;

      this.Base = new CompactWriter(MemoryStream, true);
      Base.WriteInt32(0); // reserve four bytes for the packet length.
    }
    public void Dispose()
    {
      Base.Dispose();
    }

    public long Size
    {
      get { return MemoryStream.Position - 4; }
    }

    public CompactWriter AsCompact()
    {
      return Base;
    }
    public void Reset()
    {
      MemoryStream.Position = 0;
      MemoryStream.SetLength(0);
      Base.WriteInt32(0);
    }
    public Inv.TransportPacket ToPacket()
    {
      MemoryStream.Flush();
      MemoryStream.Position = 0;
      Base.WriteInt32((int)MemoryStream.Length);

      return new Inv.TransportPacket(MemoryStream.ToArray());
    }

    private MemoryStream MemoryStream;
    private Inv.CompactWriter Base;
  }

  public sealed class TransportReader : IDisposable
  {
    public TransportReader(Inv.TransportPacket Packet)
    {
      this.MemoryStream = new MemoryStream(Packet.Buffer);
      this.Base = new CompactReader(MemoryStream, true);

      var PacketLength = Base.ReadInt32();
      if (PacketLength != Packet.Buffer.Length)
        throw new Exception("Packet length is invalid.");
    }
    public void Dispose()
    {
      Base.Dispose();
    }

    public bool EndOfPacket
    {
      get { return MemoryStream.Position >= MemoryStream.Length; }
    }

    public CompactReader AsCompact()
    {
      return Base;
    }

    private Inv.CompactReader Base;
    private MemoryStream MemoryStream;
  }

  /// <summary>
  /// Thread-safe abstraction for sending and receiving packets on a single transport flow (usually a TCP/IP socket).
  /// Packets are sent and received in background tasks.
  /// </summary>
  public sealed class TransportQueue
  {
    internal TransportQueue(Inv.TransportFlow Flow, string Name)
    {
      this.Flow = Flow;
      this.Name = Name;

      this.SendCriticalSection = new ExclusiveCriticalSection("TransportQueue-Send-" + Name);
      this.SendReadySignal = new AutoResetSignal(false, $"TransportQueue-Send-{Name}");
      this.SendQueue = new Queue<Inv.TransportPacket>();
      this.SendTask = Inv.TaskGovernor.NewActivity("TransportQueue-Send-" + Name, SendPackets);

      this.ReceiveCriticalSection = new ExclusiveCriticalSection("TransportQueue-Recv-" + Name);
      this.ReceiveReadySignal = new AutoResetSignal(false, $"TransportQueue-Recv-{Name}");
      this.ReceiveQueue = new Queue<Inv.TransportPacket>();
      this.ReceiveTask = Inv.TaskGovernor.NewActivity("TransportQueue-Recv-" + Name, ReceivePackets);
    }

    /// <summary>
    /// Tracks the total number of bytes sent.
    /// </summary>
    public ulong SentBytes { get; private set; }
    /// <summary>
    /// Tracks the total number of bytes received.
    /// </summary>
    public ulong ReceivedBytes { get; private set; }

    /// <summary>
    /// Start the background tasks for sending and receiving packets.
    /// </summary>
    public void Start()
    {
      if (!IsActive)
      {
        this.IsActive = true;

        ReceiveTask.Start();
        SendTask.Start();
      }
    }
    /// <summary>
    /// Stop the background tasks for sending and receiving packets.
    /// </summary>
    public void Stop()
    {
      if (IsActive)
      {
        CancelTasks();

        ReceiveTask.Wait();
        SendTask.Wait();
      }
    }

    /// <summary>
    /// Add the packet to the send queue.
    /// </summary>
    /// <param name="Packet"></param>
    public void SendPacket(Inv.TransportPacket Packet)
    {
      using (SendCriticalSection.Lock())
      {
        SendQueue.Enqueue(Packet);

        SendReadySignal.Set();
      }
    }
    /// <summary>
    /// Waits until there is a packet to remove from the receive queue.
    /// </summary>
    /// <returns></returns>
    public Inv.TransportPacket ReceivePacket()
    {
      if (IsActive && ReceiveReadySignal.WaitOne())
      {
        if (!IsActive)
          return null;

        using (ReceiveCriticalSection.Lock())
        {
          var Result = ReceiveQueue.Dequeue();

          if (ReceiveQueue.Count > 0)
            ReceiveReadySignal.Set();

          return Result;
        }
      }
      else
      {
        return null;
      }
    }
    /// <summary>
    /// Immediately takes the next packet from the receive queue or return null if there are no available packets.
    /// </summary>
    /// <returns></returns>
    public Inv.TransportPacket TryReceivePacket()
    {
      return TryReceivePacket(TimeSpan.Zero);
    }
    /// <summary>
    /// Takes the next packet from the receive queue and will Wait up to the specified time span for one to be received.
    /// Returns null if there are no available packets within the specified time span.
    /// </summary>
    /// <returns></returns>
    public Inv.TransportPacket TryReceivePacket(TimeSpan TimeSpan)
    {
      if (IsActive && ReceiveReadySignal.WaitOne(TimeSpan))
      {
        if (!IsActive)
          return null;

        using (ReceiveCriticalSection.Lock())
        {
          var Result = ReceiveQueue.Dequeue();

          if (ReceiveQueue.Count > 0)
            ReceiveReadySignal.Set();

          return Result;
        }
      }
      else
      {
        return null;
      }
    }

    private void ReceivePackets()
    {
      while (IsActive)
      {
        var Packet = Flow.TryReceivePacket();

        if (Packet == null)
        {
          CancelTasks();
        }
        else
        {
          using (ReceiveCriticalSection.Lock())
          {
            ReceiveQueue.Enqueue(Packet);

            ReceiveReadySignal.Set();
          }

          ReceivedBytes += (uint)Packet.Buffer.Length;
        }
      }
    }
    private void SendPackets()
    {
      while (IsActive)
      {
        while (SendReadySignal.WaitOne() && IsActive)
        {
          Inv.TransportPacket Result;

          using (SendCriticalSection.Lock())
          {
            Result = SendQueue.Dequeue();

            if (SendQueue.Count > 0)
              SendReadySignal.Set();
          }

          if (!Flow.TrySendPacket(Result))
            IsActive = false;
          else
            SentBytes += (uint)Result.Buffer.Length;
        }
      }
    }
    private void CancelTasks()
    {
      this.IsActive = false;

      // wake up the tasks so they can finish.
      ReceiveReadySignal.Set(); 
      SendReadySignal.Set();
    }

    private Inv.TransportFlow Flow;
    private ExclusiveCriticalSection SendCriticalSection;
    private AutoResetSignal SendReadySignal;
    private Queue<Inv.TransportPacket> SendQueue;
    private Inv.TaskActivity SendTask;
    private ExclusiveCriticalSection ReceiveCriticalSection;
    private AutoResetSignal ReceiveReadySignal;
    private Queue<Inv.TransportPacket> ReceiveQueue;
    private Inv.TaskActivity ReceiveTask;
    private bool IsActive;
    private readonly string Name;
  }
}