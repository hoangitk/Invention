﻿using System;
using UIKit;
using CoreGraphics;
using Inv.Support;
using System.Diagnostics;

namespace Inv
{
  internal sealed class iOSDateTimePicker : UIViewController
  {
    public iOSDateTimePicker(UIViewController Parent)
    {
      this.Parent = Parent;
      this.ModalPresentationStyle = UIKit.UIModalPresentationStyle.Custom;

      this.EmbedView = new UIView();

      this.HeaderLabel = new UILabel(new CGRect(0, 0, 320 / 2, 44));
      HeaderLabel.AutoresizingMask = UIViewAutoresizing.FlexibleWidth;
      HeaderLabel.TextAlignment = UITextAlignment.Center;

      this.CancelButton = UIButton.FromType(UIButtonType.System);
      CancelButton.BackgroundColor = UIColor.Clear;
      CancelButton.SetTitle("Cancel", UIControlState.Normal);
      CancelButton.TouchUpInside += (Sender, Event) =>
      {
        DismissViewController(true, null);

        CalendarPicker.CancelInvoke();
      };

      this.DoneButton = UIButton.FromType(UIButtonType.System);
      DoneButton.BackgroundColor = UIColor.Clear;
      DoneButton.SetTitle("Done", UIControlState.Normal);
      DoneButton.TouchUpInside += (Sender, Event) =>
      {
        // TODO: does this do timezones correctly?

        var Value = CalendarPicker.SetDate ? DatePicker.Date.NSDateToDateTime().Date : DateTime.MinValue.Date;

        if (CalendarPicker.SetTime)
          Value += TimePicker.Date.NSDateToDateTime().TimeOfDay.TruncateSeconds(); // seconds are not editable in UIDatePicker.

        DismissViewController(true, null);

        CalendarPicker.Value = Value;
        CalendarPicker.SelectInvoke();
      };

      this.DatePicker = new UIDatePicker();
      DatePicker.BackgroundColor = UIColor.White;
      DatePicker.Mode = UIDatePickerMode.Date;

      this.TimePicker = new UIDatePicker();
      TimePicker.BackgroundColor = UIColor.White;
      TimePicker.Mode = UIDatePickerMode.Time;
    }

    public void SetCalendarPicker(Inv.CalendarPicker CalendarPicker)
    {
      this.CalendarPicker = CalendarPicker;
    }

    public override void ViewDidLoad()
    {
      base.ViewDidLoad();

      View.BackgroundColor = UIColor.Clear;

      EmbedView.BackgroundColor = HeaderBackgroundColor;

      HeaderLabel.BackgroundColor = HeaderBackgroundColor;
      HeaderLabel.TextColor = HeaderTextColor;
      HeaderLabel.Text = "Select" + (CalendarPicker.SetDate ? " Date" : "") + (CalendarPicker.SetTime ? " Time" : "");

      CancelButton.SetTitleColor(HeaderTextColor, UIControlState.Normal);

      DoneButton.SetTitleColor(HeaderTextColor, UIControlState.Normal);

      if (CalendarPicker.SetDate)
      {
        DatePicker.Date = (Foundation.NSDate)CalendarPicker.Value.ToLocalTime();

        EmbedView.AddSubview(DatePicker);
      }

      if (CalendarPicker.SetTime)
      {
        TimePicker.Date = (Foundation.NSDate)CalendarPicker.Value.ToLocalTime();

        EmbedView.AddSubview(TimePicker);
      }

      EmbedView.AddSubview(HeaderLabel);
      EmbedView.AddSubview(CancelButton);
      EmbedView.AddSubview(DoneButton);

      Add(EmbedView);
    }
    public override void ViewWillAppear(bool animated)
    {
      base.ViewDidAppear(animated);

      Show(false);
    }
    public override void DidRotate(UIInterfaceOrientation fromInterfaceOrientation)
    {
      base.DidRotate(fromInterfaceOrientation);

      Show(true);

      View.SetNeedsDisplay();
    }

    private void Show(bool Rotating)
    {
      Debug.Assert(CalendarPicker.SetDate || CalendarPicker.SetTime);

      if (CalendarPicker.SetDate && !CalendarPicker.SetTime)
        ArrangeSingle(DatePicker, Rotating);
      else if (CalendarPicker.SetTime && !CalendarPicker.SetDate)
        ArrangeSingle(TimePicker, Rotating);
      else if (CalendarPicker.SetDate && CalendarPicker.SetTime)
        ArrangeMultiple(Rotating);

      HeaderLabel.Frame = new CGRect(20 + ButtonSize.Width, 4, Parent.View.Frame.Width - (40 + 2 * ButtonSize.Width), 35);
      DoneButton.Frame = new CGRect(EmbedView.Frame.Width - ButtonSize.Width - 10, 7, ButtonSize.Width, ButtonSize.Height);
      CancelButton.Frame = new CGRect(10, 7, ButtonSize.Width, ButtonSize.Height);
    }
    private void ArrangeSingle(UIDatePicker Picker, bool Rotating)
    {
      var FrameWidth = Parent.View.Frame.Width;

      var EmbedViewSize = new CGSize(FrameWidth, Picker.Frame.Height + HeaderBarHeight);

      var EmbedViewRect = new CGRect(0, View.Frame.Height - EmbedViewSize.Height, EmbedViewSize.Width, EmbedViewSize.Height);
      EmbedView.Frame = EmbedViewRect;

      Picker.Frame = new CGRect(Picker.Frame.X, HeaderBarHeight, EmbedViewRect.Width, Picker.Frame.Height);
    }
    private void ArrangeMultiple(bool Rotating)
    {
      var FrameWidth = Parent.View.Frame.Width;

      CGRect EmbedViewRect;

      if (FrameWidth < 568)
      {
        // stack date and time on top of each other (iPhone portrait).
        var EmbedViewSize = new CGSize(FrameWidth, DatePicker.Frame.Height + TimePicker.Frame.Height + HeaderBarHeight);
        EmbedViewRect = new CGRect(0, View.Frame.Height - EmbedViewSize.Height, EmbedViewSize.Width, EmbedViewSize.Height);

        DatePicker.Frame = new CGRect(0, HeaderBarHeight, EmbedViewRect.Width, DatePicker.Frame.Height);
        TimePicker.Frame = new CGRect(0, HeaderBarHeight + DatePicker.Frame.Height, EmbedViewRect.Width, TimePicker.Frame.Height);
      }
      else
      {
        // date and time pickers are side-by-side when there's enough width to split in half.
        var EmbedViewSize = new CGSize(FrameWidth, Math.Max(DatePicker.Frame.Height, TimePicker.Frame.Height) + HeaderBarHeight);
        EmbedViewRect = new CGRect(0, View.Frame.Height - EmbedViewSize.Height, EmbedViewSize.Width, EmbedViewSize.Height);

        var SplitWidth = EmbedViewRect.Width / 2;
        DatePicker.Frame = new CGRect(0, HeaderBarHeight, SplitWidth, DatePicker.Frame.Height);
        TimePicker.Frame = new CGRect(SplitWidth, HeaderBarHeight, SplitWidth, TimePicker.Frame.Height);
      }

      EmbedView.Frame = EmbedViewRect;
    }

    private Inv.CalendarPicker CalendarPicker;
    private UILabel HeaderLabel;
    private UIButton DoneButton;
    private UIButton CancelButton;
    private UIViewController Parent;
    private UIView EmbedView;
    private UIDatePicker DatePicker;
    private UIDatePicker TimePicker;

    private static readonly float HeaderBarHeight = 40.0F;
    private static readonly CGSize ButtonSize = new CGSize(71, 30);
    private static readonly UIColor HeaderBackgroundColor = UIColor.DarkGray;
    private static readonly UIColor HeaderTextColor = UIColor.White;
  }
}