﻿using System;
using System.Linq;
using Inv.Support;

namespace Inv.Manual
{
  internal sealed class SearchSurface : Inv.Mimic<Inv.Surface>
  {
    public SearchSurface(Application Application)
    {
      this.Base = Application.NewSurface();

      Base.Background.Colour = Theme.SheetColour;

      var SearchDock = Base.NewVerticalDock();
      Base.Content = SearchDock;

      var SearchSubject = new SubjectPanel(Base);
      SearchDock.AddHeader(SearchSubject);
      SearchSubject.Text = "Topics";

      var SearchEdit = Base.NewSearchEdit();
      SearchDock.AddHeader(SearchEdit);
      SearchEdit.Border.Set(2);
      SearchEdit.Border.Colour = Inv.Colour.DimGray;
      SearchEdit.Font.Size = 20;
      SearchEdit.Font.Colour = Inv.Colour.White;
      SearchEdit.Size.SetWidth(300);
      SearchEdit.Margin.Set(0, 0, 0, Theme.DocumentGap);
      SearchEdit.Padding.Set(4);
      SearchEdit.Alignment.Center();

      var SearchFlow = Base.NewFlow();
      SearchDock.AddClient(SearchFlow);

      var SearchNavigate = new NavigatePanel(Base);
      SearchDock.AddFooter(SearchNavigate);
      SearchNavigate.TitleIsEnabled = false;
      SearchNavigate.BackText = "< MANUAL";
      SearchNavigate.BackEvent += () => Application.JumpIntroduction();
      SearchNavigate.NextIsEnabled = false;

      void Refresh()
      {
        SearchFlow.RemoveSections();

        var SearchQuery = SearchEdit.Text.NullAsEmpty().Trim();

        foreach (var Book in Application.GetBooks().OrderBy(B => B.Title))
        {
          var TopicArray = Book.GetTopics().Where(T => string.IsNullOrEmpty(SearchQuery) || T.Subject.Contains(SearchQuery, StringComparison.OrdinalIgnoreCase)).OrderBy(T => T.Subject).ToArray();

          if (TopicArray.Length > 0 || string.IsNullOrEmpty(SearchQuery) || Book.Title.Contains(SearchQuery, StringComparison.OrdinalIgnoreCase))
          {
            var BookSection = SearchFlow.AddSection();

            var BookButton = new BookButton(Base);
            BookSection.SetHeader(BookButton);
            BookButton.Text = Book.Title;
            BookButton.Progress = Application.GetBookProgress(Book);
            BookButton.SelectEvent += () => Application.JumpBook(Book);

            BookSection.ItemQuery += (I) =>
            {
              var Topic = TopicArray[I];

              var TopicButton = new TopicButton(Base);
              TopicButton.Text = Topic.Subject;
              TopicButton.Badge = Application.GetTopicBadge(Topic);
              TopicButton.SelectEvent += () => Application.JumpTopic(Topic);

              return TopicButton;
            };
            BookSection.SetItemCount(TopicArray.Length);
          }
        }
      }

      SearchEdit.ChangeEvent += () => Refresh();

      Base.GestureBackwardEvent += () => SearchNavigate.Back();
      Base.ArrangeEvent += () =>
      {
        SearchEdit.Focus.Set();

        Refresh();
      };
    }
  }
}
