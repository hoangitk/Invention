﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Inv.Support;

namespace InvEmailTests
{
  [TestClass]
  public class EmailAddressTests
  {
    [TestMethod]
    public void Validation()
    {
      // Valid addresses
      var ValidEmailAddresses = new[]
      {
        "prettyandsimple@example.com",
        "very.common@example.com",
        "disposable.style.email.with+symbol@example.com",
        "other.email-with-dash@example.com",
        "very.unusual.unusual.com@example.com",
        "has'apostrophe@example.com"
      };

      foreach (var ValidAddress in ValidEmailAddresses)
      {
        var Result = Inv.Support.EmailHelper.CheckAddress(ValidAddress);
        if (Result != null)
          Assert.Fail(string.Format("Valid address failed validation: {0} ({1}", ValidAddress, Result.Value.FormatTitle()));
      }

      // Invalid addresses
      var InvalidEmailAddresses = new Inv.DistinctList<string>();
      InvalidEmailAddresses.Add("Abc.example.com");// (no @ character)
      InvalidEmailAddresses.Add("A @b@c @example.com");// (only one @ is allowed outside quotation marks)
      InvalidEmailAddresses.Add("a\"b(c)d,e:f;g<h>i[j\\k]l@example.com");// (none of the special characters in this local part are allowed outside quotation marks)
      InvalidEmailAddresses.Add("just\"not\"right@example.com");// (quoted strings must be dot separated or the only element making up the local - part)
      InvalidEmailAddresses.Add("this is\"not\\allowed@example.com");// (spaces, quotes, and backslashes may only exist when within quoted strings and preceded by a backslash)
      InvalidEmailAddresses.Add("john..doe@example.com");// (double dot before @)
      InvalidEmailAddresses.Add("john.doe @example..com");// (double dot after @)
      InvalidEmailAddresses.Add(" prettyandsimple@example.com");//a valid address with a leading space
      InvalidEmailAddresses.Add("prettyandsimple @example.com");//a valid address with a trailing space

      foreach (var InvalidAddress in InvalidEmailAddresses)
      {
        var Result = EmailHelper.CheckAddress(InvalidAddress);
        if (Result == null)
          Assert.Fail(string.Format("Invalid address passed validation: {0}", InvalidAddress));
      }
    }
  }
}
