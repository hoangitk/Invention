using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Inv
{
  internal sealed class LockFreeNode<T>
  {
    public LockFreeNode<T> Next;
    public T Item;
  }

  public sealed class LockFreeQueue<T>
  {
    public LockFreeQueue()
    {
      head = new LockFreeNode<T>();
      tail = head;
    }

    public void Enqueue(T item)
    {
      LockFreeNode<T> oldTail = null;
      LockFreeNode<T> oldTailNext;

      var newNode = new LockFreeNode<T>();
      newNode.Item = item;

      var newNodeWasAdded = false;
      while (!newNodeWasAdded)
      {
        oldTail = tail;
        oldTailNext = oldTail.Next;

        if (tail == oldTail)
        {
          if (oldTailNext == null)
            newNodeWasAdded = CAS(ref tail.Next, null, newNode);
          else
            CAS(ref tail, oldTail, oldTailNext);
        }
      }

      CAS(ref tail, oldTail, newNode);
    }

    public bool Dequeue(out T item)
    {
      item = default;

      var haveAdvancedHead = false;
      while (!haveAdvancedHead)
      {
        var oldHead = head;
        var oldTail = tail;
        var oldHeadNext = oldHead.Next;

        if (oldHead == head)
        {
          if (oldHead == oldTail)
          {
            if (oldHeadNext == null)
              return false;

            CAS(ref tail, oldTail, oldHeadNext);
          }
          else
          {
            item = oldHeadNext.Item;
            haveAdvancedHead = CAS(ref head, oldHead, oldHeadNext);
          }
        }
      }
      return true;
    }

    private bool CAS<TN>(ref TN location, TN comparand, TN newValue) where TN : class
    {
      return (object)comparand == (object)Interlocked.CompareExchange<TN>(ref location, newValue, comparand);
    }

    private LockFreeNode<T> head;
    private LockFreeNode<T> tail;
  }
}