﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;
using Inv.Support;
using System.Threading.Tasks;

namespace Inv
{
  // Asynchronous but sequential message processing via a delegate.

  public sealed class TaskGate<T> : IDisposable
  {
    public TaskGate(string Name, Action<T> ProcessDelegate, int Limit = 1000)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Name != null, "Name must be specified.");
        Inv.Assert.Check(ProcessDelegate != null, "ProcessDelegate must be specified.");
        Inv.Assert.Check(Limit > 0, "Limit must be greater than 0.");
      }

      this.Name = Name;
      this.IsActive = true;
      this.MessageQueue = new Queue<T>();
      this.PostSignal = new AutoResetSignal(true, $"{Name}-Post");
      this.FlushSignal = new ManualResetSignal(true, $"{Name}-Flush");
      this.ProcessSignal = new AutoResetSignal(false, $"{Name}-Process");
      this.ProcessDelegate = ProcessDelegate;
      this.Limit = Limit;
      this.ProcessTask = Inv.TaskGovernor.RunActivity("InvTaskGate-" + Name, ProcessMethod);
    }
    public void Dispose()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(IsActive, "Gate must be active.");

      if (IsActive)
      {
        IsActive = false;
        ProcessSignal.Set();
        ProcessTask.Wait();

        // NOTE: this can happen if you dispose while the queue is not empty - interrupting the processing seems like correct behaviour.
        //Inv.Assert.Check(MessageQueue.Count == 0, "Message queue must be empty after the process thread finishes.");

        ProcessSignal.Dispose();
        PostSignal.Dispose();
        FlushSignal.Dispose();
      }
    }

    public string Name { get; private set; }
    public int Limit { get; private set; }

    public void Post(params T[] MessageArray)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(IsActive, "Gate must be active.");

      if (IsActive)
      {
        foreach (var Message in MessageArray)
        {
          if (PostSignal.WaitOne())
          {
            if (ProcessException != null)
              throw ProcessException;

            lock (MessageQueue)
            {
              MessageQueue.Enqueue(Message);

              if (MessageQueue.Count == 1)
              {
                ProcessSignal.Set();
                FlushSignal.Reset();
              }

              if (MessageQueue.Count < Limit)
                PostSignal.Set();
            }
          }
        }
      }
    }
    public void Flush()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(IsActive, "Gate must be active.");

      if (IsActive)
      {
        FlushSignal.WaitOne();

        if (ProcessException != null)
          throw ProcessException;
      }
    }

    private void ProcessMethod()
    {
      do
      {
        if (ProcessSignal.WaitOne())
        {
          while (IsActive && TryDequeue(out var Message))
          {
            try
            {
              ProcessDelegate(Message);
            }
            catch (Exception Exception)
            {
              ProcessException = Exception;

              FlushSignal.Set();
              PostSignal.Set();

              return; // Unhandled process exception cause the gate to stop.
            }

            lock (MessageQueue)
            {
              if (MessageQueue.Count == 0)
                FlushSignal.Set();
            }
          }
        }
      }
      while (IsActive);
    }
    private bool TryDequeue(out T Record)
    {
      lock (MessageQueue)
      {
        if (MessageQueue.Count == 0)
        {
          Record = default(T);
          return false;
        }
        else
        {
          if (MessageQueue.Count == Limit)
            PostSignal.Set();

          Record = MessageQueue.Dequeue();

          return true;
        }
      }
    }

    private Action<T> ProcessDelegate;
    private Exception ProcessException;
    private bool IsActive;
    private Inv.TaskActivity ProcessTask;
    private AutoResetSignal ProcessSignal;
    private AutoResetSignal PostSignal;
    private ManualResetSignal FlushSignal;
    private Queue<T> MessageQueue;
  }
}
