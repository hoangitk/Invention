﻿using System;
using System.Runtime.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Inv.Support;

namespace InvDateTime
{
  [TestClass]
  public class DayOfWeekTests
  {
    [TestMethod]
    public void StartOfWeekDate()
    {
      var Date = new Inv.Date(2019, 03, 28);

      Assert.AreEqual(new Inv.Date(2019, 03, 25), Date.StartOfWeek(DayOfWeek.Monday));
      Assert.AreEqual(new Inv.Date(2019, 03, 24), Date.StartOfWeek(DayOfWeek.Sunday));
    }
    [TestMethod]
    public void StartOfWeekDateTime()
    {
      var Date = new DateTime(2019, 03, 28);

      Assert.AreEqual(new DateTime(2019, 03, 25), Date.StartOfWeek(DayOfWeek.Monday));
      Assert.AreEqual(new DateTime(2019, 03, 24), Date.StartOfWeek(DayOfWeek.Sunday));
    }
    [TestMethod]
    public void EndOfWeekDate()
    {
      var Date = new Inv.Date(2019, 03, 28);

      Assert.AreEqual(new Inv.Date(2019, 04, 01), Date.EndOfWeek(DayOfWeek.Monday));
      Assert.AreEqual(new Inv.Date(2019, 03, 31), Date.EndOfWeek(DayOfWeek.Sunday));
    }
    [TestMethod]
    public void EndOfWeekDateTime()
    {
      var Date = new DateTime(2019, 03, 28);

      Assert.AreEqual(new DateTime(2019, 04, 01), Date.EndOfWeek(DayOfWeek.Monday));
      Assert.AreEqual(new DateTime(2019, 03, 31), Date.EndOfWeek(DayOfWeek.Sunday));
    }
  }

  [TestClass]
  public class DateTests
  {
    [TestMethod]
    public void NextDayOfWeek()
    {
      var StartDate = new Inv.Date(2013, 9, 3);
      var FindDayOfWeek = DayOfWeek.Wednesday;

      var ExpectedResult = new Inv.Date(2013, 9, 4);
      var Result = StartDate.NextDayOfWeek(FindDayOfWeek);
      if (Result != ExpectedResult)
        Assert.Fail("NextDayOfWeek fail.");

      StartDate = new Inv.Date(2013, 9, 6);
      FindDayOfWeek = DayOfWeek.Monday;
      ExpectedResult = new Inv.Date(2013, 9, 9);
      Result = StartDate.NextDayOfWeek(FindDayOfWeek);
      if (Result != ExpectedResult)
        Assert.Fail("NextDayOfWeek fail.");

      StartDate = new Inv.Date(2013, 9, 6);
      FindDayOfWeek = DayOfWeek.Friday;
      ExpectedResult = new Inv.Date(2013, 9, 13);
      Result = StartDate.NextDayOfWeek(FindDayOfWeek);
      if (Result != ExpectedResult)
        Assert.Fail("NextDayOfWeek fail.");
    }
    [TestMethod]
    public void PreviousDayOfWeek()
    {
      var StartDate = new Inv.Date(2014, 9, 8);
      var FindDayOfWeek = DayOfWeek.Monday;
      var ExpectedResult = new Inv.Date(2014, 9, 1);
      var Result = StartDate.PreviousDayOfWeek(FindDayOfWeek);
      if (Result != ExpectedResult)
        Assert.Fail("PreviousDayOfWeek fail.");

      StartDate = new Inv.Date(2014, 9, 5);
      FindDayOfWeek = DayOfWeek.Monday;
      ExpectedResult = new Inv.Date(2014, 9, 1);
      Result = StartDate.PreviousDayOfWeek(FindDayOfWeek);
      if (Result != ExpectedResult)
        Assert.Fail("PreviousDayOfWeek fail.");

      StartDate = new Inv.Date(2014, 9, 7);
      FindDayOfWeek = DayOfWeek.Monday;
      ExpectedResult = new Inv.Date(2014, 9, 1);
      Result = StartDate.PreviousDayOfWeek(FindDayOfWeek);
      if (Result != ExpectedResult)
        Assert.Fail("PreviousDayOfWeek fail.");
    }
  }

  [TestClass]
  public class DateRangeTests
  {
    [TestMethod]
    public void Compare()
    {
      if (!Inv.DateRangeSet.Empty.IsEqualTo(Inv.DateRangeSet.Empty))
        Assert.Fail("Empty date range comparison fail.");

      if (!Inv.DateRangeSet.Universal.IsEqualTo(Inv.DateRangeSet.Universal))
        Assert.Fail("Universal date range comparison fail.");

      var LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 15));
      var RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 15));

      if (!LeftDateRangeSet.IsEqualTo(RightDateRangeSet))
        Assert.Fail("Single date range comparison fail.");

      LeftDateRangeSet = Inv.DateRangeSet.FromDateRangeArray(new[] { new Inv.DateRange(new Inv.Date(2013, 09, 08), new Inv.Date(2013, 09, 15)), new Inv.DateRange(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 15)) });
      RightDateRangeSet = Inv.DateRangeSet.FromDateRangeArray(new[] { new Inv.DateRange(new Inv.Date(2013, 09, 08), new Inv.Date(2013, 09, 15)), new Inv.DateRange(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 15)) });

      if (!LeftDateRangeSet.IsEqualTo(RightDateRangeSet))
        Assert.Fail("Multiple date range comparison fail.");
    }
    [TestMethod]
    public void Union()
    {
      var LeftDateRangeSet = new Inv.DateRangeSet();
      var RightDateRangeSet = new Inv.DateRangeSet();
      var ResultDateRangeSet = new Inv.DateRangeSet();
      if (!LeftDateRangeSet.Union(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range union #1 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2014, 01, 24), new Inv.Date(2014, 01, 26));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2014, 01, 27), null);
      ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2014, 01, 24), null);
      if (!LeftDateRangeSet.Union(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range union #2 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      if (!LeftDateRangeSet.Union(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range union #3 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 10), new Inv.Date(2013, 08, 15));
      ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 15));
      if (!LeftDateRangeSet.Union(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range union #4 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 11), new Inv.Date(2013, 08, 15));
      ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 15));
      if (!LeftDateRangeSet.Union(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range union #5 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(null, new Inv.Date(2013, 08, 09));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      ResultDateRangeSet = new Inv.DateRangeSet(null, new Inv.Date(2013, 08, 10));
      if (!LeftDateRangeSet.Union(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range union #6 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(null, new Inv.Date(2013, 08, 09));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2013, 08, 11), new Inv.Date(2013, 08, 18)));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 06), new Inv.Date(2013, 08, 13));
      ResultDateRangeSet = new Inv.DateRangeSet(null, new Inv.Date(2013, 08, 18));
      if (!LeftDateRangeSet.Union(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range union #7 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2011, 10, 22), new Inv.Date(2013, 07, 22));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2011, 10, 22), new Inv.Date(2012, 10, 22)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2012, 10, 18), new Inv.Date(2013, 01, 18)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2012, 12, 29), new Inv.Date(2013, 03, 29)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2013, 03, 15), new Inv.Date(2013, 06, 15)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2013, 07, 20), new Inv.Date(2013, 10, 20)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2013, 10, 21), new Inv.Date(2014, 01, 21)));
      ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2011, 10, 22), new Inv.Date(2014, 01, 21));
      if (!LeftDateRangeSet.IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range union #8 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2004, 12, 27), new Inv.Date(2005, 12, 27));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2006, 04, 18), new Inv.Date(2007, 04, 18)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2008, 06, 27), new Inv.Date(2009, 06, 27)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2009, 06, 29), new Inv.Date(2010, 06, 29)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2010, 06, 29), new Inv.Date(2011, 06, 29)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2011, 07, 06), new Inv.Date(2012, 07, 06)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2012, 07, 06), new Inv.Date(2013, 07, 06)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2013, 07, 06), new Inv.Date(2014, 07, 06)));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2003, 11, 04), new Inv.Date(2014, 07, 05)));
      ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2003, 11, 04), new Inv.Date(2014, 07, 06));
      if (!LeftDateRangeSet.IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range union #9 fail.");
    }
    [TestMethod]
    public void Intersect()
    {
      var LeftDateRangeSet = new Inv.DateRangeSet();
      var RightDateRangeSet = new Inv.DateRangeSet();
      var ResultDateRangeSet = new Inv.DateRangeSet();

      if (!LeftDateRangeSet.Intersect(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range intersect #1 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      if (!LeftDateRangeSet.Intersect(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range intersect #2 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 10), new Inv.Date(2013, 08, 15));
      ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 10), new Inv.Date(2013, 08, 10));
      if (!LeftDateRangeSet.Intersect(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range intersect #3 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 10));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 11), new Inv.Date(2013, 08, 15));
      ResultDateRangeSet = Inv.DateRangeSet.Empty;
      if (!LeftDateRangeSet.Intersect(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range intersect #4 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(null, new Inv.Date(2013, 08, 10));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 11), null);
      ResultDateRangeSet = Inv.DateRangeSet.Empty;
      if (!LeftDateRangeSet.Intersect(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range intersect #5 fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(null, new Inv.Date(2013, 08, 10));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 01), null);
      ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 01), new Inv.Date(2013, 08, 10));
      if (!LeftDateRangeSet.Intersect(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range intersect #6 fail.");

      LeftDateRangeSet = Inv.DateRangeSet.FromDateRangeArray(new[] { new Inv.DateRange(null, new Inv.Date(2013, 08, 01)), new Inv.DateRange(new Inv.Date(2013, 08, 15), null) });
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 08), new Inv.Date(2013, 08, 13));
      ResultDateRangeSet = Inv.DateRangeSet.Empty;
      if (!LeftDateRangeSet.Intersect(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range intersect #7 fail.");

      LeftDateRangeSet = Inv.DateRangeSet.FromDateRangeArray(new[] { new Inv.DateRange(null, new Inv.Date(2013, 08, 01)), new Inv.DateRange(new Inv.Date(2013, 08, 15), null) });
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 15), null);
      ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 15), null);
      if (!LeftDateRangeSet.Intersect(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range intersect #8 fail.");
    }
    [TestMethod]
    public void Subtract()
    {
      var LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 12), new Inv.Date(2013, 09, 12));
      var RightDateRangeSet = Inv.DateRangeSet.FromDateRangeArray(new[] { new Inv.DateRange(new Inv.Date(2013, 08, 01), new Inv.Date(2013, 08, 22)), new Inv.DateRange(new Inv.Date(2013, 08, 26), new Inv.Date(2013, 11, 29)) });
      var ResultDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2013, 08, 23), new Inv.Date(2013, 08, 25));
      if (!LeftDateRangeSet.Subtract(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range subtract fail.");

      LeftDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2014, 01, 24), new Inv.Date(2014, 02, 24));
      RightDateRangeSet = new Inv.DateRangeSet(new Inv.Date(2014, 01, 24), new Inv.Date(2014, 01, 26));
      RightDateRangeSet = RightDateRangeSet.Union(new Inv.DateRange(new Inv.Date(2014, 01, 27), null));
      ResultDateRangeSet = Inv.DateRangeSet.Empty;
      if (!LeftDateRangeSet.Subtract(RightDateRangeSet).IsEqualTo(ResultDateRangeSet))
        Assert.Fail("Date range subtract fail.");
    }
    [TestMethod]
    public void Invert()
    {
      var NothingSet = new Inv.DateRangeSet();
      var EverythingSet = NothingSet.Invert();
      if (!(EverythingSet.Count == 1 && EverythingSet.FirstRange.From == null && EverythingSet.FirstRange.Until == null))
        Assert.Fail("Nothing -> Everything Invert set fail.");
      NothingSet = EverythingSet.Invert();
      if (!(NothingSet.Count == 0))
        Assert.Fail("Everything -> Nothing Invert set fail.");

      var SingleSet = new Inv.DateRangeSet(new Inv.Date(2013, 10, 10), new Inv.Date(2013, 10, 15));
      var DoubleSet = SingleSet.Invert();
      if (!(DoubleSet.Count == 2 && DoubleSet.FirstRange.From == null && DoubleSet.FirstRange.Until == new Inv.Date(2013, 10, 09) && DoubleSet.LastRange.From == new Inv.Date(2013, 10, 16) && DoubleSet.LastRange.Until == null))
        Assert.Fail("Single -> Double Invert set fail.");

      var TwoSet = Inv.DateRangeSet.FromDateRangeArray(new[] { new Inv.DateRange(null, new Inv.Date(2013, 10, 10)), new Inv.DateRange(new Inv.Date(2013, 10, 15), null) });
      var OneSet = TwoSet.Invert();
      if (!(OneSet.Count == 1 && OneSet.FirstRange.From == new Inv.Date(2013, 10, 11) && OneSet.FirstRange.Until == new Inv.Date(2013, 10, 14)))
        Assert.Fail("Two -> One Invert set fail.");
    }
    [TestMethod]
    public void Intersects()
    {
      var DateRangeSet = new Inv.DateRangeSet(new Inv.Date(2014, 01, 01), new Inv.Date(2014, 01, 05));
      var Range = new Inv.DateRange(new Inv.Date(2014, 01, 05), new Inv.Date(2014, 01, 10));

      if (!DateRangeSet.Intersects(Range))
        Assert.Fail("Expecting the ranges to intersect as they are inclusive.");
    }
    [TestMethod]
    public void AsWordedApproximation()
    {
      const int DaysInAMonth = 30;
      // the constant above is duplicated from DateRangeHelper.ToApproximation()

      var StartDate = new Inv.Date(2014, 01, 01);

      if (new Inv.DateRange(StartDate, StartDate).ToApproximation() != "Today")
        Assert.Fail("Failed to correctly approximate a today range");

      if (new Inv.DateRange(StartDate, StartDate.AddDays(1)).ToApproximation() != "Tomorrow")
        Assert.Fail("Failed to correctly approximate a tomorrow range");

      if (new Inv.DateRange(StartDate, StartDate.AddDays(2)).ToApproximation() != "2 days")
        Assert.Fail("Failed to correctly approximate a 2 day range");

      if (new Inv.DateRange(StartDate, StartDate.AddDays(7)).ToApproximation() != "7 days")
        Assert.Fail("Failed to correctly approximate a 7 day range");

      if (new Inv.DateRange(StartDate, StartDate.AddDays(13)).ToApproximation() != "13 days")
        Assert.Fail("Failed to correctly approximate a 13 day range");

      if (new Inv.DateRange(StartDate, StartDate.AddWeeks(2)).ToApproximation() != "2 weeks")
        Assert.Fail("Failed to correctly approximate a 2 week range");

      if (new Inv.DateRange(StartDate, StartDate.AddWeeks(2).AddDays(1)).ToApproximation() != "2 weeks")
        Assert.Fail("Failed to correctly approximate a 2 week 1 day range");

      if (new Inv.DateRange(StartDate, StartDate.AddWeeks(2).AddDays(4)).ToApproximation() != "2 weeks")
        Assert.Fail("Failed to correctly approximate a 2 week 4 day range");

      if (new Inv.DateRange(StartDate, StartDate.AddWeeks(2).AddDays(5)).ToApproximation() != "3 weeks")
        Assert.Fail("Failed to correctly approximate a 2 week 5 day range");

      var WeekStartDate = StartDate.AddWeeks(4);

      if (new Inv.DateRange(StartDate, WeekStartDate.AddDays(-1)).ToApproximation() != "4 weeks")
        Assert.Fail("Failed to correctly approximate a 4 week - 1 day range");

      if (new Inv.DateRange(StartDate, WeekStartDate).ToApproximation() != "4 weeks")
        Assert.Fail("Failed to correctly approximate a 4 week - 1 day range");

      if (new Inv.DateRange(StartDate, WeekStartDate.AddDays(1)).ToApproximation() != "4 weeks")
        Assert.Fail("Failed to correctly approximate a 4 week 1 day range");

      if (new Inv.DateRange(StartDate, WeekStartDate.AddDays(2)).ToApproximation() != "1 month")
        Assert.Fail("Failed to correctly approximate a 4 week 2 day range");

      var MonthStartDate = StartDate.AddDays(DaysInAMonth);

      if (new Inv.DateRange(StartDate, MonthStartDate.AddDays(14)).ToApproximation() != "1 month")
        Assert.Fail("Failed to correctly approximate a 1 month and 14 day range");

      if (new Inv.DateRange(StartDate, MonthStartDate.AddDays(15)).ToApproximation() != "1 month")
        Assert.Fail("Failed to correctly approximate a 1 month and 15 day range");

      if (new Inv.DateRange(StartDate, MonthStartDate.AddDays(16)).ToApproximation() != "2 months")
        Assert.Fail("Failed to correctly approximate a 1 month and 16 day range");

      if (new Inv.DateRange(StartDate, StartDate.AddDays(DaysInAMonth * 2)).ToApproximation() != "2 months")
        Assert.Fail("Failed to correctly approximate a 2 month range");
    }
    [TestMethod]
    public void HUMEMembershipSuspensionUnionMerge()
    {
      var LeftDateRangeSet = Inv.DateRangeSet.Empty;
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2017, 09, 12), new Inv.Date(2017, 09, 24));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2018, 01, 13), new Inv.Date(2018, 01, 29));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2018, 04, 03), new Inv.Date(2018, 04, 17));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2018, 05, 07), new Inv.Date(2018, 05, 28));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2018, 05, 01), new Inv.Date(2018, 05, 06));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2018, 05, 29), new Inv.Date(2018, 06, 03));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2018, 06, 15), new Inv.Date(2018, 07, 23));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2018, 07, 24), new Inv.Date(2018, 09, 03));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2018, 06, 04), new Inv.Date(2018, 06, 14));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2018, 09, 17), new Inv.Date(2018, 10, 30));
      LeftDateRangeSet = LeftDateRangeSet.Union(new Inv.Date(2018, 12, 24), new Inv.Date(2019, 01, 13));

      var ResultDateRangeSet = Inv.DateRangeSet.Empty;
      ResultDateRangeSet = ResultDateRangeSet.Union(new Inv.Date(2017, 09, 12), new Inv.Date(2017, 09, 24));
      ResultDateRangeSet = ResultDateRangeSet.Union(new Inv.Date(2018, 01, 13), new Inv.Date(2018, 01, 29));
      ResultDateRangeSet = ResultDateRangeSet.Union(new Inv.Date(2018, 04, 03), new Inv.Date(2018, 04, 17));
      ResultDateRangeSet = ResultDateRangeSet.Union(new Inv.Date(2018, 05, 01), new Inv.Date(2018, 09, 03));
      ResultDateRangeSet = ResultDateRangeSet.Union(new Inv.Date(2018, 09, 17), new Inv.Date(2018, 10, 30));
      ResultDateRangeSet = ResultDateRangeSet.Union(new Inv.Date(2018, 12, 24), new Inv.Date(2019, 01, 13));

      if (!ResultDateRangeSet.IsEqualTo(LeftDateRangeSet))
        Assert.Fail("Failed to perform union");
    }
  }

  [TestClass]
  public class TimeRangeTests
  {
    [TestMethod]
    public void Compare()
    {
      if (!Inv.TimeRangeSet.Empty.IsEqualTo(Inv.TimeRangeSet.Empty))
        Assert.Fail("Empty Time range comparison fail.");

      if (!Inv.TimeRangeSet.Universal.IsEqualTo(Inv.TimeRangeSet.Universal))
        Assert.Fail("Universal Time range comparison fail.");

      var LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(11, 00)));
      var RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(11, 00)));

      if (!LeftTimeRangeSet.IsEqualTo(RightTimeRangeSet))
        Assert.Fail("Single Time range comparison fail.");

      LeftTimeRangeSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(11, 00)), new Inv.TimeRange(new Inv.Time(07, 00), new Inv.Time(08, 00)) });
      RightTimeRangeSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(11, 00)), new Inv.TimeRange(new Inv.Time(07, 00), new Inv.Time(08, 00)) });

      if (!LeftTimeRangeSet.IsEqualTo(RightTimeRangeSet))
        Assert.Fail("Multiple Time range comparison fail.");
    }
    [TestMethod]
    public void Union()
    {
      var LeftTimeRangeSet = new Inv.TimeRangeSet();
      var RightTimeRangeSet = new Inv.TimeRangeSet();
      var ResultTimeRangeSet = new Inv.TimeRangeSet();

      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #1 fail.");

      LeftTimeRangeSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(new Inv.Time(09, 00), new Inv.Time(10, 00)), new Inv.TimeRange(new Inv.Time(12, 00), new Inv.Time(14, 30)) });
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(12, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(09, 00), new Inv.Time(14, 30)));

      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #2 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(09, 00), new Inv.Time(11, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), null));
      ResultTimeRangeSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(new Inv.Time(09, 00), new Inv.Time(11, 00)), new Inv.TimeRange(new Inv.Time(12, 00), null) });
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #3 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(09, 00), new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), null));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(09, 00), null));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #3 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #4 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(15, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(15, 00)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #5 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(11, 00), new Inv.Time(15, 00)));
      ResultTimeRangeSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)), new Inv.TimeRange(new Inv.Time(11, 00), new Inv.Time(15, 00)) });
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #6 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(09, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(10, 00)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #7 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(09, 00)));
      LeftTimeRangeSet = LeftTimeRangeSet.Union(new Inv.TimeRange(new Inv.Time(11, 00), new Inv.Time(18, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(06, 00), new Inv.Time(13, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(18, 00)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #8 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(18, 00), new Inv.Time(23, 59)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(17, 00), new Inv.Time(17, 59)));
      ResultTimeRangeSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(new Inv.Time(17, 00), new Inv.Time(17, 59)), new Inv.TimeRange(new Inv.Time(18, 00), new Inv.Time(23, 59)) });
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #9 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(17, 00), new Inv.Time(17, 59)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(18, 00), new Inv.Time(23, 59)));
      ResultTimeRangeSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(new Inv.Time(17, 00), new Inv.Time(17, 59)), new Inv.TimeRange(new Inv.Time(18, 00), new Inv.Time(23, 59)) });
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #10 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(03, 00), new Inv.Time(23, 59)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(12, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(03, 00), new Inv.Time(23, 59)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #11 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(03, 00), new Inv.Time(23, 59)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(03, 00), new Inv.Time(23, 59)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #12 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(23, 59)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(23, 59)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #13 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(23, 59)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(12, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(23, 59)));
      if (!LeftTimeRangeSet.Union(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range union #14 fail.");
    }
    [TestMethod]
    public void Intersect()
    {
      var LeftTimeRangeSet = new Inv.TimeRangeSet();
      var RightTimeRangeSet = new Inv.TimeRangeSet();
      var ResultTimeRangeSet = new Inv.TimeRangeSet();

      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #1 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #2 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), null));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #3 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(10, 01)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), null));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(10, 01)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #4 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), null));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(10, 00)));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #5 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), null));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(10, 01)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(10, 01)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #6 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(11, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), null));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #7 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), null));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #8 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(12, 30)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), null));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), new Inv.Time(12, 30)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #9 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 30), new Inv.Time(14, 30)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), null));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 30), new Inv.Time(14, 30)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #10 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), null));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(11, 00)));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #11 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), null));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(12, 00)));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #12 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), null));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(12, 30)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), new Inv.Time(12, 30)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #13 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), null));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 30), new Inv.Time(14, 30)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 30), new Inv.Time(14, 30)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #14 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 30), new Inv.Time(14, 30)));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #15 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(11, 30), new Inv.Time(14, 30)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(11, 30), new Inv.Time(12, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #16 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(12, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), new Inv.Time(14, 30)));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #17 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 30), new Inv.Time(14, 30)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(12, 00)));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #18 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(11, 30), new Inv.Time(14, 30)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(12, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(11, 30), new Inv.Time(12, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #19 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(12, 00), new Inv.Time(14, 30)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(12, 00)));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #20 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 00), new Inv.Time(15, 00)));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #21 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(11, 00), new Inv.Time(15, 00)));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #22 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(11, 00), null));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #23 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(null, new Inv.Time(10, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), null));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(10, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #24 fail.");

      LeftTimeRangeSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(null, new Inv.Time(01, 00)), new Inv.TimeRange(new Inv.Time(15, 00), null) });
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 00), new Inv.Time(13, 00)));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #25 fail.");

      LeftTimeRangeSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(null, new Inv.Time(01, 00)), new Inv.TimeRange(new Inv.Time(15, 00), null) });
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(15, 00), null));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(15, 00), null));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #26 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #27 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(01, 30)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(01, 30)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #28 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 30), new Inv.Time(02, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 30), new Inv.Time(02, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #29 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 15), new Inv.Time(01, 45)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 15), new Inv.Time(01, 45)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #30 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #31 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(01, 30)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(01, 30)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #32 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 30), new Inv.Time(02, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 30), new Inv.Time(02, 00)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #33 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 15), new Inv.Time(01, 45)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(02, 00)));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 15), new Inv.Time(01, 45)));
      if (!LeftTimeRangeSet.Intersect(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range intersect #34 fail.");
    }
    [TestMethod]
    public void Subtract()
    {
      var LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 12), new Inv.Time(09, 12)));
      var RightTimeRangeSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(new Inv.Time(08, 01), new Inv.Time(08, 22)), new Inv.TimeRange(new Inv.Time(08, 26), new Inv.Time(11, 29)) });
      var ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(08, 22), new Inv.Time(08, 26)));
      if (!LeftTimeRangeSet.Subtract(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range subtract #1 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(06, 00)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 00), new Inv.Time(03, 00)));
      RightTimeRangeSet = RightTimeRangeSet.Union(new Inv.TimeRange(new Inv.Time(04, 00), null));
      ResultTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(03, 00), new Inv.Time(04, 00)));
      if (!LeftTimeRangeSet.Subtract(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range subtract #2 fail.");

      LeftTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 24), new Inv.Time(02, 24)));
      RightTimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 24), new Inv.Time(01, 26)));
      RightTimeRangeSet = RightTimeRangeSet.Union(new Inv.TimeRange(new Inv.Time(01, 26), null));
      ResultTimeRangeSet = Inv.TimeRangeSet.Empty;
      if (!LeftTimeRangeSet.Subtract(RightTimeRangeSet).IsEqualTo(ResultTimeRangeSet))
        Assert.Fail("Time range subtract #3 fail.");

    }
    [TestMethod]
    public void Invert()
    {
      var NothingSet = new Inv.TimeRangeSet();
      var EverythingSet = NothingSet.Invert();
      if (!(EverythingSet.Count == 1 && EverythingSet.FirstRange.From == null && EverythingSet.FirstRange.Until == null))
        Assert.Fail("Nothing -> Everything Invert set fail.");

      NothingSet = EverythingSet.Invert();
      if (!(NothingSet.Count == 0))
        Assert.Fail("Everything -> Nothing Invert set fail.");

      var SingleSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(10, 10), new Inv.Time(10, 15)));
      var DoubleSet = SingleSet.Invert();
      if (!(DoubleSet.Count == 2 && DoubleSet.FirstRange.From == null && DoubleSet.FirstRange.Until == new Inv.Time(10, 10) && DoubleSet.LastRange.From == new Inv.Time(10, 15) && DoubleSet.LastRange.Until == null))
        Assert.Fail("Single -> Double Invert set fail.");

      var TwoSet = Inv.TimeRangeSet.FromTimeRangeArray(new[] { new Inv.TimeRange(null, new Inv.Time(10, 10)), new Inv.TimeRange(new Inv.Time(10, 15), null) });
      var OneSet = TwoSet.Invert();
      if (!(OneSet.Count == 1 && OneSet.FirstRange.From == new Inv.Time(10, 10) && OneSet.FirstRange.Until == new Inv.Time(10, 15)))
        Assert.Fail("Two -> One Invert set fail.");
    }
    [TestMethod]
    public void Intersects()
    {
      var TimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 01), new Inv.Time(01, 05)));
      var Range = new Inv.TimeRange(new Inv.Time(01, 05), new Inv.Time(01, 10));

      if (TimeRangeSet.Intersects(Range))
        Assert.Fail("Expecting the ranges to NOT intersect as they are exclusive.");

      TimeRangeSet = new Inv.TimeRangeSet(new Inv.TimeRange(new Inv.Time(01, 01), new Inv.Time(01, 06)));
      Range = new Inv.TimeRange(new Inv.Time(01, 05), new Inv.Time(01, 10));

      if (!TimeRangeSet.Intersects(Range))
        Assert.Fail("Expecting the ranges to intersect as they are exclusive.");
    }
  }

  [TestClass]
  public class XmlSerializationTests
  { 
    [TestMethod]
    public void RoundTrip()
    {
      using (var MemoryStream = new System.IO.MemoryStream())
      {
        var FirstPackage = new XmlSerializationPackage()
        {
          Date = new Inv.Date(2013, 9, 3),
          Time = new Inv.Time(04, 01, 02, 03),
          TimePeriod = new Inv.TimePeriod(1, 2, 0, 4, 5, 6, 7, 8)
        }; // weeks are not supported in Xml.

        var BinaryFormatter = new DataContractSerializer(typeof(XmlSerializationPackage));
        BinaryFormatter.WriteObject(MemoryStream, FirstPackage);

        MemoryStream.Flush();

        MemoryStream.Position = 0;

        var SecondPackage = (XmlSerializationPackage)BinaryFormatter.ReadObject(MemoryStream);

        if (FirstPackage.Date != SecondPackage.Date)
          Assert.Fail("Xml Serialization Date fail.");

        if (FirstPackage.Time != SecondPackage.Time)
          Assert.Fail("Xml Serialization Time fail.");

        if (FirstPackage.TimePeriod != SecondPackage.TimePeriod)
          Assert.Fail("Xml Serialization TimePeriod fail.");
      }
    }

    [DataContract]
    private sealed class XmlSerializationPackage
    {
      [DataMember]
      public Inv.Date Date { get; set; }
      [DataMember]
      public Inv.Time Time { get; set; }
      [DataMember]
      public Inv.TimePeriod TimePeriod { get; set; }
    }
  }
}