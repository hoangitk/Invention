﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  public sealed class CSharpCodepoint
  {
    public CSharpCodepoint()
    {
    }

    public bool HasLabel
    {
      get { return LabelFilePath != null; }
    }

    [Conditional("DEBUG")]
    public void Anchor(int Depth = 0)
    {
      if (!HasLabel)
      {
        var StackFrame = new StackFrame(Depth + 1, true);

        this.LabelFilePath = StackFrame.GetFileName();
        this.LabelLineNumber = StackFrame.GetFileLineNumber();
      }
    }
    [Conditional("DEBUG")]
    public void Reanchor(int Depth = 0)
    {
      this.LabelFilePath = null;
      Anchor(Depth);
    }
    [Conditional("DEBUG")]
    public void Goto()
    {
      if (HasLabel)
      {
        var VisualStudio = new Inv.VisualStudioAutomation();
        VisualStudio.OpenFile(LabelFilePath, LabelLineNumber);
      }
    }
    [Conditional("DEBUG")]
    public void Raise()
    {
      if (GoToCodeRaising)
      {
        Goto();
      }
      else
      {
        GoToCodeRaising = true;
        try
        {
          var Control = System.Windows.Input.Mouse.DirectlyOver;
          var Visual = Control as System.Windows.Media.Visual;

          if (Visual != null)
            Control.RaiseEvent(new System.Windows.Input.KeyEventArgs(System.Windows.Input.Keyboard.PrimaryDevice, System.Windows.PresentationSource.FromVisual(Visual), 0, System.Windows.Input.Key.F12) { RoutedEvent = System.Windows.Input.Keyboard.KeyDownEvent });
          else
            Goto();
        }
        finally
        {
          GoToCodeRaising = false;
        }
      }
    }

    private string LabelFilePath;
    private int LabelLineNumber;
    private bool GoToCodeRaising;
  }
}
