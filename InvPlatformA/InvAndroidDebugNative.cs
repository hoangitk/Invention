using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

#if DEBUG
namespace Inv
{
  public abstract class AndroidDebugNativeActivity : Android.App.Activity
  {
    protected override void OnCreate(Android.OS.Bundle bundle)
    {
      AppDomain.CurrentDomain.UnhandledException += (Sender, Event) =>
      {
        var Exception = Event.ExceptionObject as System.Exception;
        if (Exception != null)
          System.Diagnostics.Debug.WriteLine(Exception.Message);
      };

      // TODO: this is not re-entrant.
      Android.Runtime.AndroidEnvironment.UnhandledExceptionRaiser += (Sender, Event) =>
      {
        var Exception = Event.Exception;
        if (Exception != null)
          System.Diagnostics.Debug.WriteLine(Exception.Message);
        Event.Handled = true;
      };
      RequestWindowFeature(Android.Views.WindowFeatures.NoTitle);
      Window.AddFlags(Android.Views.WindowManagerFlags.Fullscreen | Android.Views.WindowManagerFlags.HardwareAccelerated);

      try
      {
        // NOTE: this is a deliberate workaround, we need to use a different enum and cast it to get the desired behaviour.
        var UiFlags = Android.Views.SystemUiFlags.Fullscreen | Android.Views.SystemUiFlags.HideNavigation | Android.Views.SystemUiFlags.ImmersiveSticky;
        Window.DecorView.SystemUiVisibility = (Android.Views.StatusBarVisibility)UiFlags;

        base.OnCreate(bundle);
        
        var ButtonContainer = new Inv.AndroidContainer(this, null);
        SetContentView(ButtonContainer);
        ButtonContainer.LayoutParameters = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MatchParent, FrameLayout.LayoutParams.MatchParent); // root container.
        ButtonContainer.SetBackgroundColor(Android.Graphics.Color.Pink);
        ButtonContainer.SetPadding(10, 10, 10, 10);
        ButtonContainer.SetContentHorizontal(Inv.AndroidHorizontal.Center);
        ButtonContainer.SetContentVertical(Inv.AndroidVertical.Stretch);
        
        var Button = new Inv.AndroidButton(this);
        ButtonContainer.SetContentElement(Button);
        Button.SetPadding(20, 20, 20, 20);
        Button.Background.SetColor(Android.Graphics.Color.Green);
        Button.Background.SetBorderStroke(Android.Graphics.Color.Red, 10, 10, 10, 10);
        Button.Click += (Sender, Event) => PrintViewHierarchy(Button);

        var LabelContainer = new Inv.AndroidContainer(this, null);
        Button.SetContentElement(LabelContainer);
        LabelContainer.SetBackgroundColor(Android.Graphics.Color.Orange);
        LabelContainer.SetContentHorizontal(Inv.AndroidHorizontal.Stretch);
        LabelContainer.SetContentVertical(Inv.AndroidVertical.Stretch);
        
        var Label = new Inv.AndroidLabel(this);
        LabelContainer.SetContentElement(Label);
        Label.Text = "Hello World";
        Label.SetPadding(10, 10, 10, 10);
        Label.SetTextColor(Android.Graphics.Color.White);
        Label.SetBackgroundColor(Android.Graphics.Color.Purple);
        Label.SetTextSize(Android.Util.ComplexUnitType.Sp, 20);
      }
      catch (System.Exception Exception)
      {
        System.Diagnostics.Debug.WriteLine(Exception.Message);
      }
    }

    private static void PrintViewHierarchy(View Parent)
    {
      var Builder = new StringBuilder();
      Builder.AppendLine("View hierarchy:\n");
      AppendViewHierarchy(Builder, Parent);
      System.Diagnostics.Debug.Print(Builder.ToString());
    }
    private static void AppendViewHierarchy(StringBuilder Builder, View Parent, int IndentLevel = 0)
    {
      var IndentString = new String(' ', IndentLevel * 2);
      Builder.AppendFormat("{0}{1} ({2} \u00d7 {3})", IndentString, Parent.GetType().FullName, Parent.Width, Parent.Height);

      var ParentGroup = Parent as ViewGroup;
      if (ParentGroup != null && ParentGroup.ChildCount > 0)
      {
        Builder.AppendFormat("\n{0}{{\n", IndentString);

        for (var Index = 0; Index < ParentGroup.ChildCount; Index++)
        {
          var Child = ParentGroup.GetChildAt(Index);
          AppendViewHierarchy(Builder, Child, IndentLevel + 1);

          if (Index + 1 < ParentGroup.ChildCount)
            Builder.Append(",");

          Builder.AppendLine();
        }

        Builder.AppendFormat("{0}}}", IndentString);
      }
    }
  }
}
#endif