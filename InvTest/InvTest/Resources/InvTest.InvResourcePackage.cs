namespace InvTest
{
  public static class Resources
  {
    static Resources()
    {
      global::Inv.Resource.Foundation.Import(typeof(Resources), "Resources.InvTest.InvResourcePackage.rs");
    }

    public static readonly ResourcesDocuments Documents;
    public static readonly ResourcesImages Images;
    public static readonly ResourcesSounds Sounds;
    public static readonly ResourcesTexts Texts;
  }

  public sealed class ResourcesDocuments
  {
    public ResourcesDocuments() { }

    ///<Summary>(.docx) ~ (200.7KB)</Summary>
    public readonly global::Inv.Resource.BinaryReference PhoenixLogoDoc;
    ///<Summary>(.pdf) ~ (67.9KB)</Summary>
    public readonly global::Inv.Resource.BinaryReference PhoenixPlan;
  }

  public sealed class ResourcesImages
  {
    public ResourcesImages() { }

    ///<Summary>(.png) 192 x 192 (1.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AccountBoxBlack;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference AnnouncementBlack;
    ///<Summary>(.png) 192 x 192 (0.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference BookmarkWhite;
    ///<Summary>(.png) 96 x 96 (0.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ClearWhite;
    ///<Summary>(.png) 192 x 192 (2.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference HelpBlack;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference LoopBlack;
    ///<Summary>(.png) 500 x 500 (67.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhoenixLogo500x500;
    ///<Summary>(.png) 960 x 540 (74.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference PhoenixLogo960x540;
    ///<Summary>(.png) 48 x 48 (1.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RemoveCircleGray;
    ///<Summary>(.jpg) 728 x 910 (202.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference RequestForm2;
    ///<Summary>(.png) 192 x 192 (1.7KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SettingsApplicationsBlack;
    ///<Summary>(.png) 256 x 256 (2.8KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SoundOff256x256;
    ///<Summary>(.png) 256 x 256 (3.3KB)</Summary>
    public readonly global::Inv.Resource.ImageReference SoundOn256x256;
    ///<Summary>(.png) 192 x 192 (2.4KB)</Summary>
    public readonly global::Inv.Resource.ImageReference StarsBlack;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ViewHeadlineWhite;
    ///<Summary>(.png) 192 x 192 (0.1KB)</Summary>
    public readonly global::Inv.Resource.ImageReference ViewModuleWhite;
    ///<Summary>(.png) 128 x 128 (15.9KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Water128x128;
    ///<Summary>(.jpg) 1024 x 512 (193.5KB)</Summary>
    public readonly global::Inv.Resource.ImageReference Waves;
    ///<Summary>(.png) 1024 x 512 (174.2KB)</Summary>
    public readonly global::Inv.Resource.ImageReference WavesBox;
  }

  public sealed class ResourcesSounds
  {
    public ResourcesSounds() { }

    ///<Summary>(.mp3) ~ (26.3KB)</Summary>
    public readonly global::Inv.Resource.SoundReference Burn;
    ///<Summary>(.mp3) ~ (19.2KB)</Summary>
    public readonly global::Inv.Resource.SoundReference Clang;
    ///<Summary>(.mp3) ~ (13.9KB)</Summary>
    public readonly global::Inv.Resource.SoundReference Electricity;
    ///<Summary>(.mp3) ~ (21.7KB)</Summary>
    public readonly global::Inv.Resource.SoundReference Gas;
    ///<Summary>(.mp3) ~ (21.6KB)</Summary>
    public readonly global::Inv.Resource.SoundReference Magic;
  }

  public sealed class ResourcesTexts
  {
    public ResourcesTexts() { }

    ///<Summary>(.txt) Um zaxants te zo Whaxang & Jothaxang cemmunitupp bel axarr zo dinspilaxatien. (1.3KB)</Summary>
    public readonly global::Inv.Resource.TextReference Credits;
  }
}