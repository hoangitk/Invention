﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IWshRuntimeLibrary;
using System.ComponentModel;
using System.Runtime.InteropServices;

namespace Inv
{
  public sealed class FileSystemHardLink
  {
    public FileSystemHardLink(string FilePath)
    {
      this.FilePath = FilePath;
    }

    public void AddFile(string LinkFilePath)
    {
      if (!Win32.Kernel32.CreateHardLink(LinkFilePath, FilePath, IntPtr.Zero))
        throw new Win32Exception();
    }
    public IEnumerable<string> EnumerateFiles()
    {
      UInt32 Length = 256;
      var Builder = new StringBuilder(256);
      var OuterRetry = false;

      do
      {
        var Handle = Win32.Kernel32.FindFirstFileNameW(FilePath, 0, ref Length, Builder);

        if (Handle.IsInvalid)
        {
          var Error = Marshal.GetLastWin32Error();

          if (Error == Win32.Kernel32.ERROR_MORE_DATA && !OuterRetry)
          {
            // NOTE: retry only once.
            OuterRetry = true;

            Builder.EnsureCapacity((int)Length);
          }
          else
          {
            throw new Win32Exception(Error);
          }
        }
        else
        {
          OuterRetry = false;

          yield return Builder.ToString();

          var InnerRetry = false;
          bool More;
          do
          {
            Builder.Length = 0;
            
            More = Win32.Kernel32.FindNextFileNameW(Handle, ref Length, Builder);
            
            if (!More)
            {
              var error = Marshal.GetLastWin32Error();

              if (error == Win32.Kernel32.ERROR_MORE_DATA && !InnerRetry)
              {
                // Buffer needs more space.
                Builder.EnsureCapacity((int)Length);
                InnerRetry = true;
              }
              else if (error == Win32.Kernel32.ERROR_HANDLE_EOF)
              {
                // No more files.
                yield break;
              }
              else
              {
                // An unexpected error occurred
                throw new Win32Exception(error);
              }
            }
            else
            {
              InnerRetry = false;

              yield return Builder.ToString();
            }
          }
          while (More || InnerRetry);
        }
      }
      while (OuterRetry);
    }

    private string FilePath;
  }

  public sealed class FileSystemShortcutLink
  {
    public FileSystemShortcutLink(string LinkFilePath)
    {
      var Shell = new WshShell();

      var Link = (IWshShortcut)Shell.CreateShortcut(LinkFilePath);

      TargetPath = Link.TargetPath;
      WorkingDirectory = Link.WorkingDirectory;
    }

    public static void CreateShortcut(string Path, string Target, string Arguments)
    {
      var Shell = new WshShell();
      var Link = (IWshShortcut)Shell.CreateShortcut(Path);
      Link.TargetPath = Target;
      Link.Arguments = Arguments;
      Link.Save();
    }

    public string TargetPath { get; private set; }
    public string WorkingDirectory { get; private set; }
  }

  public sealed class FileSystemUriLink
  {
    public FileSystemUriLink(string UrlFilePath)
    {
      var IniFile = new Inv.IniFile();
      IniFile.Load(UrlFilePath);

      TargetAddress = IniFile.ReadSetting("InternetShortcut", "URL");
    }

    public string TargetAddress { get; private set; }
  }
}
