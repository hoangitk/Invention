﻿  using System;
  using System.Collections.Generic;
  using System.Diagnostics;
  using System.Linq;
  using System.Reflection;
  using System.Text;
  using Inv.Support;

namespace Inv
{
  public sealed class Typeswitch<TContext, TBase>
  {
    public Typeswitch()
    {
      this.ContextType = typeof(TContext);
      this.ActionDictionary = new Dictionary<Type, Action<TContext, object>>();
    }

    public void Compile(object Owner)
    {
      var BaseType = typeof(TBase);
      var OwnerType = Owner.GetType().GetReflectionInfo();
      var MethodInfoArray = OwnerType.GetReflectionMethods().Where(M => !M.IsPublic && !M.IsStatic).ToArray();

      var AddMethod = this.GetType().GetReflectionMethod("Add");

      foreach (var MethodInfo in MethodInfoArray)
      {
        var ParameterInfoArray = MethodInfo.GetParameters();

        if (ParameterInfoArray.Length == 2 && ParameterInfoArray[0].ParameterType == ContextType && ParameterInfoArray[1].ParameterType.GetReflectionInfo().IsSubclassOf(BaseType) && !ParameterInfoArray[1].ParameterType.GetReflectionInfo().IsAbstract)
        {
          var RecordType = ParameterInfoArray[1].ParameterType;

          var MethodDelegate = MethodInfo.CreateDelegate(typeof(Action<,>).MakeGenericType(ContextType, RecordType), Owner);
          AddMethod.MakeGenericMethod(RecordType).Invoke(this, new object[] { MethodDelegate });
        }
      }

#if DEBUG
      var SubtypeArray = BaseType.GetReflectionInfo().Assembly.GetReflectionTypes().Where(T => !T.GetReflectionInfo().IsAbstract && T.GetReflectionInfo().IsSubclassOf(BaseType)).ToArray();

      var MissingTypeArray = SubtypeArray.Except(ActionDictionary.Keys).ToArray();

      if (MissingTypeArray.Length > 0)
      {
        var MissingText = "Compiled route must declare methods for missing subtypes:\n" + MissingTypeArray.Select(T => T.FullName).AsSeparatedText("\n");
        throw new Exception(MissingText);
      }
#endif
    }
    public void Execute(TContext Context, TBase Record)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Record, nameof(Record));

      if (Record != null)
      {
        var RegisterType = Record.GetType();
        var Action = ActionDictionary.GetValueOrDefault(RegisterType);

        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(Action != null, "Type not registered: {0}", RegisterType.FullName);

        if (Action != null)
          Action(Context, Record);
      }
    }

    // used by reflection.
    private void Add<TSpecific>(Action<TContext, TSpecific> Action)
      where TSpecific : TBase
    {
      ActionDictionary.Add(typeof(TSpecific), (C, R) => Action(C, (TSpecific)R));
    }

    private readonly Dictionary<Type, Action<TContext, object>> ActionDictionary;
    private readonly Type ContextType;
  }
}
  