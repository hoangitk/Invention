﻿#if DEBUG
//#define PERFORMANCE
//#define MINIMUM_MEASURE
#endif
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Android.Graphics;
using Android.Views;
using Android.Media;
using Inv.Support;
using Android.Webkit;

namespace Inv
{
  public sealed class AndroidSurface : Android.Views.ViewGroup
  {
    internal AndroidSurface(Android.Content.Context context)
      : base(context)
    {
      this.SetClipChildren(false);
      this.SetWillNotDraw(false);

      this.Background = new AndroidBackground(this);

      //this.MotionEventSplittingEnabled = false;
    }

    public new AndroidBackground Background { get; }

    public void SetContentElement(Android.Views.View Element)
    {
      if (this.ContentElement != Element)
      {
        if (this.ContentElement != null)
          this.SafeRemoveView(this.ContentElement);

        this.ContentElement = Element;

        if (this.ContentElement != null)
          this.SafeAddView(this.ContentElement);

        this.Arrange();
      }
    }
    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      this.MeasureOnlyChild(ContentElement, WidthMeasureSpec, HeightMeasureSpec, out var RequestedWidth, out var RequestedHeight);

      SetMeasuredDimension(RequestedWidth, RequestedHeight);

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      this.LayoutOnlyChild(ContentElement, Left, Top, Right, Bottom);

      SW.StopLayout();
    }
    protected override void OnDraw(Android.Graphics.Canvas canvas)
    {
      base.OnDraw(canvas);

      Background.Draw(canvas);
    }

    private Android.Views.View ContentElement;
    private readonly AndroidStopwatch SW = AndroidInstrumentation.SurfacePerformance.NewStopwatch();
  }

  public sealed class AndroidButton : Android.Views.ViewGroup, Android.Views.View.IOnLongClickListener, Android.Views.View.IOnTouchListener, Android.Views.View.IOnClickListener, Android.Views.View.IOnHoverListener
  {
    internal AndroidButton(Android.Content.Context context)
      : base(context)
    {
      this.SetClipChildren(false);
      this.SetWillNotDraw(false);
      this.Background = new AndroidBackground(this);
      this.Clickable = true;

      //this.MotionEventSplittingEnabled = false;

      SetOnClickListener(this);
      SetOnLongClickListener(this);
      SetOnTouchListener(this);
      SetOnHoverListener(this);
    }

    public event Action ClickEvent;
    public event Action LongClickEvent;
    public event Action<Android.Views.MotionEventActions> TouchEvent;
    public event Action<Android.Views.MotionEventActions> HoverEvent;
    public new AndroidBackground Background { get; }

    public void SetContentElement(AndroidContainer Element)
    {
      if (this.ContentElement != Element)
      {
        if (this.ContentElement != null)
          this.SafeRemoveView(this.ContentElement);

        this.ContentElement = Element;

        if (this.ContentElement != null)
          this.SafeAddView(this.ContentElement);

        this.Arrange();
      }
    }
    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      this.MeasureOnlyChild(ContentElement, WidthMeasureSpec, HeightMeasureSpec, out var RequestedWidth, out var RequestedHeight);

      SetMeasuredDimension(RequestedWidth, RequestedHeight);

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      this.LayoutOnlyChild(ContentElement, Left, Top, Right, Bottom);

      SW.StopLayout();
    }
    protected override void OnDraw(Android.Graphics.Canvas canvas)
    {
      base.OnDraw(canvas);

      Background.Draw(canvas);
    }

    bool Android.Views.View.IOnLongClickListener.OnLongClick(Android.Views.View v)
    {
      this.ShouldClick = false;

      // signal that a long hold has happened.
      if (LongClickEvent != null)
        LongClickEvent();

      return true;
    }
    bool Android.Views.View.IOnTouchListener.OnTouch(Android.Views.View v, Android.Views.MotionEvent e)
    {
      //if (e.Action != MotionEventActions.Move)
      //  System.Diagnostics.Debug.Write(e.Action.ToString() + "|COUNT=" + e.PointerCount.ToString() + "|INDEX=" + e.ActionIndex);

      if (e.Action == MotionEventActions.Down)
      {
        this.ShouldClick = true;
      }
      else if (e.Action == MotionEventActions.Up && ShouldClick)
      {
        // NOTE: we are doing this manually so that the ClickEvent fires before the ReleaseEvent

        // release has to be inside the button to be an actual click.
        var ClickX = e.GetX();
        var ClickY = e.GetY();
        if (ClickX >= 0 && ClickX < Width && ClickY >= 0 && ClickY < Height)
        {
          if (ClickEvent != null)
            ClickEvent();
        }
      }

      if (TouchEvent != null)
        TouchEvent(e.Action);

      return false; // not handled so the click events fire.
    }
    void Android.Views.View.IOnClickListener.OnClick(Android.Views.View v)
    {
      System.Diagnostics.Debug.Assert(ShouldClick, "ShouldClick should be true since the OnClick listener has been triggered.");
      this.ShouldClick = false;
    }
    bool Android.Views.View.IOnHoverListener.OnHover(Android.Views.View v, Android.Views.MotionEvent e)
    {
      if (HoverEvent != null)
      {
        HoverEvent(e.Action);
        return true;
      }

      return false;
    }

    private AndroidContainer ContentElement;
    private bool ShouldClick;
    private readonly AndroidStopwatch SW = AndroidInstrumentation.ButtonPerformance.NewStopwatch();
  }

  public sealed class AndroidSwitch : Android.Widget.Switch, Android.Widget.CompoundButton.IOnCheckedChangeListener
  {
    internal AndroidSwitch(Android.Content.Context context)
      : base(context)
    {
      this.SetWillNotDraw(false);

      this.Background = new AndroidBackground(this);
      this.Clickable = true;
      this.ShowText = false;

      this.Gravity = Android.Views.GravityFlags.Left | Android.Views.GravityFlags.CenterVertical;

      SetOnCheckedChangeListener(this);
    }

    public event Action ChangeEvent;
    public new AndroidBackground Background { get; }

    public override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      //this.MeasureOnlyChild(ContentElement, WidthMeasureSpec, HeightMeasureSpec, out var RequestedWidth, out var RequestedHeight);

      //SetMeasuredDimension(71, 51);

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      base.OnLayout(Changed, Left, Top, Right, Bottom);

      SW.StopLayout();
    }
    protected override void OnDraw(Android.Graphics.Canvas canvas)
    {
      Background.Draw(canvas);

      base.OnDraw(canvas);
    }

    void Android.Widget.CompoundButton.IOnCheckedChangeListener.OnCheckedChanged(Android.Widget.CompoundButton v, bool isChecked)
    {
      if (ChangeEvent != null)
        ChangeEvent();
    }

    private readonly AndroidStopwatch SW = AndroidInstrumentation.SwitchPerformance.NewStopwatch();
  }
  
  public sealed class AndroidBlock : Android.Widget.TextView
  {
    internal AndroidBlock(Android.Content.Context context)
      : base(context)
    {
      this.Background = new AndroidBackground(this);

      this.Ellipsize = Android.Text.TextUtils.TruncateAt.End;
      this.Gravity = Android.Views.GravityFlags.CenterVertical;

      //base.SetTypeface(FontTypeface, Android.Graphics.TypefaceStyle.Normal);
      base.SetTextColor(Android.Graphics.Color.Black); // default Inv expectation.
      //base.SetTextSize(Android.Util.ComplexUnitType.Sp, 14);
    }

    public new AndroidBackground Background { get; }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      base.OnLayout(Changed, Left, Top, Right, Bottom);

      SW.StopLayout();
    }
    protected override void OnDraw(Android.Graphics.Canvas canvas)
    {
      Background.Draw(canvas);

      base.OnDraw(canvas);
    }

    private readonly AndroidStopwatch SW = AndroidInstrumentation.BlockPerformance.NewStopwatch();
  }

  public sealed class AndroidLabel : Android.Widget.TextView
  {
    internal AndroidLabel(Android.Content.Context context)
      : base(context)
    {
      this.Background = new AndroidBackground(this);

      this.Ellipsize = Android.Text.TextUtils.TruncateAt.End;
      this.Gravity = Android.Views.GravityFlags.CenterVertical;

      //base.SetTypeface(FontTypeface, Android.Graphics.TypefaceStyle.Normal);
      base.SetTextColor(Android.Graphics.Color.Black); // default Inv expectation.
      //base.SetTextSize(Android.Util.ComplexUnitType.Sp, 14);
    }

    public new AndroidBackground Background { get; }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      base.OnLayout(Changed, Left, Top, Right, Bottom);

      SW.StopLayout();
    }
    protected override void OnDraw(Android.Graphics.Canvas canvas)
    {
      Background.Draw(canvas);

      base.OnDraw(canvas);
    }

    private readonly AndroidStopwatch SW = AndroidInstrumentation.LabelPerformance.NewStopwatch();
  }

  public sealed class AndroidEdit : Android.Widget.EditText, Android.Widget.EditText.IOnEditorActionListener, Android.Widget.EditText.IOnKeyListener, Android.Widget.EditText.IOnFocusChangeListener, Android.Text.ITextWatcher
  {
    internal AndroidEdit(Android.Content.Context context)
      : base(context)
    {
      this.Background = new AndroidBackground(this);

      this.SetBackgroundColor(Android.Graphics.Color.Transparent);
      this.SetPadding(0, 0, 0, 0); // removes the top and bottom padding.
      this.SetTextSize(Android.Util.ComplexUnitType.Sp, 14);

      this.FocusableInTouchMode = true;

      this.SetSingleLine();

      this.OnFocusChangeListener = this;
      this.SetOnEditorActionListener(this);
      this.SetOnKeyListener(this);
      this.AddTextChangedListener(this);
    }

    public new AndroidBackground Background { get; }
    public event Action ChangeEvent;
    public event Action ReturnEvent;
    public event Action GotFocusEvent;
    public event Action LostFocusEvent;
    public new string Text
    {
      get { return base.Text; }
      set
      {
        var Cursor = base.SelectionStart;

        base.Text = value;

        // try to retain the current cursor position when the text is programmatically changed.
        var NewLength = (value ?? "").Length;
        if (Cursor > NewLength)
          Cursor = NewLength;

        if (Cursor > base.SelectionStart)
          base.SetSelection(Cursor);
      }
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      base.OnLayout(Changed, Left, Top, Right, Bottom);

      SW.StopLayout();
    }
    protected override void OnDraw(Android.Graphics.Canvas canvas)
    {
      Background.Draw(canvas);

      base.OnDraw(canvas);
    }

    void Android.Text.ITextWatcher.BeforeTextChanged(Java.Lang.ICharSequence s, int start, int count, int after)
    {
    }
    void Android.Text.ITextWatcher.OnTextChanged(Java.Lang.ICharSequence s, int start, int before, int count)
    {
    }
    void Android.Text.ITextWatcher.AfterTextChanged(Android.Text.IEditable s)
    {
      if (ChangeEvent != null)
        ChangeEvent();
    }
    bool Android.Widget.TextView.IOnEditorActionListener.OnEditorAction(Android.Widget.TextView v, Android.Views.InputMethods.ImeAction actionId, Android.Views.KeyEvent e)
    {
      if (actionId == Android.Views.InputMethods.ImeAction.Done)
        Return();

      return false;
    }
    bool Android.Widget.EditText.IOnKeyListener.OnKey(Android.Views.View v, Android.Views.Keycode keyCode, Android.Views.KeyEvent e)
    {
      if (e.Action == Android.Views.KeyEventActions.Down && keyCode == Android.Views.Keycode.Enter)
        Return();

      return false;
    }
    void Android.Widget.EditText.IOnFocusChangeListener.OnFocusChange(View v, bool hasFocus)
    {
      if (hasFocus)
      {
        if (GotFocusEvent != null)
          GotFocusEvent();
      }
      else
      {
        if (LostFocusEvent != null)
          LostFocusEvent();

        HideKeyboard(); // close the soft keyboard.
      }
    }

    private void HideKeyboard()
    {
      var imm = (Android.Views.InputMethods.InputMethodManager)this.Context.GetSystemService(Android.Content.Context.InputMethodService);
      imm.HideSoftInputFromWindow(this.WindowToken, 0);
    }
    private void Return()
    {
      HideKeyboard();

      if (ReturnEvent != null)
        ReturnEvent();
    }

    private readonly AndroidStopwatch SW = AndroidInstrumentation.EditPerformance.NewStopwatch();
  }

  public sealed class AndroidSearchFrame : Android.Views.ViewGroup, AndroidFocusContract
  {
    internal AndroidSearchFrame(Android.Content.Context context, AndroidEngine AndroidEngine)
      : base(context)
    {
      SetClipChildren(false);
      SetWillNotDraw(false);

      this.SearchView = new AndroidSearchView(context, AndroidEngine);
      this.AddView(SearchView);

      this.Background = new AndroidBackground(this);
    }

    public new AndroidBackground Background { get; }
    public event Action ChangeEvent
    {
      add => SearchView.ChangeEvent += value;
      remove => SearchView.ChangeEvent -= value;
    }
    public event Action ReturnEvent
    {
      add => SearchView.ReturnEvent += value;
      remove => SearchView.ReturnEvent -= value;
    }
    public event Action GotFocusEvent
    {
      add => SearchView.GotFocusEvent += value;
      remove => SearchView.GotFocusEvent -= value;
    }
    public event Action LostFocusEvent
    {
      add => SearchView.LostFocusEvent += value;
      remove => SearchView.LostFocusEvent -= value;
    }

    public string Query
    {
      get => SearchView.Query;
      set => SearchView.Query = value;
    }
    public Android.Widget.TextView GetTextView() => SearchView.GetTextView();
    public void SetFont(Android.Graphics.Typeface FontTypeface, Android.Graphics.Color FontColor, int FontSize) => SearchView.SetFont(FontTypeface, FontColor, FontSize);

    public override bool OnTouchEvent(Android.Views.MotionEvent Event)
    {
      if (Event.Action == MotionEventActions.Down)
        return SearchView.RequestFocus();
      else
        return base.OnTouchEvent(Event);
    }
    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      this.MeasureOnlyChild(SearchView, WidthMeasureSpec, HeightMeasureSpec, out var RequestedWidth, out var RequestedHeight);

      SetMeasuredDimension(RequestedWidth, RequestedHeight);

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      this.LayoutOnlyChild(SearchView, Left, Top, Right, Bottom);

      SW.StopLayout();
    }
    protected override void OnDraw(Android.Graphics.Canvas canvas)
    {
      base.OnDraw(canvas);

      Background.Draw(canvas);
    }

    Android.Views.View AndroidFocusContract.View => GetTextView();

    private readonly AndroidSearchView SearchView;
    private readonly AndroidStopwatch SW = AndroidInstrumentation.SearchPerformance.NewStopwatch();
  }

  internal interface AndroidFocusContract
  {
    Android.Views.View View { get; }
  }

  internal sealed class AndroidSearchView : Android.Widget.SearchView, Android.Widget.SearchView.IOnQueryTextListener, Android.Widget.SearchView.IOnFocusChangeListener
  {
    public AndroidSearchView(Android.Content.Context context, AndroidEngine AndroidEngine)
      : base(context)
    {
      this.AndroidEngine = AndroidEngine;
      this.SetWillNotDraw(false);
      this.FocusableInTouchMode = true;

      base.SetPadding(0, 0, 0, 0);

      this.EditText = (Android.Widget.AutoCompleteTextView)FindViewById(Context.Resources.GetIdentifier("android:id/search_src_text", null, null));
      EditText.SetBackgroundColor(Android.Graphics.Color.Transparent);
      EditText.SetTextSize(Android.Util.ComplexUnitType.Sp, 14);
      EditText.EditorAction += (Sender, Event) =>
      {
        if (Event.ActionId == Android.Views.InputMethods.ImeAction.Search)
        {
          ReturnInvoke();

          ClearFocus(); // close the soft keyboard.
        }
      };
      EditText.OnFocusChangeListener = this;

      //this.PlateView = FindViewById(Context.Resources.GetIdentifier("android:id/search_plate", null, null));
      //this.SearchEditFrame = FindViewById(Context.Resources.GetIdentifier("android:id/search_edit_frame", null, null));
      //this.SubmitArea = FindViewById(Context.Resources.GetIdentifier("android:id/submit_area", null, null));
      //this.SubmitButton = (Android.Widget.ImageView)FindViewById(Context.Resources.GetIdentifier("android:id/search_go_btn", null, null));
      //this.SearchButton = (Android.Widget.ImageView)FindViewById(Context.Resources.GetIdentifier("android:id/search_button", null, null));
      //this.VoiceButton = FindViewById(Context.Resources.GetIdentifier("android:id/search_voice_btn", null, null));
      //this.SearchHintIcon = (Android.Widget.ImageView)FindViewById(Context.Resources.GetIdentifier("android:id/search_mag_icon", null, null));
      this.CloseButton = (Android.Widget.ImageView)FindViewById(Context.Resources.GetIdentifier("android:id/search_close_btn", null, null));

      // TODO: not sure about this - it fixes a height problem (collapsed is +8px taller than it should be, but expanded is correct).
      OnActionViewExpanded();

      SetOnQueryTextListener(this);

      ClearFocus(); // NOTE: otherwise, the newly created search view will automatically take focus and displays the soft keyboard.

      ZeroChildMargins(this);

      EditText.Hint = ""; // TODO: can't get a flat colour unicode magnifying glass?

#if DEBUG
      //EditText.SetBackgroundColor(Android.Graphics.Color.Red);
      //PlateView.SetBackgroundColor(Android.Graphics.Color.Blue);
#endif
    }

    //public new AndroidBackground Background { get; }
    public event Action ChangeEvent;
    public event Action ReturnEvent;
    public event Action GotFocusEvent;
    public event Action LostFocusEvent;
    public new string Query
    {
      get => base.Query;
      set => this.SetQuery(value, submit: false);
    }

    public Android.Widget.TextView GetTextView()
    {
      return EditText;
    }
    public void SetFont(Android.Graphics.Typeface FontTypeface, Android.Graphics.Color FontColor, int FontSize)
    {
      EditText.SetTypeface(FontTypeface, Android.Graphics.TypefaceStyle.Normal);
      EditText.SetTextColor(FontColor);
      EditText.SetHintTextColor(FontColor);
      EditText.SetTextSize(Android.Util.ComplexUnitType.Sp, FontSize);

      // TODO: the magnifying glass is not changing its colour.
      //SearchHintIcon.SetColorFilter(FontColor, Android.Graphics.PorterDuff.Mode.SrcAtop);

      // NOTE: but this does change the 'cancel search' X.
      CloseButton.SetColorFilter(FontColor, Android.Graphics.PorterDuff.Mode.SrcAtop);

      //SubmitButton.SetColorFilter(FontColor, Android.Graphics.PorterDuff.Mode.SrcAtop);

      //SearchButton.SetColorFilter(FontColor, Android.Graphics.PorterDuff.Mode.SrcAtop);
    }

    bool Android.Widget.SearchView.IOnQueryTextListener.OnQueryTextChange(string newText)
    {
      if (ChangeEvent != null)
        ChangeEvent();

      return false;
    }
    bool Android.Widget.SearchView.IOnQueryTextListener.OnQueryTextSubmit(string query)
    {
      ReturnInvoke();

      return false;
    }
    void Android.Widget.SearchView.IOnFocusChangeListener.OnFocusChange(View v, bool hasFocus)
    {
      if (hasFocus)
      {
        if (GotFocusEvent != null)
          GotFocusEvent();
      }
      else
      {
        if (LostFocusEvent != null)
          LostFocusEvent();

        ClearFocus(); // required when navigating away from the view.
      }
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      // remeasure without the useless top/bottom padding.
      base.OnMeasure(MeasureSpec.MakeMeasureSpec(MeasuredWidth, MeasureSpecMode.Exactly), MeasureSpec.MakeMeasureSpec(MeasuredHeight - AndroidEngine.PtToPx(16), MeasureSpecMode.Exactly));
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      base.OnLayout(Changed, Left, Top, Right, Bottom);
    }

    private void ReturnInvoke()
    {
      if (ReturnEvent != null)
        ReturnEvent();
    }
    [Conditional("DEBUG")]
    private void Find(Android.Views.ViewGroup Self)
    {
      for (var i = 0; i < Self.ChildCount; i++)
      {
        var v = Self.GetChildAt(i);

        System.Diagnostics.Debug.WriteLine(v.GetType().Name);
        System.Diagnostics.Debug.WriteLine("PADDING: " + v.PaddingLeft + ", " + v.PaddingTop + ", " + v.PaddingRight + ", " + v.PaddingBottom);
        v.SetPadding(0, 0, 0, 0);

        var LLP = v.LayoutParameters as Android.Widget.LinearLayout.LayoutParams;
        System.Diagnostics.Debug.WriteLine("MARGIN: " + LLP.LeftMargin + ", " + LLP.TopMargin + ", " + LLP.RightMargin + ", " + LLP.BottomMargin);
        //v.LayoutParameters = new LinearLayout.LayoutParams(LLP.Width, LLP.Height, LLP.Weight);
        //v.RequestLayout();

        if (v is Android.Views.ViewGroup)
          Find(v as Android.Views.ViewGroup);
      }
    }

    private static void ZeroChildMargins(ViewGroup ViewGroup)
    {
      var ChildCount = ViewGroup.ChildCount;
      for (var ChildIndex = 0; ChildIndex < ChildCount; ChildIndex++)
      {
        var ChildView = ViewGroup.GetChildAt(ChildIndex);

        if (ChildView is ViewGroup)
          ZeroChildMargins((ViewGroup)ChildView);

        var LayoutParams = ChildView.LayoutParameters as ViewGroup.MarginLayoutParams;
        if (LayoutParams != null)
        {
          //if (LayoutParams.LeftMargin != 0 || LayoutParams.RightMargin != 0 || LayoutParams.TopMargin != 0 || LayoutParams.BottomMargin != 0)
          //  System.Diagnostics.Debug.WriteLine("**" + ChildView.ToString());

          LayoutParams.LeftMargin = 0;
          LayoutParams.RightMargin = 0;
          LayoutParams.TopMargin = 0;
          LayoutParams.BottomMargin = 0;
        }

        //if (ChildView.PaddingLeft != 0 || ChildView.PaddingRight != 0 || ChildView.PaddingTop != 0 || ChildView.PaddingBottom != 0)
        //  System.Diagnostics.Debug.WriteLine("**" + ChildView.ToString());

        //if (ChildView is Android.Widget.ImageView)
        //  ChildView.SetBackgroundColor(Android.Graphics.Color.Red);
        //else
        ChildView.SetBackgroundColor(Android.Graphics.Color.Transparent);

        ChildView.SetPadding(0, 0, 0, 0);
      }
    }

    private readonly AndroidEngine AndroidEngine;
    private readonly Android.Widget.AutoCompleteTextView EditText;
    //private readonly Android.Views.View PlateView;
    //private readonly Android.Views.View SearchEditFrame;
    //private readonly Android.Views.View SubmitArea;
    //private readonly Android.Widget.ImageView SubmitButton;
    //private readonly Android.Widget.ImageView SearchButton;
    //private readonly Android.Views.View VoiceButton;
    //private readonly Android.Widget.ImageView SearchHintIcon;
    private readonly Android.Widget.ImageView CloseButton;
  }

  public sealed class AndroidMemo : AndroidGroup, Android.Text.ITextWatcher, Android.Widget.EditText.IOnFocusChangeListener
  {
    internal AndroidMemo(Android.Content.Context context)
      : base(context)
    {
      this.SetClipChildren(true);

      this.VerticalScroll = new AndroidVerticalScroll(context);
      AddView(VerticalScroll);

      this.EditText = new Android.Widget.EditText(context);
      VerticalScroll.SetContentElement(EditText);
      EditText.SetPadding(0, 0, 0, 0); // removes the top and bottom padding.
      EditText.Background = null;
      EditText.Gravity = Android.Views.GravityFlags.Top;
      EditText.AddTextChangedListener(this);
      EditText.OnFocusChangeListener = this;
      EditText.SetTextSize(Android.Util.ComplexUnitType.Sp, 14);
    }

    public string Text
    {
      get { return EditText.Text; }
      set
      {
        var Cursor = EditText.SelectionStart;

        EditText.Text = value;

        // try to retain the current cursor position when the text is programmatically changed.
        var NewLength = (value ?? "").Length;
        if (Cursor > NewLength)
          Cursor = NewLength;

        if (Cursor > EditText.SelectionStart)
          EditText.SetSelection(Cursor);
      }
    }
    public bool IsReadOnly
    {
      get { return EditText.KeyListener == null; }
      set
      {
        if (IsReadOnly != value)
        {
          var Swap = this.ReadOnlyKeyListener;
          this.ReadOnlyKeyListener = EditText.KeyListener;
          EditText.KeyListener = Swap;
        }
      }
    }
    public event Action ChangeEvent;
    public event Action GotFocusEvent;
    public event Action LostFocusEvent;

    public Android.Widget.TextView GetTextView()
    {
      return EditText;
    }
    public void SetSpannableString(Android.Text.SpannableString String)
    {
      EditText.SetText(String, Android.Widget.TextView.BufferType.Spannable);
    }
    public void SetTypeface(Android.Graphics.Typeface tf, Android.Graphics.TypefaceStyle style)
    {
      EditText.SetTypeface(tf, style);
    }
    public void SetTextColor(Android.Graphics.Color color)
    {
      EditText.SetTextColor(color);
    }
    public void SetTextSize(Android.Util.ComplexUnitType unit, float size)
    {
      EditText.SetTextSize(unit, size);
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      this.MeasureOnlyChild(VerticalScroll, WidthMeasureSpec, HeightMeasureSpec, out var RequestedWidth, out var RequestedHeight);

      SetMeasuredDimension(RequestedWidth, RequestedHeight);

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      this.LayoutOnlyChild(VerticalScroll, Left, Top, Right, Bottom);

      SW.StopLayout();
    }

    void Android.Text.ITextWatcher.BeforeTextChanged(Java.Lang.ICharSequence s, int start, int count, int after)
    {
    }
    void Android.Text.ITextWatcher.OnTextChanged(Java.Lang.ICharSequence s, int start, int before, int count)
    {
    }
    void Android.Text.ITextWatcher.AfterTextChanged(Android.Text.IEditable s)
    {
      if (ChangeEvent != null)
        ChangeEvent();
    }
    void Android.Widget.EditText.IOnFocusChangeListener.OnFocusChange(View v, bool hasFocus)
    {
      if (hasFocus)
      {
        if (GotFocusEvent != null)
          GotFocusEvent();
      }
      else
      {
        if (LostFocusEvent != null)
          LostFocusEvent();
      }
    }

    private Android.Text.Method.IKeyListener ReadOnlyKeyListener;
    private readonly AndroidVerticalScroll VerticalScroll;
    private readonly Android.Widget.EditText EditText;
    private readonly AndroidStopwatch SW = AndroidInstrumentation.MemoPerformance.NewStopwatch();
  }

  public sealed class AndroidDock : AndroidGroup
  {
    internal AndroidDock(Android.Content.Context Context)
      : base(Context)
    {
      this.ElementList = new DistinctList<AndroidContainer>();
      this.HeaderList = new DistinctList<AndroidContainer>();
      this.ClientList = new DistinctList<AndroidContainer>();
      this.FooterList = new DistinctList<AndroidContainer>();
    }

    public Android.Widget.Orientation Orientation
    {
      get { return OrientationField; }
      set
      {
        if (OrientationField != value)
        {
          this.OrientationField = value;
          this.Arrange();
        }
      }
    }

    internal void ComposeElements(IEnumerable<AndroidContainer> Headers, IEnumerable<AndroidContainer> Clients, IEnumerable<AndroidContainer> Footers)
    {
      this.HeaderList.Clear();
      HeaderList.AddRange(Headers);

      this.ClientList.Clear();
      ClientList.AddRange(Clients);

      this.FooterList.Clear();
      FooterList.AddRange(Footers);

      var PreviousElementList = ElementList;
      this.ElementList = new DistinctList<AndroidContainer>(HeaderList.Count + ClientList.Count + FooterList.Count);
      ElementList.AddRange(HeaderList);
      ElementList.AddRange(ClientList);
      ElementList.AddRange(FooterList);

      foreach (var Element in ElementList.Except(PreviousElementList))
        this.SafeAddView(Element);

      foreach (var Header in PreviousElementList.Except(ElementList))
        this.SafeRemoveView(Header);

      this.Arrange();
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      if (!ElementList.Any())
      {
        this.MeasureOnlyChild(null, WidthMeasureSpec, HeightMeasureSpec, out var RequestedWidth, out var RequestedHeight);
        SetMeasuredDimension(RequestedWidth, RequestedHeight);
      }
      else
      {
        var WidthMode = MeasureSpec.GetMode(WidthMeasureSpec);
        var HeightMode = MeasureSpec.GetMode(HeightMeasureSpec);
        var HorizontalPadding = PaddingLeft + PaddingRight;
        var VerticalPadding = PaddingTop + PaddingBottom;

        var RequestedWidth = MeasuredWidth;
        var RequestedHeight = MeasuredHeight;
#if MINIMUM_MEASURE
        if (RequestedWidth < MinimumWidth)
          RequestedWidth = MinimumWidth;

        if (RequestedHeight < MinimumHeight)
          RequestedHeight = MinimumHeight;
#endif

        var ClientArray = ClientList.Where(C => C.ContentVisible).ToArray();

        switch (Orientation)
        {
          case Android.Widget.Orientation.Horizontal:
            var ElementHeightSpec = MeasureSpec.MakeMeasureSpec(Math.Max(0, RequestedHeight - VerticalPadding), HeightMode);

            switch (WidthMode)
            {
              case Android.Views.MeasureSpecMode.AtMost:
              case Android.Views.MeasureSpecMode.Exactly:
                var RemainingWidth = Math.Max(0, RequestedWidth - HorizontalPadding);

                foreach (var Element in HeaderList.Union(FooterList))
                {
                  Element.Measure(MeasureSpec.MakeMeasureSpec(RemainingWidth, Android.Views.MeasureSpecMode.AtMost), ElementHeightSpec);
                  RemainingWidth -= Element.MeasuredWidth;
                  if (RemainingWidth < 0)
                    RemainingWidth = 0;
                }

                if (ClientArray.Length > 0)
                {
                  var ClientWidthSpec = MeasureSpec.MakeMeasureSpec(RemainingWidth / ClientArray.Length, WidthMode);

                  foreach (var Client in ClientArray)
                    Client.Measure(ClientWidthSpec, ElementHeightSpec);
                }

                var ToMeasureList = new Inv.DistinctList<MeasureableElement>();

                if (HeightMode != Android.Views.MeasureSpecMode.Exactly)
                {
                  var ElementHeight = ElementList.Max(E => E.MeasuredHeight);
                  ElementHeightSpec = MeasureSpec.MakeMeasureSpec(ElementHeight, Android.Views.MeasureSpecMode.Exactly);

                  ToMeasureList.AddRange(ElementList.Where(E => E.MeasuredHeight != ElementHeight).Select(E => new MeasureableElement(E, null, ElementHeightSpec)));
                }

                if (WidthMode == Android.Views.MeasureSpecMode.AtMost && ClientArray.Length > 1)
                {
                  var ClientWidth = ClientArray.Max(C => C.MeasuredWidth);
                  var ClientWidthSpec = MeasureSpec.MakeMeasureSpec(ClientWidth, Android.Views.MeasureSpecMode.Exactly);
                  foreach (var Client in ClientArray.Where(C => C.MeasuredWidth != ClientWidth))
                  {
                    var ExistingItem = ToMeasureList.FirstOrDefault(R => R.Element == Client);

                    if (ExistingItem == null)
                      ToMeasureList.Add(new MeasureableElement(Client, ClientWidthSpec, ElementHeightSpec));
                    else
                      ExistingItem.WidthMeasureSpec = ClientWidthSpec;
                  }
                }

                foreach (var Element in ToMeasureList)
                  Element.Measure();
                break;

              case Android.Views.MeasureSpecMode.Unspecified:
                foreach (var Element in ElementList)
                  Element.Measure(WidthMeasureSpec, ElementHeightSpec);

                if (ClientArray.Length > 1)
                {
                  var ClientWidth = ClientArray.Max(C => C.MeasuredWidth);
                  var ClientWidthSpec = MeasureSpec.MakeMeasureSpec(ClientWidth, Android.Views.MeasureSpecMode.Exactly);

                  foreach (var Client in ClientArray.Where(C => C.MeasuredWidth != ClientWidth))
                    Client.Measure(ClientWidthSpec, ElementHeightSpec);
                }
                break;

              default:
                throw new UnhandledEnumCaseException<Android.Views.MeasureSpecMode>();
            }

            var HorizontalSummedWidth = ElementList.Sum(E => E.MeasuredWidth) + HorizontalPadding;
            var HorizontalMaxedHeight = ElementList.Max(E => E.MeasuredHeight) + VerticalPadding;

            var HorizontalRequestedWidth = WidthMode == Android.Views.MeasureSpecMode.Exactly ? RequestedWidth : WidthMode == Android.Views.MeasureSpecMode.AtMost ? Math.Min(HorizontalSummedWidth, MeasuredWidth) : HorizontalSummedWidth;
            var HorizontalRequestedHeight = HeightMode == Android.Views.MeasureSpecMode.Exactly ? RequestedHeight : HeightMode == Android.Views.MeasureSpecMode.AtMost ? Math.Min(HorizontalMaxedHeight, MeasuredHeight) : HorizontalMaxedHeight;
            SetMeasuredDimension(HorizontalRequestedWidth, HorizontalRequestedHeight);
            break;

          case Android.Widget.Orientation.Vertical:
            var ElementWidthSpec = MeasureSpec.MakeMeasureSpec(Math.Max(0, RequestedWidth - HorizontalPadding), WidthMode);

            switch (HeightMode)
            {
              case Android.Views.MeasureSpecMode.AtMost:
              case Android.Views.MeasureSpecMode.Exactly:
                var RemainingHeight = Math.Max(0, RequestedHeight - VerticalPadding);

                foreach (var Element in HeaderList.Union(FooterList))
                {
                  Element.Measure(ElementWidthSpec, MeasureSpec.MakeMeasureSpec(RemainingHeight, Android.Views.MeasureSpecMode.AtMost));
                  RemainingHeight -= Element.MeasuredHeight;
                  if (RemainingHeight < 0)
                    RemainingHeight = 0;
                }

                if (ClientArray.Length > 0)
                {
                  var ClientHeightSpec = MeasureSpec.MakeMeasureSpec(RemainingHeight / ClientArray.Length, HeightMode);

                  foreach (var Client in ClientArray)
                    Client.Measure(ElementWidthSpec, ClientHeightSpec);
                }

                var ToMeasureList = new DistinctList<MeasureableElement>();

                if (WidthMode != Android.Views.MeasureSpecMode.Exactly)
                {
                  var ElementWidth = ElementList.Max(E => E.MeasuredWidth);
                  ElementWidthSpec = MeasureSpec.MakeMeasureSpec(ElementWidth, Android.Views.MeasureSpecMode.Exactly);

                  ToMeasureList.AddRange(ElementList.Where(E => E.MeasuredWidth != ElementWidth).Select(E => new MeasureableElement(E, ElementWidthSpec, null)));
                }

                if (HeightMode == Android.Views.MeasureSpecMode.AtMost && ClientArray.Length > 1)
                {
                  var ClientHeight = ClientArray.Max(C => C.MeasuredHeight);
                  var ClientHeightSpec = MeasureSpec.MakeMeasureSpec(ClientHeight, Android.Views.MeasureSpecMode.Exactly);
                  foreach (var Client in ClientArray.Where(C => C.MeasuredHeight != ClientHeight))
                  {
                    var ExistingItem = ToMeasureList.FirstOrDefault(R => R.Element == Client);

                    if (ExistingItem == null)
                      ToMeasureList.Add(new MeasureableElement(Client, ElementWidthSpec, ClientHeightSpec));
                    else
                      ExistingItem.HeightMeasureSpec = ClientHeightSpec;
                  }
                }

                foreach (var Element in ToMeasureList)
                  Element.Measure();
                break;

              case Android.Views.MeasureSpecMode.Unspecified:
                foreach (var Element in ElementList)
                  Element.Measure(ElementWidthSpec, HeightMeasureSpec);

                if (ClientArray.Length > 1)
                {
                  var ClientHeight = ClientArray.Max(C => C.MeasuredHeight);
                  var ClientHeightSpec = MeasureSpec.MakeMeasureSpec(ClientHeight, Android.Views.MeasureSpecMode.Exactly);

                  foreach (var Client in ClientArray.Where(C => C.MeasuredHeight != ClientHeight))
                    Client.Measure(ElementWidthSpec, ClientHeightSpec);
                }
                break;

              default:
                throw new UnhandledEnumCaseException<Android.Views.MeasureSpecMode>();
            }

            var VerticalMaxedWidth = ElementList.Max(E => E.MeasuredWidth) + HorizontalPadding;
            var VerticalSummedHeight = ElementList.Sum(E => E.MeasuredHeight) + VerticalPadding;

            var VerticalRequestedWidth = WidthMode == Android.Views.MeasureSpecMode.Exactly ? RequestedWidth : WidthMode == Android.Views.MeasureSpecMode.AtMost ? Math.Min(VerticalMaxedWidth, RequestedWidth) : VerticalMaxedWidth;
            var VerticalRequestedHeight = HeightMode == Android.Views.MeasureSpecMode.Exactly ? RequestedHeight : HeightMode == Android.Views.MeasureSpecMode.AtMost ? Math.Min(VerticalSummedHeight, RequestedHeight) : VerticalSummedHeight;
            SetMeasuredDimension(VerticalRequestedWidth, VerticalRequestedHeight);
            break;

          default:
            throw new UnhandledEnumCaseException<Android.Widget.Orientation>();
        }
      }

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      var DockWidth = Right - Left;
      var DockHeight = Bottom - Top;
      var ClientArray = ClientList.Where(C => C.ContentVisible).ToArray();

      switch (Orientation)
      {
        case Android.Widget.Orientation.Horizontal:
          var PanelLeft = PaddingLeft;
          var HeaderWidth = 0;

          var TopEdge = PaddingTop;
          if (TopEdge > DockHeight)
            TopEdge = DockHeight;

          var BottomEdge = DockHeight - PaddingBottom;
          if (BottomEdge < 0)
            BottomEdge = DockHeight;

          foreach (var Header in HeaderList)
          {
            Header.LayoutChild(PanelLeft, TopEdge, PanelLeft + Header.MeasuredWidth, BottomEdge);
            PanelLeft += Header.MeasuredWidth;
            HeaderWidth += Header.MeasuredWidth;
          }

          var FooterWidth = FooterList.Sum(F => F.MeasuredWidth);
          var RemainderWidth = DockWidth - HeaderWidth - FooterWidth - PaddingLeft - PaddingRight;

          if (ClientArray.Length > 0)
          {
            var SharedWidth = RemainderWidth < 0 ? 0 : RemainderWidth / ClientArray.Length;

            var LastClient = ClientArray.Last();

            foreach (var Client in ClientArray)
            {
              var FrameWidth = SharedWidth;
              if (RemainderWidth > 0 && Client == LastClient)
                FrameWidth += RemainderWidth - (SharedWidth * ClientArray.Length);

              Client.LayoutChild(PanelLeft, TopEdge, PanelLeft + FrameWidth, BottomEdge);
              PanelLeft += FrameWidth;
            }
          }

          var PanelRight = (RemainderWidth < 0 ? DockWidth - RemainderWidth : DockWidth) - PaddingRight;

          foreach (var Footer in FooterList)
          {
            PanelRight -= Footer.MeasuredWidth;
            Footer.LayoutChild(PanelRight, TopEdge, PanelRight + Footer.MeasuredWidth, BottomEdge);
          }
          break;

        case Android.Widget.Orientation.Vertical:
          var PanelTop = PaddingTop;
          var HeaderHeight = 0;

          var LeftEdge = PaddingLeft;
          if (LeftEdge > DockWidth)
            LeftEdge = DockWidth;

          var RightEdge = DockWidth - PaddingRight;
          if (RightEdge < 0)
            RightEdge = DockWidth;

          foreach (var Header in HeaderList)
          {
            Header.LayoutChild(LeftEdge, PanelTop, RightEdge, PanelTop + Header.MeasuredHeight);
            PanelTop += Header.MeasuredHeight;
            HeaderHeight += Header.MeasuredHeight;
          }

          var FooterHeight = FooterList.Sum(F => F.MeasuredHeight);
          var RemainderHeight = DockHeight - HeaderHeight - FooterHeight - PaddingTop - PaddingBottom;

          if (ClientArray.Length > 0)
          {
            var SharedHeight = RemainderHeight < 0 ? 0 : RemainderHeight / ClientArray.Length;
            var LastClient = ClientArray.Last();

            foreach (var Client in ClientArray)
            {
              var FrameHeight = SharedHeight;
              if (RemainderHeight > 0 && Client == LastClient)
                FrameHeight += RemainderHeight - (SharedHeight * ClientArray.Length);

              Client.LayoutChild(LeftEdge, PanelTop, RightEdge, PanelTop + FrameHeight);
              PanelTop += FrameHeight;
            }
          }

          var PanelBottom = (RemainderHeight < 0 ? DockHeight - RemainderHeight : DockHeight) - PaddingBottom;

          foreach (var Footer in FooterList)
          {
            PanelBottom -= Footer.MeasuredHeight;
            Footer.LayoutChild(LeftEdge, PanelBottom, RightEdge, PanelBottom + Footer.MeasuredHeight);
          }
          break;

        default:
          throw new UnhandledEnumCaseException<Android.Widget.Orientation>();
      }

      SW.StopLayout();
    }

    private Android.Widget.Orientation OrientationField;
    private Inv.DistinctList<AndroidContainer> ElementList;
    private readonly Inv.DistinctList<AndroidContainer> HeaderList;
    private readonly Inv.DistinctList<AndroidContainer> ClientList;
    private readonly Inv.DistinctList<AndroidContainer> FooterList;
    private readonly AndroidStopwatch SW = AndroidInstrumentation.DockPerformance.NewStopwatch();

    private sealed class MeasureableElement
    {
      public MeasureableElement(AndroidContainer Element, int? WidthMeasureSpec, int? HeightMeasureSpec)
      {
        this.Element = Element;
        this.WidthMeasureSpec = WidthMeasureSpec;
        this.HeightMeasureSpec = HeightMeasureSpec;
      }

      public void Measure()
      {
        Element.Measure(WidthMeasureSpec ?? MeasureSpec.MakeMeasureSpec(Element.MeasuredWidth, Android.Views.MeasureSpecMode.Exactly), HeightMeasureSpec ?? MeasureSpec.MakeMeasureSpec(Element.MeasuredHeight, Android.Views.MeasureSpecMode.Exactly));
      }

      public AndroidContainer Element { get; }
      public int? WidthMeasureSpec { get; set; }
      public int? HeightMeasureSpec { get; set; }
    }
  }

  public sealed class AndroidStack : AndroidGroup
  {
    internal AndroidStack(Android.Content.Context context)
      : base(context)
    {
      this.ElementArray = new Android.Views.View[] { };
    }

    public Android.Widget.Orientation Orientation
    {
      get { return OrientationField; }
      set
      {
        if (OrientationField != value)
        {
          this.OrientationField = value;
          this.Arrange();
        }
      }
    }

    internal void ComposeElements(IEnumerable<Android.Views.View> Elements)
    {
      var PreviousArray = ElementArray;
      this.ElementArray = Elements.ToArray();

      foreach (var Element in ElementArray.Except(PreviousArray))
        this.SafeAddView(Element);

      foreach (var Element in PreviousArray.Except(ElementArray))
        this.SafeRemoveView(Element);

      this.Arrange();
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      if (ElementArray.Length == 0)
      {
        this.MeasureOnlyChild(null, WidthMeasureSpec, HeightMeasureSpec, out var RequestedWidth, out var RequestedHeight);
        SetMeasuredDimension(RequestedWidth, RequestedHeight);
      }
      else
      {
        var WidthMode = MeasureSpec.GetMode(WidthMeasureSpec);
        var HeightMode = MeasureSpec.GetMode(HeightMeasureSpec);
        var HorizontalPadding = PaddingLeft + PaddingRight;
        var VerticalPadding = PaddingTop + PaddingBottom;

        var RequestedWidth = MeasuredWidth;
        var RequestedHeight = MeasuredHeight;
#if MINIMUM_MEASURE
        if (RequestedWidth < MinimumWidth)
          RequestedWidth = MinimumWidth;

        if (RequestedHeight < MinimumHeight)
          RequestedHeight = MinimumHeight;
#endif

        switch (Orientation)
        {
          case Android.Widget.Orientation.Horizontal:
            var ElementHeightSpec = MeasureSpec.MakeMeasureSpec(Math.Max(0, RequestedHeight - VerticalPadding), HeightMode);
            var RemainingWidth = Math.Max(0, RequestedWidth - HorizontalPadding);

            switch (WidthMode)
            {
              case Android.Views.MeasureSpecMode.AtMost:
              case Android.Views.MeasureSpecMode.Exactly:
                foreach (var Element in ElementArray)
                {
                  Element.Measure(MeasureSpec.MakeMeasureSpec(RemainingWidth, Android.Views.MeasureSpecMode.AtMost), ElementHeightSpec);
                  RemainingWidth -= Element.MeasuredWidth;
                  if (RemainingWidth < 0)
                    RemainingWidth = 0;
                }
                break;

              case Android.Views.MeasureSpecMode.Unspecified:
                var WidthSpec = MeasureSpec.MakeMeasureSpec(RemainingWidth, WidthMode);
                foreach (var Element in ElementArray)
                  Element.Measure(WidthSpec, ElementHeightSpec);
                break;

              default:
                throw new UnhandledEnumCaseException<Android.Views.MeasureSpecMode>();
            }

            if (HeightMode != Android.Views.MeasureSpecMode.Exactly)
            {
              var ElementHeight = ElementArray.Max(E => E.MeasuredHeight);
              ElementHeightSpec = MeasureSpec.MakeMeasureSpec(ElementHeight, Android.Views.MeasureSpecMode.Exactly);

              foreach (var Element in ElementArray.Where(E => E.MeasuredHeight != ElementHeight))
                Element.Measure(MeasureSpec.MakeMeasureSpec(Element.MeasuredWidth, Android.Views.MeasureSpecMode.Exactly), ElementHeightSpec);
            }

            var HorizontalSummedWidth = ElementArray.Sum(E => E.MeasuredWidth) + HorizontalPadding;
            var HorizontalMaxedHeight = ElementArray.Max(E => E.MeasuredHeight) + VerticalPadding;

            var HorizontalRequestedWidth = WidthMode == Android.Views.MeasureSpecMode.Exactly ? RequestedWidth : WidthMode == Android.Views.MeasureSpecMode.AtMost ? Math.Min(HorizontalSummedWidth, RequestedWidth) : HorizontalSummedWidth;
            var HorizontalRequestedHeight = HeightMode == Android.Views.MeasureSpecMode.Exactly ? RequestedHeight : HeightMode == Android.Views.MeasureSpecMode.AtMost ? Math.Min(HorizontalMaxedHeight, RequestedHeight) : HorizontalMaxedHeight;
            SetMeasuredDimension(HorizontalRequestedWidth, HorizontalRequestedHeight);
            break;

          case Android.Widget.Orientation.Vertical:
            var ElementWidthSpec = MeasureSpec.MakeMeasureSpec(Math.Max(0, RequestedWidth - HorizontalPadding), WidthMode);
            var RemainingHeight = Math.Max(0, RequestedHeight - VerticalPadding);

            switch (HeightMode)
            {
              case Android.Views.MeasureSpecMode.AtMost:
              case Android.Views.MeasureSpecMode.Exactly:
                foreach (var Element in ElementArray)
                {
                  Element.Measure(ElementWidthSpec, MeasureSpec.MakeMeasureSpec(RemainingHeight, Android.Views.MeasureSpecMode.AtMost));
                  RemainingHeight -= Element.MeasuredHeight;
                  if (RemainingHeight < 0)
                    RemainingHeight = 0;
                }
                break;

              case Android.Views.MeasureSpecMode.Unspecified:
                var HeightSpec = MeasureSpec.MakeMeasureSpec(RemainingHeight, HeightMode);
                foreach (var Element in ElementArray)
                  Element.Measure(ElementWidthSpec, HeightSpec);
                break;

              default:
                throw new UnhandledEnumCaseException<Android.Views.MeasureSpecMode>();
            }

            if (WidthMode != Android.Views.MeasureSpecMode.Exactly)
            {
              var ElementWidth = ElementArray.Max(E => E.MeasuredWidth);
              ElementWidthSpec = MeasureSpec.MakeMeasureSpec(ElementWidth, Android.Views.MeasureSpecMode.Exactly);

              foreach (var Element in ElementArray.Where(E => E.MeasuredWidth != ElementWidth))
                Element.Measure(ElementWidthSpec, MeasureSpec.MakeMeasureSpec(Element.MeasuredHeight, Android.Views.MeasureSpecMode.Exactly));
            }

            var VerticalMaxedWidth = ElementArray.Max(E => E.MeasuredWidth) + HorizontalPadding;
            var VerticalSummedHeight = ElementArray.Sum(E => E.MeasuredHeight) + VerticalPadding;

            var VerticalRequestedWidth = WidthMode == Android.Views.MeasureSpecMode.Exactly ? RequestedWidth : WidthMode == Android.Views.MeasureSpecMode.AtMost ? Math.Min(VerticalMaxedWidth, RequestedWidth) : VerticalMaxedWidth;
            var VerticalRequestedHeight = HeightMode == Android.Views.MeasureSpecMode.Exactly ? RequestedHeight : HeightMode == Android.Views.MeasureSpecMode.AtMost ? Math.Min(VerticalSummedHeight, RequestedHeight) : VerticalSummedHeight;
            SetMeasuredDimension(VerticalRequestedWidth, VerticalRequestedHeight);
            break;

          default:
            throw new UnhandledEnumCaseException<Android.Widget.Orientation>();
        }
      }

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      switch (Orientation)
      {
        case Android.Widget.Orientation.Horizontal:
          var StackHeight = Bottom - Top;

          var EdgeTop = PaddingTop;
          if (EdgeTop > StackHeight)
            EdgeTop = StackHeight;

          var EdgeBottom = StackHeight - PaddingBottom;
          if (EdgeBottom < 0)
            EdgeBottom = StackHeight;

          var ElementLeft = PaddingLeft;
          foreach (var Element in ElementArray)
          {
            Element.LayoutChild(ElementLeft, EdgeTop, ElementLeft + Element.MeasuredWidth, EdgeBottom);
            ElementLeft += Element.MeasuredWidth;
          }
          break;

        case Android.Widget.Orientation.Vertical:
          var StackWidth = Right - Left;

          var EdgeLeft = PaddingLeft;
          if (EdgeLeft > StackWidth)
            EdgeLeft = StackWidth;

          var EdgeRight = StackWidth - PaddingRight;
          if (EdgeRight < 0)
            EdgeRight = StackWidth;

          var ElementTop = PaddingTop;
          foreach (var Element in ElementArray)
          {
            Element.LayoutChild(EdgeLeft, ElementTop, EdgeRight, ElementTop + Element.MeasuredHeight);
            ElementTop += Element.MeasuredHeight;
          }
          break;

        default:
          throw new UnhandledEnumCaseException<Android.Widget.Orientation>();
      }

      SW.StopLayout();
    }

    private Android.Widget.Orientation OrientationField;
    private Android.Views.View[] ElementArray;
    private readonly AndroidStopwatch SW = AndroidInstrumentation.StackPerformance.NewStopwatch();
  }

  public sealed class AndroidTable : AndroidGroup
  {
    internal AndroidTable(Android.Content.Context context)
      : base(context)
    {
      this.RowList = new Inv.DistinctList<AndroidTableRow>();
      this.ColumnList = new Inv.DistinctList<AndroidTableColumn>();
      this.CellList = new Inv.DistinctList<AndroidTableCell>();
      this.ElementList = new Inv.DistinctList<AndroidContainer>();
    }

    internal void ComposeElements(IEnumerable<AndroidTableRow> Rows, IEnumerable<AndroidTableColumn> Columns, IEnumerable<AndroidTableCell> Cells)
    {
      this.RowList.Clear();
      RowList.AddRange(Rows);

      this.ColumnList.Clear();
      ColumnList.AddRange(Columns);

      this.CellList.Clear();
      CellList.AddRange(Cells);

      var PreviousElementList = ElementList;
      this.ElementList = new Inv.DistinctList<AndroidContainer>(RowList.Count + ColumnList.Count + CellList.Count);
      ElementList.AddRange(RowList.Select(R => R.Element).ExceptNull());
      ElementList.AddRange(ColumnList.Select(R => R.Element).ExceptNull());
      ElementList.AddRange(CellList.Select(R => R.Element).ExceptNull());

      foreach (var Element in ElementList.Except(PreviousElementList))
        this.SafeAddView(Element);

      foreach (var Header in PreviousElementList.Except(ElementList))
        this.SafeRemoveView(Header);

      this.Arrange();
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      var WidthMode = MeasureSpec.GetMode(WidthMeasureSpec);
      var HeightMode = MeasureSpec.GetMode(HeightMeasureSpec);
      var WidthSize = MeasureSpec.GetSize(WidthMeasureSpec);
      var HeightSize = MeasureSpec.GetSize(HeightMeasureSpec);

      //var AvailableWidth = WidthSize - PaddingLeft - PaddingRight;
      //var AvailableHeight = HeightSize - PaddingTop - PaddingBottom;

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);
      var AvailableWidth = MeasuredWidth;
      var AvailableHeight = MeasuredHeight;

      var InternalWidthMode = WidthMode == Android.Views.MeasureSpecMode.Exactly && ColumnList.Count != 1 ? Android.Views.MeasureSpecMode.AtMost : WidthMode;
      var InternalHeightMode = HeightMode == Android.Views.MeasureSpecMode.Exactly && RowList.Count != 1 ? Android.Views.MeasureSpecMode.AtMost : HeightMode;

      var WidestRowWidth = 0;
      var TallestColumnHeight = 0;

      var StarRowArray = RowList.Where(R => R.Length.IsStar).ToArray();
      var StarColumnArray = ColumnList.Where(C => C.Length.IsStar).ToArray();

      var StarRowWeight = StarRowArray.Sum(R => R.Length.Size ?? 1);
      var StarColumnWeight = StarColumnArray.Sum(C => C.Length.Size ?? 1);

      // Fixed rows.
      var FixedHeight = 0;
      foreach (var Row in RowList)
      {
        Row.TempHeight = -1;

        if (Row.Length.IsFixed)
        {
          Row.TempHeight = Row.Length.Size ?? 0;
          FixedHeight += Row.TempHeight;

          if (Row.Element != null)
          {
            Row.Element.Measure(
              MeasureSpec.MakeMeasureSpec(AvailableWidth, WidthMode),
              MeasureSpec.MakeMeasureSpec(Row.TempHeight, Android.Views.MeasureSpecMode.Exactly));

            if (Row.Element.MeasuredWidth > WidestRowWidth)
              WidestRowWidth = Row.Element.MeasuredWidth;
          }
        }
      }

      // fixed columns.
      var FixedWidth = 0;
      foreach (var Column in ColumnList)
      {
        Column.TempWidth = -1;

        if (Column.Length.IsFixed)
        {
          Column.TempWidth = Column.Length.Size ?? 0;
          FixedWidth += Column.TempWidth;

          if (Column.Element != null)
          {
            Column.Element.Measure(
              MeasureSpec.MakeMeasureSpec(Column.TempWidth, Android.Views.MeasureSpecMode.Exactly),
              MeasureSpec.MakeMeasureSpec(AvailableHeight, HeightMode));

            if (Column.Element.MeasuredHeight > TallestColumnHeight)
              TallestColumnHeight = Column.Element.MeasuredHeight;
          }
        }
      }

      var RemainingWidth = AvailableWidth - FixedWidth;
      var RemainingHeight = AvailableHeight - FixedHeight;

      // auto rows.
      foreach (var Row in RowList.Where(R => R.Length.IsAuto))
      {
        var RowWidth = 0;
        var RowHeight = 0;

        if (Row.Element != null)
        {
          Row.Element.Measure(MeasureSpec.MakeMeasureSpec(AvailableWidth, WidthMode), MeasureSpec.MakeMeasureSpec(RemainingHeight, InternalHeightMode));
          RowWidth = Row.Element.MeasuredWidth;
          RowHeight = Row.Element.MeasuredHeight;
        }

        var UsedWidth = 0;
        foreach (var Cell in CellList.Where(C => C.Row == Row && C.Element != null))
        {
          Cell.Element.Measure(
            Cell.Column.TempWidth == -1 ? MeasureSpec.MakeMeasureSpec(Math.Max(RemainingWidth - UsedWidth, 0), InternalWidthMode) : MeasureSpec.MakeMeasureSpec(Cell.Column.TempWidth, Android.Views.MeasureSpecMode.Exactly),
            MeasureSpec.MakeMeasureSpec(RemainingHeight, InternalHeightMode));

          UsedWidth += Cell.Element.MeasuredWidth;
          if (Cell.Element.MeasuredHeight > RowHeight)
            RowHeight = Cell.Element.MeasuredHeight;
        }

        RowWidth = Math.Max(RowWidth, UsedWidth);
        if (RowWidth > WidestRowWidth)
          WidestRowWidth = RowWidth;

        Row.TempHeight = RowHeight;
        RemainingHeight -= RowHeight;
      }

      // auto columns.
      foreach (var Column in ColumnList.Where(C => C.Length.IsAuto))
      {
        var ColumnWidth = 0;
        var ColumnHeight = 0;

        if (Column.Element != null)
        {
          Column.Element.Measure(MeasureSpec.MakeMeasureSpec(RemainingWidth, InternalWidthMode), MeasureSpec.MakeMeasureSpec(AvailableHeight, HeightMode));
          ColumnWidth = Column.Element.MeasuredWidth;
          ColumnHeight = Column.Element.MeasuredHeight;
        }

        var UsedHeight = 0;
        foreach (var Cell in CellList.Where(C => C.Column == Column && C.Element != null))
        {
          Cell.Element.Measure(
            MeasureSpec.MakeMeasureSpec(RemainingWidth, InternalWidthMode),
            Cell.Row.TempHeight == -1 ? MeasureSpec.MakeMeasureSpec(Math.Max(RemainingHeight - UsedHeight, 0), InternalHeightMode) : MeasureSpec.MakeMeasureSpec(Cell.Row.TempHeight, Android.Views.MeasureSpecMode.Exactly));

          UsedHeight += Cell.Element.MeasuredHeight;
          if (Cell.Element.MeasuredWidth > ColumnWidth)
            ColumnWidth = Cell.Element.MeasuredWidth;
        }

        ColumnHeight = Math.Max(ColumnHeight, UsedHeight);
        if (ColumnHeight > TallestColumnHeight)
          TallestColumnHeight = ColumnHeight;

        Column.TempWidth = ColumnWidth;
        RemainingWidth -= ColumnWidth;
      }

      var StarRowFactor = StarRowWeight == 0 ? 0f : RemainingHeight / (float)StarRowWeight;
      var StarColumnFactor = StarColumnWeight == 0 ? 0f : RemainingWidth / (float)StarColumnWeight;
      var StarWidthMode = WidthMode;// == Android.Views.MeasureSpecMode.Exactly ? WidthMode : Android.Views.MeasureSpecMode.AtMost;
      var StarHeightMode = HeightMode;// == Android.Views.MeasureSpecMode.Exactly ? HeightMode : Android.Views.MeasureSpecMode.AtMost;

      // star rows.
      foreach (var Row in StarRowArray)
      {
        var TargetRowHeight = (int)(StarRowFactor * (Row.Length.Size ?? 1));

        var RowWidth = 0;
        var RowHeight = -1;

        if (Row.Element != null)
        {
          Row.Element.Measure(
            MeasureSpec.MakeMeasureSpec(WidestRowWidth, WidthMode),
            MeasureSpec.MakeMeasureSpec(TargetRowHeight, StarHeightMode));

          RowWidth = Row.Element.MeasuredWidth;
          RowHeight = Row.Element.MeasuredHeight;
        }

        var UsedWidth = 0;
        foreach (var Cell in CellList.Where(C => C.Row == Row && C.Element != null))
        {
          var CellWidthSpec = Cell.Column.TempWidth == -1 ? MeasureSpec.MakeMeasureSpec(Math.Max(RemainingWidth - UsedWidth, 0), InternalWidthMode) : MeasureSpec.MakeMeasureSpec(Cell.Column.TempWidth, Android.Views.MeasureSpecMode.Exactly);
          var CellHeightSpec = MeasureSpec.MakeMeasureSpec(Math.Max(RowHeight, TargetRowHeight), StarHeightMode);
          Cell.Element.Measure(CellWidthSpec, CellHeightSpec);

          UsedWidth += Cell.Element.MeasuredWidth;
          if (Cell.Element.MeasuredHeight > RowHeight)
            RowHeight = Cell.Element.MeasuredHeight;
        }

        RowWidth = Math.Max(RowWidth, UsedWidth);
        if (RowWidth > WidestRowWidth)
          WidestRowWidth = RowWidth;

        Row.TempHeight = RowHeight;
        RemainingHeight -= RowHeight;
      }

      // star columns.
      foreach (var Column in StarColumnArray)
      {
        var TargetColumnWidth = (int)(StarColumnFactor * (Column.Length.Size ?? 1));

        var ColumnWidth = -1;
        var ColumnHeight = 0;

        if (Column.Element != null)
        {
          Column.Element.Measure(
            MeasureSpec.MakeMeasureSpec(TargetColumnWidth, StarWidthMode),
            MeasureSpec.MakeMeasureSpec(TallestColumnHeight, HeightMode));

          ColumnWidth = Column.Element.MeasuredWidth;
          ColumnHeight = Column.Element.MeasuredHeight;
        }

        var UsedHeight = 0;
        foreach (var Cell in CellList.Where(C => C.Column == Column && C.Element != null))
        {
          Cell.Element.Measure(
            MeasureSpec.MakeMeasureSpec(Math.Max(ColumnWidth, TargetColumnWidth), StarWidthMode),
            MeasureSpec.MakeMeasureSpec(Cell.Row.TempHeight, Android.Views.MeasureSpecMode.Exactly));

          UsedHeight += Cell.Element.MeasuredHeight;

          if (Cell.Element.MeasuredWidth > ColumnWidth)
            ColumnWidth = Cell.Element.MeasuredWidth;
        }

        ColumnHeight = Math.Max(ColumnHeight, UsedHeight);
        if (ColumnHeight > TallestColumnHeight)
          TallestColumnHeight = ColumnHeight;

        Column.TempWidth = ColumnWidth;
        RemainingWidth -= ColumnWidth;
      }

      var FinalWidth = Math.Max(WidestRowWidth, ColumnList.Sum(C => C.TempWidth));
      var FinalHeight = Math.Max(TallestColumnHeight, RowList.Sum(R => R.TempHeight));

      // row elements.
      var RowOffset = 0;
      foreach (var Row in RowList)
      {
        Row.TempTop = RowOffset;
        RowOffset += Row.TempHeight;
        if (Row.Element != null && (Row.Element.MeasuredWidth != FinalWidth || Row.Element.MeasuredHeight != Row.TempHeight))
        {
          Row.Element.Measure(
            MeasureSpec.MakeMeasureSpec(FinalWidth, Android.Views.MeasureSpecMode.Exactly),
            MeasureSpec.MakeMeasureSpec(Row.TempHeight, Android.Views.MeasureSpecMode.Exactly));
        }
      }

      // column elements.
      var ColumnOffset = 0;
      foreach (var Column in ColumnList)
      {
        Column.TempLeft = ColumnOffset;
        ColumnOffset += Column.TempWidth;
        if (Column.Element != null && (Column.Element.MeasuredWidth != Column.TempWidth || Column.Element.MeasuredHeight != FinalHeight))
        {
          Column.Element.Measure(
            MeasureSpec.MakeMeasureSpec(Column.TempWidth, Android.Views.MeasureSpecMode.Exactly),
            MeasureSpec.MakeMeasureSpec(FinalHeight, Android.Views.MeasureSpecMode.Exactly));
        }
      }

      // re-measure cells exactly based on what we have learned about column/row length.
      foreach (var Cell in CellList)
      {
        if (Cell.Element != null && (Cell.Element.MeasuredWidth != Cell.Column.TempWidth || Cell.Element.MeasuredHeight != Cell.Row.TempHeight))
        {
          Cell.Element.Measure(
            MeasureSpec.MakeMeasureSpec(Cell.Column.TempWidth, Android.Views.MeasureSpecMode.Exactly),
            MeasureSpec.MakeMeasureSpec(Cell.Row.TempHeight, Android.Views.MeasureSpecMode.Exactly));
        }
      }

      if (WidthMode == Android.Views.MeasureSpecMode.Exactly && FinalWidth != WidthSize)
        FinalWidth = WidthSize;

      if (HeightMode == Android.Views.MeasureSpecMode.Exactly && FinalHeight != HeightSize)
        FinalHeight = HeightSize;

      if (FinalWidth != MeasuredWidth || FinalHeight != MeasuredHeight)
        SetMeasuredDimension(FinalWidth, FinalHeight);

      SW.StopMeasure();// $"({WidthMode}:{WidthSize} | {HeightMode}:{HeightSize}) = $({FinalWidth}, {FinalHeight})");
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      var TableWidth = Right - Left;
      var TableHeight = Bottom - Top;

      var ColumnTop = 0;
      var ColumnBottom = TableHeight;// - PaddingBottom;
      var RowLeft = 0;
      var RowRight = TableWidth;// - PaddingRight;

      foreach (var Column in ColumnList)
      {
        if (Column.Element != null)
          Column.Element.LayoutChild(Column.TempLeft, ColumnTop, Column.TempLeft + Column.TempWidth, ColumnBottom);
      }

      foreach (var Row in RowList)
      {
        if (Row.Element != null)
          Row.Element.LayoutChild(RowLeft, Row.TempTop, RowRight, Row.TempTop + Row.TempHeight);
      }

      foreach (var Cell in CellList)
      {
        if (Cell.Element != null)
          Cell.Element.LayoutChild(Cell.Column.TempLeft, Cell.Row.TempTop, Cell.Column.TempLeft + Cell.Column.TempWidth, Cell.Row.TempTop + Cell.Row.TempHeight);
      }

      SW.StopLayout();// $"**TABLE** LAYOUT {SW.ElapsedMilliseconds} ms ({Left}, {Top}, {Right}, {Bottom})");
    }

    internal Inv.DistinctList<AndroidTableRow> RowList { get; private set; }
    internal Inv.DistinctList<AndroidTableColumn> ColumnList { get; private set; }
    internal Inv.DistinctList<AndroidTableCell> CellList { get; private set; }

    private Inv.DistinctList<AndroidContainer> ElementList;
    private readonly AndroidStopwatch SW = AndroidInstrumentation.TablePerformance.NewStopwatch();
  }

  internal sealed class AndroidTableLength
  {
    public AndroidTableLength(bool Fill, int? Value)
    {
      this.Fill = Fill;
      this.Size = Value;
    }

    public bool Fill { get; private set; }
    public int? Size { get; private set; }
    public bool IsStar
    {
      get { return Fill; }
    }
    public bool IsAuto
    {
      get { return !Fill && Size == null; }
    }
    public bool IsFixed
    {
      get { return !Fill && Size != null; }
    }
  }

  internal sealed class AndroidTableColumn
  {
    public AndroidTableColumn(AndroidContainer Element, bool Fill, int? Value)
    {
      this.Element = Element;
      this.Length = new AndroidTableLength(Fill, Value);
    }

    public AndroidContainer Element { get; private set; }
    public AndroidTableLength Length { get; private set; }

    internal int TempLeft;
    internal int TempWidth;
  }

  internal sealed class AndroidTableRow
  {
    public AndroidTableRow(AndroidContainer Element, bool Fill, int? Value)
    {
      this.Element = Element;
      this.Length = new AndroidTableLength(Fill, Value);
    }

    public AndroidContainer Element { get; private set; }
    public AndroidTableLength Length { get; private set; }

    internal int TempTop;
    internal int TempHeight;
  }

  internal sealed class AndroidTableCell
  {
    public AndroidTableCell(AndroidTableColumn Column, AndroidTableRow Row, AndroidContainer Element)
    {
      this.Column = Column;
      this.Row = Row;
      this.Element = Element;
    }

    public AndroidTableColumn Column { get; private set; }
    public AndroidTableRow Row { get; private set; }
    public AndroidContainer Element { get; private set; }
  }

  public sealed class AndroidWrap : AndroidGroup
  {
    internal AndroidWrap(Android.Content.Context context)
      : base(context)
    {
      this.ElementArray = new AndroidContainer[] { };
      this.ElementLineList = new Inv.DistinctList<ElementLine>();
    }

    public Android.Widget.Orientation Orientation
    {
      get { return OrientationField; }
      set
      {
        if (OrientationField != value)
        {
          this.OrientationField = value;
          this.Arrange();
        }
      }
    }

    internal void ComposeElements(IEnumerable<AndroidContainer> Elements)
    {
      var PreviousArray = ElementArray;
      this.ElementArray = Elements.ToArray();

      foreach (var Element in ElementArray.Except(PreviousArray))
        this.SafeAddView(Element);

      foreach (var Element in PreviousArray.Except(ElementArray))
        this.SafeRemoveView(Element);

      this.Arrange();
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      ElementLineList.Clear();

      if (ElementArray.Length == 0)
      {
        this.MeasureOnlyChild(null, WidthMeasureSpec, HeightMeasureSpec, out var RequestedWidth, out var RequestedHeight);
        SetMeasuredDimension(RequestedWidth, RequestedHeight);
      }
      else
      {
        var WidthMode = MeasureSpec.GetMode(WidthMeasureSpec);
        var HeightMode = MeasureSpec.GetMode(HeightMeasureSpec);

        var WidthSize = MeasureSpec.GetSize(WidthMeasureSpec);
        var HeightSize = MeasureSpec.GetSize(HeightMeasureSpec);

        var InnerWidthMode = WidthMode == Android.Views.MeasureSpecMode.Unspecified ? Android.Views.MeasureSpecMode.Unspecified : Android.Views.MeasureSpecMode.AtMost;
        var InnerHeightMode = HeightMode == Android.Views.MeasureSpecMode.Unspecified ? Android.Views.MeasureSpecMode.Unspecified : Android.Views.MeasureSpecMode.AtMost;

        var HorizontalPadding = PaddingLeft + PaddingRight;
        var VerticalPadding = PaddingTop + PaddingBottom;

        var UsableWidthSize = Math.Max(0, WidthSize - HorizontalPadding);
        var UsableHeightSize = Math.Max(0, HeightSize - VerticalPadding);

        var CurrentX = 0;
        var CurrentY = 0;
        var LineSize = 0;
        var CurrentLineElementList = new Inv.DistinctList<AndroidContainer>();

        switch (Orientation)
        {
          case Android.Widget.Orientation.Horizontal:
            var InnerWidthMeasureSpec = MeasureSpec.MakeMeasureSpec(UsableWidthSize, InnerWidthMode);
            var MaximumWidth = 0;

            foreach (var Element in ElementArray)
            {
              Element.Measure(
                InnerWidthMeasureSpec,
                MeasureSpec.MakeMeasureSpec(Math.Max(0, UsableHeightSize - CurrentY), InnerHeightMode)
              );

              if (CurrentX > 0 && CurrentX + Element.MeasuredWidth > UsableWidthSize && InnerWidthMode != Android.Views.MeasureSpecMode.Unspecified)
              {
                // Available height has changed since element will be on a new line, so re-measure
                Element.Measure(
                  InnerWidthMeasureSpec,
                  MeasureSpec.MakeMeasureSpec(Math.Max(0, UsableHeightSize - CurrentY - LineSize), InnerHeightMode)
                );

                ElementLineList.Add(new ElementLine(CurrentLineElementList, LineSize));
                CurrentLineElementList.Clear();
                CurrentX = 0;
                CurrentY += LineSize;
                LineSize = 0;
              }

              if (Element.MeasuredHeight > LineSize)
                LineSize = Element.MeasuredHeight;

              CurrentX += Element.MeasuredWidth;
              CurrentLineElementList.Add(Element);

              if (CurrentX > MaximumWidth)
                MaximumWidth = CurrentX;
            }

            if (CurrentLineElementList.Any())
              ElementLineList.Add(new ElementLine(CurrentLineElementList, LineSize));

            SetMeasuredDimension(
              WidthMode == Android.Views.MeasureSpecMode.Exactly ? WidthSize : MaximumWidth + HorizontalPadding,
              HeightMode == Android.Views.MeasureSpecMode.Exactly ? HeightSize : CurrentY + LineSize + VerticalPadding
            );
            break;

          case Android.Widget.Orientation.Vertical:
            var InnerHeightMeasureSpec = MeasureSpec.MakeMeasureSpec(UsableHeightSize, InnerHeightMode);
            var MaximumHeight = 0;

            foreach (var Element in ElementArray)
            {
              Element.Measure(
                MeasureSpec.MakeMeasureSpec(Math.Max(0, UsableWidthSize - CurrentX), InnerWidthMode),
                InnerHeightMeasureSpec
              );

              if (CurrentY > 0 && CurrentY + Element.MeasuredHeight > UsableHeightSize && InnerHeightMode != Android.Views.MeasureSpecMode.Unspecified)
              {
                // Available width has changed since element will be on a new line, so re-measure
                Element.Measure(
                  MeasureSpec.MakeMeasureSpec(Math.Max(0, UsableWidthSize - CurrentX - LineSize), InnerWidthMode),
                  InnerHeightMeasureSpec
                );

                ElementLineList.Add(new ElementLine(CurrentLineElementList, LineSize));
                CurrentLineElementList.Clear();
                CurrentX += LineSize;
                CurrentY = 0;
                LineSize = 0;
              }

              if (Element.MeasuredWidth > LineSize)
                LineSize = Element.MeasuredWidth;

              CurrentY += Element.MeasuredHeight;
              CurrentLineElementList.Add(Element);

              if (CurrentY > MaximumHeight)
                MaximumHeight = CurrentY;
            }

            if (CurrentLineElementList.Any())
              ElementLineList.Add(new ElementLine(CurrentLineElementList, LineSize));

            SetMeasuredDimension(
              WidthMode == Android.Views.MeasureSpecMode.Exactly ? WidthSize : CurrentX + LineSize + HorizontalPadding,
              HeightMode == Android.Views.MeasureSpecMode.Exactly ? HeightSize : MaximumHeight + VerticalPadding
            );
            break;

          default:
            throw new UnhandledEnumCaseException<Android.Widget.Orientation>();
        }
      }

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      var MinX = PaddingLeft;
      var MinY = PaddingTop;

      var CurrentX = MinX;
      var CurrentY = MinY;

      switch (Orientation)
      {
        case Android.Widget.Orientation.Horizontal:
          foreach (var Line in ElementLineList)
          {
            CurrentX = MinX;

            foreach (var Element in Line.ElementArray)
            {
              Element.Layout(CurrentX, CurrentY, CurrentX + Element.MeasuredWidth, CurrentY + Line.LineSize);
              CurrentX += Element.MeasuredWidth;
            }

            CurrentY += Line.LineSize;
          }
          break;

        case Android.Widget.Orientation.Vertical:
          foreach (var Line in ElementLineList)
          {
            CurrentY = MinY;

            foreach (var Element in Line.ElementArray)
            {
              Element.Layout(CurrentX, CurrentY, CurrentX + Line.LineSize, CurrentY + Element.MeasuredHeight);
              CurrentY += Element.MeasuredHeight;
            }

            CurrentX += Line.LineSize;
          }
          break;

        default:
          throw new UnhandledEnumCaseException<Android.Widget.Orientation>();
      }

      SW.StopLayout();
    }

    private Android.Widget.Orientation OrientationField;
    private AndroidContainer[] ElementArray;
    private readonly Inv.DistinctList<ElementLine> ElementLineList;
    private readonly AndroidStopwatch SW = AndroidInstrumentation.WrapPerformance.NewStopwatch();

    private sealed class ElementLine
    {
      public ElementLine(IEnumerable<AndroidContainer> Elements, int LineSize)
      {
        this.ElementArray = Elements.ToArray();
        this.LineSize = LineSize;
      }

      public AndroidContainer[] ElementArray { get; }
      public int LineSize { get; }
    }
  }

  public sealed class AndroidBoard : AndroidGroup
  {
    internal AndroidBoard(Android.Content.Context context)
      : base(context)
    {
      this.PinArray = new AndroidPin[] { };
      this.ElementArray = new AndroidContainer[] { };
    }

    public void ComposePins(IEnumerable<AndroidPin> Pins)
    {
      var PreviousPinArray = PinArray;
      var PreviousElementArray = ElementArray;
      this.PinArray = Pins.ToArray();
      this.ElementArray = PinArray.Select(P => P.Element).ToArray();

      foreach (var Element in ElementArray.Except(PreviousElementArray))
        this.SafeAddView(Element);

      foreach (var Element in PreviousElementArray.Except(ElementArray))
        this.SafeRemoveView(Element);

      this.Arrange();
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      if (PinArray.Length == 0)
      {
        this.MeasureOnlyChild(null, WidthMeasureSpec, HeightMeasureSpec, out var RequestedWidth, out var RequestedHeight);
        SetMeasuredDimension(RequestedWidth, RequestedHeight);
      }
      else
      {
        foreach (var Pin in PinArray)
          Pin.Element.Measure(MeasureSpec.MakeMeasureSpec(Pin.Right - Pin.Left, Android.Views.MeasureSpecMode.Exactly), MeasureSpec.MakeMeasureSpec(Pin.Bottom - Pin.Top, Android.Views.MeasureSpecMode.Exactly));

        var WidthMode = MeasureSpec.GetMode(WidthMeasureSpec);
        var HeightMode = MeasureSpec.GetMode(HeightMeasureSpec);

        var MaxedRight = PinArray.Max(R => R.Right);
        var MaxedBottom = PinArray.Max(R => R.Bottom);

        var IdealWidth = Math.Max(SuggestedMinimumWidth, PaddingLeft + MaxedRight + PaddingRight);
        var IdealHeight = Math.Max(SuggestedMinimumHeight, PaddingTop + MaxedBottom + PaddingBottom);

        switch (WidthMode)
        {
          case Android.Views.MeasureSpecMode.AtMost:
            IdealWidth = Math.Min(IdealWidth, MeasuredWidth);
            break;

          case Android.Views.MeasureSpecMode.Exactly:
            IdealWidth = MeasuredWidth;
            break;

          case Android.Views.MeasureSpecMode.Unspecified:
            break;

          default:
            throw new UnhandledEnumCaseException<Android.Views.MeasureSpecMode>();
        }

        switch (HeightMode)
        {
          case Android.Views.MeasureSpecMode.AtMost:
            IdealHeight = Math.Min(IdealHeight, MeasuredHeight);
            break;

          case Android.Views.MeasureSpecMode.Exactly:
            IdealHeight = MeasuredHeight;
            break;

          case Android.Views.MeasureSpecMode.Unspecified:
            break;

          default:
            throw new UnhandledEnumCaseException<Android.Views.MeasureSpecMode>();
        }

        SetMeasuredDimension(IdealWidth, IdealHeight);
      }

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      foreach (var Pin in PinArray)
        Pin.Element.LayoutChild(PaddingLeft + Pin.Left, PaddingTop + Pin.Top, Pin.Right - PaddingRight, Pin.Bottom - PaddingBottom);

      SW.StopLayout();
    }

    private AndroidPin[] PinArray;
    private AndroidContainer[] ElementArray;
    private readonly AndroidStopwatch SW = AndroidInstrumentation.BoardPerformance.NewStopwatch();
  }

  public struct AndroidPin
  {
    public AndroidPin(AndroidContainer Element, int Left, int Top, int Right, int Bottom)
    {
      this.Element = Element;
      this.Left = Left;
      this.Top = Top;
      this.Right = Right;
      this.Bottom = Bottom;
    }

    public readonly AndroidContainer Element;
    public readonly int Left;
    public readonly int Top;
    public readonly int Right;
    public readonly int Bottom;
  }

  /* // TODO: attempt to solve the corner radius clipping.
    internal sealed class AndroidGraphic : AndroidGroup
    {
      public AndroidGraphic(Android.Content.Context context)
        : base(context)
      {
        this.SetClipChildren(true); // TODO: necessary to clip the inner imageview?

        this.ImageView = new Android.Widget.ImageView(context);
        AddView(ImageView);
        // NOTE: both of these seem to work together to emulate the WPF 'uniform' scaling.
        ImageView.SetAdjustViewBounds(true);
        ImageView.SetScaleType(Android.Widget.ImageView.ScaleType.FitCenter);
      }

      public void SetImageBitmap(Bitmap Bitmap)
      {
        ImageView.SetImageBitmap(Bitmap);
      }

      protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
      {
        base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

        int RequestedWidth, RequestedHeight;
        this.MeasureOnlyChild(ImageView, WidthMeasureSpec, HeightMeasureSpec, out RequestedWidth, out RequestedHeight);

        SetMeasuredDimension(RequestedWidth, RequestedHeight);
      }
      protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
      {
        this.LayoutOnlyChild(ImageView, Left, Top, Right, Bottom);
      }

      private Android.Widget.ImageView ImageView;
    }
  */

  public sealed class AndroidGraphic : Android.Widget.ImageView
  {
    internal AndroidGraphic(Android.Content.Context context)
      : base(context)
    {
      this.Background = new AndroidBackground(this);

      // NOTE: both of these seem to work together to emulate the WPF 'uniform' scaling.
      SetAdjustViewBounds(true);
      SetScaleType(Android.Widget.ImageView.ScaleType.FitCenter);

      // TODO: actually... Android is the only platform that will 'center' an image based on the pixel width/height.
    }

    public new AndroidBackground Background { get; }
    public override void SetImageBitmap(Android.Graphics.Bitmap bm)
    {
      base.SetImageBitmap(bm);

      this.Image = bm;
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      //var WidthMode = MeasureSpec.GetMode(WidthMeasureSpec);
      //var HeightMode = MeasureSpec.GetMode(HeightMeasureSpec);
      var WidthSize = MeasureSpec.GetSize(WidthMeasureSpec);
      var HeightSize = MeasureSpec.GetSize(HeightMeasureSpec);

      if (Image != null && (WidthSize > 0 || HeightSize > 0))
      {
        // NOTE: this scales the image to fit the available max size and maintains aspect ratio.
        //       this may not be expected when simply centering and image in the screen but is consistent with Wpf/Uwp.

        float ImageAspect;
        try
        {
          // reading the width/height can throw an exception.
          if (Image.Width > 0 && Image.Height > 0)
            ImageAspect = Image.Width / Image.Height;
          else
            return;
        }
        catch (System.ObjectDisposedException)
        {
          // MemoryReclamation causes the bitmap to be disposed, but it still renders okay.
          return;
          // TODO: does handling this exception, repeatedly, impact on performance?
        }

        if (ImageAspect > 0)
        {
          if (WidthSize > 0 && HeightSize > 0)
          {
            if (ImageAspect > (float)WidthSize / (float)HeightSize)
              SetMeasuredDimension(WidthSize, (int)Math.Ceiling(WidthSize / ImageAspect));
            else
              SetMeasuredDimension((int)Math.Ceiling(HeightSize * ImageAspect), HeightSize);
          }
          else
          {
            if (WidthSize > 0)
              SetMeasuredDimension(WidthSize, (int)Math.Ceiling(WidthSize / ImageAspect));
            else if (HeightSize > 0)
              SetMeasuredDimension((int)Math.Ceiling(HeightSize * ImageAspect), HeightSize);
          }
        }
      }

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      base.OnLayout(Changed, Left, Top, Right, Bottom);

      SW.StopLayout();
    }
    protected override void OnDraw(Android.Graphics.Canvas canvas)
    {
      Background.Draw(canvas);

      base.OnDraw(canvas);
    }

    private Android.Graphics.Bitmap Image;
    private readonly AndroidStopwatch SW = AndroidInstrumentation.GraphicPerformance.NewStopwatch();
  }

  public sealed class AndroidOverlay : AndroidGroup
  {
    internal AndroidOverlay(Android.Content.Context context)
      : base(context)
    {
      this.ElementArray = new AndroidContainer[] { };
    }

    public void ComposeElements(IEnumerable<AndroidContainer> Elements)
    {
      var PreviousArray = ElementArray;
      this.ElementArray = Elements.ToArray();

      foreach (var Element in ElementArray.Except(PreviousArray))
        this.SafeAddView(Element);

      foreach (var Element in PreviousArray.Except(ElementArray))
        this.SafeRemoveView(Element);

      this.Arrange();
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      var RequestedWidth = 0;
      var RequestedHeight = 0;

      if (ElementArray.Length > 0)
      {
        var FinalWidth = RequestedWidth;
        var FinalHeight = RequestedHeight;

        foreach (var Element in ElementArray)
        {
          this.MeasureOnlyChild(Element, WidthMeasureSpec, HeightMeasureSpec, out RequestedWidth, out RequestedHeight);

          if (RequestedWidth > FinalWidth)
            FinalWidth = RequestedWidth;

          if (RequestedHeight > FinalHeight)
            FinalHeight = RequestedHeight;
        }

#if MINIMUM_MEASURE
        if (FinalWidth < MinimumWidth)
          FinalWidth = MinimumWidth;

        if (FinalHeight < MinimumHeight)
          FinalHeight = MinimumHeight;
#endif

        // now we know the max width/height, we need to remeasure everyone.
        var RemeasureWidthSpec = MeasureSpec.MakeMeasureSpec(FinalWidth, Android.Views.MeasureSpecMode.Exactly);
        var RemeasureHeightSpec = MeasureSpec.MakeMeasureSpec(FinalHeight, Android.Views.MeasureSpecMode.Exactly);
        foreach (var Element in ElementArray)
        {
          if (Element.MeasuredWidth != FinalWidth || Element.MeasuredHeight != FinalHeight)
            this.MeasureOnlyChild(Element, RemeasureWidthSpec, RemeasureHeightSpec, out RequestedWidth, out RequestedHeight);
        }

        RequestedWidth = FinalWidth;
        RequestedHeight = FinalHeight;
      }
      else
      {
        this.MeasureOnlyChild(null, WidthMeasureSpec, HeightMeasureSpec, out RequestedWidth, out RequestedHeight);
      }

      SetMeasuredDimension(RequestedWidth, RequestedHeight);

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      foreach (var Element in ElementArray)
        this.LayoutOnlyChild(Element, Left, Top, Right, Bottom);

      SW.StopLayout();
    }

    private AndroidContainer[] ElementArray;
    private readonly AndroidStopwatch SW = AndroidInstrumentation.OverlayPerformance.NewStopwatch();
  }

  public sealed class AndroidFrame : AndroidGroup
  {
    internal AndroidFrame(Android.Content.Context context)
      : base(context)
    {
    }

    public AndroidContainer ContentElement { get; private set; }

    public void SetContentElement(AndroidContainer Element)
    {
      this.ContentElement = Element;

      // NOTE: SafeRemoveView/SafeAddView is called externally (so animations are possible).

      this.Arrange();
    }

    internal bool IsTransitioning
    {
      get => IsTransitionField;
      set
      {
        if (IsTransitionField != value)
        {
          this.IsTransitionField = value;

          if (AndroidFoundation.SdkVersion >= 21)
            this.ClipToOutline = value; // clipping is required while transitioning (carousel animation can exit the bounds of the frame).
        }
      }
    }

    private bool IsTransitionField;

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      System.Diagnostics.Debug.Assert(ContentElement == null || IndexOfChild(ContentElement) >= 0, "ContentElement must be a subview.");

      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      this.MeasureOnlyChild(ContentElement, WidthMeasureSpec, HeightMeasureSpec, out var RequestedWidth, out var RequestedHeight);

      SetMeasuredDimension(RequestedWidth, RequestedHeight);

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      this.LayoutOnlyChild(ContentElement, Left, Top, Right, Bottom);

      SW.StopLayout();
    }

    private readonly AndroidStopwatch SW = AndroidInstrumentation.FramePerformance.NewStopwatch();
  }

  public sealed class AndroidNative : AndroidGroup
  {
    internal AndroidNative(Android.Content.Context context)
      : base(context)
    {
    }

    public Android.Views.View ContentElement { get; private set; }

    public void SetContentElement(Android.Views.View Element)
    {
      this.ContentElement = Element;
      this.Arrange();
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      this.MeasureOnlyChild(ContentElement, WidthMeasureSpec, HeightMeasureSpec, out var RequestedWidth, out var RequestedHeight);

      SetMeasuredDimension(RequestedWidth, RequestedHeight);

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      this.LayoutOnlyChild(ContentElement, Left, Top, Right, Bottom);

      SW.StopLayout();
    }

    private readonly AndroidStopwatch SW = AndroidInstrumentation.NativePerformance.NewStopwatch();
  }

  public sealed class AndroidFlow : AndroidGroup
  {
    internal AndroidFlow(Android.Content.Context Context, AndroidFlowConfiguration Configuration)
      : base(Context)
    {
      this.SetClipChildren(true);

      this.Adapter = new FlowAdapter(Configuration);

      this.ListView = new Android.Widget.ListView(Context);
      ListView.Adapter = Adapter;
      ListView.LayoutParameters = new LayoutParams(LayoutParams.MatchParent, LayoutParams.MatchParent);
      ListView.Divider = null;
      ListView.DividerHeight = 0;
      ListView.SetSelector(Android.Resource.Color.Transparent);
      ListView.Recycler += (Sender, Event) =>
      {
        var Result = Event.View as FlowItemContainerView;

        if (Result != null && Result.SectionIndex >= 0 && Result.ItemIndex >= 0 && Result.ContentItem.Container != null)
        {
          //System.Diagnostics.Debug.WriteLine($"RECYCLED: {Result.SectionIndex} - {Result.ItemIndex}");

          Configuration.RecycleEvent(Result.SectionIndex, Result.ItemIndex, Result.ContentItem);
        }
      };
      AddView(ListView);

      this.Arrange();
    }

    public bool IsRefreshable
    {
      get
      {
        return RefreshLayout != null;
      }
      set
      {
        if (value != IsRefreshable)
        {
          if (value)
          {
            RemoveView(ListView);

            this.RefreshLayout = new Android.Support.V4.Widget.SwipeRefreshLayout(Context);
            RefreshLayout.LayoutParameters = new LayoutParams(LayoutParams.MatchParent, LayoutParams.MatchParent);
            RefreshLayout.Refresh += (Sender, Event) => RefreshInvoke();
            RefreshLayout.AddView(ListView);
            AddView(RefreshLayout);
          }
          else
          {
            RefreshLayout.RemoveView(ListView);
            RemoveView(RefreshLayout);
            RefreshLayout = null;

            AddView(ListView);
          }

          if (!IsInLayout)
            RequestLayout();
        }
      }
    }
    public event Action RefreshEvent;

    public void NewRefresh()
    {
      if (RefreshLayout != null)
      {
        RefreshLayout.Refreshing = true;
        RefreshInvoke();
      }
    }
    public void CompleteRefresh()
    {
      if (RefreshLayout != null)
        RefreshLayout.Refreshing = false;
    }
    public void Reload()
    {
      Adapter.NotifyDataSetChanged();
    }
    public void ScrollToItem(int Section, int Index)
    {
      var Position = Adapter.GetAbsoluteIndex(Section, Index);
      ListView.SetSelection(Position);
      //ListView.SmoothScrollToPosition(Position); // NOTE: this is an animated version, similar to SetSelection.
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      var RequestedWidth = MeasuredWidth;
      var RequestedHeight = MeasuredHeight;

#if MINIMUM_MEASURE
      if (RequestedWidth < MinimumWidth)
        RequestedWidth = MinimumWidth;

      if (RequestedHeight < MinimumHeight)
        RequestedHeight = MinimumHeight;
#endif

      ChildView.Measure(MeasureSpec.MakeMeasureSpec(RequestedWidth, Android.Views.MeasureSpecMode.Exactly), MeasureSpec.MakeMeasureSpec(RequestedHeight, Android.Views.MeasureSpecMode.Exactly));

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      ChildView.LayoutChild(0, 0, Right - Left, Bottom - Top);

      SW.StopLayout();
    }
    protected override void OnDetachedFromWindow()
    {
      SelectionIndex = ListView.FirstVisiblePosition;
      var FirstView = ListView.GetChildAt(0);
      SelectionY = FirstView == null ? 0 : FirstView.Top - ListView.PaddingTop;

      base.OnDetachedFromWindow();
    }
    protected override void OnAttachedToWindow()
    {
      base.OnAttachedToWindow();

      ListView.SetSelectionFromTop(SelectionIndex, SelectionY);
    }

    private void RefreshInvoke()
    {
      if (RefreshEvent != null)
        RefreshEvent();
    }

    private Android.Support.V4.Widget.SwipeRefreshLayout RefreshLayout;
    private readonly Android.Widget.ListView ListView;
    private readonly FlowAdapter Adapter;
    private int SelectionIndex;
    private int SelectionY;
    private Android.Views.View ChildView
    {
      get
      {
        if (RefreshLayout == null)
          return ListView;
        else
          return RefreshLayout;
      }
    }
    private readonly AndroidStopwatch SW = AndroidInstrumentation.FlowPerformance.NewStopwatch();

    private sealed class FlowItemContainerView : Android.Views.ViewGroup
    {
      public FlowItemContainerView(Android.Content.Context Context)
        : base(Context)
      {
        this.SetClipChildren(false);
      }

      public int SectionIndex { get; private set; }
      public int ItemIndex { get; private set; }
      public AndroidFlowItem ContentItem { get; private set; }

      public void SetContent(int SectionIndex, int ItemIndex, AndroidFlowItem ContentItem)
      {
        this.SectionIndex = SectionIndex;
        this.ItemIndex = ItemIndex;

        var OldContentElement = this.ContentItem.Container;
        var NewContentElement = ContentItem.Container;

        if (OldContentElement != NewContentElement)
        {
          if (OldContentElement != null)
            RemoveAllViews();

          this.ContentItem = ContentItem;

          if (NewContentElement != null)
          {
            // Since header and footer views aren't recreated, they need to be detached from their parents
            if (NewContentElement.Parent is Android.Views.ViewGroup)
              (NewContentElement.Parent as Android.Views.ViewGroup).RemoveView(NewContentElement);
            AddView(NewContentElement);
          }

          RequestLayout();
        }
      }

      protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
      {
        base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

        this.MeasureOnlyChild(ContentItem.Container, WidthMeasureSpec, HeightMeasureSpec, out var RequestedWidth, out var RequestedHeight);

        SetMeasuredDimension(RequestedWidth, RequestedHeight);
      }
      protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
      {
        this.LayoutOnlyChild(ContentItem.Container, Left, Top, Right, Bottom);
      }
    }

    private sealed class FlowAdapter : Android.Widget.BaseAdapter
    {
      public FlowAdapter(AndroidFlowConfiguration Configuration)
        : base()
      {
        this.Configuration = Configuration;
      }

      public override int Count
      {
        get { return Enumerable.Range(0, Configuration.SectionCountQuery()).Sum(Section => (Configuration.HeaderContentQuery(Section).Container == null ? 0 : 1) + Configuration.ItemCountQuery(Section) + (Configuration.FooterContentQuery(Section).Container == null ? 0 : 1)); }
      }

      public override Java.Lang.Object GetItem(int Position)
      {
        return null;
      }
      public override long GetItemId(int Position)
      {
        return Position;
      }
      public override Android.Views.View GetView(int Position, Android.Views.View ConvertView, Android.Views.ViewGroup Parent)
      {
        // NOTE: Ignoring the ConvertView has been observed to cause issues with flow item buttons not working, so it is used to hold a simple container view.
        var RecycledContainer = ConvertView as FlowItemContainerView;
        if (RecycledContainer == null)
          RecycledContainer = new FlowItemContainerView(Parent.Context);

        var AbsoluteIndex = 0;
        var SectionIndex = 0;

        while (AbsoluteIndex <= Position)
        {
          var SectionHeader = Configuration.HeaderContentQuery(SectionIndex);
          if (SectionHeader.Container != null)
          {
            if (AbsoluteIndex == Position)
            {
              RecycledContainer.SetContent(SectionIndex, -1, SectionHeader);
              return RecycledContainer;
            }

            AbsoluteIndex++;
          }

          var SectionItemCount = Configuration.ItemCountQuery(SectionIndex);
          if (Position <= AbsoluteIndex + SectionItemCount - 1)
          {
            var ItemIndex = Position - AbsoluteIndex;
            RecycledContainer.SetContent(SectionIndex, ItemIndex, Configuration.ItemContentQuery(SectionIndex, ItemIndex));
            return RecycledContainer;
          }

          AbsoluteIndex += SectionItemCount;

          var SectionFooter = Configuration.FooterContentQuery(SectionIndex);
          if (SectionFooter.Container != null)
          {
            if (AbsoluteIndex == Position)
            {
              RecycledContainer.SetContent(SectionIndex, -1, SectionFooter);
              return RecycledContainer;
            }

            AbsoluteIndex++;
          }

          SectionIndex++;
        }

        return null;
      }

      public int GetAbsoluteIndex(int Section, int Index)
      {
        var Position = 0;

        for (var SectionIndex = 0; SectionIndex <= Section; SectionIndex++)
        {
          if (Configuration.HeaderContentQuery(SectionIndex).Container != null)
            Position++;

          if (SectionIndex == Section)
          {
            Position += Index;
            break;
          }

          Position += Configuration.ItemCountQuery(SectionIndex);

          if (Configuration.FooterContentQuery(SectionIndex).Container != null)
            Position++;
        }

        return Position;
      }

      private readonly AndroidFlowConfiguration Configuration;
    }
  }

  internal sealed class AndroidFlowConfiguration
  {
    public Func<int> SectionCountQuery;
    public Func<int, int> ItemCountQuery;
    public Func<int, int, AndroidFlowItem> ItemContentQuery;
    public Func<int, AndroidFlowItem> HeaderContentQuery;
    public Func<int, AndroidFlowItem> FooterContentQuery;
    public Action<int, int, AndroidFlowItem> RecycleEvent;
  }

  internal struct AndroidFlowItem
  {
    public Inv.Panel Panel;
    public AndroidContainer Container;
  }

  public sealed class AndroidScroll : AndroidGroup
  {
    internal AndroidScroll(Android.Content.Context context)
      : base(context)
    {
      this.SetClipChildren(true); // necessary to clip the inner scroll views properly.
    }

    public void SetOrientation(bool IsHorizontal)
    {
      if (IsHorizontal)
      {
        if (ScrollView == null || ScrollView != HorizontalScroll)
        {
          var AssignContentElement = VerticalScroll?.ContentElement;
          if (AssignContentElement != null)
            VerticalScroll.SetContentElement(null);

          if (HorizontalScroll == null)
            this.HorizontalScroll = new AndroidHorizontalScroll(Context);

          if (AssignContentElement != null)
            HorizontalScroll.SetContentElement(AssignContentElement);

          RemoveAllViews();
          this.ScrollView = HorizontalScroll;
          AddView(ScrollView);
        }
      }
      else
      {
        if (ScrollView == null || ScrollView != VerticalScroll)
        {
          var AssignContentElement = HorizontalScroll?.ContentElement;
          if (AssignContentElement != null)
            HorizontalScroll.SetContentElement(null);

          if (VerticalScroll == null)
            this.VerticalScroll = new AndroidVerticalScroll(Context);

          if (AssignContentElement != null)
            VerticalScroll.SetContentElement(AssignContentElement);

          RemoveAllViews();
          this.ScrollView = VerticalScroll;
          AddView(ScrollView);
        }
      }
    }
    public void SetContentElement(AndroidContainer Element)
    {
      if (VerticalScroll != null)
        VerticalScroll.SetContentElement(Element);
      else
        HorizontalScroll.SetContentElement(Element);
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      this.MeasureOnlyChild(ScrollView, WidthMeasureSpec, HeightMeasureSpec, out var RequestedWidth, out var RequestedHeight);

      SetMeasuredDimension(RequestedWidth, RequestedHeight);

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      this.LayoutOnlyChild(ScrollView, Left, Top, Right, Bottom);

      SW.StopLayout();
    }

    private Android.Views.View ScrollView;
    private AndroidVerticalScroll VerticalScroll;
    private AndroidHorizontalScroll HorizontalScroll;
    private readonly AndroidStopwatch SW = AndroidInstrumentation.ScrollPerformance.NewStopwatch();
  }

  internal sealed class AndroidHorizontalScroll : Android.Widget.HorizontalScrollView
  {
    public AndroidHorizontalScroll(Android.Content.Context context)
      : base(context)
    {
      this.FillViewport = true;
    }

    public Android.Views.View ContentElement { get; private set; }

    public void SetContentElement(Android.Views.View Element)
    {
      if (this.ContentElement != Element)
      {
        if (this.ContentElement != null)
          this.SafeRemoveView(this.ContentElement);

        this.ContentElement = Element;

        if (this.ContentElement != null)
          this.SafeAddView(this.ContentElement);

        this.Arrange();
      }
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      this.LayoutOnlyChild(ContentElement, Left, Top, Right, Bottom);

      // HACK: this forces the scrollview to make sure the scrolled content is actually in the viewport.
      ScrollTo(ScrollX, ScrollY);
    }
  }

  internal sealed class AndroidVerticalScroll : Android.Widget.ScrollView
  {
    public AndroidVerticalScroll(Android.Content.Context context)
      : base(context)
    {
      this.FillViewport = true;
    }

    public Android.Views.View ContentElement { get; private set; }

    public void SetContentElement(Android.Views.View Element)
    {
      if (this.ContentElement != Element)
      {
        if (this.ContentElement != null)
          this.SafeRemoveView(this.ContentElement);

        this.ContentElement = Element;

        if (this.ContentElement != null)
          this.SafeAddView(this.ContentElement);

        this.Arrange();
      }
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      this.LayoutOnlyChild(ContentElement, Left, Top, Right, Bottom);

      // HACK: this forces the scrollview to make sure the scrolled content is actually in the viewport.
      ScrollTo(ScrollX, ScrollY);
    }
  }

  public sealed class AndroidBrowser : AndroidGroup
  {
    internal AndroidBrowser(Android.Content.Context context)
      : base(context)
    {
      this.SetClipChildren(true); // TODO: is this necessary?

      // NOTE: we need to embed the WebView in a parent view because it is no longer possible to custom draw the background from Android OS 9.x.
      this.WebView = new AndroidWebView(context);
      AddView(WebView);
    }

    public event Func<string, bool> BlockQuery
    {
      add => WebView.BlockQuery += value;
      remove => WebView.BlockQuery -= value;
    }
    public event Action<string> ReadyEvent
    {
      add => WebView.ReadyEvent += value;
      remove => WebView.ReadyEvent -= value;
    }

    public void Navigate(Uri Uri, string Html) => WebView.Navigate(Uri, Html);

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      this.MeasureOnlyChild(WebView, WidthMeasureSpec, HeightMeasureSpec, out var RequestedWidth, out var RequestedHeight);

      SetMeasuredDimension(RequestedWidth, RequestedHeight);

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      this.LayoutOnlyChild(WebView, Left, Top, Right, Bottom);

      SW.StopLayout();
    }

    private readonly AndroidWebView WebView;
    private readonly AndroidStopwatch SW = AndroidInstrumentation.BrowserPerformance.NewStopwatch();
  }

  public sealed class AndroidWebView : Android.Webkit.WebView
  {
    internal AndroidWebView(Android.Content.Context context)
      : base(context)
    {
      //this.SetClipToPadding(false);
      //this.SetClipChildren(false);

      this.Background = new AndroidBackground(this);

      Settings.JavaScriptEnabled = true;
      //Settings.DomStorageEnabled = true;

      SetWebViewClient(new SSLTolerentWebViewClient(this)); // https://www.google.com/ requires this for some reason.

      SetBackgroundColor(Android.Graphics.Color.Transparent); // initially makes webview transparent, and in the start of the activity you can see the background colour, but after webpage starts to load it disappears, and color of webpage takes place.
      //SetLayerType(Android.Views.LayerType.Software, null); // doesn't seem to help.
    }

    public new AndroidBackground Background { get; }
    public event Func<string, bool> BlockQuery;
    public event Action<string> ReadyEvent;

    public void Navigate(Uri Uri, string Html)
    {
      if (Html != null)
        LoadData(Html, "text/html; charset=utf-8", "UTF-8");
      else if (Uri != null)
        LoadUrl(Uri.AbsoluteUri);
      else
        LoadUrl("about:blank");
    }

    private bool Block(string Url)
    {
      return BlockQuery != null && BlockQuery(Url);
    }
    private void Ready(string Url)
    {
      if (ReadyEvent != null)
        ReadyEvent(Url);
    }

    private class SSLTolerentWebViewClient : Android.Webkit.WebViewClient
    {
      public SSLTolerentWebViewClient(AndroidWebView AndroidWebView)
      {
        this.AndroidWebView = AndroidWebView;
      }

      [Obsolete]
      public override bool ShouldOverrideUrlLoading(Android.Webkit.WebView view, string url)
      {
        // NOTE: only this obsolete ShouldOverrideUrlLoading method actually gets called (the below one doesn't).
        if (AndroidWebView.Block(url))
          return true;
        else
          return base.ShouldOverrideUrlLoading(view, url);
      }
      public override bool ShouldOverrideUrlLoading(Android.Webkit.WebView view, Android.Webkit.IWebResourceRequest request)
      {
        // NOTE: only the above, obsolete ShouldOverrideUrlLoading method actually gets called.
        if (AndroidWebView.Block(request.Url.ToString()))
          return true;
        else
          return base.ShouldOverrideUrlLoading(view, request);
      }
      public override void OnPageFinished(WebView view, string url)
      {
        AndroidWebView.Ready(url);

        base.OnPageFinished(view, url);
      }

      public override void OnReceivedSslError(Android.Webkit.WebView view, Android.Webkit.SslErrorHandler handler, Android.Net.Http.SslError error)
      {
        base.OnReceivedSslError(view, handler, error);
        //handler.Proceed();
      }
      //public override void OnPageFinished(Android.Webkit.WebView view, string url)
      //{
      //  base.OnPageFinished(view, url);
      //  view.SetBackgroundColor(Color.Transparent);
      //  view.SetLayerType(Android.Views.LayerType.Software, null);
      //}

      private readonly AndroidWebView AndroidWebView;
    }
  }

  public sealed class AndroidVideo : AndroidGroup
  {
    internal AndroidVideo(Android.Content.Context context)
      : base(context)
    {
      SetClipChildren(true);

      this.VideoSurface = new AndroidVideoSurface(context);
      AddView(VideoSurface);
    }

    public void LoadAsset(Android.Content.Res.AssetFileDescriptor Descriptor)
    {
      VideoSurface.LoadAsset(Descriptor);
    }
    public void LoadUri(Uri Uri)
    {
      VideoSurface.LoadUri(Uri);
    }
    public void Unload()
    {
      VideoSurface.Unload();
    }
    public void Start()
    {
      VideoSurface.Start();
    }
    public void Pause()
    {
      VideoSurface.Pause();
    }
    public void Stop()
    {
      VideoSurface.Stop();
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      this.MeasureOnlyChild(VideoSurface, WidthMeasureSpec, HeightMeasureSpec, out var RequestedWidth, out var RequestedHeight);

      SetMeasuredDimension(RequestedWidth, RequestedHeight);

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      var MiddleX = (Right - Left - VideoSurface.MeasuredWidth - PaddingLeft - PaddingRight) / 2;
      var MiddleY = (Bottom - Top - VideoSurface.MeasuredHeight - PaddingTop - PaddingBottom) / 2;

      VideoSurface.LayoutChild(PaddingLeft + MiddleX, PaddingTop + MiddleY, PaddingLeft + MiddleX + VideoSurface.MeasuredWidth, PaddingTop + MiddleY + VideoSurface.MeasuredHeight);

      //this.LayoutOnlyChild(Surface, Left, Top, Right, Bottom);

      SW.StopLayout();
    }

    private readonly AndroidVideoSurface VideoSurface;
    private readonly AndroidStopwatch SW = AndroidInstrumentation.VideoPerformance.NewStopwatch();
  }

  public sealed class AndroidVideoSurface : Android.Views.SurfaceView, Android.Views.ISurfaceHolderCallback, Android.Media.MediaPlayer.IOnPreparedListener, Android.Media.MediaPlayer.IOnVideoSizeChangedListener, Android.Media.MediaPlayer.IOnCompletionListener, Android.Media.MediaPlayer.IOnErrorListener
  {
    internal AndroidVideoSurface(Android.Content.Context context)
      : base(context)
    {
      this.VideoWidth = 0;
      this.VideoHeight = 0;

      /*
      this.mAudioManager = (AudioManager)Context.GetSystemService(Context.AUDIO_SERVICE);
      this.mAudioAttributes = new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_MEDIA).setContentType(AudioAttributes.CONTENT_TYPE_MOVIE).build();
      */
      Holder.AddCallback(this);
      //Holder.SetType(SurfaceType.PushBuffers);
      /*
      Focusable = true;
      FocusableInTouchMode = true;
      RequestFocus();
      */

      this.CurrentState = AndroidVideoState.IDLE;
      this.TargetState = AndroidVideoState.IDLE;

      //Visibility = ViewStates.Invisible;
      Alpha = 0.0F;
    }

    public void LoadAsset(Android.Content.Res.AssetFileDescriptor Descriptor)
    {
      this.Descriptor = Descriptor;
      this.Uri = null;
      Acquire();
      RequestLayout();
      Invalidate();
    }
    public void LoadUri(Uri Uri)
    {
      this.Descriptor = null;
      this.Uri = Uri;
      Acquire();
      RequestLayout();
      Invalidate();
    }
    public void Unload()
    {
      this.Descriptor = null;
      this.Uri = null;
      Release(true);
    }
    public void Start()
    {
      if (IsInPlaybackState())
      {
        MediaPlayer.Start();
        this.CurrentState = AndroidVideoState.PLAYING;
      }
      else
      {
        Acquire();
        this.TargetState = AndroidVideoState.PLAYING;
      }

      // show the surface view (can't use Visibility as this messes with SurfaceCreated).
      Alpha = 1.0F;
    }
    public void Pause()
    {
      if (IsInPlaybackState())
      {
        if (MediaPlayer.IsPlaying)
        {
          MediaPlayer.Pause();
          this.CurrentState = AndroidVideoState.PAUSED;
        }
      }

      this.TargetState = AndroidVideoState.PAUSED;
    }
    public void Stop()
    {
      // hide clear the last displayed frame.
      Alpha = 0.0F;

      if (MediaPlayer != null)
      {
        MediaPlayer.Stop();
        MediaPlayer.Release();
        this.MediaPlayer = null;
        this.CurrentState = AndroidVideoState.IDLE;
        this.TargetState = AndroidVideoState.IDLE;
        /*
        mAudioManager.abandonAudioFocus(null);
        */
      }
    }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      var RequestedWidth = GetDefaultSize(VideoWidth, WidthMeasureSpec);
      var RequestedHeight = GetDefaultSize(VideoHeight, HeightMeasureSpec);

      if (VideoWidth > 0 && VideoHeight > 0)
      {
        var WidthMode = MeasureSpec.GetMode(WidthMeasureSpec);
        var WidthSize = MeasureSpec.GetSize(WidthMeasureSpec);
        var HeightMode = MeasureSpec.GetMode(HeightMeasureSpec);
        var HeightSize = MeasureSpec.GetSize(HeightMeasureSpec);

        if (WidthMode == MeasureSpecMode.Exactly && HeightMode == MeasureSpecMode.Exactly)
        {
          // the size is fixed
          RequestedWidth = WidthSize;
          RequestedHeight = HeightSize;
          // for compatibility, we adjust size based on aspect ratio
          if (VideoWidth * RequestedHeight < RequestedWidth * VideoHeight)
          {
            //Log.i("@@@", "image too wide, correcting");
            RequestedWidth = RequestedHeight * VideoWidth / VideoHeight;
          }
          else if (VideoWidth * RequestedHeight > RequestedWidth * VideoHeight)
          {
            //Log.i("@@@", "image too tall, correcting");
            RequestedHeight = RequestedWidth * VideoHeight / VideoWidth;
          }
        }
        else if (WidthMode == MeasureSpecMode.Exactly)
        {
          // only the width is fixed, adjust the height to match aspect ratio if possible
          RequestedWidth = WidthSize;
          RequestedHeight = RequestedWidth * VideoHeight / VideoWidth;
          if (HeightMode == MeasureSpecMode.AtMost && RequestedHeight > HeightSize)
            RequestedHeight = HeightSize; // couldn't match aspect ratio within the constraints
        }
        else if (HeightMode == MeasureSpecMode.Exactly)
        {
          // only the height is fixed, adjust the width to match aspect ratio if possible
          RequestedHeight = HeightSize;
          RequestedWidth = RequestedHeight * VideoWidth / VideoHeight;
          if (WidthMode == MeasureSpecMode.AtMost && RequestedWidth > WidthSize)
            RequestedWidth = WidthSize; // couldn't match aspect ratio within the constraints
        }
        else
        {
          // neither the width nor the height are fixed, try to use actual video size
          RequestedWidth = VideoWidth;
          RequestedHeight = VideoHeight;

          if (HeightMode == MeasureSpecMode.AtMost && RequestedHeight > HeightSize)
          {
            // too tall, decrease both width and height
            RequestedHeight = HeightSize;
            RequestedWidth = RequestedHeight * VideoWidth / VideoHeight;
          }

          if (WidthMode == MeasureSpecMode.AtMost && RequestedWidth > WidthSize)
          {
            // too wide, decrease both width and height
            RequestedWidth = WidthSize;
            RequestedHeight = RequestedWidth * VideoHeight / VideoWidth;
          }
        }
      }
      else
      {
        // no size yet, just adopt the given spec sizes
      }

#if MINIMUM_MEASURE
      if (RequestedWidth < MinimumWidth)
        RequestedWidth = MinimumWidth;

      if (RequestedHeight < MinimumHeight)
        RequestedHeight = MinimumHeight;
#endif

      SetMeasuredDimension(RequestedWidth, RequestedHeight);
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      base.OnLayout(Changed, Left, Top, Right, Bottom);
    }

    private void Acquire()
    {
      if ((Descriptor == null && Uri == null) || SurfaceHolder == null)
        return;

      Release(false);

      /*
      if (mAudioFocusType != AudioManager.AUDIOFOCUS_NONE)
        mAudioManager.requestAudioFocus(null, mAudioAttributes, mAudioFocusType, 0);
      */

      try
      {
        this.MediaPlayer = new Android.Media.MediaPlayer();
        /*
        var controller = new SubtitleController(Context, mMediaPlayer.getMediaTimeProvider(), mMediaPlayer);
        controller.registerRenderer(new WebVttRenderer(Context));
        controller.registerRenderer(new TtmlRenderer(Context));
        controller.registerRenderer(new Cea708CaptionRenderer(Context));
        controller.registerRenderer(new ClosedCaptionRenderer(Context));
        mMediaPlayer.setSubtitleAnchor(controller, this);
        */
        /*
        if (mAudioSession != 0)
          mMediaPlayer.setAudioSessionId(mAudioSession);
        else
          mAudioSession = mMediaPlayer.getAudioSessionId();
        */

        MediaPlayer.SetOnPreparedListener(this);
        MediaPlayer.SetOnVideoSizeChangedListener(this);
        MediaPlayer.SetOnCompletionListener(this);
        MediaPlayer.SetOnErrorListener(this);
        /*
        mMediaPlayer.SetOnInfoListener(this);
        mMediaPlayer.SetOnBufferingUpdateListener(this);
        this.mCurrentBufferPercentage = 0;
        */
        if (Descriptor != null)
          MediaPlayer.SetDataSource(Descriptor.FileDescriptor, Descriptor.StartOffset, Descriptor.Length);
        else if (Uri != null)
          MediaPlayer.SetDataSource(Context, Android.Net.Uri.Parse(Uri.AbsoluteUri));
        MediaPlayer.SetDisplay(SurfaceHolder);
        /*
        mMediaPlayer.SetAudioAttributes(mAudioAttributes);
        */
        MediaPlayer.SetScreenOnWhilePlaying(true);
        MediaPlayer.PrepareAsync();

        /*
        for (Pair<InputStream, MediaFormat> pending: mPendingSubtitleTracks)
        {
          try
          {
            mMediaPlayer.addSubtitleSource(pending.first, pending.second);
          }
          catch (IllegalStateException e)
          {
            mInfoListener.onInfo(mMediaPlayer, MediaPlayer.MEDIA_INFO_UNSUPPORTED_SUBTITLE, 0);
          }
        }
        */

        this.CurrentState = AndroidVideoState.PREPARING;
      }
      catch
      {
        this.CurrentState = AndroidVideoState.ERROR;
        this.TargetState = AndroidVideoState.ERROR;
      }
    }
    private void Release(bool cleartargetstate)
    {
      if (MediaPlayer != null)
      {
        MediaPlayer.Reset();
        MediaPlayer.Release();
        this.MediaPlayer = null;

        CurrentState = AndroidVideoState.IDLE;
        if (cleartargetstate)
          TargetState = AndroidVideoState.IDLE;

        /*
        if (mAudioFocusType != AudioManager.AUDIOFOCUS_NONE)
          mAudioManager.abandonAudioFocus(null);
        */
      }
    }
    private bool IsInPlaybackState()
    {
      return MediaPlayer != null && CurrentState != AndroidVideoState.ERROR && CurrentState != AndroidVideoState.IDLE && CurrentState != AndroidVideoState.PREPARING;
    }

    void ISurfaceHolderCallback.SurfaceChanged(ISurfaceHolder holder, Format format, int width, int height)
    {
      this.SurfaceWidth = width;
      this.SurfaceHeight = height;

      var isValidState = (TargetState == AndroidVideoState.PLAYING);
      var hasValidSize = (VideoWidth == width && VideoHeight == height);
      if (MediaPlayer != null && isValidState && hasValidSize)
      {
        /*
        if (mSeekWhenPrepared != 0)
          seekTo(mSeekWhenPrepared);
        */

        Start();
      }
    }
    void ISurfaceHolderCallback.SurfaceCreated(ISurfaceHolder holder)
    {
      this.SurfaceHolder = holder;
      Acquire();
    }
    void ISurfaceHolderCallback.SurfaceDestroyed(ISurfaceHolder holder)
    {
      this.SurfaceHolder = null;
      /*
      if (mMediaController != null)
        mMediaController.hide();
      */
      Release(true);
    }
    void MediaPlayer.IOnPreparedListener.OnPrepared(MediaPlayer mp)
    {
      this.CurrentState = AndroidVideoState.PREPARED;
      /*
      // Get the capabilities of the player for this stream
      var data = mp.getMetadata(MediaPlayer.METADATA_ALL, MediaPlayer.BYPASS_METADATA_FILTER);
      if (data != null)
      {
        mCanPause = !data.has(Metadata.PAUSE_AVAILABLE) || data.getBoolean(Metadata.PAUSE_AVAILABLE);
        mCanSeekBack = !data.has(Metadata.SEEK_BACKWARD_AVAILABLE) || data.getBoolean(Metadata.SEEK_BACKWARD_AVAILABLE);
        mCanSeekForward = !data.has(Metadata.SEEK_FORWARD_AVAILABLE) || data.getBoolean(Metadata.SEEK_FORWARD_AVAILABLE);
      }
      else
      {
        mCanPause = true;
        mCanSeekBack = true;
        mCanSeekForward = true;
      }

      if (mMediaController != null)
        mMediaController.setEnabled(true);
      */
      this.VideoWidth = mp.VideoWidth;
      this.VideoHeight = mp.VideoHeight;

      if (VideoWidth != 0 && VideoHeight != 0)
      {
        Holder.SetFixedSize(VideoWidth, VideoHeight);
        if (SurfaceWidth == VideoWidth && SurfaceHeight == VideoHeight)
        {
          // We didn't actually change the size (it was already at the size
          // we need), so we won't get a "surface changed" callback, so
          // start the video here instead of in the callback.
          if (TargetState == AndroidVideoState.PLAYING)
          {
            Start();
            /*
            if (mMediaController != null)
              mMediaController.show();
            */
          }
          /*
          else if (!isPlaying() && (seekToPosition != 0 || getCurrentPosition() > 0))
          {
            // Show the media controls when we're paused into a video and make 'em stick.
            if (mMediaController != null)
              mMediaController.show(0);
          }
          */
        }
      }
      else
      {
        // We don't know the video size yet, but should start anyway.
        // The video size might be reported to us later.
        if (TargetState == AndroidVideoState.PLAYING)
          Start();
      }
    }
    void MediaPlayer.IOnVideoSizeChangedListener.OnVideoSizeChanged(MediaPlayer mp, int width, int height)
    {
      this.VideoWidth = mp.VideoWidth;
      this.VideoHeight = mp.VideoHeight;
      if (VideoWidth != 0 && VideoHeight != 0)
      {
        Holder.SetFixedSize(VideoWidth, VideoHeight);
        RequestLayout();
      }
    }
    void MediaPlayer.IOnCompletionListener.OnCompletion(MediaPlayer mp)
    {
      this.CurrentState = AndroidVideoState.PLAYBACK_COMPLETED;
      this.TargetState = AndroidVideoState.PLAYBACK_COMPLETED;

      /*
      if (mMediaController != null)
        mMediaController.hide();

      if (mAudioFocusType != AudioManager.AUDIOFOCUS_NONE)
        mAudioManager.abandonAudioFocus(null);
      */
    }
    bool MediaPlayer.IOnErrorListener.OnError(MediaPlayer mp, MediaError what, int extra)
    {
      this.CurrentState = AndroidVideoState.ERROR;
      this.TargetState = AndroidVideoState.ERROR;
      /*
      if (mMediaController != null)
        mMediaController.hide();

      // Otherwise, pop up an error dialog so the user knows that
      // something bad has happened. Only try and pop up the dialog
      // if we're attached to a window. When we're going away and no
      // longer have a window, don't bother showing the user an error.
      if (WindowToken != null)
      {
        var r = Context.Resources;
        int messageId;
        if (framework_err == MediaPlayer.MEDIA_ERROR_NOT_VALID_FOR_PROGRESSIVE_PLAYBACK)
        {
          messageId = com.android.internal.R.string.VideoView_error_text_invalid_progressive_playback;
        } else {
                    messageId = com.android.internal.R.string.VideoView_error_text_unknown;
                }
      new AlertDialog.Builder(mContext)
                              .setMessage(messageId)
                              .setPositiveButton(com.android.internal.R.string.VideoView_error_button,
                                      new DialogInterface.OnClickListener() {
                                          public void onClick(DialogInterface dialog, int whichButton)
      {
        // If we get here, there is no onError listener, so at least inform them that the video is over.
        if (mOnCompletionListener != null)
          mOnCompletionListener.onCompletion(mMediaPlayer);
      }
                                      })
                              .setCancelable(false).show();
                  }
      */
      return true;
    }

    private Android.Content.Res.AssetFileDescriptor Descriptor;
    private Uri Uri;
    private Android.Media.MediaPlayer MediaPlayer;
    private ISurfaceHolder SurfaceHolder;
    private int VideoWidth;
    private int VideoHeight;
    private int SurfaceWidth;
    private int SurfaceHeight;
    private AndroidVideoState TargetState;
    private AndroidVideoState CurrentState;

    private enum AndroidVideoState
    {
      ERROR = -1,
      IDLE = 0,
      PREPARING = 1,
      PREPARED = 2,
      PLAYING = 3,
      PAUSED = 4,
      PLAYBACK_COMPLETED = 5
    }
  }

  //public sealed class AndroidCanvas : Android.Views.SurfaceView, ISurfaceHolderCallback, Android.Views.GestureDetector.IOnGestureListener, ScaleGestureDetector.IOnScaleGestureListener
  public sealed class AndroidCanvas : Android.Views.View, Android.Views.GestureDetector.IOnGestureListener, Android.Views.ScaleGestureDetector.IOnScaleGestureListener, Inv.DrawContract
  {
    internal AndroidCanvas(Android.Content.Context context, AndroidEngine AndroidEngine)
      : base(context)
    {
      //this.Clickable = true; // NOTE: does not seem to be required when you are directly handling touch events.

      if (AndroidFoundation.SdkVersion >= 21)
        this.ClipToOutline = true; // clip drawing to inside the canvas view.

      this.ScaleDetector = new Android.Views.ScaleGestureDetector(context, this);

      this.Background = new AndroidBackground(this);

      this.AndroidEngine = AndroidEngine;

      this.AndroidArcRect = new Android.Graphics.RectF();
      this.AndroidEllipseRect = new Android.Graphics.RectF();
      this.AndroidBitmapRect = new Android.Graphics.Rect();
      this.AndroidTextRect = new Android.Graphics.Rect();
      this.AndroidImageMatrix = new Android.Graphics.Matrix();
    }

    public new AndroidBackground Background { get; }

    public event Action<int, int> PressEvent;
    public event Action<int, int> ReleaseEvent;
    public event Action<int, int> MoveEvent;
    public event Action<int, int> SingleTapEvent;
    public event Action<int, int> DoubleTapEvent;
    public event Action<int, int> LongPressEvent;
    public event Action<int, int, int> ZoomEvent;
    public Action GraphicsDrawAction;

    protected override void OnDraw(Android.Graphics.Canvas Canvas)
    {
      //base.OnDraw(Canvas);
      Background.Draw(Canvas);

      this.AndroidContext = Canvas;
      GraphicsDrawAction();
    }
    public override bool OnTouchEvent(Android.Views.MotionEvent Event)
    {
      //Debug.WriteLine(Event.Action.ToString());

      ScaleDetector.OnTouchEvent(Event);
      //GestureDetector.OnTouchEvent(Event);

      switch (Event.Action & Android.Views.MotionEventActions.Mask)
      {
        case Android.Views.MotionEventActions.Down:
          if (Handler != null)
            Handler.RemoveCallbacks(LongPressedInvoke);

          this.LastX = (int)Event.GetX();
          this.LastY = (int)Event.GetY();

          var CurrentTime = System.Environment.TickCount;
          var IsDoubleTap = CurrentTime - LastTime < Android.Views.ViewConfiguration.DoubleTapTimeout;
          if (IsDoubleTap)
          {
            if (IsPressed)
            {
              this.IsPressed = false;

              if (Handler != null)
                Handler.RemoveCallbacks(LongPressedInvoke);
            }

            if (DoubleTapEvent != null)
              DoubleTapEvent(LastX, LastY);
          }
          else
          {
            if (IsPressed && Handler != null)
              Handler.RemoveCallbacks(LongPressedInvoke);
            else
              this.IsPressed = true;

            if (Handler != null)
              Handler.PostDelayed(LongPressedInvoke, Android.Views.ViewConfiguration.LongPressTimeout);

            if (PressEvent != null)
              PressEvent(LastX, LastY);
          }
          this.LastTime = CurrentTime;
          break;

        case Android.Views.MotionEventActions.Move:
          var MoveX = Event.GetX();
          var MoveY = Event.GetY();

          if (MoveEvent != null)
            MoveEvent((int)MoveX, (int)MoveY);

          if (IsPressed)
          {
            var Distance = ((MoveY - LastY) * (MoveY - LastY)) + ((MoveX - LastX) * (MoveX - LastX));
            if (Distance > 400) // sqrt(400) == 20
            {
              this.IsPressed = false;

              if (Handler != null)
                Handler.RemoveCallbacks(LongPressedInvoke);
            }
          }
          break;

        case Android.Views.MotionEventActions.Up:
          var ReleaseX = (int)Event.GetX();
          var ReleaseY = (int)Event.GetY();

          if (IsPressed)
          {
            this.IsPressed = false;

            if (SingleTapEvent != null)
              SingleTapEvent(ReleaseX, ReleaseY);

            if (Handler != null)
              Handler.RemoveCallbacks(LongPressedInvoke);
          }

          if (ReleaseEvent != null)
            ReleaseEvent(ReleaseX, ReleaseY);
          break;

        case Android.Views.MotionEventActions.Cancel:
          if (IsPressed)
          {
            this.IsPressed = false;

            if (Handler != null)
              Handler.RemoveCallbacks(LongPressedInvoke);
          }
          break;
      }

      return true;
    }
    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      base.OnLayout(Changed, Left, Top, Right, Bottom);

      SW.StopLayout();
    }

    private void SingleTapInvoke()
    {
    }
    private void LongPressedInvoke()
    {
      this.IsPressed = false;

      if (LongPressEvent != null)
        LongPressEvent(LastX, LastY);
    }

    bool Android.Views.GestureDetector.IOnGestureListener.OnDown(Android.Views.MotionEvent Event)
    {
      this.LastX = (int)Event.GetX();
      this.LastY = (int)Event.GetY();
      SingleTapInvoke();

      return true;
    }
    bool Android.Views.GestureDetector.IOnGestureListener.OnFling(Android.Views.MotionEvent Event1, Android.Views.MotionEvent Event2, float velocityX, float velocityY)
    {
      return true;
    }
    void Android.Views.GestureDetector.IOnGestureListener.OnLongPress(Android.Views.MotionEvent Event)
    {
      this.LastX = (int)Event.GetX();
      this.LastY = (int)Event.GetY();
      LongPressedInvoke();
    }
    bool Android.Views.GestureDetector.IOnGestureListener.OnScroll(Android.Views.MotionEvent Event1, Android.Views.MotionEvent Event2, float distanceX, float distanceY)
    {
      return true;
    }
    void Android.Views.GestureDetector.IOnGestureListener.OnShowPress(Android.Views.MotionEvent Event)
    {
    }
    bool Android.Views.GestureDetector.IOnGestureListener.OnSingleTapUp(Android.Views.MotionEvent Event)
    {
      return true;
    }
    bool Android.Views.ScaleGestureDetector.IOnScaleGestureListener.OnScale(Android.Views.ScaleGestureDetector detector)
    {
      if (ZoomEvent != null)
        ZoomEvent((int)detector.FocusX, (int)detector.FocusY, (int)(detector.ScaleFactor) > 0 ? +1 : -1);

      return true;
    }
    bool Android.Views.ScaleGestureDetector.IOnScaleGestureListener.OnScaleBegin(Android.Views.ScaleGestureDetector detector)
    {
      return true;
    }
    void Android.Views.ScaleGestureDetector.IOnScaleGestureListener.OnScaleEnd(Android.Views.ScaleGestureDetector detector)
    {
    }

    void DrawContract.DrawText(string TextFragment, string TextFontName, int TextFontSize, FontWeight TextFontWeight, Colour TextFontColour, Point TextPoint, HorizontalPosition TextHorizontal, VerticalPosition TextVertical)
    {
      var TextSizePixels = AndroidEngine.PtToPx(TextFontSize);
      var TextPaint = AndroidEngine.TranslateGraphicsTextPaint(TextFontName.EmptyAsNull(), TextSizePixels, TextFontWeight, TextFontColour, false);
      var TextPointX = AndroidEngine.PtToPx(TextPoint.X);
      var TextPointY = AndroidEngine.PtToPx(TextPoint.Y);
      var TextString = TextFragment ?? ""; // avoid null-ops.

      TextPaint.GetTextBounds(TextString, 0, TextString.Length, AndroidTextRect);

      if (TextHorizontal != HorizontalPosition.Left)
      {
        var TextWidth = AndroidTextRect.Width();

        if (TextHorizontal == HorizontalPosition.Right)
          TextPointX -= TextWidth;
        else if (TextHorizontal == HorizontalPosition.Center)
          TextPointX -= TextWidth / 2;
      }

      if (TextVertical != VerticalPosition.Bottom)
      {
        var TextHeight = AndroidTextRect.Height();

        if (TextVertical == VerticalPosition.Top)
          TextPointY += TextHeight;
        else if (TextVertical == VerticalPosition.Center)
          TextPointY += TextHeight / 2;
      }

      AndroidContext.DrawText(TextString, TextPointX, TextPointY, TextPaint);
    }
    void DrawContract.DrawLine(Inv.Colour LineStrokeColour, int LineStrokeThickness, Inv.Point LineSourcePoint, Inv.Point LineTargetPoint, params Inv.Point[] LineExtraPointArray)
    {
      if (LineStrokeThickness <= 0 || LineStrokeColour == Inv.Colour.Transparent)
        return;

      var LineStrokePixels = AndroidEngine.PtToPx(LineStrokeThickness);

      var LinePaint = AndroidEngine.TranslateGraphicsStrokePaint(LineStrokeColour, LineStrokePixels);

      if (LineExtraPointArray.Length == 0)
      {
        AndroidContext.DrawLine(
          AndroidEngine.PtToPx(LineSourcePoint.X), AndroidEngine.PtToPx(LineSourcePoint.Y),
          AndroidEngine.PtToPx(LineTargetPoint.X), AndroidEngine.PtToPx(LineTargetPoint.Y), LinePaint);
      }
      else
      {
        var PositionArray = new float[(LineExtraPointArray.Length * 4) + 4];

        PositionArray[0] = AndroidEngine.PtToPx(LineSourcePoint.X);
        PositionArray[1] = AndroidEngine.PtToPx(LineSourcePoint.Y);
        PositionArray[2] = AndroidEngine.PtToPx(LineTargetPoint.X);
        PositionArray[3] = AndroidEngine.PtToPx(LineTargetPoint.Y);

        var CurrentPointX = PositionArray[2];
        var CurrentPointY = PositionArray[3];

        var PositionIndex = 4;
        for (var Index = 0; Index < LineExtraPointArray.Length; Index++)
        {
          var TargetPoint = LineExtraPointArray[Index];
          var TargetPointX = AndroidEngine.PtToPx(TargetPoint.X);
          var TargetPointY = AndroidEngine.PtToPx(TargetPoint.Y);

          PositionArray[PositionIndex++] = CurrentPointX;
          PositionArray[PositionIndex++] = CurrentPointY;
          PositionArray[PositionIndex++] = TargetPointX;
          PositionArray[PositionIndex++] = TargetPointY;

          CurrentPointX = TargetPointX;
          CurrentPointY = TargetPointY;
        }

        AndroidContext.DrawLines(PositionArray, LinePaint);
      }
    }
    void DrawContract.DrawRectangle(Colour RectangleFillColour, Colour RectangleStrokeColour, int RectangleStrokeThickness, Rect RectangleRect)
    {
      var RectangleStrokePixels = AndroidEngine.PtToPx(RectangleStrokeThickness);
      var RectangleLeft = AndroidEngine.PtToPx(RectangleRect.Left);
      var RectangleTop = AndroidEngine.PtToPx(RectangleRect.Top);
      var RectangleRight = AndroidEngine.PtToPx(RectangleRect.Right + 1);
      var RectangleBottom = AndroidEngine.PtToPx(RectangleRect.Bottom + 1);

      if (RectangleFillColour != null)
        AndroidContext.DrawRect(RectangleLeft, RectangleTop, RectangleRight, RectangleBottom, AndroidEngine.TranslateGraphicsFillPaint(RectangleFillColour));

      if (RectangleStrokeColour != null && RectangleStrokePixels > 0)
      {
        var HalfThickness = RectangleStrokePixels / 2;

        RectangleLeft += HalfThickness;
        RectangleTop += HalfThickness;

        if (RectangleLeft + HalfThickness < RectangleRight)
          RectangleRight -= HalfThickness;

        if (RectangleTop + HalfThickness < RectangleBottom)
          RectangleBottom -= HalfThickness;

        AndroidContext.DrawRect(RectangleLeft, RectangleTop, RectangleRight, RectangleBottom, AndroidEngine.TranslateGraphicsStrokePaint(RectangleStrokeColour, RectangleStrokePixels));
      }
    }
    void DrawContract.DrawArc(Inv.Colour ArcFillColour, Inv.Colour ArcStrokeColour, int ArcStrokeThickness, Inv.Point ArcCenter, Inv.Point ArcRadius, float StartAngle, float SweepAngle)
    {
      var ArcStrokePixels = AndroidEngine.PtToPx(ArcStrokeThickness);
      AndroidArcRect.Left = AndroidEngine.PtToPx(ArcCenter.X - ArcRadius.X);
      AndroidArcRect.Top = AndroidEngine.PtToPx(ArcCenter.Y - ArcRadius.Y);
      AndroidArcRect.Right = AndroidEngine.PtToPx(ArcCenter.X + ArcRadius.X);
      AndroidArcRect.Bottom = AndroidEngine.PtToPx(ArcCenter.Y + ArcRadius.Y);

      // NOTE: StartAngle-90 because Android starts at 3 o'clock for some reason.

      if (ArcFillColour != null)
        AndroidContext.DrawArc(AndroidArcRect, StartAngle - 90, SweepAngle - StartAngle, true, AndroidEngine.TranslateGraphicsFillPaint(ArcFillColour));

      if (ArcStrokeColour != null && ArcStrokeThickness > 0)
        AndroidContext.DrawArc(AndroidArcRect, StartAngle - 90, SweepAngle - StartAngle, true, AndroidEngine.TranslateGraphicsStrokePaint(ArcStrokeColour, ArcStrokePixels));
    }
    void DrawContract.DrawEllipse(Colour EllipseFillColour, Colour EllipseStrokeColour, int EllipseStrokeThickness, Point EllipseCenter, Point EllipseRadius)
    {
      var EllipseStrokePixels = AndroidEngine.PtToPx(EllipseStrokeThickness);
      AndroidEllipseRect.Left = AndroidEngine.PtToPx(EllipseCenter.X - EllipseRadius.X);
      AndroidEllipseRect.Top = AndroidEngine.PtToPx(EllipseCenter.Y - EllipseRadius.Y);
      AndroidEllipseRect.Right = AndroidEngine.PtToPx(EllipseCenter.X + EllipseRadius.X);
      AndroidEllipseRect.Bottom = AndroidEngine.PtToPx(EllipseCenter.Y + EllipseRadius.Y);

      if (EllipseFillColour != null)
        AndroidContext.DrawOval(AndroidEllipseRect, AndroidEngine.TranslateGraphicsFillPaint(EllipseFillColour));

      if (EllipseStrokeColour != null && EllipseStrokeThickness > 0)
        AndroidContext.DrawOval(AndroidEllipseRect, AndroidEngine.TranslateGraphicsStrokePaint(EllipseStrokeColour, EllipseStrokePixels));

      // NOTE: had a problem with a 5.0 emulator, so decided to just not use this supposed 21+ API call.
      //if (IsAPILevel21) 
      //{
      //  if (EllipseFillColour != null)
      //    AndroidCanvas.DrawOval(EllipseLeft, EllipseTop, EllipseRight, EllipseBottom, TranslateGraphicsFillPaint(EllipseFillColour));
      //
      //  if (EllipseStrokeColour != null && EllipseStrokeThickness > 0)
      //    AndroidCanvas.DrawOval(EllipseLeft, EllipseTop, EllipseRight, EllipseBottom, TranslateGraphicsStrokePaint(EllipseStrokeColour, EllipseStrokePixels));
      //}
    }
    void DrawContract.DrawImage(Image ImageSource, Rect ImageRect, float ImageOpacity, Colour ImageTint, Mirror? ImageMirror, float ImageRotation)
    {
      var ImageBitmap = AndroidEngine.TranslateGraphicsBitmap(ImageSource);

      if (ImageBitmap != null)
      {
        AndroidBitmapRect.Left = AndroidEngine.PtToPx(ImageRect.Left);
        AndroidBitmapRect.Top = AndroidEngine.PtToPx(ImageRect.Top);
        AndroidBitmapRect.Right = AndroidEngine.PtToPx(ImageRect.Right + 1);
        AndroidBitmapRect.Bottom = AndroidEngine.PtToPx(ImageRect.Bottom + 1);

        var ImagePaint = AndroidEngine.TranslateGraphicsImagePaint(ImageOpacity, ImageTint);

        if (ImageMirror == null && ImageRotation == 0.0F)
        {
          AndroidContext.DrawBitmap(ImageBitmap, null, AndroidBitmapRect, ImagePaint);
        }
        else
        {
          AndroidImageMatrix.Reset();

          var BitmapWidth = AndroidBitmapRect.Width();
          var BitmapHeight = AndroidBitmapRect.Height();

          var ScaleX = (float)BitmapWidth / (float)ImageBitmap.Width;
          var ScaleY = (float)BitmapHeight / (float)ImageBitmap.Height;

          if (ImageMirror == Mirror.Horizontal)
          {
            AndroidImageMatrix.SetScale(-ScaleX, +ScaleY);
            AndroidImageMatrix.PostTranslate(AndroidBitmapRect.Left + BitmapWidth, AndroidBitmapRect.Top);
          }
          else if (ImageMirror == Mirror.Vertical)
          {
            AndroidImageMatrix.SetScale(+ScaleX, -ScaleY);
            AndroidImageMatrix.PostTranslate(AndroidBitmapRect.Left, AndroidBitmapRect.Top + BitmapHeight);
          }
          else
          {
            AndroidImageMatrix.SetScale(+ScaleX, +ScaleY);
            AndroidImageMatrix.PostTranslate(AndroidBitmapRect.Left, AndroidBitmapRect.Top);
          }

          if (ImageRotation != 0.0F)
            AndroidImageMatrix.PreRotate(ImageMirror != null ? -ImageRotation : ImageRotation, ImageBitmap.Width / 2, ImageBitmap.Height / 2);

          AndroidContext.DrawBitmap(ImageBitmap, AndroidImageMatrix, ImagePaint);
        }
      }
    }
    void DrawContract.DrawPolygon(Colour FillColour, Colour StrokeColour, int StrokeThickness, LineJoin LineJoin, Point StartPoint, params Point[] PointArray)
    {
      var Path = new Android.Graphics.Path();
      Path.MoveTo(AndroidEngine.PtToPx(StartPoint.X), AndroidEngine.PtToPx(StartPoint.Y));

      foreach (var Point in PointArray)
        Path.LineTo(AndroidEngine.PtToPx(Point.X), AndroidEngine.PtToPx(Point.Y));

      var PolygonStrokePixels = AndroidEngine.PtToPx(StrokeThickness);

      AndroidContext.DrawPath(Path, AndroidEngine.TranslateGraphicsFillAndStrokePaint(FillColour, PolygonStrokePixels));
    }

    private int LastTime;
    private int LastX;
    private int LastY;
    private bool IsPressed;
    private Android.Graphics.Canvas AndroidContext;
    private readonly Android.Views.ScaleGestureDetector ScaleDetector;
    private readonly AndroidEngine AndroidEngine;
    private readonly Android.Graphics.RectF AndroidEllipseRect;
    private readonly Android.Graphics.RectF AndroidArcRect;
    private readonly Android.Graphics.Rect AndroidBitmapRect;
    private readonly Android.Graphics.Rect AndroidTextRect;
    private readonly Android.Graphics.Matrix AndroidImageMatrix;
    private readonly AndroidStopwatch SW = AndroidInstrumentation.CanvasPerformance.NewStopwatch();
  }

  public enum AndroidHorizontal
  {
    Stretch,
    Center,
    Left,
    Right
  }

  public enum AndroidVertical
  {
    Stretch,
    Center,
    Top,
    Bottom
  }

  public sealed class AndroidContainer : Android.Views.ViewGroup
  {
    internal AndroidContainer(Android.Content.Context context, Inv.Control ReferenceControl)
      : base(context)
    {
      this.SetClipChildren(false); // cannot be set to true or elevations may be clipped.

      this.ReferenceControl = ReferenceControl;
      this.ContentWidth = -1;
      this.ContentHeight = -1;
      this.ContentMinimumWidth = -1;
      this.ContentMinimumHeight = -1;
      this.ContentMaximumWidth = -1;
      this.ContentMaximumHeight = -1;
      this.ContentVisible = true;

      //this.MotionEventSplittingEnabled = false;
    }

    public Android.Views.View ContentElement { get; private set; }
    public event Action<Inv.Control> SizeChanged;
    [Obsolete("Containers should not have a background", true)]
    public new Android.Graphics.Drawables.Drawable Background
    {
      get { return base.Background; }
      set { base.Background = value; }
    }

    public void SetContentVisiblity(bool ContentVisible)
    {
      if (ContentVisible != this.ContentVisible)
      {
        this.ContentVisible = ContentVisible;

        this.Visibility = ContentVisible ? ViewStates.Visible : ViewStates.Gone;

        this.Arrange();
      }
    }
    public void SetContentHorizontal(AndroidHorizontal ContentHorizontal)
    {
      if (ContentHorizontal != this.ContentHorizontal)
      {
        this.ContentHorizontal = ContentHorizontal;

        this.Arrange();
      }
    }
    public void SetContentVertical(AndroidVertical ContentVertical)
    {
      if (ContentVertical != this.ContentVertical)
      {
        this.ContentVertical = ContentVertical;

        this.Arrange();
      }
    }
    public void SetContentElement(Android.Views.View ContentElement)
    {
      if (ContentElement != this.ContentElement)
      {
        if (this.ContentElement != null)
          this.SafeRemoveView(this.ContentElement);

        this.ContentElement = ContentElement;

        if (this.ContentElement != null)
          this.SafeAddView(this.ContentElement);

        this.Arrange();
      }
    }
    public void SetContentWidth(int ContentWidth)
    {
      if (ContentWidth != this.ContentWidth)
      {
        this.ContentWidth = ContentWidth;

        this.Arrange();
      }
    }
    public void SetContentHeight(int ContentHeight)
    {
      if (ContentHeight != this.ContentHeight)
      {
        this.ContentHeight = ContentHeight;

        this.Arrange();
      }
    }
    public void SetContentMinimumWidth(int ContentMinimumWidth)
    {
      if (ContentMinimumWidth != this.ContentMinimumWidth)
      {
        this.ContentMinimumWidth = ContentMinimumWidth;

        this.Arrange();
      }
    }
    public void SetContentMaximumWidth(int ContentMaximumWidth)
    {
      if (ContentMaximumWidth != this.ContentMaximumWidth)
      {
        this.ContentMaximumWidth = ContentMaximumWidth;

        this.Arrange();
      }
    }
    public void SetContentMinimumHeight(int ContentMinimumHeight)
    {
      if (ContentMinimumHeight != this.ContentMinimumHeight)
      {
        this.ContentMinimumHeight = ContentMinimumHeight;

        this.Arrange();
      }
    }
    public void SetContentMaximumHeight(int ContentMaximumHeight)
    {
      if (ContentMaximumHeight != this.ContentMaximumHeight)
      {
        this.ContentMaximumHeight = ContentMaximumHeight;

        this.Arrange();
      }
    }
    public void ClearContentWidth()
    {
      SetContentWidth(-1);
    }
    public void ClearContentHeight()
    {
      SetContentHeight(-1);
    }
    public void ClearContentMinimumWidth()
    {
      SetContentMinimumWidth(-1);
    }
    public void ClearContentMinimumHeight()
    {
      SetContentMinimumHeight(-1);
    }
    public void ClearContentMaximumWidth()
    {
      SetContentMaximumWidth(-1);
    }
    public void ClearContentMaximumHeight()
    {
      SetContentMaximumHeight(-1);
    }
    public void SetContentVerticalCenter()
    {
      SetContentVertical(AndroidVertical.Center);
    }
    public void SetContentHorizontalCenter()
    {
      SetContentHorizontal(AndroidHorizontal.Center);
    }
    public void SetContentVerticalTop()
    {
      SetContentVertical(AndroidVertical.Top);
    }
    public void SetContentVerticalBottom()
    {
      SetContentVertical(AndroidVertical.Bottom);
    }
    public void SetContentHorizontalLeft()
    {
      SetContentHorizontal(AndroidHorizontal.Left);
    }
    public void SetContentHorizontalRight()
    {
      SetContentHorizontal(AndroidHorizontal.Right);
    }
    public void SetContentHorizontalStretch()
    {
      SetContentHorizontal(AndroidHorizontal.Stretch);
    }
    public void SetContentVerticalStretch()
    {
      SetContentVertical(AndroidVertical.Stretch);
    }
    public void SetContentMargins(int Left, int Top, int Right, int Bottom)
    {
      SetPadding(Left, Top, Right, Bottom);
    }

    internal readonly Inv.Control ReferenceControl;
    internal bool ContentVisible { get; private set; }

    protected override void OnMeasure(int WidthMeasureSpec, int HeightMeasureSpec)
    {
      SW.StartMeasure();

      base.OnMeasure(WidthMeasureSpec, HeightMeasureSpec);

      var WidthSize = MeasuredWidth; //MeasureSpec.GetSize(WidthMeasureSpec);
      var WidthMode = MeasureSpec.GetMode(WidthMeasureSpec);
      var HeightSize = MeasuredHeight; //MeasureSpec.GetSize(HeightMeasureSpec);
      var HeightMode = MeasureSpec.GetMode(HeightMeasureSpec);
      var HorizontalPadding = PaddingLeft + PaddingRight;
      var VerticalPadding = PaddingTop + PaddingBottom;

      if (ContentElement != null)
      {
        if (ContentVisible)
        {
          //ContentElement.Visibility = Android.Views.ViewStates.Visible;

          var ChildWidthSize = Math.Max(0, WidthSize - HorizontalPadding);
          var ChildWidthMode = WidthMode;
          var ChildHeightSize = Math.Max(0, HeightSize - VerticalPadding);
          var ChildHeightMode = HeightMode;

          if (WidthMode == Android.Views.MeasureSpecMode.Exactly && ContentHorizontal != AndroidHorizontal.Stretch)
            ChildWidthMode = Android.Views.MeasureSpecMode.AtMost;

          if (HeightMode == Android.Views.MeasureSpecMode.Exactly && ContentVertical != AndroidVertical.Stretch)
            ChildHeightMode = Android.Views.MeasureSpecMode.AtMost;

          if (ContentWidth >= 0)
          {
            if (ContentWidth <= ChildWidthSize || ChildWidthMode == Android.Views.MeasureSpecMode.Unspecified)
              ChildWidthSize = ContentWidth;

            ChildWidthMode = Android.Views.MeasureSpecMode.Exactly;
          }
          else if (ContentMaximumWidth >= 0)
          {
            if (ChildWidthSize >= ContentMaximumWidth || ChildWidthMode == Android.Views.MeasureSpecMode.Unspecified)
            {
              ChildWidthSize = Math.Min(ContentMaximumWidth, ChildWidthSize);
              ChildWidthMode = Android.Views.MeasureSpecMode.AtMost;
            }
          }

          if (ContentHeight >= 0)
          {
            if (ContentHeight <= ChildHeightSize || ChildHeightMode == Android.Views.MeasureSpecMode.Unspecified)
              ChildHeightSize = ContentHeight;

            ChildHeightMode = Android.Views.MeasureSpecMode.Exactly;
          }
          else if (ContentMaximumHeight >= 0)
          {
            if (ChildHeightSize >= ContentMaximumHeight || ChildHeightMode == Android.Views.MeasureSpecMode.Unspecified)
            {
              ChildHeightSize = Math.Min(ContentMaximumHeight, ChildHeightSize);
              ChildHeightMode = Android.Views.MeasureSpecMode.AtMost;
            }
          }

#if MINIMUM_MEASURE
          ContentElement.SetMinimumWidth(ContentMinimumWidth);
          ContentElement.SetMinimumHeight(ContentMinimumHeight);
#endif

          ContentElement.Measure(MeasureSpec.MakeMeasureSpec(ChildWidthSize, ChildWidthMode), MeasureSpec.MakeMeasureSpec(ChildHeightSize, ChildHeightMode));

          var RequireSecondMeasure = false;

          if (ContentElement.MeasuredWidth < ContentMinimumWidth && ChildWidthMode != Android.Views.MeasureSpecMode.Exactly)
          {
#if MINIMUM_MEASURE
            System.Diagnostics.Debug.Assert(false, "MinimumWidth did not work.");
#endif

            ChildWidthSize = ChildWidthMode == MeasureSpecMode.AtMost ? Math.Min(ContentMinimumWidth, ChildWidthSize) : ContentMinimumWidth;
            ChildWidthMode = Android.Views.MeasureSpecMode.Exactly;
            RequireSecondMeasure = true;
          }

          if (ContentElement.MeasuredHeight < ContentMinimumHeight && ChildHeightMode != Android.Views.MeasureSpecMode.Exactly)
          {
#if MINIMUM_MEASURE
            System.Diagnostics.Debug.Assert(false, "MinimumHeight did not work.");
#endif

            ChildHeightSize = ChildHeightMode == MeasureSpecMode.AtMost ? Math.Min(ContentMinimumHeight, ChildHeightSize) : ContentMinimumHeight;
            ChildHeightMode = Android.Views.MeasureSpecMode.Exactly;
            RequireSecondMeasure = true;
          }

          if (RequireSecondMeasure)
            ContentElement.Measure(MeasureSpec.MakeMeasureSpec(ChildWidthSize, ChildWidthMode), MeasureSpec.MakeMeasureSpec(ChildHeightSize, ChildHeightMode));

          var ContainerWidth = WidthSize;
          var ContainerHeight = HeightSize;

          var TotalContentWidth = ContentElement.MeasuredWidth + HorizontalPadding;
          if ((WidthMode == Android.Views.MeasureSpecMode.Unspecified && TotalContentWidth > ContainerWidth) || (WidthMode == Android.Views.MeasureSpecMode.AtMost && TotalContentWidth < ContainerWidth))
            ContainerWidth = TotalContentWidth;

          var TotalContentHeight = ContentElement.MeasuredHeight + VerticalPadding;
          if ((HeightMode == Android.Views.MeasureSpecMode.Unspecified && TotalContentHeight > ContainerHeight) || (HeightMode == Android.Views.MeasureSpecMode.AtMost && TotalContentHeight < ContainerHeight))
            ContainerHeight = TotalContentHeight;

#if DEBUG
          //if (ContainerWidth < 0 || ContainerHeight < 0 )
          //  System.Diagnostics.Debug.WriteLine("Container: " + ContainerWidth + ", " + ContainerHeight);
#endif

          SetMeasuredDimension(ContainerWidth, ContainerHeight);
        }
        else
        {
          //ContentElement.Visibility = Android.Views.ViewStates.Gone;
          ContentElement.Measure(MeasureSpec.MakeMeasureSpec(0, Android.Views.MeasureSpecMode.Exactly), MeasureSpec.MakeMeasureSpec(0, Android.Views.MeasureSpecMode.Exactly));
          SetMeasuredDimension(WidthMode == Android.Views.MeasureSpecMode.Exactly ? WidthSize : 0, HeightMode == Android.Views.MeasureSpecMode.Exactly ? HeightSize : 0);
        }
      }
      else
      {
        var ResultWidth = WidthMode == Android.Views.MeasureSpecMode.Exactly ? WidthSize : HorizontalPadding;
        var ResultHeight = HeightMode == Android.Views.MeasureSpecMode.Exactly ? HeightSize : VerticalPadding;
        SetMeasuredDimension(ResultWidth, ResultHeight);
      }

      SW.StopMeasure();
    }
    protected override void OnLayout(bool Changed, int Left, int Top, int Right, int Bottom)
    {
      SW.StartLayout();

      if (ContentElement != null)
      {
        if (!ContentVisible)
        {
          //ContentElement.LayoutChild(0, 0, 0, 0);
        }
        else
        {
          var ElementLeft = 0;
          var ElementTop = 0;
          var ElementWidth = ContentElement.MeasuredWidth;
          var ElementHeight = ContentElement.MeasuredHeight;
          var BoundsWidth = Right - Left;
          var BoundsHeight = Bottom - Top;

          switch (ContentHorizontal)
          {
            case AndroidHorizontal.Left:
              ElementLeft = PaddingLeft;
              break;

            case AndroidHorizontal.Center:
            case AndroidHorizontal.Stretch:
              ElementLeft = PaddingLeft + (BoundsWidth - PaddingLeft - PaddingRight - ElementWidth) / 2;
              break;

            case AndroidHorizontal.Right:
              ElementLeft = BoundsWidth - PaddingRight - ElementWidth;
              break;

            default:
              throw new UnhandledEnumCaseException<AndroidHorizontal>();
          }

          switch (ContentVertical)
          {
            case AndroidVertical.Top:
              ElementTop = PaddingTop;
              break;

            case AndroidVertical.Center:
            case AndroidVertical.Stretch:
              ElementTop = PaddingTop + (BoundsHeight - PaddingTop - PaddingBottom - ElementHeight) / 2;
              break;

            case AndroidVertical.Bottom:
              ElementTop = BoundsHeight - PaddingBottom - ElementHeight;
              break;

            default:
              throw new UnhandledEnumCaseException<AndroidVertical>();
          }

#if DEBUG
          //if (ElementWidth > BoundsWidth || ElementHeight > BoundsHeight)
          //  System.Diagnostics.Debug.WriteLine("Element Size: (" + ElementWidth + ", " + ElementHeight + ") vs (" + BoundsWidth + ", " + BoundsHeight + ")");
#endif

          var OldWidth = ContentElement.Width;
          var OldHeight = ContentElement.Height;

          ContentElement.LayoutChild(ElementLeft, ElementTop, ElementLeft + ElementWidth, ElementTop + ElementHeight);

          if (ElementWidth != OldWidth || ElementHeight != OldHeight)
          {
            if (SizeChanged != null)
              SizeChanged(ReferenceControl);
          }
        }
      }

      SW.StopLayout();
    }

    private AndroidVertical ContentVertical;
    private AndroidHorizontal ContentHorizontal;
    private int ContentWidth;
    private int ContentHeight;
    private int ContentMinimumWidth;
    private int ContentMinimumHeight;
    private int ContentMaximumWidth;
    private int ContentMaximumHeight;
    private readonly AndroidStopwatch SW = AndroidInstrumentation.ContainerPerformance.NewStopwatch();
  }

  /// <summary>
  /// Represents the background of a view including colour, corner radius and borders.
  /// </summary>
  public sealed class AndroidBackground
  {
    // NOTE: this is no longer a subclass of Android.Graphics.Drawables.Drawable because that led to global weak reference (gref) leaks.
    internal AndroidBackground(Android.Views.View OwnerView)
    {
#if DEBUG
      if (OwnerView.WillNotDraw())
        throw new Exception("OwnerView must not be marked as 'Will Not Draw' and OnDraw(canvas) must be overridden to call Background.Draw(canvas).");
#endif

      this.OwnerView = OwnerView;
    }

    public Android.Graphics.Color? GetColor()
    {
      return BackgroundColour;
    }
    public void SetColor(Android.Graphics.Color Color)
    {
      this.BackgroundColour = Color;

      OwnerView.Invalidate();
    }
    public void SetCornerRadius(int TopLeft, int TopRight, int BottomRight, int BottomLeft)
    {
      this.TopLeftCornerRadius = TopLeft;
      this.TopRightCornerRadius = TopRight;
      this.BottomRightCornerRadius = BottomRight;
      this.BottomLeftCornerRadius = BottomLeft;

      OwnerView.Invalidate();
    }
    public void SetBorderStroke(Android.Graphics.Color Color, int Left, int Top, int Right, int Bottom)
    {
      this.BorderColour = Color;

      this.LeftBorderThickness = Left;
      this.TopBorderThickness = Top;
      this.RightBorderThickness = Right;
      this.BottomBorderThickness = Bottom;

      OwnerView.Invalidate();
    }

    internal void Draw(Android.Graphics.Canvas Canvas)
    {
      Paint.Color = BackgroundColour ?? Android.Graphics.Color.Transparent;

      var BorderThicknessSet = IsBorderThicknessSet;
      var IsClipPath = BorderThicknessSet || IsCornerRadiusSet;

      Android.Graphics.Drawables.Shapes.Shape InnerShape = null;
      Android.Graphics.Drawables.Shapes.Shape BorderShape = null;
      try
      {
        if (IsClipPath)
        {
          var ViewWidth = OwnerView.Width;
          var ViewHeight = OwnerView.Height;

          PathSize.Width = ViewWidth;
          PathSize.Height = ViewHeight;

          PathCornerRadius.TopLeft = TopLeftCornerRadius;
          PathCornerRadius.TopRight = TopRightCornerRadius;
          PathCornerRadius.BottomRight = BottomRightCornerRadius;
          PathCornerRadius.BottomLeft = BottomLeftCornerRadius;

          PathBorderThickness.Left = LeftBorderThickness;
          PathBorderThickness.Top = TopBorderThickness;
          PathBorderThickness.Right = RightBorderThickness;
          PathBorderThickness.Bottom = BottomBorderThickness;

          // Clipping is required on android to resolve situation where inner border edges can extend beyond outer edges
          ClipPath.Reset();
          ClipBuilder.AddRoundedRect(PathSize, PathCornerRadius, PathBorderThickness);

          InnerPath.Reset();
          InnerBuilder.AddRoundedRectMinusBorder(PathSize, PathCornerRadius, PathBorderThickness);

          InnerShape = new Android.Graphics.Drawables.Shapes.PathShape(InnerPath, ViewWidth, ViewHeight);
          InnerShape.Resize(ViewWidth, ViewHeight);

          if (BorderThicknessSet)
          {
            BorderPaint.Color = BorderColour ?? Android.Graphics.Color.Transparent;

            BorderPath.Reset();
            BorderBuilder.AddRoundedRectBorder(PathSize, PathCornerRadius, PathBorderThickness);

            BorderShape = new Android.Graphics.Drawables.Shapes.PathShape(BorderPath, ViewWidth, ViewHeight);
            BorderShape.Resize(ViewWidth, ViewHeight);
          }
        }

        if (IsClipPath)
          Canvas.ClipPath(ClipPath);

        if (InnerShape == null)
        {
          // 2018-08-17 KJV Android Pie (9.0) appears to be broken with respect to clipping at the moment.
          if (AndroidFoundation.SdkVersion >= 28)
          {
            Canvas.Save();
            Canvas.ClipRect(0, 0, OwnerView.MeasuredWidth, OwnerView.MeasuredHeight);
            Canvas.DrawPaint(Paint);
            Canvas.Restore();
          }
          else
          {
            Canvas.DrawPaint(Paint);
          }
        }
        else
        {
          InnerShape.Draw(Canvas, Paint);

          if (BorderShape != null)
            BorderShape.Draw(Canvas, BorderPaint);
        }
      }
      finally
      {
        BorderShape?.Dispose();
        InnerShape?.Dispose();
      }
    }

    private readonly Android.Views.View OwnerView;
    private int TopLeftCornerRadius;
    private int TopRightCornerRadius;
    private int BottomRightCornerRadius;
    private int BottomLeftCornerRadius;
    private int LeftBorderThickness;
    private int TopBorderThickness;
    private int RightBorderThickness;
    private int BottomBorderThickness;
    private Android.Graphics.Color? BackgroundColour;
    private Android.Graphics.Color? BorderColour;
    private bool IsCornerRadiusSet
    {
      get { return TopLeftCornerRadius != 0 || TopRightCornerRadius != 0 || BottomRightCornerRadius != 0 || BottomLeftCornerRadius != 0; }
    }
    private bool IsBorderThicknessSet
    {
      get { return LeftBorderThickness != 0 || TopBorderThickness != 0 || RightBorderThickness != 0 || BottomBorderThickness != 0; }
    }

    private static readonly Android.Graphics.Paint Paint = new Android.Graphics.Paint();
    private static readonly Android.Graphics.Paint BorderPaint = new Android.Graphics.Paint();
    private static readonly Android.Graphics.Path ClipPath = new Android.Graphics.Path();
    private static readonly AndroidBezierPathBuilder ClipBuilder = new AndroidBezierPathBuilder(ClipPath);
    private static readonly Android.Graphics.Path InnerPath = new Android.Graphics.Path();
    private static readonly AndroidBezierPathBuilder InnerBuilder = new AndroidBezierPathBuilder(InnerPath);
    private static readonly Android.Graphics.Path BorderPath = new Android.Graphics.Path();
    private static readonly AndroidBezierPathBuilder BorderBuilder = new AndroidBezierPathBuilder(BorderPath);
    private static Inv.BezierSize PathSize = new Inv.BezierSize();
    private static Inv.BezierCornerRadius PathCornerRadius = new Inv.BezierCornerRadius();
    private static Inv.BezierBorderThickness PathBorderThickness = new Inv.BezierBorderThickness();
  }

  internal sealed class AndroidBezierPathBuilder : Inv.BezierPathBuilder<Android.Graphics.Path>
  {
    public AndroidBezierPathBuilder(Android.Graphics.Path Path)
    {
      this.Path = Path;
      Path.Reset();
    }

    public override void MoveTo(Inv.BezierPoint Point)
    {
      Path.MoveTo(Point.X, Point.Y);
    }
    public override void AddLineTo(Inv.BezierPoint Point)
    {
      Path.LineTo(Point.X, Point.Y);
    }
    public override void AddCurveTo(Inv.BezierPoint EndPoint, Inv.BezierPoint ControlPoint1, Inv.BezierPoint ControlPoint2)
    {
      Path.CubicTo(ControlPoint1.X, ControlPoint1.Y, ControlPoint2.X, ControlPoint2.Y, EndPoint.X, EndPoint.Y);
    }
    public override void Close()
    {
      Path.Close();
    }
    public override Android.Graphics.Path Render()
    {
      return Path;
    }

    private readonly Android.Graphics.Path Path;
  }

  public abstract class AndroidGroup : Android.Views.ViewGroup
  {
    internal AndroidGroup(Android.Content.Context context)
      : base(context)
    {
      this.SetClipChildren(false);
      this.SetWillNotDraw(false);

      this.Background = new AndroidBackground(this);
      //this.MotionEventSplittingEnabled = false;
    }

    public new AndroidBackground Background { get; }

    public override bool OnTouchEvent(Android.Views.MotionEvent Event)
    {
      if ((Background.GetColor() ?? Android.Graphics.Color.Transparent) == Android.Graphics.Color.Transparent)
        return base.OnTouchEvent(Event);
      else
        return this.PassthroughTouchEvent();
    }
    protected override void OnDraw(Android.Graphics.Canvas canvas)
    {
      base.OnDraw(canvas);

      Background.Draw(canvas);
    }
  }

  internal sealed class AndroidTranslateAnimation : Android.Views.Animations.TranslateAnimation, Android.Views.Animations.Animation.IAnimationListener
  {
    public AndroidTranslateAnimation(float fromXDelta, float toXDelta, float fromYDelta, float toYDelta)
      : base(fromXDelta, toXDelta, fromYDelta, toYDelta)
    {
      SetAnimationListener(this);
    }

    // These events should be used instead of AnimationStart/End.
    // AnimationStart/End events cause a global reference to be instantiated which can lead to weak reference global table crashes.
    public event Action StartEvent;
    public event Action EndEvent;

    void Android.Views.Animations.Animation.IAnimationListener.OnAnimationStart(Android.Views.Animations.Animation animation)
    {
      if (StartEvent != null)
        StartEvent();
    }
    void Android.Views.Animations.Animation.IAnimationListener.OnAnimationEnd(Android.Views.Animations.Animation animation)
    {
      if (EndEvent != null)
        EndEvent();
    }
    void Android.Views.Animations.Animation.IAnimationListener.OnAnimationRepeat(Android.Views.Animations.Animation animation)
    {
    }
  }

  internal sealed class AndroidRotateAnimation : Android.Views.Animations.RotateAnimation, Android.Views.Animations.Animation.IAnimationListener
  {
    public AndroidRotateAnimation(float fromAngle, float toAngle)
      : base(fromAngle, toAngle, Android.Views.Animations.Dimension.RelativeToSelf, 0.5F, Android.Views.Animations.Dimension.RelativeToSelf, 0.5F)
    {
      SetAnimationListener(this);
    }

    // These events should be used instead of AnimationStart/End.
    // AnimationStart/End events cause a global reference to be instantiated which can lead to weak reference global table crashes.
    public event Action StartEvent;
    public event Action EndEvent;

    void Android.Views.Animations.Animation.IAnimationListener.OnAnimationStart(Android.Views.Animations.Animation animation)
    {
      if (StartEvent != null)
        StartEvent();
    }
    void Android.Views.Animations.Animation.IAnimationListener.OnAnimationEnd(Android.Views.Animations.Animation animation)
    {
      if (EndEvent != null)
        EndEvent();
    }
    void Android.Views.Animations.Animation.IAnimationListener.OnAnimationRepeat(Android.Views.Animations.Animation animation)
    {
    }
  }

  internal sealed class AndroidScaleAnimation : Android.Views.Animations.ScaleAnimation, Android.Views.Animations.Animation.IAnimationListener
  {
    public AndroidScaleAnimation(float fromX, float toX, float fromY, float toY)
      : base(fromX, toX, fromY, toY, Android.Views.Animations.Dimension.RelativeToSelf, 0.5F, Android.Views.Animations.Dimension.RelativeToSelf, 0.5F)
    {
      SetAnimationListener(this);
    }

    // These events should be used instead of AnimationStart/End.
    // AnimationStart/End events cause a global reference to be instantiated which can lead to weak reference global table crashes.
    public event Action StartEvent;
    public event Action EndEvent;

    void Android.Views.Animations.Animation.IAnimationListener.OnAnimationStart(Android.Views.Animations.Animation animation)
    {
      if (StartEvent != null)
        StartEvent();
    }
    void Android.Views.Animations.Animation.IAnimationListener.OnAnimationEnd(Android.Views.Animations.Animation animation)
    {
      if (EndEvent != null)
        EndEvent();
    }
    void Android.Views.Animations.Animation.IAnimationListener.OnAnimationRepeat(Android.Views.Animations.Animation animation)
    {
    }
  }

  internal sealed class AndroidAlphaAnimation : Android.Views.Animations.AlphaAnimation, Android.Views.Animations.Animation.IAnimationListener
  {
    public AndroidAlphaAnimation(float fromAlpha, float toAlpha)
      : base(fromAlpha, toAlpha)
    {
      SetAnimationListener(this);
    }

    public event Action StartEvent;
    public event Action EndEvent;

    void Android.Views.Animations.Animation.IAnimationListener.OnAnimationStart(Android.Views.Animations.Animation animation)
    {
      if (StartEvent != null)
        StartEvent();
    }
    void Android.Views.Animations.Animation.IAnimationListener.OnAnimationEnd(Android.Views.Animations.Animation animation)
    {
      if (EndEvent != null)
        EndEvent();
    }
    void Android.Views.Animations.Animation.IAnimationListener.OnAnimationRepeat(Android.Views.Animations.Animation animation)
    {
    }
  }

  internal static class AndroidFoundation
  {
    static AndroidFoundation()
    {
      SdkVersion = (int)Android.OS.Build.VERSION.SdkInt;
#if DEBUG
      //SdkVersion = 19; // for compatibility testing.
#endif
    }

    public static readonly int SdkVersion;

    public static void SafeAddView(this Android.Views.ViewGroup AndroidViewGroup, Android.Views.View ElementView)
    {
      if (ElementView.Parent == null)
      {
        AndroidViewGroup.AddView(ElementView);
      }
      else
      {
        var ViewGroup = ElementView.Parent as Android.Views.ViewGroup;

        if (ViewGroup != AndroidViewGroup)
        {
          ViewGroup.RemoveView(ElementView);
          AndroidViewGroup.AddView(ElementView);
        }
      }
    }
    public static void SafeRemoveView(this Android.Views.ViewGroup AndroidViewGroup, Android.Views.View ElementView)
    {
      if (ElementView.Parent == AndroidViewGroup)
        AndroidViewGroup.RemoveView(ElementView);
    }
    public static void Arrange(this Android.Views.View AndroidView)
    {
      // NOTE: doesn't seem to need to request layout all the way up the parent views.
      AndroidView.RequestLayout();

      //var AndroidCurrent = AndroidView;
      //while (AndroidCurrent != null)
      //{
      //  AndroidCurrent.RequestLayout();
      //  //AndroidCurrent.PostInvalidate();
      //  //AndroidCurrent.Invalidate();
      //  //AndroidCurrent.ForceLayout();
      //
      //  AndroidCurrent = AndroidCurrent.Parent as View;
      //}
    }
    public static void LayoutChild(this Android.Views.View AndroidView, int l, int t, int r, int b)
    {
#if DEBUG
      //if (l < 0 || t < 0 || l > r || t > b)
      //  System.Diagnostics.Debug.WriteLine("Layout incorrect (" + l + ", " + t + ", " + r + ", " + b + ")");
#endif

      AndroidView.Layout(l, t, r, b);
    }
    public static void MeasureOnlyChild(this Android.Views.View AndroidView, Android.Views.View ContentElement, int WidthMeasureSpec, int HeightMeasureSpec, out int MeasuredWidth, out int MeasuredHeight)
    {
      var IdealWidth = AndroidView.PaddingLeft + AndroidView.PaddingRight;
      var IdealHeight = AndroidView.PaddingTop + AndroidView.PaddingBottom;

      var WidthSize = Android.Views.View.MeasureSpec.GetSize(WidthMeasureSpec);
      var WidthMode = Android.Views.View.MeasureSpec.GetMode(WidthMeasureSpec);
      var HeightSize = Android.Views.View.MeasureSpec.GetSize(HeightMeasureSpec);
      var HeightMode = Android.Views.View.MeasureSpec.GetMode(HeightMeasureSpec);

      if (ContentElement != null)
      {
        var ChildWidthSize = Math.Max(0, WidthSize - AndroidView.PaddingLeft - AndroidView.PaddingRight);
        var ChildWidthSpec = Android.Views.View.MeasureSpec.MakeMeasureSpec(ChildWidthSize, WidthMode);
        var ChildHeightSize = Math.Max(0, HeightSize - AndroidView.PaddingTop - AndroidView.PaddingBottom);
        var ChildHeightSpec = Android.Views.View.MeasureSpec.MakeMeasureSpec(ChildHeightSize, HeightMode);

#if MINIMUM_MEASURE
        // pass-through minimum width/height.
        ContentElement.SetMinimumWidth(AndroidView.MinimumWidth);
        ContentElement.SetMinimumHeight(AndroidView.MinimumHeight);
#endif
        ContentElement.Measure(ChildWidthSpec, ChildHeightSpec);

        IdealWidth += ContentElement.MeasuredWidth;
        IdealHeight += ContentElement.MeasuredHeight;
      }
#if MINIMUM_MEASURE
      else
      {
        // TODO: does this include or exclude padding?
        if (IdealWidth < AndroidView.MinimumWidth)
          IdealWidth = AndroidView.MinimumWidth;

        if (IdealHeight < AndroidView.MinimumHeight)
          IdealHeight = AndroidView.MinimumHeight;
      }
#endif

      switch (WidthMode)
      {
        case Android.Views.MeasureSpecMode.AtMost:
          MeasuredWidth = Math.Min(IdealWidth, WidthSize);
          break;

        case Android.Views.MeasureSpecMode.Exactly:
          MeasuredWidth = WidthSize;
          break;

        case Android.Views.MeasureSpecMode.Unspecified:
          MeasuredWidth = IdealWidth;
          break;

        default:
          throw new UnhandledEnumCaseException<Android.Views.MeasureSpecMode>();
      }

      switch (HeightMode)
      {
        case Android.Views.MeasureSpecMode.AtMost:
          MeasuredHeight = Math.Min(IdealHeight, HeightSize);
          break;

        case Android.Views.MeasureSpecMode.Exactly:
          MeasuredHeight = HeightSize;
          break;

        case Android.Views.MeasureSpecMode.Unspecified:
          MeasuredHeight = IdealHeight;
          break;

        default:
          throw new UnhandledEnumCaseException<Android.Views.MeasureSpecMode>();
      }
    }
    public static void LayoutOnlyChild(this Android.Views.View AndroidView, Android.Views.View ContentElement, int Left, int Top, int Right, int Bottom)
    {
      if (ContentElement != null)
      {
        // NOTE: child should always match the measurement of the parent (as it is a layout container).
        //System.Diagnostics.Debug.Assert(Right - Left == ContentElement.MeasuredWidth - AndroidView.PaddingLeft - AndroidView.PaddingRight);
        //System.Diagnostics.Debug.Assert(Bottom - Top == ContentElement.MeasuredHeight - AndroidView.PaddingTop - AndroidView.PaddingBottom);

        ContentElement.LayoutChild(AndroidView.PaddingLeft, AndroidView.PaddingTop, AndroidView.PaddingLeft + ContentElement.MeasuredWidth, AndroidView.PaddingTop + ContentElement.MeasuredHeight);
      }
    }
    public static IEnumerable<Android.Views.ViewGroup> EnumerateParents(this Android.Views.View AndroidView)
    {
      while (AndroidView.Parent != null && AndroidView.Parent is Android.Views.ViewGroup)
      {
        var Parent = AndroidView.Parent as Android.Views.ViewGroup;
        AndroidView = Parent;
        yield return Parent;
      }
    }
    public static string ChildHierarchyToString(this Android.App.Activity Activity)
    {
      var Writer = new HierarchyWriter<Android.Views.View>(GetChildCount, EnumerateChildren, ViewToString);
      return Writer.WriteToString(Activity.Window.DecorView);
    }
    public static string ChildHierarchyToString(this Android.Views.View RootView)
    {
      var Writer = new HierarchyWriter<Android.Views.View>(GetChildCount, EnumerateChildren, ViewToString);
      return Writer.WriteToString(RootView);
    }
    public static bool PassthroughTouchEvent(this Android.Views.View View)
    {
      var Current = View.Parent;

      while (Current != null)
      {
        if (Current is AndroidButton)
          return false;

        Current = Current.Parent;
      }

      return true;
    }

    private static int GetChildCount(Android.Views.View View)
    {
      var Group = View as Android.Views.ViewGroup;
      if (Group == null)
        return 0;
      else
        return Group.ChildCount;
    }
    private static IEnumerable<Android.Views.View> EnumerateChildren(Android.Views.View View)
    {
      var Group = View as Android.Views.ViewGroup;

      if (Group != null)
      {
        var Index = 0;
        while (Index < Group.ChildCount)
          yield return Group.GetChildAt(Index++);
      }
    }
    private static string ViewToString(Android.Views.View View)
    {
      var Properties = new Dictionary<string, string>();
      Properties.Add("frame", $"({View.Left} {View.Top}; {View.Right - View.Left} {View.Bottom - View.Top})");

      switch (View)
      {
        case Android.Widget.TextView TextView:
          Properties.Add("text", $"\"{TextView.Text}\"");
          break;

        case Inv.AndroidDock Dock:
          Properties.Add("orientation", Dock.Orientation.ToString());
          break;
      }

      return $"<{View.GetType().FullName}; {string.Join("; ", Properties.Select(P => $"{P.Key} = {P.Value}"))}>";
    }
  }

  public static class AndroidInstrumentation
  {
    [Conditional("DEBUG")]
    public static void Clear()
    {
#if PERFORMANCE
      foreach (var Performance in PerformanceArray)
        Performance.Clear();
#endif
    }
    [Conditional("DEBUG")]
    public static void Print()
    {
#if PERFORMANCE
      Debug.WriteLine("**PERFORMANCE REPORT**");

      foreach (var Performance in PerformanceArray.OrderBy(P => P.Name))
      {
        if (Performance.MeasureCount > 0)
        {
          var MeasureElapsedMilliseconds = TimeSpan.FromTicks(Performance.MeasureElapsedTicks).TotalMilliseconds;

          Debug.WriteLine($"**{Performance.Name.ToUpper()}**MEASURE x{Performance.MeasureCount} = {(int)MeasureElapsedMilliseconds} ms (avg ~{(int)(MeasureElapsedMilliseconds / Performance.MeasureCount)} ms)");
        }

        if (Performance.LayoutCount > 0)
        {
          var LayoutElapsedMilliseconds = TimeSpan.FromTicks(Performance.LayoutElapsedTicks).TotalMilliseconds;

          Debug.WriteLine($"**{Performance.Name.ToUpper()}**LAYOUT x{Performance.LayoutCount} = {(int)LayoutElapsedMilliseconds} ms (avg ~{(int)(LayoutElapsedMilliseconds / Performance.LayoutCount)} ms)");
        }
      }

      Debug.WriteLine("**********************");
#endif
    }

    internal static AndroidStopwatch NewStopwatch(this AndroidPerformance Performance)
    {
#if PERFORMANCE
      return new AndroidStopwatch(Performance);
#else
      return null;
#endif
    }
    [Conditional("DEBUG")]
    internal static void StartMeasure(this AndroidStopwatch SW)
    {
#if PERFORMANCE
      AccumulateStack.Push(new AndroidAccumulate());

      SW.Start();
#endif
    }
    [Conditional("DEBUG")]
    internal static void StopMeasure(this AndroidStopwatch SW)
    {
#if PERFORMANCE
      SW.Stop();

      var Result = AccumulateStack.Pop();
      if (AccumulateStack.Count > 0)
        AccumulateStack.Peek().MeasureElapsedTicks += SW.ElapsedTicks;

      SW.Performance.MeasureCount++;
      SW.Performance.MeasureElapsedTicks += SW.ElapsedTicks - Result.MeasureElapsedTicks;
#endif
    }
    [Conditional("DEBUG")]
    internal static void StartLayout(this AndroidStopwatch SW)
    {
#if PERFORMANCE
      AccumulateStack.Push(new AndroidAccumulate());

      SW.Start();
#endif
    }
    [Conditional("DEBUG")]
    internal static void StopLayout(this AndroidStopwatch SW)
    {
#if PERFORMANCE
      SW.Stop();

      var Result = AccumulateStack.Pop();
      if (AccumulateStack.Count > 0)
        AccumulateStack.Peek().LayoutElapsedTicks += SW.ElapsedTicks;

      SW.Performance.LayoutCount++;
      SW.Performance.LayoutElapsedTicks += SW.ElapsedTicks - Result.LayoutElapsedTicks;
#endif
    }

    internal static readonly AndroidPerformance BlockPerformance = NewPerformance("block");
    internal static readonly AndroidPerformance BoardPerformance = NewPerformance("board");
    internal static readonly AndroidPerformance BrowserPerformance = NewPerformance("browser");
    internal static readonly AndroidPerformance ButtonPerformance = NewPerformance("button");
    internal static readonly AndroidPerformance CanvasPerformance = NewPerformance("canvas");
    internal static readonly AndroidPerformance ContainerPerformance = NewPerformance("container");
    internal static readonly AndroidPerformance DockPerformance = NewPerformance("dock");
    internal static readonly AndroidPerformance EditPerformance = NewPerformance("edit");
    internal static readonly AndroidPerformance FlowPerformance = NewPerformance("flow");
    internal static readonly AndroidPerformance FramePerformance = NewPerformance("frame");
    internal static readonly AndroidPerformance LabelPerformance = NewPerformance("label");
    internal static readonly AndroidPerformance GraphicPerformance = NewPerformance("graphic");
    internal static readonly AndroidPerformance MemoPerformance = NewPerformance("memo");
    internal static readonly AndroidPerformance NativePerformance = NewPerformance("native");
    internal static readonly AndroidPerformance OverlayPerformance = NewPerformance("overlay");
    internal static readonly AndroidPerformance SearchPerformance = NewPerformance("search");
    internal static readonly AndroidPerformance ScrollPerformance = NewPerformance("scroll");
    internal static readonly AndroidPerformance StackPerformance = NewPerformance("stack");
    internal static readonly AndroidPerformance SwitchPerformance = NewPerformance("switch");
    internal static readonly AndroidPerformance SurfacePerformance = NewPerformance("surface");
    internal static readonly AndroidPerformance TablePerformance = NewPerformance("table");
    internal static readonly AndroidPerformance VideoPerformance = NewPerformance("video");
    internal static readonly AndroidPerformance WrapPerformance = NewPerformance("wrap");

#if PERFORMANCE
    private static readonly Stack<AndroidAccumulate> AccumulateStack = new Stack<AndroidAccumulate>();

    private static readonly AndroidPerformance[] PerformanceArray = new AndroidPerformance[]
    {
      BoardPerformance,
      BrowserPerformance,
      ButtonPerformance,
      CanvasPerformance,
      ContainerPerformance,
      DockPerformance,
      EditPerformance,
      FlowPerformance,
      FramePerformance,
      GraphicPerformance,
      LabelPerformance,
      MemoPerformance,
      NativePerformance,
      OverlayPerformance,
      SearchPerformance,
      ScrollPerformance,
      StackPerformance,
      SwitchPerformance,
      SurfacePerformance,
      TablePerformance,
      VideoPerformance,
      WrapPerformance
    };
#endif

    private static AndroidPerformance NewPerformance(string Name)
    {
#if PERFORMANCE
      return new AndroidPerformance(Name);
#else
      return null;
#endif
    }
  }

#if PERFORMANCE
  internal sealed class AndroidAccumulate
  {
    public long MeasureElapsedTicks;
    public long LayoutElapsedTicks;
  }
#endif

  internal sealed class AndroidPerformance
  {
    public AndroidPerformance(string Name)
    {
      this.Name = Name;
    }

    public string Name { get; }
    public int MeasureCount;
    public long MeasureElapsedTicks;
    public int LayoutCount;
    public long LayoutElapsedTicks;

    public void Clear()
    {
      this.MeasureCount = 0;
      this.MeasureElapsedTicks = 0;
      this.LayoutCount = 0;
      this.LayoutElapsedTicks = 0;
    }
  }

  internal sealed class AndroidStopwatch
  {
    public AndroidStopwatch(AndroidPerformance Performance)
    {
      this.Base = new System.Diagnostics.Stopwatch();
      this.Performance = Performance;
    }

    public AndroidPerformance Performance { get; }
    public long ElapsedTicks => Base.ElapsedTicks;

    public void Start()
    {
      Debug.Assert(!Base.IsRunning);

      Base.Restart();
    }
    public void Stop()
    {
      Debug.Assert(Base.IsRunning);

      Base.Stop();
    }

    private readonly System.Diagnostics.Stopwatch Base;
  }

  internal sealed class UnhandledEnumCaseException<TEnum> : Exception
    where TEnum : struct
  {
    public UnhandledEnumCaseException()
      : base(string.Format("Unhandled enum case for {0}", typeof(TEnum).FullName))
    {
    }
  }
}