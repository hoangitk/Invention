﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  public sealed class BuildScript
  {
    public BuildScript(string Title)
    {
      this.Title = Title;
      this.ScriptList = new DistinctList<BuildProcedure>();
      this.ProcedureSet = new HashSet<BuildProcedure>();
      this.TaskSet = new HashSet<BuildTask>();
      this.StepSet = new HashSet<BuildStep>();
    }

    public string Title { get; }
    public HashSet<BuildProcedure> ProcedureSet { get; }
    public HashSet<BuildTask> TaskSet { get; }
    public HashSet<BuildStep> StepSet { get; }

    public BuildProcedure AddProcedure(string Name)
    {
      var Result = new BuildProcedure(Name);

      ScriptList.Add(Result);

      return Result;
    }
    public IEnumerable<BuildProcedure> GetProcedures()
    {
      return ScriptList;
    }
    public BuildSolution SelectSolution(string FilePath)
    {
      return new BuildSolution(FilePath);
    }
    public BuildProject SelectProject(string FilePath)
    {
      return new BuildProject(FilePath);
    }
    public BuildTimestamp SelectTimestamp(string FilePath)
    {
      return new BuildTimestamp(FilePath);
    }
    public BuildNugetVersion SelectNugetVersion(string FilePath)
    {
      return new BuildNugetVersion(FilePath);
    }
    public BuildMacServer SelectMacServer(string Address, string Username, string Password)
    {
      return new BuildMacServer(Address, Username, Password);
    }

    private readonly Inv.DistinctList<BuildProcedure> ScriptList;
  }

  public sealed class BuildSolution
  {
    internal BuildSolution(string FilePath)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(string.Equals(System.IO.Path.GetExtension(FilePath), ".sln", StringComparison.InvariantCultureIgnoreCase), "Solution file must be a .sln: {0}", FilePath);
        Inv.Assert.Check(System.IO.File.Exists(FilePath), "Solution file must exist: " + FilePath);
      }

      this.FilePath = FilePath;
    }

    public string FilePath { get; set; }

    public BuildTarget SelectTarget(params string[] PathArray)
    {
      return new BuildTarget(this, PathArray);
    }
  }

  public sealed class BuildTarget
  {
    internal BuildTarget(BuildSolution Solution, string[] PathArray)
    {
      this.Solution = Solution;
      this.PathArray = PathArray;
    }

    public BuildSolution Solution { get; }
    public string[] PathArray { get; }

    public BuildTarget SelectTarget(params string[] PathArray)
    {
      return new BuildTarget(Solution, this.PathArray.Union(PathArray).ToArray());
    }
  }

  public sealed class BuildProject
  {
    internal BuildProject(string FilePath)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(string.Equals(System.IO.Path.GetExtension(FilePath), ".csproj", StringComparison.InvariantCultureIgnoreCase), "Project file must be a .csproj: {0}", FilePath);
        Inv.Assert.Check(System.IO.File.Exists(FilePath), "Project file must exist: " + FilePath);
      }

      this.FilePath = FilePath;
    }

    public string FilePath { get; set; }

    public BuildAndroidManifest SelectAndroidManifest()
    {
      return new BuildAndroidManifest(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(FilePath), "Properties", "AndroidManifest.xml"));
    }
    public BuildiOSManifest SelectiOSManifest()
    {
      return new BuildiOSManifest(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(FilePath), "Info.plist"));
    }
    public BuildUwaManifest SelectUwaManifest()
    {
      return new BuildUwaManifest(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(FilePath), "Package.appxmanifest"));
    }
    public BuildVsixManifest SelectVsixManifest()
    {
      return new BuildVsixManifest(System.IO.Path.Combine(System.IO.Path.GetDirectoryName(FilePath), "source.extension.vsixmanifest"));
    }
  }

  public enum BuildConfiguration
  {
    Debug,
    Release,
    Adhoc
  }

  public enum BuildPlatform
  {
    x86,
    x64,
    AnyCPU,
    iPhone,
    ARM
  }

  public sealed class BuildMacServer
  {
    internal BuildMacServer(string Address, string Username, string Password)
    {
      this.Address = Address;
      this.Username = Username;
      this.Password = Password;
    }

    public string Address { get; }
    public string Username { get; }
    public string Password { get; }
  }

  public sealed class BuildTimestamp
  {
    internal BuildTimestamp(string FilePath)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(string.Equals(System.IO.Path.GetExtension(FilePath), ".txt", StringComparison.InvariantCultureIgnoreCase), "Timestamp file must be a .txt: {0}", FilePath);
        Inv.Assert.Check(System.IO.File.Exists(FilePath), "Timestamp file must exist: " + FilePath);
      }

      this.FilePath = FilePath;
    }

    public char Prefix { get; internal set; }
    public string Version { get; internal set; }

    internal string FilePath { get; }
  }

  public sealed class BuildNugetVersion
  {
    internal BuildNugetVersion(string FilePath)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(string.Equals(System.IO.Path.GetExtension(FilePath), ".txt", StringComparison.InvariantCultureIgnoreCase), "Nuget version file must be a .txt: {0}", FilePath);
        Inv.Assert.Check(System.IO.File.Exists(FilePath), "Nuget version file must exist: {0}", FilePath);
      }

      this.FilePath = FilePath;
    }

    public string Text { get; internal set; }

    internal string FilePath { get; }
  }

  public sealed class BuildAndroidManifest
  {
    internal BuildAndroidManifest(string FilePath)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(string.Equals(System.IO.Path.GetExtension(FilePath), ".xml", StringComparison.InvariantCultureIgnoreCase), "Android manifest must be a .xml: {0}", FilePath);
        Inv.Assert.Check(System.IO.File.Exists(FilePath), "Android manifest file must exist: {0}", FilePath);
      }

      this.FilePath = FilePath;
    }

    public string Identifier { get; internal set; }
    public string Version { get; internal set; }

    internal string FilePath { get; }
  }

  public sealed class BuildiOSManifest
  {
    internal BuildiOSManifest(string FilePath)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(string.Equals(System.IO.Path.GetExtension(FilePath), ".plist", StringComparison.InvariantCultureIgnoreCase), "iOS manifest must be a .plist: {0}", FilePath);
        Inv.Assert.Check(System.IO.File.Exists(FilePath), "iOS manifest file must exist: {0}", FilePath);
      }

      this.FilePath = FilePath;
    }

    // TODO: This isn't set correctly unless the version is incremented.
    public string Version { get; internal set; }

    internal string FilePath { get; }
  }

  public sealed class BuildUwaManifest
  {
    internal BuildUwaManifest(string FilePath)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(string.Equals(System.IO.Path.GetExtension(FilePath), ".appxmanifest", StringComparison.InvariantCultureIgnoreCase), "Uwa manifest must be a .appxmanifest: {0}", FilePath);
        Inv.Assert.Check(System.IO.File.Exists(FilePath), "Uwa manifest file must exist: " + FilePath);
      }

      this.FilePath = FilePath;
    }

    public string Version { get; internal set; }

    internal string FilePath { get; }
  }

  public sealed class BuildVsixManifest
  {
    internal BuildVsixManifest(string FilePath)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(string.Equals(System.IO.Path.GetExtension(FilePath), ".vsixmanifest", StringComparison.InvariantCultureIgnoreCase), "Vsix manifest must be a .vsixmanifest: {0}", FilePath);
        Inv.Assert.Check(System.IO.File.Exists(FilePath), "Vsix manifest file must exist: " + FilePath);
      }

      this.FilePath = FilePath;
    }

    public string Version { get; internal set; }

    internal string FilePath { get; }
  }

  public sealed class BuildProcedure
  {
    internal BuildProcedure(string Name)
    {
      this.Name = Name;
      this.TaskList = new DistinctList<BuildTask>();
    }

    public string Name { get; }

    public BuildTask AddTask(string Name, Action<BuildTask> Action = null)
    {
      var Result = new BuildTask(Name);

      TaskList.Add(Result);

      if (Action != null)
        Action(Result);

      return Result;
    }

    internal IEnumerable<BuildTask> GetTasks()
    {
      return TaskList;
    }

    private readonly Inv.DistinctList<BuildTask> TaskList;
  }

  public sealed class BuildTask
  {
    internal BuildTask(string Name)
    {
      this.Name = Name;
      this.StepList = new DistinctList<BuildStep>();
    }

    public string Name { get; }

    public void ApplyTimestamp(char WritePrefix, BuildTimestamp Timestamp)
    {
      AddStep("Apply Timestamp", WritePrefix + " yyyy.MMdd.HHmm", Context =>
      {
        Timestamp.Prefix = WritePrefix;
        Timestamp.Version = DateTime.Now.ToString("yyyy.MMdd.HHmm");

        if (System.IO.File.Exists(Timestamp.FilePath))
          Inv.Support.FileHelper.SetReadOnly(Timestamp.FilePath, false);

        System.IO.File.WriteAllText(Timestamp.FilePath, Timestamp.Prefix + Timestamp.Version);
      });
    }
    public void IncrementNugetVersion(BuildNugetVersion Version)
    {
      AddStep("Increment Nuget Version", System.IO.Path.GetFileName(Version.FilePath), Context =>
      {
        Version.Text = System.IO.File.ReadAllText(Version.FilePath).Trim();

        var VersionArray = Version.Text.Split('.');
        var VersionCode = VersionArray[VersionArray.Length - 1];

        if (int.TryParse(VersionCode, out int VersionNumber))
        {
          VersionNumber++;
          VersionArray[VersionArray.Length - 1] = VersionNumber.ToString();
          Version.Text = VersionArray.AsSeparatedText(".");

          Inv.Support.FileHelper.SetReadOnly(Version.FilePath, false);
          System.IO.File.WriteAllText(Version.FilePath, Version.Text);
        }
      });
    }
    public void CleanTarget(BuildTarget Target, BuildConfiguration Configuration, BuildPlatform Platform)
    {
      AddStep("Clean Target", Target.PathArray.AsSeparatedText(@"\"), Context =>
      {
        var MSBuildArguments =
          string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:{3}:Clean", Target.Solution.FilePath, GetArgument(Configuration), GetArgument(Platform), Target.PathArray.AsSeparatedText(@"\"));

        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
      });
    }
    public void BuildTarget(BuildTarget Target, BuildConfiguration Configuration, BuildPlatform Platform)
    {
      AddStep("Build Target", Target.PathArray.AsSeparatedText(@"\"), Context =>
      {
        var MSBuildArguments =
          string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:{3}", Target.Solution.FilePath, GetArgument(Configuration), GetArgument(Platform), Target.PathArray.AsSeparatedText(@"\"));

        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
      });
    }
    public void GenerateResources(BuildProject Project)
    {
      AddStep("Generate Resources", System.IO.Path.GetFileNameWithoutExtension(Project.FilePath), Context =>
      {
        Inv.GenShell.Execute(Project.FilePath);
      });
    }
    public void CleanAndroidTarget(BuildTarget Target, BuildConfiguration Configuration, BuildPlatform Platform)
    {
      AddStep("Clean Android Target", Target.PathArray.AsSeparatedText(@"\"), Context =>
      {
        var MSBuildArguments =
          string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:{3}:Clean", Target.Solution.FilePath, GetArgument(Configuration), GetArgument(Platform), Target.PathArray.AsSeparatedText(@"\"));

        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
      });
    }
    public void BuildAndroidTarget(BuildTarget Target, BuildConfiguration Configuration, BuildPlatform Platform)
    {
      AddStep("Build Android Target", Target.PathArray.AsSeparatedText(@"\"), Context =>
      {
        var MSBuildArguments =
          string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:{3}", Target.Solution.FilePath, GetArgument(Configuration), GetArgument(Platform), Target.PathArray.AsSeparatedText(@"\"));

        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
      });
    }
    public void IncrementAndroidVersion(BuildAndroidManifest Manifest)
    {
      AddStep("Increment Android Version", System.IO.Path.GetFileName(Manifest.FilePath), Context =>
      {
        var AndroidManifest = new AndroidManifestFile(Manifest.FilePath);

        if (AndroidManifest.IncrementVersionCode())
          AndroidManifest.Save();

        Manifest.Identifier = AndroidManifest.PackageIdentifier;
        Manifest.Version = AndroidManifest.VersionCode;
      });
    }
    public void SignAndroidProject(BuildProject Project, BuildConfiguration Configuration, BuildPlatform Platform)
    {
      AddStep("Sign Android Project", System.IO.Path.GetFileNameWithoutExtension(Project.FilePath), Context =>
      {
        var MSBuildArguments =
          string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:SignAndroidPackage", Project.FilePath, GetArgument(Configuration), GetArgument(Platform).StripWhitespace());

        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
      });
    }
    public void DeployAndroidPackage(BuildProject Project, BuildConfiguration Configuration, BuildPlatform Platform, string TargetPath, BuildAndroidManifest Manifest)
    {
      AddStep("Deploy Android Apk", TargetPath + "-v.apk", Context =>
      {
        System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(TargetPath));

        var SourcePath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Project.FilePath), "bin", GetArgument(Configuration), Platform == BuildPlatform.AnyCPU ? "" : GetArgument(Platform), Manifest.Identifier);

        System.IO.File.Copy(SourcePath + "-Signed.apk", TargetPath + "-" + Manifest.Version + ".apk", true);
      });
    }
    public void IncrementiOSVersion(BuildiOSManifest Manifest)
    {
      AddStep("Increment iOS Version", System.IO.Path.GetFileName(Manifest.FilePath), Context =>
      {
        var iOSManifest = new iOSManifestFile(Manifest.FilePath);

        if (iOSManifest.IncrementBundleVersion())
          iOSManifest.Save();

        Manifest.Version = iOSManifest.BundleVersion;
      });
    }
    public void CleaniOSTarget(BuildTarget Target, BuildConfiguration Configuration, BuildPlatform Platform, BuildMacServer MacServer)
    {
      AddStep("Clean iOS Target", Target.PathArray.AsSeparatedText(@"\"), Context =>
      {
        var MSBuildArguments =
          string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:{3}:Clean /p:ServerAddress={4};ServerUser={5};ServerPassword={6}",
          Target.Solution.FilePath, GetArgument(Configuration), GetArgument(Platform), Target.PathArray.AsSeparatedText(@"\"), MacServer.Address, MacServer.Username, MacServer.Password);

        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
      });
    }
    public void BuildiOSTarget(BuildTarget Target, BuildConfiguration Configuration, BuildPlatform Platform, BuildMacServer MacServer)
    {
      AddStep("Build iOS Target", Target.PathArray.AsSeparatedText(@"\"), Context =>
      {
        var MSBuildArguments =
          string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:{3} /p:ServerAddress={4};ServerUser={5};ServerPassword={6}",
          Target.Solution.FilePath, GetArgument(Configuration), GetArgument(Platform), Target.PathArray.AsSeparatedText(@"\"), MacServer.Address, MacServer.Username, MacServer.Password);

        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
      });
    }
    public void DeployiOSPackage(BuildProject Project, BuildConfiguration Configuration, BuildPlatform Platform, string TargetPath, BuildiOSManifest Manifest)
    {
      AddStep("Deploy iOS Package", TargetPath + "-v.ipa", Context =>
      {
        System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(TargetPath));

        var SourcePath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Project.FilePath), "bin", GetArgument(Platform), GetArgument(Configuration), System.IO.Path.GetFileNameWithoutExtension(Project.FilePath));

        System.IO.File.Copy(SourcePath + ".ipa", TargetPath + "-" + Manifest.Version + ".ipa", true);
      });
    }
    public void DeployiOSdSYM(BuildProject Project, BuildConfiguration Configuration, BuildPlatform Platform, string TargetPath, BuildiOSManifest Manifest)
    {
      AddStep("Deploy iOS dSYM", TargetPath + "-v.app.dSYM.zip", Context =>
      {
        System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(TargetPath));

        var SourcePath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Project.FilePath), "bin", GetArgument(Platform), GetArgument(Configuration), System.IO.Path.GetFileNameWithoutExtension(Project.FilePath));

        ExecuteProcess(Context, SevenZipPath, "a \"" + TargetPath + "-" + Manifest.Version + ".app.dSYM.zip\" \"" + SourcePath + ".app.dSYM\\Contents\"");
      });
    }
    public void IncrementUwaVersion(BuildUwaManifest Manifest)
    {
      AddStep("Increment Uwa Version", System.IO.Path.GetFileName(Manifest.FilePath), Context =>
      {
        var UwpManifest = new UwpManifestFile(Manifest.FilePath);

        if (UwpManifest.IncrementVersionBuild())
          UwpManifest.Save();

        Manifest.Version = UwpManifest.Version;
      });
    }
    public void CleanUwaTarget(BuildTarget Target, BuildConfiguration Configuration, BuildPlatform[] PlatformArray)
    {
      AddStep("Clean Uwa Target", Target.PathArray.AsSeparatedText(@"\"), Context =>
      {
        foreach (var CleanPlatform in PlatformArray)
        {
          var MSBuildArguments =
            string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:{3}:Clean",
            Target.Solution.FilePath, GetArgument(Configuration), GetArgument(CleanPlatform), Target.PathArray.AsSeparatedText(@"\"));

          ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
        }
      });
    }
    public void BuildUwaTarget(BuildTarget Target, BuildConfiguration Configuration, BuildPlatform[] PlatformArray)
    {
      AddStep("Build Uwa Target", Target.PathArray.AsSeparatedText(@"\"), Context =>
      {
        var MSBuildArguments =
          string.Format("\"{0}\" /p:Configuration={1};AppxBundle=Always;AppxBundlePlatforms=\"{2}\";BuildAppxUploadPackageForUap=true /t:{3}",
          Target.Solution.FilePath, GetArgument(Configuration), PlatformArray.Select(P => GetArgument(P)).AsSeparatedText("|"), Target.PathArray.AsSeparatedText(@"\"));

        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
      });
    }
    public void DeployUwaPackage(BuildProject Project, BuildPlatform[] PlatformArray, string TargetPath, BuildUwaManifest Manifest)
    {
      AddStep("Deploy Uwa Package", TargetPath + "-v.appxupload", Context =>
      {
        System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(TargetPath));

        var SourcePath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Project.FilePath), "AppPackages", System.IO.Path.GetFileNameWithoutExtension(Project.FilePath));

        System.IO.File.Copy(SourcePath + "_" + Manifest.Version + "_" + PlatformArray.Select(P => GetArgument(P)).AsSeparatedText("_") + "_bundle.appxupload", TargetPath + "-" + Manifest.Version + ".appxupload", true);
      });
    }
    public void CleanWpfTarget(BuildTarget Target, BuildConfiguration Configuration, BuildPlatform Platform)
    {
      AddStep("Clean Wpf Target", Target.PathArray.AsSeparatedText(@"\"), Context =>
      {
        var MSBuildArguments =
          string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:{3}:Clean", Target.Solution.FilePath, GetArgument(Configuration), GetArgument(Platform), Target.PathArray.AsSeparatedText(@"\"));

        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
      });
    }
    public void BuildWpfTarget(BuildTarget Target, BuildConfiguration Configuration, BuildPlatform Platform)
    {
      AddStep("Build Wpf Target", Target.PathArray.AsSeparatedText(@"\"), Context =>
      {
        var MSBuildArguments =
          string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:{3}", Target.Solution.FilePath, GetArgument(Configuration), GetArgument(Platform), Target.PathArray.AsSeparatedText(@"\"));

        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);
      });
    }
    public void WriteNugetAssemblyInfo(BuildProject Project, BuildNugetVersion Version)
    {
      AddStep("Write Nuget AssemblyInfo", System.IO.Path.GetFileNameWithoutExtension(Project.FilePath), Context =>
      {
        // set the version in assemblyinfo.
        var AssemblyInfoPath = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(Project.FilePath), "Properties", "AssemblyInfo.cs");
        var AssemblyInfoTimestamp = System.IO.File.GetLastWriteTimeUtc(AssemblyInfoPath);
        var AssemblyInfoOriginalText = System.IO.File.ReadAllText(AssemblyInfoPath);
        var AssemblyInfoOriginalArray = AssemblyInfoOriginalText.Split(Environment.NewLine);
        var AssemblyInfoUpdateArray = (string[])AssemblyInfoOriginalArray.Clone();

        for (var AssemblyInfoIndex = 0; AssemblyInfoIndex < AssemblyInfoOriginalArray.Length; AssemblyInfoIndex++)
        {
          var AssemblyInfoLine = AssemblyInfoOriginalArray[AssemblyInfoIndex].Trim();

          if (AssemblyInfoLine.StartsWith("[assembly: AssemblyVersion") || AssemblyInfoLine.StartsWith("[assembly: AssemblyFileVersion") || AssemblyInfoLine.StartsWith("[assembly: AssemblyInformationalVersion"))
          {
            var AssemblyInfoTextStart = AssemblyInfoLine.IndexOf('"') + 1;
            var AssemblyInfoTextEnd = AssemblyInfoLine.IndexOf('"', AssemblyInfoTextStart);

            if (AssemblyInfoTextStart < AssemblyInfoTextEnd)
              AssemblyInfoUpdateArray[AssemblyInfoIndex] = AssemblyInfoLine.Remove(AssemblyInfoTextStart, AssemblyInfoTextEnd - AssemblyInfoTextStart).Insert(AssemblyInfoTextStart, Version.Text);
          }
        }

        Inv.Support.FileHelper.SetReadOnly(AssemblyInfoPath, false);
        System.IO.File.WriteAllLines(AssemblyInfoPath, AssemblyInfoUpdateArray);
      });
    }
    public void PackNugetProject(BuildProject Project, string DeploymentPath, BuildNugetVersion Version, BuildConfiguration Configuration, BuildPlatform Platform, BuildMacServer MacServer = null)
    {
      AddStep("Pack Nuget Project", System.IO.Path.GetFileNameWithoutExtension(Project.FilePath), Context =>
      {
        var ConfigurationArgument = GetArgument(Configuration);
        var PlatformArgument = GetArgument(Platform).StripWhitespace().Strip('\"');

        // clean the project.
        var MSBuildArguments = string.Format("\"{0}\" /p:Configuration={1};Platform={2} /t:Clean", Project.FilePath, ConfigurationArgument, PlatformArgument); // AnyCPU not "Any CPU"
        ExecuteProcess(Context, MSBuildPath, MSBuildArguments);

        // build the project.
        var NugetMacServer = MacServer == null ? "" : string.Format("ServerAddress={0};ServerUser={1};ServerPassword={2};",
          MacServer.Address, MacServer.Username, MacServer.Password);
        /*
        if (Project.FilePath.EndsWith("InvLibrary.csproj") || Project.FilePath.EndsWith("InvPlatform.csproj") || Project.FilePath.EndsWith("InvPlatformS.csproj"))
        {
          // Example: MSBuild /t:pack /p:InventionVersion=1.0.1;PackageOutputPath=C:\Deployment\Inv;Configuration="RELEASE";Constants="NUGET"

          var NugetArguments =
            string.Format("/t:pack \"{0}\" /p:PackageOutputPath={1};InventionVersion={2};Configuration={3};Platform={4};Constants=\"NUGET\"{5}",
              Project.FilePath, DeploymentPath.ExcludeAfter(System.IO.Path.DirectorySeparatorChar.ToString()), Version.Text, ConfigurationArgument, PlatformArgument, (NugetMacServer != "" ? ";" : string.Empty) + NugetMacServer);

          ExecuteProcess(Context, MSBuildPath, NugetArguments);
        }
        else*/
        {
          var NugetArguments =
            string.Format("pack \"{0}\" -OutputDirectory \"{1}\" -Build -Properties InventionVersion={2};Configuration={3};Platform={4};Constants=\"NUGET\";{5}",
            Project.FilePath, DeploymentPath.ExcludeAfter(System.IO.Path.DirectorySeparatorChar.ToString()), Version.Text, ConfigurationArgument, PlatformArgument, NugetMacServer);
          ExecuteProcess(Context, NugetPath, NugetArguments);
        }
      });
    }
    public void PushNugetPackage(string DeploymentPath, string PackageName, BuildNugetVersion Version)
    {
      AddStep("Push Nuget Package", PackageName, Context =>
      {
        var NugetArguments =
          string.Format("push \"{0}{1}.{2}.nupkg\" -Source https://www.nuget.org/api/v2/package", DeploymentPath, PackageName, Version.Text);

        ExecuteProcess(Context, NugetPath, NugetArguments);
      });
    }
    public void IncrementVsixVersion(BuildVsixManifest Manifest)
    {
      AddStep("Increment Vsix Version", System.IO.Path.GetFileName(Manifest.FilePath), Context =>
      {
        var VsixManifest = new VsixManifestFile(Manifest.FilePath);

        if (VsixManifest.IncrementVersionBuild())
          VsixManifest.Save();

        Manifest.Version = VsixManifest.Version;
      });
    }
    public void DeployVsixFile(string SourcePath, string TargetPath, BuildVsixManifest Manifest)
    {
      AddStep("Deploy Vsix File", TargetPath + "-v.vsix", Context =>
      {
        System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(TargetPath));

        System.IO.File.Copy(SourcePath, TargetPath + "-" + Manifest.Version + ".vsix", true);
      });
    }
    public void RunProgram(string FilePath, Func<string> ArgumentFunc = null)
    {
      AddStep("Run Program", System.IO.Path.GetFileName(FilePath), Context =>
      {
        ExecuteProcess(Context, FilePath, ArgumentFunc != null ? (ArgumentFunc() ?? "") : "");
      });
    }
    public void DeleteFile(string FileName, Func<string> FileFunc = null)
    {
      AddStep("Delete File", System.IO.Path.GetFileName(FileName), Context =>
      {
        var FilePath = FileFunc != null ? FileFunc() : FileName;

        if (System.IO.File.Exists(FilePath))
          System.IO.File.Delete(FilePath);
      });
    }
    public void CopyFile(string FileName, Func<string> SourceFunc, Func<string> TargetFunc)
    {
      AddStep("Copy File", System.IO.Path.GetFileName(FileName), Context =>
      {
        var SourceFilePath = SourceFunc();
        var TargetFilePath = TargetFunc();

        if (System.IO.File.Exists(SourceFilePath))
          System.IO.File.Copy(SourceFilePath, TargetFilePath, true);
      });
    }
    public void WriteTextFile(string FilePath, Func<string> ArgumentFunc)
    {
      AddStep("Write Text File", System.IO.Path.GetFileName(FilePath), Context =>
      {
        System.IO.File.WriteAllText(FilePath, ArgumentFunc != null ? (ArgumentFunc() ?? "") : "");
      });
    }
    public void Sleep(TimeSpan TimeSpan)
    {
      AddStep("Sleep", TimeSpan.FormatTimeSpanShort(), Context =>
      {
        System.Threading.Thread.Sleep(TimeSpan);
      });
    }

    internal IEnumerable<BuildStep> GetSteps()
    {
      return StepList;
    }

    private void ExecuteProcess(BuildContext Context, string Command, string Arguments)
    {
      Context.Log("EXECUTE: \"" + Command + "\" " + Arguments);

      var ProcessStart = new System.Diagnostics.ProcessStartInfo();
      ProcessStart.FileName = Command;
      ProcessStart.Arguments = Arguments;
      ProcessStart.RedirectStandardOutput = true;
      ProcessStart.RedirectStandardError = true;
      ProcessStart.UseShellExecute = false;
      ProcessStart.CreateNoWindow = true;
      //ProcessStart.EnvironmentVariables["VSSDK140Install"] = @"C:\Program Files (x86)\Microsoft Visual Studio 14.0\VSSDK\";
      //ProcessStart.EnvironmentVariables["VS140COMNTOOLS"] = @"C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\Tools\";
      //ProcessStart.EnvironmentVariables["GYP_MSVS_VERSION"] = "2015";
      //ProcessStart.EnvironmentVariables["VisualStudioVersion"] = "15.0";
      //ProcessStart.EnvironmentVariables["MSBuildExtensionsPath"] = @"C:\Program Files (x86)\Microsoft Visual Studio\2017\Professional\MSBuild";
      ProcessStart.EnvironmentVariables["VisualStudioVersion"] = "16.0";
      ProcessStart.EnvironmentVariables["MSBuildExtensionsPath"] = @"C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\MSBuild";

      System.Diagnostics.Process ProcessResult;
      try
      {
        ProcessResult = System.Diagnostics.Process.Start(ProcessStart);
      }
      catch (Exception Exception)
      {
        throw new Exception("Unable to execute process: \"" + Command + "\" " + Arguments, Exception);
      }

      while (!ProcessResult.StandardOutput.EndOfStream)
      {
        if (Context.IsCancelled)
        {
          ProcessResult.Kill();
          return;
        }

        var StreamLine = ProcessResult.StandardOutput.ReadLine().Trim();

        Context.Log(StreamLine);
      }

      while (!ProcessResult.WaitForExit(500))
      {
        if (Context.IsCancelled)
        {
          ProcessResult.Kill();
          return;
        }
      }

      if (ProcessResult.ExitCode != 0)
      {
        while (!ProcessResult.StandardError.EndOfStream)
        {
          var StreamLine = ProcessResult.StandardError.ReadLine().Trim();
          Context.Log(StreamLine);
        }

        Context.Fail();
      }
    }
    private BuildStep AddStep(string Name, string Description, Action<BuildContext> Action)
    {
      var Result = new BuildStep(Name, Description, Action);

      StepList.Add(Result);

      return Result;
    }
    private string GetArgument(BuildConfiguration Configuration)
    {
      switch (Configuration)
      {
        case BuildConfiguration.Adhoc:
          return "Ad-hoc";

        case BuildConfiguration.Debug:
          return "Debug";

        case BuildConfiguration.Release:
          return "Release";

        default:
          throw new Exception("BuildConfiguration not handled: " + Configuration);
      }
    }
    private string GetArgument(BuildPlatform Platform)
    {
      switch (Platform)
      {
        case BuildPlatform.AnyCPU:
          return "\"Any CPU\"";

        case BuildPlatform.x86:
          return "x86";

        case BuildPlatform.x64:
          return "x64";

        case BuildPlatform.iPhone:
          return "iPhone";

        case BuildPlatform.ARM:
          return "ARM";

        default:
          throw new Exception("BuildPlatform not handled: " + Platform);
      }
    }

    private readonly Inv.DistinctList<BuildStep> StepList;
    private const string NugetPath = @"C:\Tools\Nuget.exe";
    private const string SevenZipPath = @"C:\Program Files\7-zip\7z.exe";
    private static readonly string MSBuildPath;

    static BuildTask()
    {
      MSBuildPath = @"MSBuild.exe"; // fallback and hope for environment paths.

      var ProgramFiles = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);
      var YearArray = new[] { "2019", "2017" };
      var EditionArray = new[] { "Professional", "Community", "Enterprise", "BuildTools" };
      var VersionArray = new[] { "Current", "15.0" };

      foreach (var Year in YearArray)
      {
        foreach (var Edition in EditionArray)
        {
          foreach (var Version in VersionArray)
          {
            var FilePath = System.IO.Path.Combine(ProgramFiles, $@"Microsoft Visual Studio\{Year}\{Edition}\MSBuild\{Version}\Bin\msbuild.exe");

            if (System.IO.File.Exists(FilePath))
            {
              MSBuildPath = FilePath;
              return;
            }
          }
        }
      }
    }
  }

  public sealed class BuildStep
  {
    internal BuildStep(string Name, string Description, Action<BuildContext> Action)
    {
      this.Name = Name;
      this.Description = Description;
      this.Action = Action;
    }

    public string Name { get; }
    public string Description { get; }
    public Action<BuildContext> Action { get; }
  }

  public sealed class BuildContext
  {
    internal BuildContext()
    {
      this.LogBuilder = new StringBuilder();
    }

    public void Log(string Line)
    {
      LogBuilder.AppendLine(Line);

      if (LogEvent != null)
        LogEvent(Line);
    }

    public bool IsCancelled
    {
      get { return IsInterrupted || IsFailed || Exception != null; }
    }
    public bool IsFailed { get; private set; }
    public bool IsInterrupted { get; private set; }
    public Exception Exception { get; private set; }

    internal event Action<string> LogEvent;

    internal void Clear()
    {
      LogBuilder.Clear();
    }
    internal void Interupt()
    {
      this.IsInterrupted = true;
    }
    internal void Fail()
    {
      this.IsFailed = true;
    }
    internal void Caught(Exception Exception)
    {
      this.Exception = Exception;
    }
    internal string GetLog()
    {
      return LogBuilder.ToString();
    }
    internal bool HasLog()
    {
      return LogBuilder.Length > 0;
    }

    private readonly StringBuilder LogBuilder;
  }
}
