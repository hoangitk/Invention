﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;

namespace Inv
{
  /// <summary>
  /// Helper methods for ILGenerator to allow slightly less verbose IL generation code, as well as slightly more correct code
  /// due to the many, many overloads of ILGenerator.Emit(OpCode, x) occasionally picking the wrong implementation and ending badly
  /// for all concerned (typically, when the parameter is an int16/int32/int64 - OpCodes.Ldc_I8 with a literal implied int32 (like, say, 100) will crash the JIT
  /// at runtime with occasionally vague errors about 'internal limitations', for instance).
  /// 
  /// Replaces the (frequent) pattern:
  ///   ilGenerator.Emit(OpCodes.Something);
  ///   ilGenerator.Emit(Opcodes.SomethingWithOneParameter, x);
  /// with:
  ///   ilGenerator.Something();
  ///   ilGenerator.SomethingWithOneParameter(x);
  ///   
  /// etc.
  /// 
  /// This class is not complete, but was built from the set of opcodes I've needed so far. Add additional opcodes as necessary.
  /// </summary>
  [DebuggerNonUserCode]
  internal static class ILGeneratorHelpers
  {
    internal static void Add(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Add); }
    internal static void Sub(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Sub); }
    internal static void Div(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Div); }
    internal static void Mul(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Mul); }
    internal static void Ceq(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Ceq); }
    internal static void Clt(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Clt); }
    internal static void Cgt(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Cgt); }
    internal static void Ldloc(this ILGenerator ilGen, LocalBuilder local) { ilGen.Emit(OpCodes.Ldloc, local); }
    internal static void Ldloc_S(this ILGenerator ilGen, LocalBuilder local) { ilGen.Emit(OpCodes.Ldloc_S, local); }
    internal static void Ldloc_0(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Ldloc_0); }
    internal static void Ldloc_1(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Ldloc_1); }
    internal static void Ldloc_2(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Ldloc_2); }
    internal static void Ldloc_3(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Ldloc_3); }

    internal static void Ldloca(this ILGenerator ilGen, LocalBuilder local) { ilGen.Emit(OpCodes.Ldloca, local); }
    internal static void Ldfld(this ILGenerator ilGen, FieldInfo field) { ilGen.Emit(OpCodes.Ldfld, field); }
    internal static void Ldflda(this ILGenerator ilGen, FieldInfo field) { ilGen.Emit(OpCodes.Ldflda, field); }
    internal static void Call(this ILGenerator ilGen, MethodInfo method) { ilGen.Emit(OpCodes.Call, method); }
    internal static void Call(this ILGenerator ilGen, ConstructorInfo constructor) { ilGen.Emit(OpCodes.Call, constructor); }
    internal static void Callvirt(this ILGenerator ilGen, MethodInfo method) { ilGen.Emit(OpCodes.Callvirt, method); }
    internal static void Callvirt(this ILGenerator ilGen, ConstructorInfo constructor) { ilGen.Emit(OpCodes.Callvirt, constructor); }
    internal static void Ret(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Ret); }

    internal static void Newobj(this ILGenerator ilGen, ConstructorInfo constructor) { ilGen.Emit(OpCodes.Newobj, constructor); }
    internal static void Newarr(this ILGenerator ilGen, Type type) { ilGen.Emit(OpCodes.Newarr, type); }
    internal static void Throw(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Throw); }
    internal static void Stloc(this ILGenerator ilGen, LocalBuilder local) { ilGen.Emit(OpCodes.Stloc, local); }
    internal static void Stloc_S(this ILGenerator ilGen, LocalBuilder local) { ilGen.Emit(OpCodes.Stloc_S, local); }
    internal static void Stloc_0(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Stloc_0); }
    internal static void Stloc_1(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Stloc_1); }
    internal static void Stloc_2(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Stloc_2); }
    internal static void Stloc_3(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Stloc_3); }

    internal static void Ldc_I4(this ILGenerator ilGen, Int32 value)
    {
      switch (value)
      {
        case 0:
          ilGen.Emit(OpCodes.Ldc_I4_0);
          break;

        case 1:
          ilGen.Emit(OpCodes.Ldc_I4_1);
          break;

        case 2:
          ilGen.Emit(OpCodes.Ldc_I4_2);
          break;

        case 3:
          ilGen.Emit(OpCodes.Ldc_I4_3);
          break;

        case 4:
          ilGen.Emit(OpCodes.Ldc_I4_4);
          break;

        case 5:
          ilGen.Emit(OpCodes.Ldc_I4_5);
          break;

        case 6:
          ilGen.Emit(OpCodes.Ldc_I4_6);
          break;

        case 7:
          ilGen.Emit(OpCodes.Ldc_I4_7);
          break;

        case 8:
          ilGen.Emit(OpCodes.Ldc_I4_8);
          break;

        case -1:
          ilGen.Emit(OpCodes.Ldc_I4_M1);
          break;

        default:
          if (value >= 0 && value < 255)
            ilGen.Emit(OpCodes.Ldc_I4_S, value);
          else
            ilGen.Emit(OpCodes.Ldc_I4, value);
          break;
      }
    }

    internal static void Stind_I1(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Stind_I1); }
    internal static void Stind_I2(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Stind_I2); }
    internal static void Stind_I4(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Stind_I4); }
    internal static void Stind_I8(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Stind_I8); }
    internal static void Stind_R4(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Stind_R4); }
    internal static void Stind_R8(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Stind_R8); }
    internal static void Stind_Ref(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Stind_Ref); }
    internal static void Stobj(this ILGenerator ilGen, Type type) { ilGen.Emit(OpCodes.Stobj, type); }
    internal static void StindByType(this ILGenerator ilGen, Type variableType)
    {
      if (variableType == typeof(bool))
        ilGen.Stind_I1();
      else if (variableType == typeof(Int16) ||
               variableType == typeof(UInt16))
        ilGen.Stind_I2();
      else if (variableType == typeof(Int32) ||
               variableType == typeof(UInt32))
        ilGen.Stind_I4();
      else if (variableType == typeof(Int64) ||
               variableType == typeof(UInt64))
        ilGen.Stind_I8();
      else if (variableType == typeof(Single) ||
               variableType == typeof(float))
        ilGen.Stind_R4();
      else if (variableType == typeof(Double))
        ilGen.Stind_R8();
      else if (variableType.IsValueType || variableType.IsByRef)
        ilGen.Stobj(variableType);
      else
        ilGen.Stind_Ref();

    }

    internal static void Ldc_I8(this ILGenerator ilGen, long value) { ilGen.Emit(OpCodes.Ldc_I8, value); }

    internal static void Ldc_R8(this ILGenerator ilGen, Double value) { ilGen.Emit(OpCodes.Ldc_R8, value); }

    internal static void Ldstr(this ILGenerator ilGen, string value) { ilGen.Emit(OpCodes.Ldstr, value); }

    internal static void Ldnull(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Ldnull); }

    internal static void Ldarg_0(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Ldarg_0); }
    internal static void Ldarg_1(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Ldarg_1); }
    internal static void Ldarg_2(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Ldarg_2); }
    internal static void Ldarg_3(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Ldarg_3); }
    internal static void Ldarg(this ILGenerator ilGen, int arg) 
    {
      switch (arg)
      {
        case 0:
          ilGen.Ldarg_0();
          break;

        case 1:
          ilGen.Ldarg_1();
          break;

        case 2:
          ilGen.Ldarg_2();
          break;

        case 3:
          ilGen.Ldarg_3();
          break;

        default:
          ilGen.Emit(OpCodes.Ldarg, arg);
          break;
      }      
    }

    internal static void Ldarga(this ILGenerator ilGen, int arg) { ilGen.Emit(OpCodes.Ldarga, arg); }

    internal static void Ldsfld(this ILGenerator ilGen, FieldInfo field) { ilGen.Emit(OpCodes.Ldsfld, field); }
    internal static void Stsfld(this ILGenerator ilGen, FieldInfo field) { ilGen.Emit(OpCodes.Stsfld, field); }

    internal static void Br(this ILGenerator ilGen, Label label) { ilGen.Emit(OpCodes.Br, label); }
    internal static void Br_S(this ILGenerator ilGen, Label label) { ilGen.Emit(OpCodes.Br_S, label); }

    internal static void Brtrue(this ILGenerator ilGen, Label label) { ilGen.Emit(OpCodes.Brtrue, label); }
    internal static void Brtrue_S(this ILGenerator ilGen, Label label) { ilGen.Emit(OpCodes.Brtrue_S, label); }

    internal static void Brfalse(this ILGenerator ilGen, Label label) { ilGen.Emit(OpCodes.Brfalse, label); }
    internal static void Brfalse_S(this ILGenerator ilGen, Label label) { ilGen.Emit(OpCodes.Brfalse_S, label); }

    internal static void Leave(this ILGenerator ilgen, Label label) { ilgen.Emit(OpCodes.Leave, label); }

    internal static void Conv_U1(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Conv_U1); }
    internal static void Conv_I2(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Conv_I2); }
    internal static void Conv_U2(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Conv_U2); }
    internal static void Conv_I4(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Conv_I4); }
    internal static void Conv_U4(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Conv_U4); }
    internal static void Conv_I8(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Conv_I8); }
    internal static void Conv_U8(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Conv_U8); }
    internal static void Conv_R8(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Conv_R8); }

    internal static void Dup(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Dup); }
    internal static void Pop(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Pop); }

    internal static void Ldelem_U1(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Ldelem_U1); }
    internal static void Ldlen(this ILGenerator ilGen) { ilGen.Emit(OpCodes.Ldlen); }

    internal static void Castclass(this ILGenerator ilGen, Type type) { ilGen.Emit(OpCodes.Castclass, type); }

    internal static void Ldtoken(this ILGenerator ilGen, Type type) { ilGen.Emit(OpCodes.Ldtoken, type); }

    internal static void Box(this ILGenerator ilGen, Type type) { ilGen.Emit(OpCodes.Box, type); }
    internal static void Unbox(this ILGenerator ilGen, Type type) { ilGen.Emit(OpCodes.Unbox, type); }

    internal static void GetProperty(this ILGenerator ilGen, Type type, string Property)
    {
      ilGen.Emit(OpCodes.Callvirt, type.GetProperty(Property).GetGetMethod());
    }

    internal static void SetProperty(this ILGenerator ilGen, Type type, string Property)
    {
      ilGen.Emit(OpCodes.Callvirt, type.GetProperty(Property).GetSetMethod());
    }

  }

}
