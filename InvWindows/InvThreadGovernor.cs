﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.ComponentModel;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// Manages groups of execution threads.
  /// </summary>
  /// <remarks>
  /// <see cref="ThreadGovernor"/> provides methods to create <see cref="ThreadGroup"/> objects to manage execution of logical groups of threads, as well as providing
  /// <see cref="NewContext"/> to directly create <see cref="ThreadContext"/> objects on the default thread context for tasks that don't require separate grouping.
  /// </remarks>
  public static class ThreadGovernor
  {
    static ThreadGovernor()
    {
      CriticalSection = new Inv.ExclusiveCriticalSection("InvThreadGovernor");
      ThreadNameDictionary = new Dictionary<int, string>();

      DefaultThreadGroup = new Inv.ThreadGroup();

      ThreadGroupList = Inv.WeakReferenceGovernor.NewCollection<ThreadGroup>();
      ThreadGroupList.Add(DefaultThreadGroup);
    }

    public static string ActiveIdentity()
    {
      var CurrentThread = System.Threading.Thread.CurrentThread;

      var CurrentThreadName = CurrentThread.Name;
      if (!string.IsNullOrWhiteSpace(CurrentThreadName))
        return CurrentThreadName;

      var CurrentTaskName = TaskGovernor.ActiveIdentity();
      if (CurrentTaskName != null)
        return CurrentTaskName;

      var CurrentThreadId = CurrentThread.ManagedThreadId;

      using (CriticalSection.Lock())
      {
        var ThreadName = ThreadNameDictionary.GetValueOrDefault(CurrentThreadId);
        return ThreadName ?? "MT_" + CurrentThreadId;
      }
    }

    public static TaskActivity NewTask(string Name, Action Method, CancellationToken? CancellationToken = null)
    {
      return TaskGovernor.NewActivity(Name, Method, CancellationToken);
    }
    public static TaskActivity RunTask(string Name, Action Method, CancellationToken? CancellationToken = null)
    {
      return TaskGovernor.RunActivity(Name, Method, CancellationToken);
    }
    /// <summary>
    /// Create a new thread context on the default thread group.
    /// </summary>
    /// <param name="Name">The name for the new <see cref="ThreadContext"/>.</param>
    /// <param name="Method">The method delegate to execute on the new <see cref="ThreadContext"/>.</param>
    /// <returns>The new <see cref="ThreadContext"/></returns>
    public static ThreadContext NewContext(string Name, Action Method)
    {
      Debug.Assert(!string.IsNullOrEmpty(Name), "Thread context name must be specified.");
      Debug.Assert(Method != null, "Thread context method must be specified.");

      return DefaultThreadGroup.NewContext(Name, Method);
    }
    /// <summary>
    /// Create a new thread group.
    /// </summary>
    /// <returns>The new thread group.</returns>
    public static ThreadGroup NewGroup()
    {
      var ThreadGroup = new ThreadGroup();

      ThreadGroupList.Add(ThreadGroup);

      return ThreadGroup;
    }
    /// <summary>
    /// Get all <see cref="ThreadContext"/>s in all thread groups.
    /// </summary>
    /// <returns>An <see cref="IEnumerable{ThreadContext}"/> of all the <see cref="ThreadContext"/>s contained within all <see cref="ThreadGroup"/>s.</returns>
    public static IEnumerable<ThreadContext> AccessAllContexts()
    {
      var ThreadContextList = new Inv.DistinctList<ThreadContext>();

      foreach (var ThreadGroup in ThreadGroupList)
        ThreadContextList.AddRange(ThreadGroup.AccessContexts());

      return ThreadContextList;
    }
    /// <summary>
    /// Get all <see cref="ThreadContext"/>s in the default <see cref="ThreadGroup"/>.
    /// </summary>
    /// <returns>An <see cref="IEnumerable{ThreadContext}"/> of all the <see cref="ThreadContext"/>s contained within the default <see cref="ThreadGroup"/>.</returns>
    public static IEnumerable<ThreadContext> AccessDefaultContexts()
    {
      return DefaultThreadGroup.AccessContexts();
    }

    internal static readonly Inv.ExclusiveCriticalSection CriticalSection;
    internal static readonly Dictionary<int, string> ThreadNameDictionary;

    private static Inv.WeakReferenceCollection<Inv.ThreadGroup> ThreadGroupList;
    private static Inv.ThreadGroup DefaultThreadGroup;
  }

  /// <summary>
  /// Represents a logical group of <see cref="ThreadContext"/> objects.
  /// </summary>
  /// <remarks>
  /// <para><see cref="ThreadGroup"/> objects are not directly instantiated. Instead, use <see cref="ThreadGovernor.NewGroup"/> to obtain a new <see cref="ThreadGroup"/> object.</para>
  /// <para><see cref="ThreadGroup"/> objects are used to group together logical collections of <see cref="ThreadContext"/> objects for execution.</para>
  /// <para>
  /// Calling <see cref="Start"/>, <see cref="Stop"/>, or <see cref="Abort"/> will operate on the member <see cref="ThreadContext"/> objects in the order they were added to the
  /// <see cref="ThreadGroup"/>.
  /// </para>
  /// <example>
  /// <code>
  /// Inv.ThreadGroup group = Inv.ThreadGovernor.NewGroup();
  /// 
  /// Inv.ThreadContext ctx = group.NewContext("NewThread1", () => 
  /// {
  ///   // Code to do some work goes here.
  ///   Thread.Sleep(500);
  ///   Console.Out.WriteLine("In new thread.");
  ///   Thread.Sleep(500);
  ///   Console.Out.WriteLine("Still in new thread.");
  /// });
  /// 
  /// group.Start();
  /// Console.Out.WriteLine("Thread started.");
  /// group.Join();
  /// Console.Out.WriteLine("Thread completed.");
  /// </code>
  /// <para>Results in the output:</para>
  /// <code language="none">
  /// Thread started.
  /// (0.5 second pause)
  /// In new thread.
  /// (0.5 second pause)
  /// Still in new thread.
  /// Thread completed.
  /// </code>
  /// </example>
  /// </remarks>
  public sealed class ThreadGroup
  {
    internal ThreadGroup()
    {
      ThreadContextCollection = Inv.WeakReferenceGovernor.NewCollection<Inv.ThreadContext>();
    }

    /// <summary>
    /// Create a new thread context.
    /// </summary>
    /// <param name="Name">The name for the new <see cref="ThreadContext"/>.</param>
    /// <param name="Method">The method delegate to execute on the new <see cref="ThreadContext"/>.</param>
    /// <returns>The new <see cref="ThreadContext"/></returns>
    public ThreadContext NewContext(string Name, Action Method)
    {
      Debug.Assert(Name != null, "Name must be specified.");
      Debug.Assert(Method != null, "Method must be specified.");

      var ThreadContext = new ThreadContext(this, Name, Method);

      ThreadContextCollection.Add(ThreadContext);

      return ThreadContext;
    }
    /// <summary>
    /// Get all <see cref="ThreadContext"/>s in this <see cref="ThreadGroup"/>.
    /// </summary>
    /// <returns>An <see cref="IEnumerable{ThreadContext}"/> of all the <see cref="ThreadContext"/>s in this <see cref="ThreadGroup"/>.</returns>
    public IEnumerable<ThreadContext> AccessContexts()
    {
      return ThreadContextCollection;
    }
    /// <summary>
    /// Start every <see cref="ThreadContext"/> in this <see cref="ThreadGroup"/> in sequence.
    /// </summary>
    /// <remarks>
    /// The threads will be started in the order in which they were added to the <see cref="ThreadGroup"/>.
    /// </remarks>
    public void Start()
    {
      foreach (var ThreadContext in ThreadContextCollection)
        ThreadContext.Start();
    }
    /// <summary>
    /// Stop and join to every <see cref="ThreadContext"/> in this <see cref="ThreadGroup"/> in sequence.
    /// </summary>
    /// <remarks>
    /// The threads will be joined in the order in which they were added to the <see cref="ThreadGroup"/>.
    /// </remarks>
    public void Stop()
    {
      foreach (var ThreadContext in ThreadContextCollection)
        ThreadContext.Stop();
    }
    /// <summary>
    /// Abort every <see cref="ThreadContext"/> in this <see cref="ThreadGroup"/> in sequence.
    /// </summary>
    /// <remarks>
    /// The threads will be abort in the order in which they were added to the <see cref="ThreadGroup"/>.
    /// </remarks>
    public void Abort()
    {
      foreach (var ThreadContext in ThreadContextCollection)
        ThreadContext.Abort();
    }

    private Inv.WeakReferenceCollection<Inv.ThreadContext> ThreadContextCollection;
  }

  /// <summary>
  /// Represents a thread execution context.
  /// </summary>
  /// <remarks>
  /// <see cref="ThreadContext"/> objects are not directly instantiated. Instead, use <see cref="ThreadGovernor.NewContext">ThreadGovernor.NewContext(String, Action)</see> or
  /// <see cref="ThreadGroup.NewContext">ThreadGroup.NewContext(String, Action)</see> to obtain a new <see cref="ThreadContext"/> object.
  /// <example>See <see cref="ThreadGroup"/> for an example.</example>
  /// </remarks>
  public sealed class ThreadContext
  {
    internal ThreadContext(ThreadGroup Group, string Name, Action Method)
    {
      this.Group = Group;
      this.Name = Name;
      this.Method = Method;
      this.IsBackground = false;
      this.ApartmentState = System.Threading.ApartmentState.Unknown;
    }

    internal ThreadGroup Group { get; private set; }
    internal Thread Handle { get; private set; }
    internal Action Method { get; private set; }

    public int ID => Handle?.ManagedThreadId ?? 0;
    /// <summary>
    /// The name of the thread represented by this <see cref="ThreadContext"/>.
    /// </summary>
    public string Name { get; private set; }
    public bool IsBackground { get; private set; }
    public bool IsCurrent
    {
      get { return Handle == Thread.CurrentThread; }
    }
    public bool IsActive
    {
      get { return Handle != null; }
    }
    public ApartmentState ApartmentState { get; private set; }
    /// <summary>
    /// The state of the thread represented by this <see cref="ThreadContext"/>.
    /// </summary>
    public System.Threading.ThreadState State
    {
      get
      {
        if (Handle != null)
          return Handle.ThreadState;
        else
          return System.Threading.ThreadState.Unstarted;
      }
    }
    /// <summary>
    /// The invocation time of the method specified for this thread.
    /// </summary>
    /// <remarks>
    /// <see cref="InvokeTime"/> is set when the method delegate is executed.
    /// </remarks>
    public DateTimeOffset? InvokeTime { get; private set; }

    /// <summary>
    /// Start the thread represented by this <see cref="ThreadContext"/>.
    /// </summary>
    public void Start()
    {
      Debug.Assert(Handle == null, "Cannot start a thread that has not been joined or aborted.");

      this.Handle = new Thread(InvokeMethod)
      {
        Name = Name,
        IsBackground = IsBackground
      };

      if (ApartmentState != System.Threading.ApartmentState.Unknown)
        Handle.SetApartmentState(ApartmentState);

      Handle.Start();
    }
    /// <summary>
    /// Stop and join the thread represented by this <see cref="ThreadContext"/>.
    /// </summary>
    public void Stop()
    {
      Debug.Assert(Handle != null, "Cannot join to thread that is not started.");
      Debug.Assert(!IsCurrent, "Cannot join to the current thread.");

      if (Handle != null)
      {
        Handle.Join();
        Handle = null;
      }
    }
    /// <summary>
    /// Stop and join the thread represented by this <see cref="ThreadContext"/>.
    /// </summary>
    public bool Stop(TimeSpan TimeSpan)
    {
      Debug.Assert(Handle != null, "Cannot join to thread that is not started.");
      Debug.Assert(!IsCurrent, "Cannot join to the current thread.");

      return Handle.Join(TimeSpan);
    }
    /// <summary>
    /// Interrupt the thread represented by this <see cref="ThreadContext"/>.
    /// </summary>
    public void Interrupt()
    {
      Debug.Assert(!IsCurrent, "Cannot interrupt the current thread.");

      Handle.Interrupt();
    }
    /// <summary>
    /// Abort the thread represented by this <see cref="ThreadContext"/>.
    /// </summary>
    public void Abort()
    {
      Debug.Assert(Handle != null, "Cannot abort a thread that is not started.");
      Debug.Assert(!IsCurrent, "Cannot abort the current thread.");

      if (Handle != null)
      {
        Handle.Abort();
        Handle = null;
      }
    }
    /// <summary>
    /// Make the thread represented by this <see cref="ThreadContext"/> a background thread.
    /// </summary>
    public void SetBackground()
    {
      this.IsBackground = true;
    }
    /// <summary>
    /// Make the thread represented by this <see cref="ThreadContext"/> a foreground thread.
    /// </summary>
    public void SetForeground()
    {
      this.IsBackground = false;
    }
    /// <summary>
    /// Set the thread represented by this <see cref="ThreadContext"/> to a single-threaded apartment.
    /// </summary>
    public void SetSingleThreadedApartmentState()
    {
      this.ApartmentState = ApartmentState.STA;
    }
    public void SetMultiThreadedApartmentState()
    {
      this.ApartmentState = ApartmentState.MTA;
    }
    /*
    public StackTrace CaptureStackTrace()
    {
      Debug.Assert(Handle != null, "Cannot capture stack trace for a thread that is not started.");

      // NOTE: suspend/resume are required to capture the stack trace of a running thread.

      if (Handle != null && Handle.ThreadState == System.Threading.ThreadState.Stopped)
      {
#pragma warning disable 0618
        return new StackTrace(Handle, true);
      }
      else
      {
        // NOTE: there is no guaranteed safe way to get the stack trace for a running thread - suspend() can hang forever.

        return new StackTrace(new StackFrame());
      }
      else if (Handle.GetApartmentState() == ApartmentState.STA)
      {
        return new StackTrace(new StackFrame());

        // TODO: suspend of STA threads is unreliable, the call to suspend() can hang forever.
      }
      else
      {
#pragma warning disable 0618
        try
        {
          Handle.Suspend();
        }
        catch (ThreadStateException)
        {
          return new StackTrace(Handle, true);
        }

        try
        {
          return new StackTrace(Handle, true);
        }
        finally
        {
          Handle.Resume();
        }
#pragma warning restore 0618
      }
    }*/

    private void InvokeMethod()
    {
      try
      {
        this.InvokeTime = DateTimeOffset.Now;

        Method();
      }
      catch (ThreadAbortException)
      {
        // NOTE: this is also a legitimate method of shutting down a background thread.
      }
      catch (Exception Exception)
      {
        // NOTE: suppress the unhandled exception.
        if (Debugger.IsAttached)
          Debugger.Break();
        else
          Debug.Assert(false, Exception.AsReport());
      }
    }
  }

  public sealed class ThreadWorker<TResult>
  {
    public ThreadWorker()
    {
      this.Worker = new BackgroundWorkerExtended();
      Worker.WorkerReportsProgress = true;
      Worker.WorkerSupportsCancellation = true;
      Worker.DoWork += ThreadExecute;
      Worker.RunWorkerCompleted += ThreadCompleted;
      Worker.ExceptionEvent += ExceptionInvoke;
      Worker.ProgressChanged += ThreadProgress;
    }

    public bool IsBusy
    {
      get { return Worker.IsBusy; }
    }
    public event Action<ThreadWorkerResult<TResult>> PrepareEvent;
    public event Action<ThreadWorkerResult<TResult>> ProcessEvent;
    public event Action<ThreadWorkerResult<TResult>> CompletedEvent;
    public event Action<int> ProgressEvent;
    public event Action<Exception> ExceptionEvent;
    public event Action CancelledEvent;

    public void Start()
    {
      Start(default);
    }
    public void Start(TResult Context)
    {
      Debug.Assert(!Worker.IsBusy, "ThreadWorker is already started.");

      var Result = new ThreadWorkerResult<TResult>(this);
      Result.Context = Context;

      if (PrepareEvent != null)
        PrepareEvent(Result);

      lock (Worker)
        Worker.IsCancelled = false;

      Worker.ResetEvent.Reset();
      Worker.RunWorkerAsync(Result);
    }
    public void Cancel()
    {
      Worker.CancelAsync();
    }
    public void CancelWaitOne()
    {
      Cancel();
      Worker.ResetEvent.WaitOne();
    }
    public void WaitOne()
    {
      Worker.ResetEvent.WaitOne();
    }
    public void CancelWaitOne(TimeSpan Span)
    {
      Cancel();
      Worker.ResetEvent.WaitOne(Span);
    }
    public ThreadWorkerToken GetToken()
    {
      return new ThreadWorkerToken(Worker);
    }

    internal bool IsCancellationPending
    {
      get { return Worker.CancellationPending; }
    }
    
    internal void ProcessInvoke(ThreadWorkerResult<TResult> Result)
    {
      if (ProcessEvent != null)
        ProcessEvent(Result);
    }
    internal void CompletedInvoke(ThreadWorkerResult<TResult> Result)
    {
      if (CompletedEvent != null)
        CompletedEvent(Result);
    }
    internal void CancelledInvoke()
    {
      if (CancelledEvent != null)
        CancelledEvent();
    }
    internal void ExceptionInvoke(Exception Exception)
    {
      if (ExceptionEvent != null)
        ExceptionEvent(Exception);
    }
    internal void ReportProgress(int Value)
    {
      Worker.ReportProgress(Value);
    }

    private void ThreadProgress(object Sender, ProgressChangedEventArgs Arguments)
    {
      if (ProgressEvent != null)
        ProgressEvent(Arguments.ProgressPercentage);
    }
    private static void ThreadExecute(object Sender, DoWorkEventArgs Arguments)
    {
      var Worker = Sender as BackgroundWorkerExtended;

      if (Worker != null)
      {
        var Result = Arguments.Argument as ThreadWorkerResult<TResult>;

        Debug.Assert(Result != null, "Result is not assigned.");

        if (Result != null)
        {
          Arguments.Result = Result;

          if (ThreadCancel(Worker, Result))
            return;

          Result.WorkerProcessInvoke();

          if (ThreadCancel(Worker, Result))
            return;
        }
      }
    }
    private static void ThreadCompleted(object Sender, RunWorkerCompletedEventArgs Arguments)
    {      
      var Worker = Sender as BackgroundWorkerExtended;
      Worker.ResetEvent.Set();

      if (Worker != null)
      {
        if (Arguments.Error != null)
        {
          Worker.ExceptionInvoke(Arguments.Error.Preserve());

          throw Arguments.Error;
        }
        else if (Arguments.Cancelled)
        {
          Debug.Assert(false, "This should never happen as we are using our own IsCancelled.");
        }
        else
        {
          var Result = Arguments.Result as ThreadWorkerResult<TResult>;

          Debug.Assert(Result != null, "Result is not assigned.");

          if (Result != null)
          {
            var IsCancelledValue = false;
            lock (Worker)
              IsCancelledValue = Worker.IsCancelled;

            Result.IsCancelled = IsCancelledValue;

            if (IsCancelledValue)
              Result.WorkerCancelledInvoke();
            else
              Result.WorkerCompleteInvoke();
          }
        }
      }
    }
    private static bool ThreadCancel(BackgroundWorkerExtended Worker, ThreadWorkerResult<TResult> Result)
    {
      var Value = Worker.CancellationPending;

      if (Value)
      {
        lock (Worker)
          Worker.IsCancelled = Value;
      }

      return Value;
    }

    private BackgroundWorkerExtended Worker;

    private sealed class BackgroundWorkerExtended : BackgroundWorker
    {
      public BackgroundWorkerExtended()
      {
        this.IsCancelled = false;
        this.ResetEvent = new ManualResetEvent(false);
      }

      public bool IsCancelled { get; set; }
      public ManualResetEvent ResetEvent { get; private set; }
      public event Action<Exception> ExceptionEvent;

      public void ExceptionInvoke(Exception Exception)
      {
        if (ExceptionEvent != null)
          ExceptionEvent(Exception);
      }
    }
  }

  public sealed class ThreadWorkerResult<TResult>
  {
    internal ThreadWorkerResult(ThreadWorker<TResult> Worker)
    {
      this.Worker = Worker;
      
    }

    public TResult Context { get; set; }
    public bool IsCancelled { get; internal set; }
    public bool IsCancellationPending
    {
      get { return Worker.IsCancellationPending; }
    }

    public void ReportProgress(int Value)
    {
      Worker.ReportProgress(Value);
    }

    internal void WorkerProcessInvoke()
    {
      Worker.ProcessInvoke(this);
    }
    internal void WorkerCompleteInvoke()
    {
      Worker.CompletedInvoke(this);
    }
    internal void WorkerCancelledInvoke()
    {
      Worker.CancelledInvoke();
    }

    private ThreadWorker<TResult> Worker;
  }

  public sealed class ThreadWorkerToken
  {
    internal ThreadWorkerToken(BackgroundWorker Worker)
    {
      this.Worker = Worker;
    }

    public bool IsCancelling
    {
      get { return Worker.CancellationPending; }
    }
    public void ReportProgress(double Value, double Total)
    {
      Worker.ReportProgress((int)Math.Ceiling((Value / Total) * 100));
    }

    private BackgroundWorker Worker;
  }
}
