﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  public struct Version : IComparable, IComparable<Version>, IEquatable<Version>
  {
    public Version(params int[] NumberArray)
    {
      this.NumberArray = Array.ConvertAll(NumberArray, V => (long)V);
    }
    public Version(params long[] NumberArray)
    {
      this.NumberArray = NumberArray;
    }
    public Version(string Number)
    {
      this.NumberArray = Number.Split('.').Select(N => long.Parse(N)).ToArray();
    }

    public override string ToString()
    {
      return GetNumberText();
    }
    public override bool Equals(object obj)
    {
      var Source = obj as Version?;

      return Source != null && EqualTo(Source.Value);
    }
    public override int GetHashCode()
    {
      var Result = NumberArray.Length;
      
      foreach (var Number in NumberArray)
        Result = unchecked(Result * 314159 + (int)Number);

      return Result; 
    }

    public Inv.Version ReplaceLast(long Value)
    {
      var NewArray = new long[NumberArray.Length];
      NumberArray.CopyTo(NewArray, 0);
      NewArray[NewArray.Length - 1] = Value;

      return new Inv.Version(NewArray);
    }
    public long Last()
    {
      return NumberArray[NumberArray.Length - 1];
    }
    public Inv.Version RemoveLast()
    {
      return new Inv.Version(NumberArray.Take(NumberArray.Length - 1).ToArray());
    }
    public string GetNumberText()
    {
      return NumberArray.Select(N => N.ToString()).AsSeparatedText(".");
    }
    public long[] GetNumberArray()
    {
      return NumberArray;
    }
    public string GetNumberPadWithLeadingZeros(params int[] LeadingZeroArray)
    {
      var StringBuilder = new StringBuilder();

      for (var NumberIndex = 0; NumberIndex < NumberArray.Length; NumberIndex++)
      {
        var Number = NumberArray[NumberIndex];

        if (NumberIndex < LeadingZeroArray.Length)
          StringBuilder.Append(Number.ToString(new string('0', LeadingZeroArray[NumberIndex])));
        else
          StringBuilder.Append(Number.ToString());

        if (NumberIndex < NumberArray.Length - 1)
          StringBuilder.Append('.');
      }

      return StringBuilder.ToString();
    }
    public int CompareTo(Version Version)
    {
      if (Version == this)
        return 0;
      else
        return this.NumberArray.ShallowCompareTo(Version.NumberArray);
    }
    public bool EqualTo(Version Version)
    {
      return CompareTo(Version) == 0;
    }

    public static bool operator <(Version t1, Version t2)
    {
      return t1.NumberArray.ShallowCompareTo(t2.NumberArray) < 0;
    }
    public static bool operator <=(Version t1, Version t2)
    {
      return t1.NumberArray.ShallowCompareTo(t2.NumberArray) <= 0;
    }
    public static bool operator ==(Version t1, Version t2)
    {
      return t1.NumberArray.ShallowCompareTo(t2.NumberArray) == 0;
    }
    public static bool operator !=(Version t1, Version t2)
    {
      return t1.NumberArray.ShallowCompareTo(t2.NumberArray) != 0;
    }
    public static bool operator >(Version t1, Version t2)
    {
      return t1.NumberArray.ShallowCompareTo(t2.NumberArray) > 0;
    }
    public static bool operator >=(Version t1, Version t2)
    {
      return t1.NumberArray.ShallowCompareTo(t2.NumberArray) >= 0;
    }

    int IComparable<Version>.CompareTo(Version other)
    {
      return CompareTo(other);
    }
    bool IEquatable<Version>.Equals(Version other)
    {
      return EqualTo(other);
    }
    int IComparable.CompareTo(object obj)
    {
      return CompareTo((Version)obj);
    }

    private readonly long[] NumberArray;
  }
}
