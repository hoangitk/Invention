﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// Represents a sequence of tasks to be executed in order, halting if any task fails.
  /// </summary>
  /// <remarks>
  /// <para>Executes a sequence of <see cref="TaskOperation"/>s, optionally executing a delegate before and/or after each step.</para>
  /// <para>In the event of an exception occurring during execution of a task step, <see cref="TaskSequence"/> will execute
  /// the <see cref="Exception"/> delegate, if it exists, and then abort the sequence.</para>
  /// <example>
  /// <code>
  /// TaskSequence seq = new TaskSequence();
  ///           
  /// // This will be executed before each task in the sequence.
  /// seq.Before = (Operation) => { Console.Out.WriteLine("Before"); }
  /// 
  /// // This will be executed after each task in the sequence.
  /// seq.After = (Operation) => { Console.Out.WriteLine("After"); }
  /// 
  /// TaskOperation op1 = seq.Add("TaskName");
  /// op1.Execute = () => { Console.Out.WriteLine("Task 1"); }
  /// 
  /// TaskOperation op2 = seq.Add("TaskName2");
  /// op2.Execute = () => { Console.Out.WriteLine("Task 2"); }
  /// 
  /// seq.Run();
  /// </code>
  /// <para>This example creates a <see cref="TaskSequence"/>, adds two task operations to the sequence, and
  /// executes the sequence, resulting in the following output:</para>
  /// <code language="none">
  /// Before
  /// Task 1
  /// After
  /// Before
  /// Task 2
  /// After
  /// </code>
  /// </example>
  /// </remarks>
  public sealed class TaskSequence
  {
    /// <summary>
    /// Initialize a new instance of the <see cref="TaskSequence"/> class.
    /// </summary>
    public TaskSequence()
    {
      this.OperationList = new Inv.DistinctList<TaskOperation>();
    }

    /// <summary>
    /// The current step number.
    /// </summary>
    public int CurrentStep { get; private set; }
    /// <summary>
    /// The total number of steps in this <see cref="TaskSequence"/>.
    /// </summary>
    public int StepCount { get { return OperationList.Count; } }

    /// <summary>
    /// Remove all tasks from this <see cref="TaskSequence"/>.
    /// </summary>
    public void Clear()
    {
      OperationList.Clear();
    }
    /// <summary>
    /// Add a new named task to this <see cref="TaskSequence"/>.
    /// </summary>
    /// <param name="Name">The name of the new task.</param>
    /// <returns>A <see cref="TaskOperation"/> object to which an execution delegate can be set.</returns>
    public TaskOperation Add(string Name)
    {
      var Result = new TaskOperation(this, Name);

      OperationList.Add(Result);

      return Result;
    }
    public Func<TaskOperation, bool> Interrupt;
    /// <summary>
    /// A delegate which will be executed before each task in the <see cref="TaskSequence"/>.
    /// </summary>
    /// <value>The delegate to execute.</value>
    public Action<TaskOperation> Before
    {
      set
      {
        BeforeDelegate = value;
      }
    }
    /// <summary>
    /// A delegate which will be executed after the successful execution of each task in the <see cref="TaskSequence"/>.
    /// </summary>
    /// <value>The delegate to execute.</value>
    public Action<TaskOperation> After
    {
      set
      {
        AfterDelegate = value;
      }
    }
    /// <summary>
    /// A delegate which will be executed if a task in the <see cref="TaskSequence"/> raises an exception.
    /// </summary>
    /// <value>The delegate to execute.</value>
    public Action<TaskOperation, Exception> Exception
    {
      set
      {
        ExceptionDelegate = value;
      }
    }
    /// <summary>
    /// Execute each operation in the <see cref="TaskSequence"/>, halting if any task fails to execute to completion.
    /// </summary>
    /// <remarks>
    /// The operations will be called in the order they were added. Execution of the sequence will cease upon any operation
    /// throwing an exception; the <see cref="Exception"/> delegate will be called before <see cref="Run()"/> returns.
    /// </remarks>
    public void Run()
    {
      CurrentStep = 0;

      foreach (var Operation in OperationList)
      {
        if (!Execute(Operation))
          break;

        CurrentStep++;
      }
    }

    private bool Execute(TaskOperation Operation)
    {
      if (Interrupt != null && Interrupt(Operation))
      {
        return false;
      }
      else
      {
        try
        {
          if (BeforeDelegate != null)
            BeforeDelegate(Operation);

          if (Operation.ExecuteDelegate != null)
            Operation.ExecuteDelegate();

          if (AfterDelegate != null)
            AfterDelegate(Operation);
        }
        catch (Exception ExecuteException)
        {
          if (ExceptionDelegate != null)
            ExceptionDelegate(Operation, ExecuteException.Preserve());

          return false;
        }

        foreach (var Continue in Operation.ContinueList)
        {
          if (!Execute(Continue))
            return false;
        }

        return true;
      }
    }

    private Inv.DistinctList<TaskOperation> OperationList;
    private Action<TaskOperation> BeforeDelegate;
    private Action<TaskOperation> AfterDelegate;
    private Action<TaskOperation, Exception> ExceptionDelegate;
  }

  /// <summary>
  /// Represents an individual task operation within a <see cref="TaskSequence"/>.
  /// </summary>
  /// <remarks>
  /// <see cref="TaskOperation"/> objects are not directly instantiated, but can be obtained by:
  /// <list type="bullet">
  ///   <item>
  ///     <description>
  ///       Calling TaskSequence.Add(String) to add a new
  ///       task to a <see cref="TaskSequence"/>, or
  ///     </description>
  ///   </item>
  ///   <item>
  ///     <description>
  ///       Calling TaskSequence.Continue(String) to add a continuation requirement to an existing <see cref="TaskOperation"/>.
  ///     </description>    
  ///   </item>
  /// </list>
  /// </remarks>
  public sealed class TaskOperation
  {
    internal TaskOperation(TaskSequence Sequence, string Name)
    {
      this.Sequence = Sequence;
      this.Name = Name;
      this.ContinueList = new Inv.DistinctList<TaskOperation>();
    }
    /// <summary>
    /// The name of the task that this <see cref="TaskOperation"/> represents.
    /// </summary>
    public string Name { get; private set; }
    /// <summary>
    /// The delegate that this task will execute.
    /// </summary>
    /// <value>The delegate that this task will execute.</value>
    public Action Execute
    {
      set { this.ExecuteDelegate = value; }
    }

    /// <summary>
    /// Add a task sequence continuation requirement subtask to this <see cref="TaskOperation"/>.
    /// </summary>
    /// <remarks>
    /// <para>Adds a task sequence continuation requirement subtask to this <see cref="TaskOperation"/>.</para>
    /// <para>The delegate function added to the returned <see cref="TaskOperation"/> must execute successfully for
    /// the parent <see cref="TaskSequence"/> to continue execution beyond this <see cref="TaskOperation"/>.</para>
    /// </remarks>
    /// <param name="TaskName">The name of the subtask</param>
    /// <returns>A new <see cref="TaskOperation"/> object to which an execution delegate can be added.</returns>
    public TaskOperation Continue(string TaskName)
    {
      var Result = new TaskOperation(Sequence, TaskName);

      ContinueList.Add(Result);

      return Result;
    }

    internal TaskSequence Sequence { get; private set; }
    internal Action ExecuteDelegate { get; private set; }
    internal Inv.DistinctList<TaskOperation> ContinueList { get; private set; }
  }
}
