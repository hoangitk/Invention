﻿namespace Inv.Manual
{
  public class Program
  {
    [System.STAThread]
    static void Main(string[] args)
    {
      Inv.WpfShell.CheckRequirements(() =>
      {
        Inv.WpfShell.Options.DeviceEmulation = Inv.WpfDeviceEmulation.iPhoneX;
        Inv.WpfShell.Options.DeviceEmulationRotated = false;
        Inv.WpfShell.Options.DeviceEmulationFramed = true;
        Inv.WpfShell.Options.FullScreenMode = false;
        Inv.WpfShell.Options.DefaultWindowWidth = 1920;
        Inv.WpfShell.Options.DefaultWindowHeight = 1080;

        Inv.WpfShell.Run(Inv.Manual.Shell.Install);
      });
    }
  }
}