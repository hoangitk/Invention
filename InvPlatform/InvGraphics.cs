﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  /// <summary>
  /// See <see cref="Application.Graphics"/>.
  /// </summary>
  public sealed class Graphics
  {
    internal Graphics(Application Application)
    {
      this.Application = Application;
    }

    /// <summary>
    /// Get the dimensions (width, height) of the image.
    /// </summary>
    /// <param name="Image"></param>
    /// <returns></returns>
    public Inv.Dimension GetDimension(Inv.Image Image)
    {
      Application.RequireThreadAffinity();

      return Application.Platform.GraphicsGetDimension(Image);
    }
    /// <summary>
    /// Convert the image into a grayscale version.
    /// </summary>
    /// <param name="Image"></param>
    /// <returns></returns>
    public Inv.Image Grayscale(Inv.Image Image)
    {
      Application.RequireThreadAffinity();

      if (Image == null)
        return null;
      
      return Application.Platform.GraphicsGrayscale(Image);
    }
    /// <summary>
    /// Tint the input image with the provided colour.
    /// </summary>
    /// <param name="Image"></param>
    /// <param name="Colour"></param>
    /// <returns></returns>
    public Inv.Image Tint(Inv.Image Image, Inv.Colour Colour)
    {
      Application.RequireThreadAffinity();

      return Application.Platform.GraphicsTint(Image, Colour);
    }
    /// <summary>
    /// Resize the input image to the specified dimension.
    /// </summary>
    /// <param name="Image"></param>
    /// <param name="Dimension"></param>
    /// <returns></returns>
    public Inv.Image Resize(Inv.Image Image, Inv.Dimension Dimension)
    {
      Application.RequireThreadAffinity();

      return Application.Platform.GraphicsResize(Image, Dimension);
    }

    internal Application Application { get; private set; }
  }
}