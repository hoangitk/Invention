﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Globalization;
using System.Xml;
using System.Xml.Serialization;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// A serializable class representing a monetary amount.
  /// </summary>
  [XmlSchemaProvider("MySchema")]
  public struct Money : IComparable, IComparable<Money>, IEquatable<Money>, IXmlSerializable
  {
    /// <summary>
    /// Initializes a new <see cref="Money"/> object with the specified decimal amount.
    /// </summary>
    /// <param name="Amount">The decimal value representing the money amount.</param>
    public Money(decimal Amount)
    {
      this.AmountField = As4DP(Amount);
    }
    public static readonly Money Zero = new Money(0.0m);
    public static Money Parse(string Value)
    {
      if (!TryParse(Value, out var Result))
        throw new FormatException(string.Format("'{0}' is not a valid money representation", Value));

      return Result;
    }
    public static bool TryParse(string Value, out Money Money)
    {
      var Result = decimal.TryParse(Value, NumberStyles.Currency, NumberFormatInfo.CurrentInfo, out var DecimalValue);

      Money = new Money(DecimalValue);

      return Result;
    }

    public static Money Max(Money Value1, Money Value2)
    {
      return new Money(Math.Max(Value1.GetAmount(), Value2.GetAmount()));
    }
    public static Money Min(Money Value1, Money Value2)
    {
      return new Money(Math.Min(Value1.GetAmount(), Value2.GetAmount()));
    }

    /// <summary>
    /// Get the decimal value for the amount represented by this <see cref="Money"/> object.
    /// </summary>
    /// <returns>The decimal value for the amount represented by this <see cref="Money"/> object.</returns>
    public decimal GetAmount()
    {
      return AmountField;
    }
    public Money AsAbsolute()
    {
      return new Money(Math.Abs(this.AmountField));
    }
    /// <summary>
    /// Create a clone of this <see cref="Money"/> object.
    /// </summary>
    /// <returns>A new <see cref="Money"/> object which represents the same amount as this <see cref="Money"/> object.</returns>
    public Money Clone()
    {
      return new Money(this.AmountField);
    }
    /// <summary>
    /// Compare this <see cref="Money"/> object to another <see cref="Money"/> object.
    /// </summary>
    /// <param name="Money">The <see cref="Money"/> object to compare to this <see cref="Money"/> object.</param>
    /// <returns>
    /// <para>A signed number indicating the relative values of this <see cref="Money"/> object and <paramref name="Money"/></para>
    /// <para>
    /// <list type="table">
    /// <listheader>
    /// <term>Return Value</term>
    /// <term>Description</term>
    /// </listheader>
    /// <item>
    /// <description>Less than zero</description>
    /// <description>This <see cref="Money"/> is less than <paramref name="Money"/>.</description>
    /// </item>
    /// <item>
    /// <description>Zero</description>
    /// <description>This <see cref="Money"/> is equivalent to <paramref name="Money"/>.</description>
    /// </item>
    /// <item>
    /// <description>Greater than zero</description>
    /// <description>This <see cref="Money"/> is greater to <paramref name="Money"/>.</description>
    /// </item>
    /// </list>
    /// </para>
    /// </returns>
    public int CompareTo(Money Money)
    {
      if (Money == this)
        return 0;
      else
        return this.AmountField.CompareTo(Money.AmountField);
    }
    public bool EqualTo(Money Money)
    {
      return CompareTo(Money) == 0;
    }
    public Money RoundUp(Money AccuracyValue)
    {
      return new Money((AmountField / AccuracyValue.AmountField).Ceiling() * AccuracyValue.AmountField);
    }
    public Money RoundDown(Money AccuracyValue)
    {
      return new Money((AmountField / AccuracyValue.AmountField).Floor() * AccuracyValue.AmountField);
    }
    public Money RoundNearest(Money AccuracyValue)
    {
      return new Money(Math.Round(AmountField / AccuracyValue.AmountField) * AccuracyValue.AmountField);
    }
    public Money RoundBankers()
    {
      return new Money(AmountField.RoundBankers());
    }
    public Money RoundPricing()
    {
      return new Money(AmountField.RoundPricing());
    }
    public Money AsCents()
    {
      return new Money(AmountField - AmountField.Truncate());
    }
    public Money AsWholeDollars()
    {
      return new Money(AmountField.Truncate());
    }

    /// <summary>
    /// Get a string representation of this <see cref="Money"/> object.
    /// </summary>
    /// <returns>A string in the current money format.</returns>
    public override string ToString()
    {
      return AmountField.ToString("C");
    }
    public string ToString(string Format)
    {
      if (Format == null)
        return ToString();
      else
        return AmountField.ToString(Format);
    }
    public string ToString(int DecimalPlaces)
    {
      return AmountField.ToString("C" + DecimalPlaces);
    }
    public string ToStringWithoutCurrencySymbol()
    {
      return AmountField.ToString("C").Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol, "");
    }
    public string ToStringWithoutCurrencySymbol(int DecimalPlaces)
    {
      return AmountField.ToString("C" + DecimalPlaces).Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol, "");
    }
    public string ToStringCentPart(int DecimalPlaces)
    {
      return string.Format("{0:#." + new string('0', DecimalPlaces) + "}", GetAmount());
    }
    /// <summary>
    /// Compare this <see cref="Money"/> object to another <see cref="Money"/> object.
    /// </summary>
    /// <param name="obj">The <see cref="Money"/> object to compare to this <see cref="Money"/> object.</param>
    /// <returns>True if the two <see cref="Money"/> objects represent the same amount.</returns>
    public override bool Equals(object obj)
    {
      var Source = obj as Money?;

      return Source != null && EqualTo(Source.Value);
    }
    /// <summary>
    /// Returns the hash code for this <see cref="Money"/>.
    /// </summary>
    /// <returns>The hash code for this <see cref="Money"/>.</returns>
    public override int GetHashCode()
    {
      return this.AmountField.GetHashCode();
    }

    public static Money operator -(Money t)
    {
      return new Money(-t.AmountField);
    }
    public static Money operator -(Money t1, Money t2)
    {
      return new Money(t1.AmountField - t2.AmountField);
    }
    public static Money operator +(Money t)
    {
      return new Money(+t.AmountField);
    }
    public static Money operator +(Money t1, Money t2)
    {
      return new Money(t1.AmountField + t2.AmountField);
    }
    public static Money operator *(Money t1, Money t2)
    {
      return new Money(t1.AmountField * t2.AmountField);
    }
    public static Money operator *(Money t1, decimal t2)
    {
      return new Money(t1.AmountField * t2);
    }
    public static Money operator *(decimal t1, Money t2)
    {
      return new Money(t1 * t2.AmountField);
    }
    public static Money operator /(Money t1, Money t2)
    {
      return new Money(t1.AmountField / t2.AmountField);
    }
    public static Money operator /(Money t1, decimal t2)
    {
      return new Money(t1.AmountField / t2);
    }
    public static Money operator /(decimal t1, Money t2)
    {
      return new Money(t1 / t2.AmountField);
    }
    public static bool operator <(Money t1, Money t2)
    {
      return t1.AmountField < t2.AmountField;
    }
    public static bool operator <=(Money t1, Money t2)
    {
      return t1.AmountField <= t2.AmountField;
    }
    public static bool operator ==(Money t1, Money t2)
    {
      return t1.AmountField == t2.AmountField;
    }
    public static bool operator !=(Money t1, Money t2)
    {
      return t1.AmountField != t2.AmountField;
    }
    public static bool operator >(Money t1, Money t2)
    {
      return t1.AmountField > t2.AmountField;
    }
    public static bool operator >=(Money t1, Money t2)
    {
      return t1.AmountField >= t2.AmountField;
    }

    private static decimal As4DP(decimal Value)
    {
      var Result = Math.Round(Value, 4);
      
      if (Value > 0 && Result > Value)
        return Result - new decimal(1, 0, 0, false, 4);
      
      if (Value < 0 && Result < Value)
        return Result + new decimal(1, 0, 0, false, 4);

      return Result.Normalise();
    }

    int IComparable<Money>.CompareTo(Money other)
    {
      return CompareTo(other);
    }
    bool IEquatable<Money>.Equals(Money other)
    {
      return EqualTo(other);
    }
    int IComparable.CompareTo(object obj)
    {
      return CompareTo((Money)obj);
    }

    private static XmlQualifiedName MySchema(object xs)
    {
      return new XmlQualifiedName("decimal", "http://www.w3.org/2001/XMLSchema");
    }
    System.Xml.Schema.XmlSchema IXmlSerializable.GetSchema()
    {
      return null; // NOTE: this is required to be null.
    }
    void IXmlSerializable.ReadXml(System.Xml.XmlReader reader)
    {
      AmountField = XmlConvert.ToDecimal(reader.ReadInnerXml());
    }
    void IXmlSerializable.WriteXml(System.Xml.XmlWriter writer)
    {
      writer.WriteRaw(XmlConvert.ToString(AmountField));
    }

    private /*readonly*/ decimal AmountField;
  }
}