﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Security.Permissions;

namespace Win32
{
  public static class Ktmw32
  {
    [DllImport("Ktmw32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    public static extern IntPtr CreateTransaction(IntPtr securityAttributes, IntPtr guid, int options, int isolationLevel, int isolationFlags, int milliSeconds, string description);

    [DllImport("Ktmw32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    public static extern bool CommitTransaction(IntPtr transaction);

    [DllImport("Ktmw32.dll", SetLastError = true, CharSet = CharSet.Unicode)]
    public static extern bool RollbackTransaction(IntPtr transaction);

    [ComImport]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    [Guid("79427A2B-F895-40e0-BE79-B57DC82ED231")]
    public interface IKernelTransaction
    {
      void GetHandle([Out] out IntPtr handle);
    }
  }
}