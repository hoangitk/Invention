﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// See <see cref="Application.Process"/>
  /// </summary>
  public sealed class Process
  {
    internal Process(Application Application)
    {
      this.Application = Application;
    }

    /// <summary>
    /// Unique process Id (all platforms have this concept).
    /// </summary>
    public int Id { get; internal set; }

    /// <summary>
    /// Get the memory usage of this process.
    /// </summary>
    /// <returns></returns>
    public Inv.DataSize GetMemoryUsage()
    {
      // NOTE: allowed from any thread.
      //Application.CheckThreadAffinity();

      return Inv.DataSize.FromBytes(Application.Platform.ProcessMemoryUsedBytes());
    }
    /// <summary>
    /// Request that the platform reclaims memory by releasing internal cache.
    /// </summary>
    public void MemoryReclamation()
    {
      Application.RequireThreadAffinity();

      Application.Platform.ProcessMemoryReclamation();
    }
    /// <summary>
    /// Get ancillary process information, specific to the device.
    /// </summary>
    /// <returns></returns>
    public string GetAncillaryInformation()
    {
      return Application.Platform.ProcessAncillaryInformation();
    }
    /// <summary>
    /// Intentionally cause the app to crash.
    /// This is for testing the behaviour of crash reporting such as HockeyApp.
    /// </summary>
    public void IntentionalCrash()
    {
      throw new ProcessIntentionalCrashException();
    }

    private Application Application;
  }

  internal sealed class ProcessIntentionalCrashException : Exception
  {
    internal ProcessIntentionalCrashException()
      : base("Intentional fatal crash was generated by the end user.")
    {
    }
  }
}