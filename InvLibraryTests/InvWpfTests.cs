﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Inv.Support;

namespace InvWpf
{
  [TestClass]
  public class MediaTests
  {
    [TestMethod]
    public void ColorConversionRoundtrip()
    {
      var SampleMediaColorArray = new System.Windows.Media.Color[] { System.Windows.Media.Colors.Red, System.Windows.Media.Colors.Black, System.Windows.Media.Colors.White, System.Windows.Media.Colors.Yellow, System.Windows.Media.Colors.Tan, System.Windows.Media.Colors.Blue, System.Windows.Media.Colors.Green };

      foreach (var SampleMediaColor in SampleMediaColorArray)
      {
        var ConvertInvColour = SampleMediaColor.ConvertToInvColour();
        var RevertInvMediaColor = ConvertInvColour.ConvertToMediaColor();
        if (SampleMediaColor != RevertInvMediaColor)
          Assert.Fail("The sample media color did not roundtrip via the Inv.Colour conversion");
      }
    }
  }
}