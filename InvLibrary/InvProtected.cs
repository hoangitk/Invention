﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.Serialization;

namespace Inv
{
  public sealed class ProtectedEntity
  {
    public ProtectedEntity()
    {
      this.AttributeList = new Inv.DistinctList<ProtectedAttribute>();
      this.ChoiceList = new Inv.DistinctList<ProtectedChoice>();
    }

    public ProtectedAttribute AddAttribute(string Name)
    {
      var Result = new ProtectedAttribute(Name);

      AttributeList.Add(Result);

      return Result;
    }
    public ProtectedChoice AddChoice(string Name)
    {
      var Result = new ProtectedChoice(Name);

      ChoiceList.Add(Result);

      return Result;
    }
    public IEnumerable<ProtectedAttribute> GetAttributes()
    {
      return AttributeList;
    }
    public IEnumerable<ProtectedChoice> GetChoices()
    {
      return ChoiceList;
    }

    private Inv.DistinctList<ProtectedAttribute> AttributeList;
    private Inv.DistinctList<ProtectedChoice> ChoiceList;
  }

  public sealed class ProtectedChoice
  {
    internal ProtectedChoice(string Name)
    {
      this.Name = Name;
      this.AttributeList = new Inv.DistinctList<ProtectedAttribute>();
    }

    public string Name { get; private set; }
    public bool ReadShielded { get; set; }
    public bool WriteShielded { get; set; }

    public ProtectedAttribute AddAttribute(string Name)
    {
      var Result = new ProtectedAttribute(Name);

      AttributeList.Add(Result);

      return Result;
    }
    public IEnumerable<ProtectedAttribute> GetAttributes()
    {
      return AttributeList;
    }

    private Inv.DistinctList<ProtectedAttribute> AttributeList;
  }

  public sealed class ProtectedAttribute
  {
    internal ProtectedAttribute(string Name)
    {
      this.Name = Name;
    }

    public string Name { get; private set; }
    public bool ReadShielded { get; set; }
    public bool WriteShielded { get; set; }
    public ProtectedEntity Entity { get; set; } 
  }

  public interface IProtectedRecord
  {
    IEnumerable<Inv.IProtectedField> EnumerateFields();
    IEnumerable<Inv.IProtectedOption> EnumerateOptions();
  }

  public interface IProtectedField
  {
    bool Defined { get; }
    bool ReadShielded { get; set; }
    bool WriteShielded { get; set; }
    object Object { get; set; }
    IProtectedRecord ObjectAsRecord();
    IProtectedCollection ObjectAsCollection();
    void Define();
    void Undefine();
  }

  public interface IProtectedCollection : System.Collections.IEnumerable
  {
    bool ReadShielded { get; set; }
    bool WriteShielded { get; set; }
    bool IsRecordType { get; }
  }

  public interface IProtectedOption
  {
    bool Defined { get; }
    bool ReadShielded { get; set; }
    bool WriteShielded { get; set; }
    ProtectedFlags[] FieldFlagsArray { get; }
    int? ActiveIndex { get; }
    object ActiveObject { get; }
    IProtectedRecord ActiveObjectAsRecord();
    void Define();
    void Undefine();
  }

  [DataContract]
  public sealed class ProtectedRecord : IProtectedRecord
  {
    public ProtectedRecord()
    {
      this.FieldList = new Inv.DistinctList<IProtectedField>();
      this.OptionList = new Inv.DistinctList<IProtectedOption>();
    }

    public ProtectedOption<TBase, TEnumeration> AddOption<TBase, TEnumeration>() where TEnumeration : struct
    {
      var Result = new ProtectedOption<TBase, TEnumeration>();

      OptionList.Add(Result);

      return Result;
    }
    public ProtectedField<TField> AddField<TField>()
    {
      var Result = new ProtectedField<TField>();

      FieldList.Add(Result);

      return Result;
    }

    IEnumerable<IProtectedField> IProtectedRecord.EnumerateFields()
    {
      return FieldList;
    }
    IEnumerable<IProtectedOption> IProtectedRecord.EnumerateOptions()
    {
      return OptionList;
    }

    [DataMember]
    private Inv.DistinctList<IProtectedOption> OptionList;
    [DataMember]
    private Inv.DistinctList<IProtectedField> FieldList;
  }

  [DataContract]
  public sealed class ProtectedField<TObject> : IProtectedField
  {
    public TObject Object
    {
      [DebuggerNonUserCode]
      get
      {
        if (ReadShielded)
          throw new Exception($"Field of type '{typeof(TObject).FullName}' is shielded from read access.");

        if (!Defined)
          throw new Exception($"Field of type '{typeof(TObject).FullName}' has not been defined.");

        return ObjectField;
      }
      [DebuggerNonUserCode]
      set
      {
        if (WriteShielded)
          throw new Exception($"Field of type '{typeof(TObject).FullName}' is shielded from write access.");

        this.ObjectField = value;

        this.Defined = true;
      }
    }

    public bool Defined
    {
      get { return (Flags & ProtectedFlags.Defined) != 0; }
      set 
      { 
        if (value)
          Flags |= ProtectedFlags.Defined; 
        else
          Flags &= ~ProtectedFlags.Defined; 
      }
    }
    public bool ReadShielded
    {
      get { return (Flags & ProtectedFlags.ReadShielded) != 0; }
      set
      {
        if (value)
          Flags |= ProtectedFlags.ReadShielded;
        else
          Flags &= ~ProtectedFlags.ReadShielded;
      }
    }
    public bool WriteShielded
    {
      get { return (Flags & ProtectedFlags.WriteShielded) != 0; }
      set
      {
        if (value)
          Flags |= ProtectedFlags.WriteShielded;
        else
          Flags &= ~ProtectedFlags.WriteShielded;
      }
    }

    public void Define()
    {
      this.Defined = true;
    }
    public void Undefine()
    {
      this.Defined = false;
    }

    IProtectedCollection IProtectedField.ObjectAsCollection()
    {
      return ObjectField as IProtectedCollection;
    }
    IProtectedRecord IProtectedField.ObjectAsRecord()
    {
      return ObjectField as IProtectedRecord;
    }
    object IProtectedField.Object
    {
      get { return Object; }
      set { Object = (TObject)value; }
    }

    [DataMember]
    private TObject ObjectField;
    [DataMember]
    private ProtectedFlags Flags;
  }

  [DataContract]
  public sealed class ProtectedOption<TBase, TEnumeration> : IProtectedOption where TEnumeration : struct
  {
    public ProtectedOption(params Type[] TypeArray)
    {
      this.ObjectField = new Inv.Choice<TBase, TEnumeration>(TypeArray);
      this.FieldFlagsArray = new ProtectedFlags[TypeArray.Length];
    }

    public Type[] TypeArray
    {
      get { return ObjectField.TypeArray; }
      set { ObjectField.TypeArray = value; }
    }
    public bool Defined
    {
      get { return (Flags & ProtectedFlags.Defined) != 0; }
      set
      {
        if (value)
          Flags |= ProtectedFlags.Defined;
        else
          Flags &= ~ProtectedFlags.Defined;
      }
    }
    public bool ReadShielded
    {
      get { return (Flags & ProtectedFlags.ReadShielded) != 0; }
      set
      {
        if (value)
          Flags |= ProtectedFlags.ReadShielded;
        else
          Flags &= ~ProtectedFlags.ReadShielded;
      }
    }
    public bool WriteShielded
    {
      get { return (Flags & ProtectedFlags.WriteShielded) != 0; }
      set
      {
        if (value)
          Flags |= ProtectedFlags.WriteShielded;
        else
          Flags &= ~ProtectedFlags.WriteShielded;
      }
    }
    [DataMember]
    public ProtectedFlags[] FieldFlagsArray { get; private set; }

    [DataMember]
    public Inv.Choice<TBase, TEnumeration> ObjectField;
    
    public TEnumeration? Index
    {
      [DebuggerNonUserCode]
      get
      {
        if (ReadShielded)
          throw new Exception("Choice index is shielded from read access.");

        if (!Defined)
          throw new Exception("Choice index has not been defined.");

        return ObjectField.Index;
      }
      [DebuggerNonUserCode]
      set
      {
        if (WriteShielded)
          throw new Exception("Choice index is shielded from write access.");

        ObjectField.Index = value;

        Defined = true;

        if (value != null)
          FieldFlagsArray[(int)(object)value] |= ProtectedFlags.Defined;
      }
    }
    [DebuggerNonUserCode]
    public TObject Retrieve<TObject>(TEnumeration RetrieveIndex) where TObject : TBase
    {
      if (ReadShielded)
        throw new Exception("Choice is shielded from retrieve access.");

      if (!Defined)
        throw new Exception("Choice index has not been defined.");

      if ((FieldFlagsArray[(int)(object)RetrieveIndex] & ProtectedFlags.ReadShielded) != 0)
        throw new Exception(string.Format("Choice field {0} is shielded from read access.", RetrieveIndex));

      if ((FieldFlagsArray[(int)(object)RetrieveIndex] & ProtectedFlags.Defined) == 0)
        throw new Exception(string.Format("Choice field {0} has not been defined.", RetrieveIndex));

      return ObjectField.Retrieve<TObject>(RetrieveIndex);
    }
    [DebuggerNonUserCode]
    public void Store<TObject>(TEnumeration StoreIndex, TObject Object) where TObject : TBase
    {
      if (WriteShielded)
        throw new Exception("Choice is shielded from write access.");

      if ((FieldFlagsArray[(int)(object)StoreIndex] & ProtectedFlags.WriteShielded) != 0)
        throw new Exception(string.Format("Choice field {0} is shielded from write access.", StoreIndex));

      ObjectField.Store(StoreIndex, Object);

      FieldFlagsArray[(int)(object)StoreIndex] |= ProtectedFlags.Defined;
      Defined = true;
    }

    public void Define()
    {
      if (ObjectField.Index != null)
        FieldFlagsArray[(int)(object)ObjectField.Index] |= ProtectedFlags.Defined;

      Defined = true;
    }
    public void Undefine()
    {
      if (ObjectField.Index != null)
        FieldFlagsArray[(int)(object)ObjectField.Index] &= ~ProtectedFlags.Defined;

      Defined = false;
    }

    int? IProtectedOption.ActiveIndex
    {
      get { return ObjectField.Index != null ? Convert.ToInt32(ObjectField.Index.Value) : (int?)null; }
    }
    object IProtectedOption.ActiveObject
    {
      get { return ObjectField.Option; }
    }
    IProtectedRecord IProtectedOption.ActiveObjectAsRecord()
    {
      return ObjectField.Option as IProtectedRecord;
    }

    [DataMember]
    private ProtectedFlags Flags;
  }

  [DataContract]
  public enum ProtectedFlags : byte
  {
    Defined = 0x01,
    ReadShielded = 0x02,
    WriteShielded = 0x04
  }
}