﻿namespace InvManualU
{
  public sealed partial class MainPage : Windows.UI.Xaml.Controls.Page
  {
    public MainPage()
    {
      this.InitializeComponent();

      Inv.UwaShell.HockeyAppID = "742b664bbd6242b4b0cb95f2187be48e";
      Inv.UwaShell.XboxLive.AutoSignIn = false;
      Inv.UwaShell.Attach(this, Inv.Manual.Shell.Install);
    }
  }
}