﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Inv
{
  public sealed class GtkMutex : IDisposable
  {
    public GtkMutex(string Name)
    {
      var MutexId = string.Format("Global\\{0}", Name);

      this.Handle = new System.Threading.Mutex(false, MutexId);
    }
    public void Dispose()
    {
      if (Handle != null)
      {
        try
        {
          Unlock();
        }
        catch (Exception Exception)
        {
          Debug.WriteLine(Exception.Message);

          if (Debugger.IsAttached)
            Debugger.Break();
        }

        Handle.Dispose();
        Handle = null;
      }
    }

    public bool Lock(TimeSpan Timeout)
    {
      if (!IsAcquired)
      {
        try
        {
          this.IsAcquired = Handle.WaitOne(Timeout);
        }
        catch (AbandonedMutexException)
        {
          this.IsAcquired = true;
        }
      }

      return IsAcquired;
    }
    public void Unlock()
    {
      if (IsAcquired)
      {
        Handle.ReleaseMutex();
        IsAcquired = false;
      }
    }

    private System.Threading.Mutex Handle;
    private bool IsAcquired;
  }

  public sealed class GtkWindow : Gtk.Window
  {
    public GtkWindow()
      : base(Gtk.WindowType.Toplevel)
    {
    }
  }

  public sealed class GtkSurface : Gtk.EventBox
  {
    public GtkSurface()
    {
      this.VisibleWindow = true;
      this.Show();
    }
  }

  public sealed class GtkTimer
  {
    public GtkTimer(Action Action)
    {
      this.Base = new System.Threading.Timer(State => Action());
    }

    public TimeSpan IntervalTime { get; private set; }
    public bool IsEnabled { get; private set; }

    public void Set(bool IsEnabled, TimeSpan Interval)
    {
      Base.Change(IsEnabled ? System.Threading.Timeout.Infinite : 0, (int)Interval.TotalMilliseconds);
    }

    private System.Threading.Timer Base;
  }

  public sealed class GtkBorder : Gtk.EventBox
  {
    public GtkBorder()
    {
      this.Alignment = new Gtk.Alignment(0.0F, 0.0F, 1.0F, 1.0F);
      base.Child = Alignment;
      Alignment.Show();
    }

    public Gtk.Alignment Alignment { get; private set; }

    public new Gtk.Widget Child
    {
      get { return Alignment.Child; }
      set { Alignment.Child = value; }
    }
  }

  public abstract class GtkControl : Gtk.Alignment
  {
    public GtkControl()
      : base(0.0F, 0.0F, 0.0F, 0.0F)
    {
      this.Border = new GtkBorder();
      base.Child = Border;
      Border.Show();

      this.Background = new Gtk.EventBox();
      Border.Child = Background;
      Background.Show();
    }

    public Gtk.EventBox Background { get; private set; }
    public GtkBorder Border { get; private set; }

    public new Gtk.Widget Child
    {
      get { return Background.Child; }
      set { Background.Child = value; }
    }
  }

  public sealed class GtkDock : GtkControl
  {
    public GtkDock(bool Horizontal)
    {
      this.Box = Horizontal ? (Gtk.Box)new Gtk.HBox(false, 0) : (Gtk.Box)new Gtk.VBox(false, 0);
      this.Child = Box;
      Box.Show();
    }

    public Gtk.Box Box { get; private set; }

    internal void Compose(IEnumerable<Gtk.Widget> Headers, IEnumerable<Gtk.Widget> Clients, IEnumerable<Gtk.Widget> Footers)
    {
      // TODO: remove all.
      foreach (var Child in Box.Children)
        Box.Remove(Child);

      foreach (var Header in Headers)
        Box.PackStart(Header, false, false, 0);

      foreach (var Client in Clients)
        Box.PackStart(Client, true, true, 0);

      foreach (var Footer in Footers)
        Box.PackEnd(Footer, false, false, 0);
    }
  }

  public sealed class GtkButton : GtkControl
  {
    public GtkButton()
    {
      this.Button = new Gtk.Button();
      this.Child = Button;
      Button.Relief = Gtk.ReliefStyle.None;
      //Button.Entered += (Sender, Event) => Button.ModifyBg(StateType.Normal, new Gdk.Color(55, 174, 99));
      Button.Show();
    }

    public Gtk.Button Button { get; private set; }
  }

  public sealed class GtkBoard : GtkControl
  {
    public GtkBoard()
    {
      this.Fixed = new Gtk.Fixed();
      this.Child = Fixed;
      Fixed.Show();
    }

    public Gtk.Fixed Fixed { get; private set; }
  }

  public sealed class GtkGraphic : GtkControl
  {
    public GtkGraphic()
    {
      this.Image = new Gtk.Image();
      this.Child = Image;
      Image.Show();
    }

    public Gtk.Image Image { get; private set; }
  }

  public sealed class GtkOverlay : GtkControl
  {
    public GtkOverlay()
    {
      this.Fixed = new Gtk.Fixed();
      this.Child = Fixed;
      Fixed.SizeAllocated += (Sender, Event) =>
      {
        //Debug.WriteLine(Event.Allocation.Width + " x " + Event.Allocation.Height);

        foreach (var Child in Fixed.Children)
          Child.SizeAllocate(new Gdk.Rectangle(0, 0, Event.Allocation.Width, Event.Allocation.Height));
      };
      Fixed.Show();
    }

    public Gtk.Fixed Fixed { get; private set; }
  }

  public sealed class GtkEdit : GtkControl
  {
    public GtkEdit()
    {
      this.Entry = new Gtk.Entry();
      this.Child = Entry;
      Entry.ShadowType = Gtk.ShadowType.None;
      Entry.Show();
    }

    public Gtk.Entry Entry { get; private set; }
  }

  public sealed class GtkFrame : GtkControl
  {
    public GtkFrame()
    {
    }
  }

  public sealed class GtkLabel : GtkControl
  {
    public GtkLabel()
    {
      this.Label = new Gtk.Label();
      this.Child = Label;
      Label.SetAlignment(0.5F, 0.5F); // centered.
      Label.Show();
    }

    public Gtk.Label Label { get; private set; }
  }

  public sealed class GtkMemo : GtkControl
  {
    public GtkMemo()
    {
      this.TextView = new Gtk.TextView();
      this.Child = TextView;
      TextView.AcceptsTab = true;
      TextView.BorderWidth = 0;
      TextView.Show();
    }

    public Gtk.TextView TextView { get; private set; }
  }

  public sealed class GtkCanvas : GtkControl
  {
    public GtkCanvas()
    {
    }
  }

  public sealed class GtkScroll : GtkControl
  {
    public GtkScroll()
    {
      this.ScrolledWindow = new Gtk.ScrolledWindow();
      this.Child = ScrolledWindow;
      ScrolledWindow.Show();
    }

    public Gtk.ScrolledWindow ScrolledWindow { get; private set; }
  }

  public sealed class GtkStack : GtkControl
  {
    public GtkStack(bool Horizontal)
    {
      this.Box = Horizontal ? (Gtk.Box)new Gtk.HBox(false, 0) : (Gtk.Box)new Gtk.VBox(false, 0);
      this.Child = Box;
      Box.Show();
    }

    public Gtk.Box Box { get; private set; }

    internal void Compose(IEnumerable<Gtk.Widget> Widgets)
    {
      // TODO: remove all.
      foreach (var Child in Box.Children)
        Box.Remove(Child);

      foreach (var Widget in Widgets)
        Box.PackStart(Widget, false, false, 0);
    }
  }
}
