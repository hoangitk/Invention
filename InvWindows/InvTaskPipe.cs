﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using Inv.Support;

namespace Inv
{
  public sealed class TaskPipeManager
  {
    public TaskPipeManager(int PipeLimit)
    {
      this.PipeLimit = PipeLimit;
      this.PipeList = new Inv.DistinctList<IPipe>();
    }

    public int PipeLimit { get; private set; }

    public TaskPipe<T> AddPipe<T>()
    {
      var Pipe = new TaskPipe<T>(PipeLimit);

      PipeList.Add(Pipe);

      return Pipe;
    }
    public void Process(int Timeout)
    {
      var PipeIndex = EventWaitHandle.WaitAny(PipeList.Select(Index => Index.WaitHandle).ToArray(), Timeout);

      if (PipeIndex != WaitHandle.WaitTimeout)
        PipeList[PipeIndex].Process();
    }
    public void Process(TimeSpan TimeSpan)
    {
      var PipeIndex = EventWaitHandle.WaitAny(PipeList.Select(Index => Index.WaitHandle).ToArray(), TimeSpan);

      if (PipeIndex != WaitHandle.WaitTimeout)
        PipeList[PipeIndex].Process();
    }

    private Inv.DistinctList<IPipe> PipeList;
  }

  internal interface IPipe
  {
    EventWaitHandle WaitHandle { get; }

    void Process();
  }

  public sealed class TaskPipe<T> : IPipe
  {
    public TaskPipe(int Limit)
    {
      this.Limit = Limit;
      this.WaitHandle = new EventWaitHandle(false, EventResetMode.ManualReset);
      this.Queue = new Queue<T>();
    }

    public event Action<T> ProcessEvent;

    public void Post(T Item)
    {
      lock (Queue)
      {
        if (Queue.Count == 0)
          WaitHandle.Set();

        Queue.Enqueue(Item);

        while (Queue.Count > Limit)
          Queue.Dequeue();
      }
    }
    public void Process(int Timeout)
    {
      if (WaitHandle.WaitOne(Timeout))
        ProcessInvoke();
    }
    public void Process(TimeSpan TimeSpan)
    {
      if (WaitHandle.WaitOne(TimeSpan))
        ProcessInvoke();
    }

    private void ProcessInvoke()
    {
      lock (Queue)
      {
        if (Queue.TryDequeue(out var Item))
        {
          if (Queue.Count == 0)
            WaitHandle.Reset();

          ProcessEvent(Item);
        }
      }
    }

    EventWaitHandle IPipe.WaitHandle
    {
      get { return WaitHandle; }
    }
    void IPipe.Process()
    {
      ProcessInvoke();
    }

    private EventWaitHandle WaitHandle;
    private Queue<T> Queue;
    private int Limit;
  }
}
