﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  /// <summary>
  /// Global assertion manager used in the Inv projects.
  /// This is different to System.Diagnostics.Assert() as it is a runtime option.
  /// It is recommended to disable assertions in your RELEASE builds.
  /// </summary>
  public static class Assert
  {
    static Assert()
    {
      // Assertions are enabled by default for DEBUG mode and NUGET packages.
#if DEBUG || NUGET
      IsEnabled = true;
#else
      IsEnabled = false;
#endif
    }

    /// <summary>
    /// Assertions are enabled by default.
    /// </summary>
    public static bool IsEnabled { get; private set; }

    /// <summary>
    /// Enable assertions.
    /// </summary>
    public static void Enable()
    {
      IsEnabled = true;
    }
    /// <summary>
    /// Disable assertions.
    /// </summary>
    public static void Disable()
    {
      IsEnabled = false;
    }

    /// <summary>
    /// If the condition is false then throw an exception with the provided message.
    /// </summary>
    /// <param name="Condition"></param>
    /// <param name="Message"></param>
    [DebuggerNonUserCode]
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Check(bool Condition, string Message)
    {
      if (IsEnabled && !Condition)
        throw new Exception(Message);
    }
    /// <summary>
    /// If the condition is false then throw an exception with the formatted message.
    /// The string is only formatted when the condition is false.
    /// </summary>
    /// <param name="Condition"></param>
    /// <param name="Format"></param>
    /// <param name="ArgumentArray"></param>
    [DebuggerNonUserCode]
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Check(bool Condition, string Format, params object[] ArgumentArray)
    {
      if (IsEnabled && !Condition)
        throw new Exception(string.Format(CultureInfo.InvariantCulture, Format, ArgumentArray));
    }
    /// <summary>
    /// Throw an exception with the provided message.
    /// </summary>
    /// <param name="Message"></param>
    [DebuggerNonUserCode]
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void Fail(string Message)
    {
      if (IsEnabled)
        throw new Exception(Message);
    }
    /// <summary>
    /// Throw an ArgumentNullException if the provided variable is null.
    /// </summary>
    /// <param name="ArgumentObject"></param>
    /// <param name="ArgumentName"></param>
    [DebuggerNonUserCode]
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void CheckNotNull(object ArgumentObject, string ArgumentName)
    {
      if (IsEnabled && ArgumentObject == null)
        throw new ArgumentNullException(ArgumentName);
    }
  }
}
