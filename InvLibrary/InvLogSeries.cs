﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Inv.Support;

namespace Inv
{
  public interface ILogDirectory
  {
    Stream StartFile(string Name);
    Stream OpenFile(string Name);
    void DeleteFile(string Name);
    bool ExistsFile(string Name);
    IEnumerable<string> GetFiles(string SearchMask);
  }

  /// <summary>
  /// Represents a folder containing multiple log file series.
  /// </summary>
  /// <remarks>
  /// <para>After creating a <see cref="LogFolder"/> object, create one or more <see cref="LogSeries"/> objects using <see cref="AddSeries"/> and then call <see cref="LogSeries.Acquire"/>
  /// on the <see cref="LogSeries"/> object(s) to receive <see cref="LogHandle"/> objects which can be used to write to disk.</para>
  /// <para>Unlike Inv.LogFile, actual writes to the log file are unmanaged; <see cref="LogHandle"/> directly exposes a <see cref="Stream"/>, so for logs that comprise mostly
  /// string records, Inv.LogFile will probably be more useful.</para>
  /// </remarks>
  /// <example>
  /// <para>This example will write a single log entry to the 'series1' log series in C:\Temp. Each time the code is run, a new log file will be generated with 
  /// an incrementing filename; files older than 30 days (by default) will be automatically deleted.</para>
  /// <code>
  /// LogFolder folder = new LogFolder("C:\\Temp");
  /// LogSeries series = folder.AddSeries("series1");
  ///
  /// folder.Open();
  ///
  /// using (LogContext ctx = series.AcquireContext())
  /// {
  ///   StreamWriter writer = new StreamWriter(ctx.FileStream);
  ///
  ///   writer.WriteLine("Log entry");
  ///
  ///   writer.Close();
  /// }
  /// </code>
  /// </example>
  public sealed class LogFolder
  {
    /// <summary>
    /// Initializes a new <see cref="LogFolder"/> with the specified path.
    /// </summary>
    /// <param name="Directory">The interface to an implementation of the log directory.</param>
    public LogFolder(Inv.ILogDirectory Directory)
    {
      this.Directory = Directory;
      this.LogSeriesList = new Inv.DistinctList<LogSeries>();
    }

    /// <summary>
    /// Add a new series of log files with the specified extension and name to the <see cref="LogFolder"/>.
    /// </summary>
    /// <param name="Name">The name to give the new <see cref="LogSeries"/>.</param>
    /// <param name="Title">The title to give the new <see cref="LogSeries"/>.</param>
    /// <param name="Extension">The extension of the log files in the new <see cref="LogSeries"/>.</param>
    /// <returns>The new <see cref="LogSeries"/> object.</returns>
    public LogSeries AddSeries(string Name, string Title, string Extension)
    {
      var Result = new LogSeries(this, Name, Title, Extension);

      LogSeriesList.Add(Result);

      return Result;
    }
    /// <summary>
    /// Open the log folder, creating it if necessary.
    /// </summary>
    public void Open()
    {
      foreach (var LogSeries in LogSeriesList)
        LogSeries.Open();
    }
    /// <summary>
    /// Close all the <see cref="LogSeries"/> associated with this <see cref="LogFolder"/>.
    /// </summary>
    public void Close()
    {
      foreach (var LogSeries in LogSeriesList)
        LogSeries.Close();
    }
    /// <summary>
    /// Request to break the log file to the new day, instead of waiting for the first log line of the new day to be written.
    /// </summary>
    public void Rollover()
    {
      foreach (var LogSeries in LogSeriesList)
        LogSeries.Rollover();
    }
    public IEnumerable<Inv.LogSeries> EnumerateSeries()
    {
      return LogSeriesList;
    }

    internal Inv.ILogDirectory Directory { get; private set; }

    private Inv.DistinctList<Inv.LogSeries> LogSeriesList;
  }

  /// <summary>
  /// Represents a series of log files.
  /// </summary>
  /// <remarks>
  /// <para><see cref="LogSeries"/> objects are not directly instantiated. Instead, use <see cref="LogFolder.AddSeries">LogFolder.AddSeries(String)</see> to obtain a new <see cref="LogSeries"/> object.</para>
  /// <para><see cref="LogSeries"/> manages creating and deleting individual log files automatically; actual log output is done via a <see cref="LogHandle"/> obtained by
  /// calling <see cref="Acquire"/>.</para>
  /// <para>By default, log files managed by a <see cref="LogSeries"/> are deleted after 30 days. To modify this, set an appropriate value for <see cref="RelevantTimeSpan"/>.</para>
  /// </remarks>
  /// <example>See <see cref="LogFolder"/> for a code example.</example>
  public sealed class LogSeries
  {
    internal LogSeries(Inv.LogFolder Folder, string Name, string Title, string Extension)
    {
      this.Folder = Folder;
      this.Title = Title;
      this.Name = Name;
      this.Extension = Extension;
      this.DateFormat = "yyyy-MM-dd";
      this.ActiveHandleSet = new HashSet<LogHandle>();
      this.RelevantTimeSpan = TimeSpan.FromDays(30);
      this.CriticalSection = new Inv.ExclusiveCriticalSection("InvLogFile-" + Name);
    }

    /// <summary>
    /// The <see cref="LogFolder"/> this <see cref="LogSeries"/> writes log files into.
    /// </summary>
    public Inv.LogFolder Folder { get; private set; }
    /// <summary>
    /// The display name for log files managed by this <see cref="LogSeries"/>.
    /// </summary>
    public string Name { get; private set; }
    /// <summary>
    /// The file name prefix for log files managed by this <see cref="LogSeries"/>.
    /// </summary>
    public string Title { get; private set; }
    /// <summary>
    /// The file extension for log files managed by this <see cref="LogSeries"/>.
    /// </summary>
    public string Extension { get; private set; }
    public string DateFormat { get; private set; }
    /// <summary>
    /// The timespan for which log files managed by this <see cref="LogSeries"/> stay relevant.
    /// </summary>
    public TimeSpan RelevantTimeSpan { get; private set; }
    public bool IsActive { get; private set; }

    /// <summary>
    /// Acquire a <see cref="LogHandle"/> representing the active log file managed by this <see cref="LogSeries"/>.
    /// </summary>
    /// <remarks>
    /// <see cref="Acquire"/> will not create the initial log context, and will throw an exception if you do not first call <see cref="LogFolder.Open"/> 
    /// on the <see cref="LogFolder"/> containing this <see cref="LogSeries"/>.
    /// </remarks>
    /// <returns>A <see cref="LogHandle"/> that can be used to log to disk.</returns>
    public Inv.LogHandle Acquire()
    {
      using (CriticalSection.Lock())
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(ActiveWriter == null, "Series must not have an active writer.");

        return AcquireHandle();
      }
    }
    public void Release(Inv.LogHandle LogHandle)
    {
      using (CriticalSection.Lock())
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(ActiveWriter == null, "Series must not have an active writer.");

        ReleaseHandle(LogHandle);
      }
    }
    public Inv.LogWriter AsWriter()
    {
      using (CriticalSection.Lock())
      {
        if (ActiveWriter == null)
          this.ActiveWriter = new Inv.LogWriter(this);

        return ActiveWriter;
      }
    }
    public string CurrentFileName()
    {
      return AcquireHandle()?.FileName;
    }

    internal readonly Inv.ExclusiveCriticalSection CriticalSection;

    internal void Open()
    {
      using (CriticalSection.Lock())
      {
        if (!this.IsActive)
        {
          this.IsActive = true;

          DeleteLogFiles();

          ActiveHandleSet.Clear();

          OpenLogFile(Inv.Date.Now);
        }
      }
    }
    internal void Close()
    {
      using (CriticalSection.Lock())
      {
        if (this.IsActive)
        {
          this.IsActive = false;

          if (CurrentHandle != null)
          {
            CloseLogFile(CurrentHandle);
            this.CurrentHandle = null;
          }

          if (Inv.Assert.IsEnabled)
            Inv.Assert.Check(ActiveHandleSet.Count == 0, "LogFolder must not have any active contexts when it is closed.");

          foreach (var ActiveFileContext in ActiveHandleSet)
            CloseLogFile(ActiveFileContext);

          ActiveHandleSet.Clear();
        }
      }
    }
    internal void Rollover()
    {
      // acquire and release the context should force the logs to immediately rollover to the next day (if it is actually the next day).
      using (CriticalSection.Lock())
        ReleaseHandle(AcquireHandle());
    }
    internal Inv.LogHandle AcquireHandle()
    {
      //using (LockObject.Lock()) // NOTE: this used from within a lock already.
      {
        if (CurrentHandle != null)
        {
          var CurrentDate = Inv.Date.Now;

          if (CurrentHandle.Timestamp != CurrentDate)
          {
            if (CurrentHandle.ReferenceCount <= 0)
              CloseLogFile(CurrentHandle);
            else
              ActiveHandleSet.Add(CurrentHandle);

            OpenLogFile(CurrentDate);

            // Breaking over a day, cleanup the old logs. This is important for when a service is running for longer than 30days.
            DeleteLogFiles();
          }

          CurrentHandle.Increment();
        }

        return CurrentHandle;
      }
    }
    internal void ReleaseHandle(Inv.LogHandle LogHandle)
    {
      //using (LockObject.Lock()) // NOTE: this used from within a lock already.
      {
        if (LogHandle != null)
        {
          LogHandle.Decrement();

          if (CurrentHandle != LogHandle && LogHandle.ReferenceCount <= 0)
          {
            CloseLogFile(LogHandle);

            ActiveHandleSet.Remove(LogHandle);
          }
        }
      }
    }
    internal void DisposeWriter(Inv.LogWriter LogWriter)
    {
      using (CriticalSection.Lock())
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(this.ActiveWriter == LogWriter, "LogWriter mismatch");

        this.ActiveWriter = null;
      }
    }

    private void OpenLogFile(Inv.Date LogDate)
    {
      this.CurrentHandle = new LogHandle(this, LogDate);
      
      var DateText = LogDate.ToString(DateFormat);
      var SearchMask = string.Format("{0} {1},*.{2}", Title, DateText, Extension);
      var QueryMask = new Regex(string.Format("{0} {1}\\, ([0-9]*)\\.{2}", Title, DateText, Extension));
      var LastSerialNumber = 0;

      foreach (var LogFile in Folder.Directory.GetFiles(SearchMask))
      {
        var Match = QueryMask.Match(LogFile);

        if (Match.Success)
        {
          var SerialText = Match.Groups[1].Value.Trim();

          if (int.TryParse(SerialText, out var SerialNumber))
          {
            if (SerialNumber > LastSerialNumber)
              LastSerialNumber = SerialNumber;
          }
        }
      }

      CurrentHandle.Stream = null;
      CurrentHandle.FileName = null;

      var AttemptCount = 0;
      do
      {
        LastSerialNumber++;

        var FileName = string.Format("{0} {1}, {2:000}.{3}", Title, DateText, LastSerialNumber, Extension);
        try
        {
          CurrentHandle.Stream = Folder.Directory.StartFile(FileName);
          CurrentHandle.FileName = FileName;
        }
        catch (Exception Exception)
        {
          AttemptCount++;

          if (AttemptCount > 5)
            throw Exception.Preserve();
        }
      }
      while (CurrentHandle.Stream == null);
    }
    private void CloseLogFile(Inv.LogHandle CloseHandle)
    {
      if (CloseHandle != null)
      {
        CloseHandle.Stream.Dispose();
        CloseHandle.Stream = null;
        CloseHandle.FileName = null;
      }
    }
    private void DeleteLogFiles()
    {
      try
      {
        var SearchMask = Title + " *." + Extension;
        var QueryMask = new Regex(Title + " (.*)\\, [0-9]*\\." + Extension);
        var DeleteDate = DateTime.Now - RelevantTimeSpan;

        foreach (var LogFile in Folder.Directory.GetFiles(SearchMask))
        {
          var Match = QueryMask.Match(LogFile);

          if (Match.Success && DateTime.TryParse(Match.Groups[1].Value, out var FileDate))
          {
            if (FileDate < DeleteDate)
            {
              try
              {
                Folder.Directory.DeleteFile(LogFile);
              }
              catch
              {
                // NOTE: unable to delete legacy log file, move on.
              }
            }
          }
        }
      }
      catch
      {
        // NOTE: unable to delete legacy log file, move on.

        if (Debugger.IsAttached)
          Debugger.Break();
      }
    }

    private HashSet<Inv.LogHandle> ActiveHandleSet;
    private Inv.LogHandle CurrentHandle;
    private Inv.LogWriter ActiveWriter;
  }

  /// <summary>
  /// Represents a logging context to which log entries can be written.
  /// </summary>
  /// <remarks>
  /// <para><see cref="LogHandle"/> provides an abstracted interface to a <see cref="Stream"/> representing a log file on disk. The actual log file name is
  /// determined by the parent <see cref="LogSeries"/> which created the <see cref="LogHandle"/>.</para>
  /// <para>To obtain a <see cref="LogHandle"/>, call <see cref="LogSeries.Acquire"/>.</para>
  /// </remarks>
  /// <example>See <see cref="LogFolder"/> for a code example.</example>
  public sealed class LogHandle
  {
    internal LogHandle(Inv.LogSeries Series, Inv.Date Timestamp)
    {
      this.Series = Series;
      this.Timestamp = Timestamp;
    }

    /// <summary>
    /// Gets a <see cref="Stream"/> object representing the on-disk log file managed by this <see cref="LogHandle"/>.
    /// </summary>
    public Stream Stream { get; internal set; }
    public string FileName { get; internal set; }

    public void Release()
    {
      Series.Release(this);
    }

    /// <summary>
    /// Releases all resources used by the <see cref="LogHandle"/>.
    /// </summary>
    internal Inv.LogSeries Series { get; }
    internal Inv.Date Timestamp { get; }
    internal int ReferenceCount { get; private set; }

    internal void Increment()
    {
      ReferenceCount++;
    }
    internal void Decrement()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(ReferenceCount > 0, "Reference count must be a positive integer");

      ReferenceCount--;
    }
  }

  public sealed class LogWriter : IDisposable
  {
    internal LogWriter(Inv.LogSeries LogSeries)
    {
      this.LogSeries = LogSeries;
      this.StreamWriter = null;
    }
    public void Dispose()
    {
      using (LogSeries.CriticalSection.Lock())
      {
        if (StreamWriter != null)
        {
          if (ReleaseEvent != null)
            ReleaseEvent(StreamWriter);

          StreamWriter.Flush();
          StreamWriter = null;
        }

        if (LogContext != null)
        {
          LogSeries.ReleaseHandle(LogContext);
          LogContext = null;
        }
      }

      LogSeries.DisposeWriter(this);
    }

    public event Action<StreamWriter> AcquireEvent;
    public event Action<StreamWriter> ReleaseEvent;

    public void Write(Action<StreamWriter> WriteAction)
    {
      using (LogSeries.CriticalSection.Lock())
      {
        var AcquireLogContext = LogSeries.AcquireHandle();

        if (AcquireLogContext != null)
        {
          if (AcquireLogContext == LogContext)
          {
            LogSeries.ReleaseHandle(AcquireLogContext);
          }
          else
          {
            if (StreamWriter != null)
            {
              if (ReleaseEvent != null)
                ReleaseEvent(StreamWriter);

              StreamWriter.Flush();
              StreamWriter = null;
            }

            LogSeries.ReleaseHandle(LogContext);
            LogContext = AcquireLogContext;
          }

          if (StreamWriter == null)
          {
            StreamWriter = new StreamWriter(AcquireLogContext.Stream);

            if (AcquireEvent != null)
              AcquireEvent(StreamWriter);
          }

          WriteAction(StreamWriter);

          StreamWriter.Flush();
        }
      }
    }
    public void WriteLine(string Line)
    {
      var Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.CurrentCulture) + "\t" + Line;

      Write(SW => SW.WriteLine(Text));
    }
    public void WriteRecord(params string[] FieldArray)
    {
      WriteLine(FieldArray.AsSeparatedText("\t"));
    }

    private Inv.LogSeries LogSeries;
    private Inv.LogHandle LogContext;
    private StreamWriter StreamWriter;
  }
}
