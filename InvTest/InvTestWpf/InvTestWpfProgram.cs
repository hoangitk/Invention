﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace InvTest
{
  public class Program
  {
    [STAThread]
    static void Main(string[] args)
    {
      Inv.WpfShell.CheckRequirements(() =>
      {
        Inv.WpfShell.Options.DeviceEmulation = Inv.WpfDeviceEmulation.iPadPro;
        Inv.WpfShell.Options.DeviceEmulationRotated = true;
        Inv.WpfShell.Options.FullScreenMode = false;
        Inv.WpfShell.Options.DefaultWindowWidth = 1920;
        Inv.WpfShell.Options.DefaultWindowHeight = 1080;

        Inv.WpfShell.RunBridge(B =>
        {
          /*
          // NOTE: example of mapping to actual wpf control.
          var InvSurface = B.Application.Window.NewSurface();

          var InvFrame = InvSurface.NewFrame();
          InvSurface.Content = InvFrame;

          var WpfFrame = B.GetFrame(InvFrame);

          var ProgressBar = new System.Windows.Controls.ProgressBar();
          WpfFrame.AddChild(ProgressBar);
          ProgressBar.Value = 5;
          ProgressBar.Maximum = 10;
          ProgressBar.Width = 400;
          ProgressBar.Height = 20;

          B.Application.Window.Transition(InvSurface);
          */
          InvTest.Shell.Install(B.Application);
        });
      });
    }
  }
}
