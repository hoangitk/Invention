﻿using System;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Diagnostics;
using Inv.Support;

namespace Inv
{
  public class PlayProgram
  {
    [STAThread]
    static void Main(string[] args)
    {
      // we need to default assertions to enabled.
      // otherwise the RELEASE build of InvPlay will disable assertions.
      // assertions can be turned off inside the target project if required.
      Inv.Assert.Enable(); 

#if DEBUG
      const string Fallback = "DEBUG";
#elif RELEASE
      const string Fallback = "RELEASE";
#else
      const string Fallback = "";
#endif

      var ProjectPath = args.Length > 0 ? args[0] : null;
      var BuildPlatform = args.Length > 1 ? args[1] : "ANYCPU";
      var ConditionSymbols = args.Length > 2 ? args[2] : Fallback;

#if DEBUG
      if (ProjectPath == null)
      //ProjectPath = @"C:\Development\Forge\Inv\InvManual\InvManual\InvManual.csproj";
      //ProjectPath = @"C:\Development\Forge\Inv\InvTest\InvTest\InvTest.csproj";
      ProjectPath = @"C:\Development\Forge\Karisma\KarismaPlay\KarismaPlay.csproj";

      System.Net.ServicePointManager.ServerCertificateValidationCallback += (Sender, Cert, Chain, Error) => true;
#endif

      PlayShell.Run(ProjectPath, BuildPlatform, ConditionSymbols);
    }
  }

  public static class PlayShell
  {
    public static void Run(string ProjectFilePath, string BuildPlatform, string ConditionalSymbols)
    {
      Inv.WpfShell.CheckRequirements(() =>
      {
        Inv.WpfShell.Options.PreventDeviceEmulation = false;
        Inv.WpfShell.Options.DeviceEmulation = Inv.WpfDeviceEmulation.iPhoneX;
        Inv.WpfShell.Options.DeviceEmulationArray = new[] { Inv.WpfDeviceEmulation.iPhone5, Inv.WpfDeviceEmulation.iPhone6_7, Inv.WpfDeviceEmulation.iPhone6_7Plus, Inv.WpfDeviceEmulation.iPhoneX, Inv.WpfDeviceEmulation.iPad_Mini, Inv.WpfDeviceEmulation.iPadPro };
        Inv.WpfShell.Options.DeviceEmulationRotated = false;
        Inv.WpfShell.Options.DefaultWindowWidth = 1280;
        Inv.WpfShell.Options.DefaultWindowHeight = 1024;
        Inv.WpfShell.Options.FullScreenMode = false;

        var MissingSpec = ProjectFilePath == null;
        var IncorrectSpec = !MissingSpec && !string.Equals(System.IO.Path.GetExtension(ProjectFilePath), ".csproj", StringComparison.InvariantCultureIgnoreCase);
        var MissingFile = !MissingSpec && !IncorrectSpec && !System.IO.File.Exists(ProjectFilePath);

        if (MissingSpec || IncorrectSpec || MissingFile)
        {
          Inv.WpfShell.Run(A =>
          {
            var ErrorSurface = A.Window.NewSurface();

            string ErrorCause;
            if (MissingSpec)
              ErrorCause = ".csproj file path was not provided.";
            else if (IncorrectSpec)
              ErrorCause = "Provided file path must have a .csproj extension.";
            else if (MissingFile)
              ErrorCause = ".csproj file path does not exist:";
            else
              throw new Exception("Error case not handled.");

            var ErrorMessage = 
              "InvPlay requires a single command line parameter which is the full path of your Invention .csproj" + Environment.NewLine + Environment.NewLine +
              "E.g." + Environment.NewLine +
             @"    InvPlay.exe ""C:\Dev\ExampleProject.csproj""" + Environment.NewLine + Environment.NewLine +
             ErrorCause + (!MissingSpec ? Environment.NewLine + Environment.NewLine + ProjectFilePath : "");

            A.TransitionError(ErrorMessage, Inv.Colour.DarkRed);
          });
        }
        else
        {
          Inv.WpfShell.Options.MainFolderPath = Path.Combine(Inv.WpfShell.Options.MainFolderPath, Path.GetFileNameWithoutExtension(ProjectFilePath));
          System.IO.Directory.CreateDirectory(Inv.WpfShell.Options.MainFolderPath);

          new PlayApplication(ProjectFilePath, BuildPlatform, ConditionalSymbols.Split(';', StringSplitOptions.RemoveEmptyEntries)).Run();
        }
      });
    }

    internal static void TransitionError(this Inv.Application Application, string Message, Inv.Colour Colour)
    {
      var Surface = Application.Window.NewSurface();
      Surface.Background.Colour = Colour;

      var Memo = Surface.NewMemo();
      Surface.Content = Memo;
      Memo.Font.Monospaced();
      Memo.Font.Colour = Inv.Colour.White;
      Memo.Font.Size = 18;
      Memo.Alignment.Center();
      Memo.IsReadOnly = true;
      Memo.Text = Message;

      Application.Window.Transition(Surface);
    }
  }

  public sealed class PlayApplication
  {
    internal PlayApplication(string ProjectFilePath, string BuildPlatform, string[] ConditionSymbolArray)
    {
      this.ProjectFilePath = ProjectFilePath;
      this.BuildPlatform = Inv.Support.EnumHelper.ParseOrDefault<Inv.RoslynBuildPlatform>(BuildPlatform.StripWhitespace(), null, true) ?? RoslynBuildPlatform.AnyCPU;
      this.ConditionSymbolArray = ConditionSymbolArray;
      this.WpfEngine = new WpfEngine();
    }

    public void Run()
    {
      using (var PlayEnvironment = new RoslynEnvironment(new RoslynSpecification(ProjectFilePath, BuildPlatform, ConditionSymbolArray)))
      {
        PlayEnvironment.ChangeEvent += (ForceReload, ForceResources) =>
        {
          WpfEngine.Post(() =>
          {
            WpfEngine.Uninstall();
            try
            {
              Execute(PlayEnvironment, ForceReload, ForceResources);
            }
            finally
            {
              WpfEngine.Restart();
            }
          });
        };

        Execute(PlayEnvironment, true, true);

        WpfEngine.Run();
      }
    }

    private void Execute(RoslynEnvironment PlayEnvironment, bool ForceReload, bool ForceResources)
    {
      var Application = new Inv.Application();
      WpfEngine.Install(Application);

      // regenerate the resources.
      if (ForceResources)
        new Stopwatch().Measure("__InvGen__", () => Inv.GenShell.Execute(ProjectFilePath));

      var PlayPackage = PlayEnvironment.Load(ForceReload);
      try
      {
        var ResultMessage = PlayPackage.Messages;
        var ResultColour = Inv.Colour.Black;

        if (PlayPackage.Assembly != null)
        {
          foreach (var CandidateType in PlayPackage.Assembly.ExportedTypes)
          {
            // ignore non-static classes and the special 'Resources' class.
            if (!CandidateType.IsAbstract || CandidateType.Name == "Resources")
              continue;

            foreach (var RuntimeMethod in CandidateType.GetRuntimeMethods())
            {
              if (!RuntimeMethod.IsSpecialName && RuntimeMethod.ReturnType == typeof(void) && RuntimeMethod.GetParameters().Length == 1 && RuntimeMethod.GetParameters()[0].ParameterType == typeof(Inv.Application))
              {
                var AssetFolderPathField = CandidateType.GetField("AssetFolderPath");
                if (AssetFolderPathField != null)
                  Inv.WpfShell.Options.OverrideAssetFolderPath = (string)AssetFolderPathField.GetValue(null);
                else
                  Inv.WpfShell.Options.OverrideAssetFolderPath = null;

                RuntimeMethod.Invoke(null, new object[] { Application });
                return; // SUCCESS.
              }
            }
          }

          ResultColour = Inv.Colour.DarkSlateGray;
          ResultMessage =
@"Invention main method was not found in the project.

" +  PlayPackage.Assembly.FullName +
@"
There must be a public static class with a public static void method that takes an Inv.Application parameter. For example:

namespace MyProject
{
  public static class Shell
  {
    public static void Install(Inv.Application Application)
    {
      // application logic goes here.
    }
  }
}";
        }

        Application.TransitionError(ResultMessage, ResultColour);
      }
      catch (Exception Exception)
      {
        Application.TransitionError(Exception.AsReport(), Inv.Colour.DarkRed);
      }
    }

    private WpfEngine WpfEngine;
    private string ProjectFilePath;
    private string[] ConditionSymbolArray;
    private Inv.RoslynBuildPlatform BuildPlatform;
  }
}