﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  internal static class UwaStyles
  {
    static UwaStyles()
    {
      var StyleDictionary = new Windows.UI.Xaml.ResourceDictionary();
      StyleDictionary.Source = new Uri("ms-appx:///" + typeof(UwaShell).GetReflectionAssembly().GetName().Name + "/InvUwaStyles.xaml", UriKind.RelativeOrAbsolute);

      ButtonStyle = GetStyle(StyleDictionary, "InvUwaButton");
      PlainMemoStyle = GetStyle(StyleDictionary, "InvUwaPlainMemo");
      RichMemoStyle = GetStyle(StyleDictionary, "InvUwaRichMemo");
      TextBoxStyle = GetStyle(StyleDictionary, "InvUwaTextBox");
      PasswordBoxStyle = GetStyle(StyleDictionary, "InvUwaPasswordBox");
    }

    public static readonly Windows.UI.Xaml.Style ButtonStyle;
    public static readonly Windows.UI.Xaml.Style PlainMemoStyle;
    public static readonly Windows.UI.Xaml.Style RichMemoStyle;
    public static readonly Windows.UI.Xaml.Style TextBoxStyle;
    public static readonly Windows.UI.Xaml.Style PasswordBoxStyle;

    private static Windows.UI.Xaml.Style GetStyle(Windows.UI.Xaml.ResourceDictionary Dictionary, string Name)
    {
      return (Windows.UI.Xaml.Style)Dictionary[Name];
    }
  }

  internal interface UwaOverrideFocusContract
  {
    void OverrideFocus();
  }

  public abstract class UwaPanel : Windows.UI.Xaml.Controls.ContentControl
  {
    internal UwaPanel()
    {
      this.Border = new Windows.UI.Xaml.Controls.Border(); // border is a sealed class.
      Content = Border;

      HorizontalContentAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
      VerticalContentAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;
    }

    public Windows.UI.Xaml.Controls.Border Border { get; private set; }
  }

  [Windows.UI.Xaml.TemplatePart(Name = "PART_Border", Type = typeof(Windows.UI.Xaml.Controls.Border))]
  public sealed class UwaButton : Windows.UI.Xaml.Controls.Button
  {
    internal UwaButton()
    {
      this.Style = UwaStyles.ButtonStyle;
      this.Border = new Windows.UI.Xaml.Controls.Border(); // need an extra border because PartBorder is only available after the template is applied.
      this.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
      this.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;
      this.IsFlat = true;
      this.IsTabStop = false;

      this.Loaded += (Sender, Event) =>
      {
        RefreshBackground();
      };
      this.Unloaded += (Sender, Event) =>
      {
        if (IsPushed)
        {
          this.IsLeftClicked = false;
          this.IsRightClicked = false;
          this.IsHovered = false;

          // NOTE: removing from the visual tree means 'released' may not be otherwise fired.
          ReleaseButtonPointer();
        }
        else
        {
          RefreshBackground();
        }
      };
      this.PointerEntered += (Sender, Event) =>
      {
        this.IsHovered = true;
        if (HoverOverEvent != null)
          HoverOverEvent();
        RefreshBackground();
      };
      this.PointerExited += (Sender, Event) =>
      {
        this.IsHovered = false;
        if (HoverAwayEvent != null)
          HoverAwayEvent();
        RefreshBackground();
      };
      this.PointerCanceled += (Sender, Event) =>
      {
        ReleaseButtonPointer();
      };
      this.PointerCaptureLost += (Sender, Event) =>
      {
        ReleaseButtonPointer();
      };
      /*#if DEBUG
      this.PointerPressed += (Sender, Event) =>
      {
        // NOTE: .PointerPressed += doesn't fire for buttons?
        if (Debugger.IsAttached)
          Debugger.Break();
      };
      this.PointerReleased += (Sender, Event) =>
      {
        // NOTE: .PointerReleased += doesn't fire for buttons?
        if (Debugger.IsAttached)
          Debugger.Break();
      };
      #endif*/

      this.AddHandler(Windows.UI.Xaml.Controls.Primitives.ButtonBase.PointerPressedEvent, new Windows.UI.Xaml.Input.PointerEventHandler((S, E) =>
      {
        this.IsHovered = true; // if we are pressed on it, we are hovered on it.

        CapturePointer(E.Pointer);

        var CurrentPoint = E.GetCurrentPoint(this);
        this.IsLeftClicked = !CurrentPoint.Properties.IsRightButtonPressed;
        this.IsRightClicked = CurrentPoint.Properties.IsRightButtonPressed;

        RefreshBackground();

        if (PressEvent != null)
          PressEvent();
      }), true);
      this.AddHandler(Windows.UI.Xaml.Controls.Primitives.ButtonBase.PointerReleasedEvent, new Windows.UI.Xaml.Input.PointerEventHandler((S, E) =>
      {
        ReleasePointerCapture(E.Pointer);

        this.IsLeftClicked = false;

        if (IsRightClicked)
        {
          this.IsRightClicked = false;

          if (IsHovered)
          {
            if (RightClickEvent != null)
              RightClickEvent();
          }
        }

        ReleaseButtonPointer();
      }), true);

      this.Click += (Sender, Event) =>
      {
        if (LeftClickEvent != null)
          LeftClickEvent();
      };
      this.IsEnabledChanged += (Sender, Event) => RefreshBackground();

      // NOTE: *tapped does not fire if you tap inside the button, move slightly and then release. 
      // Click event does fire as expected, but it fires before the pointer releaesd event.
      //Result.Tapped += (Sender, Event) =>
      //{
      //  if (InvApplication.Window.IsActiveSurface(P.Surface))
      //  {
      //    this.ActivatedButton = null;
      //    P.SingleTapInvoke();
      //  }
      //
      //  Event.Handled = true;
      //};
      //Result.RightTapped += (Sender, Event) =>
      //{
      //  if (InvApplication.Window.IsActiveSurface(P.Surface) && ActivatedButton == Result)
      //  {
      //    this.ActivatedButton = null;
      //    P.ContextTapInvoke();
      //  }
      //
      //  Event.Handled = true;
      //};
    }

    [Obsolete("Do not use! Use Border.Child instead.", true)]
    public new object Content
    {
      set { base.Content = value; }
    }
    public bool IsHovered { get; private set; }
    public bool IsPushed => IsLeftClicked || IsRightClicked;
    public Windows.UI.Xaml.Controls.Border Border { get; private set; }
    public bool IsFlat { get; set; }
    public Windows.UI.Xaml.Media.Brush NormalBackgroundBrush { get; set; }
    public Windows.UI.Xaml.Media.Brush HoverBackgroundBrush { get; set; }
    public Windows.UI.Xaml.Media.Brush PressedBackgroundBrush { get; set; }
    public event Action LeftClickEvent;
    public event Action RightClickEvent;
    public event Action HoverOverEvent;
    public event Action HoverAwayEvent;
    public event Action PressEvent;
    public event Action ReleaseEvent;

    public void RefreshBackground()
    {
      if (IsFlat)
      {
        if (IsPushed)
          Border.Background = PressedBackgroundBrush;
        else if (IsHovered)
          Border.Background = HoverBackgroundBrush;
        else
          Border.Background = NormalBackgroundBrush;

        Border.Opacity = IsEnabled ? 1.00 : 0.50;
      }
    }

    protected override void OnApplyTemplate()
    {
      base.OnApplyTemplate();

      this.PartBorder = this.GetTemplateChild("PART_Border") as Windows.UI.Xaml.Controls.Border;

      if (PartBorder != null)
        PartBorder.Child = Border;
    }

    private void ReleaseButtonPointer()
    {
      if (ReleaseEvent != null)
        ReleaseEvent();

      RefreshBackground();
    }

    private bool IsLeftClicked;
    private bool IsRightClicked;
    private Windows.UI.Xaml.Controls.Border PartBorder;
  }

  public sealed class UwaBoard : UwaPanel
  {
    internal UwaBoard()
    {
      this.Canvas = new Windows.UI.Xaml.Controls.Canvas();
      Border.Child = Canvas;
    }

    public Windows.UI.Xaml.Controls.Canvas Canvas { get; private set; }
  }

  public sealed class UwaBrowser : UwaPanel
  {
    internal UwaBrowser()
    {
      this.Inner = new Windows.UI.Xaml.Controls.WebView();
      Border.Child = Inner;

      //Inner.UnsafeContentWarningDisplaying += (Sender, Event) =>
      //{
      //  Debug.WriteLine("UnsafeContentWarning");
      //};
      //Inner.UnviewableContentIdentified += (Sender, Event) =>
      //{
      //  Debug.WriteLine("UnviewableContentIdentified");
      //};
      Inner.NavigationStarting += (Sender, Event) =>
      {
        if (BlockQuery != null && Event.Uri != null)
          Event.Cancel = BlockQuery(Event.Uri);
      };
      Inner.NavigationCompleted += (Sender, Event) =>
      {
        if (ReadyEvent != null)
          ReadyEvent(Event.Uri);
      };
    }

    public event Func<Uri, bool> BlockQuery;
    public event Action<Uri> ReadyEvent;

    public void Navigate(Uri Uri, string Html)
    {
      if (Html != null)
        Inner.NavigateToString(Html);
      else if (Uri != null)
        Inner.Navigate(Uri);
      else
        Inner.Navigate(new Uri("about:blank"));
    }

    private readonly Windows.UI.Xaml.Controls.WebView Inner;
  }

  public sealed class UwaVideo : UwaPanel
  {
    internal UwaVideo()
    {
      this.Inner = new Windows.UI.Xaml.Controls.MediaElement();
      Border.Child = Inner;
      Inner.AutoPlay = false;
      Inner.MediaOpened += (Sender, Event) => Inner.Play();
    }

    public void LoadUri(Uri Source)
    {
      this.SourceField = Source;
      Inner.Source = null; // have to 'play' after you set the uri.
    }

    public void Play()
    {
      if (Inner.Source != SourceField)
        Inner.Source = SourceField;
      else
        Inner.Play();
    }
    public void Pause()
    {
      Inner.Pause();
    }
    public void Stop()
    {
      Inner.Stop();
      Inner.Source = null; // This will close the stream attached to the media element and blank the control in the visual tree.
    }

    private readonly Windows.UI.Xaml.Controls.MediaElement Inner;
    private Uri SourceField;
  }

  public sealed class UwaFlow : UwaPanel
  {
    internal UwaFlow()
    {
      this.ScrollViewer = new Windows.UI.Xaml.Controls.ScrollViewer();
      Border.Child = ScrollViewer;
      ScrollViewer.VerticalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Auto;
      ScrollViewer.HorizontalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Disabled;

      this.StackPanel = new Windows.UI.Xaml.Controls.StackPanel();
      ScrollViewer.Content = StackPanel;
      StackPanel.Orientation = Windows.UI.Xaml.Controls.Orientation.Vertical;
    }

    public Func<int, Windows.UI.Xaml.FrameworkElement> HeaderContentQuery;
    public Func<int, Windows.UI.Xaml.FrameworkElement> FooterContentQuery;
    public Func<int> SectionCountQuery;
    public Func<int, int> ItemCountQuery;
    public Func<int, int, Windows.UI.Xaml.FrameworkElement> ItemContentQuery;

    public void Reload()
    {
      StackPanel.Children.Clear();

      var SectionCount = SectionCountQuery();

      for (var SectionIndex = 0; SectionIndex < SectionCount; SectionIndex++)
      {
        var HeaderContent = HeaderContentQuery(SectionIndex);
        if (HeaderContent != null)
          StackPanel.SafeAddChild(HeaderContent);

        var ItemCount = ItemCountQuery(SectionIndex);

        for (var ItemIndex = 0; ItemIndex < ItemCount; ItemIndex++)
        {
          var ItemContent = ItemContentQuery(SectionIndex, ItemIndex);

          if (ItemContent != null)
            StackPanel.SafeAddChild(ItemContent);
        }

        var FooterContent = FooterContentQuery(SectionIndex);
        if (FooterContent != null)
          StackPanel.SafeAddChild(FooterContent);
      }
    }
    public void ScrollTo(int Section, int Index)
    {
      // NOTE: idle async is required if the control has not yet been layed out in the visual tree.
      Dispatcher.RunIdleAsync(e =>
      {
        var ItemContent = ItemContentQuery(Section, Index);

        if (ItemContent != null)
        {
          var visual = ItemContent.TransformToVisual(ScrollViewer);
          var point = visual.TransformPoint(new Windows.Foundation.Point(0, 0));

          ScrollViewer.ChangeView(null, point.Y, null);
        }
      }).AsTask().Forget();
    }
    /*
    private int GetAbsoluteIndex(int Section, int Index)
    {
      var Position = 0;

      for (var SectionIndex = 0; SectionIndex <= Section; SectionIndex++)
      {
        if (HeaderContentQuery(SectionIndex) != null)
          Position++;

        if (SectionIndex == Section)
        {
          Position += Index;
          break;
        }

        Position += ItemCountQuery(SectionIndex);

        if (FooterContentQuery(SectionIndex) != null)
          Position++;
      }

      return Position;
    }
    */
    private readonly Windows.UI.Xaml.Controls.ScrollViewer ScrollViewer;
    private readonly Windows.UI.Xaml.Controls.StackPanel StackPanel;
  }

  /* // Clone of the Wpf Flow, lots of problems.
    public sealed class UwaFlowVirtual : UwaPanel
    {
      internal UwaFlowVirtual()
      {
        this.FlowItemsControl = new UwaFlowItemsControl();
        Border.Child = FlowItemsControl;

        Windows.UI.Xaml.Controls.ScrollViewer.SetVerticalScrollBarVisibility(FlowItemsControl, Windows.UI.Xaml.Controls.ScrollBarVisibility.Auto);
        Windows.UI.Xaml.Controls.ScrollViewer.SetHorizontalScrollBarVisibility(FlowItemsControl, Windows.UI.Xaml.Controls.ScrollBarVisibility.Disabled);
        //Windows.UI.Xaml.Controls.ScrollViewer.SetPanningMode(FlowItemsControl, Windows.UI.Xaml.Controls.PanningMode.VerticalOnly);

        this.ScrollTargetIndex = -1;

        UwaFlowVirtualizingStackPanel.AddScrollChangeHandler(FlowItemsControl, (Sender, E) =>
        {
          this.Panel = E.Panel;

          //Debug.WriteLine("Name: {0} ViewportHeight: {1}", E.Name, E.ViewportHeight);

          QueryItemRange((int)E.VerticalOffset, (int)(E.VerticalOffset + E.ViewportHeight));

          if (Panel != null && ScrollTargetIndex >= 0)
          {
            Panel.BringIndexIntoViewPublic(ScrollTargetIndex);
            this.ScrollTargetIndex = -1;
          }
        });
      }

      public Func<int, Windows.UI.Xaml.FrameworkElement> HeaderContentQuery;
      public Func<int, Windows.UI.Xaml.FrameworkElement> FooterContentQuery;
      public Func<int> SectionCountQuery;
      public Func<int, int> ItemCountQuery;
      public Func<int, int, Windows.UI.Xaml.FrameworkElement> ItemContentQuery;

      public void Reload()
      {
        this.PlaceholderCollection = new System.Collections.ObjectModel.ObservableCollection<Windows.UI.Xaml.FrameworkElement>();

        var SectionCount = SectionCountQuery();

        for (var SectionIndex = 0; SectionIndex < SectionCount; SectionIndex++)
        {
          var HeaderContent = HeaderContentQuery(SectionIndex);
          if (HeaderContent != null)
            AddPlaceholder(null);

          var ItemCount = ItemCountQuery(SectionIndex);

          for (int ItemIndex = 0; ItemIndex < ItemCount; ItemIndex++)
            AddPlaceholder(null);

          var FooterContent = FooterContentQuery(SectionIndex);
          if (FooterContent != null)
            AddPlaceholder(null);
        }

        FlowItemsControl.ItemsSource = PlaceholderCollection;

        if (FlowItemsControl.IsLoaded Panel != null)
          QueryItemRange((int)Panel.VerticalOffset, (int)(Panel.VerticalOffset + Panel.ViewportHeight));
      }
      public void ScrollTo(int Section, int Index)
      {
        var TargetIndex = GetAbsoluteIndex(Section, Index);

        if (Panel != null)
          Panel.BringIndexIntoViewPublic(TargetIndex);
        else
          this.ScrollTargetIndex = TargetIndex;
      }

      private UwaFlowItemsControl FlowItemsControl;
      private System.Collections.ObjectModel.ObservableCollection<Windows.UI.Xaml.FrameworkElement> PlaceholderCollection;
      private UwaFlowVirtualizingStackPanel Panel;
      private int ScrollTargetIndex;

      private void QueryItemRange(int FromIndex, int UntilIndex)
      {
        var SectionCount = SectionCountQuery();
        var AbsoluteIndex = 0;

        for (var SectionIndex = 0; SectionIndex < SectionCount; SectionIndex++)
        {
          var ItemCount = ItemCountQuery(SectionIndex);

          var HeaderContent = HeaderContentQuery(SectionIndex);
          if (HeaderContent != null)
          {
            if (AbsoluteIndex.Between(FromIndex, UntilIndex))
              AddOrUpdatePlaceholder(AbsoluteIndex, HeaderContent);

            AbsoluteIndex++;
          }

          for (int ItemIndex = 0; ItemIndex < ItemCount; ItemIndex++)
          {
            if (AbsoluteIndex.Between(FromIndex, UntilIndex))
              AddOrUpdatePlaceholder(AbsoluteIndex, ItemContentQuery(SectionIndex, ItemIndex));

            AbsoluteIndex++;
          }

          var FooterContent = FooterContentQuery(SectionIndex);
          if (FooterContent != null)
          {
            if (AbsoluteIndex.Between(FromIndex, UntilIndex))
              AddOrUpdatePlaceholder(AbsoluteIndex, FooterContent);

            AbsoluteIndex++;
          }
        }
      }
      private void AddPlaceholder(Windows.UI.Xaml.FrameworkElement Element)
      {
        PlaceholderCollection.Add(new Windows.UI.Xaml.Controls.ContentPresenter() { Height = Element != null ? double.NaN : 50, Content = Element });
      }
      private void AddOrUpdatePlaceholder(int AbsoluteIndex, Windows.UI.Xaml.FrameworkElement Element)
      {
        if (PlaceholderCollection.Count <= AbsoluteIndex)
        {
          AddPlaceholder(Element);
        }
        else
        {
          var ExistingContentPresenter = PlaceholderCollection.ElementAt(AbsoluteIndex) as Windows.UI.Xaml.Controls.ContentPresenter;
          if (ExistingContentPresenter != null)
          {
            ExistingContentPresenter.Content = Element;
            ExistingContentPresenter.Height = double.NaN;
          }
        }
      }
      private int GetAbsoluteIndex(int Section, int Index)
      {
        var Position = 0;

        for (var SectionIndex = 0; SectionIndex <= Section; SectionIndex++)
        {
          if (HeaderContentQuery(SectionIndex) != null)
            Position++;

          if (SectionIndex == Section)
          {
            Position += Index;
            break;
          }

          Position += ItemCountQuery(SectionIndex);

          if (FooterContentQuery(SectionIndex) != null)
            Position++;
        }

        return Position;
      }

      internal delegate void UwaFlowOffsetChangedEventHandler(object sender, UwaFlowOffsetChangedEventArgs e);

      internal class UwaFlowOffsetChangedEventArgs : Windows.UI.Xaml.RoutedEventArgs
      {
        internal UwaFlowOffsetChangedEventArgs(Windows.UI.Xaml.RoutedEvent routedEvent, object source)
        {
          this.RoutedEvent = routedEvent;
          this.Source = source;
        }

        public object Source { get; }
        public Windows.UI.Xaml.RoutedEvent RoutedEvent { get; }
        public UwaFlowVirtualizingStackPanel Panel { get; set; }
        public double ViewportHeight { get; set; }
        public double VerticalOffset { get; set; }

        public string Name { get; set; }
      }

      internal class UwaFlowVirtualizingStackPanel : Windows.UI.Xaml.Controls.VirtualizingStackPanel
      {
        static UwaFlowVirtualizingStackPanel()
        {
          ScrollChangeEvent = Windows.UI.Xaml.EventManager.RegisterRoutedEvent("OffsetChangedEvent", Windows.UI.Xaml.RoutingStrategy.Bubble, typeof(UwaFlowOffsetChangedEventHandler), typeof(UwaFlowVirtualizingStackPanel));
        }

        public static readonly Windows.UI.Xaml.RoutedEvent ScrollChangeEvent;

        public static void AddScrollChangeHandler(Windows.UI.Xaml.DependencyObject o, UwaFlowOffsetChangedEventHandler Event)
        {
          ((Windows.UI.Xaml.UIElement)o).AddHandler(UwaFlowVirtualizingStackPanel.ScrollChangeEvent, Event);
        }
        public static void RemoveScrollChangeHandler(Windows.UI.Xaml.DependencyObject o, UwaFlowOffsetChangedEventHandler Event)
        {
          ((Windows.UI.Xaml.UIElement)o).RemoveHandler(UwaFlowVirtualizingStackPanel.ScrollChangeEvent, Event);
        }

        protected override void OnViewportOffsetChanged(System.Numerics.Vector OldViewportOffset, System.Numerics.Vector NewViewportOffset)
        {
          base.OnViewportOffsetChanged(OldViewportOffset, NewViewportOffset);

          OffsetChangedInvoke("OnViewportOffsetChanged");
        }
        protected override void OnViewportSizeChanged(Windows.Foundation.Size OldViewportSize, Windows.Foundation.Size NewViewportSize)
        {
          base.OnViewportSizeChanged(OldViewportSize, NewViewportSize);

          OffsetChangedInvoke("OnViewportSizeChanged");
        }

        private void OffsetChangedInvoke(string Name)
        {
          this.RaiseEvent(new UwaFlowOffsetChangedEventArgs(UwaFlowVirtualizingStackPanel.ScrollChangeEvent, this) { Name = Name, Panel = this, ViewportHeight = this.ViewportHeight, VerticalOffset = this.VerticalOffset });
        }
      }

      internal class UwaFlowItemContainerControl : Windows.UI.Xaml.Controls.ContentControl
      {
        public UwaFlowItemContainerControl()
        {
        }
      }

      internal class UwaFlowItemsControl : Windows.UI.Xaml.Controls.ItemsControl
      {
        static UwaFlowItemsControl()
        {
          var StyleDictionary = UwaShell.LoadResourceDictionary("InvUwaVirtualisedItemsControl.xaml");

          if (StyleDictionary != null)
          {
            DefaultStyle = (Windows.UI.Xaml.Style)StyleDictionary["BaseStyle"];
            DefaultStyle.Seal();
          }
        }

        private static readonly Windows.UI.Xaml.Style DefaultStyle;

        public UwaFlowItemsControl()
        {
          this.Style = DefaultStyle;
        }

        protected override bool IsItemItsOwnContainerOverride(object Item)
        {
          return Item is UwaFlowItemContainerControl;
        }
        protected override Windows.UI.Xaml.DependencyObject GetContainerForItemOverride()
        {
          return new UwaFlowItemContainerControl();
        }
      }
    }
    */

  public sealed class UwaFlowVirtual : UwaPanel
  {
    internal UwaFlowVirtual()
    {
      this.ListView = new Windows.UI.Xaml.Controls.ListView();
      Border.Child = ListView;

      //var StackPanel = ListView.ItemsPanelRoot as Windows.UI.Xaml.Controls.ItemsStackPanel;


      //Windows.UI.Xaml.Controls.ScrollViewer.SetVerticalScrollBarVisibility(FlowItemsControl, Windows.UI.Xaml.Controls.ScrollBarVisibility.Auto);
      //Windows.UI.Xaml.Controls.ScrollViewer.SetHorizontalScrollBarVisibility(FlowItemsControl, Windows.UI.Xaml.Controls.ScrollBarVisibility.Disabled);
    }

    public Func<int, Windows.UI.Xaml.FrameworkElement> HeaderContentQuery;
    public Func<int, Windows.UI.Xaml.FrameworkElement> FooterContentQuery;
    public Func<int> SectionCountQuery;
    public Func<int, int> ItemCountQuery;
    public Func<int, int, Windows.UI.Xaml.FrameworkElement> ItemContentQuery;

    public void Reload()
    {
    }
    //public void ScrollTo(int Section, int Index)
    //{
    //}

    private readonly Windows.UI.Xaml.Controls.ListView ListView;
    //private System.Collections.ObjectModel.ObservableCollection<Windows.UI.Xaml.FrameworkElement> PlaceholderCollection;
    //private Windows.UI.Xaml.Controls.VirtualizingStackPanel Panel;
    //private int ScrollTargetIndex;
  }

  internal class UwaFlowItemContainerControl : Windows.UI.Xaml.Controls.ContentControl
  {
    public UwaFlowItemContainerControl()
    {
    }
  }

  internal class UwaFlowItemsControl : Windows.UI.Xaml.Controls.ItemsControl
  {
    static UwaFlowItemsControl()
    {
      var StyleDictionary = UwaShell.LoadResourceDictionary("InvUwaVirtualisedItemsControl.xaml");

      if (StyleDictionary != null)
      {
        DefaultStyle = (Windows.UI.Xaml.Style)StyleDictionary["BaseStyle"];
        DefaultStyle.Seal();
      }
    }

    private static readonly Windows.UI.Xaml.Style DefaultStyle;

    public UwaFlowItemsControl()
    {
      this.Style = DefaultStyle; // TODO: breaks the app.
    }

    protected override void OnApplyTemplate()
    {
      base.OnApplyTemplate();
    }

    protected override bool IsItemItsOwnContainerOverride(object Item)
    {
      return Item is UwaFlowItemContainerControl;
    }
    protected override Windows.UI.Xaml.DependencyObject GetContainerForItemOverride()
    {
      return new UwaFlowItemContainerControl();
    }
  }

  public sealed class UwaDock : UwaPanel
  {
    internal UwaDock()
    {
      this.Panel = new UwaDockPanel();
      Border.Child = Panel;
    }

    public bool IsHorizontal
    {
      get { return Panel.IsHorizontal; }
      set { Panel.IsHorizontal = value; }
    }

    public void Compose(IEnumerable<Windows.UI.Xaml.FrameworkElement> UwaHeaders, IEnumerable<Windows.UI.Xaml.FrameworkElement> UwaClients, IEnumerable<Windows.UI.Xaml.FrameworkElement> UwaFooters)
    {
      Panel.Compose(UwaHeaders, UwaClients, UwaFooters);
    }

    private readonly UwaDockPanel Panel;

    private sealed class UwaDockPanel : Windows.UI.Xaml.Controls.Panel
    {
      internal UwaDockPanel()
        : base()
      {
        this.UwaHeaderList = new Inv.DistinctList<Windows.UI.Xaml.FrameworkElement>();
        this.UwaClientList = new Inv.DistinctList<Windows.UI.Xaml.FrameworkElement>();
        this.UwaFooterList = new Inv.DistinctList<Windows.UI.Xaml.FrameworkElement>();
        this.UwaActiveList = new DistinctList<Windows.UI.Xaml.FrameworkElement>();
      }

      public bool IsHorizontal
      {
        get { return IsHorizontalField; }
        set
        {
          if (IsHorizontalField != value)
          {
            this.IsHorizontalField = value;
            InvalidateMeasure();
            InvalidateArrange();
          }
        }
      }

      protected override Windows.Foundation.Size MeasureOverride(Windows.Foundation.Size constraint)
      {
        var Result = new Windows.Foundation.Size();

        var FitSize = constraint;

        foreach (var UwaPanelList in new[] { UwaHeaderList, UwaFooterList })
        {
          foreach (var UwaElement in UwaPanelList)
          {
            if (UwaElement.Visibility != Windows.UI.Xaml.Visibility.Collapsed)
            {
              UwaElement.Measure(FitSize);

              var UwaDesiredSize = UwaElement.DesiredSize;

              if (IsHorizontal)
              {
                if (Result.Height < UwaDesiredSize.Height)
                  Result.Height = UwaDesiredSize.Height;

                Result.Width += UwaDesiredSize.Width;

                if (FitSize.Width > 0)
                  FitSize.Width -= Math.Min(FitSize.Width, UwaDesiredSize.Width); // FitSize can be 5.599999 when DesiredWidth is 5.6 (which causes a measure error on set width).
              }
              else
              {
                if (Result.Width < UwaDesiredSize.Width)
                  Result.Width = UwaDesiredSize.Width;

                Result.Height += UwaDesiredSize.Height;

                if (FitSize.Height > 0)
                  FitSize.Height -= Math.Min(FitSize.Height, UwaDesiredSize.Height);
              }
            }
          }
        }

        var UwaClientArray = UwaClientList.Where(C => C.Visibility != Windows.UI.Xaml.Visibility.Collapsed).ToArray();

        if (UwaClientArray.Length > 0)
        {
          var SharedSize = new Windows.Foundation.Size(IsHorizontal ? FitSize.Width / UwaClientArray.Length : FitSize.Width, IsHorizontal ? FitSize.Height : FitSize.Height / UwaClientArray.Length);

          foreach (var UwaElement in UwaClientArray)
          {
            UwaElement.Measure(SharedSize);

            var UwaDesiredSize = UwaElement.DesiredSize;

            if (IsHorizontal)
            {
              if (Result.Height < UwaDesiredSize.Height)
                Result.Height = UwaDesiredSize.Height;

              Result.Width += UwaDesiredSize.Width;
            }
            else
            {
              if (Result.Width < UwaDesiredSize.Width)
                Result.Width = UwaDesiredSize.Width;

              Result.Height += UwaDesiredSize.Height;
            }
          }
        }

        return Result;
      }
      protected override Windows.Foundation.Size ArrangeOverride(Windows.Foundation.Size arrangeBounds)
      {
        var DockWidth = arrangeBounds.Width;
        var DockHeight = arrangeBounds.Height;

        var UwaClientArray = UwaClientList.Where(C => C.Visibility != Windows.UI.Xaml.Visibility.Collapsed).ToArray();

        if (IsHorizontal)
        {
          var PanelLeft = 0.0;

          var HeaderWidth = 0.0;

          foreach (var UwaElement in UwaHeaderList)
          {
            if (UwaElement.Visibility != Windows.UI.Xaml.Visibility.Collapsed)
            {
              var HeaderSize = UwaElement.DesiredSize;
              UwaElement.Arrange(new Windows.Foundation.Rect(PanelLeft, 0, HeaderSize.Width, DockHeight));
              PanelLeft += HeaderSize.Width;
              HeaderWidth += HeaderSize.Width;
            }
          }

          var FooterTotal = UwaFooterList.Sum(F => F.DesiredSize.Width);
          var ClientRemainder = DockWidth - HeaderWidth - FooterTotal;

          if (UwaClientArray.Length > 0)
          {
            var SharedWidth = ClientRemainder < 0 ? 0 : ClientRemainder / UwaClientArray.Length;

            var LastClient = UwaClientArray.Last();

            foreach (var Client in UwaClientArray)
            {
              var FrameWidth = SharedWidth;
              if (ClientRemainder > 0 && Client == LastClient)
                FrameWidth += ClientRemainder - (SharedWidth * UwaClientArray.Length);

              Client.Arrange(new Windows.Foundation.Rect(PanelLeft, 0, FrameWidth, DockHeight));
              PanelLeft += FrameWidth;
            }
          }

          var PanelRight = (ClientRemainder < 0 ? DockWidth - ClientRemainder : DockWidth);

          foreach (var UwaElement in UwaFooterList)
          {
            if (UwaElement.Visibility != Windows.UI.Xaml.Visibility.Collapsed)
            {
              var FooterWidth = UwaElement.DesiredSize.Width;
              PanelRight -= FooterWidth;
              UwaElement.Arrange(new Windows.Foundation.Rect(PanelRight, 0, FooterWidth, DockHeight));
            }
          }
        }
        else
        {
          var HeaderHeight = 0.0;

          foreach (var UwaElement in UwaHeaderList)
          {
            if (UwaElement.Visibility != Windows.UI.Xaml.Visibility.Collapsed)
            {
              var CurrentHeight = UwaElement.DesiredSize.Height;
              UwaElement.Arrange(new Windows.Foundation.Rect(0, HeaderHeight, DockWidth, CurrentHeight));
              HeaderHeight += CurrentHeight;
            }
          }

          var FooterTotal = UwaFooterList.Sum(F => F.DesiredSize.Height);
          var ClientRemainder = DockHeight - HeaderHeight - FooterTotal;

          if (UwaClientArray.Length > 0)
          {
            var SharedHeight = ClientRemainder < 0 ? 0 : ClientRemainder / UwaClientArray.Length;
            var LastClient = UwaClientArray.Last();

            var ClientTop = HeaderHeight;

            foreach (var UwaElement in UwaClientArray)
            {
              var FrameHeight = SharedHeight;
              if (ClientRemainder > 0 && UwaElement == LastClient)
                FrameHeight += ClientRemainder - (SharedHeight * UwaClientArray.Length);

              UwaElement.Arrange(new Windows.Foundation.Rect(0, ClientTop, DockWidth, FrameHeight));
              ClientTop += FrameHeight;
            }
          }

          var FooterBottom = (ClientRemainder < 0 ? DockHeight - ClientRemainder : DockHeight);

          foreach (var UwaElement in UwaFooterList)
          {
            if (UwaElement.Visibility != Windows.UI.Xaml.Visibility.Collapsed)
            {
              var FooterHeight = UwaElement.DesiredSize.Height;
              FooterBottom -= FooterHeight;
              UwaElement.Arrange(new Windows.Foundation.Rect(0, FooterBottom, DockWidth, FooterHeight));
            }
          }
        }

        return arrangeBounds;
      }

      public void Compose(IEnumerable<Windows.UI.Xaml.FrameworkElement> UwaHeaders, IEnumerable<Windows.UI.Xaml.FrameworkElement> UwaClients, IEnumerable<Windows.UI.Xaml.FrameworkElement> UwaFooters)
      {
        UwaHeaderList.Clear();
        UwaHeaderList.AddRange(UwaHeaders);

        UwaClientList.Clear();
        UwaClientList.AddRange(UwaClients);

        UwaFooterList.Clear();
        UwaFooterList.AddRange(UwaFooters.Reverse()); // will be arranged in reverse order.

        var UwaPreviousList = UwaActiveList;
        this.UwaActiveList = new DistinctList<Windows.UI.Xaml.FrameworkElement>(UwaHeaderList.Count + UwaClientList.Count + UwaFooterList.Count);
        UwaActiveList.AddRange(UwaHeaderList);
        UwaActiveList.AddRange(UwaClientList);
        UwaActiveList.AddRange(UwaFooterList);

        foreach (var Container in UwaActiveList.Except(UwaPreviousList))
          this.SafeAddChild(Container);

        foreach (var Container in UwaPreviousList.Except(UwaActiveList))
          Children.Remove(Container);
      }

      private bool IsHorizontalField;
      private readonly Inv.DistinctList<Windows.UI.Xaml.FrameworkElement> UwaHeaderList;
      private readonly Inv.DistinctList<Windows.UI.Xaml.FrameworkElement> UwaClientList;
      private readonly Inv.DistinctList<Windows.UI.Xaml.FrameworkElement> UwaFooterList;
      private Inv.DistinctList<Windows.UI.Xaml.FrameworkElement> UwaActiveList;
    }
  }

  public sealed class UwaFrame : UwaPanel
  {
    internal UwaFrame()
    {
      this.Grid = new Windows.UI.Xaml.Controls.Grid();
      Border.Child = Grid;

      Grid.Loaded += (Sender, Event) => Clipping();
      Grid.SizeChanged += (Sender, Event) => Clipping();
    }

    public Windows.UI.Xaml.Controls.Grid Grid { get; private set; }

    internal bool IsTransitioning { get; set; }

    private void Clipping()
    {
      Grid.Clip = new Windows.UI.Xaml.Media.RectangleGeometry { Rect = new Windows.Foundation.Rect(0, 0, Grid.ActualWidth, Grid.ActualHeight) };
    }
  }

  public sealed class UwaNative : UwaPanel
  {
    internal UwaNative()
    {
    }
  }

  public sealed class UwaEdit : UwaPanel, UwaOverrideFocusContract
  {
    internal UwaEdit(bool SearchControl, bool PasswordMask)
    {
      if (SearchControl)
      {
        // TODO: do we want to use AutoSuggestBox instead (font styling looks annoying though).

        var LayoutDock = new UwaDock();
        Border.Child = LayoutDock;
        LayoutDock.IsHorizontal = true;

        var ClearButton = new UwaButton();
        ClearButton.HorizontalContentAlignment = Windows.UI.Xaml.HorizontalAlignment.Center;
        ClearButton.VerticalContentAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
        ClearButton.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
        ClearButton.Click += (Sender, Event) =>
        {
          SearchBox.Text = ""; // fires change event.
          SearchBox.Focus(Windows.UI.Xaml.FocusState.Programmatic);
        };

        var ClearGeometry = new Windows.UI.Xaml.Media.PathGeometry();
        var ForwardFigure = new Windows.UI.Xaml.Media.PathFigure();
        ForwardFigure.StartPoint = new Windows.Foundation.Point(0, 0);
        ForwardFigure.Segments.Add(new Windows.UI.Xaml.Media.LineSegment() { Point = new Windows.Foundation.Point(1, 1) });
        ClearGeometry.Figures.Add(ForwardFigure);
        var BackwardFigure = new Windows.UI.Xaml.Media.PathFigure();
        BackwardFigure.StartPoint = new Windows.Foundation.Point(0, 1);
        BackwardFigure.Segments.Add(new Windows.UI.Xaml.Media.LineSegment() { Point = new Windows.Foundation.Point(1, 0) });
        ClearGeometry.Figures.Add(BackwardFigure);

        var ClearPath = new Windows.UI.Xaml.Shapes.Path();
        ClearPath.Data = ClearGeometry;
        ClearPath.Margin = new Windows.UI.Xaml.Thickness(4);
        ClearPath.Stretch = Windows.UI.Xaml.Media.Stretch.UniformToFill;
        ClearPath.StrokeThickness = 2;

        ClearButton.Border.Child = ClearPath;

        this.SearchBox = new Windows.UI.Xaml.Controls.TextBox()
        {
          Background = null,
          BorderBrush = null,
          BorderThickness = new Windows.UI.Xaml.Thickness(0),
          TextWrapping = Windows.UI.Xaml.TextWrapping.Wrap,
          Style = UwaStyles.TextBoxStyle,
          FontSize = 12
        };
        SearchBox.SizeChanged += (Sender, Event) =>
        {
          ClearButton.Width = SearchBox.ActualHeight;
          ClearButton.Height = ClearButton.Width;
        };
        SearchBox.TextChanged += (Sender, Event) =>
        {
          ClearButton.Visibility = string.IsNullOrEmpty(SearchBox.Text) ? Windows.UI.Xaml.Visibility.Collapsed : Windows.UI.Xaml.Visibility.Visible;

          if (ClearButton.Visibility == Windows.UI.Xaml.Visibility.Visible)
          {
            ClearPath.Stroke = SearchBox.Foreground ?? new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Black);

            // NOTE: these need to be set to something for consistent behaviour (null brushes cause hit-test issues).
            ClearButton.NormalBackgroundBrush = SearchBox.Background ?? new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Transparent);
            ClearButton.HoverBackgroundBrush = SearchBox.Background ?? new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Transparent);
            ClearButton.PressedBackgroundBrush = SearchBox.Background ?? new Windows.UI.Xaml.Media.SolidColorBrush(Windows.UI.Colors.Transparent);
          }
        };

        LayoutDock.Compose(new Windows.UI.Xaml.FrameworkElement[0], this.SearchBox.SingleToDistinctList(), ClearButton.SingleToDistinctList());
      }
      else if (PasswordMask)
      {
        this.PasswordBox = new Windows.UI.Xaml.Controls.PasswordBox()
        {
          Background = null,
          BorderBrush = null,
          BorderThickness = new Windows.UI.Xaml.Thickness(0),
          PasswordRevealMode = Windows.UI.Xaml.Controls.PasswordRevealMode.Peek,
          Style = UwaStyles.PasswordBoxStyle,
          FontSize = 12
        };
        Border.Child = PasswordBox;
      }
      else
      {
        this.TextBox = new Windows.UI.Xaml.Controls.TextBox()
        {
          AcceptsReturn = false,
          Background = null,
          BorderBrush = null,
          TextWrapping = Windows.UI.Xaml.TextWrapping.Wrap,
          BorderThickness = new Windows.UI.Xaml.Thickness(0),
          Style = UwaStyles.TextBoxStyle,
          FontSize = 12
        };
        Border.Child = TextBox;

        // TODO: IsSpellCheckEnabled
      }
    }

    public Windows.UI.Xaml.Controls.TextBox TextBox { get; private set; }
    public Windows.UI.Xaml.Controls.PasswordBox PasswordBox { get; private set; }
    public Windows.UI.Xaml.Controls.TextBox SearchBox { get; private set; }

    void UwaOverrideFocusContract.OverrideFocus()
    {
      if (TextBox != null)
        TextBox.Focus(Windows.UI.Xaml.FocusState.Programmatic);
      else if (PasswordBox != null)
        PasswordBox.Focus(Windows.UI.Xaml.FocusState.Programmatic);
      else
        SearchBox.Focus(Windows.UI.Xaml.FocusState.Programmatic);
    }
  }

  public sealed class UwaGraphic : UwaPanel
  {
    internal UwaGraphic()
    {
      this.Image = new Windows.UI.Xaml.Controls.Image()
      {
        Stretch = Windows.UI.Xaml.Media.Stretch.Uniform
      };
      Image.CacheMode = new Windows.UI.Xaml.Media.BitmapCache(); // this causes the better scaling algorithm to be used for some reason.

      Border.Child = Image;
    }

    public Windows.UI.Xaml.Controls.Image Image { get; private set; }
  }

  public sealed class UwaBlock : UwaPanel
  {
    internal UwaBlock()
    {
      this.TextBlock = new Windows.UI.Xaml.Controls.TextBlock()
      {
        VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center,
        TextTrimming = Windows.UI.Xaml.TextTrimming.CharacterEllipsis,
        //Padding = new Windows.UI.Xaml.Thickness(0),
        FontSize = 12 // otherwise the default is 15pt!
      };
      Border.Child = TextBlock;
    }

    public Windows.UI.Xaml.Controls.TextBlock TextBlock { get; private set; }
  }

  public sealed class UwaLabel : UwaPanel
  {
    internal UwaLabel()
    {
      this.TextBlock = new Windows.UI.Xaml.Controls.TextBlock()
      {
        VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center,
        TextTrimming = Windows.UI.Xaml.TextTrimming.CharacterEllipsis,
        //Padding = new Windows.UI.Xaml.Thickness(0),
        FontSize = 12 // otherwise the default is 15pt!
      };
      Border.Child = TextBlock;
    }

    public Windows.UI.Xaml.Controls.TextBlock TextBlock { get; private set; }
  }

  public sealed class UwaMemo : UwaPanel, UwaOverrideFocusContract
  {
    internal UwaMemo()
    {
      SetPlainText(null);
    }

    public string Text
    {
      get { return TextField; }
    }
    public bool IsReadOnly
    {
      get { return IsReadOnlyField; }
      set
      {
        if (IsReadOnly != value)
        {
          this.IsReadOnlyField = value;

          if (RichEditBox != null)
            RichEditBox.IsReadOnly = value;
          else if (PlainTextBox != null)
            PlainTextBox.IsReadOnly = value;
        }
      }
    }
    public event Action TextChangedEvent;
    public event Action GotFocusEvent;
    public event Action LostFocusEvent;

    public void SetRichText(string Text, Action<Windows.UI.Xaml.Controls.RichEditBox> SetAction)
    {
      //var IsFirst = this.TextField == null && Text != null;

      this.TextField = Text;

      if (RichEditBox == null)
      {
        this.RichEditBox = new Windows.UI.Xaml.Controls.RichEditBox()
        {
          AcceptsReturn = true,
          //AcceptsTabs = true, // Not available in WinRT?
          TextWrapping = Windows.UI.Xaml.TextWrapping.Wrap,
          Background = null,
          BorderBrush = null,
          Margin = new Windows.UI.Xaml.Thickness(0),
          Padding = new Windows.UI.Xaml.Thickness(0),
          BorderThickness = new Windows.UI.Xaml.Thickness(0),
          Style = UwaStyles.RichMemoStyle,
          FontSize = 12,
          IsColorFontEnabled = true, // colourful emotions.
          IsReadOnly = IsReadOnlyField
        };
        Border.Child = RichEditBox;
        RichEditBox.GotFocus += (Sender, Event) =>
        {
          if (GotFocusEvent != null)
            GotFocusEvent();
        };
        RichEditBox.LostFocus += (Sender, Event) =>
        {
          if (LostFocusEvent != null)
            LostFocusEvent();
        };
        RichEditBox.TextChanged += (Sender, Event) =>
        {
          RichEditBox.Document.GetText(Windows.UI.Text.TextGetOptions.None, out this.TextField);

          if (TextField.Length > 0 && TextField[TextField.Length - 1] == '\r' && !TextField.Contains('\n'))
            this.TextField = TextField.RemoveLast(1).Replace("\r", Environment.NewLine);

          if (!TextField.Contains(Environment.NewLine))
            RichEditBox.Margin = new Windows.UI.Xaml.Thickness(0, 0, 0, -16); // single-line or empty causes a trailing 16px footer.
          else
            RichEditBox.Margin = new Windows.UI.Xaml.Thickness(0, 0, 0, 0);

          if (TextChangedEvent != null)
            TextChangedEvent();
        };
      }

      if (PlainTextBox != null)
      {
        RichEditBox.FontFamily = PlainTextBox.FontFamily;

        var DefaultFormat = RichEditBox.Document.GetDefaultCharacterFormat();

        var PlainBrush = (PlainTextBox.Foreground as Windows.UI.Xaml.Media.SolidColorBrush);
        DefaultFormat.ForegroundColor = PlainBrush != null ? PlainBrush.Color : Windows.UI.Colors.Black;
        RichEditBox.Document.SetDefaultCharacterFormat(DefaultFormat);

        RichEditBox.FontWeight = PlainTextBox.FontWeight;
        RichEditBox.FontSize = PlainTextBox.FontSize;

        this.PlainTextBox = null;
      }

      RichEditBox.Document.BatchDisplayUpdates();
      try
      {
        // NOTE: you will get an 'access denied' exception if you try to set the text when readonly.
        RichEditBox.IsReadOnly = false;
        RichEditBox.Document.SetText(Windows.UI.Text.TextSetOptions.None, Text);
        SetAction(RichEditBox);
        RichEditBox.IsReadOnly = IsReadOnlyField;
      }
      finally
      {
        RichEditBox.Document.ApplyDisplayUpdates();
      }
    }
    public void SetPlainText(string Text)
    {
      this.TextField = Text;

      if (PlainTextBox == null)
      {
        this.PlainTextBox = new Windows.UI.Xaml.Controls.TextBox()
        {
          AcceptsReturn = true,
          //AcceptsTabs = true, // Not available in WinRT?
          TextWrapping = Windows.UI.Xaml.TextWrapping.Wrap,
          Background = null,
          BorderBrush = null,
          Margin = new Windows.UI.Xaml.Thickness(0),
          Padding = new Windows.UI.Xaml.Thickness(0),
          BorderThickness = new Windows.UI.Xaml.Thickness(0),
          Style = UwaStyles.PlainMemoStyle,
          FontSize = 12,
          IsReadOnly = IsReadOnlyField
        };
        Border.Child = PlainTextBox;
        PlainTextBox.GotFocus += (Sender, Event) =>
        {
          if (GotFocusEvent != null)
            GotFocusEvent();
        };
        PlainTextBox.LostFocus += (Sender, Event) =>
        {
          if (LostFocusEvent != null)
            LostFocusEvent();
        };
        PlainTextBox.TextChanged += (Sender, Event) =>
        {
          this.TextField = PlainTextBox.Text;

          if (TextChangedEvent != null)
            TextChangedEvent();
        };
      }

      if (RichEditBox != null)
      {
        PlainTextBox.FontFamily = RichEditBox.FontFamily;
        PlainTextBox.Foreground = new Windows.UI.Xaml.Media.SolidColorBrush(RichEditBox.Document.GetDefaultCharacterFormat().ForegroundColor);
        PlainTextBox.FontWeight = RichEditBox.FontWeight;
        PlainTextBox.FontSize = RichEditBox.FontSize;

        this.RichEditBox = null;
      }

      PlainTextBox.Text = Text ?? "";
    }
    public Windows.UI.Xaml.Controls.Control GetControl()
    {
      return (Windows.UI.Xaml.Controls.Control)RichEditBox ?? (Windows.UI.Xaml.Controls.Control)PlainTextBox;
    }
    public Windows.UI.Xaml.Controls.RichEditBox GetRichEditBox()
    {
      return RichEditBox;
    }
    public Windows.UI.Xaml.Controls.TextBox GetPlainTextBox()
    {
      return PlainTextBox;
    }

    void UwaOverrideFocusContract.OverrideFocus()
    {
      PlainTextBox.Focus(Windows.UI.Xaml.FocusState.Programmatic);
    }

    private string TextField;
    private bool IsReadOnlyField;
    private Windows.UI.Xaml.Controls.RichEditBox RichEditBox;
    private Windows.UI.Xaml.Controls.TextBox PlainTextBox;
  }

  public sealed class UwaOverlay : UwaPanel
  {
    internal UwaOverlay()
    {
      this.Grid = new Windows.UI.Xaml.Controls.Grid();
      Border.Child = Grid;
    }

    public Windows.UI.Xaml.Controls.Grid Grid { get; private set; }

    public void Compose(IEnumerable<Windows.UI.Xaml.FrameworkElement> UwaElements)
    {
      Grid.Children.Clear();
      foreach (var UwaElement in UwaElements)
        Grid.SafeAddChild(UwaElement);
      /*
      var ChildIndex = 0;

      foreach (var UwaElement in UwaElements)
      {
        while (ChildIndex < Grid.Children.Count && UwaElement != Grid.Children[ChildIndex])
          Grid.Children.RemoveAt(ChildIndex);

        if (ChildIndex >= Grid.Children.Count)
          Grid.SafeAddChild(UwaElement);

        ChildIndex++;
      }*/
    }

    internal void AddChild(Windows.UI.Xaml.FrameworkElement UwaPanel)
    {
      Grid.SafeAddChild(UwaPanel);
    }
  }

  public sealed class UwaCanvas : UwaPanel, Inv.DrawContract
  {
    internal UwaCanvas(UwaEngine UwaEngine)
    {
      this.UwaEngine = UwaEngine;
      this.Canvas = new Microsoft.Graphics.Canvas.UI.Xaml.CanvasControl();
      Border.Child = Canvas;
      Canvas.CreateResources += (Sender, Event) =>
      {
        if (ResetEvent != null)
          ResetEvent();
      };
      Canvas.Draw += (Sender, Event) =>
      {
        this.UwaSession = Event.DrawingSession;
        DrawAction();
      };

      this.UwaStrokeStyle = new Microsoft.Graphics.Canvas.Geometry.CanvasStrokeStyle()
      {
        EndCap = Microsoft.Graphics.Canvas.Geometry.CanvasCapStyle.Round
      };
    }

    public event Action ResetEvent;
    public Action DrawAction;

    public void Invalidate()
    {
      // NOTE: this seems to fire only once per two frames!
      //Canvas.Invalidate();

      // NOTE: the following seems to make it run every frame.
      this.Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.High, Canvas.Invalidate).AsTask().Forget();
    }

    void DrawContract.DrawText(string TextFragment, string TextFontName, int TextFontSize, FontWeight TextFontWeight, Colour TextFontColour, Point TextPoint, HorizontalPosition TextHorizontal, VerticalPosition TextVertical)
    {
      var TextFormat = new Microsoft.Graphics.Canvas.Text.CanvasTextFormat()
      {
        FontSize = TextFontSize,
        WordWrapping = Microsoft.Graphics.Canvas.Text.CanvasWordWrapping.NoWrap,
        FontWeight = UwaEngine.TranslateFontWeight(TextFontWeight)
      };

      var TextLayout = new Microsoft.Graphics.Canvas.Text.CanvasTextLayout(UwaSession, TextFragment, TextFormat, 0.0f, 0.0f);

      var TextX = TextPoint.X - (int)TextLayout.DrawBounds.X;
      var TextY = TextPoint.Y - (int)TextLayout.DrawBounds.Y;

      if (TextHorizontal != HorizontalPosition.Left)
      {
        var TextWidth = (int)TextLayout.DrawBounds.Width;

        if (TextHorizontal == HorizontalPosition.Right)
          TextX -= TextWidth;
        else if (TextHorizontal == HorizontalPosition.Center)
          TextX -= TextWidth / 2;
      }

      if (TextVertical != VerticalPosition.Top)
      {
        var TextHeight = (int)TextLayout.DrawBounds.Height;

        if (TextVertical == VerticalPosition.Bottom)
          TextY -= TextHeight;
        else if (TextVertical == VerticalPosition.Center)
          TextY -= TextHeight / 2;
      }

      UwaSession.DrawTextLayout(TextLayout, TextX, TextY, UwaEngine.TranslateMediaColour(TextFontColour));
    }
    void DrawContract.DrawLine(Inv.Colour LineStrokeColour, int LineStrokeThickness, Inv.Point LineSourcePoint, Inv.Point LineTargetPoint, params Inv.Point[] LineExtraPointArray)
    {
      if (LineStrokeThickness <= 0 || LineStrokeColour == Inv.Colour.Transparent)
        return;

      var LineStrokeBrush = UwaEngine.TranslateCanvasBrush(UwaSession, LineStrokeColour);

      UwaSession.DrawLine(LineSourcePoint.X, LineSourcePoint.Y, LineTargetPoint.X, LineTargetPoint.Y, LineStrokeBrush, LineStrokeThickness, UwaStrokeStyle);

      var CurrentPoint = LineTargetPoint;
      for (var Index = 0; Index < LineExtraPointArray.Length; Index++)
      {
        var TargetPoint = LineExtraPointArray[Index];
        UwaSession.DrawLine(CurrentPoint.X, CurrentPoint.Y, TargetPoint.X, TargetPoint.Y, LineStrokeBrush, LineStrokeThickness, UwaStrokeStyle);

        CurrentPoint = TargetPoint;
      }
    }
    void DrawContract.DrawRectangle(Colour RectangleFillColour, Colour RectangleStrokeColour, int RectangleStrokeThickness, Rect RectangleRect)
    {
      var RectangleX = RectangleRect.Left;
      var RectangleY = RectangleRect.Top;
      var RectangleWidth = RectangleRect.Width;
      var RectangleHeight = RectangleRect.Height;

      if (RectangleFillColour != null)
        UwaSession.FillRectangle(RectangleX, RectangleY, RectangleWidth, RectangleHeight, UwaEngine.TranslateCanvasBrush(UwaSession, RectangleFillColour));

      if (RectangleStrokeColour != null && RectangleStrokeThickness > 0)
      {
        RectangleX += RectangleStrokeThickness / 2;
        RectangleY += RectangleStrokeThickness / 2;

        if (RectangleWidth >= RectangleStrokeThickness)
          RectangleWidth -= RectangleStrokeThickness;

        if (RectangleHeight >= RectangleStrokeThickness)
          RectangleHeight -= RectangleStrokeThickness;

        UwaSession.DrawRectangle(RectangleX, RectangleY, RectangleWidth, RectangleHeight, UwaEngine.TranslateCanvasBrush(UwaSession, RectangleStrokeColour), RectangleStrokeThickness);
      }
    }
    void DrawContract.DrawEllipse(Colour EllipseFillColour, Colour EllipseStrokeColour, int EllipseStrokeThickness, Point EllipseCenter, Point EllipseRadius)
    {
      var EllipseX = EllipseCenter.X;
      var EllipseY = EllipseCenter.Y;
      var EllipseRadiusX = EllipseRadius.X;
      var EllipseRadiusY = EllipseRadius.Y;

      if (EllipseFillColour != null)
        UwaSession.FillEllipse(EllipseX, EllipseY, EllipseRadiusX, EllipseRadiusY, UwaEngine.TranslateCanvasBrush(UwaSession, EllipseFillColour));

      if (EllipseStrokeColour != null && EllipseStrokeThickness > 0)
        UwaSession.DrawEllipse(EllipseX, EllipseY, EllipseRadiusX, EllipseRadiusY, UwaEngine.TranslateCanvasBrush(UwaSession, EllipseStrokeColour), EllipseStrokeThickness);
    }
    void DrawContract.DrawArc(Inv.Colour ArcFillColour, Inv.Colour ArcStrokeColour, int ArcStrokeThickness, Inv.Point ArcCenter, Inv.Point ArcRadius, float StartAngle, float SweepAngle)
    {
      var StartX = ArcRadius.X * (float)Math.Cos(StartAngle * Math.PI / 180.0);
      var StartY = ArcRadius.Y * (float)Math.Sin(StartAngle * Math.PI / 180.0);

      var EndX = ArcRadius.X * (float)Math.Cos(SweepAngle * Math.PI / 180.0);
      var EndY = ArcRadius.Y * (float)Math.Sin(SweepAngle * Math.PI / 180.0);

      var WpfBuilder = new Microsoft.Graphics.Canvas.Geometry.CanvasPathBuilder(Canvas);

      WpfBuilder.BeginFigure(new System.Numerics.Vector2() { X = ArcCenter.X + StartX, Y = ArcCenter.Y - StartY });
      WpfBuilder.AddArc(
        endPoint: new System.Numerics.Vector2() { X = ArcCenter.X + EndX, Y = ArcCenter.Y - EndY }, 
        radiusX: ArcRadius.X, 
        radiusY: ArcRadius.Y,
        rotationAngle: 0.0F, 
        sweepDirection: Microsoft.Graphics.Canvas.Geometry.CanvasSweepDirection.CounterClockwise, 
        arcSize: (SweepAngle - StartAngle) > 180 ? Microsoft.Graphics.Canvas.Geometry.CanvasArcSize.Large : Microsoft.Graphics.Canvas.Geometry.CanvasArcSize.Small
      );
      WpfBuilder.AddLine(ArcCenter.X, ArcCenter.Y);

      WpfBuilder.EndFigure(Microsoft.Graphics.Canvas.Geometry.CanvasFigureLoop.Closed);

      var WpfGeometry = Microsoft.Graphics.Canvas.Geometry.CanvasGeometry.CreatePath(WpfBuilder);

      if (ArcFillColour != null)
        UwaSession.FillGeometry(WpfGeometry, UwaEngine.TranslateCanvasBrush(UwaSession, ArcFillColour));

      if (ArcStrokeColour != null && ArcStrokeThickness > 0)
        UwaSession.DrawGeometry(WpfGeometry, UwaEngine.TranslateCanvasBrush(UwaSession, ArcStrokeColour), ArcStrokeThickness);
    }
    void DrawContract.DrawImage(Image ImageSource, Rect ImageRect, float ImageOpacity, Colour ImageTint, Mirror? ImageMirror, float ImageRotation)
    {
      var ImageBitmap = UwaEngine.TranslateCanvasBitmap(UwaSession, ImageSource);

      ImageSourceRect.Width = ImageBitmap.Size.Width;
      ImageSourceRect.Height = ImageBitmap.Size.Height;

      ImageDestinationRect.X = ImageRect.Left;
      ImageDestinationRect.Y = ImageRect.Top;
      ImageDestinationRect.Width = ImageRect.Width;
      ImageDestinationRect.Height = ImageRect.Height;

      var CanvasImage = (Microsoft.Graphics.Canvas.ICanvasImage)ImageBitmap;

      using (var TintTarget = ImageTint != null ? new Microsoft.Graphics.Canvas.CanvasRenderTarget(UwaSession, (int)ImageSourceRect.Width, (int)ImageSourceRect.Height) : null)
      {
        if (TintTarget != null)
        {
          using (var TintSession = TintTarget.CreateDrawingSession())
          {
            TintSession.Clear(Windows.UI.Colors.Transparent);
            TintSession.DrawImage(ImageBitmap, 0, 0, ImageSourceRect, 1.0F, Microsoft.Graphics.Canvas.CanvasImageInterpolation.HighQualityCubic);
            TintSession.FillRectangle(ImageSourceRect, new Microsoft.Graphics.Canvas.Brushes.CanvasSolidColorBrush(TintSession, UwaEngine.TranslateMediaColour(ImageTint)), new Microsoft.Graphics.Canvas.Brushes.CanvasImageBrush(TintSession, ImageBitmap));
          }

          CanvasImage = TintTarget;
        }

        if (ImageMirror != null || ImageRotation != 0.0F)
        {
          var CenterVector = new System.Numerics.Vector2((float)ImageSourceRect.Width / 2.0F, (float)ImageSourceRect.Height / 2.0F);

          var MirrorMatrix = ImageMirror != null ? (System.Numerics.Matrix3x2.CreateScale(ImageMirror.Value == Mirror.Horizontal ? -1 : +1, ImageMirror.Value == Mirror.Vertical ? -1 : +1, CenterVector)) : System.Numerics.Matrix3x2.Identity;
          var RotationMatrix = ImageRotation != 1.0F ? (System.Numerics.Matrix3x2.CreateRotation((float)(Math.PI * ImageRotation / 180.0), CenterVector)) : System.Numerics.Matrix3x2.Identity;

          using (var TransformEffect = new Microsoft.Graphics.Canvas.Effects.Transform2DEffect() { Source = CanvasImage, TransformMatrix = MirrorMatrix * RotationMatrix })
            UwaSession.DrawImage(TransformEffect, ImageDestinationRect, ImageSourceRect, ImageOpacity, Microsoft.Graphics.Canvas.CanvasImageInterpolation.HighQualityCubic);
        }
        else
        {
          UwaSession.DrawImage(CanvasImage, ImageDestinationRect, ImageSourceRect, ImageOpacity, Microsoft.Graphics.Canvas.CanvasImageInterpolation.HighQualityCubic);
        }
      }
    }
    void DrawContract.DrawPolygon(Colour FillColour, Colour StrokeColour, int StrokeThickness, LineJoin LineJoin, Point StartPoint, params Point[] PointArray)
    {
      throw new NotImplementedException();
    }

    private readonly UwaEngine UwaEngine;
    private readonly Microsoft.Graphics.Canvas.UI.Xaml.CanvasControl Canvas;
    private readonly Microsoft.Graphics.Canvas.Geometry.CanvasStrokeStyle UwaStrokeStyle;
    private Microsoft.Graphics.Canvas.CanvasDrawingSession UwaSession;
    private Windows.Foundation.Rect ImageSourceRect = new Windows.Foundation.Rect();
    private Windows.Foundation.Rect ImageDestinationRect = new Windows.Foundation.Rect();
  }

  public sealed class UwaScroll : UwaPanel
  {
    internal UwaScroll()
    {
      this.ScrollViewer = new Windows.UI.Xaml.Controls.ScrollViewer();
      Border.Child = ScrollViewer;
    }

    public void SetOrientation(bool IsHorizontal)
    {
      if (IsHorizontal != IsHorizontalField)
      {
        this.IsHorizontalField = IsHorizontal;

        if (IsHorizontal)
        {
          ScrollViewer.VerticalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Disabled;
          ScrollViewer.HorizontalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Auto;
          ScrollViewer.HorizontalScrollMode = Windows.UI.Xaml.Controls.ScrollMode.Auto;
          ScrollViewer.VerticalScrollMode = Windows.UI.Xaml.Controls.ScrollMode.Disabled;
        }
        else
        {
          ScrollViewer.VerticalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Auto;
          ScrollViewer.HorizontalScrollBarVisibility = Windows.UI.Xaml.Controls.ScrollBarVisibility.Disabled;
          ScrollViewer.HorizontalScrollMode = Windows.UI.Xaml.Controls.ScrollMode.Disabled;
          ScrollViewer.VerticalScrollMode = Windows.UI.Xaml.Controls.ScrollMode.Auto;
        }
      }
    }

    public Windows.UI.Xaml.Controls.ScrollViewer ScrollViewer { get; private set; }

    private bool? IsHorizontalField; // nullable is important so the first time in will set the correct modes and visibility.
  }

  public sealed class UwaStack : UwaPanel
  {
    internal UwaStack()
    {
      this.StackPanel = new Windows.UI.Xaml.Controls.StackPanel();
      Border.Child = StackPanel;
    }

    public Windows.UI.Xaml.Controls.StackPanel StackPanel { get; private set; }
    public Windows.UI.Xaml.Controls.Orientation Orientation
    {
      get { return StackPanel.Orientation; }
      set
      {
        if (StackPanel.Orientation != value)
          StackPanel.Orientation = value;
      }
    }

    public void Compose(IEnumerable<Windows.UI.Xaml.FrameworkElement> UwaElements)
    {
      StackPanel.Children.Clear();
      foreach (var UwaElement in UwaElements)
        StackPanel.SafeAddChild(UwaElement);

      /*
      var ChildIndex = 0;

      foreach (var UwaElement in UwaElements)
      {
        while (ChildIndex < StackPanel.Children.Count && !object.ReferenceEquals(UwaElement, StackPanel.Children[ChildIndex]))
          StackPanel.Children.RemoveAt(ChildIndex);

        if (ChildIndex >= StackPanel.Children.Count)
          StackPanel.SafeAddChild(UwaElement);

        ChildIndex++;
      }*/
    }
  }

  public sealed class UwaSwitch : UwaPanel
  {
    internal UwaSwitch()
    {
      this.Inner = new Windows.UI.Xaml.Controls.ToggleSwitch();
      Inner.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Right;
      Inner.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Center;
      Inner.OffContent = null;
      Inner.OnContent = null;
      Inner.MinWidth = 0;

      Border.Child = Inner;
    }

    public bool IsOn
    {
      get { return Inner.IsOn; }
      set { Inner.IsOn = value; }
    }
    public event Windows.UI.Xaml.RoutedEventHandler ChangeEvent
    {
      add
      {
        Inner.Toggled += value;
      }
      remove
      {
        Inner.Toggled -= value;
      }
    }

    private readonly Windows.UI.Xaml.Controls.ToggleSwitch Inner;
  }

  public sealed class UwaWrap : UwaPanel
  {
    internal UwaWrap()
    {
      this.WrapPanel = new WinRTXamlToolkit.Controls.WrapPanel();
      Border.Child = WrapPanel;
    }

    public WinRTXamlToolkit.Controls.WrapPanel WrapPanel { get; private set; }
    public Windows.UI.Xaml.Controls.Orientation Orientation
    {
      get { return WrapPanel.Orientation; }
      set
      {
        if (WrapPanel.Orientation != value)
          WrapPanel.Orientation = value;
      }
    }

    public void Compose(IEnumerable<Windows.UI.Xaml.FrameworkElement> UwaElements)
    {
      WrapPanel.Children.Clear();

      foreach (var UwaElement in UwaElements)
        WrapPanel.SafeAddChild(UwaElement);
    }
  }

  public sealed class UwaSurface : UwaPanel
  {
    internal UwaSurface()
    {
      this.Loaded += (Sender, Event) =>
      {
        this.IsLoaded = true;
      };
      this.Unloaded += (Sender, Event) =>
      {
        this.IsLoaded = false;
      };
    }

    public new bool IsLoaded { get; private set; }

    public void SetContent(Windows.UI.Xaml.FrameworkElement UwaContent)
    {
      Border.SafeSetChild(UwaContent);
    }
  }

  public sealed class UwaTable : UwaPanel
  {
    internal UwaTable()
    {
      this.Grid = new Windows.UI.Xaml.Controls.Grid();
      Border.Child = Grid;
    }

    public Windows.UI.Xaml.Controls.Grid Grid { get; private set; }

    public void AddChild(Windows.UI.Xaml.FrameworkElement UwaCell)
    {
      Grid.SafeAddChild(UwaCell);
    }
  }

  internal static class UwaFoundation
  {
    public static void SafeSetContent(this Windows.UI.Xaml.Controls.ContentControl Parent, Windows.UI.Xaml.FrameworkElement Content)
    {
      if (Content == null || SafeRemoveChild(Parent, Content))
        Parent.Content = Content;
    }
    public static void SafeSetChild(this Windows.UI.Xaml.Controls.Border Parent, Windows.UI.Xaml.FrameworkElement Child)
    {
      if (Child == null || SafeRemoveChild(Parent, Child))
        Parent.Child = Child;
    }
    public static void SafeAddChild(this Windows.UI.Xaml.Controls.Panel Parent, Windows.UI.Xaml.FrameworkElement Child)
    {
      if (Child != null && SafeRemoveChild(Parent, Child))
      {
        try
        {
          Parent.Children.Add(Child);
        }
        catch (Exception Exception)
        {
          throw new UwaAddChildException($"{Parent.GetType().FullName}.Add({Child.GetType().FullName}): {Exception.Message}", Exception);
        }
      }
    }

    private static bool SafeRemoveChild(Windows.UI.Xaml.FrameworkElement Parent, Windows.UI.Xaml.FrameworkElement Child)
    {
      // NOTE: returns false if the child is already directly connected to the parent.

      var Owner = Windows.UI.Xaml.Media.VisualTreeHelper.GetParent(Child);

      if (Owner == null)
      {
        return true;
      }
      else if (Owner != Parent)
      {
        var OwnerAsPanel = Owner as Windows.UI.Xaml.Controls.Panel;
        if (OwnerAsPanel != null)
        {
          OwnerAsPanel.Children.Remove(Child);
        }
        else
        {
          var OwnerAsContentControl = Owner as Windows.UI.Xaml.Controls.ContentControl;
          if (OwnerAsContentControl != null)
          {
            OwnerAsContentControl.Content = null;
          }
          else
          {
            var OwnerAsBorder = Owner as Windows.UI.Xaml.Controls.Border;
            if (OwnerAsBorder != null)
            {
              OwnerAsBorder.Child = null;
            }
            else
            {
              Debug.Assert(false, "Unhandled owner.");
            }
          }
        }

        return true;
      }

      return false;
    }
  }

  internal sealed class UwaAddChildException : Exception
  {
    public UwaAddChildException(string Message, Exception Inner)
      : base(Message, Inner)
    {
    }
  }
}
