﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Reflection;
using Inv.Support;

namespace Inv
{
  public static class LinuxShell
  {
    static LinuxShell()
    {
      DefaultWindowWidth = 800;
      DefaultWindowHeight = 600;

      MainFolderPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
    }

    public static int DefaultWindowWidth { get; set; }
    public static int DefaultWindowHeight { get; set; }
    public static string MainFolderPath { get; set; }

    public static void Run(Action<Inv.Application> InvAction)
    {
      var Application = new Inv.Application();

      var LinuxEngine = new LinuxEngine(Application);

      InvAction?.Invoke(Application);

      LinuxEngine.Run();
    }
  }

  internal sealed class LinuxEngine
  {
    public LinuxEngine(Inv.Application Application)
    {
      Application.SetPlatform(new LinuxPlatform(this));
    }

    public void Run()
    {

    }
  }

  internal sealed class LinuxPlatform : Inv.Platform
  {
    public LinuxPlatform(LinuxEngine Engine)
    {
      this.Engine = Engine;
      this.CurrentProcess = System.Diagnostics.Process.GetCurrentProcess();
    }

    int Platform.ThreadAffinity()
    {
      return Thread.CurrentThread.ManagedThreadId;
    }
    string Platform.CalendarTimeZoneName()
    {
      return TimeZoneInfo.Local.DisplayName;
    }
    void Platform.CalendarShowPicker(CalendarPicker CalendarPicker)
    {
      // TODO: Linux date/time picker.
    }
    bool Platform.EmailSendMessage(EmailMessage EmailMessage)
    {
      // TODO: mail.

      return false;
    }
    bool Platform.PhoneIsSupported
    {
      get { return false; }
    }
    void Platform.PhoneDial(string PhoneNumber)
    {
    }
    void Platform.PhoneSMS(string PhoneNumber)
    {
    }
    long Platform.DirectoryGetLengthFile(File File)
    {
      return new System.IO.FileInfo(SelectFilePath(File)).Length;
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcFile(File File)
    {
      return new System.IO.FileInfo(SelectFilePath(File)).LastWriteTimeUtc;
    }
    void Platform.DirectorySetLastWriteTimeUtcFile(File File, DateTime Timestamp)
    {
      System.IO.File.SetLastWriteTimeUtc(SelectFilePath(File), Timestamp);
    }
    Stream Platform.DirectoryCreateFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), FileMode.Create, FileAccess.Write, FileShare.Read, 65536);
    }
    Stream Platform.DirectoryAppendFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), FileMode.Append, FileAccess.Write, FileShare.Read, 65536);
    }
    Stream Platform.DirectoryOpenFile(File File)
    {
      return new System.IO.FileStream(SelectFilePath(File), FileMode.Open, FileAccess.Read, FileShare.Read, 65536);
    }
    bool Platform.DirectoryExistsFile(File File)
    {
      return System.IO.File.Exists(SelectFilePath(File));
    }
    void Platform.DirectoryDeleteFile(File File)
    {
      System.IO.File.Delete(SelectFilePath(File));
    }
    void Platform.DirectoryCopyFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Copy(SelectFilePath(SourceFile), SelectFilePath(TargetFile));
    }
    void Platform.DirectoryMoveFile(File SourceFile, File TargetFile)
    {
      System.IO.File.Move(SelectFilePath(SourceFile), SelectFilePath(TargetFile));
    }
    IEnumerable<File> Platform.DirectoryGetFolderFiles(Folder Folder, string FileMask)
    {
      return new DirectoryInfo(SelectFolderPath(Folder)).GetFiles(FileMask).Select(F => Folder.NewFile(F.Name));
    }
    bool Platform.DirectoryExistsAsset(Asset Asset)
    {
      return System.IO.File.Exists(SelectAssetPath(Asset));
    }
    Stream Platform.DirectoryOpenAsset(Asset Asset)
    {
      return new System.IO.FileStream(SelectAssetPath(Asset), FileMode.Open, FileAccess.Read, FileShare.Read, 65536);
    }
    string Platform.DirectoryGetFolderPath(Inv.Folder Folder)
    {
      return SelectFolderPath(Folder);
    }
    string Platform.DirectoryGetFilePath(Inv.File File)
    {
      return SelectFilePath(File);
    }
    bool Platform.LocationIsSupported
    {
#if DEBUG
      get { return true; }
#else
      get { return false; }
#endif
    }
    void Platform.LocationLookup(LocationResult LocationLookup)
    {
      // TODO: implement using web services?
      LocationLookup.SetPlacemarks(new LocationPlacemark[] { });
    }
    void Platform.AudioPlaySound(Inv.Sound Sound, float Volume, float Rate, float Pan)
    {
      // TODO: play sound.
    }
    void Platform.WindowBrowse(Inv.File File)
    {
    }
    void Platform.WindowPost(Action Action)
    {
      //Engine.Asynchronise(Action);
    }
    long Platform.ProcessMemoryUsedBytes()
    {
      CurrentProcess.Refresh();
      return CurrentProcess.PrivateMemorySize64;
    }
    void Platform.ProcessMemoryReclamation()
    {
      //Engine.Reclamation();
    }
    void Platform.WebClientConnect(WebClient WebClient)
    {
      var TcpClient = new Inv.Tcp.Client(WebClient.Host, WebClient.Port, WebClient.CertHash);
      TcpClient.Connect();

      WebClient.Node = TcpClient;
      WebClient.SetStreams(TcpClient.Stream, TcpClient.Stream);
    }
    void Platform.WebClientDisconnect(WebClient WebClient)
    {
      var TcpClient = (Inv.Tcp.Client)WebClient.Node;
      if (TcpClient != null)
      {
        WebClient.Node = null;
        WebClient.SetStreams(null, null);

        TcpClient.Disconnect();
      }
    }
    void Platform.WebLaunchUri(Uri Uri)
    {
      System.Diagnostics.Process.Start(Uri.AbsoluteUri);
    }
    void Platform.WebInstallUri(Uri Uri)
    {
      System.Diagnostics.Process.Start(Uri.AbsoluteUri);
    }
    void Platform.MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
    }
    void Platform.VaultLoadSecret(Secret Secret)
    {
      throw new NotImplementedException();
    }
    void Platform.VaultSaveSecret(Secret Secret)
    {
      throw new NotImplementedException();
    }
    void Platform.VaultDeleteSecret(Secret Secret)
    {
      throw new NotImplementedException();
    }

    private string SelectAssetPath(Asset Asset)
    {
      return Path.Combine(Inv.LinuxShell.MainFolderPath, "Assets", Asset.Name);
    }
    private string SelectFilePath(File File)
    {
      return System.IO.Path.Combine(SelectFolderPath(File.Folder), File.Name);
    }
    private string SelectFolderPath(Folder Folder)
    {
      string Result;

      if (Folder.Name != null)
        Result = System.IO.Path.Combine(LinuxShell.MainFolderPath, Folder.Name);
      else
        Result = LinuxShell.MainFolderPath;

      System.IO.Directory.CreateDirectory(Result);

      return Result;
    }

    private readonly LinuxEngine Engine;
    private readonly System.Diagnostics.Process CurrentProcess;

    // TODO: implement properly.
    void Platform.DirectoryShowFilePicker(DirectoryFilePicker FilePicker)
    {
      throw new NotImplementedException();
    }
    void Platform.WindowCall(Action Action)
    {
      throw new NotImplementedException();
    }
    void Platform.WebServerConnect(WebServer WebServer)
    {
      var TcpServer = new Inv.Tcp.Server(WebServer.Host, WebServer.Port, WebServer.CertHash);
      TcpServer.AcceptEvent += (TcpChannel) => WebServer.AcceptChannel(TcpChannel, TcpChannel.Stream, TcpChannel.Stream);
      TcpServer.RejectEvent += (TcpChannel) => WebServer.RejectChannel(TcpChannel);

      WebServer.Node = TcpServer;
      WebServer.DropDelegate = (Node) => ((Inv.Tcp.Channel)Node).Drop();

      TcpServer.Connect();
    }
    void Platform.WebServerDisconnect(WebServer WebServer)
    {
      var TcpServer = (Inv.Tcp.Server)WebServer.Node;
      if (TcpServer != null)
      {
        TcpServer.Disconnect();

        WebServer.Node = null;
        WebServer.DropDelegate = null;
      }
    }
    void Platform.AudioPlayClip(AudioClip Clip)
    {
      throw new NotImplementedException();
    }
    void Platform.AudioStopClip(AudioClip Clip)
    {
      throw new NotImplementedException();
    }
    Dimension Platform.WindowGetDimension(Panel Panel)
    {
      throw new NotImplementedException();
    }
    string Platform.ProcessAncillaryInformation()
    {
      throw new NotImplementedException();
    }
    string Platform.ClipboardGet()
    {
      throw new NotImplementedException();
    }
    void Platform.ClipboardSet(string Text)
    {
      throw new NotImplementedException();
    }
    Dimension Platform.GraphicsGetDimension(Image Image)
    {
      throw new NotImplementedException();
    }
    Image Platform.GraphicsGrayscale(Image Image)
    {
      throw new NotImplementedException();
    }
    Image Platform.GraphicsTint(Image Image, Colour Colour)
    {
      throw new NotImplementedException();
    }
    Image Platform.GraphicsResize(Image Image, Dimension Dimension)
    {
      throw new NotImplementedException();
    }
    void Platform.LocationShowMap(string Location)
    {
      throw new NotImplementedException();
    }
    void Platform.WindowStartAnimation(Animation Animation)
    {
      throw new NotImplementedException();
    }
    void Platform.WindowStopAnimation(Animation Animation)
    {
      throw new NotImplementedException();
    }
    void Platform.WindowShare(File File)
    {
      throw new NotImplementedException();
    }
    void Platform.WebBroadcastConnect(WebBroadcast Broadcast)
    {
      throw new NotImplementedException();
    }
    void Platform.WebBroadcastDisconnect(WebBroadcast Broadcast)
    {
      throw new NotImplementedException();
    }
    TimeSpan Platform.AudioGetSoundLength(Sound Sound)
    {
      throw new NotImplementedException();
    }
    void Platform.HapticFeedback(HapticFeedback Feedback)
    {
      throw new NotImplementedException();
    }
  }
}
