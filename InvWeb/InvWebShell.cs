﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;
using System.Diagnostics;

// TODO: escaping html.

namespace Inv
{
  public static class WebShell
  {
    public static int FileStorageMB = 50;

    public static void Run(Inv.Application InvApplication)
    {
      var WebEngine = new WebEngine(InvApplication);
      WebEngine.Run();
    }
    public static void RunTest()
    {
      var WebDocument = new WebDocument();
      var WebWindow = new WebWindow();

      var WebBody = WebDocument.GetBody();

      var WebCanvas = WebDocument.NewCanvas();
      WebBody.AddElement(WebCanvas);

      var WebButton = WebDocument.NewButton();
      WebBody.AddElement(WebButton);
      WebButton.Text = WebWindow.Width + " x " + WebWindow.Height;
      WebButton.ClickEvent += () =>
      {
        var ResButton = WebDocument.NewButton();
        ResButton.Text = "Hello!";
        WebBody.AddElement(ResButton);
      };

      var WebTable = WebDocument.NewTable();
      WebBody.AddElement(WebTable);

      WebBody.KeyDownEvent += (WebKeystroke) =>
      {
        var InvKeystroke = WebKeyboard.TranslateKeystroke(WebKeystroke);

        if (InvKeystroke == null)
          WebButton.Text = "UNHANDLED KEYCODE: " + WebKeystroke.Code;
        else
          WebButton.Text = InvKeystroke.ToString();
      };

      foreach (var Index in new int[] { 1, 2, 3, 4, 5 })
      {
        var Button = WebDocument.NewButton();
        WebTable.AddRow().AddCell().AddElement(Button);

        var Label = WebDocument.NewLabel();
        Button.AddElement(Label);
        Label.Text = "Label " + Index;

        Button.ClickEvent += () =>
        {
          Label.Text += " P" + Index;
        };
      }

      var ctx = WebCanvas.Get2DContext();

      WebWindow.ResizeEvent += () => WebButton.Text = WebWindow.Width + " x " + WebWindow.Height;
      WebWindow.SetInterval(25, () =>
      {
        TestX += 2;

        ctx.ClearRect(0, 0, 300, 150);
        ctx.FillStyle = Inv.Colour.Orange.WebColour();
        ctx.FillRect(TestX, TestY, 20, 20);

        // TODO: 60 FPS
      });
    }

    internal static string WebEncodeText(this string Text)
    {
      // TODO: HtmlEncode is not implemented in JSIL.

      if (Text == null)
        return null;
      else
        return Text; //System.Net.WebUtility.HtmlEncode(Text);
    }
    internal static string WebDecodeText(this string Text)
    {
      // TODO: HtmlEncode is not implemented in JSIL.

      if (Text == null)
        return null;
      else
        return Text; //System.Net.WebUtility.HtmlDecode(Text);
    }
    internal static string WebColour(this Inv.Colour? Colour)
    {
      if (Colour == null)
        return "";
      else
        return Colour.Value.WebColour();
    }
    internal static string WebColour(this Inv.Colour Colour)
    {
      var Record = Colour.GetARGBRecord();

      return string.Format("rgba({0}, {1}, {2}, {3})", Record.R, Record.G, Record.B, Record.A / 255.0F);
    }

    private static int TestX = 10;
    private static int TestY = 20;
  }

  internal sealed class WebPlatform : Inv.Platform
  {
    public WebPlatform(WebEngine Engine)
    {
      this.Engine = Engine;
    }

    int Platform.ThreadAffinity()
    {
      return 1;
    }
    void Platform.CalendarShowPicker(CalendarPicker CalendarPicker)
    {
      throw new NotImplementedException();
    }
    bool Platform.EmailSendMessage(EmailMessage EmailMessage)
    {
      var MailUri = new Uri(string.Format("mailto:{0}?subject={1}&body={2}", EmailMessage.GetTos().Select(T => T.Address).AsSeparatedText(","), Uri.EscapeDataString(EmailMessage.Subject ?? ""), Uri.EscapeDataString(EmailMessage.Body ?? "")));

      // TODO: attachments are not supported by mailto protocol!
      if (EmailMessage.HasAttachments())
        return false;
      else
        return true;
    }
    bool Platform.PhoneIsSupported
    {
      get { return true; }
    }
    bool Platform.PhoneDial(string PhoneNumber)
    {
      // TODO: iOS requirement? Engine.WebWindow.Handle.open("tel:" + PhoneNumber, "_system");

      Engine.WebWindow.Handle.location.href = "tel:" + PhoneNumber;
      return true;
    }
    bool Platform.PhoneSMS(string PhoneNumber)
    {
      Engine.WebWindow.Handle.location.href = "sms:" + PhoneNumber;
      return true;
    }
    long Platform.DirectoryGetLengthFile(File File)
    {
      return 0L;
    }
    DateTime Platform.DirectoryGetLastWriteTimeUtcFile(File File)
    {
      return DateTime.UtcNow;
    }
    void Platform.DirectorySetLastWriteTimeUtcFile(File File, DateTime Timestamp)
    {
    }
    System.IO.Stream Platform.DirectoryOpenAsset(Asset Asset)
    {
      return new System.IO.MemoryStream(); // System.IO.Stream.Null;
    }
    System.IO.Stream Platform.DirectoryCreateFile(File File)
    {
      return new System.IO.MemoryStream(); // System.IO.Stream.Null;
    }
    System.IO.Stream Platform.DirectoryAppendFile(File File)
    {
      return new System.IO.MemoryStream(); // System.IO.Stream.Null;
    }
    System.IO.Stream Platform.DirectoryOpenFile(File File)
    {
      return new System.IO.MemoryStream(); // System.IO.Stream.Null;
    }
    bool Platform.DirectoryExistsFile(File File)
    {
      return false;
    }
    void Platform.DirectoryDeleteFile(File File)
    {
    }
    void Platform.DirectoryCopyFile(File SourceFile, File TargetFile)
    {
    }
    void Platform.DirectoryMoveFile(File SourceFile, File TargetFile)
    {
    }
    IEnumerable<File> Platform.DirectoryGetFolderFiles(Folder Folder, string FileMask)
    {
      return new File[] { };
    }
    bool Platform.LocationIsSupported
    {
      get { return false; }
    }
    void Platform.LocationLookup(LocationLookup LocationLookup)
    {
      throw new NotImplementedException();
    }
    void Platform.AudioPlaySound(Sound Sound, float Volume)
    {
      Engine.WebWindow.PlayAudio(Sound, Sound.GetBuffer(), Volume);
    }
    void Platform.WindowAsynchronise(Action Action)
    {
      // TODO: Javascript is not multi-threaded?
      Action();
    }
    Modifier Platform.WindowGetModifier()
    {
      return new Modifier()
      {
        IsLeftAlt = Engine.WebWindow.IsAltPressed,
        IsRightAlt = Engine.WebWindow.IsAltPressed,
        IsLeftCtrl = Engine.WebWindow.IsCtrlPressed,
        IsRightCtrl = Engine.WebWindow.IsCtrlPressed,
        IsLeftShift = Engine.WebWindow.IsShiftPressed,
        IsRightShift = Engine.WebWindow.IsShiftPressed,
      };
    }
    long Platform.ProcessMemoryBytes()
    {
      return Engine.WebWindow.PerformanceMemoryBytes();
    }
    void Platform.WebSocketConnect(WebSocket Socket)
    {
      var SocketClient = new Inv.Socket.Client(Socket.Host, Socket.Port);
      SocketClient.Connect();

      Socket.Node = SocketClient;
      Socket.InputStream = SocketClient.InputStream;
      Socket.OutputStream = SocketClient.OutputStream;
    }
    void Platform.WebSocketDisconnect(WebSocket Socket)
    {
      var SocketClient = (Inv.Socket.Client)Socket.Node;
      if (SocketClient != null)
      {
        Socket.Node = null;
        Socket.InputStream = null;
        Socket.OutputStream = null;

        SocketClient.Disconnect();
      }
    }
    void Platform.WebLaunchUri(Uri Uri)
    {
    }
    void Platform.MarketBrowse(string AppleiTunesID, string GooglePlayID, string WindowsStoreID)
    {
    }

    private WebEngine Engine;
  }

  internal static class WebKeyboard
  {
    static WebKeyboard()
    {
      CodeKeyDictionary = new Dictionary<int, Inv.Key>()
      {
        { 8, Inv.Key.Delete }, // TODO: Backspace.
        { 9, Inv.Key.Tab },
        { 13, Inv.Key.Enter },
        //{ 16, Inv.Key.Shift },
        //{ 17, Inv.Key.Ctrl },
        //{ 18, Inv.Key.Alt },
        //{ 19, Inv.Key.Break },
        //{ 20, Inv.Key.CapsLock },
        { 27, Inv.Key.Escape },
        { 32, Inv.Key.Space },
        { 33, Inv.Key.PageUp },
        { 34, Inv.Key.PageDown },
        { 35, Inv.Key.End },
        { 36, Inv.Key.Home },
        { 37, Inv.Key.Left },
        { 38, Inv.Key.Up },
        { 39, Inv.Key.Right },
        { 40, Inv.Key.Down },
        { 45, Inv.Key.Insert },
        { 46, Inv.Key.Delete },
        { 48, Inv.Key.n0 },
        { 49, Inv.Key.n1 },
        { 50, Inv.Key.n2 },
        { 51, Inv.Key.n3 },
        { 52, Inv.Key.n4 },
        { 53, Inv.Key.n5 },
        { 54, Inv.Key.n6 },
        { 55, Inv.Key.n7 },
        { 56, Inv.Key.n8 },
        { 57, Inv.Key.n9 },
        { 65, Inv.Key.A },
        { 66, Inv.Key.B },
        { 67, Inv.Key.C },
        { 68, Inv.Key.D },
        { 69, Inv.Key.E },
        { 70, Inv.Key.F },
        { 71, Inv.Key.G },
        { 72, Inv.Key.H },
        { 73, Inv.Key.I },
        { 74, Inv.Key.J },
        { 75, Inv.Key.K },
        { 76, Inv.Key.L },
        { 77, Inv.Key.M },
        { 78, Inv.Key.N },
        { 79, Inv.Key.O },
        { 80, Inv.Key.P },
        { 81, Inv.Key.Q },
        { 82, Inv.Key.R },
        { 83, Inv.Key.S },
        { 84, Inv.Key.T },
        { 85, Inv.Key.U },
        { 86, Inv.Key.V },
        { 87, Inv.Key.W },
        { 88, Inv.Key.X },
        { 89, Inv.Key.Y },
        { 90, Inv.Key.Z },
        //{ 91, Inv.Key.Windows },
        //{ 93, Inv.Key.RightClick },
        { 96, Inv.Key.Insert },
        { 97, Inv.Key.End },
        { 98, Inv.Key.Down },
        { 99, Inv.Key.PageDown },
        { 100, Inv.Key.Left },
        { 101, Inv.Key.Clear },
        { 102, Inv.Key.Right },
        { 103, Inv.Key.Home },
        { 104, Inv.Key.Up },
        { 105, Inv.Key.PageUp },
        { 106, Inv.Key.Asterisk },
        { 107, Inv.Key.Plus },
        { 109, Inv.Key.Minus },
        { 110, Inv.Key.Period },
        { 111, Inv.Key.Slash },
        { 112, Inv.Key.F1 },
        { 113, Inv.Key.F2 },
        { 114, Inv.Key.F3 },
        { 115, Inv.Key.F4 },
        { 116, Inv.Key.F5 },
        { 117, Inv.Key.F6 },
        { 118, Inv.Key.F7 },
        { 119, Inv.Key.F8 },
        { 120, Inv.Key.F9 },
        { 121, Inv.Key.F10 },
        { 122, Inv.Key.F11 },
        { 123, Inv.Key.F12 },
        //{ 144, Inv.Key.NumLock },
        //{ 145, Inv.Key.ScrollLock },
        //{ 182, Inv.Key.MyComputer },
        //{ 183, Inv.Key.MyCalculator },
        //{ 186, Inv.Key.Semicolon },
        //{ 187, Inv.Key.Equal },
        { 188, Inv.Key.Comma },
        { 189, Inv.Key.Minus },
        { 190, Inv.Key.Period },
        { 191, Inv.Key.Slash },
        { 192, Inv.Key.BackQuote },
        //{ 219, Inv.Key.OpenSquare },
        { 220, Inv.Key.Backslash },
        //{ 221, Inv.Key.CloseSquare },
        //{ 222, Inv.Key.SingleQuote },
      };
    }

    public static Inv.Keystroke TranslateKeystroke(WebKeystroke WebKeystroke)
    {
      Inv.Key InvKey;
      if (!CodeKeyDictionary.TryGetValue(WebKeystroke.Code, out InvKey))
        return null;

      return new Inv.Keystroke()
      {
        Key = InvKey,
        Modifier = new Inv.Modifier()
        {
          IsLeftAlt = WebKeystroke.IsAlt,
          IsRightAlt = WebKeystroke.IsAlt,
          IsLeftCtrl = WebKeystroke.IsCtrl,
          IsRightCtrl = WebKeystroke.IsCtrl,
          IsLeftShift = WebKeystroke.IsShift,
          IsRightShift = WebKeystroke.IsShift,
        }
      };
    }

    private static Dictionary<int, Key> CodeKeyDictionary;
  }

  internal sealed class WebEngine
  {
    public WebEngine(Inv.Application InvApplication)
    {
      this.InvApplication = InvApplication;
      this.WebDocument = new WebDocument();
      this.WebWindow = new WebWindow();

      this.ImageDictionary = new Dictionary<Inv.Image, WebImage>();

      this.RouteDictionary = new Dictionary<Type, Func<Inv.Panel, WebNode>>()
      {
        { typeof(Inv.Button), TranslateButton },
        { typeof(Inv.Canvas), TranslateCanvas },
        { typeof(Inv.Dock), TranslateDock },
        { typeof(Inv.Edit), TranslateEdit },
        { typeof(Inv.Graphic), TranslateGraphic },
        { typeof(Inv.Label), TranslateLabel },
        { typeof(Inv.Memo), TranslateMemo },
        { typeof(Inv.Overlay), TranslateOverlay },
        { typeof(Inv.Render), TranslateRender },
        { typeof(Inv.Scroll), TranslateScroll },
        { typeof(Inv.Frame), TranslateFrame },
        { typeof(Inv.Stack), TranslateStack },
        { typeof(Inv.Table), TranslateTable },
      };

      this.WebHtml = WebDocument.GetHtml();

      this.WebHead = WebDocument.GetHead();

      this.WebBody = WebDocument.GetBody();

      WebBody.KeyDownEvent += (WebKeystroke) =>
      {
        var InvActiveSurface = InvApplication.Window.ActiveSurface;

        if (InvActiveSurface != null)
        {
          var InvKeystroke = WebKeyboard.TranslateKeystroke(WebKeystroke);

          if (InvKeystroke != null)
          {
            InvActiveSurface.KeystrokeInvoke(InvKeystroke);

            Process();
          }
        }
      };

      if (!WebDocument.IsHtml5)
      {
        var WarningLabel = WebDocument.NewLabel();
        WarningLabel.Text = "WARNING: document must select html5 as the DOCTYPE";
        WebBody.AddElement(WarningLabel);
      }
      else if (InvApplication.Title != WebHead.Title)
      {
        // TODO: the above check isn't working - WebHead.Title is blank at this stage.

        //var WarningLabel = WebDocument.NewLabel();
        //WarningLabel.Text = "WARNING: application title does not match document title";
        //WebBody.AddElement(WarningLabel);
      }
      else
      {
        // TODO: check for Inv.css stylesheet link.
      }

      this.WebMaster = WebDocument.NewDiv();
      WebBody.AddElement(WebMaster);
      WebMaster.Handle.style.width = "100%";
      WebMaster.Handle.style.height = "100%";

      this.ImageBuffer = WebDocument.NewCanvas(); // off-screen buffer for custom rendering.
      this.ImageContext = ImageBuffer.Get2DContext();

      WebWindow.ResizeEvent += () => Arrange();
      // TODO: close query?
      WebWindow.CloseEvent += () => Close();

      InvApplication.Platform = new WebPlatform(this);
      InvApplication.Begin();

      WebWindow.RequestFileSystem(WebShell.FileStorageMB);
    }

    public void Run()
    {
      // TODO: javascript for browser information?
      InvApplication.Device.Name = "";
      InvApplication.Device.Model = "";
      InvApplication.Device.System = "";

      Resize();

      InvApplication.StartInvoke();

      Process();

      this.ProcessTimer = WebWindow.SetInterval(16, Process);
    }

    internal WebWindow WebWindow { get; private set; }
    internal WebDocument WebDocument { get; private set; }

    private void Arrange()
    {
      Resize();

      var InvActiveSurface = InvApplication.Window.ActiveSurface;

      if (InvActiveSurface != null)
        InvActiveSurface.ArrangeInvoke();
    }
    private void Resize()
    {
      InvApplication.Window.Width = WebWindow.Width; // (int)(WebWindow.Width / 96.0F * 160.0F);
      InvApplication.Window.Height = WebWindow.Height; // (int)(WebWindow.Height / 96.0F * 160.0F);
    }
    private void Close()
    {
      ProcessTimer.Stop();

      InvApplication.StopInvoke();
    }
    private void Process()
    {
      if (InvApplication.IsExit)
      {
        WebWindow.Close();
      }
      else
      {
        var InvWindow = InvApplication.Window;

        if (InvWindow.ActiveTimerSet.Count > 0)
        {
          foreach (var InvTimer in InvWindow.ActiveTimerSet.ToArray())
          {
            var WebTimer = AccessTimer(InvTimer, S =>
            {
              var Result = WebWindow.NewTimer();
              Result.IntervalEvent += () => InvTimer.IntervalInvoke();
              return Result;
            });

            if (WebTimer.IntervalTime != InvTimer.IntervalTime)
              WebTimer.IntervalTime = InvTimer.IntervalTime;

            if (InvTimer.IsEnabled && !WebTimer.Enabled)
              WebTimer.Start();
            else if (!InvTimer.IsEnabled && WebTimer.Enabled)
              WebTimer.Stop();

            if (!InvTimer.IsEnabled)
              InvWindow.ActiveTimerSet.Remove(InvTimer);
          }
        }        

        var InvActiveSurface = InvWindow.ActiveSurface;

        if (InvActiveSurface != null)
        {
          var WebSurface = AccessSurface(InvActiveSurface, S =>
          {
            var Result = new WebSurface(WebDocument);
            return Result;
          });

          if (!WebMaster.HasElement(WebSurface))
            InvActiveSurface.ArrangeInvoke();

          ProcessTransition(WebSurface);

          InvActiveSurface.ComposeInvoke();

          if (InvActiveSurface.Render())
          {
            TranslateBackground(InvActiveSurface.Background, WebSurface);

            WebSurface.SetContent(TranslatePanel(InvActiveSurface.Content));
          }

          InvActiveSurface.ProcessChanges(P => TranslatePanel(P));

          if (InvActiveSurface.Focus != null)
          {
            // TODO: focus.
          }
        }
        else
        {
          WebMaster.RemoveElements();
        }

        if (InvWindow.Render())
        {
          if (InvWindow.Background.Render())
            TranslateBackgroundColour(InvWindow.Background.Colour ?? Inv.Colour.Black, WebMaster);

          TranslateFont(InvWindow.DefaultFont, WebMaster);
        }

        // TODO: animations.

        InvWindow.DisplayRate.Calculate();
      }
    }
    private void ProcessTransition(WebSurface WebSurface)
    {
      var InvWindow = InvApplication.Window;

      var InvTransition = InvWindow.ActiveTransition;
      if (InvTransition == null)
      {
      }
      else
      {
        // TODO: animations.

        if (!WebMaster.HasElement(WebSurface))
        {
          WebMaster.RemoveElements();
          WebMaster.AddElement(WebSurface);
        }

        InvWindow.ActiveTransition = null;
      }
    }

    private WebNode TranslateButton(Inv.Panel InvPanel)
    {
      var InvButton = (Inv.Button)InvPanel;

      var WebNode = AccessPanel(InvButton, P =>
      {
        var Result = WebDocument.NewButton();
        Result.Class = "button";
        Result.MouseEnterEvent += () =>
        {
          TranslateBackgroundColour(P.Background.Colour != null ? P.Background.Colour.Value.Lighten(0.25F) : (Inv.Colour?)null, Result);
        };
        Result.MouseLeaveEvent += () =>
        {
          TranslateBackgroundColour(P.Background.Colour, Result);
        };
        Result.MouseDownEvent += () =>
        {
          TranslateBackgroundColour(P.Background.Colour != null ? P.Background.Colour.Value.Darken(0.25F) : (Inv.Colour?)null, Result);
        };
        Result.MouseUpEvent += () =>
        {
          TranslateBackgroundColour(P.Background.Colour, Result);
        };
        Result.ClickEvent += () =>
        {
          if (InvApplication.Window.IsActiveSurface(P.Surface))
          {
            P.SingleTapInvoke();

            Process();
          }
        };
        Result.ContextMenuEvent += () =>
        {
          if (InvApplication.Window.IsActiveSurface(P.Surface))
          {
            P.ContextTapInvoke();

            Process();
          }
        };
        return Result;
      });

      RenderPanel(InvButton, WebNode, () =>
      {
        var WebLayout = WebNode.LayoutElement;
        var WebButton = WebNode.PanelElement;

        TranslateLayout(InvButton, WebLayout, WebButton);

        WebButton.IsEnabled = InvButton.IsEnabled;
        //WebButton.Button.Focusable = InvButton.IsFocused;

        if (InvButton.ContentSingleton.Render())
        {
          WebButton.RemoveElements();

          var WebContent = TranslatePanel(InvButton.ContentSingleton.Data);
          if (WebContent != null)
            WebButton.AddElement(WebContent);
        }
      });

      return WebNode;
    }
    private WebNode TranslateCanvas(Inv.Panel InvPanel)
    {
      var InvCanvas = (Inv.Canvas)InvPanel;

      var WebNode = AccessPanel(InvCanvas, P =>
      {
        var Result = WebDocument.NewDiv();
        Result.Class = "canvas";
        return Result;
      });

      RenderPanel(InvCanvas, WebNode, () =>
      {
        var WebLayout = WebNode.LayoutElement;
        var WebCanvas = WebNode.PanelElement;

        TranslateLayout(InvCanvas, WebLayout, WebCanvas);

        if (InvCanvas.PieceCollection.Render())
        {
          WebCanvas.RemoveElements();

          foreach (var InvElement in InvCanvas.PieceCollection)
          {
            var WebElement = WebDocument.NewDiv();
            WebCanvas.AddElement(WebElement);
            WebElement.Handle.style.position = "absolute";
            WebElement.Handle.style.left = HorizontalPtToPx(InvElement.Rect.Left) + "px";
            WebElement.Handle.style.top = VerticalPtToPx(InvElement.Rect.Top) + "px";
            WebElement.Handle.style.width = HorizontalPtToPx(InvElement.Rect.Width) + "px";
            WebElement.Handle.style.height = HorizontalPtToPx(InvElement.Rect.Height) + "px";
            WebElement.Handle.style.pointerEvents = "none";

            var WebPanel = TranslatePanel(InvElement.Panel);
            WebElement.AddElement(WebPanel);
          }
        }
      });

      return WebNode;
    }
    private WebNode TranslateDock(Inv.Panel InvPanel)
    {
      var InvDock = (Inv.Dock)InvPanel;

      var IsHorizontal = InvDock.Orientation == DockOrientation.Horizontal;

      var WebNode = AccessPanel(InvDock, P =>
      {
        var Result = WebDocument.NewDiv();
        Result.Class = IsHorizontal ? "hlayout" : "vlayout";
        return Result;
      });

      RenderPanel(InvDock, WebNode, () =>
      {
        var WebLayout = WebNode.LayoutElement;
        var WebDock = WebNode.PanelElement;

        TranslateLayout(InvDock, WebLayout, WebDock);

        if (InvDock.CollectionRender())
        {
          WebDock.RemoveElements();

          var DockElementClass = IsHorizontal ? "hlayoutitem" : "vlayoutitem";

          foreach (var InvElement in InvDock.GetHeaders())
          {
            var WebCell = WebDocument.NewDiv();
            WebDock.AddElement(WebCell);
            WebCell.Class = DockElementClass;

            var WebElement = TranslatePanel(InvElement);
            WebCell.AddElement(WebElement);
            WebElement.Handle.style.flexGrow = "0";
          }

          if (InvDock.HasClients())
          {
            foreach (var InvElement in InvDock.GetClients())
            {
              var WebCell = WebDocument.NewDiv();
              WebDock.AddElement(WebCell);
              WebCell.Class = DockElementClass;
              WebCell.Handle.style.flexGrow = "1";

              var WebElement = TranslatePanel(InvElement);
              WebCell.AddElement(WebElement);
              WebElement.Handle.style.flexGrow = "1";
            }
          }
          else
          {
            // fill unused client space so the footers are pushed to the bottom.
            var WebCell = WebDocument.NewDiv();
            WebDock.AddElement(WebCell);
            WebCell.Class = DockElementClass;
            WebCell.Handle.style.flexGrow = "1";
          }

          foreach (var InvElement in InvDock.GetFooters())
          {
            var WebCell = WebDocument.NewDiv();
            WebDock.AddElement(WebCell);
            WebCell.Class = DockElementClass;

            var WebElement = TranslatePanel(InvElement);
            WebCell.AddElement(WebElement);
            WebElement.Handle.style.flexGrow = "0";
          }
        }
      });

      return WebNode;
    }
    private WebNode TranslateEdit(Inv.Panel InvPanel)
    {
      var InvEdit = (Inv.Edit)InvPanel;

      var WebNode = AccessPanel(InvEdit, P =>
      {
        var Result = WebDocument.NewTextInput();
        Result.Class = "edit";
        Result.ChangeEvent += () => P.ChangeInvoke(Result.Text);
        return Result;
      });

      RenderPanel(InvEdit, WebNode, () =>
      {
        var WebLayout = WebNode.LayoutElement;
        var WebEdit = WebNode.PanelElement;

        TranslateLayout(InvEdit, WebLayout, WebEdit);
        TranslateFont(InvEdit.Font, WebEdit);

        WebEdit.IsReadOnly = InvEdit.IsReadOnly;
        WebEdit.Text = InvEdit.Text ?? "";
      });

      return WebNode;
    }
    private WebNode TranslateGraphic(Inv.Panel InvPanel)
    {
      var InvGraphic = (Inv.Graphic)InvPanel;

      var WebNode = AccessPanel(InvGraphic, P =>
      {
        var Result = WebDocument.NewImage();
        Result.Class = "graphic";
        return Result;
      });

      RenderPanel(InvGraphic, WebNode, () =>
      {
        var WebLayout = WebNode.LayoutElement;
        var WebGraphic = WebNode.PanelElement;

        TranslateLayout(InvGraphic, WebLayout, WebGraphic);

        // TODO: image without a size specified must be reduced by one third because they are designed as '3 pixels per 1 point'.
        //       scale and transform origin are guesses that need to be tested.
        if (InvGraphic.Size.Width == null && InvGraphic.Size.Height == null)
        {
          WebGraphic.Handle.style.transform = "scale(0.3, 0.3)";
          WebGraphic.Handle.style.transformOrigin = "50% 100%";
        }
        else if (InvGraphic.Size.Width == null)
        {
          WebGraphic.Handle.style.transform = "scaleX(0.3)";
          WebGraphic.Handle.style.transformOrigin = "50% 100%";
        }
        else if (InvGraphic.Size.Height == null)
        {
          WebGraphic.Handle.style.transform = "scaleY(0.3)";
          WebGraphic.Handle.style.transformOrigin = "50% 100%";
        }

        // TODO: do we need Inv.Image metadata so we can set these fields?
        //if (InvGraphic.Size.Width != null)
        //  WebGraphic.Handle.style.width = HorizontalPtToPx(InvGraphic.Size.Width.Value);
        //
        //if (InvGraphic.Size.Height != null)
        //  WebGraphic.Handle.style.height = VerticalPtToPx(InvGraphic.Size.Height.Value);

        if (InvGraphic.ImageSingleton.Render())
        {
          // TODO: caching this string?
          if (InvGraphic.Image == null)
            WebGraphic.Source = "";
          else
            WebGraphic.Source = TranslateImageSource(InvGraphic.Image);
        }
      });

      return WebNode;
    }
    private WebNode TranslateLabel(Inv.Panel InvPanel)
    {
      var InvLabel = (Inv.Label)InvPanel;

      var WebNode = AccessPanel(InvLabel, P =>
      {
        var Result = WebDocument.NewLabel();
        Result.Class = "label";
        return Result;
      });

      RenderPanel(InvLabel, WebNode, () =>
      {
        var WebLayout = WebNode.LayoutElement;
        var WebLabel = WebNode.PanelElement;

        TranslateLayout(InvLabel, WebLayout, WebLabel);

        TranslateFont(InvLabel.Font, WebLabel);

        // TODO: TextWrapping.

        if (InvLabel.Justification == null)
          WebLabel.Handle.style.removeProperty("textAlign");
        else
          WebLabel.Handle.style.textAlign = InvLabel.Justification == Justification.Left ? "left" : InvLabel.Justification == Justification.Right ? "right" : "center";

        WebLabel.Text = InvLabel.Text ?? "";
      });

      return WebNode;
    }
    private WebNode TranslateMemo(Inv.Panel InvPanel)
    {
      var InvMemo = (Inv.Memo)InvPanel;

      var WebNode = AccessPanel(InvMemo, P =>
      {
        var Result = WebDocument.NewTextArea();
        Result.Class = "memo";
        Result.Wrap = false;
        Result.ChangeEvent += () => P.ChangeInvoke(Result.Text);
        return Result;
      });

      RenderPanel(InvMemo, WebNode, () =>
      {
        var WebLayout = WebNode.LayoutElement;
        var WebMemo = WebNode.PanelElement;

        TranslateLayout(InvMemo, WebLayout, WebMemo);
        TranslateFont(InvMemo.Font, WebMemo);

        WebMemo.IsReadOnly = InvMemo.IsReadOnly;
        WebMemo.Text = InvMemo.Text ?? "";

        // TODO: memo doesn't size to content correctly.
        //WebMemo.Handle.style.height = "auto";
        //WebMemo.Handle.style.height = WebMemo.Handle.scrollHeight + "px";
      });

      return WebNode;
    }
    private WebNode TranslateOverlay(Inv.Panel InvPanel)
    {
      var InvOverlay = (Inv.Overlay)InvPanel;

      var WebNode = AccessPanel(InvOverlay, P =>
      {
        var Result = WebDocument.NewDiv();
        Result.Class = "overlay";
        return Result;
      });

      RenderPanel(InvOverlay, WebNode, () =>
      {
        var WebLayout = WebNode.LayoutElement;
        var WebOverlay = WebNode.PanelElement;

        TranslateLayout(InvOverlay, WebLayout, WebOverlay);

        if (InvOverlay.PanelCollection.Render())
        {
          WebOverlay.RemoveElements();

          foreach (var InvElement in InvOverlay.PanelCollection)
          {
            var WebElement = WebDocument.NewDiv();
            WebOverlay.AddElement(WebElement);
            WebElement.Handle.style.position = "absolute";
            WebElement.Handle.style.left = "0px";
            WebElement.Handle.style.top = "0px";
            WebElement.Handle.style.width = "100%";
            WebElement.Handle.style.height = "100%";
            WebElement.Handle.style.pointerEvents = "none";

            var WebPanel = TranslatePanel(InvElement);
            WebElement.AddElement(WebPanel);
          }
        }
      });

      return WebNode;
    }
    private WebNode TranslateRender(Inv.Panel InvPanel)
    {
      var InvRender = (Inv.Render)InvPanel;

      var WebNode = AccessPanel(InvRender, P =>
      {
        var Result = WebDocument.NewCanvas();
        Result.MouseMoveEvent += (X, Y) => P.MoveInvoke(new Inv.Point(HorizontalPxToPt(X), VerticalPxToPt(Y)));
        Result.MouseDownEvent += (X, Y) => P.PressInvoke(new Inv.Point(HorizontalPxToPt(X), VerticalPxToPt(Y)));
        Result.MouseUpEvent += (X, Y) => P.ReleaseInvoke(new Inv.Point(HorizontalPxToPt(X), VerticalPxToPt(Y)));
        Result.SingleClickEvent += (X, Y) => P.SingleTapInvoke(new Inv.Point(HorizontalPxToPt(X), VerticalPxToPt(Y)));
        Result.DoubleClickEvent += (X, Y) => P.DoubleTapInvoke(new Inv.Point(HorizontalPxToPt(X), VerticalPxToPt(Y)));
        Result.ContextMenuEvent += (X, Y) => P.ContextTapInvoke(new Inv.Point(HorizontalPxToPt(X), VerticalPxToPt(Y)));
        return Result;
      });

      RenderPanel(InvRender, WebNode, () =>
      {
        var WebLayout = WebNode.LayoutElement;
        var WebRender = WebNode.PanelElement;

        TranslateLayout(InvRender, WebLayout, WebRender);

        var RenderWidth = WebRender.ClientWidth;
        var RenderHeight = WebRender.ClientHeight;

        InvRender.ContextSingleton.Data.Width = RenderWidth;
        InvRender.ContextSingleton.Data.Height = RenderHeight;
        if (InvRender.ContextSingleton.Render())
        {
          WebRender.Width = RenderWidth;
          WebRender.Height = RenderHeight;

          var WebContext = WebRender.Get2DContext();

          WebContext.ClearRect(0, 0, RenderWidth, RenderHeight);

          foreach (var InvRenderElement in InvRender.GetCommands())
          {
            switch (InvRenderElement.Type)
            {
              case RenderType.Rectangle:
                var RectangleRect = InvRenderElement.RectangleRect;
                var RectangleFillColour = InvRenderElement.RectangleFillColour;
                var RectangleStrokeColour = InvRenderElement.RectangleStrokeColour;
                var RectangleStrokeThickness = InvRenderElement.RectangleStrokeThickness;

                var RectangleX = HorizontalPtToPx(RectangleRect.Left);
                var RectangleY = VerticalPtToPx(RectangleRect.Top);
                var RectangleWidth = HorizontalPtToPx(RectangleRect.Width);
                var RectangleHeight = VerticalPtToPx(RectangleRect.Height);

                if (RectangleFillColour != null)
                {
                  WebContext.FillStyle = RectangleFillColour.Value.WebColour();
                  WebContext.FillRect(RectangleX, RectangleY, RectangleWidth, RectangleHeight);
                }

                if (RectangleStrokeColour != null)
                {
                  WebContext.LineWidth = VerticalPtToPx(RectangleStrokeThickness);
                  WebContext.StrokeStyle = RectangleStrokeColour.Value.WebColour();
                  WebContext.StrokeRect(RectangleX, RectangleY, RectangleWidth, RectangleHeight);
                }
                break;

              case RenderType.Ellipse:
                var EllipseCenter = InvRenderElement.EllipseCenter;
                var EllipseRadius = InvRenderElement.EllipseRadius;
                var EllipseFillColour = InvRenderElement.EllipseFillColour;
                var EllipseStrokeColour = InvRenderElement.EllipseStrokeColour;
                var EllipseStrokeThickness = InvRenderElement.EllipseStrokeThickness;

                var EllipseCenterX = HorizontalPtToPx(EllipseCenter.X);
                var EllipseCenterY = VerticalPtToPx(EllipseCenter.Y);
                var EllipseRadiusX = HorizontalPtToPx(EllipseRadius.X);
                var EllipseRadiusY = VerticalPtToPx(EllipseRadius.Y);

                if (EllipseFillColour != null)
                {
                  WebContext.FillStyle = EllipseFillColour.Value.WebColour();
                  WebContext.FillEllipse(EllipseCenterX, EllipseCenterY, EllipseRadiusX, EllipseRadiusY);
                }

                if (EllipseStrokeColour != null)
                {
                  WebContext.LineWidth = VerticalPtToPx(EllipseStrokeThickness);
                  WebContext.StrokeStyle = EllipseStrokeColour.Value.WebColour();
                  WebContext.StrokeEllipse(EllipseCenterX, EllipseCenterY, EllipseRadiusX, EllipseRadiusY);
                }
                break;

              case RenderType.Text:
                var TextPoint = InvRenderElement.TextPoint;
                var TextFragment = InvRenderElement.TextFragment;
                var TextFontSize = VerticalPtToPx(InvRenderElement.TextFontSize);

                var TextX = HorizontalPtToPx(TextPoint.X);
                var TextY = VerticalPtToPx(TextPoint.Y);

                WebContext.Handle.font = TextFontSize + "px " + (InvRenderElement.TextFontName.EmptyAsNull() ?? InvApplication.Window.DefaultFont.Name ?? "sans-serif");

                var TextHorizontal = InvRenderElement.TextHorizontal;
                var TextVertical = InvRenderElement.TextVertical;

                if (TextHorizontal != HorizontalPosition.Left)
                {
                  var TextWidth = (int)WebContext.Handle.measureText(TextFragment).width; // NOTE: there is no .height property.

                  if (TextHorizontal == HorizontalPosition.Right)
                    TextX -= TextWidth;
                  else if (TextHorizontal == HorizontalPosition.Center)
                    TextX -= TextWidth / 2;
                }

                if (TextVertical != VerticalPosition.Bottom)
                {
                  var TextHeight = (int)TextFontSize; // TODO: this seems unlikely to be correct in all cases.

                  if (TextVertical == VerticalPosition.Top)
                    TextY += TextHeight;
                  else if (TextVertical == VerticalPosition.Center)
                    TextY += TextHeight / 2;
                }

                WebContext.FillStyle = InvRenderElement.TextFontColour.WebColour();
                WebContext.FillText(TextX, TextY, TextFragment);
                break;

              case RenderType.Image:
                var Image = ImageDictionary.GetOrAdd(InvRenderElement.ImageSource.Value, I =>
                {
                  var Result = WebDocument.NewImage();
                  Result.Source = TranslateImageSource(I);
                  return Result;
                });

                var ImageRect = InvRenderElement.ImageRect;
                var ImageX = HorizontalPtToPx(ImageRect.Left);
                var ImageY = VerticalPtToPx(ImageRect.Top);
                var ImageWidth = HorizontalPtToPx(ImageRect.Width);
                var ImageHeight = VerticalPtToPx(ImageRect.Height);
                var ImageOpacity = InvRenderElement.ImageOpacity;
                var ImageTint = InvRenderElement.ImageTint;
                var ImageMirror = InvRenderElement.ImageMirror;

                if (ImageOpacity != 1.0F || ImageMirror != null || ImageTint != null)
                {
                  WebContext.Save();

                  if (ImageOpacity != 1.0F)
                    WebContext.GlobalOpacity = ImageOpacity;

                  if (ImageMirror != null)
                  {
                    if (ImageMirror.Value == Mirror.Horizontal)
                    {
                      WebContext.Translate(ImageWidth, 0);
                      WebContext.Scale(-1, +1);
                    }
                    else if (ImageMirror.Value == Mirror.Vertical)
                    {
                      ImageY = -ImageY - ImageHeight;
                      WebContext.Scale(+1, -1);
                    }
                  }
                }

                WebContext.DrawImage(ImageX, ImageY, ImageWidth, ImageHeight, Image);

                if (ImageTint != null)
                {
                  // NOTE: chrome requires that the off-screen buffer have the same dimensions as the original image (IE doesn't.)
                  var BufferWidth = Image.Width;
                  var BufferHeight = Image.Height;

                  ImageBuffer.Width = BufferWidth;
                  ImageBuffer.Height = BufferHeight;

                  ImageContext.Save();

                  ImageContext.FillStyle = ImageTint.WebColour();
                  ImageContext.FillRect(0, 0, BufferWidth, BufferHeight);

                  ImageContext.GlobalCompositeOperation = "destination-atop";
                  ImageContext.DrawImage(0, 0, Image);

                  ImageContext.Restore();

                  WebContext.DrawCanvas(ImageX, ImageY, ImageWidth, ImageHeight, ImageBuffer);
                }

                if (ImageOpacity != 1.0F || ImageMirror != null || ImageTint != null)
                  WebContext.Restore();
                break;

              default:
                throw new ApplicationException("RenderType not handled: " + InvRenderElement.Type);
            }
          }
        }
      });

      return WebNode;
    }
    private WebNode TranslateScroll(Inv.Panel InvPanel)
    {
      var InvScroll = (Inv.Scroll)InvPanel;

      var IsHorizontal = InvScroll.Orientation == ScrollOrientation.Horizontal;

      var WebNode = AccessPanel(InvScroll, P =>
      {
        var Result = WebDocument.NewDiv();
        Result.Class = IsHorizontal ? "hscroll" : "vscroll";
        return Result;
      });

      RenderPanel(InvScroll, WebNode, () =>
      {
        var WebLayout = WebNode.LayoutElement;
        var WebScroll = WebNode.PanelElement;

        TranslateLayout(InvScroll, WebLayout, WebScroll);

        if (InvScroll.ContentSingleton.Render())
        {
          WebScroll.RemoveElements();

          var WebContent = TranslatePanel(InvScroll.ContentSingleton.Data);
          if (WebContent != null)
          {
            var WebElement = WebDocument.NewDiv();
            WebScroll.AddElement(WebElement);
            WebElement.Handle.style.position = "absolute";
            WebElement.Handle.style.left = "0px";
            WebElement.Handle.style.top = "0px";
            WebElement.Handle.style.width = "100%";
            WebElement.Handle.style.height = "100%";
            WebElement.Handle.style.pointerEvents = "none";

            WebElement.AddElement(WebContent);
          }
        }
      });

      return WebNode;
    }
    private WebNode TranslateFrame(Inv.Panel InvPanel)
    {
      var InvFrame = (Inv.Frame)InvPanel;

      var WebNode = AccessPanel(InvFrame, P =>
      {
        var Result = WebDocument.NewDiv();
        Result.Class = "socket";
        return Result;
      });

      RenderPanel(InvFrame, WebNode, () =>
      {
        var WebLayout = WebNode.LayoutElement;
        var WebFrame = WebNode.PanelElement;

        TranslateLayout(InvFrame, WebLayout, WebFrame);

        if (InvFrame.ContentSingleton.Render())
        {
          WebFrame.RemoveElements();

          var WebContent = TranslatePanel(InvFrame.ContentSingleton.Data);
          if (WebContent != null)
            WebFrame.AddElement(WebContent);
        }
      });

      return WebNode;
    }
    private WebNode TranslateStack(Inv.Panel InvPanel)
    {
      var InvStack = (Inv.Stack)InvPanel;

      var IsHorizontal = InvStack.Orientation == StackOrientation.Horizontal;

      var WebNode = AccessPanel(InvStack, P =>
      {
        var Result = WebDocument.NewDiv();
        Result.Class = IsHorizontal ? "hlayout" : "vlayout";
        return Result;
      });

      RenderPanel(InvStack, WebNode, () =>
      {
        var WebLayout = WebNode.LayoutElement;
        var WebStack = WebNode.PanelElement;

        TranslateLayout(InvStack, WebLayout, WebStack);

        if (InvStack.PanelCollection.Render())
        {
          WebStack.RemoveElements();

          var StackElementClass = IsHorizontal ? "hlayoutitem" : "vlayoutitem";

          foreach (var InvElement in InvStack.PanelCollection)
          {
            var WebCell = WebDocument.NewDiv();
            WebStack.AddElement(WebCell);
            WebCell.Class = StackElementClass;

            var WebElement = TranslatePanel(InvElement);
            WebCell.AddElement(WebElement);
          }
        }
      });

      return WebNode;
    }
    private WebNode TranslateTable(Inv.Panel InvPanel)
    {
      var InvTable = (Inv.Table)InvPanel;

      var WebNode = AccessPanel(InvTable, P =>
      {
        var Result = WebDocument.NewDiv();
        Result.Class = "table";
        return Result;
      });

      RenderPanel(InvTable, WebNode, () =>
      {
        var WebLayout = WebNode.LayoutElement;
        var WebTable = WebNode.PanelElement;

        TranslateLayout(InvTable, WebLayout, WebTable);

        if (InvTable.CollectionRender())
        {
          WebTable.RemoveElements();

          // TODO: web table.
        }
      });

      return WebNode;
    }
    private void TranslateFont(Inv.Font Font, WebElement WebElement)
    {
      if (Font.Render())
      {
        if (Font.Name == null)
          WebElement.Handle.style.removeProperty("fontFamily");
        else
          WebElement.Handle.style.fontFamily = Font.Name;

        if (Font.Size == null)
          WebElement.Handle.style.removeProperty("fontSize");
        else
          WebElement.Handle.style.fontSize = VerticalPtToPx(Font.Size.Value) + "px";

        if (Font.Colour == null)
          WebElement.Handle.style.removeProperty("color");
        else
          WebElement.Handle.style.color = Font.Colour.WebColour();
      }
    }
    private void TranslateLayout(Inv.Panel InvPanel, WebElement WebLayout, WebElement WebPanel)
    {
      if (InvPanel.Opacity.Render())
        WebPanel.Handle.style.opacity = InvPanel.Opacity.Get().ToString();

      TranslateBackground(InvPanel.Background, WebPanel);

      var InvBorder = InvPanel.Border;
      if (InvBorder.Render())
      {
        if (InvBorder.Colour != null)
          WebPanel.Handle.style.borderColor = InvBorder.Colour.WebColour();
        else
          WebPanel.Handle.style.removeProperty("borderColor");

        var InvBorderThickness = InvBorder.Thickness;
        if (InvBorderThickness.Render())
        {
          if (InvBorderThickness.IsSet)
          {
            WebPanel.Handle.style.borderStyle = "solid";
            WebPanel.Handle.style.borderWidth = string.Format("{0}px {1}px {2}px {3}px", VerticalPtToPx(InvBorderThickness.Top), HorizontalPtToPx(InvBorderThickness.Right), VerticalPtToPx(InvBorderThickness.Bottom), HorizontalPtToPx(InvBorderThickness.Left));
          }
          else
          {
            WebPanel.Handle.style.removeProperty("borderStyle");
            WebPanel.Handle.style.removeProperty("borderWidth");
          }
        }
      }

      var InvCornerRadius = InvPanel.CornerRadius;
      if (InvCornerRadius.Render())
      {
        if (InvCornerRadius.IsSet)
          WebPanel.Handle.style.borderRadius = string.Format("{0}px {1}px {2}px {3}px", VerticalPtToPx(InvCornerRadius.TopLeft), HorizontalPtToPx(InvCornerRadius.TopRight), VerticalPtToPx(InvCornerRadius.BottomRight), HorizontalPtToPx(InvCornerRadius.BottomLeft));
        else
          WebPanel.Handle.style.removeProperty("borderRadius");
      }

      var InvPadding = InvPanel.Padding;
      if (InvPadding.Render())
      {
        if (InvPadding.IsSet)
        {
          var PaddingTop = VerticalPtToPx(InvPadding.Top);
          var PaddingRight = HorizontalPtToPx(InvPadding.Right);
          var PaddingBottom = VerticalPtToPx(InvPadding.Bottom);
          var PaddingLeft = HorizontalPtToPx(InvPadding.Left);

          WebPanel.Handle.style.padding = string.Format("{0}px {1}px {2}px {3}px", PaddingTop, PaddingRight, PaddingBottom, PaddingLeft);
        }
        else
        {
          WebPanel.Handle.style.removeProperty("padding");
        }
      }

      var NeedsArranging = false;

      var InvSize = InvPanel.Size;
      if (InvSize.Render())
        NeedsArranging = true;

      var InvAlignment = InvPanel.Alignment;
      if (InvAlignment.Render())
        NeedsArranging = true;

      var InvMargin = InvPanel.Margin;
      if (InvMargin.Render())
        NeedsArranging = true;

      var InvVisibility = InvPanel.Visibility;
      if (InvVisibility.Render())
        NeedsArranging = true;

      if (NeedsArranging)
      {
        var IsVisible = InvVisibility.Get();
        var PxWidth = InvSize.Width == null ? (int?)null : HorizontalPtToPx(InvSize.Width.Value);
        var PxHeight = InvSize.Height == null ? (int?)null : HorizontalPtToPx(InvSize.Height.Value);

        var TranslatePlacement = InvAlignment.Get();
        if (TranslatePlacement == Placement.Stretch)
        {
          if (PxWidth != null && PxHeight != null)
            TranslatePlacement = Placement.Center;
          else if (PxWidth != null)
            TranslatePlacement = Placement.StretchCenter;
          else if (PxHeight != null)
            TranslatePlacement = Placement.CenterStretch;
        }
        else if (TranslatePlacement == Placement.StretchLeft)
        {
          if (PxHeight != null)
            TranslatePlacement = Placement.CenterLeft;
        }
        else if (TranslatePlacement == Placement.StretchRight)
        {
          if (PxHeight != null)
            TranslatePlacement = Placement.CenterRight;
        }
        else if (TranslatePlacement == Placement.StretchCenter)
        {
          if (PxHeight != null)
            TranslatePlacement = Placement.Center;
        }
        else if (TranslatePlacement == Placement.TopStretch)
        {
          if (PxWidth != null)
            TranslatePlacement = Placement.TopCenter;
        }
        else if (TranslatePlacement == Placement.CenterStretch)
        {
          if (PxWidth != null)
            TranslatePlacement = Placement.Center;
        }
        else if (TranslatePlacement == Placement.BottomStretch)
        {
          if (PxWidth != null)
            TranslatePlacement = Placement.BottomCenter;
        }

        var AlignHorizontal = TranslatePlacement.IsHorizontalStretch();
        var AlignVertical = TranslatePlacement.IsVerticalStretch();

        WebLayout.Class = TranslatePlacement.ToString();
        if (IsVisible)
          WebLayout.Handle.style.removeProperty("visibility");
        else
          WebLayout.Handle.style.visibility = "collapse";

        int MarginHeight;
        int MarginWidth;

        if (InvMargin.IsSet && IsVisible)
        {
          var MarginTop = VerticalPtToPx(InvMargin.Top);
          var MarginRight = HorizontalPtToPx(InvMargin.Right);
          var MarginBottom = VerticalPtToPx(InvMargin.Bottom);
          var MarginLeft = HorizontalPtToPx(InvMargin.Left);

          WebPanel.Handle.style.margin = string.Format("{0}px {1}px {2}px {3}px", MarginTop, MarginRight, MarginBottom, MarginLeft);

          MarginWidth = MarginLeft + MarginRight;
          MarginHeight = MarginTop + MarginBottom;
        }
        else
        {
          WebPanel.Handle.style.removeProperty("margin");

          MarginWidth = 0;
          MarginHeight = 0;
        }
        
        // NOTE: Chrome doesn't implement visibility collapse as expected so we need the 0px workaround.
        WebLayout.Handle.style.width = IsVisible ? "100%" : "0px"; 
        WebLayout.Handle.style.height = IsVisible ? "100%" : "0px";

        if (PxWidth != null)
          WebPanel.Handle.style.width = PxWidth.Value + "px";
        else if (AlignVertical)
          WebPanel.Handle.style.width = MarginWidth == 0 ? "100%" : "calc(100% - " + MarginWidth + "px)";
        else
          WebPanel.Handle.style.removeProperty("width");

        if (PxHeight != null)
          WebPanel.Handle.style.height = PxHeight.Value + "px";
        else if (AlignVertical)
          WebPanel.Handle.style.height = MarginHeight == 0 ? "100%" : "calc(100% - " + MarginHeight + "px)";
        else
          WebPanel.Handle.style.removeProperty("height");

        if (AlignHorizontal)
          WebPanel.Handle.style.flexGrow = PxWidth == null ? 1 : 0;
        else if (AlignVertical)
          WebPanel.Handle.style.flexGrow = PxHeight == null ? 1 : 0;
        else
          WebPanel.Handle.style.flexGrow = 0;
      }
    }
    private void TranslateBackground(Background InvBackground, WebElement WebElement)
    {
      if (InvBackground.Render())
        TranslateBackgroundColour(InvBackground.Colour, WebElement);
    }
    private void TranslateBackgroundColour(Inv.Colour? InvColour, WebElement WebElement)
    {
      if (InvColour != null)
        WebElement.Handle.style.backgroundColor = InvColour.Value.WebColour();
      else
        WebElement.Handle.style.removeProperty("backgroundColor");
    }
    private string TranslateImageSource(Inv.Image? InvImage)
    {
      // TODO: is this worth caching in a dictionary?

      // TODO: assess mime type from header bytes?

      // image source is a base64 string.
      if (InvImage != null)
        return "";
      else
        return "data:image/png;base64," + Convert.ToBase64String(InvImage.Value.GetBuffer());
    }
    private int HorizontalPtToPx(int Points)
    {
      // TODO: convert Inv Points to Html Px?
      return Points;
    }
    private int VerticalPtToPx(int Points)
    {
      // TODO: convert Inv Points to Html Px?
      return Points;
    }
    private int HorizontalPxToPt(int Pixels)
    {
      // TODO: convert Html Px to Inv Points?
      return Pixels;
    }
    private int VerticalPxToPt(int Pixels)
    {
      // TODO: convert Html Px to Inv Points?
      return Pixels;
    }
    private WebSurface AccessSurface(Inv.Surface InvSurface, Func<Inv.Surface, WebSurface> BuildFunction)
    {
      if (InvSurface.Node == null)
      {
        var Result = BuildFunction(InvSurface);

        InvSurface.Node = Result;

        return Result;
      }
      else
      {
        return (WebSurface)InvSurface.Node;
      }
    }
    private WebTimer AccessTimer(Inv.Timer InvTimer, Func<Inv.Timer, WebTimer> BuildFunction)
    {
      if (InvTimer.Node == null)
      {
        var Result = BuildFunction(InvTimer);

        InvTimer.Node = Result;

        return Result;
      }
      else
      {
        return (WebTimer)InvTimer.Node;
      }
    }
    private WebElement TranslatePanel(Inv.Panel InvPanel)
    {
      if (InvPanel == null)
        return null;
      else
        return RouteDictionary[InvPanel.GetType()](InvPanel).LayoutElement;
    }
    private WebNode<TElement> AccessPanel<TElement, TPanel>(TPanel InvPanel, Func<TPanel, TElement> BuildFunction)
      where TElement : WebElement
      where TPanel : Inv.Panel
    {
      if (InvPanel.Node == null)
      {
        var Result = new WebNode<TElement>();
        
        Result.LayoutElement = WebDocument.NewDiv();

        Result.PanelElement = BuildFunction(InvPanel);

        Result.LayoutElement.AddElement(Result.PanelElement);

        InvPanel.Node = Result;

        return Result;
      }
      else
      {
        return (WebNode<TElement>)InvPanel.Node;
      }
    }
    private void RenderPanel(Inv.Panel InvPanel, WebNode WebNode, Action Action)
    {
      if (InvPanel.Render())
      {
        Action();
      }
    }

    private Application InvApplication;
    private Dictionary<Type, Func<Panel, WebNode>> RouteDictionary;
    private WebHtml WebHtml;
    private WebBody WebBody;
    private WebDiv WebMaster;
    private WebHead WebHead;
    private WebCanvas ImageBuffer;
    private Dictionary<Inv.Image, WebImage> ImageDictionary;
    private WebTimer ProcessTimer;
    private Web2DContext ImageContext;
  }

  internal abstract class WebNode
  {
    public WebElement LayoutElement { get; set; }
    public WebElement PanelElement { get; set; }
  }

  internal sealed class WebNode<T> : WebNode
    where T : WebElement
  {
    public new T PanelElement
    {
      get { return (T)base.PanelElement; }
      set { base.PanelElement = value; }
    }
  }

  internal sealed class WebSurface
  {
    public WebSurface(WebDocument WebDocument)
    {
      this.Base = WebDocument.NewDiv();
      Base.Handle.style.width = "100%";
      Base.Handle.style.height = "100%";
    }

    public void SetContent(WebElement WebElement)
    {
      if (Content != WebElement)
      {
        if (Content != null)
          Base.RemoveElement(Content);

        this.Content = WebElement;

        if (Content != null)
          Base.AddElement(Content);
      }
    }

    public static implicit operator WebDiv(WebSurface Surface)
    {
      return Surface != null ? Surface.Base : null;
    }

    private WebDiv Base;
    private WebElement Content;
  }
}
