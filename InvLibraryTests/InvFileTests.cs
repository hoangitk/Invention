﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InvFileTests
{
  [TestClass]
  public class FileSystemTransaction
  {
    [TestMethod]
    public void CommitAndRollback()
    {
      var TempDirectoryPath = Path.Combine(Path.GetTempPath(), "InvCaseBasic");
      var TempFilePath = Path.Combine(TempDirectoryPath, "TempFile1.txt");

      if (Directory.Exists(TempDirectoryPath))
        Directory.Delete(TempDirectoryPath, true);

      using (var Transaction = Inv.FileSystemTransaction.BeginKTM("ForgeTester.InvCase.BasicKTM-1"))
      {
        Transaction.CreateDirectory(TempDirectoryPath);

        using (var FileStream = Transaction.OpenFile(TempFilePath, FileMode.CreateNew, FileAccess.Write, FileShare.Read))
        using (var StreamWriter = new StreamWriter(FileStream))
          StreamWriter.WriteLine("Transaction 1 commit");

        Transaction.Commit();
      }

      using (var Transaction = Inv.FileSystemTransaction.BeginKTM("ForgeTester.InvCase.BasicKTM-2"))
      {
        using (var FileStream = Transaction.OpenFile(TempFilePath, FileMode.Append, FileAccess.Write, FileShare.Read))
        using (var StreamWriter = new StreamWriter(FileStream))
          StreamWriter.WriteLine("Transaction 2 commit");

        Transaction.Rollback();
      }

      if (File.ReadAllText(TempFilePath) != "Transaction 1 commit\r\n")
        Assert.Fail("Transaction failed");
    }
  }
}