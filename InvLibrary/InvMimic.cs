﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  public abstract class Mimic<T>
    where T : class
  {
    protected T Base 
    {
      get
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(BaseField != null, "Base has not been set.");

        return BaseField;
      }
      set
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(BaseField == null, "Base must only be set once.");

        this.BaseField = value;
      }
    }

    public static implicit operator T(Mimic<T> Self)
    {
      return Self?.Base;
    }

    private T BaseField;
  }
}
