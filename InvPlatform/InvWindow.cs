﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// See <see cref="Application.Window"/>.
  /// </summary>
  public sealed class Window
  {
    internal Window(Application Application)
    {
      this.Application = Application;
      this.ActiveTimerSet = new HashSet<WindowTimer>();
      this.Background = new Background(this);
      this.DisplayRate = new WindowDisplayRate(this);
      this.Accessibility = new WindowAccessibility(this);
      this.DefaultTransitionDuration = TimeSpan.FromMilliseconds(300);
      this.KeyModifier = new KeyModifier();
      this.ChangedControlList = new DistinctList<Inv.Control>();
      //this.IsChanged = true; // NOTE: this assumes all platforms give correct defaults.
    }

    /// <summary>
    /// The owning portable application.
    /// </summary>
    public Inv.Application Application { get; }
    /// <summary>
    /// The background of the window.
    /// </summary>
    public Inv.Background Background { get; }
    /// <summary>
    /// Default duration used in transitions on surfaces and frames.
    /// </summary>
    public TimeSpan DefaultTransitionDuration { get; set; }
    /// <summary>
    /// Current state of the key modifiers shift, control and alt.
    /// </summary>
    public Inv.KeyModifier KeyModifier { get; }
    /// <summary>
    /// The display rate of the app in frames per second (FPS).
    /// </summary>
    public Inv.WindowDisplayRate DisplayRate { get; }
    /// <summary>
    /// Accessibility controls for users with special needs.
    /// </summary>
    public Inv.WindowAccessibility Accessibility { get; }
    /// <summary>
    /// The surface active on the screen.
    /// </summary>
    public Inv.Surface ActiveSurface { get; private set; }
    /// <summary>
    /// The current width of the window/screen in logical points.
    /// This will change when the window is resized or the device is rotated.
    /// </summary>
    public int Width { get; internal set; }
    /// <summary>
    /// The current height of the window/screen in logical points.
    /// This will change when the window is resized or the device is rotated.
    /// </summary>
    public int Height { get; internal set; }
    /// <summary>
    /// Input is currently prevented to the all the panels on the window.
    /// </summary>
    public bool InputPrevented { get; private set; }
    /// <summary>
    /// This event is fired when a key modifier is changed (shift, control or alt).
    /// </summary>
    public event Action KeyModifierEvent;

    /// <summary>
    /// Ask if the surface is the active surface.
    /// </summary>
    /// <param name="Surface"></param>
    /// <returns></returns>
    public bool IsActiveSurface(Inv.Surface Surface)
    {
      RequireThreadAffinity();

      return Surface != null && ActiveSurface == Surface;
    }
    /// <summary>
    /// Create a new surface.
    /// </summary>
    /// <returns></returns>
    public Inv.Surface NewSurface()
    {
      RequireThreadAffinity();

      return new Surface(this);
    }
    /// <summary>
    /// Transition to another surface.
    /// </summary>
    /// <param name="Surface"></param>
    /// <returns></returns>
    public Inv.Transition Transition(Inv.Surface Surface)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Surface.Window == this, "Surface must belong to this window.");

      if (ActiveTransition == null || ActiveSurface != Surface)
      {
        if (ActiveSurface != null)
          ActiveSurface.LeaveInvoke();

        this.FromSurface = ActiveSurface;
        this.ActiveTransition = new Transition(this);
        this.ActiveSurface = Surface;

        if (ActiveSurface != null)
          ActiveSurface.EnterInvoke();
      }

      return ActiveTransition;
    }
    /// <summary>
    /// Create a timer on the main thread.
    /// </summary>
    /// <returns></returns>
    public Inv.WindowTimer NewTimer()
    {
      RequireThreadAffinity();

      return new WindowTimer(this);
    }
    /// <summary>
    /// Create a timer on the main thread.
    /// </summary>
    /// <returns></returns>
    public Inv.WindowThrottle NewThrottle()
    {
      RequireThreadAffinity();

      return new WindowThrottle(this);
    }
    /// <summary>
    /// Create a new task to run on a background thread.
    /// </summary>
    /// <param name="Action"></param>
    /// <returns></returns>
    public Inv.WindowTask NewTask(Action<WindowThread> Action)
    {
      RequireThreadAffinity();

      return new WindowTask(this, Action);
    }
    /// <summary>
    /// Create and run a new task to run on a background thread.
    /// </summary>
    /// <param name="Action"></param>
    /// <returns></returns>
    public Inv.WindowTask RunTask(Action<WindowThread> Action)
    {
      RequireThreadAffinity();

      var Result = NewTask(Action);

      Result.Run();

      return Result;
    }
    /// <summary>
    /// Launch a file in the default application.
    /// </summary>
    /// <param name="File"></param>
    public void Browse(Inv.File File)
    {
      Application.Platform.WindowBrowse(File);
    }
    /// <summary>
    /// Share or export to another app.
    /// </summary>
    /// <param name="File"></param>
    public void Share(Inv.File File)
    {
      Application.Platform.WindowShare(File);
    }
    /// <summary>
    /// Post an action delegate to be executed at the end of the queue in the main thread.
    /// </summary>
    /// <param name="Action"></param>
    public void Post(Action Action)
    {
      Application.Platform.WindowPost(Action);
    }
    /// <summary>
    /// Post the action delegate in the main thread and wait until it has been executed.
    /// If you are already in the main thread, the action delegate will simply be invoked.
    /// </summary>
    /// <param name="Action"></param>
    public void Call(Action Action)
    {
      Application.Platform.WindowCall(Action);
    }
    /// <summary>
    /// Sleep the main thread for a duration.
    /// </summary>
    /// <param name="Duration"></param>
    public void Sleep(TimeSpan Duration)
    {
      using (var WaitHandle = new ManualResetSignal(false, "Window-Sleep"))
        WaitHandle.WaitOne(Duration);
    }
    /// <summary>
    /// Set the logical focus to a specified panel.
    /// </summary>
    /// <param name="Panel"></param>
    public void SetFocus(Inv.Panel Panel)
    {
      this.Focus = Panel?.Control;
    }
    /// <summary>
    /// Disable all user interaction (touch, mouse, keyboard, etc) with the panels on this surface.
    /// </summary>
    public void PreventInput()
    {
      if (!InputPrevented)
      {
        this.InputPrevented = true;
        Change();
      }
    }
    /// <summary>
    /// Re-enable all user interaction (touch, mouse, keyboard, etc) with the panels on this surface.
    /// </summary>
    public void AllowInput()
    {
      if (InputPrevented)
      {
        this.InputPrevented = false;
        Change();
      }
    }

    internal Inv.Control Focus { get; set; }
    internal Inv.Transition ActiveTransition { get; set; }
    internal Inv.Surface FromSurface { get; set; }
    internal HashSet<Inv.WindowTimer> ActiveTimerSet { get;}
    /// <summary>
    /// Base type of a native panel in the executing platform.
    /// </summary>
    internal Type NativePanelType { get; set; }
    internal event Action ProcessEvent;

    internal bool Render()
    {
      RequireThreadAffinity();

      if (IsChanged)
      {
        this.IsChanged = false;
        return true;
      }

      return false;
    }
    internal void Change()
    {
      RequireThreadAffinity();

      this.IsChanged = true;
    }
    internal void ProcessInvoke()
    {
      if (ProcessEvent != null)
        ProcessEvent();
    }
    internal void CheckModifier(KeyModifier UpdateModifier)
    {
      if (!KeyModifier.IsEqual(UpdateModifier))
      {
        KeyModifier.SetFlags(UpdateModifier.GetFlags());

        if (KeyModifierEvent != null)
          KeyModifierEvent();
      }
    }
    internal void RequireThreadAffinity()
    {
      if (Inv.Assert.IsEnabled)
        Application.RequireThreadAffinity();
    }
    internal void ProcessChanges(Action<Inv.Panel> Action)
    {
      RequireThreadAffinity();

      var ChangedPanelArray = ChangedControlList.ToArray();
      ChangedControlList.Clear();

      foreach (var ChangedPanel in ChangedPanelArray)
      {
        // some panels will exercise sub-panels which means we don't need to check them again.
        if (ChangedPanel.IsChanged)
          Action(ChangedPanel);
      }
    }
    internal void ChangePanel(Inv.Control Control)
    {
      if (Inv.Assert.IsEnabled)
      {
        RequireThreadAffinity();
        Inv.Assert.CheckNotNull(Control, nameof(Control));
      }

      if (Control != null)
        ChangedControlList.Add(Control);
    }
    internal void RenderPanel(Inv.Control Control)
    {
      if (Inv.Assert.IsEnabled)
      {
        RequireThreadAffinity();
        Inv.Assert.CheckNotNull(Control, nameof(Control));
      }

      ChangedControlList.Remove(Control);
    }
    internal void StartAnimation(Inv.Animation Animation)
    {
      RequireThreadAffinity();

      Application.Platform.WindowStartAnimation(Animation);
    }
    internal void StopAnimation(Inv.Animation Animation)
    {
      RequireThreadAffinity();

      Application.Platform.WindowStopAnimation(Animation);
    }

    private bool IsChanged;
    private readonly Inv.DistinctList<Inv.Control> ChangedControlList;
  }

  /// <summary>
  /// See <see cref="Window.Accessibility"/>
  /// </summary>
  public sealed class WindowAccessibility
  {
    internal WindowAccessibility(Window Window)
    {
      this.Window = Window;
      this.AnnounceList = new List<string>();
    }

    /// <summary>
    /// Speak the provided text.
    /// </summary>
    /// <param name="Text"></param>
    public void Announce(string Text)
    {
      Window.RequireThreadAffinity();

      AnnounceList.Add(Text);
    }
    //public Inv.Panel GetFocus()
    //{
    //  Window.RequireThreadAffinity();
    //
    //  return FocusPanel;
    //}
    /// <summary>
    /// Move the accessibility focus to a specific panel.
    /// </summary>
    /// <param name="Panel"></param>
    public void SetFocus(Inv.Panel Panel)
    {
      Window.RequireThreadAffinity();

      this.FocusPanel = Panel;
    }

    internal List<string> AnnounceList { get; }
    internal Inv.Panel FocusPanel { get; set; }

    private readonly Window Window;
  }

  /// <summary>
  /// See <see cref="Window.DisplayRate"/>
  /// </summary>
  public sealed class WindowDisplayRate
  {
    internal WindowDisplayRate(Window Window)
    {
      this.Window = Window;
    }

    /// <summary>
    /// Number of frames per second are being rendered.
    /// </summary>
    public int PerSecond
    {
      get { return LastCount; }
    }

    internal void Calculate()
    {
      //Window.RequireThreadAffinity(); // NOTE: we can assume thread affinity in this internal method call.

      var CurrentTick = System.Environment.TickCount;
      var Difference = CurrentTick - LastTick;

      if (Difference < 0 || Difference >= 1000)
      {
        this.LastCount = CurrentCount;
        this.CurrentCount = 0;
        this.LastTick = System.Environment.TickCount;
      }

      CurrentCount++;
    }

    private readonly Window Window;
    private int LastTick;
    private int LastCount;
    private int CurrentCount;
  }

  /// <summary>
  /// Task that was launched from the window but runs in a background thread.
  /// </summary>
  public sealed class WindowTask
  {
    internal WindowTask(Window Window, Action<WindowThread> Action)
    {
      this.Window = Window;
      this.Base = new System.Threading.Tasks.Task(() =>
      {
        try
        {
          this.SW = Stopwatch.StartNew();
          try
          {
            Action(new WindowThread(this));
          }
          finally
          {
            SW.Stop();
          }
        }
        catch (Exception Exception)
        {
          Window.Application.HandleExceptionInvoke(Exception);

          // TODO: keep this exception to throw on Wait() ?
        }
      });
    }

    /// <summary>
    /// New window task.
    /// </summary>
    /// <param name="Action"></param>
    /// <returns></returns>
    public static WindowTask New(Action<WindowThread> Action) => Inv.Application.Access().Window.NewTask(Action);
    /// <summary>
    /// Run in a window task.
    /// </summary>
    /// <param name="Action"></param>
    /// <returns></returns>
    public static WindowTask Run(Action<WindowThread> Action) => Inv.Application.Access().Window.RunTask(Action);

    /// <summary>
    /// The elapsed time of the task since it was first run.
    /// </summary>
    public TimeSpan Elapsed
    {
      get { return SW.Elapsed; }
    }
    /// <summary>
    /// Ask if the task has completed.
    /// </summary>
    public bool IsCompleted
    {
      get { return Base.IsCompleted; }
    }

    /// <summary>
    /// Run the task in a background thread.
    /// </summary>
    public void Run()
    {
      Window.RequireThreadAffinity();

      Base.Start();
    }
    /// <summary>
    /// Blocking wait for the task to complete.
    /// </summary>
    public void Wait()
    {
      //Window.RequireThreadAffinity(); // waiting for a task can be done from any thread.

      if (IsRunning())
        Base.Wait();
    }
    /// <summary>
    /// Blocking wait the specified TimeSpan for the task to complete.
    /// </summary>
    /// <param name="TimeSpan"></param>
    public bool Wait(TimeSpan TimeSpan)
    {
      if (IsRunning())
        return Base.Wait(TimeSpan);

      return true;
    }

    internal Window Window { get; private set; }
    internal Stopwatch SW { get; private set; }

    private bool IsRunning()
    {
      return Base.Status == TaskStatus.WaitingToRun || Base.Status == TaskStatus.Running;
    }

    private readonly Task Base;
  }

  /// <summary>
  /// The background thread of a task.
  /// </summary>
  public sealed class WindowThread
  {
    internal WindowThread(WindowTask Task)
    {
      this.Task = Task;
    }

    /// <summary>
    /// Sleep the thread so the task will run to the minimum duration.
    /// If the task has exceeded the minimum duration then no sleep is performed.
    /// </summary>
    /// <param name="MinimumDuration"></param>
    public void Yield(TimeSpan MinimumDuration)
    {
      // TODO: check the task's thread affinity.

      var Result = MinimumDuration - Task.SW.Elapsed;

      if (Result > TimeSpan.Zero)
        Sleep(Result);
    }
    /// <summary>
    /// Sleep the thread for the duration.
    /// </summary>
    /// <param name="Duration"></param>
    public void Sleep(TimeSpan Duration)
    {
      using (var WaitHandle = new ManualResetSignal(false, "WindowThread-Sleep"))
        WaitHandle.WaitOne(Duration);
      //using (var WaitHandle = new System.Threading.ManualResetEventSlim(false))
      //  WaitHandle.Wait(Duration);
    }
    /// <summary>
    /// Post a delegate back to the main window thread.
    /// </summary>
    /// <param name="Action"></param>
    public void Post(Action Action)
    {
      Task.Window.Application.Platform.WindowPost(Action);
    }

    private readonly WindowTask Task;
  }

  /// <summary>
  /// Timer that fires on the main thread.
  /// </summary>
  public sealed class WindowTimer
  {
    internal WindowTimer(Window Window)
    {
      this.Window = Window;
    }

    /// <summary>
    /// New window timer.
    /// </summary>
    /// <returns></returns>
    public static WindowTimer New() => Inv.Application.Access().Window.NewTimer();

    /// <summary>
    /// This event is fired each interval as specified by the <see cref="IntervalTime"/>.
    /// </summary>
    public event Action IntervalEvent;
    /// <summary>
    /// The time to wait before firing the <see cref="IntervalEvent"/>.
    /// </summary>
    public TimeSpan IntervalTime { get; set; }
    /// <summary>
    /// Ask if the timer is started right now.
    /// </summary>
    public bool IsEnabled { get; private set; }

    /// <summary>
    /// Start the timer.
    /// </summary>
    public void Start()
    {
      CheckThreadAffinity();

      if (!IsEnabled)
      {
        this.IsEnabled = true;
        Window.ActiveTimerSet.Add(this);
      }
    }
    /// <summary>
    /// Stop the timer.
    /// </summary>
    public void Stop()
    {
      CheckThreadAffinity();

      if (IsEnabled)
      {
        this.IsEnabled = false;
        // NOTE: the platform engine must remove the timer from the active set once it has been stopped.
      }
    }
    /// <summary>
    /// Restart the timer and wait for a full <see cref="IntervalTime"/> before firing the <see cref="IntervalEvent"/>.
    /// </summary>
    public void Restart()
    {
      CheckThreadAffinity();

      if (!IsEnabled)
        Start();
      else
        this.IsRestarting = true;
    }

    internal object Node { get; set; }
    internal bool IsRestarting { get; set; }

    internal void IntervalInvoke()
    {
      var IntervalDelegate = IntervalEvent;

      if (IntervalDelegate != null)
        IntervalDelegate();
    }

    private void CheckThreadAffinity()
    {
      Window.RequireThreadAffinity();
    }

    private readonly Window Window;
  }

  /// <summary>
  /// Throttles multiple event invocations within a threshold into a single call.
  /// </summary>
  public sealed class WindowThrottle
  {
    internal WindowThrottle(Window Window)
    {
      this.FireTimer = Window.NewTimer();
      FireTimer.IntervalEvent += () =>
      {
        Action FireAction;

        FireTimer.Stop();

        if (IsFiring)
        {
          IsFiring = false;
          FireAction = FireEvent;

          FireTimer.IntervalTime = ThresholdTime;
          FireTimer.Start();
        }
        else
        {
          FireAction = null;
        }

        if (FireAction != null)
          FireAction();
      };
    }

    /// <summary>
    /// New window throttle.
    /// </summary>
    /// <returns></returns>
    public static WindowThrottle New() => Inv.Application.Access().Window.NewThrottle();

    /// <summary>
    /// Delay the threshold time before firing the first time in a throttled series.
    /// </summary>
    public bool InitialDelay { get; set; }
    /// <summary>
    /// The time to wait between firing in a throttled series.
    /// </summary>
    public TimeSpan ThresholdTime { get; set; }
    /// <summary>
    /// The event is fired in a throttled manner.
    /// </summary>
    public event System.Action FireEvent;

    /// <summary>
    /// Signal the throttle to fire.
    /// </summary>
    public void Fire()
    {
      lock (FireTimer)
      {
        if (!IsFiring)
        {
          IsFiring = true;

          if (!FireTimer.IsEnabled)
          {
            FireTimer.IntervalTime = InitialDelay ? ThresholdTime : TimeSpan.Zero;
            FireTimer.Start();
          }
        }
      }
    }
    /// <summary>
    /// Suppress any active firing.
    /// </summary>
    public void Suppress()
    {
      IsFiring = false;
      FireTimer.Stop();
    }

    private readonly Inv.WindowTimer FireTimer;
    private bool IsFiring;
  }

  /// <summary>
  /// Transition is used on surfaces and frames to animate the change of content.
  /// </summary>
  public sealed class Transition
  {
    internal Transition(Window Window)
    {
      this.Window = Window;
      this.Type = TransitionType.None;
      this.Duration = Window.DefaultTransitionDuration;
    }

    /// <summary>
    /// Type of animation used on the transition.
    /// </summary>
    public TransitionType Type { get; private set; }
    /// <summary>
    /// Duration of the animation.
    /// </summary>
    public TimeSpan Duration { get; set; }

    /// <summary>
    /// Set the transition by type.
    /// </summary>
    /// <param name="Type"></param>
    public void SetType(TransitionType Type)
    {
      CheckThreadAffinity();

      this.Type = Type;
    }
    /// <summary>
    /// No transition, immediately change the content.
    /// </summary>
    public void None()
    {
      SetType(TransitionType.None);
    }
    /// <summary>
    /// Fade out the old content and then fade in the new content.
    /// </summary>
    public void Fade()
    {
      SetType(TransitionType.Fade);
    }
    /// <summary>
    /// The old content exits to the right and the new content enters from the left.
    /// </summary>
    public void CarouselPrevious()
    {
      SetType(TransitionType.CarouselPrevious);
    }
    /// <summary>
    /// The old content exits to the left and the new content enters from the right.
    /// </summary>
    public void CarouselNext()
    {
      SetType(TransitionType.CarouselNext);
    }
    /// <summary>
    /// The old content exits to the top and the new content enters from the bottom.
    /// </summary>
    public void CarouselAscend()
    {
      SetType(TransitionType.CarouselAscend);
    }
    /// <summary>
    /// The old content exits to the bottom and the new content enters from the top.
    /// </summary>
    public void CarouselDescend()
    {
      SetType(TransitionType.CarouselDescend);
    }

    private void CheckThreadAffinity()
    {
      if (Inv.Assert.IsEnabled)
        Window.RequireThreadAffinity();
    }

    private readonly Window Window;
  }

  /// <summary>
  /// Enumeration of the types of content transitions.
  /// </summary>
  public enum TransitionType
  {
    /// <summary>
    /// no transition
    /// </summary>
    None,
    /// <summary>
    /// fade transition
    /// </summary>
    Fade,
    /// <summary>
    /// carousel back transition
    /// </summary>
    CarouselPrevious,
    /// <summary>
    /// carousel next transition
    /// </summary>
    CarouselNext,
    /// <summary>
    /// carousel ascend transition
    /// </summary>
    CarouselAscend,
    /// <summary>
    /// carousel descend transition
    /// </summary>
    CarouselDescend
  }
}
