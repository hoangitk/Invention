﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// The surface represents one layout perspective of the full real-estate of the device screen.
  /// You can transition between surfaces using the <see cref="Application.Window"/>.
  /// </summary>
  public sealed class Surface
  {
    internal Surface(Inv.Window Window)
    {
      this.Window = Window;
      this.Background = new Background(this);
      this.IsChanged = true;
    }

    /// <summary>
    /// The owning <see cref="Window"/> for this surface.
    /// </summary>
    public Inv.Window Window { get; }
    /// <summary>
    /// The surface has a background colour.
    /// </summary>
    public Inv.Background Background { get; }
    /// <summary>
    /// Primary content of the surface.
    /// </summary>
    public Inv.Panel Content
    {
      get { return ContentField; }
      set
      {
        if (ContentField != value)
        {
          if (ContentField != null)
            Detach(null, ContentField);

          this.ContentField = value;

          if (ContentField != null)
            Attach(null, ContentField);

          Change();
        }
      }
    }
    /// <summary>
    /// This event is fired when the user gestures to go back such as swiping on the left edge of the device or clicking the back button on a mouse.
    /// </summary>
    public event Action GestureBackwardEvent
    {
      add
      {
        RequireThreadAffinity();
        GestureBackwardDelegate += value;
        Change();
      }
      remove
      {
        RequireThreadAffinity();
        GestureBackwardDelegate -= value;
        Change();
      }
    }
    /// <summary>
    /// Ask if the <see cref="GestureBackwardEvent"/> is handled.
    /// </summary>
    public bool HasGestureBackward
    {
      get
      {
        RequireThreadAffinity();

        return GestureBackwardDelegate != null;
      }
    }
    /// <summary>
    /// This event is fired when the user gestures to go forward such as swiping on the right edge of the device or clicking the forward button on a mouse.
    /// </summary>
    public event Action GestureForwardEvent
    {
      add
      {
        GestureForwardDelegate += value;
        Change();
      }
      remove
      {
        GestureForwardDelegate -= value;
        Change();
      }
    }
    /// <summary>
    /// Ask if the <see cref="GestureForwardEvent"/> is handled.
    /// </summary>
    public bool HasGestureForward
    {
      get
      {
        RequireThreadAffinity();

        return GestureForwardDelegate != null;
      }
    }

    /// <summary>
    /// This event is fired when a key is pressed on a non-virtual keyboard including bluetooth keyboards attached to devices.
    /// </summary>
    public event Action<Inv.Keystroke> KeystrokeEvent;
    /// <summary>
    ///  The arrange event is fired when the form-factor of the surface changes.
    ///  E.g. rotation of a device of resizing of a window such as split-screen in iOS.
    /// </summary>
    public event Action ArrangeEvent;
    /// <summary>
    /// The compose event is fired once per display frame.
    /// This means the event is called up to 60 times per second (60 FPS) or every ~16 milliseconds.
    /// This can be used for custom animations and game-loops.
    /// </summary>
    public event Action ComposeEvent;
    /// <summary>
    /// The entry event is fired when you transition to this surface.
    /// </summary>
    public event Action EnterEvent;
    /// <summary>
    /// The exit event is fired when you transition away from this surface.
    /// </summary>
    public event Action LeaveEvent;

    /// <summary>
    /// Create a new <see cref="Animation" />
    /// </summary>
    /// <returns></returns>
    public Inv.Animation NewAnimation()
    {
      RequireThreadAffinity();

      return new Animation(Window);
    }
    /// <summary>
    /// Create a new <see cref="Block" />
    /// </summary>
    /// <returns></returns>
    public Inv.Block NewBlock()
    {
      RequireThreadAffinity();

      return new Block(this);
    }
    /// <summary>
    /// Create a new <see cref="Board" />
    /// </summary>
    /// <returns></returns>
    public Inv.Board NewBoard()
    {
      RequireThreadAffinity();

      return new Board(this);
    }
    /// <summary>
    /// Create a new <see cref="Button" /> with the provided button style.
    /// </summary>
    /// <returns></returns>
    public Inv.Button NewButton(Inv.ButtonStyle Style)
    {
      RequireThreadAffinity();

      return new Inv.Button(this, Style);
    }
    /// <summary>
    /// Create a new Stark <see cref="Button" />
    /// </summary>
    /// <returns></returns>
    public Inv.Button NewStarkButton()
    {
      RequireThreadAffinity();

      return new Button(this, ButtonStyle.Stark);
    }
    /// <summary>
    /// Create a new Flat <see cref="Button" />
    /// </summary>
    /// <returns></returns>
    public Inv.Button NewFlatButton()
    {
      RequireThreadAffinity();

      return new Button(this, ButtonStyle.Flat);
    }
    /// <summary>
    /// Create a new <see cref="Canvas" />
    /// </summary>
    /// <returns></returns>
    public Inv.Canvas NewCanvas()
    {
      RequireThreadAffinity();

      return new Canvas(this);
    }
    /// <summary>
    /// Create a new <see cref="Dock" /> with the provided <paramref name="Orientation"/>.
    /// </summary>
    /// <returns></returns>
    public Inv.Dock NewDock(Inv.Orientation Orientation)
    {
      RequireThreadAffinity();

      return new Dock(this, Orientation);
    }
    /// <summary>
    /// Create a new Vertical <see cref="Dock" />
    /// </summary>
    /// <returns></returns>
    public Inv.Dock NewVerticalDock()
    {
      return NewDock(Orientation.Vertical);
    }
    /// <summary>
    /// Create a new Horizontal <see cref="Dock" />
    /// </summary>
    /// <returns></returns>
    public Inv.Dock NewHorizontalDock()
    {
      return NewDock(Orientation.Horizontal);
    }
    /// <summary>
    /// Create a new <see cref="Edit" /> with the provided <paramref name="Input"/> mode.
    /// </summary>
    /// <returns></returns>
    public Inv.Edit NewEdit(Inv.EditInput Input)
    {
      RequireThreadAffinity();

      return new Edit(this, Input);
    }
    /// <summary>
    /// Create a new Text <see cref="Edit" />
    /// </summary>
    /// <returns></returns>
    public Inv.Edit NewTextEdit()
    {
      return NewEdit(EditInput.Text);
    }
    /// <summary>
    /// Create a new Number <see cref="Edit" />
    /// </summary>
    /// <returns></returns>
    public Inv.Edit NewNumberEdit()
    {
      return NewEdit(EditInput.Number);
    }
    /// <summary>
    /// Create a new Integer <see cref="Edit" />
    /// </summary>
    /// <returns></returns>
    public Inv.Edit NewIntegerEdit()
    {
      return NewEdit(EditInput.Integer);
    }
    /// <summary>
    /// Create a new Decimal <see cref="Edit" />
    /// </summary>
    /// <returns></returns>
    public Inv.Edit NewDecimalEdit()
    {
      return NewEdit(EditInput.Decimal);
    }
    /// <summary>
    /// Create a new Name <see cref="Edit" />
    /// </summary>
    /// <returns></returns>
    public Inv.Edit NewNameEdit()
    {
      return NewEdit(EditInput.Name);
    }
    /// <summary>
    /// Create a new Email <see cref="Edit" />
    /// </summary>
    /// <returns></returns>
    public Inv.Edit NewEmailEdit()
    {
      return NewEdit(EditInput.Email);
    }
    /// <summary>
    /// Create a new Phone <see cref="Edit" />
    /// </summary>
    /// <returns></returns>
    public Inv.Edit NewPhoneEdit()
    {
      return NewEdit(EditInput.Phone);
    }
    /// <summary>
    /// Create a new Password <see cref="Edit" />
    /// </summary>
    /// <returns></returns>
    public Inv.Edit NewPasswordEdit()
    {
      return NewEdit(EditInput.Password);
    }
    /// <summary>
    /// Create a new Search <see cref="Edit" />
    /// </summary>
    /// <returns></returns>
    public Inv.Edit NewSearchEdit()
    {
      return NewEdit(EditInput.Search);
    }
    /// <summary>
    /// Create a new Uri <see cref="Edit" />
    /// </summary>
    /// <returns></returns>
    public Inv.Edit NewUriEdit()
    {
      return NewEdit(EditInput.Uri);
    }
    /// <summary>
    /// Create a new Username <see cref="Edit" />
    /// </summary>
    /// <returns></returns>
    public Inv.Edit NewUsernameEdit()
    {
      return NewEdit(EditInput.Username);
    }
    /// <summary>
    /// Create a new <see cref="Graphic" />
    /// </summary>
    /// <returns></returns>
    public Inv.Graphic NewGraphic()
    {
      RequireThreadAffinity();

      return new Graphic(this);
    }
    /// <summary>
    /// Create a new <see cref="Label" />
    /// </summary>
    /// <returns></returns>
    public Inv.Label NewLabel()
    {
      RequireThreadAffinity();

      return new Label(this);
    }
    /// <summary>
    /// Create a new <see cref="Memo" />
    /// </summary>
    /// <returns></returns>
    public Inv.Memo NewMemo()
    {
      RequireThreadAffinity();

      return new Memo(this);
    }
    /// <summary>
    /// Create a new <see cref="Overlay" />
    /// </summary>
    /// <returns></returns>
    public Inv.Overlay NewOverlay()
    {
      RequireThreadAffinity();

      return new Overlay(this);
    }
    /// <summary>
    /// Create a new <see cref="Browser" />
    /// </summary>
    /// <returns></returns>
    public Inv.Browser NewBrowser()
    {
      RequireThreadAffinity();

      return new Browser(this);
    }
    /// <summary>
    /// Create a new <see cref="Scroll" /> with the provided <paramref name="Orientation"/>.
    /// </summary>
    /// <returns></returns>
    public Inv.Scroll NewScroll(Orientation Orientation)
    {
      RequireThreadAffinity();

      return new Scroll(this, Orientation);
    }
    /// <summary>
    /// Create a new Vertical <see cref="Scroll" />
    /// </summary>
    /// <returns></returns>
    public Inv.Scroll NewVerticalScroll()
    {
      return NewScroll(Orientation.Vertical);
    }
    /// <summary>
    /// Create a new Horizontal <see cref="Scroll" />
    /// </summary>
    /// <returns></returns>
    public Inv.Scroll NewHorizontalScroll()
    {
      return NewScroll(Orientation.Horizontal);
    }
    /// <summary>
    /// Create a new <see cref="Frame" />
    /// </summary>
    /// <returns></returns>
    public Inv.Frame NewFrame()
    {
      RequireThreadAffinity();

      return new Frame(this);
    }
    /// <summary>
    /// Create a new <see cref="Native" />
    /// </summary>
    /// <returns></returns>
    public Inv.Native NewNative(object Content = null)
    {
      RequireThreadAffinity();

      return new Native(this, Content);
    }
    /// <summary>
    /// Create a new <see cref="Stack" /> with the provided <paramref name="Orientation"/>.
    /// </summary>
    /// <returns></returns>
    public Inv.Stack NewStack(Orientation Orientation)
    {
      RequireThreadAffinity();

      return new Stack(this, Orientation);
    }
    /// <summary>
    /// Create a new Horizontal <see cref="Stack" />
    /// </summary>
    /// <returns></returns>
    public Inv.Stack NewHorizontalStack()
    {
      return NewStack(Orientation.Horizontal);
    }
    /// <summary>
    /// Create a new Vertical <see cref="Stack" />
    /// </summary>
    /// <returns></returns>
    public Inv.Stack NewVerticalStack()
    {
      return NewStack(Orientation.Vertical);
    }
    /// <summary>
    /// Create a new <see cref="Switch" />
    /// </summary>
    /// <returns></returns>
    public Inv.Switch NewSwitch()
    {
      RequireThreadAffinity();

      return new Switch(this);
    }
    /// <summary>
    /// Create a new <see cref="Table" />
    /// </summary>
    /// <returns></returns>
    public Inv.Table NewTable()
    {
      RequireThreadAffinity();

      return new Table(this);
    }
    /// <summary>
    /// Create a new <see cref="Flow" />
    /// </summary>
    /// <returns></returns>
    public Inv.Flow NewFlow()
    {
      RequireThreadAffinity();

      return new Flow(this);
    }
    /// <summary>
    /// Create a new <see cref="Video" />
    /// </summary>
    /// <returns></returns>
    public Inv.Video NewVideo()
    {
      RequireThreadAffinity();

      return new Video(this);
    }
    /// <summary>
    /// Create a new <see cref="Wrap" /> with the provided <paramref name="Orientation"/>.
    /// </summary>
    /// <returns></returns>
    public Inv.Wrap NewWrap(Orientation Orientation)
    {
      RequireThreadAffinity();

      return new Wrap(this, Orientation);
    }
    /// <summary>
    /// Create a new Horizontal <see cref="Wrap" />
    /// </summary>
    /// <returns></returns>
    public Inv.Wrap NewHorizontalWrap()
    {
      return NewWrap(Orientation.Horizontal);
    }
    /// <summary>
    /// Create a new Vertical <see cref="Wrap" />
    /// </summary>
    /// <returns></returns>
    public Inv.Wrap NewVerticalWrap()
    {
      return NewWrap(Orientation.Vertical);
    }
    /// <summary>
    /// Programmatically invoke the gesture backward.
    /// </summary>
    public void GestureBackward()
    {
      RequireThreadAffinity();

      GestureBackwardInvoke();
    }
    /// <summary>
    /// Programmatically invoke the gesture forward.
    /// </summary>
    public void GestureForward()
    {
      RequireThreadAffinity();

      GestureForwardInvoke();
    }
    /// <summary>
    /// Programmatically invoke a keystroke.
    /// </summary>
    public void Keystroke(Inv.Keystroke Keystroke)
    {
      RequireThreadAffinity();

      KeystrokeInvoke(Keystroke);
    }
    /// <summary>
    /// Query the number of panels connected to this surface.
    /// </summary>
    /// <returns></returns>
    public int GetPanelCount()
    {
      RequireThreadAffinity();

      int SumPanelCount(Panel Panel)
      {
        var Result = 1;

        foreach (var Child in Panel.Control.GetChilds())
          Result += SumPanelCount(Child);

        return Result;
      }

      if (Content == null)
        return 0;
      else
        return SumPanelCount(Content);
    }
    /// <summary>
    /// Query the maximum panel depth for panels connected to this surface.
    /// </summary>
    /// <returns></returns>
    public int GetPanelDepth()
    {
      RequireThreadAffinity();

      int MaxVisibleDepth(Panel Panel, int Depth)
      {
        var Result = Depth;

        var Control = Panel.Control;

        if (Control.Visibility.Get())
        {
          foreach (var Child in Control.GetChilds())
          {
            var ChildDepth = MaxVisibleDepth(Child, Depth + 1);

            if (Result < ChildDepth)
              Result = ChildDepth;
          }
        }

        return Result;
      }

      if (Content == null)
        return 0;
      else
        return MaxVisibleDepth(Content, 0);
    }
    /// <summary>
    /// Query a text visualisation of the panel tree connected to this surface.
    /// </summary>
    /// <returns></returns>
    public string GetPanelDisplay()
    {
      RequireThreadAffinity();

      var StringBuilder = new StringBuilder();

      StringBuilder.AppendLine("total panel count: " + GetPanelCount());
      StringBuilder.AppendLine("maximum visible panel depth: " + GetPanelDepth());
      StringBuilder.AppendLine();

      if (Content != null && Content.Control.Visibility.Get())
        PanelDisplay(Content, StringBuilder, 0);
      else
        StringBuilder.AppendLine("empty");

      return StringBuilder.ToString();
    }
    /// <summary>
    /// Programmatically invoke the arrange.
    /// </summary>
    public void Rearrange()
    {
      RequireThreadAffinity();

      ArrangeInvoke();
    }

    internal object Node { get; set; }

    internal void KeystrokeInvoke(Inv.Keystroke Keystroke)
    {
      RequireThreadAffinity();

      if (KeystrokeEvent != null)
        KeystrokeEvent(Keystroke);
    }
    internal void GestureBackwardInvoke()
    {
      RequireThreadAffinity();

      if (GestureBackwardDelegate != null)
        GestureBackwardDelegate();
    }
    internal void GestureForwardInvoke()
    {
      RequireThreadAffinity();

      if (GestureForwardDelegate != null)
        GestureForwardDelegate();
    }
    internal void SetGestureBackward(Action Delegate)
    {
      this.GestureBackwardDelegate = Delegate;
    }
    internal void SetGestureForward(Action Delegate)
    {
      this.GestureForwardDelegate = Delegate;
    }
    internal void ComposeInvoke()
    {
      RequireThreadAffinity();

      if (ComposeEvent != null)
        ComposeEvent();
    }
    internal void ArrangeInvoke()
    {
      RequireThreadAffinity();

      if (ArrangeEvent != null)
        ArrangeEvent();
    }
    internal void EnterInvoke()
    {
      RequireThreadAffinity();

      if (EnterEvent != null)
        EnterEvent();
    }
    internal void LeaveInvoke()
    {
      RequireThreadAffinity();

      if (LeaveEvent != null)
        LeaveEvent();
    }
    internal bool Render()
    {
      RequireThreadAffinity();

      if (IsChanged)
      {
        this.IsChanged = false;
        return true;
      }

      return false;
    }
    internal void Change()
    {
      RequireThreadAffinity();

      this.IsChanged = true;
    }
    internal void Attach(Inv.Panel Parent, Inv.Panel Child)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Parent == null || Parent.Control.Window == Window, "Parent does not belong to this window");
        Inv.Assert.Check(Child.Control.Window == Window, "Child does not belong to this window");
      }

      Attach(Child);
    }
    internal void Detach(Inv.Panel Parent, Inv.Panel Child)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Parent == null || Parent.Control.Window == Window, "Parent does not belong to this window");
        Inv.Assert.Check(Child.Control.Window == Window, "Child does not belong to this window");
      }

      Detach(Child);
    }

    private void Attach(Inv.Panel Panel)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Panel, nameof(Panel));

      var Control = Panel.Control;

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.CheckNotNull(Control, nameof(Control));
        Inv.Assert.Check(Control.Surface == null, "Panel cannot be attached because it already belongs to a surface.");
      }

      Control.SetSurface(this);

      foreach (var Child in Control.GetChilds())
        Attach(Child);
    }
    private void Detach(Inv.Panel Panel)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Panel, nameof(Panel));
     
      var Control = Panel.Control;

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.CheckNotNull(Control, nameof(Control));
        Inv.Assert.Check(Control.Surface == this, "Panel cannot be detached because it does not belong to this surface.");
      }

      Control.SetSurface(null);

      foreach (var Child in Control.GetChilds())
        Detach(Child);
    }
    private void PanelDisplay(Inv.Panel Panel, StringBuilder Builder, int Level)
    {
      var Indent = new string(' ', Level * 2);

      var Control = Panel.Control;

      var VisibleChildrenArray = Control.GetChilds().Where(P => P.Control.Visibility.Get()).ToArray();

      if (VisibleChildrenArray.Length > 0)
      {
        var PanelType = Control.DisplayType;
        if (Panel != Control)
          PanelType = Panel.GetType().Name + " : " + PanelType;

        Builder.AppendLine(Indent + PanelType);

        foreach (var Child in VisibleChildrenArray)
          PanelDisplay(Child, Builder, Level + 1);
      }
      else
      {
        Builder.AppendLine(Indent + Control.DisplayType + ";");
      }
    }

    internal void RequireThreadAffinity()
    {
      if (Inv.Assert.IsEnabled)
        Window.RequireThreadAffinity();
    }

    private Panel ContentField;
    private Action GestureBackwardDelegate;
    private Action GestureForwardDelegate;
    private bool IsChanged;
  }
}
