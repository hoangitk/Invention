﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  public static class TaskGovernor
  {
    static TaskGovernor()
    {
      CriticalSection = new Inv.ExclusiveCriticalSection("InvTaskGovernor");

      ThreadTaskDictionary = new Dictionary<int, TaskActivity>();
    }

    public static string ActiveIdentity()
    {
      var CurrentTaskId = System.Threading.Tasks.Task.CurrentId;
      if (CurrentTaskId != null)
      {
        using (CriticalSection.Lock())
        {
          var ThreadTask = ThreadTaskDictionary.GetValueOrDefault(CurrentTaskId.Value);

          return ThreadTask != null ? ThreadTask.Name : "TT_" + CurrentTaskId.Value;
        }
      }

      return null;
    }

    public static TaskActivity NewActivity(string Name, Action Method, CancellationToken? CancellationToken = null)
    {
      return new TaskActivity(Name, Method, CancellationToken);
    }
    public static TaskActivity RunActivity(string Name, Action Method, CancellationToken? CancellationToken = null)
    {
      var Result = new TaskActivity(Name, Method, CancellationToken);
      Result.Start();
      return Result;
    }
    public static void WaitAllActivities(IEnumerable<TaskActivity> Activities)
    {
      Task.WaitAll(Activities.Select(A => A.Base).ToArray());
    }
    public static void WaitAllActivities(IEnumerable<TaskActivity> Activities, TimeSpan Timeout)
    {
      Task.WaitAll(Activities.Select(A => A.Base).ToArray(), Timeout);
    }
    public static void Sleep(int MillisecondDelay)
    {
      Task.Delay(MillisecondDelay).Wait();
    }
    public static void Sleep(TimeSpan Delay)
    {
      Task.Delay(Delay).Wait();
    }
    public static IEnumerable<KeyValuePair<int, TaskActivity>> AccessActiveActivities()
    {
      using (CriticalSection.Lock())
        return ThreadTaskDictionary.ToArray();
    }

    internal static readonly Inv.ExclusiveCriticalSection CriticalSection;
    internal static readonly Dictionary<int, TaskActivity> ThreadTaskDictionary;
  }

  public sealed class TaskActivity
  {
    public TaskActivity(string Name, Action Method, CancellationToken? CancellationToken)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.CheckNotNull(Name, nameof(Name));
        Inv.Assert.CheckNotNull(Method, nameof(Method));
      }

      this.Name = Name;
      this.Method = Method;
      this.Base = CancellationToken != null ? new Task(Execute, CancellationToken.Value) : new Task(Execute);
    }

    public string Name { get; private set; }

    public void Start()
    {
      Base.Start();
    }
    public void Wait()
    {
      Base.Wait();
    }
    public bool Wait(TimeSpan Timeout)
    {
      return Base.Wait(Timeout);
    }
    public Task ContinueWith(Action<Task> continuationAction, CancellationToken? cancellationToken = null)
    {
      return cancellationToken != null ? Base.ContinueWith(continuationAction, cancellationToken.Value) : Base.ContinueWith(continuationAction);
    }

    internal Task Base { get; private set; }

    private void Execute()
    {
      using (TaskGovernor.CriticalSection.Lock())
      {
        // NOTE: duplicate task ids should not happen, but cope in RELEASE anyway.
#if DEBUG
        TaskGovernor.ThreadTaskDictionary.Add(Base.Id, this);
#else
        TaskGovernor.ThreadTaskDictionary[Base.Id] = this;
#endif
      }
      try
      {
        Method();
      }
      finally
      {
        using (TaskGovernor.CriticalSection.Lock())
          TaskGovernor.ThreadTaskDictionary.Remove(Base.Id);
      }
    }

    private readonly Action Method;
  }
}
