﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv.Database
{
  public sealed class Schematic
  {
    public Schematic()
    {
      this.TableList = new Inv.DistinctList<Table>();
      this.ViewList = new Inv.DistinctList<View>();
    }

    public void Compile(object Self)
    {
      var SchematicType = Self.GetType();
      var SchematicTypeInfo = SchematicType.GetReflectionInfo();

      var TypeDictionary = new Dictionary<Type, Contract<Table>>();

      foreach (var TableField in SchematicTypeInfo.GetReflectionFields())
      {
        var TableInstance = (Contract<Table>)null;
        if (typeof(Contract<View>).IsAssignableFrom(TableField.FieldType))
          TableInstance = (Contract<Table>)TableField.FieldType.ReflectionCreate(typeof(View), AddView(TableField.Name));
        else if (typeof(Contract<Table>).IsAssignableFrom(TableField.FieldType))
          TableInstance = (Contract<Table>)TableField.FieldType.ReflectionCreate(typeof(Table), AddTable(TableField.Name));

        if (TableInstance != null)
        {
          TableField.SetValue(Self, TableInstance);

          TypeDictionary.Add(TableField.FieldType, TableInstance);

          var Table = TableInstance.Base;
          var TableType = TableField.FieldType.GetReflectionInfo();

          foreach (var Field in TableType.GetReflectionFields())
          {
            if (typeof(Inv.Database.Contract<Column>).IsAssignableFrom(Field.FieldType))
            {
              Contract<Column> Column;

              if (Field.FieldType == typeof(PrimaryKeyColumn))
              {
                Column = Table.SetPrimaryKeyColumn(Field.Name);
              }
              else if (typeof(ForeignKeyColumn).IsAssignableFrom(Field.FieldType) && Field.FieldType.GenericTypeArguments.Length == 1)
              {
                var MethodName = $"Add{ (Field.FieldType.IsNested ? "Nullable" : "") }ForeignKeyColumn";
                var MethodInfo = typeof(Table).GetReflectionInfo().GetReflectionMethods().Find(M => M.Name == MethodName && M.ContainsGenericParameters && typeof(Contract<Table>).IsAssignableFrom(M.GetGenericArguments()[0]));

                var GenericMethodInfo = MethodInfo.MakeGenericMethod(Field.FieldType.GenericTypeArguments[0]);

                Column = (Contract<Column>)GenericMethodInfo.Invoke(Table, new object[] { Field.Name });
              }
              else
              {
                Column = (Contract<Column>)Field.FieldType.ReflectionCreate(typeof(Inv.Database.Table), TableInstance.Base, typeof(string), Field.Name);
                Table.AddColumn(Column.Base);
              }

              Field.SetValue(TableInstance, Column);
            }
          }

          foreach (var Method in TableType.GetReflectionMethods())
          {
            if (Method.IsPrivate && typeof(Inv.Database.Contract<Index>).IsAssignableFrom(Method.ReturnType))
            {
              var Index = (Contract<Index>)Method.Invoke(TableInstance, null);

              Table.AddIndex(Index);
            }
          }
        }
      }

      // knit foreign keys to tables.
      foreach (var TableField in SchematicTypeInfo.GetReflectionFields())
      {
        if (typeof(Contract<Table>).IsAssignableFrom(TableField.FieldType))
        {
          var TableInstance = (Contract<Table>)TableField.GetValue(Self);

          foreach (var ColumnField in TableField.FieldType.GetReflectionFields())
          {
            if (typeof(ForeignKeyColumn).IsAssignableFrom(ColumnField.FieldType) && ColumnField.FieldType.GenericTypeArguments.Length == 1)
            {
              // generic foreign key column.

              var Column = ColumnField.GetValue(TableInstance);

              var ForeignType = ColumnField.FieldType.GenericTypeArguments[0];

              var ForeignTable = TypeDictionary[ForeignType];

              var ColumnTableField = ColumnField.FieldType.GetReflectionProperty("ReferenceTable");
              ColumnTableField.SetValue(Column, ForeignTable);
            }
          }
        }
      }

      foreach (var ViewField in SchematicTypeInfo.GetReflectionFields())
      {
        if (typeof(Contract<View>).IsAssignableFrom(ViewField.FieldType))
        {
          var ViewInstance = (Contract<View>)ViewField.GetValue(Self);

          var BuildViewMethod = ViewField.FieldType.GetReflectionInfo().GetReflectionMethods().Find(M => M.IsPrivate && M.Name == "Build");
          System.Diagnostics.Debug.Assert(BuildViewMethod != null, "Unable to find Build() method for view '" + ViewField.Name + "'");

          var QueryType = typeof(Query<>).MakeGenericType(SchematicType);
          var Query = QueryType.ReflectionCreate(SchematicType, Self);

          var SelectClause = (SelectClause)BuildViewMethod.Invoke(ViewInstance, new object[] { Query });

          ViewInstance.Base.SelectClause = SelectClause;
        }
      }
    }
    public Table AddTable(string Name)
    {
      var Result = new Table(Name);

      TableList.Add(Result);

      return Result;
    }
    public View AddView(string Name)
    {
      var Result = new View(Name);

      ViewList.Add(Result);

      return Result;
    }
    public IEnumerable<Table> GetTables()
    {
      return TableList;
    }
    public IEnumerable<View> GetViews()
    {
      return ViewList;
    }

    private Inv.DistinctList<Table> TableList;
    private Inv.DistinctList<View> ViewList;
  }

  public interface Contract<TBase>
  {
    TBase Base { get; }
  }

  public class Table
  {
    internal Table(string Name)
    {
      this.Name = Name;
      this.ColumnList = new Inv.DistinctList<Inv.Database.Contract<Column>>();
      this.IndexList = new DistinctList<Inv.Database.Contract<Index>>();
      this.ForeignKeyColumnList = new Inv.DistinctList<ForeignKeyColumn>();
    }

    public string Name { get; private set; }
    public PrimaryKeyColumn PrimaryKeyColumn { get; private set; }

    public Column AddColumn(string Name, Inv.Database.ColumnType Type)
    {
      var Result = new Column(this, Name, Type);

      ColumnList.Add(Result);

      return Result;
    }
    public PrimaryKeyColumn SetPrimaryKeyColumn(string Name)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(PrimaryKeyColumn == null, "Table must have only one primary key column.");

      var Result = AddColumn(new PrimaryKeyColumn(this, Name));

      this.PrimaryKeyColumn = Result;

      return Result;
    }
    public ForeignKeyColumn<TTable> AddForeignKeyColumn<TTable>(string Name)
      where TTable : Contract<Table>
    {
      var Result = new ForeignKeyColumn<TTable>(this, Name);

      ForeignKeyColumnList.Add(Result);
      AddColumn(Result);

      return Result;
    }
    public ForeignKeyColumn<TTable>.Null AddNullableForeignKeyColumn<TTable>(string Name)
      where TTable : Contract<Table>
    {
      var Result = new ForeignKeyColumn<TTable>.Null(this, Name);

      ForeignKeyColumnList.Add(Result);
      AddColumn(Result);

      return Result;
    }
    public StringColumn AddStringColumn(string Name)
    {
      return AddColumn(new StringColumn(this, Name));
    }
    public StringColumn.Null AddNullableStringColumn(string Name)
    {
      return AddColumn(new StringColumn.Null(this, Name));
    }
    public ImageColumn AddImageColumn(string Name)
    {
      return AddColumn(new ImageColumn(this, Name));
    }
    public GuidColumn AddGuidColumn(string Name)
    {
      return AddColumn(new GuidColumn(this, Name));
    }
    public Integer32Column AddInteger32Column(string Name)
    {
      return AddColumn(new Integer32Column(this, Name));
    }
    public Integer64Column AddInteger64Column(string Name)
    {
      return AddColumn(new Integer64Column(this, Name));
    }
    public DateColumn AddDateColumn(string Name)
    {
      return AddColumn(new DateColumn(this, Name));
    }
    public DateColumn.Null AddNullableDateColumn(string Name)
    {
      return AddColumn(new DateColumn.Null(this, Name));
    }
    public DateTimeColumn AddDateTimeColumn(string Name)
    {
      return AddColumn(new DateTimeColumn(this, Name));
    }
    public DateTimeOffsetColumn AddDateTimeOffsetColumn(string Name)
    {
      return AddColumn(new DateTimeOffsetColumn(this, Name));
    }
    public TimeColumn AddTimeColumn(string Name)
    {
      return AddColumn(new TimeColumn(this, Name));
    }
    public TimeSpanColumn AddTimeSpanColumn(string Name)
    {
      return AddColumn(new TimeSpanColumn(this, Name));
    }
    public BooleanColumn AddBooleanColumn(string Name)
    {
      return AddColumn(new BooleanColumn(this, Name));
    }
    public bool IsPrimaryKeyColumn(Inv.Database.Column Column)
    {
      return Column == (Inv.Database.Column)PrimaryKeyColumn;
    }
    public IEnumerable<Inv.Database.Contract<Column>> GetColumns()
    {
      return ColumnList;
    }
    public IEnumerable<Inv.Database.Contract<Index>> GetIndexes()
    {
      return IndexList;
    }
    public IEnumerable<ForeignKeyColumn> GetForeignKeyColumns()
    {
      return ForeignKeyColumnList;
    }

    internal TColumn AddColumn<TColumn>(TColumn Column)
      where TColumn : Contract<Column>
    {
      ColumnList.Add(Column);

      return Column;
    }
    internal TIndex AddIndex<TIndex>(TIndex Index)
      where TIndex : Contract<Index>
    {
      Index.Base.Table = this;

      IndexList.Add(Index);

      return Index;
    }

    private Inv.DistinctList<Inv.Database.Contract<Column>> ColumnList;
    private Inv.DistinctList<Inv.Database.Contract<Index>> IndexList;
    private Inv.DistinctList<ForeignKeyColumn> ForeignKeyColumnList;
  }

  public sealed class View : Table
  {
    internal View(string Name)
      : base(Name)
    {
    }

    public SelectClause SelectClause { get; internal set; }
  }

  public sealed class Column : Contract<Column>
  {
    internal Column(Inv.Database.Table Table, string Name, ColumnType Type)
    {
      this.Table = Table;
      this.Name = Name;
      this.Type = Type;
    }

    public Inv.Database.Table Table { get; private set; }
    public string Name { get; private set; }
    public ColumnType Type { get; private set; }
    public bool Nullable { get; internal set; }

    Column Contract<Column>.Base => this;
  }

  public enum ColumnType
  {
    PrimaryKey,
    ForeignKey,
    Binary,
    Boolean,
    Date,
    DateTime,
    DateTimeOffset,
    Decimal,
    Double,
    Enum, // TODO : <T> needs to be available for check constraints etc.
    Guid,
    Image,
    Integer32,
    Integer64,
    Money,
    String,
    Time,
    TimeSpan
  }

  public sealed class PrimaryKeyColumn : Inv.Mimic<Column>, Contract<Column>
  {
    internal PrimaryKeyColumn(Inv.Database.Table Table, string Name)
    {
      this.Base = new Column(Table, Name, Inv.Database.ColumnType.PrimaryKey);
    }

    public string Name => Base.Name;
    public Inv.Database.Table Table => Base.Table;

    Column Contract<Column>.Base => Base;
  }

  public abstract class ForeignKeyColumn : Inv.Mimic<Column>, Contract<Column>
  {
    internal ForeignKeyColumn(Inv.Database.Table Table, string Name)
    {
      this.Base = new Column(Table, Name, Inv.Database.ColumnType.ForeignKey);
    }

    public string Name => Base.Name;
    public Table Table => Base.Table;
    public Table ReferenceTable { get; internal set; }
    public bool Nullable
    {
      get { return Base.Nullable; }
      set { Base.Nullable = value; }
    }

    Column Contract<Column>.Base => Base;

    public abstract class Null : ForeignKeyColumn
    {
      internal Null(Inv.Database.Table Table, string Name)
        : base(Table, Name)
      {
        Nullable = true;
      }
    }
  }

  public sealed class ForeignKeyColumn<TTable> : ForeignKeyColumn
    where TTable : Contract<Table>
  {
    internal ForeignKeyColumn(Inv.Database.Table Table, string Name)
      : base(Table, Name)
    {
    }

    internal new TTable ReferenceTable
    {
      set { base.ReferenceTable = value.Base; }
    }

    public sealed new class Null : ForeignKeyColumn.Null
    {
      internal Null(Inv.Database.Table Table, string Name)
        : base(Table, Name)
      {
        Nullable = true;
      }

      internal new TTable ReferenceTable
      {
        set { base.ReferenceTable = value.Base; }
      }
    }
  }

  public sealed class StringColumn : Inv.Mimic<Column>, Contract<Column>
  {
    public StringColumn(Inv.Database.Table Table, string Name)
    {
      this.Base = new Column(Table, Name, Inv.Database.ColumnType.String);
      Base.Nullable = false;
    }

    public string Name => Base.Name;
    public bool Nullable => Base.Nullable;

    Column Contract<Column>.Base => Base;

    public sealed class Null : Inv.Mimic<Column>, Contract<Column>
    {
      internal Null(Inv.Database.Table Table, string Name)
      {
        this.Base = new Column(Table, Name, Inv.Database.ColumnType.String);
        Base.Nullable = true;
      }

      public string Name => Base.Name;
      public bool Nullable => Base.Nullable;

      Column Contract<Column>.Base => Base;
    }
  }

  public sealed class DecimalColumn : Inv.Mimic<Column>, Contract<Column>
  {
    internal DecimalColumn(Inv.Database.Table Table, string Name)
    {
      this.Base = new Column(Table, Name, Inv.Database.ColumnType.Decimal);
      Base.Nullable = false;
    }

    public string Name => Base.Name;
    public bool Nullable => Base.Nullable;

    Column Contract<Column>.Base => Base;

    public sealed class Null : Inv.Mimic<Column>, Contract<Column>
    {
      internal Null(Inv.Database.Table Table, string Name)
      {
        this.Base = new Column(Table, Name, Inv.Database.ColumnType.Decimal);
        Base.Nullable = true;
      }

      public string Name => Base.Name;
      public bool Nullable => Base.Nullable;

      Column Contract<Column>.Base => Base;
    }
  }

  public sealed class MoneyColumn : Inv.Mimic<Column>, Contract<Column>
  {
    internal MoneyColumn(Inv.Database.Table Table, string Name)
    {
      this.Base = new Column(Table, Name, Inv.Database.ColumnType.Money);
      Base.Nullable = false;
    }

    public string Name => Base.Name;
    public bool Nullable => Base.Nullable;

    Column Contract<Column>.Base => Base;

    public sealed class Null : Inv.Mimic<Column>, Contract<Column>
    {
      internal Null(Inv.Database.Table Table, string Name)
      {
        this.Base = new Column(Table, Name, Inv.Database.ColumnType.Money);
        Base.Nullable = true;
      }

      public string Name => Base.Name;
      public bool Nullable => Base.Nullable;

      Column Contract<Column>.Base => Base;
    }
  }

  public sealed class DoubleColumn : Inv.Mimic<Column>, Contract<Column>
  {
    internal DoubleColumn(Inv.Database.Table Table, string Name)
    {
      this.Base = new Column(Table, Name, Inv.Database.ColumnType.Double);
      Base.Nullable = false;
    }

    public string Name => Base.Name;
    public bool Nullable => Base.Nullable;

    Column Contract<Column>.Base => Base;

    public sealed class Null : Inv.Mimic<Column>, Contract<Column>
    {
      internal Null(Inv.Database.Table Table, string Name)
      {
        this.Base = new Column(Table, Name, Inv.Database.ColumnType.Double);
        Base.Nullable = true;
      }

      public string Name => Base.Name;
      public bool Nullable => Base.Nullable;

      Column Contract<Column>.Base => Base;
    }
  }

  public sealed class ImageColumn : Inv.Mimic<Column>, Contract<Column>
  {
    public ImageColumn(Inv.Database.Table Table, string Name)
    {
      this.Base = new Column(Table, Name, Inv.Database.ColumnType.Image);
      Base.Nullable = false; // All image columns are nullable.
    }

    public string Name => Base.Name;

    Column Contract<Column>.Base => Base;

    public sealed class Null : Inv.Mimic<Column>, Contract<Column>
    {
      internal Null(Inv.Database.Table Table, string Name)
      {
        this.Base = new Column(Table, Name, Inv.Database.ColumnType.Image);
        Base.Nullable = true;
      }

      public string Name => Base.Name;
      public bool Nullable => Base.Nullable;

      Column Contract<Column>.Base => Base;
    }
  }

  public sealed class GuidColumn : Inv.Mimic<Column>, Contract<Column>
  {
    internal GuidColumn(Inv.Database.Table Table, string Name)
    {
      this.Base = new Column(Table, Name, Inv.Database.ColumnType.Guid);
      Base.Nullable = false;
    }

    public string Name => Base.Name;
    public bool Nullable => Base.Nullable;

    Column Contract<Column>.Base => Base;

    public sealed class Null : Inv.Mimic<Column>, Contract<Column>
    {
      internal Null(Inv.Database.Table Table, string Name)
      {
        this.Base = new Column(Table, Name, Inv.Database.ColumnType.Guid);
        Base.Nullable = true;
      }

      public string Name => Base.Name;
      public bool Nullable => Base.Nullable;

      Column Contract<Column>.Base => Base;
    }
  }

  public sealed class EnumColumn<T> : Inv.Mimic<Column>, Contract<Column>
  {
    internal EnumColumn(Inv.Database.Table Table, string Name)
    {
      this.Base = new Column(Table, Name, Inv.Database.ColumnType.Enum);
    }

    public string Name => Base.Name;
    public bool Nullable
    {
      get { return Base.Nullable; }
      set { Base.Nullable = value; }
    }

    Column Contract<Column>.Base => Base;

    public sealed class Null : Inv.Mimic<Column>, Contract<Column>
    {
      internal Null(Inv.Database.Table Table, string Name)
      {
        this.Base = new Column(Table, Name, Inv.Database.ColumnType.Enum);
        Base.Nullable = true;
      }

      public string Name => Base.Name;
      public bool Nullable => Base.Nullable;

      Column Contract<Column>.Base => Base;
    }
  }

  public sealed class Integer32Column : Inv.Mimic<Column>, Contract<Column>
  {
    internal Integer32Column(Inv.Database.Table Table, string Name)
    {
      this.Base = new Column(Table, Name, Inv.Database.ColumnType.Integer32);
      Base.Nullable = false;
    }

    public string Name => Base.Name;
    public bool Nullable => Base.Nullable;

    Column Contract<Column>.Base => Base;

    public sealed class Null : Inv.Mimic<Column>, Contract<Column>
    {
      internal Null(Inv.Database.Table Table, string Name)
      {
        this.Base = new Column(Table, Name, Inv.Database.ColumnType.Integer32);
        Base.Nullable = true;
      }

      public string Name => Base.Name;
      public bool Nullable => Base.Nullable;

      Column Contract<Column>.Base => Base;
    }
  }

  public sealed class Integer64Column : Inv.Mimic<Column>, Contract<Column>
  {
    internal Integer64Column(Inv.Database.Table Table, string Name)
    {
      this.Base = new Column(Table, Name, Inv.Database.ColumnType.Integer64);
      Base.Nullable = false;
    }

    public string Name => Base.Name;
    public bool Nullable => Base.Nullable;

    Column Contract<Column>.Base => Base;

    public sealed class Null : Inv.Mimic<Column>, Contract<Column>
    {
      internal Null(Inv.Database.Table Table, string Name)
      {
        this.Base = new Column(Table, Name, Inv.Database.ColumnType.Integer64);
        Base.Nullable = true;
      }

      public string Name => Base.Name;
      public bool Nullable => Base.Nullable;

      Column Contract<Column>.Base => Base;
    }
  }

  public sealed class DateColumn : Inv.Mimic<Column>, Contract<Column>
  {
    internal DateColumn(Inv.Database.Table Table, string Name)
    {
      this.Base = new Column(Table, Name, Inv.Database.ColumnType.Date);
      Base.Nullable = false;
    }

    public string Name => Base.Name;
    public bool Nullable => Base.Nullable;

    Column Contract<Column>.Base => Base;

    public sealed class Null : Inv.Mimic<Column>, Contract<Column>
    {
      internal Null(Inv.Database.Table Table, string Name)
      {
        this.Base = new Column(Table, Name, Inv.Database.ColumnType.Date);
        Base.Nullable = true;
      }

      public string Name => Base.Name;
      public bool Nullable => Base.Nullable;

      Column Contract<Column>.Base => Base;
    }
  }

  public sealed class DateTimeColumn : Inv.Mimic<Column>, Contract<Column>
  {
    internal DateTimeColumn(Inv.Database.Table Table, string Name)
    {
      this.Base = new Column(Table, Name, Inv.Database.ColumnType.DateTime);
      Base.Nullable = false;
    }

    public string Name => Base.Name;
    public bool Nullable => Base.Nullable;

    Column Contract<Column>.Base => Base;

    public sealed class Null : Inv.Mimic<Column>, Contract<Column>
    {
      internal Null(Inv.Database.Table Table, string Name)
      {
        this.Base = new Column(Table, Name, Inv.Database.ColumnType.DateTime);
        Base.Nullable = true;
      }

      public string Name => Base.Name;
      public bool Nullable => Base.Nullable;

      Column Contract<Column>.Base => Base;
    }
  }

  public sealed class DateTimeOffsetColumn : Inv.Mimic<Column>, Contract<Column>
  {
    internal DateTimeOffsetColumn(Inv.Database.Table Table, string Name)
    {
      this.Base = new Column(Table, Name, Inv.Database.ColumnType.DateTimeOffset);
      Base.Nullable = false;
    }

    public string Name => Base.Name;
    public bool Nullable => Base.Nullable;

    Column Contract<Column>.Base => Base;

    public sealed class Null : Inv.Mimic<Column>, Contract<Column>
    {
      internal Null(Inv.Database.Table Table, string Name)
      {
        this.Base = new Column(Table, Name, Inv.Database.ColumnType.DateTimeOffset);
        Base.Nullable = true;
      }

      public string Name => Base.Name;
      public bool Nullable => Base.Nullable;

      Column Contract<Column>.Base => Base;
    }
  }

  public sealed class TimeColumn : Inv.Mimic<Column>, Contract<Column>
  {
    internal TimeColumn(Inv.Database.Table Table, string Name)
    {
      this.Base = new Column(Table, Name, Inv.Database.ColumnType.Time);
      Base.Nullable = false;
    }

    public string Name => Base.Name;
    public bool Nullable => Base.Nullable;

    Column Contract<Column>.Base => Base;

    public sealed class Null : Inv.Mimic<Column>, Contract<Column>
    {
      internal Null(Inv.Database.Table Table, string Name)
      {
        this.Base = new Column(Table, Name, Inv.Database.ColumnType.Time);
        Base.Nullable = true;
      }

      public string Name => Base.Name;
      public bool Nullable => Base.Nullable;

      Column Contract<Column>.Base => Base;
    }
  }

  public sealed class TimeSpanColumn : Inv.Mimic<Column>, Contract<Column>
  {
    internal TimeSpanColumn(Inv.Database.Table Table, string Name)
    {
      this.Base = new Column(Table, Name, Inv.Database.ColumnType.TimeSpan);
      Base.Nullable = false;
    }

    public string Name => Base.Name;
    public bool Nullable => Base.Nullable;

    Column Contract<Column>.Base => Base;

    public sealed class Null : Inv.Mimic<Column>, Contract<Column>
    {
      internal Null(Inv.Database.Table Table, string Name)
      {
        this.Base = new Column(Table, Name, Inv.Database.ColumnType.TimeSpan);
        Base.Nullable = true;
      }

      public string Name => Base.Name;
      public bool Nullable => Base.Nullable;

      Column Contract<Column>.Base => Base;
    }
  }

  public sealed class BooleanColumn : Inv.Mimic<Column>, Contract<Column>
  {
    internal BooleanColumn(Inv.Database.Table Table, string Name)
    {
      this.Base = new Column(Table, Name, Inv.Database.ColumnType.Boolean);
      Base.Nullable = false;
    }

    public string Name => Base.Name;
    public bool Nullable => Base.Nullable;

    Column Contract<Column>.Base => Base;

    public sealed class Null : Inv.Mimic<Column>, Contract<Column>
    {
      internal Null(Inv.Database.Table Table, string Name)
      {
        this.Base = new Column(Table, Name, Inv.Database.ColumnType.Boolean);
        Base.Nullable = true;
      }

      public string Name => Base.Name;
      public bool Nullable => Base.Nullable;

      Column Contract<Column>.Base => Base;
    }
  }

  public sealed class BinaryColumn : Inv.Mimic<Column>, Contract<Column>
  {
    public BinaryColumn(Inv.Database.Table Table, string Name)
    {
      this.Base = new Column(Table, Name, Inv.Database.ColumnType.Binary);
      Base.Nullable = false;
    }

    public string Name => Base.Name;
    public bool Nullable => Base.Nullable;

    Column Contract<Column>.Base => Base;

    public sealed class Null : Inv.Mimic<Column>, Contract<Column>
    {
      internal Null(Inv.Database.Table Table, string Name)
      {
        this.Base = new Column(Table, Name, Inv.Database.ColumnType.Binary);
        Base.Nullable = true;
      }

      public string Name => Base.Name;
      public bool Nullable => Base.Nullable;

      Column Contract<Column>.Base => Base;
    }
  }

  public sealed class Index
  {
    internal Index(string Name, params Contract<Inv.Database.Column>[] ColumnArray)
    {
      this.Name = Name;
      this.ColumnList = ColumnArray.ToDistinctList();
    }

    public Inv.Database.Table Table { get; internal set; }
    public bool IsUnique { get; internal set; }

    public static UniqueIndex Unique(string Name, params Contract<Inv.Database.Column>[] ColumnArray)
    {
      return new UniqueIndex(new Index(Name, ColumnArray));
    }
    public static UniqueIndex Unique(params Contract<Inv.Database.Column>[] ColumnArray)
    {
      return new UniqueIndex(new Index(null, ColumnArray));
    }
    public static DuplicateIndex Duplicate(string Name, params Contract<Inv.Database.Column>[] ColumnArray)
    {
      return new DuplicateIndex(new Index(Name, ColumnArray));
    }
    public static DuplicateIndex Duplicate(params Contract<Inv.Database.Column>[] ColumnArray)
    {
      return new DuplicateIndex(new Index(null, ColumnArray));
    }

    internal string GetName()
    {
      return Name ?? ColumnList.Select(C => C.Base.Name).AsSeparatedText("_");
    }
    internal IEnumerable<Inv.Database.Column> GetColumns()
    {
      return ColumnList.Select(C => C.Base);
    }
    internal bool StartsWith(params Inv.Database.Column[] QueryColumnArray)
    {
      if (QueryColumnArray.Length > ColumnList.Count)
        return false;

      for (var QueryColumnIndex = 0; QueryColumnIndex < QueryColumnArray.Length; QueryColumnIndex++)
      {
        var Column = ColumnList[QueryColumnIndex];
        var QueryColumn = QueryColumnArray[QueryColumnIndex];

        if (Column != QueryColumn)
          return false;
      }

      return true;
    }

    private string Name;
    private Inv.DistinctList<Contract<Inv.Database.Column>> ColumnList;
  }

  public sealed class UniqueIndex : Inv.Mimic<Index>, Contract<Index>
  {
    internal UniqueIndex(Index Base)
    {
      this.Base = Base;
      Base.IsUnique = true;
    }

    Index Contract<Index>.Base => Base;
  }

  public sealed class DuplicateIndex : Inv.Mimic<Index>, Contract<Index>
  {
    internal DuplicateIndex(Index Base)
    {
      this.Base = Base;
      Base.IsUnique = false;
    }

    Index Contract<Index>.Base => Base;
  }
}