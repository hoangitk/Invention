﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// A serializable class representing a 32-bit ARGB colour.
  /// </summary>
  [XmlSchemaProvider("MySchema")]
  public sealed class Colour : IComparable, IEquatable<Colour>, IXmlSerializable
  {
    /// <summary>
    /// Return a colour from ARGB components.
    /// </summary>
    /// <param name="Alpha"></param>
    /// <param name="Red"></param>
    /// <param name="Green"></param>
    /// <param name="Blue"></param>
    /// <returns></returns>
    public static Inv.Colour FromArgb(byte Alpha, byte Red, byte Green, byte Blue)
    {
      return ColourFactory.Resolve(new ColourARGBRecord()
      {
        A = Alpha,
        R = Red,
        G = Green,
        B = Blue
      }.Argb);
    }
    /// <summary>
    /// Return a colour from an ARGB struct.
    /// </summary>
    /// <param name="Argb"></param>
    /// <returns></returns>
    public static Inv.Colour FromArgb(ColourARGBRecord Argb)
    {
      return ColourFactory.Resolve(Argb.Argb);
    }
    /// <summary>
    /// Return a colour from a raw number.
    /// </summary>
    /// <param name="Argb"></param>
    /// <returns></returns>
    public static Inv.Colour FromArgb(uint Argb)
    {
      return ColourFactory.Resolve((int)Argb);
    }
    /// <summary>
    /// Return a colour from a raw number.
    /// </summary>
    /// <param name="Argb"></param>
    /// <returns></returns>
    public static Inv.Colour FromArgb(int Argb)
    {
      return ColourFactory.Resolve(Argb);
    }
    /// <summary>
    /// Return a colour from HSV components.
    /// </summary>
    /// <param name="hue"></param>
    /// <param name="saturation"></param>
    /// <param name="value"></param>
    /// <returns></returns>
    public static Inv.Colour FromHSV(double hue, double saturation, double value)
    {
      int hi = Convert.ToInt32(Math.Floor(hue / 60)) % 6;
      double f = hue / 60 - Math.Floor(hue / 60);

      value *= 255;
      var v = Convert.ToByte(value);
      var p = Convert.ToByte(value * (1 - saturation));
      var q = Convert.ToByte(value * (1 - f * saturation));
      var t = Convert.ToByte(value * (1 - (1 - f) * saturation));

      if (hi == 0)
        return Inv.Colour.FromArgb(255, v, t, p);
      else if (hi == 1)
        return Inv.Colour.FromArgb(255, q, v, p);
      else if (hi == 2)
        return Inv.Colour.FromArgb(255, p, v, t);
      else if (hi == 3)
        return Inv.Colour.FromArgb(255, p, q, v);
      else if (hi == 4)
        return Inv.Colour.FromArgb(255, t, p, v);
      else
        return Inv.Colour.FromArgb(255, v, p, q);
    }
    /// <summary>
    /// Return a colour from a registered name.
    /// </summary>
    /// <param name="Name"></param>
    /// <returns></returns>
    public static Inv.Colour FromName(string Name)
    {
      var Result = NameBidictionary.GetByRightOrDefault(Name, null);

      if (Result == null)
      {
        // handle obsolete duplicates.
        if (Name == "Fuchsia")
          Result = Colour.Magenta;
        else if (Name == "Aqua")
          Result = Colour.Cyan;
      }

      return Result;
    }
    /// <summary>
    /// Return a colour from a hexadecimal string. eg. FFF0F8FF or F0F8FF
    /// Must not start with a hash (#).
    /// </summary>
    /// <param name="Value"></param>
    /// <returns></returns>
    public static Inv.Colour FromHexadecimalString(string Value)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Value != null && Value.Length >= 6 && Value[0] != '#', "Hex string must be valid: {0}", Value);

      if (Value.Length == 6)
        return Inv.Colour.FromArgb(int.Parse("FF" + Value, System.Globalization.NumberStyles.AllowHexSpecifier));
      else
        return Inv.Colour.FromArgb(int.Parse(Value, System.Globalization.NumberStyles.AllowHexSpecifier));
    }
    /// <summary>
    /// Return a colour from a html string. eg. #F0F8FF
    /// Must start with a hash (#) and represent only RR GG BB.
    /// </summary>
    /// <param name="Value"></param>
    /// <returns></returns>
    public static Inv.Colour FromHtmlString(string Value)
    {
      if (Value.Length == 7 && Value[0] == '#')
        return Inv.Colour.FromHexadecimalString("FF" + Value.Substring(1));
      else
        return null;
    }

    internal Colour(int Argb)
    {
      // NOTE: only used by ColourTree.
      this.RawValue = Argb;
    }

    /// <summary>
    /// Registered name of the colour, otherwise the Hex representation.
    /// </summary>
    public string Name
    {
      get { return NameBidictionary.GetByLeftOrDefault(this, Hex); }
    }
    /// <summary>
    /// Hex representation of the colour. eg. FF F0 F8 FF
    /// </summary>
    public string Hex
    {
      get { return RawValue.ToString("X8").Batch(2).AsSeparatedText(" "); }
    }
    /// <summary>
    /// Raw internal value.
    /// </summary>
    public int RawValue { get; private set; }
    /// <summary>
    /// Ask if the colour is opaque (Alpha 0xFF)
    /// </summary>
    public bool IsOpaque
    {
      get { return GetARGBRecord().A == 0xFF; }
    }
    /// <summary>
    /// Ask if the colour is transparent (Alpha 0x00)
    /// </summary>
    public bool IsTransparent
    {
      get { return GetARGBRecord().A == 0x00; }
    }

    /// <summary>
    /// Get the colour as an ARGBA struct.
    /// </summary>
    /// <returns></returns>
    public ColourARGBRecord GetARGBRecord()
    {
      var Result = new ColourARGBRecord();

      Result.Argb = RawValue;

      return Result;
    }
    /// <summary>
    /// Get the colour as a HSL struct.
    /// </summary>
    /// <returns></returns>
    public ColourHSLRecord GetHSLRecord()
    {
      var Record = GetARGBRecord();

      var Result = new ColourHSLRecord();

      // NOTE: lifted from System.Drawing.Color methods GetHue(), GetSaturation(), GetBrightness()

      // HUE.
      {
        if ((int)Record.R == (int)Record.G && (int)Record.G == (int)Record.B)
        {
          Result.H = 0.0f;
        }
        else
        {
          var num1 = (float)Record.R / (float)byte.MaxValue;
          var num2 = (float)Record.G / (float)byte.MaxValue;
          var num3 = (float)Record.B / (float)byte.MaxValue;
          var num4 = 0.0f;
          var num5 = num1;
          var num6 = num1;
          if ((double)num2 > (double)num5)
            num5 = num2;
          if ((double)num3 > (double)num5)
            num5 = num3;
          if ((double)num2 < (double)num6)
            num6 = num2;
          if ((double)num3 < (double)num6)
            num6 = num3;
          float num7 = num5 - num6;
          if ((double)num1 == (double)num5)
            num4 = (num2 - num3) / num7;
          else if ((double)num2 == (double)num5)
            num4 = (float)(2.0 + ((double)num3 - (double)num1) / (double)num7);
          else if ((double)num3 == (double)num5)
            num4 = (float)(4.0 + ((double)num1 - (double)num2) / (double)num7);
          var num8 = num4 * 60f;
          if ((double)num8 < 0.0)
            num8 += 360f;
          Result.H = num8;
        }
      }

      // SATURATION.
      {
        var num1 = (double)Record.R / (double)byte.MaxValue;
        var num2 = (float)Record.G / (float)byte.MaxValue;
        var num3 = (float)Record.B / (float)byte.MaxValue;
        var num4 = 0.0f;
        var num5 = (float)num1;
        var num6 = (float)num1;
        if ((double)num2 > (double)num5)
          num5 = num2;
        if ((double)num3 > (double)num5)
          num5 = num3;
        if ((double)num2 < (double)num6)
          num6 = num2;
        if ((double)num3 < (double)num6)
          num6 = num3;
        if ((double)num5 != (double)num6)
          num4 = ((double)num5 + (double)num6) / 2.0 > 0.5 ? (float)(((double)num5 - (double)num6) / (2.0 - (double)num5 - (double)num6)) : (float)(((double)num5 - (double)num6) / ((double)num5 + (double)num6));
        Result.S = num4;
      }

      // BRIGHTNESS.
      {
        var num1 = (double)Record.R / (double)byte.MaxValue;
        var num2 = (float)Record.G / (float)byte.MaxValue;
        var num3 = (float)Record.B / (float)byte.MaxValue;
        var num4 = (float)num1;
        var num5 = (float)num1;
        if ((double)num2 > (double)num4)
          num4 = num2;
        if ((double)num3 > (double)num4)
          num4 = num3;
        if ((double)num2 < (double)num5)
          num5 = num2;
        if ((double)num3 < (double)num5)
          num5 = num3;

        Result.L = (float)(((double)num4 + (double)num5) / 2.0);
      }

      return Result;
    }
    /// <summary>
    /// Get the raw value of the colour.
    /// </summary>
    /// <returns></returns>
    public int GetArgb()
    {
      return RawValue;
    }
    /// <summary>
    /// Get the html colour representation. eg. #F0F8FF
    /// </summary>
    /// <returns></returns>
    public string ToHtmlColour()
    {
      return "#" + (RawValue & 0xFFFFFF).ToString("X6");
    }

    /// <summary>
    /// Compare to another colour.
    /// </summary>
    /// <param name="Colour"></param>
    /// <returns></returns>
    public int CompareTo(Colour Colour)
    {
      return this.RawValue.CompareTo(Colour.RawValue);
    }
    /// <summary>
    /// Ask if the colours are equal.
    /// </summary>
    /// <param name="Colour"></param>
    /// <returns></returns>
    public bool EqualTo(Colour Colour)
    {
      return this.RawValue == Colour.RawValue;
    }
    /// <summary>
    /// Get hash code for the colour.
    /// </summary>
    /// <returns></returns>
    public override int GetHashCode()
    {
      return RawValue;
    }
    /// <summary>
    /// Return the registered name or hex display of the colour.
    /// </summary>
    /// <returns></returns>
    public override string ToString()
    {
      return Name;
    }

    // not a data member.
    internal object Node { get; set; }

    bool IEquatable<Colour>.Equals(Inv.Colour Colour)
    {
      return CompareTo(Colour) == 0;
    }
    int IComparable.CompareTo(object Object)
    {
      return CompareTo((Colour)Object);
    }

#pragma warning disable IDE0051 // Remove unused private members
#pragma warning disable IDE0060 // Remove unused parameter
    private static XmlQualifiedName MySchema(object xs)
#pragma warning restore IDE0060 // Remove unused parameter
#pragma warning restore IDE0051 // Remove unused private members
    {
      // NOTE: this is required by the XmlSchemaProvider attribute, and is accessed via reflection.
      return new XmlQualifiedName("string", "http://www.w3.org/2001/XMLSchema");
    }
    System.Xml.Schema.XmlSchema IXmlSerializable.GetSchema()
    {
      return null;  // NOTE: this is required to be null.
    }
    void IXmlSerializable.ReadXml(System.Xml.XmlReader reader)
    {
      RawValue = Int32.Parse(reader.ReadInnerXml(), System.Globalization.NumberStyles.HexNumber);
    }
    void IXmlSerializable.WriteXml(System.Xml.XmlWriter writer)
    {
      writer.WriteRaw(RawValue.ToString("X8"));
    }

    static Colour()
    {
      NameBidictionary = new Bidictionary<Inv.Colour, string>();

      var ColourType = typeof(Inv.Colour);
      var ColourInfo = ColourType.GetReflectionInfo();

      var ColourFieldArray = ColourInfo.GetReflectionFields().Where(F => F.IsStatic && F.IsPublic && F.FieldType == ColourType).ToArray();

      var Index = 0;

      All = new Inv.Colour[ColourFieldArray.Length];
      foreach (var Field in ColourFieldArray)
      {
        var Colour = (Inv.Colour)Field.GetValue(null);

        All[Index++] = Colour;

        NameBidictionary.Add(Colour, Field.Name);
      }
    }

    internal static Inv.Colour ConvertHSLToColor(byte Alpha, float Hue, float Saturation, float Lighting)
    {
#if DEBUG
      if (0 > Alpha || 255 < Alpha)
        throw new ArgumentOutOfRangeException("alpha");
      if (0f > Hue || 360f < Hue)
        throw new ArgumentOutOfRangeException("hue");
      if (0f > Saturation || 1f < Saturation)
        throw new ArgumentOutOfRangeException("saturation");
      if (0f > Lighting || 1f < Lighting)
        throw new ArgumentOutOfRangeException("lighting");
#endif
      if (Saturation == 0)
        return Inv.Colour.FromArgb(Alpha, Convert.ToByte(Lighting * 255), Convert.ToByte(Lighting * 255), Convert.ToByte(Lighting * 255));

      float fMax, fMid, fMin;

      if (0.5 < Lighting)
      {
        fMax = Lighting - (Lighting * Saturation) + Saturation;
        fMin = Lighting + (Lighting * Saturation) - Saturation;
      }
      else
      {
        fMax = Lighting + (Lighting * Saturation);
        fMin = Lighting - (Lighting * Saturation);
      }

      var iSextant = (int)Math.Floor(Hue / 60f);

      if (300f <= Hue)
        Hue -= 360f;

      Hue /= 60f;
      Hue -= 2f * (float)Math.Floor(((iSextant + 1f) % 6f) / 2f);

      if (iSextant % 2 == 0)
        fMid = Hue * (fMax - fMin) + fMin;
      else
        fMid = fMin - Hue * (fMax - fMin);

      var iMax = Convert.ToByte(fMax * 255);
      var iMid = Convert.ToByte(fMid * 255);
      var iMin = Convert.ToByte(fMin * 255);

      switch (iSextant)
      {
        case 1:
          return Inv.Colour.FromArgb(Alpha, iMid, iMax, iMin);
        case 2:
          return Inv.Colour.FromArgb(Alpha, iMin, iMax, iMid);
        case 3:
          return Inv.Colour.FromArgb(Alpha, iMin, iMid, iMax);
        case 4:
          return Inv.Colour.FromArgb(Alpha, iMid, iMin, iMax);
        case 5:
          return Inv.Colour.FromArgb(Alpha, iMax, iMin, iMid);
        default:
          return Inv.Colour.FromArgb(Alpha, iMax, iMid, iMin);
      }
    }

    ///<summary>
    ///AliceBlue is F0 F8 FF
    ///</summary>
    public static readonly Inv.Colour AliceBlue = FromArgb(0xFFF0F8FF);
    ///<summary>
    ///AntiqueWhite is FA EB D7
    ///</summary>
    public static readonly Inv.Colour AntiqueWhite = FromArgb(0xFFFAEBD7);
    //public static readonly Inv.Colour Aqua = FromArgb(0xFF00FFFF); // NOTE: duplicate of Cyan
    ///<summary>
    ///Aquamarine is 7F FF D4
    ///</summary>
    public static readonly Inv.Colour Aquamarine = FromArgb(0xFF7FFFD4);
    ///<summary>
    ///Azure is F0 FF FF
    ///</summary>
    public static readonly Inv.Colour Azure = FromArgb(0xFFF0FFFF);
    ///<summary>
    ///Beige is F5 F5 DC
    ///</summary>
    public static readonly Inv.Colour Beige = FromArgb(0xFFF5F5DC);
    ///<summary>
    ///Bisque is FF E4 C4
    ///</summary>
    public static readonly Inv.Colour Bisque = FromArgb(0xFFFFE4C4);
    ///<summary>
    ///Black is 00 00 00
    ///</summary>
    public static readonly Inv.Colour Black = FromArgb(0xFF000000);
    ///<summary>
    ///BlackSmoke is 28 28 28
    ///</summary>
    public static readonly Inv.Colour BlackSmoke = FromArgb(0xFF282828);
    ///<summary>
    ///BlanchedAlmond is FF EB CD
    ///</summary>
    public static readonly Inv.Colour BlanchedAlmond = FromArgb(0xFFFFEBCD);
    ///<summary>
    ///Blue is 00 00 FF
    ///</summary>
    public static readonly Inv.Colour Blue = FromArgb(0xFF0000FF);
    ///<summary>
    ///BlueViolet is 8A 2B E2
    ///</summary>
    public static readonly Inv.Colour BlueViolet = FromArgb(0xFF8A2BE2);
    ///<summary>
    ///Brown is A5 2A 2A
    ///</summary>
    public static readonly Inv.Colour Brown = FromArgb(0xFFA52A2A);
    ///<summary>
    ///BurlyWood is DE B8 87
    ///</summary>
    public static readonly Inv.Colour BurlyWood = FromArgb(0xFFDEB887);
    ///<summary>
    ///CadetBlue is 5F 9E A0
    ///</summary>
    public static readonly Inv.Colour CadetBlue = FromArgb(0xFF5F9EA0);
    ///<summary>
    ///Chartreuse is 7F FF 00
    ///</summary>
    public static readonly Inv.Colour Chartreuse = FromArgb(0xFF7FFF00);
    ///<summary>
    ///Chocolate is D2 69 1E
    ///</summary>
    public static readonly Inv.Colour Chocolate = FromArgb(0xFFD2691E);
    ///<summary>
    ///Coral is FF 7F 50
    ///</summary>
    public static readonly Inv.Colour Coral = FromArgb(0xFFFF7F50);
    ///<summary>
    ///CornflowerBlue is 64 95 ED
    ///</summary>
    public static readonly Inv.Colour CornflowerBlue = FromArgb(0xFF6495ED);
    ///<summary>
    ///Cornsilk is FF F8 DC
    ///</summary>
    public static readonly Inv.Colour Cornsilk = FromArgb(0xFFFFF8DC);
    ///<summary>
    ///Crimson is DC 14 3C
    ///</summary>
    public static readonly Inv.Colour Crimson = FromArgb(0xFFDC143C);
    ///<summary>
    ///Cyan is 00 FF FF
    ///</summary>
    public static readonly Inv.Colour Cyan = FromArgb(0xFF00FFFF);
    ///<summary>
    ///DarkBlue is 00 00 8B
    ///</summary>
    public static readonly Inv.Colour DarkBlue = FromArgb(0xFF00008B);
    ///<summary>
    ///DarkCyan is 00 8B 8B
    ///</summary>
    public static readonly Inv.Colour DarkCyan = FromArgb(0xFF008B8B);
    ///<summary>
    ///DarkGoldenrod is B8 86 0B
    ///</summary>
    public static readonly Inv.Colour DarkGoldenrod = FromArgb(0xFFB8860B);
    ///<summary>
    ///DarkGray is A9 A9 A9
    ///</summary>
    public static readonly Inv.Colour DarkGray = FromArgb(0xFFA9A9A9);
    ///<summary>
    ///DarkGreen is 00 64 00
    ///</summary>
    public static readonly Inv.Colour DarkGreen = FromArgb(0xFF006400);
    ///<summary>
    ///DarkKhaki is BD B7 6B
    ///</summary>
    public static readonly Inv.Colour DarkKhaki = FromArgb(0xFFBDB76B);
    ///<summary>
    ///DarkMagenta is 8B 00 8B
    ///</summary>
    public static readonly Inv.Colour DarkMagenta = FromArgb(0xFF8B008B);
    ///<summary>
    ///DarkOliveGreen is 55 6B 2F
    ///</summary>
    public static readonly Inv.Colour DarkOliveGreen = FromArgb(0xFF556B2F);
    ///<summary>
    ///DarkOrange is FF 8C 00
    ///</summary>
    public static readonly Inv.Colour DarkOrange = FromArgb(0xFFFF8C00);
    ///<summary>
    ///DarkOrchid is 99 32 CC
    ///</summary>
    public static readonly Inv.Colour DarkOrchid = FromArgb(0xFF9932CC);
    ///<summary>
    ///DarkRed is 8B 00 00
    ///</summary>
    public static readonly Inv.Colour DarkRed = FromArgb(0xFF8B0000);
    ///<summary>
    ///DarkSalmon is E9 96 7A
    ///</summary>
    public static readonly Inv.Colour DarkSalmon = FromArgb(0xFFE9967A);
    ///<summary>
    ///DarkSeaGreen is 8F BC 8F
    ///</summary>
    public static readonly Inv.Colour DarkSeaGreen = FromArgb(0xFF8FBC8F);
    ///<summary>
    ///DarkSlateBlue is 48 3D 8B
    ///</summary>
    public static readonly Inv.Colour DarkSlateBlue = FromArgb(0xFF483D8B);
    ///<summary>
    ///DarkSlateGray is 2F 4F 4F
    ///</summary>
    public static readonly Inv.Colour DarkSlateGray = FromArgb(0xFF2F4F4F);
    ///<summary>
    ///DarkTurquoise is 00 CE D1
    ///</summary>
    public static readonly Inv.Colour DarkTurquoise = FromArgb(0xFF00CED1);
    ///<summary>
    ///DarkViolet is 94 00 D3
    ///</summary>
    public static readonly Inv.Colour DarkViolet = FromArgb(0xFF9400D3);
    ///<summary>
    ///DarkYellow is CC CC 00
    ///</summary>
    public static readonly Inv.Colour DarkYellow = FromArgb(0xFFCCCC00);
    ///<summary>
    ///DeepPink is FF 14 93
    ///</summary>
    public static readonly Inv.Colour DeepPink = FromArgb(0xFFFF1493);
    ///<summary>
    ///DeepSkyBlue is 00 BF FF
    ///</summary>
    public static readonly Inv.Colour DeepSkyBlue = FromArgb(0xFF00BFFF);
    ///<summary>
    ///DimGray is 69 69 69
    ///</summary>
    public static readonly Inv.Colour DimGray = FromArgb(0xFF696969);
    ///<summary>
    ///DeepGray is 4B 4B 4B
    ///</summary>
    public static readonly Inv.Colour DeepGray = FromArgb(0xFF4B4B4B);
    ///<summary>
    ///DodgerBlue is 1E 90 FF
    ///</summary>
    public static readonly Inv.Colour DodgerBlue = FromArgb(0xFF1E90FF);
    ///<summary>
    ///Firebrick is B2 22 22
    ///</summary>
    public static readonly Inv.Colour Firebrick = FromArgb(0xFFB22222);
    ///<summary>
    ///FloralWhite is FF FA F0
    ///</summary>
    public static readonly Inv.Colour FloralWhite = FromArgb(0xFFFFFAF0);
    ///<summary>
    ///ForestGreen is 22 8B 22
    ///</summary>
    public static readonly Inv.Colour ForestGreen = FromArgb(0xFF228B22);
    /////<summary>
    /////Fuchsia is FF 00 FF
    /////</summary>
    //public static readonly Inv.Colour Fuchsia = FromArgb(0xFFFF00FF); // NOTE: duplicate of Magenta
    ///<summary>
    ///Gainsboro is DC DC DC
    ///</summary>
    public static readonly Inv.Colour Gainsboro = FromArgb(0xFFDCDCDC);
    ///<summary>
    ///GhostWhite is F8 F8 FF
    ///</summary>
    public static readonly Inv.Colour GhostWhite = FromArgb(0xFFF8F8FF);
    ///<summary>
    ///Gold is FF D7 00
    ///</summary>
    public static readonly Inv.Colour Gold = FromArgb(0xFFFFD700);
    ///<summary>
    ///Goldenrod is DA A5 20
    ///</summary>
    public static readonly Inv.Colour Goldenrod = FromArgb(0xFFDAA520);
    ///<summary>
    ///Gray is 80 80 80
    ///</summary>
    public static readonly Inv.Colour Gray = FromArgb(0xFF808080);
    ///<summary>
    ///GraySmoke is 32 36 3F
    ///</summary>
    public static readonly Inv.Colour GraySmoke = Inv.Colour.FromArgb(0xFF32363F);
    ///<summary>
    ///Green is 00 80 00
    ///</summary>
    public static readonly Inv.Colour Green = FromArgb(0xFF008000);
    ///<summary>
    ///GreenYellow is AD FF 2F
    ///</summary>
    public static readonly Inv.Colour GreenYellow = FromArgb(0xFFADFF2F);
    ///<summary>
    ///Honeydew is F0 FF F0
    ///</summary>
    public static readonly Inv.Colour Honeydew = FromArgb(0xFFF0FFF0);
    ///<summary>
    ///HotPink is FF 69 B4
    ///</summary>
    public static readonly Inv.Colour HotPink = FromArgb(0xFFFF69B4);
    ///<summary>
    ///IndianRed is CD 5C 5C
    ///</summary>
    public static readonly Inv.Colour IndianRed = FromArgb(0xFFCD5C5C);
    ///<summary>
    ///Indigo is 4B 00 82
    ///</summary>
    public static readonly Inv.Colour Indigo = FromArgb(0xFF4B0082);
    ///<summary>
    ///Ivory is FF FF F0
    ///</summary>
    public static readonly Inv.Colour Ivory = FromArgb(0xFFFFFFF0);
    ///<summary>
    ///Khaki is F0 E6 8C
    ///</summary>
    public static readonly Inv.Colour Khaki = FromArgb(0xFFF0E68C);
    ///<summary>
    ///Lavender is E6 E6 FA
    ///</summary>
    public static readonly Inv.Colour Lavender = FromArgb(0xFFE6E6FA);
    ///<summary>
    ///LavenderBlush is FF F0 F5
    ///</summary>
    public static readonly Inv.Colour LavenderBlush = FromArgb(0xFFFFF0F5);
    ///<summary>
    ///LawnGreen is 7C FC 00
    ///</summary>
    public static readonly Inv.Colour LawnGreen = FromArgb(0xFF7CFC00);
    ///<summary>
    ///LemonChiffon is FF FA CD
    ///</summary>
    public static readonly Inv.Colour LemonChiffon = FromArgb(0xFFFFFACD);
    ///<summary>
    ///LightBlue is AD D8 E6
    ///</summary>
    public static readonly Inv.Colour LightBlue = FromArgb(0xFFADD8E6);
    ///<summary>
    ///LightCoral is F0 80 80
    ///</summary>
    public static readonly Inv.Colour LightCoral = FromArgb(0xFFF08080);
    ///<summary>
    ///LightCyan is E0 FF FF
    ///</summary>
    public static readonly Inv.Colour LightCyan = FromArgb(0xFFE0FFFF);
    ///<summary>
    ///LightGoldenrodYellow is FA FA D2
    ///</summary>
    public static readonly Inv.Colour LightGoldenrodYellow = FromArgb(0xFFFAFAD2);
    ///<summary>
    ///LightGray is D3 D3 D3
    ///</summary>
    public static readonly Inv.Colour LightGray = FromArgb(0xFFD3D3D3);
    ///<summary>
    ///LightGreen is 90 EE 90
    ///</summary>
    public static readonly Inv.Colour LightGreen = FromArgb(0xFF90EE90);
    ///<summary>
    ///LightPink is FF B6 C1
    ///</summary>
    public static readonly Inv.Colour LightPink = FromArgb(0xFFFFB6C1);
    ///<summary>
    ///LightSalmon is FF A0 7A
    ///</summary>
    public static readonly Inv.Colour LightSalmon = FromArgb(0xFFFFA07A);
    ///<summary>
    ///LightSeaGreen is 20 B2 AA
    ///</summary>
    public static readonly Inv.Colour LightSeaGreen = FromArgb(0xFF20B2AA);
    ///<summary>
    ///LightSkyBlue is 87 CE FA
    ///</summary>
    public static readonly Inv.Colour LightSkyBlue = FromArgb(0xFF87CEFA);
    ///<summary>
    ///LightSlateGray is 77 88 99
    ///</summary>
    public static readonly Inv.Colour LightSlateGray = FromArgb(0xFF778899);
    ///<summary>
    ///LightSteelBlue is B0 C4 DE
    ///</summary>
    public static readonly Inv.Colour LightSteelBlue = FromArgb(0xFFB0C4DE);
    ///<summary>
    ///LightYellow is FF FF E0
    ///</summary>
    public static readonly Inv.Colour LightYellow = FromArgb(0xFFFFFFE0);
    ///<summary>
    ///Lime is 00 FF 00
    ///</summary>
    public static readonly Inv.Colour Lime = FromArgb(0xFF00FF00);
    ///<summary>
    ///LimeGreen is 32 CD 32
    ///</summary>
    public static readonly Inv.Colour LimeGreen = FromArgb(0xFF32CD32);
    ///<summary>
    ///Linen is FA F0 E6
    ///</summary>
    public static readonly Inv.Colour Linen = FromArgb(0xFFFAF0E6);
    ///<summary>
    ///Magenta is FF 00 FF
    ///</summary>
    public static readonly Inv.Colour Magenta = FromArgb(0xFFFF00FF);
    ///<summary>
    ///Maroon is 80 00 00
    ///</summary>
    public static readonly Inv.Colour Maroon = FromArgb(0xFF800000);
    ///<summary>
    ///MediumAquamarine is 66 CD AA
    ///</summary>
    public static readonly Inv.Colour MediumAquamarine = FromArgb(0xFF66CDAA);
    ///<summary>
    ///MediumBlue is 00 00 CD
    ///</summary>
    public static readonly Inv.Colour MediumBlue = FromArgb(0xFF0000CD);
    ///<summary>
    ///MediumOrchid is BA 55 D3
    ///</summary>
    public static readonly Inv.Colour MediumOrchid = FromArgb(0xFFBA55D3);
    ///<summary>
    ///MediumPurple is 93 70 DB
    ///</summary>
    public static readonly Inv.Colour MediumPurple = FromArgb(0xFF9370DB);
    ///<summary>
    ///MediumSeaGreen is 3C B3 71
    ///</summary>
    public static readonly Inv.Colour MediumSeaGreen = FromArgb(0xFF3CB371);
    ///<summary>
    ///MediumSlateBlue is 7B 68 EE
    ///</summary>
    public static readonly Inv.Colour MediumSlateBlue = FromArgb(0xFF7B68EE);
    ///<summary>
    ///MediumSpringGreen is 00 FA 9A
    ///</summary>
    public static readonly Inv.Colour MediumSpringGreen = FromArgb(0xFF00FA9A);
    ///<summary>
    ///MediumTurquoise is 48 D1 CC
    ///</summary>
    public static readonly Inv.Colour MediumTurquoise = FromArgb(0xFF48D1CC);
    ///<summary>
    ///MediumVioletRed is C7 15 85
    ///</summary>
    public static readonly Inv.Colour MediumVioletRed = FromArgb(0xFFC71585);
    ///<summary>
    ///MidnightBlue is 19 19 70
    ///</summary>
    public static readonly Inv.Colour MidnightBlue = FromArgb(0xFF191970);
    ///<summary>
    ///MintCream is F5 FF FA
    ///</summary>
    public static readonly Inv.Colour MintCream = FromArgb(0xFFF5FFFA);
    ///<summary>
    ///MistyRose is FF E4 E1
    ///</summary>
    public static readonly Inv.Colour MistyRose = FromArgb(0xFFFFE4E1);
    ///<summary>
    ///Moccasin is FF E4 B5
    ///</summary>
    public static readonly Inv.Colour Moccasin = FromArgb(0xFFFFE4B5);
    ///<summary>
    ///NavajoWhite is FF DE AD
    ///</summary>
    public static readonly Inv.Colour NavajoWhite = FromArgb(0xFFFFDEAD);
    ///<summary>
    ///Navy is 00 00 80
    ///</summary>
    public static readonly Inv.Colour Navy = FromArgb(0xFF000080);
    ///<summary>
    ///OldLace is FD F5 E6
    ///</summary>
    public static readonly Inv.Colour OldLace = FromArgb(0xFFFDF5E6);
    ///<summary>
    ///Olive is 80 80 00
    ///</summary>
    public static readonly Inv.Colour Olive = FromArgb(0xFF808000);
    ///<summary>
    ///OliveDrab is 6B 8E 23
    ///</summary>
    public static readonly Inv.Colour OliveDrab = FromArgb(0xFF6B8E23);
    ///<summary>
    ///Orange is FF A5 00
    ///</summary>
    public static readonly Inv.Colour Orange = FromArgb(0xFFFFA500);
    ///<summary>
    ///OrangeRed is FF 45 00
    ///</summary>
    public static readonly Inv.Colour OrangeRed = FromArgb(0xFFFF4500);
    ///<summary>
    ///Orchid is DA 70 D6
    ///</summary>
    public static readonly Inv.Colour Orchid = FromArgb(0xFFDA70D6);
    ///<summary>
    ///PaleGoldenrod is EE E8 AA
    ///</summary>
    public static readonly Inv.Colour PaleGoldenrod = FromArgb(0xFFEEE8AA);
    ///<summary>
    ///PaleGreen is 98 FB 98
    ///</summary>
    public static readonly Inv.Colour PaleGreen = FromArgb(0xFF98FB98);
    ///<summary>
    ///PaleTurquoise is AF EE EE
    ///</summary>
    public static readonly Inv.Colour PaleTurquoise = FromArgb(0xFFAFEEEE);
    ///<summary>
    ///PaleVioletRed is DB 70 93
    ///</summary>
    public static readonly Inv.Colour PaleVioletRed = FromArgb(0xFFDB7093);
    ///<summary>
    ///PapayaWhip is FF EF D5
    ///</summary>
    public static readonly Inv.Colour PapayaWhip = FromArgb(0xFFFFEFD5);
    ///<summary>
    ///PeachPuff is FF DA B9
    ///</summary>
    public static readonly Inv.Colour PeachPuff = FromArgb(0xFFFFDAB9);
    ///<summary>
    ///Peru is CD 85 3F
    ///</summary>
    public static readonly Inv.Colour Peru = FromArgb(0xFFCD853F);
    ///<summary>
    ///Pink is FF C0 CB
    ///</summary>
    public static readonly Inv.Colour Pink = FromArgb(0xFFFFC0CB);
    ///<summary>
    ///Plum is DD A0 DD
    ///</summary>
    public static readonly Inv.Colour Plum = FromArgb(0xFFDDA0DD);
    ///<summary>
    ///PowderBlue is B0 E0 E6
    ///</summary>
    public static readonly Inv.Colour PowderBlue = FromArgb(0xFFB0E0E6);
    ///<summary>
    ///Purple is 80 00 80
    ///</summary>
    public static readonly Inv.Colour Purple = FromArgb(0xFF800080);
    ///<summary>
    ///Red is FF 00 00
    ///</summary>
    public static readonly Inv.Colour Red = FromArgb(0xFFFF0000);
    ///<summary>
    ///RosyBrown is BC 8F 8F
    ///</summary>
    public static readonly Inv.Colour RosyBrown = FromArgb(0xFFBC8F8F);
    ///<summary>
    ///RoyalBlue is 41 69 E1
    ///</summary>
    public static readonly Inv.Colour RoyalBlue = FromArgb(0xFF4169E1);
    ///<summary>
    ///SaddleBrown is 8B 45 13
    ///</summary>
    public static readonly Inv.Colour SaddleBrown = FromArgb(0xFF8B4513);
    ///<summary>
    ///Salmon is FA 80 72
    ///</summary>
    public static readonly Inv.Colour Salmon = FromArgb(0xFFFA8072);
    ///<summary>
    ///SandyBrown is F4 A4 60
    ///</summary>
    public static readonly Inv.Colour SandyBrown = FromArgb(0xFFF4A460);
    ///<summary>
    ///SeaGreen is 2E 8B 57
    ///</summary>
    public static readonly Inv.Colour SeaGreen = FromArgb(0xFF2E8B57);
    ///<summary>
    ///SeaShell is FF F5 EE
    ///</summary>
    public static readonly Inv.Colour SeaShell = FromArgb(0xFFFFF5EE);
    ///<summary>
    ///Sienna is A0 52 2D
    ///</summary>
    public static readonly Inv.Colour Sienna = FromArgb(0xFFA0522D);
    ///<summary>
    ///Silver is C0 C0 C0
    ///</summary>
    public static readonly Inv.Colour Silver = FromArgb(0xFFC0C0C0);
    ///<summary>
    ///SkyBlue is 87 CE EB
    ///</summary>
    public static readonly Inv.Colour SkyBlue = FromArgb(0xFF87CEEB);
    ///<summary>
    ///SlateBlue is 6A 5A CD
    ///</summary>
    public static readonly Inv.Colour SlateBlue = FromArgb(0xFF6A5ACD);
    ///<summary>
    ///SlateGray is 70 80 90
    ///</summary>
    public static readonly Inv.Colour SlateGray = FromArgb(0xFF708090);
    ///<summary>
    ///Snow is FF FA FA
    ///</summary>
    public static readonly Inv.Colour Snow = FromArgb(0xFFFFFAFA);
    ///<summary>
    ///SpringGreen is 00 FF 7F
    ///</summary>
    public static readonly Inv.Colour SpringGreen = FromArgb(0xFF00FF7F);
    ///<summary>
    ///SteelBlue is 46 82 B4
    ///</summary>
    public static readonly Inv.Colour SteelBlue = FromArgb(0xFF4682B4);
    ///<summary>
    ///Tan is D2 B4 8C
    ///</summary>
    public static readonly Inv.Colour Tan = FromArgb(0xFFD2B48C);
    ///<summary>
    ///Teal is 00 80 80
    ///</summary>
    public static readonly Inv.Colour Teal = FromArgb(0xFF008080);
    ///<summary>
    ///Thistle is D8 BF D8
    ///</summary>
    public static readonly Inv.Colour Thistle = FromArgb(0xFFD8BFD8);
    ///<summary>
    ///Tomato is FF 63 47
    ///</summary>
    public static readonly Inv.Colour Tomato = FromArgb(0xFFFF6347);
    ///<summary>
    ///Transparent is FF FF FF
    ///</summary>
    public static readonly Inv.Colour Transparent = FromArgb(0x00FFFFFF);
    ///<summary>
    ///Turquoise is 40 E0 D0
    ///</summary>
    public static readonly Inv.Colour Turquoise = FromArgb(0xFF40E0D0);
    ///<summary>
    ///Violet is EE 82 EE
    ///</summary>
    public static readonly Inv.Colour Violet = FromArgb(0xFFEE82EE);
    ///<summary>
    ///Wheat is F5 DE B3
    ///</summary>
    public static readonly Inv.Colour Wheat = FromArgb(0xFFF5DEB3);
    ///<summary>
    ///White is FF FF FF
    ///</summary>
    public static readonly Inv.Colour White = FromArgb(0xFFFFFFFF);
    ///<summary>
    ///WhiteSmoke is F5 F5 F5
    ///</summary>
    public static readonly Inv.Colour WhiteSmoke = FromArgb(0xFFF5F5F5);
    ///<summary>
    ///Yellow is FF FF 00
    ///</summary>
    public static readonly Inv.Colour Yellow = FromArgb(0xFFFFFF00);
    ///<summary>
    ///YellowGreen is 9A CD 32
    ///</summary>
    public static readonly Inv.Colour YellowGreen = FromArgb(0xFF9ACD32);
    /// <summary>
    /// List of all registered colours.
    /// </summary>
    public static readonly Inv.Colour[] All;

    private static readonly Bidictionary<Inv.Colour, string> NameBidictionary;
  }

  /// <summary>
  /// HSL representation.
  /// </summary>
  public struct ColourHSLRecord
  {
    /// <summary>
    /// Hue
    /// </summary>
    public float H;
    /// <summary>
    /// Saturation
    /// </summary>
    public float S;
    /// <summary>
    /// Lightness
    /// </summary>
    public float L;
  }

  /// <summary>
  /// ARGBA representation.
  /// </summary>
  [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Explicit)]
  public struct ColourARGBRecord
  {
    /// <summary>
    /// Raw value.
    /// </summary>
    [System.Runtime.InteropServices.FieldOffset(0)]
    public Int32 Argb;
    /// <summary>
    /// Alpha
    /// </summary>
    [System.Runtime.InteropServices.FieldOffset(3)]
    public byte A;
    /// <summary>
    /// Red
    /// </summary>
    [System.Runtime.InteropServices.FieldOffset(2)]
    public byte R;
    /// <summary>
    /// Green
    /// </summary>
    [System.Runtime.InteropServices.FieldOffset(1)]
    public byte G;
    /// <summary>
    /// Blue
    /// </summary>
    [System.Runtime.InteropServices.FieldOffset(0)]
    public byte B;
  }

  // NOTE: this has to be a separate class to Inv.Colour due to the static constructed properties.
  internal static class ColourFactory
  {
    static ColourFactory()
    {
      Dictionary = new Dictionary<int, Colour>();
    }

    public static long MemorySize()
    {
      lock (Dictionary)
        return Dictionary.Count * 16;
    }

    public static Inv.Colour Resolve(int Argb)
    {
      lock (Dictionary)
        return Dictionary.GetOrAdd(Argb, K => new Inv.Colour(K));
    }

    private static readonly Dictionary<int, Inv.Colour> Dictionary;
  }
}