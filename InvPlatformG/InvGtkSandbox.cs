﻿using System;
using Gtk;

namespace Inv
{
  public static class GtkSandbox
  {
    public static void Main (string[] args)
    {
      //InvMainOverlay();
      GtkAlignment();
    }

    private static void GtkMain(Action<GtkWindow> Action)
    {
      Gtk.Application.Init();
      var win = new GtkWindow();

      Action(win);

      win.Hidden += (Sender, Event) => Gtk.Application.Quit();
      win.Show();
      Gtk.Application.Run();
    }

    static void DrawRoundedRectangle(Cairo.Context gr, double x, double y, double width, double height, double TopLeft, double TopRight, double BottomLeft, double BottomRight, double BorderLeft, double BorderTop, double BorderRight, double BorderBottom)
    {
      gr.Save();

      if ((TopLeft > height / 2) || (TopLeft > width / 2))
        TopLeft = Math.Min(height / 2, width / 2);

      gr.LineWidth = BorderTop;
      gr.MoveTo(x, y + TopLeft);
      gr.Arc(x + TopLeft, y + TopLeft, TopLeft, Math.PI, -Math.PI / 2);
      gr.LineTo(x + width - TopRight, y);
      gr.Stroke();

      gr.LineWidth = BorderRight;
      gr.Arc(x + width - TopRight, y + TopRight, TopRight, -Math.PI / 2, 0);
      gr.LineTo(x + width, y + height - BottomRight);
      gr.Stroke();

      gr.LineWidth = BorderBottom;
      gr.Arc(x + width - BottomRight, y + height - BottomRight, BottomRight, 0, Math.PI / 2);
      gr.LineTo(x + BottomLeft, y + height);
      gr.Stroke();

      gr.LineWidth = BorderLeft;
      gr.Arc(x + BottomLeft, y + height - BottomLeft, BottomLeft, Math.PI / 2, Math.PI);
      gr.LineTo(x, y + TopLeft);
      gr.Stroke();
      
      gr.Restore();
    }

    private static void GtkAlignment()
    {
      GtkMain(win =>
      {
        win.SetSizeRequest(1000, 1000);
        win.OverrideBackgroundColor(StateFlags.Normal, new Gdk.RGBA() { Alpha = 1.0, Red = 0.0, Green = 0.0, Blue = 0.0 });

        //var EventBox = new Gtk.EventBox();
        //win.Child = EventBox;
        //EventBox.OverrideBackgroundColor(StateFlags.Normal, new Gdk.RGBA() { Alpha = 1.0, Red = 0.5, Green = 0.5, Blue = 0.5 });

        var Alignment = new Gtk.Alignment(0.5F, 0.5F, 0.0F, 0.0F);
        win.Child = Alignment;
        Alignment.Drawn += (Sender, Event) =>
        {
          //Event.Cr.SetSourceRGBA(0.5, 0.5, 0.5, 1.0);
          //Event.Cr.Rectangle(0, 0, Alignment.AllocatedWidth, Alignment.AllocatedHeight);
          //Event.Cr.Fill();

          Event.Cr.SetSourceRGBA(1.0, 1.0, 1.0, 1.0);
          DrawRoundedRectangle(Event.Cr, 50, 50, 200, 200, 0, 10, 30, 50, 1, 1, 1, 1);

          //Event.Cr.Rectangle(50, 50, 200, 200);
          //Event.Cr.SetSourceRGBA(1.0, 1.0, 1.0, 1.0);
          //Event.Cr.Stroke();
          //
          //Event.Cr.Rectangle(50, 50, 200, 200);
          //Event.Cr.SetSourceRGBA(1.0, 0.5, 1.0, 1.0);
          //Event.Cr.Fill();

          //Event.Cr.CurveTo(40, 40, 20, 60, 60, 80);
          //Event.Cr.SetSourceRGBA(1.0, 1.0, 1.0, 1.0);
          //Event.Cr.Stroke();
        };
        Alignment.Show();

        var Label = new Gtk.Label();
        Alignment.Child = Label;
        Label.Text = "HELLO WORLD";
        Label.OverrideBackgroundColor(StateFlags.Normal, new Gdk.RGBA() { Alpha = 1.0, Red = 0.0, Green = 1.0, Blue = 0.0 });
        Label.OverrideColor(StateFlags.Normal, new Gdk.RGBA() { Alpha = 1.0, Red = 1.0, Green = 1.0, Blue = 1.0 });
        //Label.SetPadding(10, 10);
        //Label.SetAlignment(0.5F, 0.5F);
        Label.Show();
        
        //var Alignment = new Gtk.Alignment(0.5F, 0.5F, 1.0F, 1.0F);
        //win.Child = Alignment;
        //Alignment.Show();
      });
    }
    private static void GtkButton()
    {
      GtkMain(win =>
      {
        //win.ModifyBg(StateType.Normal, new Gdk.Color(0, 255, 0));
        win.BorderWidth = 10;

        var Button = new GtkButton();
        win.Child = Button;
        Button.SetSizeRequest(200, 200);
        Button.Set(0.0F, 0.0F, 1.0F, 1.0F);
        //Button.SetBackgroundColor(new Gdk.Color(255, 0, 0));

        var Label = new GtkLabel();
        Button.Child = Label;
        //Label.Label.SetBackgroundColor(new Gdk.Color(0, 0, 255));
        Label.Label.Text = "HELLO";
      });
    }
    private static void InvMainOverlay()
    {
      var InvApplication = new Inv.Application();
      InvApplication.StartEvent += () =>
      {
        InvApplication.Window.Background.Colour = Inv.Colour.WhiteSmoke;

        var InvSurface = InvApplication.Window.NewSurface();
        InvSurface.Background.Colour = Inv.Colour.DarkGreen;

        var InvOverlay = InvSurface.NewOverlay();
        InvSurface.Content = InvOverlay;
        InvOverlay.Background.Colour = Inv.Colour.DarkGray;

        var InvLabel1 = InvSurface.NewLabel();
        InvOverlay.AddPanel(InvLabel1);
        InvLabel1.Alignment.TopStretch();
        InvLabel1.Font.Colour = Inv.Colour.White;
        InvLabel1.Text = "LABEL 1";

        var InvLabel2 = InvSurface.NewLabel();
        InvOverlay.AddPanel(InvLabel2);
        InvLabel2.Alignment.BottomStretch();
        InvLabel2.Font.Colour = Inv.Colour.White;
        InvLabel2.Text = "LABEL 2";

        InvApplication.Window.Transition(InvSurface);
      };
      Inv.GtkShell.Run(InvApplication); 
    }
    private static void InvMainStack()
    {
      var InvApplication = new Inv.Application();
      InvApplication.StartEvent += () =>
      {
        InvApplication.Window.Background.Colour = Inv.Colour.DimGray;

        var InvSurface = InvApplication.Window.NewSurface();
        InvSurface.Background.Colour = Inv.Colour.DarkGreen;

        var InvScroll = InvSurface.NewVerticalScroll();
        InvSurface.Content = InvScroll;

        var InvStack = InvSurface.NewVerticalStack();
        InvScroll.Content = InvStack;
        InvStack.Background.Colour = Inv.Colour.White;
        InvStack.Alignment.CenterRight();
        //InvStack.Margin.Set(20);
        //InvStack.Padding.Set(20);

        var InvLabel = InvSurface.NewLabel();
        InvStack.AddPanel(InvLabel);
        InvLabel.Border.Set(20, 5, 40, 0);
        InvLabel.Border.Colour = Inv.Colour.Orange;
        InvLabel.Background.Colour = Inv.Colour.Blue;
        InvLabel.Text = "Hello Gtk " + InvApplication.Window.Width + " x " + InvApplication.Window.Height;
        InvLabel.Font.Size = 30;
        InvLabel.Font.Heavy();
        InvLabel.Font.Colour = Inv.Colour.Yellow;
        //InvLabel.Margin.Set(20);
        //InvLabel.Padding.Set(20);

        var InvEdit = InvSurface.NewTextEdit();
        InvStack.AddPanel(InvEdit);
        InvEdit.Background.Colour = Inv.Colour.Pink;
        InvEdit.Text = "HELLO TEXTY";
        InvEdit.Font.Size = 40;
        InvEdit.Font.Light();
        InvEdit.Font.Colour = Inv.Colour.DarkGoldenrod;

        var InvMemo = InvSurface.NewMemo();
        InvStack.AddPanel(InvMemo);
        InvMemo.Background.Colour = Inv.Colour.Turquoise;
        InvMemo.Padding.Set(20);
        InvMemo.Text = "HELLO TEXTY" + Environment.NewLine + "MULTI LINE";
        InvMemo.Font.Size = 20;
        InvMemo.Font.Light();
        InvMemo.Font.Colour = Inv.Colour.BlueViolet;

        var InvGraphic = InvSurface.NewGraphic();
        InvStack.AddPanel(InvGraphic);
        InvGraphic.Image = new Inv.Image(System.IO.File.ReadAllBytes(@"C:\Hole\Pictures\Headshot.png"), ".png");

        InvApplication.Window.Transition(InvSurface);
      };
      Inv.GtkShell.Run(InvApplication); 
    }
  }
}