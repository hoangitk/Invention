﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.ComponentModel;
using Inv.Support;

namespace Inv
{
  // TODO: serialise any object, not just a string.

  public sealed class TaskWindow : IDisposable
  {
    public static TaskWindow Create(string Class)
    {
      Debug.Assert(Class != null, "Class must be specified.");

      var Window = new TaskWindow(Class);

      Window.WndClass = new Win32.User32.WNDCLASS()
      {
        lpszClassName = Class,
        lpfnWndProc = Window.WndProc // NOTE: this delegate variable must be retained so it is not garbage collected prematurely.
      };

      var WndResult = Win32.User32.RegisterClassW(ref Window.WndClass);
      if (WndResult == 0)
        throw new Win32Exception();

      var WndHandle = Win32.User32.CreateWindowExW(0, Class, string.Empty, 0, 0, 0, 0, 0, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero);
      if (WndHandle == IntPtr.Zero)
        throw new Win32Exception();

      Window.IsOwned = true;
      Window.Handle = WndHandle;

      return Window;
    }
    public static TaskWindow Find(string Class)
    {
      var Handle = Win32.User32.FindWindow(Class, null);

      if (Handle == IntPtr.Zero)
      {
        if (Marshal.GetLastWin32Error() != 0)
          throw new Win32Exception();

        return null;
      }
      else
      {
        return new TaskWindow(Class)
        {
          Handle = Handle,
          IsOwned = false
        };
      }
    }

    private TaskWindow(string Class)
    {
      this.Class = Class;
    }
    ~TaskWindow()
    {
      Dispose();
    }
    public void Dispose()
    {
      if (IsOwned && Handle != IntPtr.Zero)
      {
        Win32.User32.DestroyWindow(Handle);

        var WndResult = Win32.User32.UnregisterClass(Class, IntPtr.Zero);
        Debug.Assert(WndResult, "Class was not registered.");
        
        this.IsOwned = false;
        this.Handle = IntPtr.Zero;
      }

      GC.SuppressFinalize(this);
    }

    public string Class { get; private set; }
    public event Action<TextWindowMessage> ReceiveTextMessageEvent;

    public void SendTextMessage(string Value)
    {
      var sarr = System.Text.Encoding.Default.GetBytes(Value);
      var len = sarr.Length;

      Win32.User32.COPYDATASTRUCT cds;

      cds.dwData = (IntPtr)100;
      cds.lpData = Value;
      cds.cbData = len + 1;

      if (Win32.User32.SendMessage(Handle, Win32.User32.WM_COPYDATA, IntPtr.Zero, ref cds) != IntPtr.Zero)
        throw new Win32Exception();
    }

    private IntPtr WndProc(IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam)
    {
      switch (msg)
      {
        case Win32.User32.WM_COPYDATA:
          if (ReceiveTextMessageEvent != null)
          {
            var CopyData = (Win32.User32.COPYDATASTRUCT)Marshal.PtrToStructure(lParam, typeof(Win32.User32.COPYDATASTRUCT));

            ReceiveTextMessageEvent(new TextWindowMessage(CopyData.lpData));
          }
          break;
      }

      return Win32.User32.DefWindowProcW(hWnd, msg, wParam, lParam);
    }

    private IntPtr Handle;
    private bool IsOwned;
    private Win32.User32.WNDCLASS WndClass;
  }

  public sealed class TextWindowMessage
  {
    internal TextWindowMessage(string Value)
    {
      this.Value = Value;
    }

    public string Value { get; set; }
  }
}