﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
#if WINDOWS_UWP
using Windows.Networking;
using Windows.Networking.Sockets;
#else
using System.Net;
using System.Net.Sockets;
#endif
using Inv.Support;

namespace Inv.Tcp
{
#if WINDOWS_UWP
  internal sealed class Client
  {
    public Client(string Host, int Port, byte[] CertHash)
    {
      this.Host = Host;
      this.Port = Port;
      this.CertHash = CertHash;
    }

    public bool IsActive { get; private set; }
    public System.IO.Stream InputStream
    {
      get { return StreamSocket.InputStream.AsStreamForRead(0); } // zero disables buffering.
    }
    public System.IO.Stream OutputStream
    {
      get { return StreamSocket.OutputStream.AsStreamForWrite(0); } // zero disables buffering.
    }

    public void Connect()
    {
      if (!IsActive)
      {
        this.IsActive = true;

        try
        {
          this.StreamSocket = new StreamSocket();
          //StreamSocket.Control.NoDelay = true;
          //StreamSocket.Control.KeepAlive = true;
          // NOTE: ignore certificate errors because we are using self-signed certificates for SSL.
          StreamSocket.Control.IgnorableServerCertificateErrors.Add(Windows.Security.Cryptography.Certificates.ChainValidationResult.Untrusted);
          StreamSocket.Control.IgnorableServerCertificateErrors.Add(Windows.Security.Cryptography.Certificates.ChainValidationResult.InvalidName);
          StreamSocket.ConnectAsync(new HostName(Host), Port.ToString(), CertHash == null ? SocketProtectionLevel.PlainSocket : SocketProtectionLevel.Tls10).AsTask().Wait();
        }
        catch
        {
          StreamSocket.Dispose();
          this.StreamSocket = null;

          throw;
        }
      }
    }
    public void Disconnect()
    {
      if (IsActive)
      {
        this.IsActive = false;

        if (StreamSocket != null)
        {
          StreamSocket.Dispose();
          this.StreamSocket = null;
        }
      }
    }

    private string Host;
    private int Port;
    private byte[] CertHash;
    private StreamSocket StreamSocket;
  }

  internal sealed class Server
  {
    public Server(string Host, int Port, byte[] CertHash)
    {
      this.Host = Host;
      this.Port = Port;
      this.CertHash = CertHash;
      this.ChannelList = new DistinctList<Channel>();
    }

    public event Action<Channel> AcceptEvent;
    public event Action<Channel> RejectEvent;

    public void Connect()
    {
      if (TcpServer == null)
      {
        this.TcpServer = new StreamSocketListener();
        TcpServer.ConnectionReceived += (Sender, Event) =>
        {
          var Channel = new Channel(this, Event.Socket);

          lock (ChannelList)
            ChannelList.Add(Channel);

          if (AcceptEvent != null)
            AcceptEvent(Channel);
        };
        TcpServer.BindServiceNameAsync(Port.ToString(), SocketProtectionLevel.PlainSocket).AsTask().Wait();
      }
    }
    public void Disconnect()
    {
      if (TcpServer != null)
      {
        TcpServer.Dispose();
        TcpServer = null;
      }

      foreach (var Channel in ChannelList)
      {
        try
        {
          Channel.Drop();
        }
        catch
        {
        }
      }

      ChannelList.Clear();
    }

    internal void CloseChannel(Channel Channel)
    {
      try
      {
        Channel.ClosedInvoke();

        if (RejectEvent != null)
          RejectEvent(Channel);
      }
      finally
      {
        Channel.Drop();

        lock (ChannelList)
          ChannelList.Remove(Channel);
      }
    }

    private StreamSocketListener TcpServer;
    private string Host;
    private int Port;
    private byte[] CertHash;
    private Inv.DistinctList<Channel> ChannelList;
  }

  internal sealed class Channel
  {
    internal Channel(Server Server, StreamSocket StreamSocket)
    {
      this.Server = Server;
      this.StreamSocket = StreamSocket;
      this.IsActive = true;

      //StreamSocket.Control.NoDelay = true;
      //StreamSocket.Control.KeepAlive = true;
    }

    public Server Server { get; private set; }
    public bool IsActive { get; private set; }
    public System.IO.Stream InputStream
    {
      get { return StreamSocket.InputStream.AsStreamForRead(0); } // zero disables buffering.
    }
    public System.IO.Stream OutputStream
    {
      get { return StreamSocket.OutputStream.AsStreamForWrite(0); } // zero disables buffering.
    }
    public event Action ClosedEvent;

    public void Drop()
    {
      this.IsActive = false;

      if (StreamSocket != null)
      {
        this.StreamSocket.Dispose();
        this.StreamSocket = null;
      }
    }

    internal void ClosedInvoke()
    {
      if (ClosedEvent != null)
        ClosedEvent();
    }

    private StreamSocket StreamSocket;
  }

  internal sealed class FlushWriteStream : System.IO.Stream
  {
    public FlushWriteStream(System.IO.Stream Base)
    {
      this.Base = Base;
    }

    public override bool CanRead
    {
      get { return Base.CanRead; }
    }
    public override bool CanSeek
    {
      get { return Base.CanSeek; }
    }
    public override bool CanWrite
    {
      get { return Base.CanWrite; }
    }
    public override void Flush()
    {
      Base.Flush();
    }
    public override long Length
    {
      get { return Base.Length; }
    }
    public override long Position
    {
      get { return Base.Position; }
      set { Base.Position = value; }
    }
    public override int Read(byte[] buffer, int offset, int count)
    {
      return Base.Read(buffer, offset, count);
    }
    public override long Seek(long offset, SeekOrigin origin)
    {
      return Base.Seek(offset, origin);
    }
    public override void SetLength(long value)
    {
      Base.SetLength(value);
    }
    public override void Write(byte[] buffer, int offset, int count)
    {
      Base.Write(buffer, offset, count);
      Base.Flush();
    }

    private Stream Base;
  }
#else
  internal sealed class Client
  {
    public Client(string Host, int Port, byte[] CertHash)
    {
      this.Host = Host;
      this.Port = Port;
      this.CertHash = CertHash;
      this.TcpClient = new System.Net.Sockets.TcpClient();
      TcpClient.SendBufferSize = TransportFoundation.SendBufferSize;
      TcpClient.ReceiveBufferSize = TransportFoundation.ReceiveBufferSize;
      TcpClient.NoDelay = true;
    }

    public bool IsActive { get; private set; }
    public System.IO.Stream Stream
    {
      get { return (System.IO.Stream)SecureStream ?? (System.IO.Stream)NetworkStream; }
    }

    public void Connect()
    {
      if (!IsActive)
      {
        this.IsActive = true;

        try
        {
          var Result = TcpClient.BeginConnect(Host, Port, null, null);
          if (!Result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(5), false))
          {
            TcpClient.Close();
            throw new TimeoutException();
          }
          TcpClient.EndConnect(Result);

          this.NetworkStream = TcpClient.GetStream();

          if (CertHash != null)
          {
            this.SecureStream = new System.Net.Security.SslStream(NetworkStream, true, (sender, cert, chain, sslPolicy) =>
            {
              // NOTE: ignore certificate errors because we are using self-signed certificates for SSL.
              return cert.GetCertHash().ShallowEqualTo(CertHash);
            });
            SecureStream.AuthenticateAsClient(Host, null, System.Security.Authentication.SslProtocols.Tls12, false);
            //SecureStream.ReadTimeout = 5000;
            //SecureStream.WriteTimeout = 5000;
          }
        }
        catch (Exception Exception)
        {
          TcpClient.Close();

          throw new Exception(Exception.Message + " (" + Host + ":" + Port + ")", Exception.Preserve());
        }
      }
    }
    public void Disconnect()
    {
      if (IsActive)
      {
        this.IsActive = false;

        if (SecureStream != null)
        {
          try
          {
            SecureStream.Dispose();
          }
          catch
          {
            // TODO: is this allowed?
          }

          this.SecureStream = null;
        }

        if (NetworkStream != null)
        {
          try
          {
            NetworkStream.Dispose();
          }
          catch
          {
            // TODO: is this allowed?
          }

          this.NetworkStream = null;
        }

        try
        {
          TcpClient.Close();
        }
        catch
        {
          // TODO: is this allowed?
        }
      }
    }

    private string Host;
    private int Port;
    private byte[] CertHash;
    private System.Net.Sockets.TcpClient TcpClient;
    private System.Net.Security.SslStream SecureStream;
    private System.Net.Sockets.NetworkStream NetworkStream;
  }

  internal sealed class Server
  {
    public Server(string Host, int Port, byte[] CertHash)
    {
      this.Host = Host;
      this.Port = Port;
      this.CertHash = CertHash;
      this.ChannelList = new Inv.DistinctList<Channel>();

      if (CertHash != null)
      {
        foreach (var StoreLocation in new[] { System.Security.Cryptography.X509Certificates.StoreLocation.CurrentUser, System.Security.Cryptography.X509Certificates.StoreLocation.LocalMachine })
        {
          var Store = new System.Security.Cryptography.X509Certificates.X509Store(System.Security.Cryptography.X509Certificates.StoreName.My, StoreLocation);
          Store.Open(System.Security.Cryptography.X509Certificates.OpenFlags.ReadOnly);
          try
          {
            foreach (var Cert in Store.Certificates)
            {
              if (Cert.GetCertHash().ShallowEqualTo(CertHash))
              {
                this.Certificate = Cert;
                break;
              }
            }
          }
          finally
          {
            Store.Close();
          }

          if (this.Certificate != null)
            break;
        }

        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(Certificate == null || Certificate.GetCertHash().ShallowEqualTo(CertHash), "CertHash mismatch.");
      }
    }

    public bool IsActive { get; private set; }
    public string Host { get; private set; }
    public int Port { get; private set; }
    public byte[] CertHash { get; private set; }
    public System.Security.Cryptography.X509Certificates.X509Certificate2 Certificate { get; private set; }
    public event Action StartEvent;
    public event Action StopEvent;
    public event Action<Exception> FaultEvent;
    public event Action<Channel> AcceptEvent;
    public event Action<Channel> RejectEvent;

    public void Connect()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Port != 0, "Port must be specified.");

      if (!IsActive)
      {
        this.TcpServer = new System.Net.Sockets.TcpListener(System.Net.IPAddress.Any, Port);
        TcpServer.Start();
        TcpServer.BeginAcceptTcpClient(AcceptTcpClient, null);

        this.IsActive = true;

        if (StartEvent != null)
          StartEvent();
      }
    }
    public void Disconnect()
    {
      if (IsActive)
      {
        this.IsActive = false;

        if (StopEvent != null)
          StopEvent();

        if (TcpServer != null)
        {
          TcpServer.Stop();
          this.TcpServer = null;
        }

        foreach (var Channel in ChannelList)
        {
          try
          {
            Channel.Drop();
          }
          catch
          {
          }
        }

        ChannelList.Clear();
      }
    }

    internal void CloseChannel(Channel Channel)
    {
      try
      {
        Channel.ClosedInvoke();

        if (RejectEvent != null)
          RejectEvent(Channel);
      }
      finally
      {
        Channel.Drop();

        lock (ChannelList)
          ChannelList.Remove(Channel);
      }
    }

    private void HandleFault(Exception Exception)
    {
      try
      {
        if (FaultEvent != null)
          FaultEvent(Exception);
      }
      catch
      {
        if (Debugger.IsAttached)
          Debugger.Break();
      }
    }
    private void AcceptTcpClient(IAsyncResult Result)
    {
      try
      {
        // immediately wait for the next client.
        if (IsActive)
        {
          TcpServer.BeginAcceptTcpClient(AcceptTcpClient, null);

          var TcpClient = EndAcceptTcpClient(Result);

          if (TcpClient != null)
          {
            TcpClient.SendBufferSize = Inv.TransportFoundation.SendBufferSize;
            TcpClient.ReceiveBufferSize = Inv.TransportFoundation.ReceiveBufferSize;
            TcpClient.NoDelay = true;

            var Channel = new Channel(this, TcpClient);

            lock (ChannelList)
              ChannelList.Add(Channel);

            if (AcceptEvent != null)
              AcceptEvent(Channel);
          }
        }
      }
      catch (Exception Exception)
      {
        HandleFault(Exception);
      }
    }
    [DebuggerNonUserCode]
    private TcpClient EndAcceptTcpClient(IAsyncResult Result)
    {
      try
      {
        return TcpServer.EndAcceptTcpClient(Result);
      }
      catch (System.Net.Sockets.SocketException Exception)
      {
        // A blocking operation was interrupted by a call to WSACancelBlockingCall"
        if (Exception.SocketErrorCode == SocketError.Interrupted && Exception.ErrorCode == 10004)
          return null;
        else
          throw Exception.Preserve();
      }
    }

    private System.Net.Sockets.TcpListener TcpServer;
    private Inv.DistinctList<Channel> ChannelList;
  }

  internal sealed class Channel
  {
    internal Channel(Server Server, System.Net.Sockets.TcpClient TcpClient)
    {
      this.Server = Server;
      this.TcpClient = TcpClient;
      this.NetworkStream = TcpClient.GetStream();

      if (Server.CertHash != null)
      {
        this.SecureStream = new System.Net.Security.SslStream(NetworkStream, false);
        SecureStream.AuthenticateAsServer(Server.Certificate, false, System.Security.Authentication.SslProtocols.Tls12, true);
        //SecureStream.ReadTimeout = 5000;
        //SecureStream.WriteTimeout = 5000;
      }

      this.IsActive = true;
    }

    public Server Server { get; private set; }
    public bool IsActive { get; private set; }
    public System.Net.IPAddress RemoteIPAddress
    {
      get { return ((System.Net.IPEndPoint)TcpClient.Client.RemoteEndPoint).Address; }
    }
    public System.IO.Stream Stream
    {
      get { return (System.IO.Stream)SecureStream ?? (System.IO.Stream)NetworkStream; }
    }
    public event Action ClosedEvent;

    public void Drop()
    {
      this.IsActive = false;

      if (SecureStream != null)
      {
        SecureStream.Dispose();
        this.SecureStream = null;
      }

      if (NetworkStream != null)
      {
        NetworkStream.Dispose();
        this.NetworkStream = null;
      }

      this.TcpClient.Close();
    }

    internal void ClosedInvoke()
    {
      if (ClosedEvent != null)
        ClosedEvent();
    }

    private System.Net.Sockets.TcpClient TcpClient;
    private System.Net.Security.SslStream SecureStream;
    private System.Net.Sockets.NetworkStream NetworkStream;
  }
#endif
}