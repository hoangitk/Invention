﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  public sealed class TaskBattery<T>
  {
    public TaskBattery()
    {
      this.FunctionList = new DistinctList<Func<T>>();
    }

    public void Add(params Func<T>[] FunctionArray)
    {
      FunctionList.AddRange(FunctionArray);
    }
    public T[] ExecuteInParallel()
    {
      var Count = FunctionList.Count;
      var ResultArray = new T[Count];

      Parallel.For(0, Count, new ParallelOptions() { MaxDegreeOfParallelism = Environment.ProcessorCount }, (Index) => ResultArray[Index] = FunctionList[Index]());

      return ResultArray;
    }

    private Inv.DistinctList<Func<T>> FunctionList;
  }
}
