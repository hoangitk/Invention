﻿#if DEBUG
//#define TIMED
#endif
using System;
using System.Text;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Inv.Support;

namespace Inv.Pathing
{
  /// <summary>
  /// Calculates the weight for a given (X, Y).
  /// </summary>
  /// <param name="X"></param>
  /// <param name="Y"></param>
  /// <param name="CheckOnly"></param>
  /// <returns></returns>
  public delegate byte WeightFunction(int X, int Y, bool CheckOnly);

  /// <summary>
  /// A* pathing algorithm.
  /// </summary>
  public sealed class Algorithm
  {
    /// <summary>
    /// Create a new pathing algorithm for a logical grid.
    /// This class is optimised and width and height must be a power of 2.
    /// </summary>
    /// <param name="GridWidth"></param>
    /// <param name="GridHeight"></param>
    public Algorithm(int GridWidth, int GridHeight)
    {
      if (!GridWidth.IsPowerOfTwo())
        throw new Exception("Invalid grid width; must be power of 2: " + GridWidth);

      if (!GridHeight.IsPowerOfTwo())
        throw new Exception("Invalid grid height; must be power of 2:" + GridHeight);

      this.GridWidth = GridWidth;
      this.GridHeight = GridHeight;
      this.GridWidthMinus1 = GridWidth - 1;
      this.GridHeightLog2 = (int)Math.Ceiling(Math.Log(GridHeight, 2.0)); // TODO: WinRT Log(64) == 5.9999999999999991!!!! WTF.

      this.WorkingGrid = new PathingNode[GridWidth * GridHeight];

      if (Math.Pow(2.0, GridHeightLog2) != GridHeight)
        throw new Exception("Invalid grid height log 2; 2^" + GridHeightLog2 + " must equal " + GridHeight);

      this.OpenNodeQueue = new PriorityQueue<int>(new PathingMatrixComparer(WorkingGrid));
      this.CloseNodeList = new List<FinderNode>();

      this.Formula = HeuristicFormula.Manhattan;
      this.HeuristicEstimate = 2;
      this.SearchLimit = 2000;
      this.TieBreaker = false;
      this.PunishChangeDirection = false;
    }

    /// <summary>
    /// Logical width of the grid.
    /// </summary>
    public int GridWidth { get; private set; }
    /// <summary>
    /// Logical height of the grid.
    /// </summary>
    public int GridHeight { get; private set; }
    /// <summary>
    /// Defaults to 2.
    /// </summary>
    public int HeuristicEstimate { get; set; }
    /// <summary>
    /// Defaults to false.
    /// </summary>
    public bool PunishChangeDirection { get; set; }
    /// <summary>
    /// Defaults to false.
    /// </summary>
    public bool TieBreaker { get; set; }
    /// <summary>
    /// Defaults to 2000.
    /// </summary>
    public int SearchLimit { get; set; }

    /// <summary>
    /// Defaults to Manhattan.
    /// </summary>
    internal HeuristicFormula Formula { get; set; }

    /// <summary>
    /// Find the best path from <paramref name="start"/> to <paramref name="end"/> using the <paramref name="WeightFunction"/> to calculate weight of each logical cell.
    /// </summary>
    /// <param name="start"></param>
    /// <param name="end"></param>
    /// <param name="WeightFunction"></param>
    /// <returns></returns>
    public List<FinderNode> FindPath(Inv.Point start, Inv.Point end, WeightFunction WeightFunction)
    {
#if TIMED
      HighResolutionTime.Start();
#endif

      this.IsFound = false;
      this.CloseNodeCounter = 0;

      OpenNodeQueue.Clear();
      CloseNodeList.Clear();
      Array.Clear(WorkingGrid, 0, WorkingGrid.Length);

      var StartLocation = (start.Y << GridHeightLog2) + start.X;
      var EndLocation = (end.Y << GridHeightLog2) + end.X;

      WorkingGrid[StartLocation].G = 0;
      WorkingGrid[StartLocation].F = HeuristicEstimate;
      WorkingGrid[StartLocation].PX = start.X;
      WorkingGrid[StartLocation].PY = start.Y;
      WorkingGrid[StartLocation].Status = PathingStatus.Open;
      OpenNodeQueue.Push(StartLocation);
      while (OpenNodeQueue.Count > 0)
      {
        var Location = OpenNodeQueue.Pop();

        //Is it in closed list? means this node was already processed
        if (WorkingGrid[Location].Status == PathingStatus.Closed)
          continue;

        var LocationX = Location & GridWidthMinus1;
        var LocationY = Location >> GridHeightLog2;

        if (Location == EndLocation)
        {
          WorkingGrid[Location].Status = PathingStatus.Closed;
          this.IsFound = true;
          break;
        }

        if (CloseNodeCounter > SearchLimit)
        {
          this.IsFound = true;
          return ReturnResult(end);
        }

        var PunishHoriz = PunishChangeDirection ? (LocationX - WorkingGrid[Location].PX) : 0;

        // Lets calculate each successors
        for (var i = 0; i < 8; i++)
        {
          var NewLocationX = LocationX + DirectionGrid[i, 0];
          var NewLocationY = LocationY + DirectionGrid[i, 1];
          var NewLocation = (NewLocationY << GridHeightLog2) + NewLocationX;

          if (NewLocationX < 0 || NewLocationX >= GridWidth || NewLocationY < 0 || NewLocationY >= GridHeight)
            continue;

          // blocked.
          var NewValue = WeightFunction(NewLocationX, NewLocationY, false);

          if (NewValue == 0)
            continue;

          // Can't move diagonally beside a blocked location.
          if (WeightFunction(NewLocationX, LocationY, true) == 0 || WeightFunction(LocationX, NewLocationY, true) == 0)
            continue;

          var NewG = WorkingGrid[Location].G + NewValue;

          if (PunishChangeDirection)
          {
            if ((NewLocationX - LocationX) != 0)
            {
              if (PunishHoriz == 0)
                NewG += Math.Abs(NewLocationX - end.X) + Math.Abs(NewLocationY - end.Y);
            }

            if ((NewLocationY - LocationY) != 0)
            {
              if (PunishHoriz != 0)
                NewG += Math.Abs(NewLocationX - end.X) + Math.Abs(NewLocationY - end.Y);
            }
          }

          // Is it open or closed?
          if (WorkingGrid[NewLocation].Status == PathingStatus.Open || WorkingGrid[NewLocation].Status == PathingStatus.Closed)
          {
            // The current node has less code than the previous? then skip this node
            if (WorkingGrid[NewLocation].G <= NewG)
              continue;
          }

          WorkingGrid[NewLocation].PX = LocationX;
          WorkingGrid[NewLocation].PY = LocationY;
          WorkingGrid[NewLocation].G = NewG;

          int HeuristicResult;
          switch (Formula)
          {
            default:
            case HeuristicFormula.Manhattan:
              HeuristicResult = HeuristicEstimate * (Math.Abs(NewLocationX - end.X) + Math.Abs(NewLocationY - end.Y));
              break;

            case HeuristicFormula.MaxDXDY:
              HeuristicResult = HeuristicEstimate * (Math.Max(Math.Abs(NewLocationX - end.X), Math.Abs(NewLocationY - end.Y)));
              break;

            case HeuristicFormula.DiagonalShortCut:
              var h_diagonal = Math.Min(Math.Abs(NewLocationX - end.X), Math.Abs(NewLocationY - end.Y));
              var h_straight = (Math.Abs(NewLocationX - end.X) + Math.Abs(NewLocationY - end.Y));
              HeuristicResult = (HeuristicEstimate * 2) * h_diagonal + HeuristicEstimate * (h_straight - 2 * h_diagonal);
              break;

            case HeuristicFormula.Euclidean:
              HeuristicResult = (int)(HeuristicEstimate * Math.Sqrt(Math.Pow((NewLocationY - end.X), 2) + Math.Pow((NewLocationY - end.Y), 2)));
              break;

            case HeuristicFormula.EuclideanNoSQR:
              HeuristicResult = (int)(HeuristicEstimate * (Math.Pow((NewLocationX - end.X), 2) + Math.Pow((NewLocationY - end.Y), 2)));
              break;

            case HeuristicFormula.Custom:
              var dxy = new Point(Math.Abs(end.X - NewLocationX), Math.Abs(end.Y - NewLocationY));
              var Orthogonal = Math.Abs(dxy.X - dxy.Y);
              var Diagonal = Math.Abs(((dxy.X + dxy.Y) - Orthogonal) / 2);
              HeuristicResult = HeuristicEstimate * (Diagonal + Orthogonal + dxy.X + dxy.Y);
              break;
          }

          if (TieBreaker)
          {
            var dx1 = LocationX - end.X;
            var dy1 = LocationY - end.Y;
            var dx2 = start.X - end.X;
            var dy2 = start.Y - end.Y;
            var cross = Math.Abs(dx1 * dy2 - dx2 * dy1);
            HeuristicResult = (int)(HeuristicResult + cross * 0.001);
          }

          WorkingGrid[NewLocation].F = NewG + HeuristicResult;

          OpenNodeQueue.Push(NewLocation);
          WorkingGrid[NewLocation].Status = PathingStatus.Open;
        }

        CloseNodeCounter++;
        WorkingGrid[Location].Status = PathingStatus.Closed;
      }

      return ReturnResult(end);
    }

    private List<FinderNode> ReturnResult(Inv.Point end)
    {
#if DEBUG
      //if (CloseNodeCounter > SearchLimit) System.Diagnostics.Debugger.Break();
#endif

      if (IsFound)
      {
        CloseNodeList.Clear();
        int posX;
        int posY;

        var fNodeTmp = WorkingGrid[(end.Y << GridHeightLog2) + end.X];
        var fNode = new FinderNode()
        {
          F = fNodeTmp.F,
          G = fNodeTmp.G,
          H = 0,
          PX = fNodeTmp.PX,
          PY = fNodeTmp.PY,
          X = end.X,
          Y = end.Y
        };

        while (fNode.X != fNode.PX || fNode.Y != fNode.PY)
        {
          CloseNodeList.Add(fNode);

          posX = fNode.PX;
          posY = fNode.PY;
          fNodeTmp = WorkingGrid[(posY << GridHeightLog2) + posX];
          fNode.F = fNodeTmp.F;
          fNode.G = fNodeTmp.G;
          fNode.H = 0;
          fNode.PX = fNodeTmp.PX;
          fNode.PY = fNodeTmp.PY;
          fNode.X = posX;
          fNode.Y = posY;
        }

        CloseNodeList.Add(fNode);

#if TIMED
        System.Diagnostics.Debug.WriteLine("PATH SUCCESS " + HighResolutionTime.GetTime().ToString("N8") + " x" + CloseNodeCounter);
#endif

        return CloseNodeList;
      }

#if TIMED
      System.Diagnostics.Debug.WriteLine("PATH FAILED " + HighResolutionTime.GetTime().ToString("N8") + " x" + CloseNodeCounter);
#endif

      return null;
    }

    private readonly int GridWidthMinus1;
    private readonly int GridHeightLog2;
    private readonly PriorityQueue<int> OpenNodeQueue;
    private readonly List<FinderNode> CloseNodeList;
    private readonly PathingNode[] WorkingGrid;
    private bool IsFound;
    private int CloseNodeCounter;

    private static readonly sbyte[,] DirectionGrid = new sbyte[8, 2] { { 0, -1 }, { 1, 0 }, { 0, 1 }, { -1, 0 }, { 1, -1 }, { 1, 1 }, { -1, 1 }, { -1, -1 } };

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    private struct PathingNode
    {
      public int F; // f = gone + heuristic
      public int G;
      public int PX; // Parent
      public int PY;
      public PathingStatus Status;
    }

    private enum PathingStatus
    {
      None = 0,
      Open = 1,
      Closed = 2
    }

    private class PathingMatrixComparer : IComparer<int>
    {
      public PathingMatrixComparer(PathingNode[] Matrix)
      {
        this.Matrix = Matrix;
      }

      public int Compare(int a, int b)
      {
        if (Matrix[a].F > Matrix[b].F)
          return 1;
        else if (Matrix[a].F < Matrix[b].F)
          return -1;
        return 0;
      }

      private readonly PathingNode[] Matrix;
    }
  }

  internal sealed class PriorityQueue<T>
  {
    public PriorityQueue()
    {
      this.mComparer = Comparer<T>.Default;
    }
    public PriorityQueue(IComparer<T> comparer)
    {
      this.mComparer = comparer;
    }
    public PriorityQueue(IComparer<T> comparer, int capacity)
    {
      this.mComparer = comparer;
      this.InnerList.Capacity = capacity;
    }

    public int Count
    {
      get { return InnerList.Count; }
    }
    public T this[int Index]
    {
      get { return InnerList[Index]; }
    }

    /// <summary>
    /// Push an object onto the PQ
    /// </summary>
    /// <param name="Item">The new object</param>
    /// <returns>The index in the list where the object is _now_. This will change when objects are taken from or put onto the PQ.</returns>
    public int Push(T Item)
    {
      var p = InnerList.Count;

      InnerList.Add(Item); // E[p] = O
      do
      {
        if (p == 0)
          break;

        var p2 = (p - 1) / 2;
        if (Compare(p, p2) < 0)
        {
          Switch(p, p2);
          p = p2;
        }
        else
        {
          break;
        }
      }
      while (true);

      return p;
    }
    /// <summary>
    /// Get the smallest object and remove it.
    /// </summary>
    /// <returns>The smallest object</returns>
    public T Pop()
    {
      var result = InnerList[0];
      InnerList[0] = InnerList[InnerList.Count - 1];
      InnerList.RemoveAt(InnerList.Count - 1);

      var p = 0;
      do
      {
        var pn = p;
        var p1 = 2 * p + 1;
        var p2 = 2 * p + 2;
        if (InnerList.Count > p1 && Compare(p, p1) > 0)
          p = p1;

        if (InnerList.Count > p2 && Compare(p, p2) > 0)
          p = p2;

        if (p == pn)
          break;

        Switch(p, pn);
      }
      while (true);

      return result;
    }
    public void Clear()
    {
      InnerList.Clear();
    }

    private void Switch(int i, int j)
    {
      var h = InnerList[i];
      InnerList[i] = InnerList[j];
      InnerList[j] = h;
    }
    private int Compare(int i, int j)
    {
      return mComparer.Compare(InnerList[i], InnerList[j]);
    }

    private List<T> InnerList = new List<T>();
    private IComparer<T> mComparer;
  }

  /// <summary>
  /// Each point in the path (X, Y).
  /// </summary>
  public struct FinderNode
  {
    /// <summary>
    /// Logical X location for this node.
    /// </summary>
    public int X;
    /// <summary>
    /// Logical Y location for this node.
    /// </summary>
    public int Y;

    internal int F;
    internal int G;
    internal int H;  // f = gone + heuristic
    internal int PX; // Parent
    internal int PY;
  }

  internal enum HeuristicFormula
  {
    Manhattan = 1,
    MaxDXDY = 2,
    DiagonalShortCut = 3,
    Euclidean = 4,
    EuclideanNoSQR = 5,
    Custom = 6
  }

#if TIMED
  internal static class HighResolutionTime
  {
    [System.Runtime.InteropServices.DllImport("Kernel32.dll")]
    private static extern bool QueryPerformanceCounter(out long perfcount);

    [System.Runtime.InteropServices.DllImport("Kernel32.dll")]
    private static extern bool QueryPerformanceFrequency(out long freq);

    private static long mStartCounter;
    private static long mFrequency;

    static HighResolutionTime()
    {
      QueryPerformanceFrequency(out mFrequency);
    }

    public static void Start()
    {
      QueryPerformanceCounter(out mStartCounter);
    }

    public static double GetTime()
    {
      QueryPerformanceCounter(out var endCounter);
      var elapsed = endCounter - mStartCounter;
      return (double)elapsed / mFrequency;
    }
  }
#endif
}