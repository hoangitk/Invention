﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  internal sealed class WpfChrome
  {
    internal WpfChrome(WpfWindow Window)
    {
      this.Window = Window;
      this.ClipElementSet = new HashSet<System.Windows.FrameworkElement>();
      this.Chrome = new System.Windows.Shell.WindowChrome()
      {
        ResizeBorderThickness = new System.Windows.Thickness(5),
        CaptionHeight = 32,
        CornerRadius = new System.Windows.CornerRadius(0),
        GlassFrameThickness = new System.Windows.Thickness(0)
      };

      Toggle(true);
    }

    public bool IsMouseOver { get; internal set; }
    public event Action MouseEnterEvent;
    public event Action MouseLeaveEvent;
    public int CaptionHeight
    {
      get { return (int)Chrome.CaptionHeight; }
      set { Chrome.CaptionHeight = value; }
    }

    public void ClipCaption(System.Windows.FrameworkElement Element)
    {
      ClipElementSet.Add(Element);

      System.Windows.Shell.WindowChrome.SetIsHitTestVisibleInChrome(Element, true);
    }
    public void BindCaptionHeight(System.Windows.FrameworkElement Element)
    {
      Element.SizeChanged += (sender, e) =>
      {
        if (e.HeightChanged)
          Chrome.CaptionHeight = e.NewSize.Height;
      };
    }

    internal void Toggle(bool Value)
    {
      if (Value)
        System.Windows.Shell.WindowChrome.SetWindowChrome(Window, Chrome);
      else
        System.Windows.Shell.WindowChrome.SetWindowChrome(Window, null);
    }
    internal void SetMouseOver(bool Value)
    {
      if (IsMouseOver != Value)
      {
        if (Value || (!Value && !ClipElementSet.Any(E => E.IsMouseOver)))
        {
          this.IsMouseOver = Value;

          //Debug.WriteLine("Chrome mouse over: " + IsMouseOver);

          if (IsMouseOver)
          {
            if (MouseEnterEvent != null)
              MouseEnterEvent();
          }
          else
          {
            if (MouseLeaveEvent != null)
              MouseLeaveEvent();
          }
        }
      }
    }

    private readonly WpfWindow Window;
    private readonly System.Windows.Shell.WindowChrome Chrome;
    private readonly HashSet<System.Windows.FrameworkElement> ClipElementSet;
  }

  internal sealed class WpfSoundPlayer
  {
    internal WpfSoundPlayer()
    {
      this.SoundList = new Inv.DistinctList<Sound>(1024);
      this.SoundEngine = new IrrKlang.ISoundEngine();
    }

    public TimeSpan GetLength(Inv.Sound InvSound)
    {
      return TimeSpan.FromMilliseconds(GetSoundSource(InvSound)?.PlayLength ?? 0);
    }
    public IrrKlang.ISound Play(Inv.Sound InvSound, float VolumeScale, float RateScale, float PanScale, bool Looped)
    {
      var Sound = SoundEngine.Play2D(GetSoundSource(InvSound), playLooped: false, startPaused: false, enableSoundEffects: true);

      if (Sound != null)
      {
        Sound.Volume = VolumeScale;
        Sound.Pan = -PanScale; // irrKlang has an opposite convention?
        Sound.PlaybackSpeed = RateScale;
        Sound.Looped = Looped;
      }

      return Sound;
    }
    public void Reclamation()
    {
      foreach (var Sound in SoundList)
      {
        var WpfSound = Sound.Node as IrrKlang.ISoundSource;

        if (WpfSound != null)
        {
          SoundEngine.RemoveSoundSource(WpfSound.Name);
          WpfSound.Dispose();
        }

        Sound.Node = null;
      }
      SoundList.Clear();
    }

    private IrrKlang.ISoundSource GetSoundSource(Inv.Sound InvSound)
    {
      if (SoundEngine == null || InvSound == null)
        return null;

      var SoundSource = InvSound.Node as IrrKlang.ISoundSource;

      if (SoundSource == null)
      {
        SoundSource = SoundEngine.AddSoundSourceFromMemory(InvSound.GetBuffer(), SoundList.Count.ToString());

        if (InvSound.Node == null || !SoundList.Contains(InvSound))
          SoundList.Add(InvSound);

        InvSound.Node = SoundSource;
      }

      return SoundSource;
    }

    private readonly IrrKlang.ISoundEngine SoundEngine;
    private readonly Inv.DistinctList<Inv.Sound> SoundList;
  }

  internal interface WpfOverrideFocusContract
  {
    void OverrideFocus();
  }

  public sealed class WpfSurface : System.Windows.Controls.Border
  {
    internal WpfSurface()
    {
      this.ClipToBounds = true;
    }
  }

  public sealed class WpfNative : WpfClipper
  {
    internal WpfNative()
    {
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }

    public void SetContent(System.Windows.FrameworkElement Element)
    {
      this.SafeSetContent(Element);
    }
  }

  public sealed class WpfButton : System.Windows.Controls.Button
  {
    internal WpfButton()
    {
      this.Background = null;
      this.Margin = new System.Windows.Thickness(0);
      this.Padding = new System.Windows.Thickness(0);
      this.BorderThickness = new System.Windows.Thickness(0); // while this seems to redundant (when viewing in the debugger), it actually removes a 1dp border.
      this.Focusable = false;
      this.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Stretch;
      this.VerticalContentAlignment = System.Windows.VerticalAlignment.Stretch;
      this.Style = DefaultStyle;
      this.IsEnabledChanged += (Sender, Event) => RefreshInvoke();
    }

    public System.Windows.CornerRadius CornerRadius
    {
      get => (System.Windows.CornerRadius)GetValue(CornerRadiusProperty);
      set => SetValue(CornerRadiusProperty, value);
    }
    public event Action LeftClick;
    public event Action RightClick;
    public event Action PressClick;
    public event Action ReleaseClick;

    public void SetContent(System.Windows.FrameworkElement Element)
    {
      this.SafeSetContent(Element);
    }
    public void AsFlat(System.Windows.Media.Brush Normal, System.Windows.Media.Brush Hovered, System.Windows.Media.Brush Pushed)
    {
      void RefreshAsFlat()
      {
        if (IsPressed)
          this.Background = Pushed;
        else if (IsMouseOver || IsFocused)
          this.Background = Hovered;
        else
          this.Background = Normal;

        this.Opacity = IsEnabled ? 1.00F : 0.50F;
      }

      RefreshAsFlat();

      this.RefreshDelegate = RefreshAsFlat;
    }

    protected override void OnGotFocus(System.Windows.RoutedEventArgs e)
    {
      base.OnGotFocus(e);

      RefreshInvoke();
    }
    protected override void OnLostFocus(System.Windows.RoutedEventArgs e)
    {
      base.OnLostFocus(e);

      RefreshInvoke();
    }
    protected override void OnMouseEnter(System.Windows.Input.MouseEventArgs e)
    {
      base.OnMouseEnter(e);

      RefreshInvoke();
    }
    protected override void OnMouseLeave(System.Windows.Input.MouseEventArgs e)
    {
      base.OnMouseLeave(e);

      RefreshInvoke();
    }
    protected override void OnMouseMove(System.Windows.Input.MouseEventArgs e)
    {
      base.OnMouseMove(e);

      if (this.IsMouseCaptured)
      {
        var isInside = false;

        System.Windows.Media.VisualTreeHelper.HitTest(
            this,
            d =>
            {
              if (d == this)
                isInside = true;

              return System.Windows.Media.HitTestFilterBehavior.Stop;
            },
            ht => System.Windows.Media.HitTestResultBehavior.Stop, new System.Windows.Media.PointHitTestParameters(e.GetPosition(this)));

        base.IsPressed = isInside;
      }
    }
    protected override void OnMouseLeftButtonDown(System.Windows.Input.MouseButtonEventArgs e)
    {
      base.OnMouseLeftButtonDown(e);

      base.IsPressed = true;

      PressInvoke();
      RefreshInvoke();

      CaptureMouse();
      this.IsLeftClicked = true;
    }
    protected override void OnMouseLeftButtonUp(System.Windows.Input.MouseButtonEventArgs e)
    {
      base.OnMouseLeftButtonUp(e);

      base.IsPressed = false;

      if (IsLeftClicked)
      {
        if (this.IsMouseOver)
          LeftClick?.Invoke();

        this.IsLeftClicked = false;
      }

      base.IsPressed = false;

      ReleaseInvoke();
      RefreshInvoke();

      e.Handled = true;
    }
    protected override void OnMouseRightButtonDown(System.Windows.Input.MouseButtonEventArgs e)
    {
      base.OnMouseRightButtonDown(e);

      base.IsPressed = true;

      PressInvoke();
      RefreshInvoke();

      CaptureMouse();
      this.IsRightClicked = true;
    }
    protected override void OnMouseRightButtonUp(System.Windows.Input.MouseButtonEventArgs e)
    {
      base.OnMouseRightButtonUp(e);

      ReleaseMouseCapture();

      if (IsRightClicked)
      {
        if (this.IsMouseOver)
          RightClick?.Invoke();

        this.IsRightClicked = false;
      }

      base.IsPressed = false;

      ReleaseInvoke();
      RefreshInvoke();

      e.Handled = true;
    }
    protected override void OnClick()
    {
      base.OnClick();

      // NOTE: this handles non-mouse clicks such as the ENTER key.
      if (!IsLeftClicked)
      {
        PressInvoke();

        LeftClick?.Invoke();

        ReleaseInvoke();
      }
    }

    private void PressInvoke()
    {
      PressClick?.Invoke();
    }
    private void ReleaseInvoke()
    {
      ReleaseClick?.Invoke();
    }
    private void RefreshInvoke()
    {
      RefreshDelegate?.Invoke();
    }

    private Action RefreshDelegate;
    private bool IsLeftClicked;
    private bool IsRightClicked;

    static WpfButton()
    {
      CornerRadiusProperty = System.Windows.DependencyProperty.Register("CornerRadius", typeof(System.Windows.CornerRadius), typeof(WpfButton), new System.Windows.FrameworkPropertyMetadata(new System.Windows.CornerRadius(0), System.Windows.FrameworkPropertyMetadataOptions.AffectsArrange | System.Windows.FrameworkPropertyMetadataOptions.AffectsMeasure));

      var ResourceDictionary = WpfShell.LoadResourceDictionary("InvWpfButton.xaml");

      DefaultStyle = (System.Windows.Style)ResourceDictionary["InvDefaultButton"];
      DefaultStyle.Seal();
    }

    public static readonly System.Windows.DependencyProperty CornerRadiusProperty;

    private static readonly System.Windows.Style DefaultStyle;
  }

  [System.Windows.Data.ValueConversion(typeof(System.Windows.CornerRadius), typeof(System.Windows.CornerRadius))]
  internal sealed class InnerCornerRadiusConverter : System.Windows.Data.IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      var InputRadius = (System.Windows.CornerRadius)value;

      return new System.Windows.CornerRadius(Math.Max(0, InputRadius.TopLeft - 1), Math.Max(0, InputRadius.TopRight - 1), Math.Max(0, InputRadius.BottomRight - 1), Math.Max(0, InputRadius.BottomLeft - 1));
    }
    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
      return null;
    }
  }

  public sealed class WpfScroll : WpfClipper
  {
    internal WpfScroll()
    {
      this.Inner = new WpfScrollViewer();
      Inner.PanningMode = System.Windows.Controls.PanningMode.Both;
      Inner.CanContentScroll = false;
      Inner.Focusable = false;
      Inner.ManipulationBoundaryFeedback += (Sender, Event) =>
      {
        // Prevent the truly nasty default window moving behaviour on reaching the scroll boundary.
        Event.Handled = true;
      };

      base.Child = Inner;

      Inner.PreviewMouseWheel += (Sender, Event) =>
      {
        if (IsHorizontalField == true)
        {
          if (Event.Delta < 0)
            Inner.LineRight();
          else
            Inner.LineLeft();
        }
      };
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public System.Windows.FrameworkElement Content
    {
      get { return (System.Windows.FrameworkElement)Inner.Content; }
      set { Inner.SafeSetContent(value); }
    }
    public bool IsScrollViewerDark
    {
      get => Inner.IsDark;
      set => Inner.IsDark = value;
    }

    public void SetOrientation(bool IsHorizontal)
    {
      if (IsHorizontal != IsHorizontalField)
      {
        this.IsHorizontalField = IsHorizontal;

        if (IsHorizontal)
        {
          Inner.VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Disabled;
          Inner.HorizontalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto;
          Inner.PanningMode = System.Windows.Controls.PanningMode.HorizontalOnly;
        }
        else
        {
          Inner.VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto;
          Inner.HorizontalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Disabled;
          Inner.PanningMode = System.Windows.Controls.PanningMode.VerticalOnly;
        }
      }
    }

    private readonly WpfScrollViewer Inner;
    private bool? IsHorizontalField;
  }

  internal sealed class WpfScrollViewer : System.Windows.Controls.ScrollViewer
  {
    public WpfScrollViewer()
    {
      this.Style = FlatScrollViewerStyle;
      this.PreviewMouseWheel += PreviewMouseWheelHandler;
    }

    static WpfScrollViewer()
    {
      IsDarkProperty = System.Windows.DependencyProperty.Register("IsDark", typeof(bool), typeof(WpfScrollViewer), new System.Windows.FrameworkPropertyMetadata(false));

      var ResourceDictionary = WpfShell.LoadResourceDictionary("InvWpfScrollViewer.xaml");

      if (ResourceDictionary != null)
      {
        FlatScrollViewerStyle = (System.Windows.Style)ResourceDictionary["FlatScrollViewerStyle"];
        FlatScrollViewerStyle.Seal();
      }
    }

    public bool MousePanningEnabled { get; set; }
    public bool IsDark
    {
      get { return (bool)GetValue(IsDarkProperty); }
      set { SetValue(IsDarkProperty, value); }
    }

    internal double? ScrollBarSize
    {
      get { return ScrollBarSizeField; }
      set
      {
        if (ScrollBarSizeField != value)
        {
          ScrollBarSizeField = value;

          UpdateScrollBarSize();
        }
      }
    }
    internal System.Windows.Controls.Primitives.ScrollBar VerticalScrollBar { get; private set; }
    internal System.Windows.Controls.Primitives.ScrollBar HorizontalScrollBar { get; private set; }

    public override void OnApplyTemplate()
    {
      base.OnApplyTemplate();

      if (Template != null)
      {
        VerticalScrollBar = Template.FindName("PART_VerticalScrollBar", this) as System.Windows.Controls.Primitives.ScrollBar;
        HorizontalScrollBar = Template.FindName("PART_HorizontalScrollBar", this) as System.Windows.Controls.Primitives.ScrollBar;

        UpdateScrollBarSize();
      }
    }

    protected override void OnMouseMove(System.Windows.Input.MouseEventArgs e)
    {
      base.OnMouseMove(e);

      if (MousePanningEnabled && IsMouseCaptured)
      {
        var CurrentPosition = e.GetPosition(this);

        var Delta = new System.Windows.Point();
        Delta.X = (CurrentPosition.X > PanStartPosition.X) ? -(CurrentPosition.X - PanStartPosition.X) : (PanStartPosition.X - CurrentPosition.X);
        Delta.Y = (CurrentPosition.Y > PanStartPosition.Y) ? -(CurrentPosition.Y - PanStartPosition.Y) : (PanStartPosition.Y - CurrentPosition.Y);

        ScrollToHorizontalOffset(PanStartOffset.X + Delta.X);
        ScrollToVerticalOffset(PanStartOffset.Y + Delta.Y);
      }
    }
    protected override void OnPreviewMouseDown(System.Windows.Input.MouseButtonEventArgs e)
    {
      base.OnPreviewMouseDown(e);

      if (MousePanningEnabled && IsMouseOver)
      {
        PanStartPosition = e.GetPosition(this);
        PanStartOffset.X = HorizontalOffset;
        PanStartOffset.Y = VerticalOffset;

        Cursor = System.Windows.Input.Cursors.ScrollAll;
        CaptureMouse();
      }
    }
    protected override void OnPreviewMouseUp(System.Windows.Input.MouseButtonEventArgs e)
    {
      base.OnPreviewMouseUp(e);

      if (MousePanningEnabled && IsMouseCaptured)
      {
        Cursor = System.Windows.Input.Cursors.Arrow;
        ReleaseMouseCapture();
      }
    }

    private void UpdateScrollBarSize()
    {
      if (ScrollBarSizeField.HasValue)
      {
        var Value = ScrollBarSizeField.Value;

        if (VerticalScrollBar != null)
          VerticalScrollBar.Width = Value;

        if (HorizontalScrollBar != null)
          HorizontalScrollBar.Height = Value;
      }
    }

    private System.Windows.Point PanStartPosition;
    private System.Windows.Point PanStartOffset;
    private double? ScrollBarSizeField;

    private static void PreviewMouseWheelHandler(object Sender, System.Windows.Input.MouseWheelEventArgs e)
    {
      var Control = Sender as System.Windows.Controls.ScrollViewer;

      Debug.Assert(Control != null, "Sender is not assigned or is not a ScrollViewer");

      if (!e.Handled && !Inv.WpfFoundation.MouseWheelEventArgsList.Contains(e))
      {
        var PreviewEventArg = new System.Windows.Input.MouseWheelEventArgs(e.MouseDevice, e.Timestamp, e.Delta)
        {
          RoutedEvent = System.Windows.UIElement.PreviewMouseWheelEvent,
          Source = Sender
        };

        var OriginalSource = e.OriginalSource as System.Windows.UIElement;

        if (OriginalSource != null)
        {
          Inv.WpfFoundation.MouseWheelEventArgsList.Add(PreviewEventArg);
          OriginalSource.RaiseEvent(PreviewEventArg);
          Inv.WpfFoundation.MouseWheelEventArgsList.Remove(PreviewEventArg);
        }

        e.Handled = PreviewEventArg.Handled;

        if (!e.Handled)
        {
          var Delta = e.Delta;

          if ((Delta > 0 && Control.VerticalOffset == 0) || (Delta <= 0 && Control.VerticalOffset >= Control.ExtentHeight - Control.ViewportHeight))
          {
            var LogicalParent = (System.Windows.UIElement)((System.Windows.FrameworkElement)Sender).Parent;
            if (LogicalParent != null)
            {
              var EventArgs = new System.Windows.Input.MouseWheelEventArgs(e.MouseDevice, e.Timestamp, Delta)
              {
                RoutedEvent = System.Windows.UIElement.MouseWheelEvent,
                Source = Sender
              };

              LogicalParent.RaiseEvent(EventArgs);

              e.Handled = EventArgs.Handled;
            }
          }
        }
      }
    }

    internal static readonly System.Windows.DependencyProperty IsDarkProperty;
    internal static readonly System.Windows.Style FlatScrollViewerStyle;
  }

  public sealed class WpfCanvas : WpfClipper, Inv.DrawContract
  {
    internal WpfCanvas(WpfHost WpfHost)
    {
      this.WpfHost = WpfHost;

      this.ClipToBounds = true; // prevents drawing outside of the control.

      this.WpfDrawingVisual = new System.Windows.Media.DrawingVisual();
      AddVisualChild(WpfDrawingVisual);

      // reused for memory management performance.
      this.WpfRectangleRect = new System.Windows.Rect();
      this.WpfImageRect = new System.Windows.Rect();

      // NOTE: uncertain how CacheMode affects custom drawn performance. However, it does prevent the RenderOptions from being used and end up with poorly scaled DrawImages.
      //this.CacheMode = new System.Windows.Media.BitmapCache();
      System.Windows.Media.RenderOptions.SetBitmapScalingMode(this, System.Windows.Media.BitmapScalingMode.Fant);
    }

    public static readonly System.Windows.RoutedEvent MouseDoubleClickEvent = System.Windows.Controls.Control.MouseDoubleClickEvent.AddOwner(typeof(WpfCanvas));

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public event System.Windows.Input.MouseButtonEventHandler MouseDoubleClick
    {
      add { base.AddHandler(WpfCanvas.MouseDoubleClickEvent, value); }
      remove { base.RemoveHandler(WpfCanvas.MouseDoubleClickEvent, value); }
    }

    public void StartDrawing()
    {
      this.WpfDrawingContext = WpfDrawingVisual.RenderOpen();

    }
    public void StopDrawing()
    {
      WpfDrawingContext.Close();
    }

    protected override void OnMouseDown(System.Windows.Input.MouseButtonEventArgs e)
    {
      if (e.ClickCount == 2)
      {
        RaiseEvent(new System.Windows.Input.MouseButtonEventArgs(System.Windows.Input.Mouse.PrimaryDevice, 0, e.ChangedButton)
        {
          RoutedEvent = WpfCanvas.MouseDoubleClickEvent,
          Source = this,
        });

        e.Handled = true;
      }
      else
      {
        base.OnMouseDown(e);
      }
    }
    protected override int VisualChildrenCount
    {
      get { return 1; }
    }
    protected override System.Windows.Media.Visual GetVisualChild(int index)
    {
      return WpfDrawingVisual;
    }

    void DrawContract.DrawText(string TextFragment, string TextFontName, int TextFontSize, FontWeight TextFontWeight, Colour TextFontColour, Point TextPoint, HorizontalPosition TextHorizontal, VerticalPosition TextVertical)
    {
      var WpfFormattedText = new System.Windows.Media.FormattedText
      (
        TextFragment,
        System.Globalization.CultureInfo.CurrentCulture,
        System.Windows.FlowDirection.LeftToRight,
        new System.Windows.Media.Typeface(new System.Windows.Media.FontFamily(TextFontName.EmptyAsNull() ?? WpfHost.WpfOptions.DefaultFontName), System.Windows.FontStyles.Normal, WpfHost.TranslateFontWeight(TextFontWeight), System.Windows.FontStretches.Normal),
        TextFontSize,
        WpfHost.TranslateBrush(TextFontColour)
      );

      var WpfTextPoint = WpfHost.TranslatePoint(TextPoint);

      if (TextHorizontal != HorizontalPosition.Left)
      {
        var TextWidth = (int)WpfFormattedText.Width;

        if (TextHorizontal == HorizontalPosition.Right)
          WpfTextPoint.X -= TextWidth;
        else if (TextHorizontal == HorizontalPosition.Center)
          WpfTextPoint.X -= TextWidth / 2;
      }

      if (TextVertical != VerticalPosition.Top)
      {
        var TextHeight = (int)WpfFormattedText.Height;

        if (TextVertical == VerticalPosition.Bottom)
          WpfTextPoint.Y -= TextHeight;
        else if (TextVertical == VerticalPosition.Center)
          WpfTextPoint.Y -= TextHeight / 2;
      }

      WpfDrawingContext.DrawText(WpfFormattedText, WpfTextPoint);
    }
    void DrawContract.DrawLine(Inv.Colour LineStrokeColour, int LineStrokeThickness, Inv.Point LineSourcePoint, Inv.Point LineTargetPoint, params Inv.Point[] LineExtraPointArray)
    {
      if (LineStrokeThickness <= 0 || LineStrokeColour == Inv.Colour.Transparent)
        return;

      var PointPen = WpfHost.TranslatePen(LineStrokeColour, LineStrokeThickness);

      // In WPF, when you draw a line, the line is centred on the coordinates you specify. 
      var StrokeWidth = LineStrokeThickness / 2.0;

      var SourcePoint = WpfHost.TranslatePoint(LineSourcePoint);
      SourcePoint.X += StrokeWidth;
      SourcePoint.Y += StrokeWidth;

      var CurrentPoint = WpfHost.TranslatePoint(LineTargetPoint);
      CurrentPoint.X += StrokeWidth;
      CurrentPoint.Y += StrokeWidth;
      WpfDrawingContext.DrawLine(PointPen, SourcePoint, CurrentPoint);

      for (var Index = 0; Index < LineExtraPointArray.Length; Index++)
      {
        var TargetPoint = WpfHost.TranslatePoint(LineExtraPointArray[Index]);
        TargetPoint.X += StrokeWidth;
        TargetPoint.Y += StrokeWidth;
        WpfDrawingContext.DrawLine(PointPen, CurrentPoint, TargetPoint);

        CurrentPoint = TargetPoint;
      }
    }
    void DrawContract.DrawRectangle(Colour RectangleFillColour, Colour RectangleStrokeColour, int RectangleStrokeThickness, Rect RectangleRect)
    {
      WpfHost.TranslateRect(RectangleRect, ref WpfRectangleRect);

      if (RectangleStrokeThickness > 0)
      {
        var StrokeWidth = RectangleStrokeThickness / 2.0;
        WpfRectangleRect.X += StrokeWidth;
        WpfRectangleRect.Y += StrokeWidth;

        if (WpfRectangleRect.Width >= RectangleStrokeThickness)
          WpfRectangleRect.Width -= RectangleStrokeThickness;

        if (WpfRectangleRect.Height >= RectangleStrokeThickness)
          WpfRectangleRect.Height -= RectangleStrokeThickness;
      }
      /*
      if (StrokeWidth > 0)
      {
        var Guidelines = new System.Windows.Media.GuidelineSet();
        Guidelines.GuidelinesX.Add(WpfRectangleRect.Left + StrokeWidth);
        Guidelines.GuidelinesX.Add(WpfRectangleRect.Right + StrokeWidth);
        Guidelines.GuidelinesY.Add(WpfRectangleRect.Top + StrokeWidth);
        Guidelines.GuidelinesY.Add(WpfRectangleRect.Bottom + StrokeWidth);
        WpfContext.PushGuidelineSet(Guidelines);
      }*/

      WpfDrawingContext.DrawRectangle(
        WpfHost.TranslateBrush(RectangleFillColour),
        WpfHost.TranslatePen(RectangleStrokeColour, RectangleStrokeThickness),
        WpfRectangleRect);

      //if (StrokeWidth > 0)
      //  WpfContext.Pop();
    }
    void DrawContract.DrawEllipse(Colour EllipseFillColour, Colour EllipseStrokeColour, int EllipseStrokeThickness, Point EllipseCenter, Point EllipseRadius)
    {
      WpfDrawingContext.DrawEllipse(
        WpfHost.TranslateBrush(EllipseFillColour),
        WpfHost.TranslatePen(EllipseStrokeColour,
        EllipseStrokeThickness),
        WpfHost.TranslatePoint(EllipseCenter), EllipseRadius.X, EllipseRadius.Y);
    }
    void DrawContract.DrawArc(Inv.Colour ArcFillColour, Inv.Colour ArcStrokeColour, int ArcStrokeThickness, Inv.Point ArcCenter, Inv.Point ArcRadius, float StartAngle, float SweepAngle)
    {
      var MaxWidth = Math.Max(0.0, (ArcRadius.X * 2) - ArcStrokeThickness);
      var MaxHeight = Math.Max(0.0, (ArcRadius.Y * 2) - ArcStrokeThickness);

      var StartX = ArcRadius.X * Math.Cos(StartAngle * Math.PI / 180.0);
      var StartY = ArcRadius.Y * Math.Sin(StartAngle * Math.PI / 180.0);

      var EndX = ArcRadius.X * Math.Cos(SweepAngle * Math.PI / 180.0);
      var EndY = ArcRadius.Y * Math.Sin(SweepAngle * Math.PI / 180.0);

      var WpfGeometry = new System.Windows.Media.PathGeometry();

      var WpfFigure = new System.Windows.Media.PathFigure
      {
        StartPoint = new System.Windows.Point(ArcCenter.X + StartX, ArcCenter.Y - StartY),
        IsFilled = true,
        IsClosed = true
      };
      WpfFigure.Segments.Add(new System.Windows.Media.ArcSegment
      {
        Point = new System.Windows.Point(ArcCenter.X + EndX, ArcCenter.Y - EndY),
        Size = new System.Windows.Size(MaxWidth / 2.0, MaxHeight / 2),
        RotationAngle = 0.0,
        IsLargeArc = (SweepAngle - StartAngle) > 180,
        SweepDirection = System.Windows.Media.SweepDirection.Counterclockwise,
        IsStroked = true,
        IsSmoothJoin = false
      });
      WpfFigure.Segments.Add(new System.Windows.Media.LineSegment
      {
        Point = new System.Windows.Point(ArcCenter.X, ArcCenter.Y),
        IsStroked = true,
        IsSmoothJoin = false
      });
      WpfFigure.Freeze();

      WpfGeometry.Figures.Add(WpfFigure);
      WpfGeometry.Freeze();

      WpfDrawingContext.DrawGeometry(WpfHost.TranslateBrush(ArcFillColour), WpfHost.TranslatePen(ArcStrokeColour, ArcStrokeThickness), WpfGeometry);
    }
    void DrawContract.DrawImage(Image ImageSource, Rect ImageRect, float ImageOpacity, Colour ImageTintColour, Mirror? ImageMirror, float ImageRotation)
    {
      WpfHost.TranslateRect(ImageRect, ref WpfImageRect);

      if (ImageOpacity != 1.0F)
      {
        // NOTE: WpfContext.PushOpacity performs terribly if you push/pop in quick succession (this is a workaround).
        var WpfTintBrush = WpfHost.TranslateBrush(Inv.Colour.White.Opacity(ImageOpacity));
        WpfDrawingContext.PushOpacityMask(WpfTintBrush);
      }

      if (ImageRotation != 0.0F)
      {
        var ImageCenter = ImageRect.Center();

        // NOTE: cannot reuse these transforms with different values in the one draw phase.
        var WpfRotateTransform = new System.Windows.Media.RotateTransform(ImageRotation, ImageCenter.X, ImageCenter.Y);
        WpfRotateTransform.Freeze();
        WpfDrawingContext.PushTransform(WpfRotateTransform);
      }

      if (ImageMirror != null)
      {
        WpfDrawingContext.PushTransform(WpfHost.MirrorTransformArray[ImageMirror.Value]);

        if (ImageMirror.Value == Mirror.Horizontal)
          WpfImageRect.X = -WpfImageRect.X - WpfImageRect.Width;

        if (ImageMirror.Value == Mirror.Vertical)
          WpfImageRect.Y = -WpfImageRect.Y - WpfImageRect.Height;
      }

      var WpfImage = WpfHost.TranslateImage(ImageSource);

      WpfDrawingContext.DrawImage(WpfImage, WpfImageRect);

      if (ImageTintColour != null)
      {
        var WpfTintBrush = WpfHost.TranslateBrush(ImageTintColour);

        var WpfImageBrush = new System.Windows.Media.ImageBrush(WpfImage);
        WpfImageBrush.Freeze();

        WpfDrawingContext.PushOpacityMask(WpfImageBrush);
        WpfDrawingContext.DrawRectangle(WpfTintBrush, null, WpfImageRect);
        WpfDrawingContext.Pop();
      }

      if (ImageMirror != null)
        WpfDrawingContext.Pop();

      if (ImageRotation != 0.0F)
        WpfDrawingContext.Pop();

      if (ImageOpacity != 1.0F)
        WpfDrawingContext.Pop();
    }
    void DrawContract.DrawPolygon(Colour FillColour, Colour StrokeColour, int StrokeThickness, LineJoin LineJoin, Point StartPoint, params Point[] PointArray)
    {
      var PathGeometry = new System.Windows.Media.PathGeometry();
      var PathFigure = new System.Windows.Media.PathFigure()
      {
        StartPoint = WpfHost.TranslatePoint(StartPoint),
        IsClosed = true
      };
      PathFigure.Segments.Add(new System.Windows.Media.PolyLineSegment(PointArray.Select(P => WpfHost.TranslatePoint(P)).ToList(), true));
      PathFigure.Freeze();
      PathGeometry.Figures.Add(PathFigure);
      PathGeometry.Freeze();

      System.Windows.Media.PenLineJoin PenLineJoin;
      switch (LineJoin)
      {
        case Inv.LineJoin.Miter:
          PenLineJoin = System.Windows.Media.PenLineJoin.Miter;
          break;

        case Inv.LineJoin.Bevel:
          PenLineJoin = System.Windows.Media.PenLineJoin.Bevel;
          break;

        case Inv.LineJoin.Round:
          PenLineJoin = System.Windows.Media.PenLineJoin.Round;
          break;

        default:
          throw new NotSupportedException("Line join type not handled: " + LineJoin.ToString());
      }

      var Pen = WpfHost.TranslatePen(StrokeColour, StrokeThickness, PenLineJoin);

      WpfDrawingContext.DrawGeometry(WpfHost.TranslateBrush(FillColour), Pen, PathGeometry);
    }

    private readonly WpfHost WpfHost;
    private readonly System.Windows.Media.DrawingVisual WpfDrawingVisual;
    private System.Windows.Media.DrawingContext WpfDrawingContext;
    private System.Windows.Rect WpfRectangleRect;
    private System.Windows.Rect WpfImageRect;
  }

  internal sealed class WpfWindow : System.Windows.Window
  {
  }

  public abstract class WpfClipper : System.Windows.Controls.Border
  {
    protected override void OnRender(System.Windows.Media.DrawingContext dc)
    {
      OnApplyChildClip();
      base.OnRender(dc);
    }

    public override System.Windows.UIElement Child
    {
      get { return base.Child; }
      set
      {
        if (this.Child != value)
        {
          if (this.Child != null)
            this.Child.SetValue(System.Windows.UIElement.ClipProperty, OldClip); // Restore original clipping

          if (value != null)
            OldClip = value.ReadLocalValue(System.Windows.UIElement.ClipProperty);
          else
            OldClip = null; // If we dont set it to null we could leak a Geometry object

          base.Child = value;
        }
      }
    }

    protected virtual void OnApplyChildClip()
    {
      var ApplyChild = this.Child;

      if (ApplyChild != null)
      {
        if (this.CornerRadius.TopLeft != 0 || this.CornerRadius.TopRight != 0 || this.CornerRadius.BottomLeft != 0 || this.CornerRadius.BottomRight != 0)
        {
          /*
          if (ClipPath == null)
            this.ClipPath = new PathGeometry();
          else
            ClipPath.Clear();

          var WpfFigure = new PathFigure
          {
            StartPoint = new System.Windows.Point(0, 0),
            IsFilled = true,
            IsClosed = true
          };
          WpfFigure.Segments.Add(new ArcSegment
          {
            Point = new System.Windows.Point(ArcCenter.X + EndX, ArcCenter.Y - EndY),
            Size = new System.Windows.Size(MaxWidth / 2.0, MaxHeight / 2),
            RotationAngle = 0.0,
            IsLargeArc = (SweepAngle - StartAngle) > 180,
            SweepDirection = System.Windows.Media.SweepDirection.Counterclockwise,
            IsStroked = true,
            IsSmoothJoin = false
          });
          WpfFigure.Segments.Add(new LineSegment
          {
            Point = new System.Windows.Point(ArcCenter.X, ArcCenter.Y),
            IsStroked = true,
            IsSmoothJoin = false
          });
          WpfFigure.Freeze();

          ClipPath.Figures.Add(WpfFigure);
          ClipPath.Freeze();
          */
          var Radius = Math.Max(0.0, this.CornerRadius.TopLeft - (this.BorderThickness.Left * 0.5));

          if (ClipRect == null)
            ClipRect = new System.Windows.Media.RectangleGeometry();

          ClipRect.RadiusX = Radius;
          ClipRect.RadiusY = Radius;
          ClipRect.Rect = new System.Windows.Rect(Child.RenderSize);

          ApplyChild.Clip = ClipRect;
        }
        else
        {
          this.ClipRect = null;
          //this.ClipPath = null;

          ApplyChild.Clip = null;
        }
      }
    }

    private System.Windows.Media.RectangleGeometry ClipRect;
    //private PathGeometry ClipPath;
    private object OldClip;
  }

  public sealed class WpfFrame : WpfGrid
  {
    internal WpfFrame()
      : base()
    {
    }

    internal bool IsTransitioning
    {
      get => ClipToBounds;
      set => ClipToBounds = value; // NOTE: required for carousel transitions.
    }
  }

  public sealed class WpfBrowser : WpfClipper
  {
    internal WpfBrowser()
    {
      this.Inner = new System.Windows.Controls.WebBrowser();
      base.Child = Inner;
      Inner.Margin = new System.Windows.Thickness(0);

      // so we can see the background of control instead of stark white, before the page is loaded.
      Inner.Visibility = System.Windows.Visibility.Hidden;
      Inner.Loaded += (Sender, Event) =>
      {
        if (MouseHook == null)
        {
          this.MouseHook = new Inv.SystemMouseHook();
          MouseHook.FaultEvent += (Exception) => FaultEvent?.Invoke(Exception);
          MouseHook.HandleEvent += (Mouse) =>
          {
            if (Inner.IsLoaded && Inner.IsVisible)
            {
              var MousePosition = GetMousePosition();
              var ControlPosition = Inner.PointFromScreen(MousePosition);

              if (ControlPosition.X >= 0 && ControlPosition.Y >= 0 && ControlPosition.X < Inner.ActualWidth && ControlPosition.Y < Inner.ActualHeight)
              {
                if (Mouse == Inv.SystemMouseType.XButton1Down)
                {
                  if (MouseBackEvent != null)
                    MouseBackEvent();
                }
                else if (Mouse == Inv.SystemMouseType.XButton2Down)
                {
                  if (MouseForwardEvent != null)
                    MouseForwardEvent();
                }
              }
            }
          };
        }
      };
      Inner.Unloaded += (Sender, Event) =>
      {
        if (MouseHook != null)
        {
          MouseHook.Dispose();
          this.MouseHook = null;
        }
      };
      Inner.LoadCompleted += (Sender, Event) =>
      {
        Inner.Visibility = System.Windows.Visibility.Visible;

        // TODO: autosize to content and hide scrollbars?
        //dynamic doc = Inner.Document;
        //Inner.Width = doc.body.scrollWidth;
        //Inner.Height = doc.body.scrollHeight;

        Inner.Dispatcher.BeginInvoke(new Action(() =>
        {
          if (ReadyEvent != null)
            ReadyEvent(Event.Uri);
        }));
      };
      Inner.Navigating += (Sender, Event) =>
      {
        if (BlockQuery != null && Event.Uri != null)
          Event.Cancel = BlockQuery(Event.Uri);
      };

      HideScriptErrors(true);

      void HideScriptErrors(bool hide)
      {
        var fiComWebBrowser = typeof(System.Windows.Controls.WebBrowser).GetField("_axIWebBrowser2", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
        if (fiComWebBrowser == null) return;
        var objComWebBrowser = fiComWebBrowser.GetValue(Inner);
        if (objComWebBrowser == null)
        {
          Inner.Loaded += (o, s) => HideScriptErrors(hide); //In case we are to early
          return;
        }
        objComWebBrowser.GetType().InvokeMember("Silent", System.Reflection.BindingFlags.SetProperty, null, objComWebBrowser, new object[] { hide });
      }

      // TODO: does not seem to be any way to prevent the WebBrowser from displaying over the window when using a carousel navigation.
      //this.ClipToBounds = true;
      //Inner.ClipToBounds = true;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public event Action<Exception> FaultEvent;
    public event Action MouseBackEvent;
    public event Action MouseForwardEvent;
    public event Func<Uri, bool> BlockQuery;
    public event Action<Uri> ReadyEvent;

    public void Navigate(Uri Uri, string Html)
    {
      if (Html != null)
        Inner.NavigateToString(Html);
      else if (Uri != null)
        Inner.Navigate(Uri);
      else
        Inner.Navigate("about:blank");
    }

    private readonly System.Windows.Controls.WebBrowser Inner;
    private Inv.SystemMouseHook MouseHook;

    [System.Runtime.InteropServices.DllImport("user32.dll")]
    [return: System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.Bool)]
    static extern bool GetCursorPos(ref Win32Point pt);

    [System.Runtime.InteropServices.StructLayout(System.Runtime.InteropServices.LayoutKind.Sequential)]
    struct Win32Point
    {
      public int X;
      public int Y;
    };
    static System.Windows.Point GetMousePosition()
    {
      var w32Mouse = new Win32Point();
      GetCursorPos(ref w32Mouse);
      return new System.Windows.Point(w32Mouse.X, w32Mouse.Y);
    }
  }

  public sealed class WpfStack : WpfClipper
  {
    internal WpfStack()
    {
      this.Inner = new System.Windows.Controls.StackPanel();
      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public System.Windows.Controls.Orientation Orientation
    {
      get { return Inner.Orientation; }
      set
      {
        if (Inner.Orientation != value)
          Inner.Orientation = value;
      }
    }

    internal void Compose(IEnumerable<System.Windows.FrameworkElement> WpfElements)
    {
      // TODO: delta algorithm?

      Inner.Children.Clear();
      foreach (var WpfElement in WpfElements)
        Inner.SafeAddChild(WpfElement);
    }

    public System.Windows.Controls.StackPanel AsStackPanel()
    {
      return Inner;
    }

    private readonly System.Windows.Controls.StackPanel Inner;
  }

  public sealed class WpfSwitch : WpfClipper
  {
    public WpfSwitch()
    {
      this.Inner = new WpfSwitchButton();
      Inner.IsThreeState = false;
      Inner.PrimaryBrush = System.Windows.Media.Brushes.DimGray;
      Inner.SecondaryBrush = System.Windows.Media.Brushes.LightGray;
      Inner.HorizontalAlignment = System.Windows.HorizontalAlignment.Right;

      base.Child = Inner;
    }

    public bool IsOn
    {
      get { return Inner.IsChecked ?? false; }
      set { Inner.IsChecked = value; }
    }
    public System.Windows.Media.Brush PrimaryBrush
    {
      get { return Inner.PrimaryBrush; }
      set { Inner.PrimaryBrush = value; }
    }
    public System.Windows.Media.Brush SecondaryBrush
    {
      get { return Inner.SecondaryBrush; }
      set { Inner.SecondaryBrush = value; }
    }
    public event System.Windows.RoutedEventHandler ChangeEvent
    {
      add
      {
        Inner.Checked += value;
        Inner.Unchecked += value;
      }
      remove
      {
        Inner.Checked -= value;
        Inner.Unchecked -= value;
      }
    }

    private readonly WpfSwitchButton Inner;
  }

  public sealed class WpfWrap : WpfClipper
  {
    internal WpfWrap()
    {
      this.Inner = new System.Windows.Controls.WrapPanel();
      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public System.Windows.Controls.Orientation Orientation
    {
      get { return Inner.Orientation; }
      set
      {
        if (Inner.Orientation != value)
          Inner.Orientation = value;
      }
    }

    internal void Compose(IEnumerable<System.Windows.FrameworkElement> WpfElements)
    {
      // TODO: delta algorithm?

      Inner.Children.Clear();
      foreach (var WpfElement in WpfElements)
        Inner.SafeAddChild(WpfElement);
    }

    public System.Windows.Controls.WrapPanel AsWrapPanel()
    {
      return Inner;
    }

    private readonly System.Windows.Controls.WrapPanel Inner;
  }

  public struct WpfFlowItem
  {
    public Inv.Panel Panel;
    public System.Windows.FrameworkElement Element;
  }

  public sealed class WpfFlow : WpfClipper
  {
    internal WpfFlow()
    {
      this.FlowItemsControl = new WpfFlowItemsControl();
      base.Child = FlowItemsControl;

      System.Windows.Controls.ScrollViewer.SetVerticalScrollBarVisibility(FlowItemsControl, System.Windows.Controls.ScrollBarVisibility.Auto);
      System.Windows.Controls.ScrollViewer.SetHorizontalScrollBarVisibility(FlowItemsControl, System.Windows.Controls.ScrollBarVisibility.Disabled);
      System.Windows.Controls.ScrollViewer.SetPanningMode(FlowItemsControl, System.Windows.Controls.PanningMode.VerticalOnly);

      //this.ScrollTargetIndex = -1;

      FlowItemsControl.ScrollChangeEvent += (E) =>
      {
        QueryItemRange((int)E.VerticalOffset, (int)(E.VerticalOffset + E.ViewportHeight));
      };

      WpfFlowVirtualizingStackPanel.AddScrollChangeHandler(FlowItemsControl, (Sender, E) =>
      {
        // NOTE: This just exists now to get access to the underlying virtualised stack panel.

        this.Panel = E.Panel;
      });

      this.IndexContentDictionary = new Dictionary<int, WpfFlowContentInfo>();
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public bool IsScrollViewerDark
    {
      get { return FlowItemsControl.IsScrollViewerDark; }
      set { FlowItemsControl.IsScrollViewerDark = value; }
    }
    public Func<int, WpfFlowItem> HeaderContentQuery;
    public Func<int, WpfFlowItem> FooterContentQuery;
    public Func<int> SectionCountQuery;
    public Func<int, int> ItemCountQuery;
    public Func<int, int, WpfFlowItem> ItemContentQuery;
    public Action<int, int, WpfFlowItem> ItemRecycleEvent;

    public void Reload()
    {
      // everything we thought we knew is gone.
      if (ItemRecycleEvent != null)
      {
        foreach (var IndexContent in IndexContentDictionary.Values)
        {
          if (IndexContent != null)
            ItemRecycleEvent(IndexContent.SectionIndex, IndexContent.ItemIndex, IndexContent.Item);
        }
      }
      IndexContentDictionary.Clear();

      this.PlaceholderCollection = new System.Collections.ObjectModel.ObservableCollection<System.Windows.FrameworkElement>();

      var SectionCount = SectionCountQuery();

      for (var SectionIndex = 0; SectionIndex < SectionCount; SectionIndex++)
      {
        var HeaderContent = HeaderContentQuery(SectionIndex);
        if (HeaderContent.Element != null)
          AddPlaceholder(null);

        var ItemCount = ItemCountQuery(SectionIndex);

        for (int ItemIndex = 0; ItemIndex < ItemCount; ItemIndex++)
          AddPlaceholder(null);

        var FooterContent = FooterContentQuery(SectionIndex);
        if (FooterContent.Element != null)
          AddPlaceholder(null);
      }

      FlowItemsControl.ItemsSource = PlaceholderCollection;

      if (/*FlowItemsControl.IsLoaded && */Panel != null)
        QueryItemRange((int)Panel.VerticalOffset, (int)(Panel.VerticalOffset + Panel.ViewportHeight));
    }
    public void ScrollTo(int Section, int Index)
    {
      var TargetIndex = GetAbsoluteIndex(Section, Index);

      if (Panel != null)
        Panel.BringIndexIntoViewPublic(TargetIndex);
      //else
      //  this.ScrollTargetIndex = TargetIndex;
    }

    private readonly WpfFlowItemsControl FlowItemsControl;
    private System.Collections.ObjectModel.ObservableCollection<System.Windows.FrameworkElement> PlaceholderCollection;
    private WpfFlowVirtualizingStackPanel Panel;
    //private int ScrollTargetIndex;

    private void QueryItemRange(int FromIndex, int UntilIndex)
    {
      //Debug.WriteLine($"QueryItemRange: {FromIndex} {UntilIndex}");

      foreach (var IndexContentEntry in IndexContentDictionary.ToArray())
      {
        var Index = IndexContentEntry.Key;

        if (Index < FromIndex || Index > UntilIndex)
        {
          var OldContent = IndexContentEntry.Value;

          if (OldContent != null)
          {
            ItemRecycleEvent?.Invoke(OldContent.SectionIndex, OldContent.ItemIndex, OldContent.Item);

            IndexContentDictionary.Remove(Index);
          }
        }
      }

      var SectionCount = SectionCountQuery();
      var AbsoluteIndex = 0;

      for (var SectionIndex = 0; SectionIndex < SectionCount; SectionIndex++)
      {
        var ItemCount = ItemCountQuery(SectionIndex);

        var HeaderContent = HeaderContentQuery(SectionIndex);
        if (HeaderContent.Element != null)
        {
          if (!HasContent(AbsoluteIndex) && AbsoluteIndex.Between(FromIndex, UntilIndex))
            IndexContentDictionary[AbsoluteIndex] = new WpfFlowContentInfo() { Item = HeaderContent, SectionIndex = SectionIndex, ItemIndex = -1 };

          AbsoluteIndex++;
        }

        for (var ItemIndex = 0; ItemIndex < ItemCount; ItemIndex++)
        {
          if (!HasContent(AbsoluteIndex) && AbsoluteIndex.Between(FromIndex, UntilIndex))
            IndexContentDictionary[AbsoluteIndex] = new WpfFlowContentInfo() { Item = ItemContentQuery(SectionIndex, ItemIndex), SectionIndex = SectionIndex, ItemIndex = ItemIndex };

          AbsoluteIndex++;
        }

        var FooterContent = FooterContentQuery(SectionIndex);
        if (FooterContent.Element != null)
        {
          if (!HasContent(AbsoluteIndex) && AbsoluteIndex.Between(FromIndex, UntilIndex))
            IndexContentDictionary[AbsoluteIndex] = new WpfFlowContentInfo() { Item = FooterContent, SectionIndex = SectionIndex, ItemIndex = -1 };

          AbsoluteIndex++;
        }
      }

      foreach (var Pair in IndexContentDictionary)
      {
        AddOrUpdatePlaceholder(Pair.Key, Pair.Value?.Item.Element);
      }
    }
    private void AddPlaceholder(System.Windows.FrameworkElement Element)
    {
      PlaceholderCollection.Add(new System.Windows.Controls.ContentPresenter() { Height = Element != null ? double.NaN : 50, Content = Element });
    }
    private void AddOrUpdatePlaceholder(int AbsoluteIndex, System.Windows.FrameworkElement Element)
    {
      if (PlaceholderCollection.Count <= AbsoluteIndex)
      {
        AddPlaceholder(Element);
      }
      else
      {
        var ExistingContentPresenter = PlaceholderCollection.ElementAt(AbsoluteIndex) as System.Windows.Controls.ContentPresenter;
        if (ExistingContentPresenter != null)
        {
          ExistingContentPresenter.Content = Element;
          ExistingContentPresenter.Height = double.NaN;
        }
      }
    }
    private int GetAbsoluteIndex(int Section, int Index)
    {
      var Position = 0;

      for (var SectionIndex = 0; SectionIndex <= Section; SectionIndex++)
      {
        if (HeaderContentQuery(SectionIndex).Element != null)
          Position++;

        if (SectionIndex == Section)
        {
          Position += Index;
          break;
        }

        Position += ItemCountQuery(SectionIndex);

        if (FooterContentQuery(SectionIndex).Element != null)
          Position++;
      }

      return Position;
    }
    private bool HasContent(int Index)
    {
      return IndexContentDictionary.TryGetValue(Index, out var Content) && Content != null;
    }

    private readonly Dictionary<int, WpfFlowContentInfo> IndexContentDictionary;

    private sealed class WpfFlowContentInfo
    {
      public int SectionIndex;
      public int ItemIndex;
      public WpfFlowItem Item;
    }

    private class WpfFlowItemContainer : System.Windows.Controls.ContentControl
    {
      public WpfFlowItemContainer()
      {
      }
    }

    internal class WpfFlowItemsControl : System.Windows.Controls.ItemsControl
    {
      static WpfFlowItemsControl()
      {
        var ResourceDictionary = WpfShell.LoadResourceDictionary("InvWpfVirtualisedItemsControl.xaml");

        if (ResourceDictionary != null)
        {
          DefaultStyle = (System.Windows.Style)ResourceDictionary["BaseStyle"];
          DefaultStyle.Seal();
        }
      }

      internal bool IsScrollViewerDark
      {
        get { return ScrollViewer?.IsDark ?? IsDarkField; }
        set
        {
          this.IsDarkField = value;

          if (ScrollViewer != null)
            ScrollViewer.IsDark = value;
        }
      }

      private static readonly System.Windows.Style DefaultStyle;

      public WpfFlowItemsControl()
      {
        this.Style = DefaultStyle;
      }

      public event Action<WpfFlowScrollChangeEventArgs> ScrollChangeEvent;

      public override void OnApplyTemplate()
      {
        if (ScrollViewer != null)
          ScrollViewer.ScrollChanged -= HandleScrollChanged;

        base.OnApplyTemplate();

        this.ScrollViewer = Template.FindName("ItemsControl_ScrollViewer", this) as WpfScrollViewer;
        ScrollViewer.IsDark = IsDarkField;
        ScrollViewer.ScrollChanged += HandleScrollChanged;
      }

      protected override bool IsItemItsOwnContainerOverride(object Item)
      {
        return Item is WpfFlowItemContainer;
      }
      protected override System.Windows.DependencyObject GetContainerForItemOverride()
      {
        return new WpfFlowItemContainer();
      }

      private void HandleScrollChanged(object sender, System.Windows.Controls.ScrollChangedEventArgs e)
      {
        ScrollChangeEvent?.Invoke(new WpfFlowScrollChangeEventArgs()
        {
          VerticalChange = e.VerticalChange,
          VerticalOffset = e.VerticalOffset,
          ViewportHeight = e.ViewportHeight
        });
      }

      private WpfScrollViewer ScrollViewer;
      private bool IsDarkField;
    }

    internal struct WpfFlowScrollChangeEventArgs
    {
      public double ViewportHeight { get; set; }
      public double VerticalOffset { get; set; }
      public double VerticalChange { get; set; }
    }
  }

  internal class WpfFlowOffsetChangedEventArgs : System.Windows.RoutedEventArgs
  {
    internal WpfFlowOffsetChangedEventArgs(System.Windows.RoutedEvent routedEvent, object source)
    {
      this.RoutedEvent = routedEvent;
      this.Source = source;
    }

    public WpfFlowVirtualizingStackPanel Panel { get; set; }
    public double ViewportHeight { get; set; }
    public double VerticalOffset { get; set; }

    public string Name { get; set; }
  }

  internal delegate void WpfFlowOffsetChangedEventHandler(object sender, WpfFlowOffsetChangedEventArgs e);

  internal class WpfFlowVirtualizingStackPanel : System.Windows.Controls.VirtualizingStackPanel
  {
    static WpfFlowVirtualizingStackPanel()
    {
      ScrollChangeEvent = System.Windows.EventManager.RegisterRoutedEvent("OffsetChangedEvent", System.Windows.RoutingStrategy.Bubble, typeof(WpfFlowOffsetChangedEventHandler), typeof(WpfFlowVirtualizingStackPanel));
    }

    public static readonly System.Windows.RoutedEvent ScrollChangeEvent;

    public static void AddScrollChangeHandler(System.Windows.DependencyObject o, WpfFlowOffsetChangedEventHandler Event)
    {
      ((System.Windows.UIElement)o).AddHandler(WpfFlowVirtualizingStackPanel.ScrollChangeEvent, Event);
    }
    public static void RemoveScrollChangeHandler(System.Windows.DependencyObject o, WpfFlowOffsetChangedEventHandler Event)
    {
      ((System.Windows.UIElement)o).RemoveHandler(WpfFlowVirtualizingStackPanel.ScrollChangeEvent, Event);
    }

    protected override void OnViewportOffsetChanged(System.Windows.Vector OldViewportOffset, System.Windows.Vector NewViewportOffset)
    {
      base.OnViewportOffsetChanged(OldViewportOffset, NewViewportOffset);

      OffsetChangedInvoke("OnViewportOffsetChanged");
    }
    protected override void OnViewportSizeChanged(System.Windows.Size OldViewportSize, System.Windows.Size NewViewportSize)
    {
      base.OnViewportSizeChanged(OldViewportSize, NewViewportSize);

      OffsetChangedInvoke("OnViewportSizeChanged");
    }

    private void OffsetChangedInvoke(string Name)
    {
      this.RaiseEvent(new WpfFlowOffsetChangedEventArgs(WpfFlowVirtualizingStackPanel.ScrollChangeEvent, this) { Name = Name, Panel = this, ViewportHeight = this.ViewportHeight, VerticalOffset = this.VerticalOffset });
    }
  }

  public abstract class WpfGrid : WpfClipper
  {
    internal WpfGrid()
    {
      this.Inner = new System.Windows.Controls.Grid();
      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }

    public int ChildCount => Inner.Children.Count;
    public System.Windows.Controls.ColumnDefinitionCollection ColumnDefinitions
    {
      get { return Inner.ColumnDefinitions; }
    }
    public System.Windows.Controls.RowDefinitionCollection RowDefinitions
    {
      get { return Inner.RowDefinitions; }
    }

    public void ClearChildren()
    {
      Inner.Children.Clear();
    }
    public bool ContainsChild(System.Windows.UIElement Element)
    {
      return Inner.Children.Contains(Element);
    }
    public void AddChild(System.Windows.UIElement Element)
    {
      Inner.SafeAddChild(Element);
    }
    public void RemoveChild(System.Windows.UIElement Element)
    {
      Inner.Children.Remove(Element);
    }
    public System.Windows.UIElement GetChild(int Index)
    {
      return Inner.Children[Index];
    }

    private readonly System.Windows.Controls.Grid Inner;
  }

  internal sealed class WpfMaster : WpfGrid
  {
    internal WpfMaster()
      : base()
    {
    }
  }

  public sealed class WpfDock : WpfClipper
  {
    internal WpfDock()
      : base()
    {
      this.Panel = new WpfDockPanel();
      base.Child = Panel;
      Panel.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
      Panel.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }

    public bool IsHorizontal
    {
      get { return Panel.IsHorizontal; }
      set { Panel.IsHorizontal = value; }
    }

    public void Compose(IEnumerable<System.Windows.FrameworkElement> WpfHeaders, IEnumerable<System.Windows.FrameworkElement> WpfClients, IEnumerable<System.Windows.FrameworkElement> WpfFooters)
    {
      Panel.Compose(WpfHeaders, WpfClients, WpfFooters);
    }

    private readonly WpfDockPanel Panel;

    private sealed class WpfDockPanel : System.Windows.Controls.Panel
    {
      internal WpfDockPanel()
        : base()
      {
        this.WpfHeaderList = new Inv.DistinctList<System.Windows.FrameworkElement>();
        this.WpfClientList = new Inv.DistinctList<System.Windows.FrameworkElement>();
        this.WpfFooterList = new Inv.DistinctList<System.Windows.FrameworkElement>();
        this.WpfActiveList = new DistinctList<System.Windows.FrameworkElement>();
      }

      [Obsolete("Do not use", true)]
      public new System.Windows.Controls.UIElementCollection Children
      {
        get { return base.Children; }
      }
      public bool IsHorizontal
      {
        get { return IsHorizontalField; }
        set
        {
          if (IsHorizontalField != value)
          {
            this.IsHorizontalField = value;
            InvalidateVisual();
          }
        }
      }

      protected override System.Windows.Size MeasureOverride(System.Windows.Size constraint)
      {
        var Result = new System.Windows.Size();

        var FitSize = constraint;

        foreach (var WpfPanelList in new[] { WpfHeaderList, WpfFooterList })
        {
          foreach (var WpfElement in WpfPanelList)
          {
            if (WpfElement.Visibility != System.Windows.Visibility.Collapsed)
            {
              WpfElement.Measure(FitSize);

              var WpfDesiredSize = WpfElement.DesiredSize;

              if (IsHorizontal)
              {
                if (Result.Height < WpfDesiredSize.Height)
                  Result.Height = WpfDesiredSize.Height;

                if (WpfDesiredSize.Width > 0)
                {
                  Result.Width += WpfDesiredSize.Width;

                  if (FitSize.Width > 0)
                    FitSize.Width -= Math.Min(FitSize.Width, WpfDesiredSize.Width); // FitSize can be 5.599999 when DesiredWidth is 5.6 (which causes a measure error on set width).
                }
              }
              else
              {
                if (Result.Width < WpfDesiredSize.Width)
                  Result.Width = WpfDesiredSize.Width;

                if (WpfDesiredSize.Height > 0)
                {
                  Result.Height += WpfDesiredSize.Height;

                  if (FitSize.Height > 0)
                    FitSize.Height -= Math.Min(FitSize.Height, WpfDesiredSize.Height);
                }
              }
            }
          }
        }

        var WpfClientArray = WpfClientList.Where(C => C.Visibility != System.Windows.Visibility.Collapsed).ToArray();

        if (WpfClientArray.Length > 0)
        {
          var SharedSize = new System.Windows.Size(IsHorizontal ? FitSize.Width / WpfClientArray.Length : FitSize.Width, IsHorizontal ? FitSize.Height : FitSize.Height / WpfClientArray.Length);

          foreach (var WpfElement in WpfClientArray)
          {
            WpfElement.Measure(SharedSize);

            var WpfDesiredSize = WpfElement.DesiredSize;

            if (IsHorizontal)
            {
              if (Result.Height < WpfDesiredSize.Height)
                Result.Height = WpfDesiredSize.Height;

              if (WpfDesiredSize.Width > 0)
                Result.Width += WpfDesiredSize.Width;
            }
            else
            {
              if (Result.Width < WpfDesiredSize.Width)
                Result.Width = WpfDesiredSize.Width;

              if (WpfDesiredSize.Height > 0)
                Result.Height += WpfDesiredSize.Height;
            }
          }
        }

        return Result;
      }
      protected override System.Windows.Size ArrangeOverride(System.Windows.Size arrangeBounds)
      {
        var DockWidth = arrangeBounds.Width;
        var DockHeight = arrangeBounds.Height;

        var WpfClientArray = WpfClientList.Where(C => C.Visibility != System.Windows.Visibility.Collapsed).ToArray();

        if (IsHorizontal)
        {
          var PanelLeft = 0.0;

          var HeaderWidth = 0.0;

          foreach (var WpfElement in WpfHeaderList)
          {
            if (WpfElement.Visibility != System.Windows.Visibility.Collapsed)
            {
              var HeaderSize = WpfElement.DesiredSize;
              WpfElement.Arrange(new System.Windows.Rect(PanelLeft, 0, HeaderSize.Width, DockHeight));
              PanelLeft += HeaderSize.Width;
              HeaderWidth += HeaderSize.Width;
            }
          }

          var FooterTotal = WpfFooterList.Sum(F => F.DesiredSize.Width);
          var ClientRemainder = DockWidth - HeaderWidth - FooterTotal;

          if (WpfClientArray.Length > 0)
          {
            var SharedWidth = ClientRemainder < 0 ? 0 : ClientRemainder / WpfClientArray.Length;

            var LastClient = WpfClientArray.Last();

            foreach (var Client in WpfClientArray)
            {
              var FrameWidth = SharedWidth;
              if (ClientRemainder > 0 && Client == LastClient)
                FrameWidth += ClientRemainder - (SharedWidth * WpfClientArray.Length);

              Client.Arrange(new System.Windows.Rect(PanelLeft, 0, FrameWidth, DockHeight));
              PanelLeft += FrameWidth;
            }
          }

          var PanelRight = (ClientRemainder < 0 ? DockWidth - ClientRemainder : DockWidth);

          foreach (var WpfElement in WpfFooterList)
          {
            if (WpfElement.Visibility != System.Windows.Visibility.Collapsed)
            {
              var FooterWidth = WpfElement.DesiredSize.Width;
              PanelRight -= FooterWidth;
              WpfElement.Arrange(new System.Windows.Rect(PanelRight, 0, FooterWidth, DockHeight));
            }
          }
        }
        else
        {
          var HeaderHeight = 0.0;

          foreach (var WpfElement in WpfHeaderList)
          {
            if (WpfElement.Visibility != System.Windows.Visibility.Collapsed)
            {
              var CurrentHeight = WpfElement.DesiredSize.Height;
              WpfElement.Arrange(new System.Windows.Rect(0, HeaderHeight, DockWidth, CurrentHeight));
              HeaderHeight += CurrentHeight;
            }
          }

          var FooterTotal = WpfFooterList.Sum(F => F.DesiredSize.Height);
          var ClientRemainder = DockHeight - HeaderHeight - FooterTotal;

          if (WpfClientArray.Length > 0)
          {
            var SharedHeight = ClientRemainder < 0 ? 0 : ClientRemainder / WpfClientArray.Length;
            var LastClient = WpfClientArray.Last();

            var ClientTop = HeaderHeight;

            foreach (var WpfElement in WpfClientArray)
            {
              var FrameHeight = SharedHeight;
              if (ClientRemainder > 0 && WpfElement == LastClient)
                FrameHeight += ClientRemainder - (SharedHeight * WpfClientArray.Length);

              WpfElement.Arrange(new System.Windows.Rect(0, ClientTop, DockWidth, FrameHeight));
              ClientTop += FrameHeight;
            }
          }

          var FooterBottom = (ClientRemainder < 0 ? DockHeight - ClientRemainder : DockHeight);

          foreach (var WpfElement in WpfFooterList)
          {
            if (WpfElement.Visibility != System.Windows.Visibility.Collapsed)
            {
              var FooterHeight = WpfElement.DesiredSize.Height;
              FooterBottom -= FooterHeight;
              WpfElement.Arrange(new System.Windows.Rect(0, FooterBottom, DockWidth, FooterHeight));
            }
          }
        }

        return arrangeBounds;
      }

      public void Compose(IEnumerable<System.Windows.FrameworkElement> WpfHeaders, IEnumerable<System.Windows.FrameworkElement> WpfClients, IEnumerable<System.Windows.FrameworkElement> WpfFooters)
      {
        WpfHeaderList.Clear();
        WpfHeaderList.AddRange(WpfHeaders);

        WpfClientList.Clear();
        WpfClientList.AddRange(WpfClients);

        WpfFooterList.Clear();
        WpfFooterList.AddRange(WpfFooters.Reverse()); // will be arranged in reverse order.

        var WpfPreviousList = WpfActiveList;
        this.WpfActiveList = new DistinctList<System.Windows.FrameworkElement>(WpfHeaderList.Count + WpfClientList.Count + WpfFooterList.Count);
        WpfActiveList.AddRange(WpfHeaderList);
        WpfActiveList.AddRange(WpfClientList);
        WpfActiveList.AddRange(WpfFooterList);

        foreach (var Container in WpfActiveList.Except(WpfPreviousList))
          this.SafeAddChild(InternalChildren, Container);

        foreach (var Container in WpfPreviousList.Except(WpfActiveList))
          InternalChildren.Remove(Container);
      }

      private readonly Inv.DistinctList<System.Windows.FrameworkElement> WpfHeaderList;
      private readonly Inv.DistinctList<System.Windows.FrameworkElement> WpfClientList;
      private readonly Inv.DistinctList<System.Windows.FrameworkElement> WpfFooterList;
      private Inv.DistinctList<System.Windows.FrameworkElement> WpfActiveList;
      private bool IsHorizontalField;
    }
  }

  public sealed class WpfOverlay : WpfGrid
  {
    internal WpfOverlay()
      : base()
    {
    }
  }

  public sealed class WpfTable : WpfGrid
  {
    internal WpfTable()
      : base()
    {
    }
  }

  public sealed class WpfBoard : WpfClipper
  {
    internal WpfBoard()
    {
      this.Inner = new System.Windows.Controls.Canvas();
      base.Child = Inner;

      this.UseLayoutRounding = true;
      this.SnapsToDevicePixels = true;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }

    public IEnumerable<WpfBoardPin> GetPins()
    {
      return Inner.Children.Cast<WpfBoardPin>();
    }
    public void RemovePins()
    {
      Inner.Children.Clear();
    }
    public WpfBoardPin AddPin()
    {
      var Result = new WpfBoardPin();

      Inner.Children.Add(Result);

      return Result;
    }

    private readonly System.Windows.Controls.Canvas Inner;
  }

  public sealed class WpfBoardPin : System.Windows.Controls.Border
  {
    internal WpfBoardPin()
    {
    }
  }

  public sealed class WpfGraphic : WpfClipper
  {
    internal WpfGraphic()
    {
      this.Inner = new System.Windows.Controls.Image()
      {
        Stretch = System.Windows.Media.Stretch.Uniform
      };
      System.Windows.Media.RenderOptions.SetBitmapScalingMode(Inner, System.Windows.Media.BitmapScalingMode.Fant);

      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public System.Windows.Media.ImageSource Source
    {
      get { return Inner.Source; }
      set { Inner.Source = value; }
    }
    public System.Windows.Media.Stretch Stretch
    {
      get { return Inner.Stretch; }
      set { Inner.Stretch = value; }
    }

    private readonly System.Windows.Controls.Image Inner;
  }

  public sealed class WpfBlock : WpfClipper
  {
    internal WpfBlock()
    {
      this.Inner = new System.Windows.Controls.TextBlock()
      {
        VerticalAlignment = System.Windows.VerticalAlignment.Center,
        TextTrimming = System.Windows.TextTrimming.CharacterEllipsis,
      };

      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set => base.Child = value;
    }
    public System.Windows.TextAlignment TextAlignment
    {
      get => Inner.TextAlignment;
      set => Inner.TextAlignment = value;
    }
    public System.Windows.TextWrapping TextWrapping
    {
      get => Inner.TextWrapping;
      set => Inner.TextWrapping = value;
    }
    public System.Windows.TextTrimming TextTrimming
    {
      get => Inner.TextTrimming;
      set => Inner.TextTrimming = value;
    }
    public System.Windows.Media.Brush Foreground
    {
      get => Inner.Foreground;
      set => Inner.Foreground = value;
    }
    public double FontSize
    {
      get => Inner.FontSize;
      set => Inner.FontSize = value;
    }
    public string Text
    {
      get => Inner.Text;
      set => Inner.Text = value;
    }

    public System.Windows.Controls.TextBlock AsTextBlock()
    {
      return Inner;
    }

    private readonly System.Windows.Controls.TextBlock Inner;
  }

  public sealed class WpfLabel : WpfClipper
  {
    internal WpfLabel()
    {
      this.Inner = new System.Windows.Controls.TextBlock()
      {
        VerticalAlignment = System.Windows.VerticalAlignment.Center,
        TextTrimming = System.Windows.TextTrimming.CharacterEllipsis,
      };

      //Debug.Assert(Inner.FontSize == 12, "FontSize is expected to be set to 12."); // TODO: this actually fails on some resolutions!

      base.Child = Inner;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set => base.Child = value;
    }
    public System.Windows.TextAlignment TextAlignment
    {
      get => Inner.TextAlignment;
      set => Inner.TextAlignment = value;
    }
    public System.Windows.TextWrapping TextWrapping
    {
      get => Inner.TextWrapping;
      set => Inner.TextWrapping = value;
    }
    public System.Windows.TextTrimming TextTrimming
    {
      get => Inner.TextTrimming;
      set => Inner.TextTrimming = value;
    }
    public System.Windows.Media.Brush Foreground
    {
      get => Inner.Foreground;
      set => Inner.Foreground = value; 
    }
    public double FontSize
    {
      get => Inner.FontSize;
      set => Inner.FontSize = value;
    }
    public string Text
    {
      get => Inner.Text;
      set => Inner.Text = value;
    }

    public System.Windows.Controls.TextBlock AsTextBlock()
    {
      return Inner;
    }

    private readonly System.Windows.Controls.TextBlock Inner;
  }

  public sealed class WpfEdit : WpfClipper, WpfOverrideFocusContract
  {
    internal WpfEdit(bool SearchControl, bool PasswordMask)
    {
      if (SearchControl)
      {
        var LayoutDock = new System.Windows.Controls.DockPanel();
        base.Child = LayoutDock;
        LayoutDock.LastChildFill = true;

        // TODO: left search image watermark.

        var ClearButton = new WpfButton();
        LayoutDock.Children.Add(ClearButton);
        System.Windows.Controls.DockPanel.SetDock(ClearButton, System.Windows.Controls.Dock.Right);
        ClearButton.Focusable = false;
        ClearButton.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Center;
        ClearButton.VerticalContentAlignment = System.Windows.VerticalAlignment.Center;
        ClearButton.Visibility = System.Windows.Visibility.Collapsed;
        ClearButton.Padding = new System.Windows.Thickness(1);

        var ClearPath = new System.Windows.Shapes.Path();
        ClearPath.Data = System.Windows.Media.Geometry.Parse("M0,0 L1,1 M0,1 L1,0");
        ClearPath.Margin = new System.Windows.Thickness(1);
        ClearPath.Stretch = System.Windows.Media.Stretch.UniformToFill;
        ClearPath.StrokeThickness = 2;
        ClearPath.StrokeStartLineCap = System.Windows.Media.PenLineCap.Round;
        ClearPath.StrokeEndLineCap = System.Windows.Media.PenLineCap.Round;
        ClearButton.Content = ClearPath;

        ClearButton.Click += (Sender, Event) =>
        {
          InnerSearchBox.Text = ""; // fires change event.
        };

        this.InnerSearchBox = new System.Windows.Controls.TextBox()
        {
          IsReadOnlyCaretVisible = true,
          AcceptsReturn = false,
          AcceptsTab = false,
          TextWrapping = System.Windows.TextWrapping.NoWrap,
          Background = System.Windows.Media.Brushes.Transparent,
          BorderBrush = System.Windows.Media.Brushes.Transparent,
          BorderThickness = new System.Windows.Thickness(0),
          Padding = new System.Windows.Thickness(-2, 0, 0, 0) // fixes leading indent.
        };
        InnerSearchBox.GotFocus += (Sender, Event) => GotFocusInvoke(Sender, Event);
        InnerSearchBox.LostFocus += (Sender, Event) => LostFocusInvoke(Sender, Event);

        var CurrentForegroundColor = (System.Windows.Media.Color?)null;

        InnerSearchBox.TextChanged += (Sender, Event) =>
        {
          var IsEmpty = string.IsNullOrEmpty(InnerSearchBox.Text);
          ClearButton.Visibility = IsEmpty ? System.Windows.Visibility.Collapsed : System.Windows.Visibility.Visible;

          if (!IsEmpty)
          {
            ClearButton.Width = InnerSearchBox.FontSize;
            ClearButton.Height = ClearButton.Width;
            ClearButton.CornerRadius = new System.Windows.CornerRadius(InnerSearchBox.FontSize / 2);

            var ForegroundColor = (InnerSearchBox.Foreground as System.Windows.Media.SolidColorBrush)?.Color ?? System.Windows.Media.Colors.Black;

            if (CurrentForegroundColor != ForegroundColor)
            {
              CurrentForegroundColor = ForegroundColor;

              var HoverBrush = new System.Windows.Media.SolidColorBrush(ForegroundColor) { Opacity = 0.20F };
              HoverBrush.Freeze();

              var PressedBrush = new System.Windows.Media.SolidColorBrush(ForegroundColor) { Opacity = 0.40F };
              PressedBrush.Freeze();

              ClearButton.AsFlat(null, HoverBrush, PressedBrush);
            }

            ClearPath.Stroke = InnerSearchBox.Foreground;
          }

          TextChangedInvoke(Sender, Event);
        };
        InnerSearchBox.KeyDown += (Sender, Event) =>
        {
          if (Event.Key == System.Windows.Input.Key.Escape)
          {
            if (!string.IsNullOrEmpty(InnerSearchBox.Text))
            {
              InnerSearchBox.Text = ""; // fires change event.

              Event.Handled = true;
            }
          }
        };
        LayoutDock.Children.Add(InnerSearchBox);
      }
      else if (PasswordMask)
      {
        this.InnerPasswordBox = new System.Windows.Controls.PasswordBox()
        {
          Background = System.Windows.Media.Brushes.Transparent,
          BorderBrush = System.Windows.Media.Brushes.Transparent,
          BorderThickness = new System.Windows.Thickness(0),
          Padding = new System.Windows.Thickness(-2, 0, 0, 0),
        };
        InnerPasswordBox.PasswordChanged += (Sender, Event) => TextChangedInvoke(Sender, Event);
        InnerPasswordBox.GotFocus += (Sender, Event) => GotFocusInvoke(Sender, Event);
        InnerPasswordBox.LostFocus += (Sender, Event) => LostFocusInvoke(Sender, Event);
        base.Child = InnerPasswordBox;
      }
      else
      {
        this.InnerTextBox = new System.Windows.Controls.TextBox()
        {
          IsReadOnlyCaretVisible = true,
          AcceptsReturn = false,
          AcceptsTab = false,
          TextWrapping = System.Windows.TextWrapping.NoWrap,
          Background = System.Windows.Media.Brushes.Transparent,
          BorderBrush = System.Windows.Media.Brushes.Transparent,
          Margin = new System.Windows.Thickness(0),
          Padding = new System.Windows.Thickness(-2, 0, 0, 0), // fixes leading indent.
          BorderThickness = new System.Windows.Thickness(0),
        };
        InnerTextBox.TextChanged += (Sender, Event) => TextChangedInvoke(Sender, Event);
        InnerTextBox.GotFocus += (Sender, Event) => GotFocusInvoke(Sender, Event);
        InnerTextBox.LostFocus += (Sender, Event) => LostFocusInvoke(Sender, Event);
        base.Child = InnerTextBox;
      }
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public bool IsReadOnly
    {
      get
      {
        if (InnerTextBox != null)
          return InnerTextBox.IsReadOnly;
        else if (InnerPasswordBox != null)
          return !InnerPasswordBox.IsEnabled;
        else if (InnerSearchBox != null)
          return InnerSearchBox.IsReadOnly;
        else
          return false;
      }
      set
      {
        if (InnerTextBox != null)
          InnerTextBox.IsReadOnly = value;
        else if (InnerPasswordBox != null)
          InnerPasswordBox.IsEnabled = !value;
        else if (InnerSearchBox != null)
          InnerSearchBox.IsReadOnly = value;
      }
    }
    public string Text
    {
      get
      {
        if (InnerTextBox != null)
          return InnerTextBox.Text;
        else if (InnerPasswordBox != null)
          return InnerPasswordBox.Password;
        else if (InnerSearchBox != null)
          return InnerSearchBox.Text;
        else
          return null;
      }
      set
      {
        if (InnerTextBox != null)
          InnerTextBox.Text = value;
        else if (InnerPasswordBox != null)
          InnerPasswordBox.Password = value;
        else if (InnerSearchBox != null)
          InnerSearchBox.Text = value;
      }
    }
    public new event System.Windows.RoutedEventHandler GotFocus;
    public new event System.Windows.RoutedEventHandler LostFocus;
    public event System.Windows.RoutedEventHandler TextChanged;
    public System.Windows.Media.Brush Foreground
    {
      get
      {
        if (InnerTextBox != null)
          return InnerTextBox.Foreground;
        else if (InnerPasswordBox != null)
          return InnerPasswordBox.Foreground;
        else if (InnerSearchBox != null)
          return InnerSearchBox.Foreground;
        else
          return null;
      }
      set
      {
        if (InnerTextBox != null)
          InnerTextBox.Foreground = value;
        else if (InnerPasswordBox != null)
          InnerPasswordBox.Foreground = value;
        else if (InnerSearchBox != null)
          InnerSearchBox.Foreground = value;
      }
    }

    public System.Windows.Controls.TextBox AsTextBox()
    {
      return InnerTextBox;
    }
    public System.Windows.Controls.PasswordBox AsPasswordBox()
    {
      return InnerPasswordBox;
    }
    public System.Windows.Controls.TextBox AsSearchBox()
    {
      return InnerSearchBox;
    }
    public new void Focus()
    {
      if (InnerTextBox != null)
        InnerTextBox.Focus();
      else if (InnerPasswordBox != null)
        InnerPasswordBox.Focus();
      else if (InnerSearchBox != null)
        InnerSearchBox.Focus();
    }

    private void TextChangedInvoke(object Sender, System.Windows.RoutedEventArgs EventArgs)
    {
      if (TextChanged != null)
        TextChanged(Sender, EventArgs);
    }
    private void GotFocusInvoke(object Sender, System.Windows.RoutedEventArgs EventArgs)
    {
      if (GotFocus != null)
        GotFocus(Sender, EventArgs);
    }
    private void LostFocusInvoke(object Sender, System.Windows.RoutedEventArgs EventArgs)
    {
      if (LostFocus != null)
        LostFocus(Sender, EventArgs);
    }

    void WpfOverrideFocusContract.OverrideFocus()
    {
      Focus();
    }

    private readonly System.Windows.Controls.TextBox InnerTextBox;
    private readonly System.Windows.Controls.PasswordBox InnerPasswordBox;
    private readonly System.Windows.Controls.TextBox InnerSearchBox;
  }

  public sealed class WpfMemo : WpfClipper, WpfOverrideFocusContract
  {
    internal WpfMemo()
    {
      SetPlainText(null);
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public bool IsReadOnly
    {
      get { return IsReadOnlyField; }
      set
      {
        this.IsReadOnlyField = value;

        if (RichTextBox != null)
          RichTextBox.IsReadOnly = value;
        else if (PlainTextBox != null)
          PlainTextBox.IsReadOnly = value;
      }
    }
    public string Text
    {
      get { return TextField; }
    }
    public bool IsPlainText
    {
      get { return PlainTextBox != null; }
    }
    public bool IsRichText
    {
      get { return RichTextBox != null; }
    }
    public event System.Windows.Controls.TextChangedEventHandler TextChanged;

    public void SetPlainText(string Text)
    {
      this.TextField = Text;

      if (PlainTextBox == null)
      {
        this.PlainTextBox = new System.Windows.Controls.TextBox()
        {
          AcceptsReturn = true,
          AcceptsTab = true,
          TextWrapping = System.Windows.TextWrapping.Wrap,
          IsReadOnlyCaretVisible = true,
          VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto,
          HorizontalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto,
          Background = System.Windows.Media.Brushes.Transparent,
          BorderBrush = System.Windows.Media.Brushes.Transparent,
          BorderThickness = new System.Windows.Thickness(0),
          Padding = new System.Windows.Thickness(-2, 0, 0, 0), // fixes leading indent.
          IsReadOnly = IsReadOnlyField
        };
        base.Child = PlainTextBox;
        PlainTextBox.TextChanged += TextChangedInvoke;
      }

      if (RichTextBox != null)
      {
        PlainTextBox.FontFamily = RichTextBox.FontFamily;
        PlainTextBox.FontSize = RichTextBox.FontSize;
        PlainTextBox.FontWeight = RichTextBox.FontWeight;
        PlainTextBox.Foreground = RichTextBox.Foreground;

        this.RichTextBox = null;
      }

      PlainTextBox.Text = Text;
    }
    public void SetRichText(string Text)
    {
      this.TextField = Text;

      if (RichTextBox == null)
      {
        this.RichTextBox = new System.Windows.Controls.RichTextBox()
        {
          AcceptsReturn = true,
          AcceptsTab = true,
          //TextWrapping = System.Windows.TextWrapping.Wrap,
          IsReadOnlyCaretVisible = true,
          VerticalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto,
          HorizontalScrollBarVisibility = System.Windows.Controls.ScrollBarVisibility.Auto,
          Background = System.Windows.Media.Brushes.Transparent,
          BorderBrush = System.Windows.Media.Brushes.Transparent,
          BorderThickness = new System.Windows.Thickness(0),
          Margin = new System.Windows.Thickness(0),
          Padding = new System.Windows.Thickness(-4, 0, 0, 0), // fixes leading padding.
          HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch,
          VerticalAlignment = System.Windows.VerticalAlignment.Stretch,
          IsReadOnly = IsReadOnlyField
        };
        base.Child = RichTextBox;
        RichTextBox.TextChanged += TextChangedInvoke;

        // <FlowDocument Name="rtbFlowDoc" PageWidth="{Binding ElementName=rtb, Path=ActualWidth}" />
        var x = new System.Windows.Data.Binding("ActualWidth");
        x.Source = RichTextBox;
        RichTextBox.Document.SetBinding(System.Windows.Documents.FlowDocument.PageWidthProperty, x);

        var Style = new System.Windows.Style(typeof(System.Windows.Documents.Paragraph));
        Style.Setters.Add(new System.Windows.Setter(System.Windows.Documents.Paragraph.MarginProperty, new System.Windows.Thickness(0)));
        RichTextBox.Resources.Add(typeof(System.Windows.Documents.Paragraph), Style);

        RichTextBox.Document.PagePadding = new System.Windows.Thickness(0);
      }

      if (PlainTextBox != null)
      {
        RichTextBox.FontFamily = PlainTextBox.FontFamily;
        RichTextBox.FontSize = PlainTextBox.FontSize;
        RichTextBox.FontWeight = PlainTextBox.FontWeight;
        RichTextBox.Foreground = PlainTextBox.Foreground;

        this.PlainTextBox = null;
      }

      new System.Windows.Documents.TextRange(RichTextBox.Document.ContentStart, RichTextBox.Document.ContentEnd).Text = TextField;
    }
    public System.Windows.Controls.TextBox AsPlainTextBox()
    {
      return PlainTextBox;
    }
    public System.Windows.Controls.RichTextBox AsRichTextBox()
    {
      return RichTextBox;
    }

    internal System.Windows.Documents.TextRange GetRichTextRange(int Index, int Count)
    {
      // NOTE: this is slow but no obvious way to optimise and still get the correct behaviour.
      var WpfStartPointer = GetTextPoint(RichTextBox.Document.ContentStart, Index);
      var WpfEndPointer = GetTextPoint(RichTextBox.Document.ContentStart, Index + Count);

      return new System.Windows.Documents.TextRange(WpfStartPointer, WpfEndPointer);
    }

    private void TextChangedInvoke(object Sender, System.Windows.Controls.TextChangedEventArgs Event)
    {
      if (RichTextBox != null)
        this.TextField = new System.Windows.Documents.TextRange(RichTextBox.Document.ContentStart, RichTextBox.Document.ContentEnd).Text;
      else if (PlainTextBox != null)
        this.TextField = PlainTextBox.Text;

      if (TextChanged != null)
        TextChanged(Sender, Event);
    }

    private static System.Windows.Documents.TextPointer GetTextPoint(System.Windows.Documents.TextPointer start, int x)
    {
      var ret = start;
      var i = 0;
      while (ret != null)
      {
        string stringSoFar = new System.Windows.Documents.TextRange(ret, ret.GetPositionAtOffset(i, System.Windows.Documents.LogicalDirection.Forward)).Text;
        if (stringSoFar.Length == x)
          break;
        i++;
        if (ret.GetPositionAtOffset(i, System.Windows.Documents.LogicalDirection.Forward) == null)
          return ret.GetPositionAtOffset(i - 1, System.Windows.Documents.LogicalDirection.Forward);
      }
      ret = ret.GetPositionAtOffset(i, System.Windows.Documents.LogicalDirection.Forward);
      return ret;
    }

    void WpfOverrideFocusContract.OverrideFocus()
    {
      if (RichTextBox != null)
        RichTextBox.Focus();
      else if (PlainTextBox != null)
        PlainTextBox.Focus();
    }

    private System.Windows.Controls.TextBox PlainTextBox;
    private System.Windows.Controls.RichTextBox RichTextBox;
    private string TextField;
    private bool IsReadOnlyField;
  }

  internal sealed class WpfCalendar : System.Windows.Controls.Calendar
  {
    static WpfCalendar()
    {
      IsDarkProperty = System.Windows.DependencyProperty.Register("IsDark", typeof(bool), typeof(System.Windows.Controls.Calendar), new System.Windows.FrameworkPropertyMetadata(false));
    }

    internal WpfCalendar(WpfHost Host)
    {
      this.Host = Host;

      Margin = new System.Windows.Thickness(0);
      Padding = new System.Windows.Thickness(0);
      BorderThickness = new System.Windows.Thickness(0);
      SelectionMode = System.Windows.Controls.CalendarSelectionMode.SingleDate;
      LayoutTransform = new System.Windows.Media.ScaleTransform(1.25, 1.25);
      DisplayDateStart = new System.DateTime(1800, 1, 1);
      DisplayDateEnd = new System.DateTime(2199, 12, 31);

      this.DisplayDateStart = new System.DateTime(1800, 1, 1);
      this.DisplayDateEnd = new System.DateTime(2199, 12, 31);
      this.LayoutTransform = new System.Windows.Media.ScaleTransform(1.25, 1.25);

      var ResourceDictionary = WpfShell.LoadResourceDictionary("InvWpfMaterialCalendar.xaml");

      if (ResourceDictionary != null)
      {
        var OverrideStyle = (System.Windows.Style)ResourceDictionary["MaterialDesignCalendarPortrait"];
        OverrideStyle.Seal();

        this.Style = OverrideStyle;
      }

      IsDark = false;
    }

    public bool IsDark
    {
      get { return (bool)GetValue(IsDarkProperty); }
      set
      {
        SetValue(IsDarkProperty, value);

        SetBackgroundBrush(value);
        SetFontBrush(value);
      }
    }

    protected override void OnPreviewMouseUp(System.Windows.Input.MouseButtonEventArgs e)
    {
      base.OnPreviewMouseUp(e);

      // NOTE: workaround for a strange focus issue, where you have to click twice to 'get out' of the calendar.
      if (System.Windows.Input.Mouse.Captured is System.Windows.Controls.Primitives.CalendarItem)
        System.Windows.Input.Mouse.Capture(null);
    }

    private void SetBackgroundBrush(bool IsDark)
    {
      Background = IsDark ? Host.TranslateBrush(Inv.Colour.GraySmoke) : Host.TranslateBrush(Inv.Colour.WhiteSmoke);
    }
    private void SetFontBrush(bool IsDarkTheme)
    {
      System.Windows.Documents.TextElement.SetForeground(this, IsDarkTheme ? Host.TranslateBrush(Inv.Colour.White) : Host.TranslateBrush(Inv.Colour.Black));
    }

    private readonly WpfHost Host;

    private static readonly System.Windows.DependencyProperty IsDarkProperty;
  }

  public sealed class WpfVideo : WpfClipper
  {
    internal WpfVideo()
    {
      this.Inner = new System.Windows.Controls.MediaElement();
      base.Child = Inner;
      Inner.LoadedBehavior = System.Windows.Controls.MediaState.Manual;
      Inner.UnloadedBehavior = System.Windows.Controls.MediaState.Manual;
    }

    [Obsolete("Do not use", true)]
    public new System.Windows.UIElement Child
    {
      set { base.Child = value; }
    }
    public void LoadUri(Uri Source)
    {
      this.SourceField = Source;
      Inner.Source = null; // have to 'play' after you set the uri.
    }

    public void Play()
    {
      if (Inner.Source != SourceField)
        Inner.Source = SourceField;

      Inner.Play();
    }
    public void Pause()
    {
      Inner.Pause();
    }
    public void Stop()
    {
      Inner.Stop();
      Inner.Close();
      Inner.Source = null; // This will close the stream attached to the media element and blank the control in the visual tree.
    }

    private readonly System.Windows.Controls.MediaElement Inner;
    private Uri SourceField;
  }

  public static class WpfFoundation
  {
    public static readonly HashSet<System.Windows.Input.MouseWheelEventArgs> MouseWheelEventArgsList = new HashSet<System.Windows.Input.MouseWheelEventArgs>();

    internal static void SafeSetContent(this System.Windows.Controls.Border Parent, System.Windows.UIElement Child)
    {
      if (Parent.Child != Child)
      {
        if (Child == null)
          Parent.Child = null;
        else
          SafePlaceChild(Parent, Child, () => Parent.Child = Child);
      }
    }
    internal static void SafeSetContent(this System.Windows.Controls.ContentControl Parent, System.Windows.UIElement Child)
    {
      if (Parent.Content != Child)
      {
        if (Child == null)
          Parent.Content = null;
        else
          SafePlaceChild(Parent, Child, () => Parent.Content = Child);
      }
    }
    internal static void SafeAddChild(this System.Windows.Controls.Panel Parent, System.Windows.UIElement Child)
    {
      SafePlaceChild(Parent, Child, () => Parent.Children.Add(Child));
    }
    internal static void SafeAddChild(this System.Windows.FrameworkElement Parent, System.Windows.Controls.UIElementCollection Collection, System.Windows.UIElement Child)
    {
      SafePlaceChild(Parent, Child, () => Collection.Add(Child));
    }

    private static void SafePlaceChild(this System.Windows.FrameworkElement Parent, System.Windows.UIElement Child, Action PlaceAction)
    {
      var Owner = System.Windows.Media.VisualTreeHelper.GetParent(Child);

      if (Owner == null)
      {
        PlaceAction();
      }
      else if (Owner != Parent)
      {
        var OwnerAsPanel = Owner as System.Windows.Controls.Panel;
        if (OwnerAsPanel != null)
        {
          OwnerAsPanel.Children.Remove(Child);
        }
        else
        {
          var OwnerAsDecorator = Owner as System.Windows.Controls.Decorator;
          if (OwnerAsDecorator != null)
          {
            if (OwnerAsDecorator.Child == Child)
              OwnerAsDecorator.Child = null;
          }
          else
          {
            var OwnerAsContentControl = Owner as System.Windows.Controls.ContentControl;
            if (OwnerAsContentControl != null)
            {
              if (OwnerAsContentControl.Content == Child)
                OwnerAsContentControl.Content = null;
            }
            else
            {
              var OwnerAsScrollContentPresenter = Owner as System.Windows.Controls.ScrollContentPresenter;
              if (OwnerAsScrollContentPresenter != null)
              {
                if (OwnerAsScrollContentPresenter.ScrollOwner.Content == Child)
                  OwnerAsScrollContentPresenter.ScrollOwner.Content = null;
              }
              else
              {
                var OwnerAsContentPresenter = Owner as System.Windows.Controls.ContentPresenter;
                if (OwnerAsContentPresenter != null)
                {
                  if (OwnerAsContentPresenter.Content == Child)
                    OwnerAsContentPresenter.Content = null;
                }
                else
                {
                  Debug.Assert(false, "Unhandled owner.");
                }
              }
            }
          }
        }

        PlaceAction();
      }
    }
  }
}