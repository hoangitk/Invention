﻿//#define CRITICALSECTION_PERFMON // TODO: .NET performance counters are full of bugs and cause all sorts of 'start service' problems in live deployment.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Globalization;

namespace Inv
{
#if CRITICALSECTION_PERFMON
  internal static class CriticalSectionPerformance
  {
    static CriticalSectionPerformance()
    {
      try
      {
        IsInstalled = PerformanceCounterCategory.Exists(Category);
      }
      catch
      {
        // NOTE: can sometimes (but not always!) fail with System.UnauthorizedAccessException "Access to the registry key 'Global' is denied."

        IsInstalled = false;
      }
    }

    internal static CriticalSectionProfile NewProfile(string Instance)
    {
      if (IsInstalled)
      {
        return new CriticalSectionProfile()
        {
          WaitsStarted = new PerformanceCounter(Category, WaitsStarted, Instance, false),
          TotalWaitTime = new PerformanceCounter(Category, TotalWaitTime, Instance, false),
          AvgWaitTime = new PerformanceCounter(Category, AvgWaitTime, Instance, false),
          WaitsStartedPerSec = new PerformanceCounter(Category, WaitsStartedPerSec, Instance, false),
          WaitTimeoutsPerSec = new PerformanceCounter(Category, WaitTimeoutsPerSec, Instance, false),
          QueueLength = new PerformanceCounter(Category, QueueLength, Instance, false),
          TotalLockTime = new PerformanceCounter(Category, TotalLockTime, Instance, false),
          AvgLockTime = new PerformanceCounter(Category, AvgLockTime, Instance, false),
        };
      }
      else
      {
        return null;
      }
    }

    private static readonly bool IsInstalled;
    private const string Category = "Forge:Locks";
    private const string WaitsStarted = "WaitsStarted";
    private const string TotalWaitTime = "TotalWaitTime";
    private const string AvgWaitTime = "AvgWaitTime";
    private const string WaitsStartedPerSec = "WaitsStartedPerSec";
    private const string WaitTimeoutsPerSec = "WaitTimeoutsPerSec";
    private const string QueueLength = "QueueLength";
    private const string TotalLockTime = "TotalLockTime";
    private const string AvgLockTime = "AvgLockTime";
  }

  internal sealed class CriticalSectionProfile
  {
    public PerformanceCounter WaitsStarted;
    public PerformanceCounter TotalWaitTime;
    public PerformanceCounter AvgWaitTime;
    public PerformanceCounter WaitsStartedPerSec;
    public PerformanceCounter WaitTimeoutsPerSec;
    public PerformanceCounter QueueLength;
    public PerformanceCounter TotalLockTime;
    public PerformanceCounter AvgLockTime;
  }
#endif

  public sealed class ExclusiveCriticalSection
  {
    public ExclusiveCriticalSection(string Name)
    {
      this.Name = Name;
      this.Handle = new ExclusiveCriticalSectionHandle(this);
#if CRITICALSECTION_PERFMON
      this.Profile = CriticalSectionPerformance.NewProfile(Name + "_X");
#endif
    }

    public string Name { get; }
    public bool IsLockHeld
    {
      get { return Monitor.IsEntered(Handle); }
    }

    public IDisposable Lock()
    {
      Enter();
      return Handle;
    }
    public bool TryEnter()
    {
      var Result = Monitor.TryEnter(Handle);
#if CRITICALSECTION_PERFMON
      if (Result)
        Win32.Kernel32.QueryPerformanceCounter(out AcquiredLock);
#endif
      return Result;
    }
    public void Enter()
    {
#if CRITICALSECTION_PERFMON
      if (Profile != null)
      {
        Profile.WaitsStarted.Increment();
        Profile.WaitsStartedPerSec.Increment();
        Profile.QueueLength.Increment();

        long WaitLock;
        Win32.Kernel32.QueryPerformanceCounter(out WaitLock);

        Monitor.Enter(Handle);

        // TODO: recursive entry?
        Win32.Kernel32.QueryPerformanceCounter(out AcquiredLock);
        Profile.TotalWaitTime.IncrementBy(AcquiredLock - WaitLock);
        Profile.AvgWaitTime.IncrementBy(AcquiredLock - WaitLock);
      }
      else
      {
        Monitor.Enter(Handle);
      }
#else
      Monitor.Enter(Handle);
#endif
    }
    public void Exit()
    {
#if CRITICALSECTION_PERFMON
      if (Profile != null)
      {
        // TODO: recursive exit?
        long ReleasedLock;
        Win32.Kernel32.QueryPerformanceCounter(out ReleasedLock);
        Profile.TotalLockTime.IncrementBy(ReleasedLock - AcquiredLock);
        Profile.AvgLockTime.IncrementBy(ReleasedLock - AcquiredLock);
        AcquiredLock = 0L;
      }
#endif

      Monitor.Exit(Handle);
    }
    public Inv.ExclusiveCriticalAccess<T> NewAccess<T>(T Value)
    {
      return new ExclusiveCriticalAccess<T>(this, Value);
    }

    private readonly ExclusiveCriticalSectionHandle Handle;
#if CRITICALSECTION_PERFMON
    private CriticalSectionProfile Profile;
    private long AcquiredLock;
#endif

    private sealed class ExclusiveCriticalSectionHandle : IDisposable
    {
      public ExclusiveCriticalSectionHandle(ExclusiveCriticalSection Section)
      {
        this.Section = Section;
      }

      void IDisposable.Dispose()
      {
        Section.Exit();
      }

      private readonly ExclusiveCriticalSection Section;
    }
  }

  public sealed class ExclusiveCriticalAccess<T>
  {
    internal ExclusiveCriticalAccess(ExclusiveCriticalSection Section, T Value)
    {
      this.Section = Section;
      Backing_Value = Value;
    }

    public T Value
    {
      get
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(Section.IsLockHeld, "Cannot read this member as the owning critical section '{0}' does not have the lock held.", Section.Name);

        return Backing_Value;
      }
      set
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(Section.IsLockHeld, "Cannot write this member as the owning critical section '{0}' does not have the lock held.", Section.Name);

        this.Backing_Value = value;
      }
    }

    private readonly ExclusiveCriticalSection Section;
    private T Backing_Value;
  }

  public sealed class ReaderWriterCriticalSection : IDisposable
  {
    public ReaderWriterCriticalSection(string Name)
    {
      this.Name = Name;
      this.Handle = new ReaderWriterLockSlim();
      this.Timeout = TimeSpan.FromSeconds(60);
#if CRITICALSECTION_PERFMON
      this.Profile = CriticalSectionPerformance.NewProfile(Name + "_S");
#endif
    }
    public void Dispose()
    {
      // NOTE: IsDisposed is used to deal with a behaviour difference between .NET 4.5.0 and .NET 4.5.1.
      //       in 4.5.0: a double dispose will raise an ObjectDisposedException
      //       in 4.5.1: a double dispose is ignored (as per the .NET specification).
      if (!IsDisposed)
      {
        IsDisposed = true;

        Handle.Dispose();
      }
    }

    public string Name { get; }
    public bool IsWriteLockHeld
    {
      get { return Handle.IsWriteLockHeld; }
    }
    public bool IsReadLockHeld
    {
      get { return Handle.IsReadLockHeld; }
    }
    public bool IsUpgradeableReadLockHeld
    {
      get { return Handle.IsUpgradeableReadLockHeld; }
    }
    public bool IsAnyLockHeld
    {
      get { return Handle.IsReadLockHeld || Handle.IsUpgradeableReadLockHeld || Handle.IsWriteLockHeld; }
    }
    public TimeSpan Timeout { get; set; }

    public Inv.ReaderWriterCriticalAccess<T> NewAccess<T>(T Value)
    {
      return new ReaderWriterCriticalAccess<T>(this, Value);
    }
    public Inv.ReaderWriterCriticalReadLock EnterReadLock()
    {
#if CRITICALSECTION_PERFMON
      var WaitLock = 0L;
      if (Profile != null)
      {
        Profile.WaitsStarted.Increment();
        Profile.WaitsStartedPerSec.Increment();
        Profile.QueueLength.Increment();

        Win32.Kernel32.QueryPerformanceCounter(out WaitLock);
      }
      try
      {
        if (!Handle.TryEnterReadLock(Timeout))
        {
          if (Profile != null)
            Profile.WaitTimeoutsPerSec.Increment();

          throw new ReaderWriterCriticalSectionTimeoutException(this);
        }

        if (Profile != null)
        {
          Win32.Kernel32.QueryPerformanceCounter(out AcquiredLock);
          Profile.TotalWaitTime.IncrementBy(AcquiredLock - WaitLock);
          Profile.AvgWaitTime.IncrementBy(AcquiredLock - WaitLock);
        }

        return new ReaderWriterCriticalReadLock(this);
      }
      finally
      {
        if (Profile != null)
          Profile.QueueLength.Decrement();
      }
#else
      if (Handle.TryEnterReadLock(Timeout))
        return new ReaderWriterCriticalReadLock(this);
      else
        throw new ReaderWriterCriticalSectionTimeoutException(this);
#endif
    }
    public void ExitReadLock()
    {
#if CRITICALSECTION_PERFMON
      if (Profile != null)
      {
        long ReleasedLock;
        Win32.Kernel32.QueryPerformanceCounter(out ReleasedLock);
        Profile.TotalLockTime.IncrementBy(ReleasedLock - AcquiredLock);
        Profile.AvgLockTime.IncrementBy(ReleasedLock - AcquiredLock);
        AcquiredLock = 0L;
      }
#endif

      Handle.ExitReadLock();
    }
    public Inv.ReaderWriterCriticalWriteLock EnterWriteLock()
    {
#if CRITICALSECTION_PERFMON
      var WaitLock = 0L;

      if (Profile != null)
      {
        Profile.WaitsStarted.Increment();
        Profile.WaitsStartedPerSec.Increment();
        Profile.QueueLength.Increment();

        Win32.Kernel32.QueryPerformanceCounter(out WaitLock);
      }
      try
      {
        if (!Handle.TryEnterWriteLock(Timeout))
        {
          if (Profile != null)
            Profile.WaitTimeoutsPerSec.Increment();

          throw new ReaderWriterCriticalSectionTimeoutException(this);
        }

        if (Profile != null)
        {
          Win32.Kernel32.QueryPerformanceCounter(out AcquiredLock);
          Profile.TotalWaitTime.IncrementBy(AcquiredLock - WaitLock);
          Profile.AvgWaitTime.IncrementBy(AcquiredLock - WaitLock);
        }

        return new ReaderWriterCriticalWriteLock(this);
      }
      finally
      {
        if (Profile != null)
          Profile.QueueLength.Decrement();
      }
#else
      if (Handle.TryEnterWriteLock(Timeout))
        return new ReaderWriterCriticalWriteLock(this);
      else
        throw new ReaderWriterCriticalSectionTimeoutException(this);
#endif
    }
    public void ExitWriteLock()
    {
#if CRITICALSECTION_PERFMON
      if (Profile != null)
      {
        long ReleasedLock;
        Win32.Kernel32.QueryPerformanceCounter(out ReleasedLock);
        Profile.TotalLockTime.IncrementBy(ReleasedLock - AcquiredLock);
        Profile.AvgLockTime.IncrementBy(ReleasedLock - AcquiredLock);
        AcquiredLock = 0L;
      }
#endif

      Handle.ExitWriteLock();
    }
    public Inv.ReaderWriterCriticalReadLock RequireAnyLock()
    {
      return IsAnyLockHeld ? null : EnterReadLock();
    }
    public Inv.ReaderWriterCriticalReadLock RequireReadLock()
    {
      return IsReadLockHeld ? null : EnterReadLock();
    }
    public void InsideReadLock(Action Action)
    {
      using (EnterReadLock())
        Action();
    }
    public T InsideReadLock<T>(Func<T> Action)
    {
      using (EnterReadLock())
        return Action();
    }
    public Inv.ReaderWriterCriticalWriteLock RequireWriteLock()
    {
      return IsWriteLockHeld ? null : EnterWriteLock();
    }
    public Inv.ReaderWriterCriticalFinalLock EnterFinalLock()
    {
      if (Handle.TryEnterWriteLock(Timeout))
        return new ReaderWriterCriticalFinalLock(this);
      else
        throw new ReaderWriterCriticalSectionTimeoutException(this);
    }
    public void InsideWriteLock(Action Action)
    {
      using (EnterWriteLock())
        Action();
    }
    public void InsideFinalLock(Action Action)
    {
      using (EnterFinalLock())
        Action();
    }
    public T InsideWriteLock<T>(Func<T> Action)
    {
      using (EnterWriteLock())
        return Action();
    }
    public T InsideFinalLock<T>(Func<T> Action)
    {
      using (EnterFinalLock())
        return Action();
    }
    public void NoTimeout()
    {
      this.Timeout = TimeSpan.FromMilliseconds(-1);
    }

    internal ReaderWriterLockSlim Handle { get; }

    private bool IsDisposed;
#if CRITICALSECTION_PERFMON
    private CriticalSectionProfile Profile;
    private long AcquiredLock;
#endif
  }

  public sealed class ReaderWriterCriticalSectionTimeoutException : Exception
  {
    public ReaderWriterCriticalSectionTimeoutException(ReaderWriterCriticalSection Section)
      : base("ReaderWriterCriticalSection timeout for '" + Section.Name + "'")
    {
    }
  }

  public sealed class ReaderWriterCriticalWriteLock : IDisposable
  {
    internal ReaderWriterCriticalWriteLock(ReaderWriterCriticalSection Section)
    {
      this.Section = Section;
    }
    public void Dispose()
    {
      if (Section != null)
      {
        Section.Handle.ExitWriteLock();
        this.Section = null;
      }
    }

    internal ReaderWriterCriticalSection Section { get; private set; }
  }

  public sealed class ReaderWriterCriticalFinalLock : IDisposable
  {
    internal ReaderWriterCriticalFinalLock(ReaderWriterCriticalSection Section)
    {
      this.Section = Section;
    }
    public void Dispose()
    {
      if (Section != null)
      {
        Section.Handle.ExitWriteLock();
        Section.Dispose();
        this.Section = null;
      }
    }

    internal ReaderWriterCriticalSection Section { get; private set; }
  }

  public sealed class ReaderWriterCriticalReadLock : IDisposable
  {
    internal ReaderWriterCriticalReadLock(ReaderWriterCriticalSection Section)
    {
      this.Section = Section;
    }
    public void Dispose()
    {
      if (Section != null)
      {
        Section.Handle.ExitReadLock();
        this.Section = null;
      }
    }

    internal ReaderWriterCriticalSection Section { get; private set; }
  }

  public sealed class ReaderWriterCriticalUpgradeableReadLock : IDisposable
  {
    internal ReaderWriterCriticalUpgradeableReadLock(ReaderWriterCriticalSection Section)
    {
      this.Section = Section;
    }
    public void Dispose()
    {
      if (Section != null)
      {
        Section.Handle.ExitUpgradeableReadLock();
        this.Section = null;
      }
    }

    internal ReaderWriterCriticalSection Section { get; private set; }
  }

  public sealed class ReaderWriterCriticalAccess<T>
  {
    internal ReaderWriterCriticalAccess(ReaderWriterCriticalSection Section, T Value)
    {
      this.Section = Section;
      Backing_Value = Value;
    }

    public T Value
    {
      get
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(Section.IsAnyLockHeld, "Cannot read this member as the owning critical section '{0}' does not have at least a read lock held.", Section.Name);

        return Backing_Value;
      }
      set
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(Section.IsWriteLockHeld, "Cannot write this member as the owning critical section '{0}' does not have a write lock held.", Section.Name);

        Backing_Value = value;
      }
    }

    private readonly ReaderWriterCriticalSection Section;
    private T Backing_Value;
  }
}