﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  public sealed class TimeRange
  {
    public TimeRange(Inv.Time? FromValue, Inv.Time? UntilValue)
    {
      this.Base = new Inv.Range<Inv.Time>(FromValue, UntilValue);
    }
    internal TimeRange(Inv.Range<Inv.Time> Base)
    {
      this.Base = Base;
    }

    public Inv.Time? From
    {
      get { return Base.From.Index; }
    }
    public Inv.Time? Until
    {
      get { return Base.Until.Index; }
    }

    public override bool Equals(object obj)
    {
      var Source = obj as Inv.TimeRange;

      if (Source != null)
        return Base.Equals(Source.Base);
      else
        return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return Base.GetHashCode();
    }
    public override string ToString()
    {
      return Base.ToString();
    }

    internal Inv.Range<Inv.Time> Base { get; private set; }
  }

  public sealed class TimeRangeSet : IEnumerable<Inv.TimeRange>
  {
    public TimeRangeSet()
    {
      this.Base = new RangeSet<Inv.Time>();
      Base.IntersectOnBoundary = false;
    }
    public TimeRangeSet(Inv.TimeRange TimeRange)
    {
      this.Base = new RangeSet<Inv.Time>(TimeRange.Base);
      Base.IntersectOnBoundary = false;
    }

    public int Count
    {
      get { return Base.Count; }
    }
    public Inv.TimeRange FirstRange
    {
      get { return AsTimeRange(Base.FirstRange); }
    }
    public Inv.TimeRange LastRange
    {
      get { return AsTimeRange(Base.LastRange); }
    }

    public override bool Equals(object obj)
    {
      var Source = obj as Inv.TimeRangeSet;

      if (Source != null)
        return Base.Equals(Source.Base);
      else
        return base.Equals(obj);
    }
    public override int GetHashCode()
    {
      return Base.GetHashCode();
    }
    public override string ToString()
    {
      return Base.ToString();
    }

    public bool IsEmpty()
    {
      return Base.IsEmpty();
    }
    public bool IsUniversal()
    {
      return Base.IsUniversal();
    }
    public bool IsEqualTo(Inv.TimeRangeSet CompareSet)
    {
      return Base.IsEqualTo(CompareSet.Base);
    }
    public bool Intersects(Inv.TimeRange Range)
    {
      return Base.Intersects(Range.Base);
    }
    public bool Intersects(Inv.Time? From, Inv.Time? Until)
    {
      return Intersects(new Inv.TimeRange(From, Until));
    }
    public bool Includes(Inv.TimeRange Range)
    {
      return Base.Includes(Range.Base);
    }
    public bool Includes(Inv.Time? From, Inv.Time? Until)
    {
      return Includes(new Inv.TimeRange(From, Until));
    }
    public Inv.TimeRangeSet Invert()
    {
      return new Inv.TimeRangeSet(Base.Invert());
    }
    public Inv.TimeRangeSet Union(Inv.Time? From, Inv.Time? Until)
    {
      return new Inv.TimeRangeSet(Base.Union(From, Until));
    }
    public Inv.TimeRangeSet Union(Inv.TimeRange Range)
    {
      return new Inv.TimeRangeSet(Base.Union(Range.Base));
    }
    public Inv.TimeRangeSet Union(Inv.TimeRangeSet UnionSet)
    {
      return new Inv.TimeRangeSet(Base.Union(UnionSet.Base));
    }
    public Inv.TimeRangeSet Subtract(Inv.Time? From, Inv.Time? Until)
    {
      return new Inv.TimeRangeSet(Base.Subtract(From, Until));
    }
    public Inv.TimeRangeSet Subtract(Inv.TimeRangeSet SubtractSet)
    {
      return new Inv.TimeRangeSet(Base.Subtract(SubtractSet.Base));
    }
    public Inv.TimeRangeSet Intersect(Inv.TimeRange Range)
    {
      return new Inv.TimeRangeSet(Base.Intersect(Range.Base));
    }
    public Inv.TimeRangeSet Intersect(Inv.TimeRangeSet IntersectSet)
    {
      return new Inv.TimeRangeSet(Base.Intersect(IntersectSet.Base));
    }

    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      return EnumerateRanges().GetEnumerator();
    }
    System.Collections.Generic.IEnumerator<Inv.TimeRange> System.Collections.Generic.IEnumerable<Inv.TimeRange>.GetEnumerator()
    {
      return EnumerateRanges().GetEnumerator();
    }

    private TimeRangeSet(Inv.RangeSet<Inv.Time> Base)
    {
      this.Base = Base;
      Base.IntersectOnBoundary = false;
    }
    private Inv.TimeRange AsTimeRange(Inv.Range<Inv.Time> Range)
    {
      return Range != null ? new Inv.TimeRange(Range) : null;
    }
    private IEnumerable<Inv.TimeRange> EnumerateRanges()
    {
      return Base.Select(R => new TimeRange(R));
    }

    public static TimeRangeSet FromTimeRanges(IEnumerable<Inv.TimeRange> TimeRanges)
    {
      var Result = Empty;

      foreach (var TimeRange in TimeRanges)
        Result = Result.Union(TimeRange);

      return Result;
    }
    public static TimeRangeSet FromTimeRangeArray(params Inv.TimeRange[] TimeRangeArray)
    {
      var Result = Empty;

      foreach (var TimeRange in TimeRangeArray)
        Result = Result.Union(TimeRange);

      return Result;
    }

    public static readonly TimeRangeSet Empty = new TimeRangeSet();
    public static readonly TimeRangeSet Universal = new TimeRangeSet(new Inv.TimeRange(null, null));

    private Inv.RangeSet<Inv.Time> Base;
  }
}