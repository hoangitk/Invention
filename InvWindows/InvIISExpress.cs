﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  public enum IISExpressBitness
  {
    Run32Bit,
    Run64Bit,
    DontCare
  }

  /// <summary>
  /// Wrapper class to launch IIS Express on a given physical path and clean up afterwards.
  /// May also launch a browser for you.
  /// 
  /// Has little to no error handling in the launch and may die if, for example:
  /// - Something else is holding a handle on the application host config file
  /// - Someone mangles the applicationhost.config resource inappropriately
  /// - Something odd happens when trying to launch the IIS Express process.
  /// 
  /// This class is not thread safe, but you probably shouldn't be trying to simultaneously start the same IIS Express instance on multiple threads anyhow.
  /// 
  /// </summary>
  public sealed class IISExpress
  {
    /// <summary>
    /// Create a new IIS Express wrapper and start it automatically.
    /// </summary>
    /// <param name="WebAppBasePhysicalPath">The root of the web app on disk.</param>
    /// <param name="SiteName">The name of the web site - also becomes the name of the IIS access logs. Defaults to "launchedSite".</param>
    /// <param name="WebAppDefaultURL">The default URL to load in the web browser ("/" by default)</param>
    /// <param name="RequestedPort">The port to listen on, or pick a random port if none is specified.</param>
    /// <param name="RequestedBitness">The requested bitness of the IIS Express instance, or the same as the launching process if none is specified.</param>
    public IISExpress(string WebAppBasePhysicalPath, string SiteName = "launchedSite", string WebAppDefaultURL = "/", int RequestedPort = -1, IISExpressBitness RequestedBitness = IISExpressBitness.DontCare)
    {
      if (RequestedPort == -1)
      {
        var rng = new Random();
        this._port = rng.Next(10000, 65535);
      }
      else
        this._port = RequestedPort;

      this._physicalPath = WebAppBasePhysicalPath;
      this._siteName = SiteName;
      this._defaultURL = string.IsNullOrEmpty(WebAppDefaultURL) ? "/" : WebAppDefaultURL;

      var iisExpress32 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86), "IIS Express", "iisexpress.exe");
      this._hasIisExpress32 = File.Exists(iisExpress32);

      var iisExpress64 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "IIS Express", "iisexpress.exe");
      this._hasIisExpress64 = File.Exists(iisExpress64);

      if (RequestedBitness == IISExpressBitness.Run32Bit && !_hasIisExpress32)
        throw new ArgumentException("32-bit IIS Express requested but not currently installed. Re-install IIS Express?", "RequestedBitness");

      if (RequestedBitness == IISExpressBitness.Run64Bit && !_hasIisExpress64)
        throw new ArgumentException("64-bit IIS Express requested but not currently installed. Re-install IIS Express?", "RequestedBitness");

      // Figure out which IIS Express we're going to use
      if (RequestedBitness == IISExpressBitness.Run64Bit)
        this._iisExpressPath = iisExpress64;
      else if (RequestedBitness == IISExpressBitness.Run32Bit)
        this._iisExpressPath = iisExpress32;
      else if (_hasIisExpress64)
        this._iisExpressPath = iisExpress64;
      else if (_hasIisExpress32)
        this._iisExpressPath = iisExpress32;
      else
        throw new Exception("IIS Express is not installed.\r\n\r\nDownload from http://www.microsoft.com/web/gallery/install.aspx?appid=IISExpress");
    }

    public bool IsRunning
    {
      get { return _iisProc != null; }
    }
    /// <summary>
    /// Start the IIS Express instance.
    /// </summary>
    public void Start()
    {
      if (_iisProc != null)
        throw new InvalidOperationException("IIS Express is already running.");

      // Set up the application host config
      var variables = new Dictionary<string, string>();
      variables["cwd"] = Directory.GetCurrentDirectory();
      variables["sitename"] = _siteName;
      variables["sitephysicalpath"] = _physicalPath;
      variables["binding"] = string.Format("*:{0}:localhost", _port);

      var configFile = new StreamReader(typeof(IISExpress).Assembly.GetManifestResourceStream(typeof(IISExpress).Assembly.GetManifestResourceNames().First(x => x.EndsWith("applicationHost.config")))).ReadToEnd();
      var finalConfig = ReplaceVariables(configFile, variables);

      this._hostConfigPath = Path.Combine(Directory.GetCurrentDirectory(), "applicationhost.config");
      File.WriteAllText(_hostConfigPath, finalConfig);

      // Start IIS Express
      var psi = new ProcessStartInfo(_iisExpressPath, string.Format(@"/site:{0} /config:""{1}""", variables["sitename"], _hostConfigPath))
      {
        WindowStyle = ProcessWindowStyle.Hidden,
        CreateNoWindow = true,
        UseShellExecute = true
      };

      _iisProc = Process.Start(psi);

      // Sleep a little while to allow IIS to start up.
      System.Threading.Thread.Sleep(1000);
    }
    /// <summary>
    /// Stop the IIS Express instance. Does nothing if it hasn't been started, silently succeeds if the attempt to kill the process fails.
    /// Thus, if you're unlucky you may end up with zombie IISExpress processes. C'est la vie.
    /// </summary>
    public void Stop()
    {
      if (_iisProc != null)
      {
        try
        {
          _iisProc.Kill();
        }
        catch (Exception) { }
        finally
        {
          _iisProc = null;
        }
      }

      if (File.Exists(_hostConfigPath))
        File.Delete(_hostConfigPath);
    }
    /// <summary>
    /// Open the user's default web browser and point it at the web application.
    /// </summary>
    public void LaunchBrowser()
    {
      Process.Start(string.Format("http://localhost:{0}{1}", _port, _defaultURL));
    }

    private static string ReplaceVariables(string input, Dictionary<string, string> variables)
    {
      var workingCopy = input;
      foreach (var varname in variables.Keys)
      {
        workingCopy = workingCopy.Replace("[" + varname + "]", variables[varname]);
      }

      return workingCopy;
    }

    private Process _iisProc;
    private string _hostConfigPath;
    private readonly string _physicalPath;
    private readonly string _defaultURL;
    private readonly int _port;
    private readonly string _iisExpressPath;
    private readonly string _siteName;
    private readonly bool _hasIisExpress32;
    private readonly bool _hasIisExpress64;
  }
}