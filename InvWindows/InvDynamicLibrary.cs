﻿//#define DISABLESAFECALLS

using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Reflection.Emit;
using System.Reflection;
using System.Threading;
using Inv.Support;

namespace Inv
{
  internal sealed class DynamicLibraryEntry
  {
    internal DynamicLibraryEntry()
    {
      this.NativeDelegates = new Dictionary<long, object>();
      this.ManagedDelegates = new Dictionary<int, Delegate>();
      this.ManagedWrapperDelegates = new Dictionary<int, Delegate>();
    }

    internal void Clear()
    {
      this.NativeDelegates.Clear();
      this.ManagedDelegates.Clear();
      this.ManagedWrapperDelegates.Clear();
    }

    public Dictionary<Int64, object> NativeDelegates;
    public Dictionary<int, Delegate> ManagedDelegates;
    public Dictionary<int, Delegate> ManagedWrapperDelegates;
  }

  public sealed class DynamicLibrary
  {
    public DynamicLibrary()
    {
      this.Handle = IntPtr.Zero;
    }
    public void Dispose()
    {
      if (Handle != IntPtr.Zero)
      {
        LibraryReferences.Remove(this.LibraryReferenceKey);
        if (!Win32.Kernel32.FreeLibrary(Handle))
          Debug.Assert(false, string.Format("Could not free library '{0}'.", Name));

        Handle = IntPtr.Zero;
      }
    }

    public void Open(string Name)
    {
      if (!TryOpen(Name))
        throw new System.ComponentModel.Win32Exception(Marshal.GetLastWin32Error(), string.Format("Could not load library '{0}'.", Name)); 
    }
    public bool TryOpen(string Name)
    {
      Debug.Assert(Handle == IntPtr.Zero, "Library must not be open.");

      this.Name = Name;
      this.Handle = Win32.Kernel32.LoadLibrary(Name);

      var Result = Handle != IntPtr.Zero;

      if (Result)
      {
        using (LibraryCriticalSection.Lock())
          LibraryReferences.Add(Name + this.GetHashCode().ToString("X"), new DynamicLibraryEntry());
      }

      return Result;
    }
    public void Close()
    {
      Debug.Assert(Handle != IntPtr.Zero, "Library must be active.");

      if (!Win32.Kernel32.FreeLibrary(Handle))
        throw new System.ComponentModel.Win32Exception(Marshal.GetLastWin32Error(), string.Format("Could not free library '{0}'.", Name));

      this.Handle = IntPtr.Zero;
      this.Name = null;

      using (LibraryCriticalSection.Lock())
      {
        if (LibraryReferences.ContainsKey(this.LibraryReferenceKey))
        {
          var Entry = LibraryReferences.RemoveValueOrDefault(this.LibraryReferenceKey);

          Debug.Assert(Entry != null, "Entry must be found for this key: " + LibraryReferenceKey);

          if (Entry != null)
            Entry.Clear();
        }
      }
    }
    public TDelegate Load<TDelegate>(string DelegateName, bool UseMarshal = false)
      where TDelegate : class
    {
      Debug.Assert(Handle != IntPtr.Zero, "Library must be active.");

      var Address = Win32.Kernel32.GetProcAddress(Handle, DelegateName);
      if (Address == IntPtr.Zero)
        throw new ApplicationException(string.Format("Could not load delegate '{0}' in library '{1}'.", DelegateName, Name));

#if DISABLESAFECALLS
      return (TDelegate)(object)Marshal.GetDelegateForFunctionPointer(Address, typeof(TDelegate));
#else
      if (UseMarshal)
        return (TDelegate)(object)Marshal.GetDelegateForFunctionPointer(Address, typeof(TDelegate));
      else
        return (TDelegate)(object)GenerateWrappedDelegateForFunctionPointer<TDelegate>(Address);
#endif
    }
    /// <summary>
    /// Generate a function pointer for the specified Delegate, wrapped in a try/catch clause that
    /// returns a HRESULT error code to the native code.
    /// </summary>
    /// <param name="d">The delegate to wrap.</param>
    /// <returns>An IntPtr that can be passed to native code directly.</returns>
    public IntPtr WrapFunctionPointerForDelegate(Delegate d)
    {
#if DISABLESAFECALLS
      return Marshal.GetFunctionPointerForDelegate(d);
#else
      if (d == null)
        return IntPtr.Zero;

      var uniqueToken = Interlocked.Increment(ref outboundWrapperUniquifier);

      GetLibraryEntry().ManagedDelegates[uniqueToken] = d;

      var wrapperDelegateType = GenerateWrapperDelegateForSpecifiedDelegate(d.GetType());

      var delegateType = d.GetType();
      var delegateMethod = delegateType.GetMethod("Invoke");
      var delegateParameters = delegateMethod.GetParameters();

      var hasReturnType = delegateMethod.ReturnType != typeof(void);

      Type[] wrapperParameterTypes;
      if (hasReturnType)
      {
        wrapperParameterTypes = new Type[delegateParameters.Count() + 1];
        Array.Copy(delegateParameters.Select(x => x.ParameterType).ToArray(), wrapperParameterTypes, delegateParameters.Count());
        wrapperParameterTypes[delegateParameters.Count()] = delegateMethod.ReturnType.MakeByRefType();
      }
      else
        wrapperParameterTypes = delegateParameters.Select(x => x.ParameterType).ToArray();

      var wrapperDelegate = new DynamicMethod("OutboundWrapper" + d.GetType().FullName.Replace('.', '_') + uniqueToken, typeof(UInt32), wrapperParameterTypes, typeof(Inv.DynamicLibrary));

      // Make sure that our parameters are defined equivalently to the user-visible delegate definition
      wrapperDelegate.DefineParameter(0, ParameterAttributes.Retval, "hr");

      for (var i = 0; i < delegateParameters.Count(); i++)
        wrapperDelegate.DefineParameter(i + 1, delegateParameters[i].Attributes, delegateParameters[i].Name);

      if (hasReturnType)
        wrapperDelegate.DefineParameter(delegateParameters.Count() + 1, ParameterAttributes.Out, "retval");

      // Build the method body
      var ilGen = wrapperDelegate.GetILGenerator();
      LocalBuilder retval = null;
      var result = ilGen.DeclareLocal(typeof(Int32));

      if (hasReturnType)
        retval = ilGen.DeclareLocal(delegateMethod.ReturnType);

      var tryBlock = ilGen.BeginExceptionBlock();

      ilGen.Ldsfld(typeof(Inv.DynamicLibrary).GetField("LibraryReferences", BindingFlags.NonPublic | BindingFlags.Static));
      ilGen.Ldstr(this.LibraryReferenceKey);
      ilGen.Call(LibraryReferences.GetType().GetMethod("get_Item", new Type[] { typeof(string) }));
      ilGen.Ldfld(typeof(DynamicLibraryEntry).GetField("ManagedDelegates", BindingFlags.Public | BindingFlags.Instance));
      ilGen.Ldc_I4(uniqueToken);

      ilGen.Callvirt(typeof(Dictionary<,>).MakeGenericType(new Type[] { typeof(int), typeof(Delegate) }).GetMethod("get_Item", new Type[] { typeof(int) }));
      ilGen.Castclass(delegateType);

      var outVars = new List<Tuple<int, LocalBuilder>>();

      // Load each parameter as appropriate
      for (var parameter = 0; parameter < delegateParameters.Count(); parameter++)
      {
        // Out parameters are passed by reference.
        if (delegateParameters[parameter].IsOut)
        {
          var outParam = new Tuple<int, LocalBuilder>(parameter, ilGen.DeclareLocal(delegateParameters[parameter].ParameterType.GetElementType()));
          outVars.Add(outParam);
          ilGen.Ldloca(outParam.Item2);
        }
        else
          ilGen.Ldarg(parameter);
      }

      // Call the native method
      ilGen.Callvirt(delegateType.GetMethod("Invoke"));

      if (hasReturnType)
      {
        ilGen.Stloc(retval);

        ilGen.Ldarg(delegateParameters.Count());
        ilGen.Ldloc(retval);

        ilGen.StindByType(delegateMethod.ReturnType);
      }

      // Resolve all the 'out' variables into their correct location
      foreach (var outParameter in outVars)
      {
        var pt = outParameter.Item2.LocalType;

        ilGen.Ldarg(outParameter.Item1);
        ilGen.Ldloc(outParameter.Item2);

        ilGen.StindByType(pt);
      }

      ilGen.Ldc_I4(0);
      ilGen.Stloc(result);

      ilGen.Leave(tryBlock);
      ilGen.BeginCatchBlock(typeof(Exception));

      ilGen.Call(this.GetType().GetMethod("GetHRForException", BindingFlags.Static | BindingFlags.NonPublic, null, new Type[] { typeof(Exception) }, null));

      ilGen.Stloc(result);
      ilGen.Leave(tryBlock);

      ilGen.EndExceptionBlock();

      ilGen.Ldloc(result);
      ilGen.Ret();

      var builtDelegate = wrapperDelegate.CreateDelegate(wrapperDelegateType);

      // Keep a reference around that should remain valid so long as this library is loaded.
      GetLibraryEntry().ManagedWrapperDelegates.Add(uniqueToken, builtDelegate);

      return Marshal.GetFunctionPointerForDelegate(builtDelegate);
#endif
    }

    private string LibraryReferenceKey
    { 
      get { return this.Name + this.Handle.ToString("X") + this.GetHashCode().ToString("X"); } 
    }

    private DynamicLibraryEntry GetLibraryEntry()
    {
      if (LibraryReferences.ContainsKey(this.LibraryReferenceKey))
        return LibraryReferences[this.LibraryReferenceKey];
      else
      {
        var entry = new DynamicLibraryEntry();
        using (LibraryCriticalSection.Lock())
          LibraryReferences.Add(this.LibraryReferenceKey, entry);
        return entry;
      }
    }
    // Mangle the method signature of the user-supplied delegate in a manner similar to P/Invoke with PreserveSig=false
    // or COM interop. Creates a delegate with the specified signature for a given unmanaged function pointer by creating an 'inner'
    // delegate with a mangled signature, and an 'outer' delegate which calls it.
    private TDelegate GenerateWrappedDelegateForFunctionPointer<TDelegate>(IntPtr Address)
      where TDelegate : class
    {
      // Generate the wrapper delegate, instantiate one representing our 
      var functionPointerDelegateDefinition = GenerateWrapperDelegateForSpecifiedDelegate(typeof(TDelegate));
      GetLibraryEntry().NativeDelegates[Address.ToInt64()] = (object)Marshal.GetDelegateForFunctionPointer(Address, functionPointerDelegateDefinition);

      var delegateType = typeof(TDelegate);
      var delegateMethod = delegateType.GetMethod("Invoke");
      var delegateParameters = delegateMethod.GetParameters();

      var delegateParameterTypes = delegateMethod.GetParameters().Select(x => x.ParameterType).ToArray();
      var wrapperDelegate = new DynamicMethod("Wrapper" + delegateType.FullName.Replace('.', '_') + "_" + Address.ToString("X"), delegateMethod.ReturnType, delegateParameterTypes, this.GetType());

      // Make sure that our parameters are defined equivalently to the user-visible delegate definition
      for (var i = 0; i < delegateParameters.Count(); i++)
        wrapperDelegate.DefineParameter(i, delegateParameters[i].Attributes, delegateParameters[i].Name);

      // Here be dynamically-generated dragons.
      var ilGen = wrapperDelegate.GetILGenerator();
      LocalBuilder retval = null;

      // Get the collection of function pointer delegates, and fetch the one we're interested in. Cast it to our previously-generated delegate type.
      // This whole chunk here is basically (with 'libraryname' and 0x12345678 being set at runtime, but before the delegate itself is created)
      // var theDelegate = (TDelegate)Inv.DynamicLibrary.LibraryReferences["libraryname"].NativeDelegates[0x12345678];
      ilGen.Ldsfld(this.GetType().GetField("LibraryReferences", BindingFlags.NonPublic | BindingFlags.Static));
      ilGen.Ldstr(this.LibraryReferenceKey);
      ilGen.Call(LibraryReferences.GetType().GetMethod("get_Item", new Type[] { typeof(string) }));
      ilGen.Ldfld(typeof(DynamicLibraryEntry).GetField("NativeDelegates", BindingFlags.Public | BindingFlags.Instance));
      ilGen.Ldc_I8(Address.ToInt64());
      ilGen.Call(typeof(Dictionary<,>).MakeGenericType(new Type[] { typeof(Int64), typeof(object) }).GetMethod("get_Item", new Type[] { typeof(Int64) }));
      ilGen.Castclass(functionPointerDelegateDefinition);

      var outVars = new List<Tuple<int, LocalBuilder>>();

      // Load each parameter as appropriate
      for (var parameter = 0; parameter < delegateParameters.Count(); parameter++)
      {
        // Out parameters are passed by reference.
        if (delegateParameters[parameter].IsOut)
        {
          var outParam = new Tuple<int, LocalBuilder>(parameter, ilGen.DeclareLocal(delegateParameters[parameter].ParameterType.GetElementType()));
          outVars.Add(outParam);
          ilGen.Ldloca(outParam.Item2);
        }
        else
          ilGen.Ldarg(parameter);
      }

      // And the return value, if there is one.
      if (delegateMethod.ReturnType != typeof(void))
      {
        retval = ilGen.DeclareLocal(delegateMethod.ReturnType);
        ilGen.Ldloca(retval);
      }

      // Call the native method
      ilGen.Callvirt(functionPointerDelegateDefinition.GetMethod("Invoke"));

      // Check the HRESULT and throw an exception if necessary
      ilGen.Call(this.GetType().GetMethod("CheckExceptionForHResult", BindingFlags.Static | BindingFlags.NonPublic, null, new Type[] { typeof(Int32) }, null));

      foreach (var outParameter in outVars)
      {
        var pt = outParameter.Item2.LocalType;

        ilGen.Ldarg(outParameter.Item1);
        ilGen.Ldloc(outParameter.Item2);

        ilGen.StindByType(pt);
      }

      // If there's a return value, push it to the stack and return it.
      if (delegateMethod.ReturnType != typeof(void))
        ilGen.Ldloc(retval);

      ilGen.Ret();

      // Compile the delegate.
      //
      // Because we can't constrain TDelegate to System.Delegate (it's a 'special class' that can't be used as a constraint - thanks, whoever decided that was a good idea)
      // we need to double-cast via Object.
      return (TDelegate)((object)wrapperDelegate.CreateDelegate(typeof(TDelegate)));
    }

    // Instance variables
    private string Name;
    public IntPtr Handle { get; private set; }

    static DynamicLibrary()
    {
      LibraryCriticalSection = new Inv.ExclusiveCriticalSection("Inv.DynamicLibrary");
      LibraryReferences = new Dictionary<string, DynamicLibraryEntry>();
      GeneratedWrappers = new Dictionary<Type, Type>();
    }

    /// <summary>
    /// Create a new delegate type that wraps the specified delegate with a HRESULT, correctly
    /// rewriting return values into out parameters
    ///
    /// i.e.
    /// delegate int SomeDelegate(bool a, out string b)
    /// ->
    /// delegate HRESULT SomeDelegateWrapper(bool a, out string b, out int retval);
    ///
    /// </summary>
    /// <param name="delegateType">The delegate type you wish to wrap</param>
    /// <returns>A Type corresponding to the wrapper delegate.</returns>
    private static Type GenerateWrapperDelegateForSpecifiedDelegate(Type delegateType)
    {
      if (GeneratedWrappers.ContainsKey(delegateType))
        return GeneratedWrappers[delegateType];

      var delegateMethod = delegateType.GetMethod("Invoke");
      var delegateParameters = delegateMethod.GetParameters();
      var retVal = delegateMethod.ReturnType;

      if (GeneratedAssembly == null)
      {
        var AssemblyName = new AssemblyName("InvDynamicLibraryWrapper");
        AssemblyName.Version = new System.Version(1, 0, 0, 0);
        GeneratedAssembly = AppDomain.CurrentDomain.DefineDynamicAssembly(AssemblyName, AssemblyBuilderAccess.RunAndSave);
        GeneratedModule = GeneratedAssembly.DefineDynamicModule("GeneratedWrappers", true);
      }

      var typeBuilder = GeneratedModule.DefineType("Wrapper" + delegateType.FullName.Replace('.', '_'), TypeAttributes.Class | TypeAttributes.Public | TypeAttributes.Sealed
                                                                                  | TypeAttributes.AnsiClass | TypeAttributes.AutoClass,
                                                                                  typeof(System.MulticastDelegate));

      var constructorBuilder = typeBuilder.DefineConstructor(MethodAttributes.RTSpecialName | MethodAttributes.HideBySig | MethodAttributes.Public, CallingConventions.Standard,
                                                             new Type[] { });

      constructorBuilder.SetImplementationFlags(MethodImplAttributes.Runtime | MethodImplAttributes.Managed);

      // Reserve an extra parameter if we need to pass an [out] return value.
      var paramTypes = new Type[delegateParameters.Length + (retVal != typeof(void) ? 1 : 0)];

      for (var i = 0; i < delegateParameters.Length; i++)
      {
        var type = delegateParameters[i].ParameterType;

        paramTypes[i] = type;
      }

      // If the original delegate has a return value, that gets pushed to the end.
      if (retVal != typeof(void))
        paramTypes[paramTypes.Length - 1] = retVal.MakeByRefType();

      // Define the Invoke method for the delegate
      var methodBuilder = typeBuilder.DefineMethod("Invoke", MethodAttributes.Public | MethodAttributes.HideBySig | MethodAttributes.NewSlot | MethodAttributes.Virtual,
                                                   typeof(UInt32), paramTypes);

      methodBuilder.DefineParameter(0, ParameterAttributes.Retval, "hr");

      for (var i = 0; i < delegateParameters.Length; i++)
      {
        var param = methodBuilder.DefineParameter(i + 1, delegateParameters[i].Attributes, delegateParameters[i].Name);
        foreach (var attr in delegateParameters[i].GetCustomAttributes(false).Where(x => x is MarshalAsAttribute).Select(x => x as MarshalAsAttribute))
        {
          var newAttr = CloneMarshalAsAttribute(attr);
          param.SetCustomAttribute(newAttr);
        }
      }

      // Propagate MarshalAs attributes on the return type to the generated wrapper type
      if (retVal != typeof(void))
      {
        var param = methodBuilder.DefineParameter(delegateParameters.Length + 1, ParameterAttributes.Out, "retval");
        foreach (var attr in delegateMethod.ReturnParameter.GetCustomAttributes(false).Where(x => x is MarshalAsAttribute).Select(x => x as MarshalAsAttribute))
        {
          var newAttr = CloneMarshalAsAttribute(attr);
          param.SetCustomAttribute(newAttr);
        }
      }

      // We don't provide an implementation, since it's effectively used as a template.
      methodBuilder.SetImplementationFlags(MethodImplAttributes.Runtime | MethodImplAttributes.Managed);

      var concreteType = typeBuilder.CreateType();
      GeneratedWrappers.Add(delegateType, concreteType);

      return concreteType;
    }

    private static CustomAttributeBuilder CloneMarshalAsAttribute(MarshalAsAttribute attr)
    {
      var attrType = typeof(MarshalAsAttribute);
      var constructor = attrType.GetConstructor(new Type[] { typeof(UnmanagedType) });
      var fieldNames = attrType.GetFields().ToArray();
      var fieldValues = fieldNames.Select(x => x.GetValue(attr)).ToArray();
      var newAttr = new CustomAttributeBuilder(constructor, new object[] { attr.Value }, fieldNames, fieldValues);
      return newAttr;
    }
    // Called from runtime-generated code.
    [DebuggerNonUserCode]
    private static void CheckExceptionForHResult(Int32 HRESULT)
    {
      // TODO: Fetch Delphi error messages and stack traces and such.
      var exception = Marshal.GetExceptionForHR(HRESULT);

      if (exception != null)
        throw exception;

      if ((HRESULT & 0x8000000) == 0x8000000)
        throw new ApplicationException("Non-success value returned from Delphi code: " + HRESULT.ToString("X"));
    }
    [DebuggerNonUserCode]
    private static Int32 GetHRForException(Exception e)
    {
      if (e is ApplicationException)
      {
        var wrapEx = new ApplicationException(e.Message + "\nStacktrace:\n" + e.StackTrace);
        Marshal.GetHRForException(wrapEx);
        unchecked { return (Int32)0x80004005; }
      }

      return Marshal.GetHRForException(e);
    }

    private static readonly Inv.ExclusiveCriticalSection LibraryCriticalSection;
    // Native delegates as returned from Marshal.GetDelegateForFunctionPointer
    private static readonly Dictionary<string, DynamicLibraryEntry> LibraryReferences;
    // Wrapper delegates we've constructed, indexed by the type of the delegate they wrap.
    private static Dictionary<Type, Type> GeneratedWrappers;
    // Internal things that we don't need multiple copies of
    private static AssemblyBuilder GeneratedAssembly;
    private static ModuleBuilder GeneratedModule;
    private static int outboundWrapperUniquifier;
  }
}
