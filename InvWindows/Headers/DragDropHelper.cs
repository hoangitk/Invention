﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.Serialization.Formatters.Binary;

namespace Win32
{
  public static class TargetHelper
  {
    static TargetHelper()
    {
      var Guid = new Guid("4657278A-411B-11D2-839A-00C04FD918D0");
      var IdentifierType = Type.GetTypeFromCLSID(Guid, true);
      DropTargetHelper = Activator.CreateInstance(IdentifierType) as IDropTargetHelper;
    }

    public static IDropTargetHelper DropTargetHelper { get; private set; }
  }

  public static class SourceHelper
  {
    static SourceHelper()
    {
      var Guid = new Guid("4657278A-411B-11D2-839A-00C04FD918D0");
      var IdentifierType = Type.GetTypeFromCLSID(Guid, true);
      DragSourceHelper = Activator.CreateInstance(IdentifierType) as IDragSourceHelper;
    }

    public static IDragSourceHelper DragSourceHelper { get; private set; }
  }

  [ComVisible(true)]
  public class DataObject : IDataObject, IDisposable
  {
    [DllImport("urlmon.dll")]
    private static extern int CopyStgMedium(ref STGMEDIUM pcstgmedSrc, ref STGMEDIUM pstgmedDest);

    private IList<KeyValuePair<FORMATETC, STGMEDIUM>> storage;

    public DataObject()
    {
      storage = new List<KeyValuePair<FORMATETC, STGMEDIUM>>();
    }
    ~DataObject()
    {
      Dispose(false);
    }
    public void Dispose()
    {
      Dispose(true);
    }

    private void ClearStorage()
    {
      lock (storage)
      {
        foreach (KeyValuePair<FORMATETC, STGMEDIUM> pair in storage)
        {
          STGMEDIUM medium = pair.Value;
          Win32.Ole32.ReleaseStgMedium(ref medium);
        }
        storage.Clear();
      }
    }
    private void Dispose(bool disposing)
    {
      if (disposing)
      {
        // No managed objects to release
      }

      ClearStorage();
    }

    #region COM IDataObject Members

    private const int OLE_E_ADVISENOTSUPPORTED = unchecked((int)0x80040003);
    private const int DV_E_FORMATETC = unchecked((int)0x80040064);
    private const int DV_E_TYMED = unchecked((int)0x80040069);
    private const int DV_E_CLIPFORMAT = unchecked((int)0x8004006A);
    private const int DV_E_DVASPECT = unchecked((int)0x8004006B);

    #region Unsupported functions
    public int DAdvise(ref FORMATETC pFormatetc, ADVF advf, IAdviseSink adviseSink, out int connection)
    {
      throw Marshal.GetExceptionForHR(OLE_E_ADVISENOTSUPPORTED);
    }
    public void DUnadvise(int connection)
    {
      throw Marshal.GetExceptionForHR(OLE_E_ADVISENOTSUPPORTED);
    }
    public int EnumDAdvise(out IEnumSTATDATA enumAdvise)
    {
      throw Marshal.GetExceptionForHR(OLE_E_ADVISENOTSUPPORTED);
    }
    public int GetCanonicalFormatEtc(ref FORMATETC formatIn, out FORMATETC formatOut)
    {
      formatOut = formatIn;
      return DV_E_FORMATETC;
    }
    public void GetDataHere(ref FORMATETC format, ref STGMEDIUM medium)
    {
      throw new NotSupportedException();
    }
    #endregion // Unsupported functions

    public IEnumFORMATETC EnumFormatEtc(DATADIR direction)
    {
      if (DATADIR.DATADIR_GET == direction)
        return new EnumFORMATETC(storage);

      throw new NotImplementedException("OLE_S_USEREG");
    }
    public void GetData(ref FORMATETC format, out STGMEDIUM medium)
    {
      lock (storage)
      {
        foreach (KeyValuePair<FORMATETC, STGMEDIUM> pair in storage)
        {
          if ((pair.Key.tymed & format.tymed) > 0
              && pair.Key.dwAspect == format.dwAspect
              && pair.Key.cfFormat == format.cfFormat)
          {
            STGMEDIUM source = pair.Value;
            medium = CopyMedium(ref source);
            return;
          }
        }
      }
      medium = new STGMEDIUM();
    }
    public int QueryGetData(ref FORMATETC format)
    {
      if ((DVASPECT.DVASPECT_CONTENT & format.dwAspect) == 0)
        return DV_E_DVASPECT;

      int ret = DV_E_TYMED;

      lock (storage)
      {
        foreach (KeyValuePair<FORMATETC, STGMEDIUM> pair in storage)
        {
          if ((pair.Key.tymed & format.tymed) > 0)
          {
            if (pair.Key.cfFormat == format.cfFormat)
            {
              return 0;
            }
            else
            {
              ret = DV_E_CLIPFORMAT;
            }
          }
          else
          {
            ret = DV_E_TYMED;
          }
        }
      }

      return ret;
    }
    public void SetData(ref FORMATETC formatIn, ref STGMEDIUM medium, bool release)
    {
      lock (storage)
      {
        foreach (KeyValuePair<FORMATETC, STGMEDIUM> pair in storage)
        {
          if ((pair.Key.tymed & formatIn.tymed) > 0
              && pair.Key.dwAspect == formatIn.dwAspect
              && pair.Key.cfFormat == formatIn.cfFormat)
          {
            storage.Remove(pair);
            break;
          }
        }
        STGMEDIUM sm = medium;
        if (!release)
          sm = CopyMedium(ref medium);

        KeyValuePair<FORMATETC, STGMEDIUM> addPair = new KeyValuePair<FORMATETC, STGMEDIUM>(formatIn, sm);
        storage.Add(addPair);
      }
    }

    private STGMEDIUM CopyMedium(ref STGMEDIUM medium)
    {
      STGMEDIUM sm = new STGMEDIUM();
      int hr = CopyStgMedium(ref medium, ref sm);
      if (hr != 0)
        throw Marshal.GetExceptionForHR(hr);

      return sm;

    }
    #endregion

    [ComVisible(true)]
    private class EnumFORMATETC : IEnumFORMATETC
    {
      private FORMATETC[] formats;
      private int currentIndex;

      internal EnumFORMATETC(IList<KeyValuePair<FORMATETC, STGMEDIUM>> storage)
      {
        formats = new FORMATETC[storage.Count];
        for (int i = 0; i < formats.Length; i++)
          formats[i] = storage[i].Key;
      }

      private EnumFORMATETC(FORMATETC[] formats)
      {
        this.formats = new FORMATETC[formats.Length];
        formats.CopyTo(this.formats, 0);
      }

      #region IEnumFORMATETC Members
      public void Clone(out IEnumFORMATETC newEnum)
      {
        EnumFORMATETC ret = new EnumFORMATETC(formats);
        ret.currentIndex = currentIndex;
        newEnum = ret;
      }
      public int Next(int celt, FORMATETC[] rgelt, int[] pceltFetched)
      {
        if (pceltFetched != null && pceltFetched.Length > 0)
          pceltFetched[0] = 0;

        int cReturn = celt;

        if (celt <= 0 || rgelt == null || currentIndex >= formats.Length)
          return 1; // S_FALSE

        if ((pceltFetched == null || pceltFetched.Length < 1) && celt != 1)
          return 1; // S_FALSE

        if (rgelt.Length < celt)
          throw new ArgumentException("The number of elements in the return array is less than the number of elements requested");

        for (int i = 0; currentIndex < formats.Length && cReturn > 0; i++, cReturn--, currentIndex++)
          rgelt[i] = formats[currentIndex];

        if (pceltFetched != null && pceltFetched.Length > 0)
          pceltFetched[0] = celt - cReturn;

        return (cReturn == 0) ? 0 : 1; // S_OK : S_FALSE
      }
      public int Reset()
      {
        currentIndex = 0;
        return 0; // S_OK
      }
      public int Skip(int celt)
      {
        if (currentIndex + celt > formats.Length)
          return 1; // S_FALSE

        currentIndex += celt;
        return 0; // S_OK
      }
      #endregion
    }
  }

  [StructLayout(LayoutKind.Sequential)]
  public struct Win32Point
  {
    public int x;
    public int y;
  }

  [ComVisible(true)]
  [ComImport]
  [Guid("4657278B-411B-11D2-839A-00C04FD918D0")]
  [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
  public interface IDropTargetHelper
  {
    void DragEnter(
        [In] IntPtr hwndTarget,
        [In, MarshalAs(UnmanagedType.Interface)] IDataObject dataObject,
        [In] ref Win32Point pt,
        [In] int effect);

    void DragLeave();

    void DragOver(
        [In] ref Win32Point pt,
        [In] int effect);

    void Drop(
        [In, MarshalAs(UnmanagedType.Interface)] IDataObject dataObject,
        [In] ref Win32Point pt,
        [In] int effect);

    void Show(
        [In] bool show);
  }

  [StructLayout(LayoutKind.Sequential)]
  public struct Win32Size
  {
    public int cx;
    public int cy;
  }

  [StructLayout(LayoutKind.Sequential)]
  public struct ShDragImage
  {
    public Win32Size sizeDragImage;
    public Win32Point ptOffset;
    public IntPtr hbmpDragImage;
    public int crColorKey;
  }

  [ComVisible(true)]
  [ComImport]
  [Guid("DE5BF786-477A-11D2-839D-00C04FD918D0")]
  [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
  public interface IDragSourceHelper
  {
    void InitializeFromBitmap(
        [In, MarshalAs(UnmanagedType.Struct)] ref ShDragImage dragImage,
        [In, MarshalAs(UnmanagedType.Interface)] IDataObject dataObject);

    void InitializeFromWindow(
        [In] IntPtr hwnd,
        [In] ref Win32Point pt,
        [In, MarshalAs(UnmanagedType.Interface)] IDataObject dataObject);
  }
}
