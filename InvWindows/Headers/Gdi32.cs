﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;

namespace Win32
{
  public sealed class Gdi32
  {
    [DllImport("gdi32.dll", ExactSpelling = true)]
    public static extern IntPtr CreateCompatibleBitmap(IntPtr hdc, int nWidth, int nHeight);

    [DllImport("gdi32.dll", ExactSpelling = true)]
    public static extern IntPtr CreateCompatibleDC(IntPtr hdc);

    [DllImport("gdi32.dll", CharSet = CharSet.Unicode)]
    public static extern IntPtr CreateDC(string szdriver, string szdevice, string szoutput, IntPtr devmode);

    [DllImport("gdi32.dll", ExactSpelling = true)]
    public static extern bool DeleteDC(IntPtr hdc);

    [DllImport("gdi32.dll", ExactSpelling = true)]
    public static extern bool DeleteObject(IntPtr hgdiobj);

    [DllImport("gdi32.dll", ExactSpelling = true)]
    public static extern int GetDeviceCaps(IntPtr hDC, int nIndex);

    [DllImport("gdi32.dll", ExactSpelling = true)]
    public static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);

    [DllImport("gdi32.dll", ExactSpelling = true)]
    public static extern int StretchDIBits(IntPtr hdc, int xdest, int ydest, int nDestWidth, int nDestHeight, int XSrc, int YSrc, int nSrcWidth, int nSrcHeight, IntPtr bitsptr, IntPtr bmiptr, int iUsage, int dwRop);

    [DllImport("gdi32.dll", ExactSpelling = true)]
    public static extern int SetDIBitsToDevice(IntPtr hdc, int xdst, int ydst, int width, int height, int xsrc, int ysrc, int start, int lines, IntPtr bitsptr, IntPtr bmiptr, int color);

    [DllImport("gdi32.dll")]
    public static extern bool BitBlt(IntPtr hObject, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hObjectSource, int nXSrc, int nYSrc, int dwRop);

    public const int SRCCOPY = 13369376;

    [StructLayout(LayoutKind.Sequential, Pack = 2)]
    public sealed class BITMAPINFOHEADER
    {
      public int biSize = 0;
      public int biWidth = 0;
      public int biHeight = 0;
      public short biPlanes = 0;
      public short biBitCount = 0;
      public int biCompression = 0;
      public int biSizeImage = 0;
      public int biXPelsPerMeter = 0;
      public int biYPelsPerMeter = 0;
      public int biClrUsed = 0;
      public int biClrImportant = 0;
    }

    public class SafeHBitmapHandle : Microsoft.Win32.SafeHandles.SafeHandleZeroOrMinusOneIsInvalid
    {
      [System.Security.SecurityCritical]
      public SafeHBitmapHandle(IntPtr preexistingHandle, bool ownsHandle)
        : base(ownsHandle)
      {
        SetHandle(preexistingHandle);
      }

      protected override bool ReleaseHandle()
      {
        return Gdi32.DeleteObject(handle);
      }
    }
  }
}
