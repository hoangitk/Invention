﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv.Material
{
  public sealed class DataTable : Inv.Panel<Inv.Dock>
  {
    public DataTable()
    {
      this.Base = Inv.Dock.NewVertical();

      this.Flow = Inv.Flow.New();
      Base.AddClient(Flow);

      this.ColumnList = new Inv.DistinctList<DataColumn>();

      this.HeaderRow = new DataRow(this);
      Base.AddHeader(HeaderRow);

      this.DataRowSet = new HashSet<DataRow>();
      DataRowSet.Add(HeaderRow);

      this.Section = Flow.AddSection();
      Section.ItemQuery += (Index) =>
      {
        var Result = new DataRow(this);

        foreach (var Column in ColumnList)
        {
          var Cell = Column.CellInvoke(Index);

          ResetCell(Column, Cell);

          Result.AddCell(Cell);
        }

        DataRowSet.Add(Result);

        return Result;
      };

      Section.RecycleEvent += (Index, Panel) =>
      {
        DataRowSet.Remove((DataRow)Panel);
      };
    }

    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Margin Margin => Base.Margin;

    public StringDataColumn AddStringColumn(string Text, int Width)
    {
      return new StringDataColumn(AddColumn(Text, Width));
    }
    public DecimalDataColumn AddDecimalColumn(string Text, int Width)
    {
      return new DecimalDataColumn(AddColumn(Text, Width));
    }
    public void SetRowCount(int Rows)
    {
      Section.SetItemCount(Rows);
    }

    internal DataColumn AddColumn(string Text, int Width)
    {
      var Result = new DataColumn(this);

      ColumnList.Add(Result);

      HeaderRow.AddCell(Result.HeaderCell);

      Result.Title = Text;
      Result.Width = Width;

      return Result;
    }
    internal void SelectRow(DataRow Row)
    {
    }
    internal void SortColumn(DataColumn Column)
    {
      Column.Sorted = !(Column.Sorted ?? false);

      foreach (var Other in ColumnList.Except(Column))
        Other.Sorted = null;
    }
    internal void ResetColumn(DataColumn Column)
    {
      var ColumnIndex = ColumnList.IndexOf(Column);

      foreach (var Row in DataRowSet)
      {
        var Cell = Row.GetCell(ColumnIndex);

        ResetCell(Column, Cell);
      }
    }
    internal void ResetCell(DataColumn Column, DataCell Cell)
    {
      Cell.Margin.Set(12, 0, 12, 0);
      Cell.Padding.Set(0, 12, 0, 12);
      Cell.Size.SetWidth(Column.Width);
    }

    private Inv.DistinctList<DataColumn> ColumnList;
    private HashSet<DataRow> DataRowSet;
    private DataRow HeaderRow;
    private Inv.Flow Flow;
    private Inv.FlowSection Section;
  }

  internal sealed class DataColumn
  {
    internal DataColumn(DataTable Table)
    {
      this.Table = Table;

      this.HeaderCell = new DataCell();

      this.Button = Inv.Button.NewStark();
      HeaderCell.Content = Button;
      Button.OverEvent += () =>
      {
        TitleLabel.Font.Colour = Theme.TextColour;
      };
      Button.AwayEvent += () =>
      {
        TitleLabel.Font.Colour = HeaderFontColour;
      };
      Button.SingleTapEvent += () =>
      {
        Table.SortColumn(this);
      };
      Button.Tooltip.ShowEvent += () =>
      {
        if (Definition != null)
        {
          if (DefinitionLabel == null)
          {
            this.DefinitionLabel = Inv.Label.New();
            Button.Tooltip.Content = DefinitionLabel;
            DefinitionLabel.Padding.Set(4);
            DefinitionLabel.Background.Colour = Inv.Colour.DimGray;
            DefinitionLabel.Font.Normal();
            DefinitionLabel.Font.Colour = Inv.Colour.White;
          }

          DefinitionLabel.Text = Definition;
        }
      };

      this.Dock = Inv.Dock.NewHorizontal();
      Button.Content = Dock;

      this.SortGraphic = Inv.Graphic.New();
      Dock.AddHeader(SortGraphic);
      SortGraphic.Size.Set(16);
      SortGraphic.Visibility.Collapse();

      this.TitleLabel = Inv.Label.New();
      Dock.AddClient(TitleLabel);
      TitleLabel.Font.Colour = HeaderFontColour;
      TitleLabel.Font.Size = 14;
      TitleLabel.Font.Medium();
      TitleLabel.LineWrapping = false;
    }

    public string Title
    {
      get => TitleLabel.Text;
      set => TitleLabel.Text = value;
    }
    public string Definition { get; set; }
    public int? Width
    {
      get => WidthField;
      set
      {
        if (WidthField != value)
        {
          this.WidthField = value;

          Table.ResetColumn(this);
        }
      }
    }
    public bool? Sorted
    {
      get => SortedField;
      set
      {
        if (SortedField != value)
        {
          this.SortedField = value;

          if (SortedField == null)
          {
            SortGraphic.Visibility.Collapse();
          }
          else
          {
            SortGraphic.Visibility.Show();
            SortGraphic.Image = SortedField == true ? Resources.Navigation.ArrowUpward : Resources.Navigation.ArrowDownward;
          }
        }
      }
    }
    public Inv.Justify Justify => TitleLabel.Justify;

    internal event Func<int, DataCell> CellQuery;
    internal DataCell HeaderCell { get; }

    internal DataCell CellInvoke(int Index)
    {
      return CellQuery?.Invoke(Index);
    }

    private DataTable Table;
    private Inv.Button Button;
    private Inv.Dock Dock;
    private Inv.Label TitleLabel;
    private Inv.Label DefinitionLabel;
    private Inv.Graphic SortGraphic;
    private int? WidthField;
    private bool? SortedField;

    private static Inv.Colour HeaderFontColour => Theme.SubtleColour;
  }

  internal sealed class DataRow : Inv.Panel<Inv.Button>
  {
    internal DataRow(DataTable Table)
    {
      this.Table = Table;

      this.Base = Inv.Button.NewStark();
      Base.Background.Colour = Inv.Colour.Transparent;
      Base.OverEvent += () =>
      {
        Base.Background.Colour = Theme.EdgeColour.Opacity(0.25F);
      };
      Base.AwayEvent += () =>
      {
        Base.Background.Colour = Inv.Colour.Transparent;
      };
      Base.SingleTapEvent += () =>
      {
        Table.SelectRow(this);
      };

      this.Stack = Inv.Stack.NewHorizontal();
      Base.Content = Stack;
      Stack.Border.Set(0, 0, 0, 1);
      Stack.Border.Colour = Theme.EdgeColour;
    }

    internal Inv.Border Border => Base.Border;

    internal void AddCell(DataCell Cell)
    {
      Stack.AddPanel(Cell);
    }
    internal DataCell GetCell(int Index)
    {
      return (DataCell)(Stack.GetPanels().ElementAt(Index));
    }

    private readonly DataTable Table;
    private readonly Inv.Stack Stack;
  }

  internal sealed class DataCell : Inv.Panel<Inv.Frame>
  {
    internal DataCell()
    {
      this.Base = Inv.Frame.New();
    }

    public Inv.Panel Content
    {
      get => Base.Content;
      set => Base.Content = value;
    }

    internal Inv.Margin Margin => Base.Margin;
    internal Inv.Padding Padding => Base.Padding;
    internal Inv.Size Size => Base.Size;
    internal Inv.Border Border => Base.Border;
  }

  public sealed class StringDataColumn
  {
    internal StringDataColumn(DataColumn Base)
    {
      this.Base = Base;
      Base.CellQuery += (Index) =>
      {
        var Result = new StringDataCell();

        Result.Justify.Set(Base.Justify.Get());
        Result.Content = CellQuery?.Invoke(Index);

        return Result;
      };
    }

    public Inv.Justify Justify => Base.Justify;
    public string Definition
    {
      get => Base.Definition;
      set => Base.Definition = value;
    }
    public int? Width
    {
      get => Base.Width;
      set => Base.Width = value;
    }
    public event Func<int, string> CellQuery;

    private DataColumn Base;
  }

  internal sealed class StringDataCell : Inv.Panel<DataCell>
  {
    internal StringDataCell()
    {
      this.Base = new DataCell();

      this.Label = Inv.Label.New();
      Base.Content = Label;
      Label.Font.Size = 14;
      Label.LineWrapping = false;
    }

    public Inv.Justify Justify => Label.Justify;
    public string Content
    {
      get => Label.Text;
      set => Label.Text = value;
    }

    public static implicit operator DataCell(StringDataCell Self) => Self?.Base;

    private Inv.Label Label;
  }

  public sealed class DecimalDataColumn
  {
    internal DecimalDataColumn(DataColumn Base)
    {
      this.Base = Base;
      Base.CellQuery += (Index) =>
      {
        var Result = new DecimalDataCell();

        Result.Justify.Set(Base.Justify.Get());
        Result.Content = CellQuery?.Invoke(Index);

        return Result;
      };
    }

    public Inv.Justify Justify => Base.Justify;
    public string Definition
    {
      get => Base.Definition;
      set => Base.Definition = value;
    }
    public event Func<int, decimal> CellQuery;

    private DataColumn Base;
  }

  internal sealed class DecimalDataCell : Inv.Panel<DataCell>
  {
    internal DecimalDataCell()
    {
      this.Base = new DataCell();

      this.Label = Inv.Label.New();
      Base.Content = Label;
      Label.Font.Size = 14;
      Label.LineWrapping = false;
    }

    public Inv.Justify Justify => Label.Justify;
    public decimal? Content
    {
      get 
      {
        if (decimal.TryParse(Label.Text, out var Result))
          return Result;
        else
          return (decimal?)null;
      }
      set => Label.Text = value.ToString();
    }

    public static implicit operator DataCell(DecimalDataCell Self) => Self?.Base;

    private Inv.Label Label;
  }
}