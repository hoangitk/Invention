﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace Win32
{
  public static class Mscoree
  {
    [DllImport("mscoree.dll")]
    public extern static int StrongNameFreeBuffer(IntPtr pbMemory);

    [DllImport("mscoree.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
    public static extern int StrongNameKeyGen(IntPtr wszKeyContainer, uint dwFlags, out IntPtr KeyBlob, out uint KeyBlobSize);
    //public extern static bool StrongNameKeyGen([MarshalAs(UnmanagedType.LPWStr)] string wszKeyContainer, StrongNameKeyGenFlags dwFlags, [Out]out IntPtr ppbKeyBlob, [Out]out ulong pcbKeyBlob);

    [DllImport("mscoree.dll", CharSet = CharSet.Unicode)]
    public static extern int StrongNameErrorInfo();

    [DllImport("mscoree.dll", CharSet = CharSet.Unicode)]
    public static extern bool StrongNameSignatureGeneration([MarshalAs(UnmanagedType.LPWStr)]string wszFilePath, [MarshalAs(UnmanagedType.LPWStr)]string wszKeyContainer, out IntPtr pbKeyBlob, ulong cbKeyBlob, out IntPtr ppbSignatureBlob, out ulong pcbSignatureBlob);

    public static byte[] StrongNameCreateBuffer()
    {
      var Buffer = IntPtr.Zero;
      try
      {
        uint BufferSize;
        if (StrongNameKeyGen(IntPtr.Zero, 0, out Buffer, out BufferSize) != 0)
          Marshal.ThrowExceptionForHR(StrongNameErrorInfo());

        if (Buffer == IntPtr.Zero)
          throw new InvalidOperationException("Failed to generate a strong name key.");

        var Result = new byte[BufferSize];

        Marshal.Copy(Buffer, Result, 0, (int)BufferSize);

        return Result;
      }
      finally
      {
        StrongNameFreeBuffer(Buffer);
      }
    }

    // FROM THE INTERNET: http://www.developerfusion.com/article/84422/the-key-to-strong-names/
    public struct StrongNamePublicKey
    {
      public uint algorithmIdSignature;
      public uint algorithmIdHash;
      public uint countBlobBytes;
      public byte keyType;
      public byte blobVersion;
      public ushort reserved;
      public uint algorithmID;
      public string magic;
      public uint keyBitLength;
      public uint rsaPublicExponent;
      public byte[] rsaModulus;
    }

    public static StrongNamePublicKey StrongNameReadPublicKey(BinaryReader br)
    {
      var Result = new StrongNamePublicKey();

      br.BaseStream.Position = 0;
      // Read PublicKeyBlob
      Result.algorithmIdSignature = br.ReadUInt32();
      Result.algorithmIdHash = br.ReadUInt32();
      Result.countBlobBytes = br.ReadUInt32();
      // Read BLOBHEADER
      Result.keyType = br.ReadByte();
      Result.blobVersion = br.ReadByte();
      Result.reserved = br.ReadUInt16();
      Result.algorithmID = br.ReadUInt32();
      // Read RSAPUBKEY
      Result.magic = new string(br.ReadChars(4));
      Result.keyBitLength = br.ReadUInt32();
      Result.rsaPublicExponent = br.ReadUInt32();
      // Read Modulus
      Result.rsaModulus = br.ReadBytes((int)Result.keyBitLength / 8);
      // Reverse byte arrays so they are formatted to read as a number
      Array.Reverse(Result.rsaModulus);

      return Result;
    }

    public struct StrongNamePrivateKey
    {
      public byte keyType;
      public byte blobVersion;
      public ushort reserved;
      public uint algorithmID;
      public string magic;
      public uint keyBitLength;
      public uint rsaPublicExponent;
      public byte[] rsaModulus;
      public byte[] rsaPrime1;
      public byte[] rsaPrime2;
      public byte[] rsaExponent1;
      public byte[] rsaExponent2;
      public byte[] rsaCoefficient;
      public byte[] rsaPrivateExponent;
    }

    public static StrongNamePrivateKey StrongNameReadPrivateKey(BinaryReader br)
    {
      var Result = new StrongNamePrivateKey();

      br.BaseStream.Position = 0;
      // Read BLOBHEADER
      Result.keyType = br.ReadByte();
      Result.blobVersion = br.ReadByte();
      Result.reserved = br.ReadUInt16();
      Result.algorithmID = br.ReadUInt32();
      // Read RSAPUBKEY
      Result.magic = new string(br.ReadChars(4));
      Result.keyBitLength = br.ReadUInt32();
      Result.rsaPublicExponent = br.ReadUInt32();
      // Read Modulus
      Result.rsaModulus = br.ReadBytes((int)Result.keyBitLength / 8);
      // Read Private Key Parameters
      Result.rsaPrime1 = br.ReadBytes((int)Result.keyBitLength / 16);
      Result.rsaPrime2 = br.ReadBytes((int)Result.keyBitLength / 16);
      Result.rsaExponent1 = br.ReadBytes((int)Result.keyBitLength / 16);
      Result.rsaExponent2 = br.ReadBytes((int)Result.keyBitLength / 16);
      Result.rsaCoefficient = br.ReadBytes((int)Result.keyBitLength / 16);
      Result.rsaPrivateExponent = br.ReadBytes((int)Result.keyBitLength / 8);
      // Format to read as a number
      Array.Reverse(Result.rsaModulus);
      Array.Reverse(Result.rsaPrime1);
      Array.Reverse(Result.rsaPrime2);
      Array.Reverse(Result.rsaExponent1);
      Array.Reverse(Result.rsaExponent2);
      Array.Reverse(Result.rsaCoefficient);
      Array.Reverse(Result.rsaPrivateExponent);

      return Result;
    }
  }
}
