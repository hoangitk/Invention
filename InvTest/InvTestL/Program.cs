﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvTestL
{
  class Program
  {
    static void Main(string[] args)
    {
      Inv.LinuxShell.Run(A =>
      {
        var Engine = Inv.ServerShell.NewEngine(A, (Identity, Application) => InvTest.Shell.Install(Application));

        Engine.Start();

        Console.ReadLine();

        Engine.Stop();
      });      
    }
  }
}
