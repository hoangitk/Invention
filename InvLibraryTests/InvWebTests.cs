﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Inv.Support;

namespace InvWeb
{
  [TestClass]
  public class URLTests
  {
    [TestMethod]
    public void Shortening()
    {
      var LongUrl = "www.kestral.com.au";

      var ShortenedUrl = WebHelper.ShortenUrl(LongUrl);

      if (ShortenedUrl == null)
        Assert.Fail("Returned URL is null");
      
      if (ShortenedUrl == "")
        Assert.Fail("Returned URL is empty");

      if (!ShortenedUrl.StartsWith("http://"))
        Assert.Fail("Returned URL doesn't start with http://");

      if (ShortenedUrl != WebHelper.ShortenUrl(LongUrl))
        Assert.Fail("Short URL is not returning the same value on subsequent calls");
    }
  }
}