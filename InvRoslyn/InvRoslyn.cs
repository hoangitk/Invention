﻿using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Emit;
using System.Diagnostics;
using Inv.Support;

namespace Inv
{
  public enum RoslynBuildPlatform
  {
    AnyCPU,
    x86,
    x64
  }

  public sealed class RoslynSpecification
  {
    public RoslynSpecification(string ProjectFilePath, RoslynBuildPlatform BuildPlatform, params string[] ConditionalSymbolArray)
    {
      this.ProjectFilePath = ProjectFilePath;
      this.BuildPlatform = BuildPlatform;
      this.ConditionalSymbolArray = ConditionalSymbolArray;
    }

    public string ProjectFilePath { get; private set; }
    public RoslynBuildPlatform BuildPlatform { get; private set; }
    public string[] ConditionalSymbolArray { get; private set; }

    internal static readonly Inv.EnumArray<RoslynBuildPlatform, Platform> PlatformMapping = new EnumArray<RoslynBuildPlatform, Platform>()
    {
      { RoslynBuildPlatform.AnyCPU, Platform.AnyCpu },
      { RoslynBuildPlatform.x86, Platform.X86 },
      { RoslynBuildPlatform.x64, Platform.X64 },
    };
  }

  public sealed class RoslynEnvironment : IDisposable
  {
    public RoslynEnvironment(RoslynSpecification Specification)
    {
      this.Specification = Specification;
      this.CodeFilePathList = new Inv.DistinctList<string>();
      this.InvPlaygroundProjectId = ProjectId.CreateNewId(); // InvPlayProject.Id;
      this.WatchSet = new HashSet<string>();
      this.AssemblyStream = new MemoryStream(1024 * 1024);
      this.SymbolStream = new MemoryStream(1024 * 1024);

      var ReloadProject = false;
      var ReloadResources = false;

      var ResourcesPath = Path.Combine(System.IO.Path.GetDirectoryName(Specification.ProjectFilePath), "Resources");

      this.Watcher = new FileSystemWatcher(Path.GetDirectoryName(Specification.ProjectFilePath));
      Watcher.Changed += (Sender, Event) =>
      {
        // TODO: WatchSet is not threadsafe.
        if (WatchSet.Contains(Event.FullPath))
        {
          if (string.Equals(Event.FullPath, Specification.ProjectFilePath, StringComparison.InvariantCultureIgnoreCase))
            ReloadProject = true;

          WatchThrottle.Fire();
        }
        else if (Path.GetDirectoryName(Event.FullPath).StartsWith(ResourcesPath, StringComparison.CurrentCultureIgnoreCase))
        {
          ReloadResources = true;

          WatchThrottle.Fire();
        }
      };
      Watcher.NotifyFilter |= NotifyFilters.Attributes;
      Watcher.NotifyFilter |= NotifyFilters.CreationTime;
      Watcher.NotifyFilter |= NotifyFilters.DirectoryName;
      Watcher.NotifyFilter |= NotifyFilters.FileName;
      Watcher.NotifyFilter |= NotifyFilters.LastAccess;
      Watcher.NotifyFilter |= NotifyFilters.LastWrite;
      Watcher.NotifyFilter |= NotifyFilters.Security;
      Watcher.NotifyFilter |= NotifyFilters.Size;
      Watcher.IncludeSubdirectories = true;
      Watcher.EnableRaisingEvents = true;

      this.WatchThrottle = new Inv.TaskThrottle();
      WatchThrottle.InitialDelay = true;
      WatchThrottle.ThresholdTime = TimeSpan.FromMilliseconds(50);
      WatchThrottle.FireEvent += () =>
      {
        if (ChangeEvent != null)
          ChangeEvent(ReloadProject, ReloadResources);

        if (ReloadProject)
          ReloadProject = false;

        if (ReloadResources)
          ReloadResources = false;
      };

      AppDomain.CurrentDomain.AssemblyResolve += AssemblyResolve;
    }

    public void Dispose()
    {
      AppDomain.CurrentDomain.AssemblyResolve -= AssemblyResolve;

      WatchThrottle.Dispose();
      Watcher.Dispose();
      AssemblyStream.Dispose();
      SymbolStream.Dispose();
    }

    public event Action<bool, bool> ChangeEvent;

    public RoslynPackage Load(bool ReloadProject)
    {
      // This is to force the compiler to include the correct reference
      var _ = typeof(Microsoft.CodeAnalysis.CSharp.Formatting.CSharpFormattingOptions);

      var ProjectFilePath = Specification.ProjectFilePath;

      var SW = new Stopwatch();

      string BuildPath = null;

      if (Solution == null || ReloadProject)
      {
        var BuildWorkspace = SW.Measure<Microsoft.CodeAnalysis.MSBuild.MSBuildWorkspace>("Create workspace", () => Microsoft.CodeAnalysis.MSBuild.MSBuildWorkspace.Create(new Dictionary<string, string> { { "CheckForSystemRuntimeDependency", "true" } }));
        var BuildProject = SW.Measure<Project>("Load project", () =>
        {
          return BuildWorkspace.OpenProjectAsync(ProjectFilePath).Result;
        });

        BuildPath = BuildProject.OutputFilePath;

        if (string.IsNullOrWhiteSpace(BuildPath))
          throw new Exception("Build output file path was not resolved in the project.");

        var CSharpProject = new Inv.CSharpProject();
        CSharpProject.LoadFromFile(ProjectFilePath);

        this.Solution = SW.Measure<Solution>("Compile playground", () =>
        {
          var InvPlaygroundSolution = new AdhocWorkspace().CurrentSolution;

          InvPlaygroundSolution = InvPlaygroundSolution.AddProject(InvPlaygroundProjectId, BuildProject.Name, BuildProject.AssemblyName, LanguageNames.CSharp);

          if (CSharpProject.Framework == "netstandard2.0")
          {
            throw new Exception("Invention does not yet support .NET Standard 2.0 projects in the live editor (Invention Play).");
          }
          else
          {
            var ProgramFilesFolder = System.Environment.GetEnvironmentVariable("ProgramFiles") ?? Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);
            var SystemFolder = $@"{ProgramFilesFolder}\Reference Assemblies\Microsoft\Framework\.NETPortable\{CSharpProject.Framework}";
            if (CSharpProject.Profile != null)
              SystemFolder += $@"\Profile\{CSharpProject.Profile}";

            foreach (var SystemFile in System.IO.Directory.GetFiles(SystemFolder, "*.dll"))
              InvPlaygroundSolution = InvPlaygroundSolution.AddMetadataReference(InvPlaygroundProjectId, MetadataReference.CreateFromFile(SystemFile));
          }

          // NOTE: metadata references never seem to be loaded by this code (BuildProject.MetadataReferences.Length == 0):
          if (BuildProject.MetadataReferences.Count == 0)
          {
            foreach (var AssemblyReference in CSharpProject.AssemblyReference.Where(R => R.HintPath != null))
              InvPlaygroundSolution = InvPlaygroundSolution.AddMetadataReference(InvPlaygroundProjectId, MetadataReference.CreateFromFile(Path.Combine(Path.GetDirectoryName(BuildPath), Path.GetFileName(AssemblyReference.HintPath))));
          }
          else
          {
            InvPlaygroundSolution = InvPlaygroundSolution.AddMetadataReferences(InvPlaygroundProjectId, BuildProject.MetadataReferences);
          }

          foreach (var ProjectReference in BuildProject.ProjectReferences)
          {
            var ReferenceProject = BuildWorkspace.CurrentSolution.GetProject(ProjectReference.ProjectId);

            // TODO: something like this to include more than one .csproj 'in memory'.
            /*if (ReferenceProject.Name == "KarismaPortable")
            {
              var PortableProjectId = ProjectId.CreateNewId(); // ReferenceProject.Id;

              InvPlaygroundSolution = InvPlaygroundSolution.AddProject(PortableProjectId, ReferenceProject.Name, ReferenceProject.AssemblyName, LanguageNames.CSharp);
              InvPlaygroundSolution = InvPlaygroundSolution.AddMetadataReferences(PortableProjectId, ReferenceProject.MetadataReferences);

              CompileProject(ref InvPlaygroundSolution, PortableProjectId, ReferenceProject.FilePath, ResourceDescriptionList);

              foreach (var NestedReference in ReferenceProject.ProjectReferences)
              {
                var NestedProject = WS.CurrentSolution.GetProject(NestedReference.ProjectId);
                InvPlaygroundSolution = InvPlaygroundSolution.AddMetadataReference(PortableProjectId, MetadataReference.CreateFromFile(NestedProject.OutputFilePath));
              }

              InvPlaygroundSolution = InvPlaygroundSolution.AddProjectReference(InvPlaygroundProjectId, new ProjectReference(PortableProjectId));
            }
            else*/
            {
              //InvPlaygroundSolution = InvPlaygroundSolution.AddMetadataReference(InvPlaygroundProjectId, MetadataReference.CreateFromFile(ReferenceProject.OutputFilePath));
              InvPlaygroundSolution = InvPlaygroundSolution.AddMetadataReference(InvPlaygroundProjectId, MetadataReference.CreateFromFile(Path.Combine(Path.GetDirectoryName(BuildPath), Path.GetFileName(ReferenceProject.OutputFilePath))));
            }
          }

          return InvPlaygroundSolution.WithProjectCompilationOptions(InvPlaygroundProjectId, new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary).
            WithWarningLevel(0). // hide all warnings.
            WithPlatform(RoslynSpecification.PlatformMapping[Specification.BuildPlatform]));
          //.WithMetadataReferenceResolver(BuildProject.CompilationOptions.MetadataReferenceResolver));
        });

        var ProjectFolderPath = Path.GetDirectoryName(ProjectFilePath);

        this.CodeFilePathList.Clear();

        var SupportedExtensionSet = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase) { ".cs", ".rs", ".resx" };

        foreach (var CSharpIncludeReference in CSharpProject.IncludeReference)
        {
          var CodeFilePath = CSharpIncludeReference.File.Path;
          if (SupportedExtensionSet.Contains(Path.GetExtension(CodeFilePath)))
            CodeFilePathList.Add(Path.Combine(ProjectFolderPath, CSharpIncludeReference.File.Path));
        }

        WatchSet.Clear();
        WatchSet.Add(ProjectFilePath); // watch the .csproj itself.
        WatchSet.AddRange(CodeFilePathList);
      }

      var ResourceDescriptionList = new Inv.DistinctList<ResourceDescription>();

      var Compilation = SW.Measure("Prepare Project", () =>
      {
        var CompileSolution = Solution;
        Compile(ref CompileSolution, InvPlaygroundProjectId, ResourceDescriptionList);
        var Project = CompileSolution.GetProject(InvPlaygroundProjectId).
          WithParseOptions(CSharpParseOptions.Default.WithPreprocessorSymbols(Specification.ConditionalSymbolArray));

        var Result = Project.GetCompilationAsync().Result;

        // needs to be after all the Roslyn assemblies have been resolved.
        if (!string.IsNullOrEmpty(BuildPath))
          this.OutputPath = Path.GetDirectoryName(BuildPath);

        return Result;
      });

      EmitResult EmitResult = null;
      Assembly InvPlaygroundAssembly = null;

      SW.Measure("Emit Assembly", () =>
      {
        AssemblyStream.SetLength(0);
        AssemblyStream.Position = 0;

        SymbolStream.SetLength(0);
        SymbolStream.Position = 0;

        EmitResult = Compilation.Emit(AssemblyStream, SymbolStream, manifestResources: ResourceDescriptionList.ToArray());

        AssemblyStream.Flush();
        SymbolStream.Flush();
      });

      if (EmitResult.Success)
      {
        SW.Measure("Load Assembly", () =>
        {
          var AssemblyBytes = AssemblyStream.GetBuffer();
          var SymbolBytes = SymbolStream.GetBuffer();

          InvPlaygroundAssembly = Assembly.Load(AssemblyBytes, SymbolBytes);
        });
      }

      return new RoslynPackage(InvPlaygroundAssembly, EmitResult.Diagnostics.Length == 0 ? string.Empty : EmitResult.Diagnostics.Select(D => D.ToString()).AsSeparatedText("\n"));
    }
    private void Compile(ref Solution InvPlaygroundSolution, ProjectId InvPlaygroundProjectId, DistinctList<ResourceDescription> ResourceDescriptionList)
    {
      foreach (var CodeFilePath in CodeFilePathList)
      {
        if (string.Equals(Path.GetExtension(CodeFilePath), ".cs", StringComparison.InvariantCultureIgnoreCase))
        {
          using (var FileStream = new FileStream(CodeFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            InvPlaygroundSolution = InvPlaygroundSolution.AddDocument(DocumentId.CreateNewId(InvPlaygroundProjectId), Path.GetFileName(CodeFilePath), Microsoft.CodeAnalysis.Text.SourceText.From(FileStream, System.Text.Encoding.UTF8), null);
        }
        else if (string.Equals(Path.GetExtension(CodeFilePath), ".rs", StringComparison.InvariantCultureIgnoreCase))
        {
          ResourceDescriptionList.Add(new ResourceDescription("Resources." + Path.GetFileName(CodeFilePath), () => System.IO.File.Open(CodeFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite), true));
        }
        else if (string.Equals(Path.GetExtension(CodeFilePath), ".resx", StringComparison.InvariantCultureIgnoreCase))
        {
          ResourceDescriptionList.Add(new ResourceDescription(Path.GetFileNameWithoutExtension(Path.GetFileName(CodeFilePath)) + ".resources", () => System.IO.File.Open(CodeFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite), true));
        }
        else
        {
          Debug.Assert(false, CodeFilePath);
        }
      }
    }
    private Assembly AssemblyResolve(object Sender, ResolveEventArgs Event)
    {
      if (Event.RequestingAssembly == null)
        return null; // inside visual studio extensions?

      if (OutputPath == null)
        return null; // this is when starting up.

      var fields = Event.Name.Split(',');
      var name = fields[0];
      var culture = fields[2];
      // failing to ignore queries for satellite resource assemblies or using [assembly: NeutralResourcesLanguage("en-US", UltimateResourceFallbackLocation.MainAssembly)] 
      // in AssemblyInfo.cs will crash the program on non en-US based system cultures.
      if (name.EndsWith(".resources") && !culture.EndsWith("neutral"))
        return null;

      var ResolvePath = Path.Combine(OutputPath, name + ".dll");
      return Assembly.LoadFile(ResolvePath);
    }

    private RoslynSpecification Specification;
    private ProjectId InvPlaygroundProjectId;
    private DistinctList<string> CodeFilePathList;
    private HashSet<string> WatchSet;
    private string OutputPath;
    private Solution Solution;
    private TaskThrottle WatchThrottle;
    private FileSystemWatcher Watcher;
    private MemoryStream AssemblyStream;
    private MemoryStream SymbolStream;
  }

  public sealed class RoslynPackage
  {
    public RoslynPackage(Assembly Assembly, string Messages)
    {
      this.Assembly = Assembly;
      this.Messages = Messages;
    }

    public Assembly Assembly { get; private set; }
    public string Messages { get; private set; }
  }
}
