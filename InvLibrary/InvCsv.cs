﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Globalization;
using System.Diagnostics;
using Inv.Support;

namespace Inv
{
  public sealed class CsvReader : IDisposable
  {
    public CsvReader(string Text)
      : this(new StringReader(Text))
    {
    }
    public CsvReader(Stream Stream)
      : this (new StreamReader(Stream, true))
    {
    }
    public CsvReader(TextReader TextReader)
    {
      this.TextParser = new TextParser(TextReader);
    }

    public bool More()
    {
      return TextParser.More();
    }
    public void ReadRecordFieldArray(params string[] FieldArray)
    {
      var Record = ReadRecord();

      if (Record.Length != FieldArray.Length)
        throw new Exception(string.Format(CultureInfo.InvariantCulture, "Expected {0} fields but found {1} fields.", FieldArray.Length, Record.Length));

      var FieldEnumerator = FieldArray.GetEnumerator();

      foreach (var Field in Record)
      {
        FieldEnumerator.MoveNext();

        if (Field != (string)FieldEnumerator.Current)
          throw new Exception(string.Format(CultureInfo.InvariantCulture, "Expected field {0} but found field {1}", Field, FieldEnumerator.Current));
      }
    }
    public string[] ReadRecord()
    {
      var Result = new List<string>();

      ReadRecordBegin();

      while (MoreRecordFields())
        Result.Add(ReadRecordField());

      ReadRecordEnd();

      return Result.ToArray();
    }
    public void ReadRecordFieldValue(string ExpectedValue)
    {
      var FoundValue = ReadRecordField();

      if (FoundValue != ExpectedValue)
        throw new Exception(string.Format(CultureInfo.InvariantCulture, "Expected field {0} but found field {1}", ExpectedValue, FoundValue));
    }
    public string ReadRecordField()
    {
      var FieldResult = new StringBuilder();
      var FieldContinue = TextParser.More() && !FieldTerminalSet.Contains(TextParser.NextCharacter());

      while (FieldContinue)
      {
        if (TextParser.NextCharacter() != '"')
        {
          // If it doesn't start with a quote then the entry is ended by a new line or the separator character.
          FieldResult.Append(TextParser.ReadCharacter());
        }
        else
        {
          // Otherwise, if it is double quoted, the entry is ended only by a closing double quote.
          // Two double quotes within the entry are resolved to one double quote and is not considered a closing quote.

          TextParser.ReadCharacter('"');

          var ResultStringBuilder = new StringBuilder();
          var QuoteContinue = TextParser.More();

          while (QuoteContinue && TextParser.More())
          {
            if (TextParser.NextCharacter() == '"')
            {
              TextParser.ReadCharacter('"');

              QuoteContinue = TextParser.More() && (TextParser.NextCharacter() == '"');

              if (QuoteContinue)
                ResultStringBuilder.Append(TextParser.ReadCharacter());
            }
            else
            {
              ResultStringBuilder.Append(TextParser.ReadCharacter());
            }
          }

          if (QuoteContinue)
            throw new Exception(ResultStringBuilder.ToString());

          FieldResult.Append(ResultStringBuilder);
        }

        FieldContinue = TextParser.More() && !FieldTerminalSet.Contains(TextParser.NextCharacter());
      }

      HasNextField = false;

      if (TextParser.More() && TextParser.NextCharacter() == ',')
      {
        TextParser.ReadCharacter(',');

        HasNextField = true;
      }

      return FieldResult.ToString();
    }
    public void ReadRecordBegin()
    {
      TextParser.ReadWhileCharacterSet(WhitespaceSet);
    }
    public void ReadRecordEnd()
    {
      TextParser.ReadWhileCharacterSet(WhitespaceSet);
    }
    public bool MoreRecordFields()
    {
      return HasNextField || (TextParser.More() && !VerticalWhitespaceSet.ContainsValue(TextParser.NextCharacter()));
    }
    public void SkipRecordField(int Count)
    {
      for (var Index = 0; Index < Count; Index++)
        ReadRecordField();
    }
    public void Dispose()
    {
      TextParser.Dispose();
    }

    private TextParser TextParser;
    private bool HasNextField;

    static CsvReader()
    {
      FieldTerminalSet = new CharacterSet()
      {
        '\0',
        '\n',
        '\r',
        ','
      };

      VerticalWhitespaceSet = new CharacterSet()
      {
        '\n',
        '\r'
      };

      HorizontalWhitespaceSet = new CharacterSet()
      {
        '\0',
        ' ',
        '\t'
      };

      WhitespaceSet = new CharacterSet();
      WhitespaceSet.AddSet(VerticalWhitespaceSet);
      WhitespaceSet.AddSet(HorizontalWhitespaceSet);
    }

    private static CharacterSet FieldTerminalSet;
    private static CharacterSet WhitespaceSet;
    private static CharacterSet HorizontalWhitespaceSet;
    private static CharacterSet VerticalWhitespaceSet;
  }

  public sealed class CsvWriter : IDisposable
  {
    public CsvWriter(Stream Stream)
      : this (new StreamWriter(Stream, Encoding.UTF8))
    {
    }
    public CsvWriter(TextWriter TextWriter)
    {
      this.TextWriter = TextWriter;
    }

    public void WriteRecordBegin()
    {
      RecordNew = true;
    }
    public void WriteRecordField(string FieldValue)
    {
      if (RecordNew)
      {
        RecordNew = false;
      }
      else
      {
        TextWriter.Write(",");
      }

      if (FieldValue != null)
        TextWriter.Write(FieldValue.EncodeCsvField());
    }
    public void WriteRecordEnd()
    {
      TextWriter.WriteLine();
    }
    public void WriteRecord(params string[] FieldValueArray)
    {
      WriteRecordBegin();

      foreach (var FieldValue in FieldValueArray)
        WriteRecordField(FieldValue);

      WriteRecordEnd();
    }
    public void Flush()
    {
      TextWriter.Flush();
    }
    public void Dispose()
    {
      TextWriter.Dispose();
    }

    private bool RecordNew;
    private TextWriter TextWriter;
  }

  public sealed class CsvTable
  {
    public CsvTable()
    {
      this.BandList = new Inv.DistinctList<CsvBand>();
    }

    public CsvColumn AddColumn(string Name)
    {
      var Band = new CsvBand(this, Name, BandList.Count);
      Band.IsStatic = true;
      var Column = Band.AddColumn(Name, 0);

      BandList.Add(Band);

      if (SpanColumn != null)
        SpanColumn.FieldIndex = BandList.Count;

      return Column;
    }
    public void SetSpanColumn(string Name)
    {
      this.SpanColumn = new CsvColumn(this, Name, BandList.Count);
    }
    public CsvReadFile ReadFile(TextReader Reader)
    {
      return new CsvReadFile(this, new Inv.CsvReader(Reader));
    }
    public CsvWriteFile WriteFile(TextWriter Writer)
    {
      return new CsvWriteFile(this, new Inv.CsvWriter(Writer));
    }

    internal Inv.DistinctList<CsvBand> BandList { get; private set; }
    internal CsvColumn SpanColumn { get; private set; }
  }

  internal sealed class CsvBand
  {
    internal CsvBand(CsvTable Table, string Name, int FieldIndex)
    {
      this.Table = Table;
      this.Name = Name;
      this.BandIndex = FieldIndex;
      this.ColumnList = new Inv.DistinctList<CsvColumn>();
    }

    internal CsvColumn AddColumn(string Name, int FieldIndex)
    {
      var Column = new CsvColumn(Table, Name, FieldIndex);
      ColumnList.Add(Column);

      return Column;
    }

    internal CsvTable Table { get; private set; }
    internal int BandIndex { get; set; }
    internal bool IsStatic { get; set; }

    private string Name;
    public Inv.DistinctList<CsvColumn> ColumnList;
  }

  public sealed class CsvColumn
  {
    internal CsvColumn(CsvTable Table, string Name, int FieldIndex)
    {
      this.Table = Table;
      this.Name = Name;
      this.FieldIndex = FieldIndex;
    }

    public string Name { get; }

    internal CsvTable Table { get; }
    internal int FieldIndex { get; set; }
  }

  public sealed class CsvWriteFile : IDisposable
  {
    internal CsvWriteFile(CsvTable Table, Inv.CsvWriter Writer)
    {
      this.Table = Table;
      this.Path = Path;
      this.Writer = Writer;

      this.ColumnList = new Inv.DistinctList<CsvColumn>();

      foreach (var Band in Table.BandList)
      {
        foreach (var Column in Band.ColumnList)
          ColumnList.AddRange(Band.ColumnList);
      }

      if (Table.SpanColumn != null)
        ColumnList.Add(Table.SpanColumn);

      Writer.WriteRecord(ColumnList.Select(C => C.Name).ToArray());
    }
    public void Dispose()
    {
      if (Writer != null)
      {
        Writer.Dispose();
        this.Writer = null;
      }
    }

    internal CsvTable Table { get; private set; }
    internal Inv.DistinctList<CsvColumn> ColumnList { get; private set; }
    internal string Path { get; private set; }

    public CsvWriteRow WriteRow()
    {
      return new CsvWriteRow(this, new string[ColumnList.Count]);
    }

    internal void CommitRow(CsvWriteRow Row)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Row.FieldArray.All(F => F != null), "All fields must be specified before committing the row.");

      Writer.WriteRecord(Row.FieldArray);
    }

    private Inv.CsvWriter Writer;
  }

  public sealed class CsvReadFile : IDisposable
  {
    internal CsvReadFile(CsvTable Table, Inv.CsvReader Reader)
    {
      this.Table = Table;
      this.Reader = Reader;

      this.ColumnList = new Inv.DistinctList<CsvColumn>();

      var StaticEntry = true;
      foreach (var Band in Table.BandList)
      {
        if (!Band.IsStatic)
        {
          StaticEntry = false;
          break;
        }
      }

      var FileHeaderArray = Reader.ReadRecord();

      if (!StaticEntry)
      {
        var BandList = Table.BandList;
        var DoubleDynamicBand = false;

        for (var Index = 1; Index < BandList.Count; Index++)
        {
          if (!BandList[Index - 1].IsStatic && !BandList[Index].IsStatic)
          {
            DoubleDynamicBand = true;
            break;
          }
        }

        if (DoubleDynamicBand)
        {
          throw new Exception("Cannot read file with adjacent bands");
        }
        else
        {
          var StaticColumnPresent = true;
          var ColumnNameSet = new Inv.DistinctList<string>();
          foreach (var Band in BandList)
          {
            if (Band.IsStatic)
            {
              var ColumnName = Band.ColumnList.First().Name;
              ColumnNameSet.Add(ColumnName);
              if (!FileHeaderArray.Contains(ColumnName, StringComparer.CurrentCultureIgnoreCase))
                StaticColumnPresent = false;
            }
          }

          if (!StaticColumnPresent)
            throw new Exception("Header must contain columns: " + ColumnNameSet.ToArray().AsSeparatedText(", "));

          for (var Index = 0; Index < BandList.Count; Index++)
          {
            var Band = BandList[Index];

            if (!Band.IsStatic)
            {
              if (Index == 0)
              {
                var NextColumnName = BandList[Index + 1].ColumnList.First().Name;
                foreach (var FileHeader in FileHeaderArray)
                {
                  if (FileHeader == NextColumnName)
                    break;

                  Band.AddColumn(FileHeader, Band.ColumnList.Count);
                }
              }
              else if (Index == BandList.Count - 1)
              {
                var LastColumnName = BandList[Index - 1].ColumnList.First().Name;
                var HitLastColumnName = false;
                foreach (var FileHeader in FileHeaderArray)
                {
                  if (FileHeader == LastColumnName)
                    HitLastColumnName = true;

                  if (HitLastColumnName)
                    Band.AddColumn(FileHeader, Band.ColumnList.Count);
                }
              }
              else
              {
                var PreviousColumnName = BandList[Index - 1].ColumnList.First().Name;
                var NextColumnName = BandList[Index + 1].ColumnList.First().Name;

                var HitPreviousColumnName = false;

                foreach (var FileHeaderName in FileHeaderArray)
                {
                  if (NextColumnName == FileHeaderName)
                    break;

                  if (HitPreviousColumnName)
                    Band.AddColumn(FileHeaderName, Band.ColumnList.Count);

                  if (PreviousColumnName == FileHeaderName)
                    HitPreviousColumnName = true;
                }
              }
            }
          }
        }
      }

      foreach (var Band in Table.BandList)
        ColumnList.AddRange(Band.ColumnList);

      var ColumnNameArray = ColumnList.Select(Index => Index.Name).ToArray();

      if (Table.SpanColumn != null)
        ColumnNameArray = ColumnNameArray.Union(Table.SpanColumn.Name).ToArray();

      if (!ColumnNameArray.Select(C => C.ToUpper()).ToArray().ShallowEqualTo(FileHeaderArray.TrimEnd(H => string.IsNullOrEmpty(H)).Select(C => C.ToUpper()).ToList()))
        throw new Exception("Header must only contain columns: " + ColumnNameArray.AsSeparatedText(", "));
    }
    public void Dispose()
    {
      if (Reader != null)
      {
        Reader.Dispose();
        this.Reader = null;
      }
    }

    internal CsvTable Table { get; private set; }
    internal Inv.DistinctList<CsvColumn> ColumnList { get; private set; }
    
    public bool HasMoreRows()
    {
      return Reader.More();
    }
    public CsvReadRow ReadRow()
    {
      var FieldArray = Reader.ReadRecord();

      if (Table.SpanColumn == null)
      {
        if (Table.BandList.Sum(Index => Index.ColumnList.Count) != FieldArray.Length)
          throw new Exception("All rows must only contain fields for columns: " + Table.BandList.SelectMany(Index => Index.ColumnList).Select(Index => Index.Name).AsSeparatedText(", "));
      }
      else
      {
        if (FieldArray.Length < Table.BandList.Sum(Index => Index.ColumnList.Count))
          throw new Exception("All rows must only contain fields for columns: " + Table.BandList.SelectMany(Index => Index.ColumnList).Select(Index => Index.Name).AsSeparatedText(", ") + " and any zero-to-many fields for span column " + Table.SpanColumn.Name + ".");
      }

      return new CsvReadRow(this, FieldArray);
    }

    private Inv.CsvReader Reader;
  }

  public sealed class CsvReadRow
  {
    public CsvReadRow(CsvReadFile Table, string[] FieldArray)
    {
      this.Table = Table;
      this.FieldArray = FieldArray;
    }

    internal CsvReadFile Table { get; private set; }

    public string ReadField(CsvColumn Column)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Table.ColumnList.Contains(Column), "Column must belong to the owning file.");

      return FieldArray[Table.ColumnList.IndexOf(Column)];
    }
    public IEnumerable<string> ReadSpanField()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Table.Table.SpanColumn != null, "Owning file must have a span column.");

      return FieldArray.Skip(Table.ColumnList.Count);
    }

    private string[] FieldArray;
  }

  public sealed class CsvWriteRow
  {
    internal CsvWriteRow(CsvWriteFile File, string[] FieldArray)
    {
      this.File = File;
      this.FieldArray = FieldArray;
    }

    internal CsvWriteFile File { get; private set; }
    internal string[] FieldArray { get; private set; }

    public void WriteField(CsvColumn Column, string Value)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(File.ColumnList.Contains(Column), "Column must belong to the owning file.");

      FieldArray[File.ColumnList.IndexOf(Column)] = Value;
    }
    public void Commit()
    {
      File.CommitRow(this);
    }
  }
}