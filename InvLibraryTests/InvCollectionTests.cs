﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InvCollection
{
  [TestClass]
  public class DistinctListTests
  {
    [TestMethod]
    public void ModifyWhileEnumerating()
    {
      var FrameworkArray = new string[] { "Hello", "world", "what", "a", "lovely", "day" };

      var DistinctList = new Inv.DistinctList<string>(FrameworkArray);

      var ListIndex = 0;
      foreach (var Item in DistinctList)
      {
        if (Item != FrameworkArray[ListIndex])
          Assert.Fail("DistinctList failed to enumerate each item.");

        DistinctList.Remove(Item);

        ListIndex++;
      }

      Assert.IsTrue(DistinctList.Count == 0, "DistinctList enumeration failed");
    }
  }

  [TestClass]
  public class WeakAugmentationTests
  {
    private static object TestAugmentation;

    private static Inv.WeakReferenceAugmentation<object, string> CreateAugmentation()
    {
      var Augmentation = Inv.WeakReferenceGovernor.NewAugmentation<object, string>();

      var ObjectArray = new object[] { new object(), new object(), new object() };

      TestAugmentation = new object();

      Augmentation.Add(ObjectArray[0], "callan");
      Augmentation.Add(ObjectArray[1], "hodgskin");
      Augmentation.Add(ObjectArray[2], "author");
      Augmentation.Add(TestAugmentation, "TestAugmentation");

      return Augmentation;
    }

    // TODO: this test case isn't viable because it has to shutdown the WeakReferenceGovernor to avoid 'thread was aborted exceptions'.
    //[TestMethod]
    public void BindToLifetime()
    {
      TestAugmentation = null;
      try
      {
        var Augmentation = CreateAugmentation();

        GC.Collect();
        Inv.WeakReferenceGovernor.Compact();

        if (Augmentation.Count != 1)
          Assert.Fail("Augmentation objects should not be alive.");

        if (!Augmentation.TryGetValue(TestAugmentation, out var TestValue) || TestValue != "TestAugmentation")
          Assert.Fail("Augmentation test object not found.");

        Inv.WeakReferenceGovernor.ShutdownCompact();
      }
      finally
      {
        TestAugmentation = null;
      }
    }
  }
}