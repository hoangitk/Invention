﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  public struct Point
  {
    public Point(int X, int Y)
      : this()
    {
      this.X = X;
      this.Y = Y;
    }

    public int X { get; private set; }
    public int Y { get; private set; }

    public static readonly Inv.Point Zero = new Point(0, 0);

    public static Inv.Point operator +(Inv.Point Left, Inv.Point Right)
    {
      return new Inv.Point(Left.X + Right.X, Left.Y + Right.Y);
    }
    public static Inv.Point operator -(Inv.Point Left, Inv.Point Right)
    {
      return new Inv.Point(Left.X - Right.X, Left.Y - Right.Y);
    }
    public static Inv.Point operator *(Inv.Point Left, int Right)
    {
      return new Inv.Point(Left.X * Right, Left.Y * Right);
    }
    public static Inv.Point operator /(Inv.Point Left, int Right)
    {
      return new Inv.Point(Left.X / Right, Left.Y / Right);
    }

    public bool EqualTo(Point Other)
    {
      return X == Other.X && Y == Other.Y;
    }

    public override string ToString()
    {
      return X + "," + Y;
    }
    public override bool Equals(object Other)
    {
      if (Other is Point)
        return EqualTo((Point)Other);

      return false;
    }
    public override int GetHashCode()
    {
      return X.GetHashCode() ^ Y.GetHashCode();
    }

    public static bool operator ==(Inv.Point Left, Inv.Point Right)
    {
      return Left.EqualTo(Right);
    }
    public static bool operator !=(Inv.Point Left, Inv.Point Right)
    {
      return !Left.EqualTo(Right);
    }
  }

  public struct Dimension
  {
    public Dimension(int Width, int Height)
      : this()
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Width >= 0, "Width must be zero or positive: {0}", Width);
        Inv.Assert.Check(Height >= 0, "Height must be zero or positive: {0}", Height);
      }

      this.Width = Width;
      this.Height = Height;
    }

    public int Width { get; private set; }
    public int Height { get; private set; }

    public bool EqualTo(Dimension Other)
    {
      return Width == Other.Width && Height == Other.Height;
    }

    public override string ToString()
    {
      return Width + "x" + Height;
    }
    public override bool Equals(object Other)
    {
      if (Other is Dimension)
        return EqualTo((Dimension)Other);

      return false;
    }
    public override int GetHashCode()
    {
      return Width.GetHashCode() ^ Height.GetHashCode();
    }
    public Inv.Point ToPoint()
    {
      return new Inv.Point(Width, Height);
    }


    public static readonly Inv.Dimension Zero = new Inv.Dimension(0, 0);
    public static bool operator ==(Inv.Dimension Left, Inv.Dimension Right)
    {
      return Left.EqualTo(Right);
    }
    public static bool operator !=(Inv.Dimension Left, Inv.Dimension Right)
    {
      return !Left.EqualTo(Right);
    }
    public static Inv.Dimension operator /(Inv.Dimension Left, int Value)
    {
      return new Inv.Dimension(Left.Width / Value, Left.Height / Value);
    }
    public static Inv.Dimension operator *(Inv.Dimension Left, int Value)
    {
      return new Inv.Dimension(Left.Width * Value, Left.Height * Value);
    }
  }

  public struct Rect
  {
    public Rect(Inv.Point Origin, Inv.Dimension Dimension)
      : this (Origin.X, Origin.Y, Dimension.Width, Dimension.Height)
    {
    }
    public Rect(int Left, int Top, int Width, int Height)
      : this()
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Width >= 0, "Width must not be zero or negative: {0}", Width);
        Inv.Assert.Check(Height >= 0, "Height must not be zero or negative: {0}", Height);
      }

      this.Left = Left;
      this.Top = Top;
      this.Right = Left + Width - 1; // denormalised for use by Android.
      this.Bottom = Top + Height - 1; // denormalised for use by Android.
      this.Width = Width;
      this.Height = Height;
    }

    public int Left { get; private set; }
    public int Top { get; private set; }
    public int Right { get; private set; }
    public int Bottom { get; private set; }
    public int Width { get; private set; }
    public int Height { get; private set; }

    public Inv.Point TopLeft()
    {
      return new Point(Left, Top);
    }
    public Inv.Point TopRight()
    {
      return new Point(Right, Top);
    }
    public Inv.Point BottomLeft()
    {
      return new Point(Left, Bottom);
    }
    public Inv.Point BottomRight()
    {
      return new Point(Right, Bottom);
    }
    public Inv.Point TopCenter()
    {
      return new Point(Left + (Width / 2), Top);
    }
    public Inv.Point BottomCenter()
    {
      return new Point(Left + (Width / 2), Bottom);
    }
    public Inv.Point Center()
    {
      return new Point(Left + (Width / 2), Top + (Height / 2));
    }
    public Inv.Point CenterRight()
    {
      return new Point(Right, Top + (Height / 2));
    }
    public Inv.Point CenterLeft()
    {
      return new Point(Left, Top + (Height / 2));
    }
    public Inv.Dimension Dimension()
    {
      return new Inv.Dimension(Width, Height);
    }
    public bool Contains(Inv.Point Point)
    {
      return Point.X >= Left && Point.X <= Right && Point.Y >= Top && Point.Y <= Bottom;
    }
    public Inv.Rect Offset(int X, int Y)
    {
      return new Inv.Rect(Left + X, Top + Y, Width, Height);
    }
    /// <summary>
    /// Return a new rect expanded around the center of this rect.
    /// This means the rect shifts up and to the left by the <paramref name="Size"/> and width and height increase by 2 x <paramref name="Size"/>.
    /// For example:
    /// <code>Rect(10, 10, 5, 5).Expand(5) -> Rect(5, 5, 15, 15)</code>
    /// </summary>
    /// <param name="Size"></param>
    /// <returns></returns>
    public Inv.Rect Expand(int Size)
    {
      var DoubleSize = Size * 2;

      return new Inv.Rect(Left - Size, Top - Size, Width + DoubleSize, Height + DoubleSize);
    }
    public Inv.Rect Reduce(int Size)
    {
      return new Inv.Rect(Left + Size, Top + Size, Width - (Size * 2), Height - (Size * 2));
    }

    public bool EqualTo(Rect Other)
    {
      return Left == Other.Left && Top == Other.Top && Width == Other.Width && Height == Other.Height;
    }

    public override string ToString()
    {
      return Left + "," + Right + ";" + Width + "x" + Height;
    }
    public override bool Equals(object Other)
    {
      if (Other is Rect)
        return EqualTo((Rect)Other);

      return false;
    }
    public override int GetHashCode()
    {
      return Left.GetHashCode() ^ Top.GetHashCode() ^ Width.GetHashCode() ^ Height.GetHashCode();
    }

    public static readonly Inv.Rect Empty = new Inv.Rect(0, 0, 0, 0);
    public static bool operator ==(Inv.Rect Left, Inv.Rect Right)
    {
      return Left.EqualTo(Right);
    }
    public static bool operator !=(Inv.Rect Left, Inv.Rect Right)
    {
      return !Left.EqualTo(Right);
    }
  }

  public struct Coordinate
  {
    public Coordinate(double Latitude, double Longitude, double Altitude)
    {
      this.Latitude = Latitude;
      this.Longitude = Longitude;
      this.Altitude = Altitude;
    }

    public double Latitude { get; private set; }
    public double Longitude { get; private set; }
    public double Altitude { get; private set; }

    /// <summary>
    /// Returns the latitude and longitude as a human readable string. eg. 37.3861° N, 122.0839° W
    /// </summary>
    public string ToPosition()
    {
      string Result;

      if (Latitude < 0)
        Result = -Latitude + "° S";
      else if (Latitude > 0)
        Result = Latitude + "° N";
      else
        Result = Latitude + "°";

      Result += ", ";

      if (Longitude < 0)
        Result += -Longitude + "° E";
      else if (Longitude > 0)
        Result += Longitude + "° W";
      else
        Result += Longitude + "°";

      return Result;
    }
  }
}
