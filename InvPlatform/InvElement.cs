﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// The abstract element is the base class of the layout elements.
  /// </summary>
  public abstract class Element
  {
    internal Element(Inv.Element Element)
      : this(Element.Change, Element.Window)
    {
    }
    internal Element(Inv.Window Window)
      : this(Window.Change, Window)
    {
    }
    internal Element(Inv.Surface Surface)
      : this(Surface.Change, Surface.Window)
    {
    }
    internal Element(Inv.Control Control)
      : this(Control.Change, Control.Window)
    {
    }
    internal Element(Action ChangeAction, Window Window)
    {
      this.ChangeAction = ChangeAction;
      this.Window = Window;
      //this.IsChanged = true; // NOTE: all platforms must observe the defaults of each element.
    }

    internal abstract ElementType ElementType { get; }
    internal Window Window { get; }
    internal bool IsChanged { get; private set; }

    internal void Change()
    {
      RequireThreadAffinity();

      if (!IsChanged)
        this.IsChanged = true;

      // TODO: this ensures the parent IsChanged is set (to workaround platform engine bugs where Element.Render() is not called).
      if (ChangeAction != null)
        ChangeAction();
    }
    internal bool Render()
    {
      RequireThreadAffinity();

      if (IsChanged)
      {
        this.IsChanged = false;
        return true;
      }
      else
      {
        return false;
      }
    }
    internal void RequireThreadAffinity()
    {
      Window.RequireThreadAffinity();
    }

    private readonly Action ChangeAction;
  }

  internal enum ElementType
  {
    Alignment,
    Background,
    Border,
    Collection,
    Corner,
    Elevation,
    Focus,
    Font,
    Justify,
    Margin,
    Padding,
    Opacity,
    Singleton,
    Size,
    TableRow,
    TableColumn,
    TableCell,
    Tooltip,
    Transform,
    Visibility
  }

  internal sealed class Singleton<T> : Element
  {
    internal Singleton(Inv.Control Control)
      : base(Control)
    {
    }

    public T Data
    {
      get
      {
        RequireThreadAffinity();

        return DataField;
      }
      set
      {
        RequireThreadAffinity();

        if (!object.Equals(DataField, value))
        {
          this.DataField = value;
          Change();
        }
      }
    }

    internal override ElementType ElementType => Inv.ElementType.Singleton;

    private T DataField;
  }

  internal sealed class Collection<T> : Element, IEnumerable<T>
  {
    internal Collection(Inv.Control Control)
      : base(Control)
    {
      this.List = new Inv.DistinctList<T>();
    }

    public T this[int Index]
    {
      get
      {
        RequireThreadAffinity();

        return List[Index];
      }
    }
    public int Count
    {
      get
      {
        RequireThreadAffinity();

        return List.Count;
      }
    }
    public void Clear()
    {
      RequireThreadAffinity();

      if (List.Count > 0)
      {
        List.Clear();
        Change();
      }
    }
    public void Add(T Item)
    {
      RequireThreadAffinity();

      List.Add(Item);
      Change();
    }
    public void Insert(int Index, T Item)
    {
      RequireThreadAffinity();

      List.Insert(Index, Item);
      Change();
    }
    public void Remove(T Item)
    {
      RequireThreadAffinity();

      if (List.Remove(Item))
        Change();
    }
    public void RemoveAt(int Index)
    {
      List.RemoveAt(Index);
    }
    public void RemoveWhere(Predicate<T> ItemPredicate)
    {
      RequireThreadAffinity();

      if (List.RemoveAll(ItemPredicate) > 0)
        Change();
    }
    public bool Contains(T Item)
    {
      RequireThreadAffinity();

      return List.Contains(Item);
    }
    public int IndexOf(T Item)
    {
      return List.IndexOf(Item);
    }

    internal override ElementType ElementType => Inv.ElementType.Collection;

    IEnumerator <T> IEnumerable<T>.GetEnumerator()
    {
      RequireThreadAffinity();

      return List.GetEnumerator();
    }
    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      RequireThreadAffinity();

      return List.GetEnumerator();
    }

    private readonly Inv.DistinctList<T> List;
  }

  /// <summary>
  /// The abstract Edge is the base class of Margin and Padding.
  /// </summary>
  public abstract class Edge : Element
  {
    internal Edge(Inv.Element Element)
      : base(Element)
    {
    }
    internal Edge(Inv.Control Control)
      : base(Control)
    {
    }

    /// <summary>
    /// The left indent in logical points.
    /// </summary>
    public int Left { get; private set; }
    /// <summary>
    /// The top indent in logical points.
    /// </summary>
    public int Top { get; private set; }
    /// <summary>
    /// The right indent in logical points.
    /// </summary>
    public int Right { get; private set; }
    /// <summary>
    /// The bottom indent in logical points.
    /// </summary>
    public int Bottom { get; private set; }
    /// <summary>
    /// Ask if any of the indents are set.
    /// </summary>
    public bool IsSet
    {
      get
      {
        RequireThreadAffinity();

        return Left != 0 || Top != 0 || Right != 0 || Bottom != 0;
      }
    }
    /// <summary>
    /// Ask if all the indents are a uniform value.
    /// </summary>
    public bool IsUniform
    {
      get
      {
        RequireThreadAffinity();

        return Left == Top && Top == Right && Right == Bottom;
      }
    }
    /// <summary>
    /// Ask if the indents are set to the same value on the left and right.
    /// </summary>
    public bool IsHorizontal
    {
      get
      {
        RequireThreadAffinity();

        return Left == Right;
      }
    }
    /// <summary>
    /// Ask if the indents are set to the same value on the top and bottom.
    /// </summary>
    public bool IsVertical
    {
      get
      {
        RequireThreadAffinity();

        return Top == Bottom;
      }
    }

    /// <summary>
    /// Clear the indent.
    /// </summary>
    public void Clear()
    {
      Set(0, 0, 0, 0);
    }
    /// <summary>
    /// Set a uniform indent.
    /// </summary>
    /// <param name="Value">The value used for the left, top, right and bottom indent</param>
    public void Set(int Value)
    {
      Set(Value, Value, Value, Value);
    }
    /// <summary>
    /// Set a uniform indent for the horizontal and vertical.
    /// </summary>
    /// <param name="Horizontal">The value used for the left and right indent</param>
    /// <param name="Vertical">The value used for the top and bottom indent</param>
    public void Set(int Horizontal, int Vertical)
    {
      Set(Horizontal, Vertical, Horizontal, Vertical);
    }
    /// <summary>
    /// Set a different indent for the left, top, right and bottom.
    /// </summary>
    /// <param name="Left"></param>
    /// <param name="Top"></param>
    /// <param name="Right"></param>
    /// <param name="Bottom"></param>
    public void Set(int Left, int Top, int Right, int Bottom)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Left >= 0 && Top >= 0 && Right >= 0 && Bottom >= 0, "Edges must be greater than or equal to zero.");

      if (this.Left != Left || this.Top != Top || this.Right != Right || this.Bottom != Bottom)
      {
        this.Left = Left;
        this.Top = Top;
        this.Right = Right;
        this.Bottom = Bottom;
        Change();
      }
    }
    /// <summary>
    /// Set indents to match another edge.
    /// </summary>
    /// <param name="Edge"></param>
    public void Set(Inv.Edge Edge)
    {
      Set(Edge.Left, Edge.Top, Edge.Right, Edge.Bottom);
    }
    /// <summary>
    /// Ask if there is a uniform indent used on the left, top, right and bottom.
    /// </summary>
    /// <param name="Size">The indent size for comparison</param>
    /// <returns></returns>
    public bool Is(int Size)
    {
      RequireThreadAffinity();

      return IsUniform && Size == Left;
    }
    /// <summary>
    /// Ask if there is a uniform indent for the horizontal and vertical.
    /// </summary>
    /// <param name="Horizontal"></param>
    /// <param name="Vertical"></param>
    /// <returns></returns>
    public bool Is(int Horizontal, int Vertical)
    {
      RequireThreadAffinity();

      return this.Left == Horizontal && this.Top == Vertical && this.Right == Horizontal && this.Bottom == Vertical;
    }
    /// <summary>
    /// Ask if there is a specific indent used.
    /// </summary>
    /// <param name="Left"></param>
    /// <param name="Top"></param>
    /// <param name="Right"></param>
    /// <param name="Bottom"></param>
    /// <returns></returns>
    public bool Is(int Left, int Top, int Right, int Bottom)
    {
      RequireThreadAffinity();

      return this.Left == Left && this.Top == Top && this.Right == Right && this.Bottom == Bottom;
    }
  }

  /// <summary>
  /// The outer edge of the panel can have a solid and coloured line.
  /// </summary>
  public sealed class Border : Edge
  {
    internal Border(Inv.Control Control)
      : base(Control)
    {
    }

    /// <summary>
    /// Colour of the border.
    /// </summary>
    public Inv.Colour Colour
    {
      get
      {
        RequireThreadAffinity();

        return ColourField;
      }
      set
      {
        RequireThreadAffinity();

        if (ColourField != value)
        {
          this.ColourField = value;
          Change();
        }
      }
    }

    /// <summary>
    /// Set a uniform border.
    /// </summary>
    /// <param name="Value">The value used for the left, top, right and bottom border</param>
    /// <returns>Returns this border so you can chain calls in a fluent style.</returns>
    public new Inv.Border Set(int Value)
    {
      base.Set(Value);
      return this;
    }
    /// <summary>
    /// Set a uniform border for the horizontal and vertical.
    /// </summary>
    /// <param name="Horizontal">The value used for the left and right indent</param>
    /// <param name="Vertical">The value used for the top and bottom indent</param>
    /// <returns>Returns this border so you can chain calls in a fluent style.</returns>
    public new Inv.Border Set(int Horizontal, int Vertical)
    {
      base.Set(Horizontal, Vertical);
      return this;
    }
    /// <summary>
    /// Set a different border for the left, top, right and bottom.
    /// </summary>
    /// <param name="Left"></param>
    /// <param name="Top"></param>
    /// <param name="Right"></param>
    /// <param name="Bottom"></param>
    /// <returns>Returns this border so you can chain calls in a fluent style.</returns>
    public new Inv.Border Set(int Left, int Top, int Right, int Bottom)
    {
      base.Set(Left, Top, Right, Bottom);

      return this;
    }
    internal override ElementType ElementType => Inv.ElementType.Border;

    private Inv.Colour ColourField;
  }

  /// <summary>
  /// The dimensions of a panel including minimum and maximum constraints.
  /// </summary>
  public sealed class Size : Element
  {
    internal Size(Inv.Control Control)
      : base(Control)
    {
    }

    /// <summary>
    /// The fixed width of the panel in logical points. Null specifies auto width.
    /// </summary>
    public int? Width { get; private set; }
    /// <summary>
    /// The fixed height of the panel in logical points. Null specifies auto height.
    /// </summary>
    public int? Height { get; private set; }
    /// <summary>
    /// The minimum width constraint of the panel in logical points. Null specifies no constraint.
    /// </summary>
    public int? MinimumWidth { get; private set; }
    /// <summary>
    /// The minimum height constraint of the panel in logical points. Null specifies no constraint.
    /// </summary>
    public int? MinimumHeight { get; private set; }
    /// <summary>
    /// The maximum width constraint of the panel in logical points. Null specifies no constraint.
    /// </summary>
    public int? MaximumWidth { get; private set; }
    /// <summary>
    /// The maximum height constraint of the panel in logical points. Null specifies no constraint.
    /// </summary>
    public int? MaximumHeight { get; private set; }

    /// <summary>
    /// Set the fixed size of the panel.
    /// Use null to specify auto size.
    /// </summary>
    /// <param name="Size"></param>
    public void Set(int? Size)
    {
      Set(Size, Size);
    }
    /// <summary>
    /// Set the fixed width and height of the panel.
    /// Use null to specify auto width or height.
    /// </summary>
    /// <param name="Width"></param>
    /// <param name="Height"></param>
    public void Set(int? Width, int? Height)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Width == null || Width >= 0, "Width must be null, zero or a positive integer.");
        Inv.Assert.Check(Height == null || Height >= 0, "Height must be null, zero or a positive integer.");
      }

      if (this.Width != Width || this.Height != Height)
      {
        this.Width = Width;
        this.Height = Height;
        Change();
      }
    }
    /// <summary>
    /// Set the fixed width of the panel.
    /// </summary>
    /// <param name="Width"></param>
    public void SetWidth(int? Width)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Width == null || Width >= 0, "Width must be null, zero or a positive integer.");

      if (this.Width != Width)
      {
        this.Width = Width;
        Change();
      }
    }
    /// <summary>
    /// Set the fixed height of the panel.
    /// </summary>
    /// <param name="Height"></param>
    public void SetHeight(int? Height)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Height == null || Height >= 0, "Height must be null, zero or a positive integer.");

      if (this.Height != Height)
      {
        this.Height = Height;
        Change();
      }
    }
    /// <summary>
    /// Ask if the panel has a fixed width and height.
    /// </summary>
    /// <param name="Width"></param>
    /// <param name="Height"></param>
    public bool Is(int? Width, int? Height)
    {
      return this.Width == Width && this.Height == Height;
    }
    /// <summary>
    /// Set the minimum width and height constraint of the panel.
    /// </summary>
    /// <param name="MinimumSize"></param>
    public void SetMinimum(int? MinimumSize)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(MinimumSize == null || MinimumSize >= 0, "MinimumSize must be null, zero or a positive integer.");
      }

      if (this.MinimumWidth != MinimumSize || this.MinimumHeight != MinimumSize)
      {
        this.MinimumWidth = MinimumSize;
        this.MinimumHeight = MinimumSize;
        Change();
      }
    }
    /// <summary>
    /// Set the minimum width and height of the panel.
    /// </summary>
    /// <param name="MinimumWidth"></param>
    /// /// <param name="MinimumHeight"></param>
    public void SetMinimum(int? MinimumWidth, int? MinimumHeight)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(MinimumWidth == null || MinimumWidth >= 0, "MinimumWidth must be null, zero or a positive integer.");
        Inv.Assert.Check(MinimumHeight == null || MinimumHeight >= 0, "MinimumHeight must be null, zero or a positive integer.");
      }

      if (this.MinimumWidth != MinimumWidth || this.MinimumHeight != MinimumHeight)
      {
        this.MinimumWidth = MinimumWidth;
        this.MinimumHeight = MinimumHeight;
        Change();
      }
    }
    /// <summary>
    /// Set the minimum width constraint of the panel.
    /// </summary>
    /// <param name="MinimumWidth"></param>
    public void SetMinimumWidth(int? MinimumWidth)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(MinimumWidth == null || MinimumWidth >= 0, "MinimumWidth must be null, zero or a positive integer.");

      if (this.MinimumWidth != MinimumWidth)
      {
        this.MinimumWidth = MinimumWidth;
        Change();
      }
    }
    /// <summary>
    /// Set the minimum height constraint of the panel.
    /// </summary>
    /// <param name="MinimumHeight"></param>
    public void SetMinimumHeight(int? MinimumHeight)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(MinimumHeight == null || MinimumHeight >= 0, "MinimumHeight must be null, zero or a positive integer.");

      if (this.MinimumHeight != MinimumHeight)
      {
        this.MinimumHeight = MinimumHeight;
        Change();
      }
    }
    /// <summary>
    /// Set the maximum width and height constraint of the panel.
    /// </summary>
    /// <param name="MaximumSize"></param>
    public void SetMaximum(int? MaximumSize)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(MaximumSize == null || MaximumSize >= 0, "MaximumSize must be null, zero or a positive integer.");
      }

      if (this.MaximumWidth != MaximumSize || this.MaximumHeight != MaximumSize)
      {
        this.MaximumWidth = MaximumSize;
        this.MaximumHeight = MaximumSize;
        Change();
      }
    }
    /// <summary>
    /// Set the maximum width and height constraint of the panel.
    /// </summary>
    /// <param name="MaximumWidth"></param>
    /// /// <param name="MaximumHeight"></param>
    public void SetMaximum(int? MaximumWidth, int? MaximumHeight)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(MaximumWidth == null || MaximumWidth >= 0, "MaximumWidth must be null, zero or a positive integer.");
        Inv.Assert.Check(MaximumHeight == null || MaximumHeight >= 0, "MaximumHeight must be null, zero or a positive integer.");
      }

      if (this.MaximumWidth != MaximumWidth || this.MaximumHeight != MaximumHeight)
      {
        this.MaximumWidth = MaximumWidth;
        this.MaximumHeight = MaximumHeight;
        Change();
      }
    }
    /// <summary>
    /// Set the maximum width constraint of the panel.
    /// </summary>
    /// <param name="MaximumWidth"></param>
    public void SetMaximumWidth(int? MaximumWidth)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(MaximumWidth == null || MaximumWidth >= 0, "MaximumWidth must be null, zero or a positive integer.");

      if (this.MaximumWidth != MaximumWidth)
      {
        this.MaximumWidth = MaximumWidth;
        Change();
      }
    }
    /// <summary>
    /// Set the maximum height constraint of the panel.
    /// </summary>
    /// <param name="MaximumHeight"></param>
    public void SetMaximumHeight(int? MaximumHeight)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(MaximumHeight == null || MaximumHeight >= 0, "MaximumHeight must be null, zero or a positive integer.");

      if (this.MaximumHeight != MaximumHeight)
      {
        this.MaximumHeight = MaximumHeight;
        Change();
      }
    }

    /// <summary>
    /// Reset to automatic width and height.
    /// </summary>
    public void Auto()
    {
      RequireThreadAffinity();

      if (this.Width != null || this.Height != null)
      {
        this.Width = null;
        this.Height = null;
        Change();
      }
    }
    /// <summary>
    /// Reset to automatic width.
    /// </summary>
    public void AutoWidth()
    {
      RequireThreadAffinity();

      if (Width != null)
      {
        this.Width = null;
        Change();
      }
    }
    /// <summary>
    /// Reset to automatic height.
    /// </summary>
    public void AutoHeight()
    {
      RequireThreadAffinity();

      if (Height != null)
      {
        this.Height = null;
        Change();
      }
    }
    /// <summary>
    /// Reset to clear the minimum constraint.
    /// </summary>
    public void AutoMinimum()
    {
      RequireThreadAffinity();

      if (MinimumWidth != null || MinimumHeight != null)
      {
        this.MinimumWidth = null;
        this.MinimumHeight = null;
        Change();
      }
    }
    /// <summary>
    /// Reset to clear the minimum width constraint.
    /// </summary>
    public void AutoMinimumWidth()
    {
      RequireThreadAffinity();

      if (MinimumWidth != null)
      {
        this.MinimumWidth = null;
        Change();
      }
    }
    /// <summary>
    /// Reset to clear the minimum height constraint.
    /// </summary>
    public void AutoMinimumHeight()
    {
      RequireThreadAffinity();

      if (MinimumHeight != null)
      {
        this.MinimumHeight = null;
        Change();
      }
    }
    /// <summary>
    /// Reset to clear the maximum constraint.
    /// </summary>
    public void AutoMaximum()
    {
      RequireThreadAffinity();

      if (MaximumWidth != null || MaximumHeight != null)
      {
        this.MaximumWidth = null;
        this.MaximumHeight = null;
        Change();
      }
    }
    /// <summary>
    /// Reset to clear the maximum width constraint.
    /// </summary>
    public void AutoMaximumWidth()
    {
      RequireThreadAffinity();

      if (MaximumWidth != null)
      {
        this.MaximumWidth = null;
        Change();
      }
    }
    /// <summary>
    /// Reset to clear the maximum height constraint.
    /// </summary>
    public void AutoMaximumHeight()
    {
      RequireThreadAffinity();

      if (MaximumHeight != null)
      {
        this.MaximumHeight = null;
        Change();
      }
    }

    internal override ElementType ElementType => Inv.ElementType.Size;
    internal void Set(int? Width, int? Height, int? MinimumWidth, int? MinimumHeight, int? MaximumWidth, int? MaximumHeight)
    {
      RequireThreadAffinity();

      var Changed = false;

      if (Width != this.Width)
      {
        this.Width = Width;
        Changed = true;
      }

      if (Height != this.Height)
      {
        this.Height = Height;
        Changed = true;
      }

      if (MinimumWidth != this.MinimumWidth)
      {
        this.MinimumWidth = MinimumWidth;
        Changed = true;
      }

      if (MinimumHeight != this.MinimumHeight)
      {
        this.MinimumHeight = MinimumHeight;
        Changed = true;
      }

      if (MaximumWidth != this.MaximumWidth)
      {
        this.MaximumWidth = MaximumWidth;
        Changed = true;
      }

      if (MaximumHeight != this.MaximumHeight)
      {
        this.MaximumHeight = MaximumHeight;
        Changed = true;
      }

      if (Changed)
        Change();
    }
  }

  /// <summary>
  /// A panel can be collapsed which removes them from the layout.
  /// </summary>
  public sealed class Visibility : Element
  {
    internal Visibility(Inv.Control Control)
      : base(Control)
    {
      this.IsVisible = true;
    }

    /// <summary>
    /// Ask if the panel is visible.
    /// </summary>
    /// <returns></returns>
    public bool Get()
    {
      RequireThreadAffinity();

      return IsVisible;
    }
    /// <summary>
    /// Set the visibility of the panel.
    /// </summary>
    /// <param name="IsVisible"></param>
    public void Set(bool IsVisible)
    {
      RequireThreadAffinity();

      if (this.IsVisible != IsVisible)
      {
        this.IsVisible = IsVisible;

        Change();
      }
    }
    /// <summary>
    /// Show the panel.
    /// </summary>
    public void Show()
    {
      Set(true);
    }
    /// <summary>
    /// Collapse the visibility of the panel.
    /// </summary>
    public void Collapse()
    {
      Set(false);
    }
    /// <summary>
    /// Toggle the visibility of the panel.
    /// </summary>
    public void Toggle()
    {
      Set(!Get());
    }

    internal override ElementType ElementType => Inv.ElementType.Visibility;

    private bool IsVisible;
  }

  /// <summary>
  /// The solid background colour of a panel.
  /// </summary>
  public sealed class Background : Inv.Element
  {
    internal Background(Inv.Window Window)
      : base(Window)
    {
    }
    internal Background(Inv.Surface Surface)
      : base(Surface)
    {
    }
    internal Background(Inv.Control Control)
      : base(Control)
    {
    }
    internal Background(Action ChangeAction, Window Window)
      : base(ChangeAction, Window)
    {
    }

    /// <summary>
    /// Get and set the solid colour background.
    /// </summary>
    public Inv.Colour Colour
    {
      get
      {
        RequireThreadAffinity();

        return ColourField;
      }
      set
      {
        RequireThreadAffinity();

        if (ColourField != value)
        {
          this.ColourField = value;
          Change();
        }
      }
    }

    internal override ElementType ElementType => Inv.ElementType.Background;

    private Inv.Colour ColourField;
  }

  /// <summary>
  /// The percentage transparency of the panel and its children between 0 and 1.
  /// </summary>
  public sealed class Opacity : Element
  {
    internal Opacity(Inv.Control Control)
      : base(Control)
    {
      this.Percent = 1.00F;
    }

    /// <summary>
    /// Get the opacity of the panel between 0.0F and 1.0F.
    /// </summary>
    /// <returns></returns>
    public float Get()
    {
      RequireThreadAffinity();

      return this.Percent;
    }
    /// <summary>
    /// Set the opacity of the panel between 0.0F and 1.0F.
    /// </summary>
    /// <param name="Percent"></param>
    public void Set(float Percent)
    {
      RequireThreadAffinity();

      if (this.Percent != Percent)
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(Percent >= 0.0F && Percent <= 1.0F, "Opacity percent must be between 0.0F and 1.0F.");

        this.Percent = Percent;
        Change();
      }
    }
    /// <summary>
    /// Make the panel completely transparent (0.0F).
    /// </summary>
    public void Transparent()
    {
      Set(0.0F);
    }
    /// <summary>
    /// Make the panel completely opaque (1.0F).
    /// </summary>
    public void Opaque()
    {
      Set(1.0F);
    }
    /// <summary>
    /// Panel is 100% opaque (completely visible, no transparency).
    /// </summary>
    /// <returns></returns>
    public bool IsOpaque() => Get() == 1.00F;
    /// <summary>
    /// Panel is 100% transparent (completely invisible).
    /// </summary>
    /// <returns></returns>
    public bool IsTransparent() => Get() == 0.00F;

    internal override ElementType ElementType => Inv.ElementType.Opacity;

    internal void BypassSet(float Percent)
    {
      // set the opacity without notifying the change, useful for animations.
      this.Percent = Percent;
    }

    private float Percent;
  }

  // TODO: not implemented.
  /// <summary>
  /// Transformed panels.
  /// </summary>
  internal sealed class Transform : Element
  {
    internal Transform(Inv.Control Control)
      : base(Control)
    {
      this.ScaleWidthPercent = 1.00F;
      this.ScaleHeightPercent = 1.00F;
    }

    /// <summary>
    /// Get the scale width of the panel.
    /// </summary>
    /// <returns></returns>
    public float GetScaleWidth()
    {
      RequireThreadAffinity();

      return this.ScaleWidthPercent;
    }
    /// <summary>
    /// Get the scale height of the panel.
    /// </summary>
    /// <returns></returns>
    public float GetScaleHeight()
    {
      RequireThreadAffinity();

      return this.ScaleHeightPercent;
    }
    /// <summary>
    /// Set the scale of the panel.
    /// </summary>
    /// <param name="Percent"></param>
    public void Scale(float Percent)
    {
      RequireThreadAffinity();

      if (this.ScaleWidthPercent != Percent || this.ScaleHeightPercent != Percent)
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(Percent >= 0.0F, "Scale percent must not be negative.");

        this.ScaleWidthPercent = Percent;
        this.ScaleHeightPercent = Percent;
        Change();
      }
    }
    /// <summary>
    /// Set the scale width of the panel.
    /// </summary>
    /// <param name="Percent"></param>
    public void ScaleWidth(float Percent)
    {
      RequireThreadAffinity();

      if (this.ScaleWidthPercent != Percent)
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(Percent >= 0.0F, "Scale percent must not be negative.");

        this.ScaleWidthPercent = Percent;
        Change();
      }
    }
    /// <summary>
    /// Set the scale height of the panel.
    /// </summary>
    /// <param name="Percent"></param>
    public void ScaleHeight(float Percent)
    {
      RequireThreadAffinity();

      if (this.ScaleHeightPercent != Percent)
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(Percent >= 0.0F, "Scale percent must not be negative.");

        this.ScaleHeightPercent = Percent;
        Change();
      }
    }

    // TODO: Rotation.

    internal override ElementType ElementType => Inv.ElementType.Transform;

    internal void BypassScaleSet(float WidthPercent, float HeightPercent)
    {
      // set the Scale without notifying the change, useful for animations.
      this.ScaleWidthPercent = WidthPercent;
      this.ScaleHeightPercent = HeightPercent;
    }

    private float ScaleWidthPercent;
    private float ScaleHeightPercent;
  }

  /// <summary>
  /// Text justification (left, center, right).
  /// </summary>
  public sealed class Justify : Element
  {
    internal Justify(Inv.Control Control)
      : base(Control)
    {
      this.Justification = Justification.Left;
    }

    /// <summary>
    /// Get the justification of the text element.
    /// </summary>
    /// <returns></returns>
    public Justification Get()
    {
      RequireThreadAffinity();

      return Justification;
    }
    /// <summary>
    /// Set the justification of the text element.
    /// </summary>
    /// <param name="Justification"></param>
    public void Set(Justification Justification)
    {
      RequireThreadAffinity();

      if (this.Justification != Justification)
      {
        this.Justification = Justification;
        Change();
      }
    }
    /// <summary>
    /// Text is left aligned.
    /// </summary>
    public void Left()
    {
      Set(Justification.Left);
    }
    /// <summary>
    /// Text is centered.
    /// </summary>
    public void Center()
    {
      Set(Justification.Center);
    }
    /// <summary>
    /// Text is right aligned.
    /// </summary>
    public void Right()
    {
      Set(Justification.Right);
    }

    internal override ElementType ElementType => Inv.ElementType.Justify;

    private Justification Justification;
  }

  /// <summary>
  /// The enum used for text justification.
  /// </summary>
  public enum Justification
  {
    /// <summary>
    /// Text is left aligned.
    /// </summary>
    Left,
    /// <summary>
    /// Text is centered.
    /// </summary>
    Center,
    /// <summary>
    /// Text is right aligned.
    /// </summary>
    Right
  }

  /// <summary>
  /// The relative placement of a panel within its parent container.
  /// </summary>
  public sealed class Alignment : Element
  {
    internal Alignment(Inv.Control Control)
      : base(Control)
    {
      this.Placement = Placement.Stretch;
    }

    /// <summary>
    /// Get the placement of the panel.
    /// </summary>
    /// <returns></returns>
    public Placement Get()
    {
      RequireThreadAffinity();

      return Placement;
    }
    /// <summary>
    /// Set the placement of the panel.
    /// </summary>
    /// <param name="Placement"></param>
    public void Set(Placement Placement)
    {
      RequireThreadAffinity();

      if (this.Placement != Placement)
      {
        this.Placement = Placement;
        Change();
      }
    }
    /// <summary>
    /// Align the panel to the top-left corner.
    /// </summary>
    public void TopLeft()
    {
      Set(Placement.TopLeft);
    }
    /// <summary>
    /// Align the panel to the top and center.
    /// </summary>
    public void TopCenter()
    {
      Set(Placement.TopCenter);
    }
    /// <summary>
    /// Align the panel to the top-right corner.
    /// </summary>
    public void TopRight()
    {
      Set(Placement.TopRight);
    }
    /// <summary>
    /// Align the panel to the top and stretch to the full width.
    /// </summary>
    public void TopStretch()
    {
      Set(Inv.Placement.TopStretch);
    }
    /// <summary>
    /// Align the panel to the vertical center and to the left edge.
    /// </summary>
    public void CenterLeft()
    {
      Set(Placement.CenterLeft);
    }
    /// <summary>
    /// Align the panel to the horizontal and vertical center.
    /// </summary>
    public void Center()
    {
      Set(Placement.Center);
    }
    /// <summary>
    /// Align the panel to the vertical center and to the right edge.
    /// </summary>
    public void CenterRight()
    {
      Set(Placement.CenterRight);
    }
    /// <summary>
    /// Align the panel to the vertical center and stretch to the full width.
    /// </summary>
    public void CenterStretch()
    {
      Set(Placement.CenterStretch);
    }
    /// <summary>
    /// Align the panel to the bottom-left corner.
    /// </summary>
    public void BottomLeft()
    {
      Set(Placement.BottomLeft);
    }
    /// <summary>
    /// Align the panel to the bottom and horizontally center.
    /// </summary>
    public void BottomCenter()
    {
      Set(Placement.BottomCenter);
    }
    /// <summary>
    /// Align the panel to the bottom-right corner.
    /// </summary>
    public void BottomRight()
    {
      Set(Placement.BottomRight);
    }
    /// <summary>
    /// Align the panel to the bottom and stretch to the full width.
    /// </summary>
    public void BottomStretch()
    {
      Set(Placement.BottomStretch);
    }
    /// <summary>
    /// Align the panel to stretch to the full width and height.
    /// </summary>
    public void Stretch()
    {
      Set(Placement.Stretch);
    }
    /// <summary>
    /// Align the panel to stretch to the full height and snap to the left edge.
    /// </summary>
    public void StretchLeft()
    {
      Set(Placement.StretchLeft);
    }
    /// <summary>
    /// Align the panel to stretch to the full height and snap to the right edge.
    /// </summary>
    public void StretchRight()
    {
      Set(Placement.StretchRight);
    }
    /// <summary>
    /// Align the panel to stretch to the full height and horizontally center.
    /// </summary>
    public void StretchCenter()
    {
      Set(Placement.StretchCenter);
    }

    internal override ElementType ElementType => Inv.ElementType.Alignment;

    private Placement Placement;
  }

  /// <summary>
  /// The enum used in panel alignment.
  /// </summary>
  public enum Placement
  {
    /// <summary>
    /// vertical stretch and horizontal stretch
    /// </summary>
    Stretch,
    /// <summary>
    /// vertical stretch and left
    /// </summary>
    StretchLeft,
    /// <summary>
    /// vertical stretch and horizontal center
    /// </summary>
    StretchCenter,
    /// <summary>
    /// vertical stretch and right
    /// </summary>
    StretchRight,
    /// <summary>
    /// top and horizontal stretch
    /// </summary>
    TopStretch,
    /// <summary>
    /// top and left
    /// </summary>
    TopLeft,
    /// <summary>
    /// top and horizontal center
    /// </summary>
    TopCenter,
    /// <summary>
    /// top and right
    /// </summary>
    TopRight,
    /// <summary>
    /// vertical center and horizontal stretch
    /// </summary>
    CenterStretch,
    /// <summary>
    /// vertical center and left
    /// </summary>
    CenterLeft,
    /// <summary>
    /// vertical center and horizontal center
    /// </summary>
    Center,
    /// <summary>
    /// vertical center and right
    /// </summary>
    CenterRight,
    /// <summary>
    /// bottom and horizontal stretch
    /// </summary>
    BottomStretch,
    /// <summary>
    /// bottom and left
    /// </summary>
    BottomLeft,
    /// <summary>
    /// bottom and horizontal center
    /// </summary>
    BottomCenter,
    /// <summary>
    /// bottom and right
    /// </summary>
    BottomRight
  }

  /// <summary>
  /// Common font sizes.
  /// </summary>
  public static class FontSize
  {
    /// <summary>
    /// 10pt.
    /// </summary>
    public const int ExtraSmall = 10;
    /// <summary>
    /// 11pt.
    /// </summary>
    public const int Small = 11;
    /// <summary>
    /// 12pt.
    /// </summary>
    public const int Normal = 12;
    /// <summary>
    /// 15pt.
    /// </summary>
    public const int Large = 15;
    /// <summary>
    /// 18pt.
    /// </summary>
    public const int ExtraLarge = 18;
    /// <summary>
    /// 24pt.
    /// </summary>
    public const int Massive = 24;
    /// <summary>
    /// 36pt.
    /// </summary>
    public const int ExtraMassive = 36;

    public static string CommonName(int FontSize)
    {
      switch (FontSize)
      {
        case ExtraSmall: return "ExtraSmall";
        case Small: return "Small";
        case Normal: return "Normal";
        case Large: return "Large";
        case ExtraLarge: return "ExtraLarge";
        case Massive: return "Massive";
        case ExtraMassive: return "ExtraMassive";
        default: return null;
      }
    }
  }

  /// <summary>
  /// Font is used to render text in panels such as <see cref="Label" />, <see cref="Edit" /> and <see cref="Memo" />.
  /// </summary>
  public sealed class Font : Element
  {
    internal Font(Inv.Control Control)
      : base(Control)
    {
    }
    internal Font(Action ChangeAction, Window Window)
      : base(ChangeAction, Window)
    {
    }

    /// <summary>
    /// Family name of the font.
    /// Use with caution as not many fonts are available on all platforms.
    /// You can use <see cref="Device.Target"/> to select a different font per platform.
    /// </summary>
    public string Name
    {
      get
      {
        RequireThreadAffinity();

        return NameField;
      }
      set
      {
        RequireThreadAffinity();

        if (NameField != value)
        {
          this.NameField = value;
          Change();
        }
      }
    }
    /// <summary>
    /// The size of the font in logical points.
    /// </summary>
    public int? Size
    {
      get
      {
        RequireThreadAffinity();

        return SizeField;
      }
      set
      {
        RequireThreadAffinity();

        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(value != 0, "Font size of zero is not supported.");

        if (SizeField != value)
        {
          this.SizeField = value;
          Change();
        }
      }
    }
    /// <summary>
    /// The colour of the font.
    /// </summary>
    public Inv.Colour Colour
    {
      get
      {
        RequireThreadAffinity();

        return ColourField;
      }
      set
      {
        RequireThreadAffinity();

        if (ColourField != value)
        {
          this.ColourField = value;
          Change();
        }
      }
    }
    /// <summary>
    /// The <see cref="Inv.FontWeight"/> of the font.
    /// </summary>
    public Inv.FontWeight? Weight
    {
      get
      {
        RequireThreadAffinity();

        return WeightField;
      }
      set
      {
        RequireThreadAffinity();

        if (WeightField != value)
        {
          this.WeightField = value;
          Change();
        }
      }
    }
    /// <summary>
    /// Render as italics.
    /// </summary>
    public bool? IsItalics
    {
      get
      {
        RequireThreadAffinity();

        return ItalicsField;
      }
      set
      {
        RequireThreadAffinity();

        if (ItalicsField != value)
        {
          this.ItalicsField = value;
          Change();
        }
      }
    }
    /// <summary>
    /// Render with an underline.
    /// </summary>
    public bool? IsUnderlined
    {
      get
      {
        RequireThreadAffinity();

        return UnderlinedField;
      }
      set
      {
        RequireThreadAffinity();

        if (UnderlinedField != value)
        {
          this.UnderlinedField = value;
          Change();
        }
      }
    }
    /// <summary>
    /// Render with a strikethrough.
    /// </summary>
    public bool? IsStrikethrough
    {
      get
      {
        RequireThreadAffinity();

        return StrikethroughField;
      }
      set
      {
        RequireThreadAffinity();

        if (StrikethroughField != value)
        {
          this.StrikethroughField = value;
          Change();
        }
      }
    }
    /// <summary>
    /// Render as small caps.
    /// </summary>
    public bool? IsSmallCaps
    {
      get
      {
        RequireThreadAffinity();

        return SmallCapsField;
      }
      set
      {
        RequireThreadAffinity();

        if (SmallCapsField != value)
        {
          this.SmallCapsField = value;
          Change();
        }
      }
    }

    /// <summary>
    /// Returns true when at least one of the font fields has been specified.
    /// </summary>
    /// <returns></returns>
    public bool IsSpecified()
    {
      return
        Name != null ||
        Size != null ||
        Colour != null ||
        Weight != null ||
        IsSmallCaps != null ||
        IsUnderlined != null ||
        IsStrikethrough != null ||
        IsItalics != null;
    }

    /// <summary>
    /// Set the font name to the default monospaced font for the device.
    /// </summary>
    /// <returns>Returns this font so you can chain calls in a fluent style.</returns>
    public Inv.Font Monospaced()
    {
      this.Name = Window.Application.Device.MonospacedFontName;
      return this;
    }
    /// <summary>
    /// Set the font name to the default proportional font for the device.
    /// </summary>
    /// <returns>Returns this font so you can chain calls in a fluent style.</returns>
    public Inv.Font Proportional()
    {
      this.Name = Window.Application.Device.ProportionalFontName;
      return this;
    }

    /// <summary>
    /// Set font size to 10pt.
    /// </summary>
    /// <returns>Returns this font so you can chain calls in a fluent style.</returns>
    public Inv.Font ExtraSmall()
    {
      this.Size = FontSize.ExtraSmall;
      return this;
    }
    /// <summary>
    /// Set font size to 11pt.
    /// </summary>
    /// <returns>Returns this font so you can chain calls in a fluent style.</returns>
    public Inv.Font Small()
    {
      this.Size = FontSize.Small;
      return this;
    }
    /// <summary>
    /// Set font size to 12pt.
    /// </summary>
    /// <returns>Returns this font so you can chain calls in a fluent style.</returns>
    public Inv.Font Normal()
    {
      this.Size = FontSize.Normal;
      return this;
    }
    /// <summary>
    /// Set font size to 15pt.
    /// </summary>
    /// <returns>Returns this font so you can chain calls in a fluent style.</returns>
    public Inv.Font Large()
    {
      this.Size = FontSize.Large;
      return this;
    }
    /// <summary>
    /// Set font size to 18pt.
    /// </summary>
    /// <returns>Returns this font so you can chain calls in a fluent style.</returns>
    public Inv.Font ExtraLarge()
    {
      this.Size = FontSize.ExtraLarge;
      return this;
    }
    /// <summary>
    /// Set font size to 24pt.
    /// </summary>
    /// <returns>Returns this font so you can chain calls in a fluent style.</returns>
    public Inv.Font Massive()
    {
      this.Size = FontSize.Massive;
      return this;
    }
    /// <summary>
    /// Set font size to 36pt.
    /// </summary>
    /// <returns>Returns this font so you can chain calls in a fluent style.</returns>
    public Inv.Font ExtraMassive()
    {
      this.Size = FontSize.ExtraMassive;
      return this;
    }

    /// <summary>
    /// Set the font weight to thin.
    /// </summary>
    /// <returns>Returns this font so you can chain calls in a fluent style.</returns>
    public Inv.Font Thin()
    {
      this.Weight = FontWeight.Thin;
      return this;
    }
    /// <summary>
    /// Set the font weight to light.
    /// </summary>
    /// <returns>Returns this font so you can chain calls in a fluent style.</returns>
    public Inv.Font Light()
    {
      this.Weight = FontWeight.Light;
      return this;
    }
    /// <summary>
    /// Set the font weight to regular.
    /// </summary>
    /// <returns>Returns this font so you can chain calls in a fluent style.</returns>
    public Inv.Font Regular()
    {
      this.Weight = FontWeight.Regular;
      return this;
    }
    /// <summary>
    /// Set the font weight to medium.
    /// </summary>
    /// <returns>Returns this font so you can chain calls in a fluent style.</returns>
    public Inv.Font Medium()
    {
      this.Weight = FontWeight.Medium;
      return this;
    }
    /// <summary>
    /// Set the font weight to bold.
    /// </summary>
    /// <returns>Returns this font so you can chain calls in a fluent style.</returns>
    public Inv.Font Bold()
    {
      this.Weight = FontWeight.Bold;
      return this;
    }
    /// <summary>
    /// Set the font weight to heavy.
    /// </summary>
    /// <returns>Returns this font so you can chain calls in a fluent style.</returns>
    public Inv.Font Heavy()
    {
      this.Weight = FontWeight.Heavy;
      return this;
    }

    /// <summary>
    /// Set Italics on (or off).
    /// </summary>
    /// <param name="On"></param>
    /// <returns>Returns this font so you can chain calls in a fluent style.</returns>
    public Inv.Font Italics(bool On = true)
    {
      this.IsItalics = On;
      return this;
    }
    /// <summary>
    /// Set Underlined on (or off).
    /// </summary>
    /// <param name="On"></param>
    /// <returns>Returns this font so you can chain calls in a fluent style.</returns>
    public Inv.Font Underlined(bool On = true)
    {
      this.IsUnderlined = On;
      return this;
    }
    /// <summary>
    /// Set Strikethrough on (or off).
    /// </summary>
    /// <param name="On"></param>
    /// <returns>Returns this font so you can chain calls in a fluent style.</returns>
    public Inv.Font Strikethrough(bool On = true)
    {
      this.IsStrikethrough = On;
      return this;
    }
    /// <summary>
    /// Set SmallCaps on (or off).
    /// </summary>
    /// <param name="On"></param>
    /// <returns>Returns this font so you can chain calls in a fluent style.</returns>
    public Inv.Font SmallCaps(bool On = true)
    {
      this.IsSmallCaps = On;
      return this;
    }

    internal override ElementType ElementType => Inv.ElementType.Font;

    private string NameField;
    private int? SizeField;
    private Inv.Colour ColourField;
    private Inv.FontWeight? WeightField;
    private bool? ItalicsField;
    private bool? UnderlinedField;
    private bool? StrikethroughField;
    private bool? SmallCapsField;
  }

  /// <summary>
  /// The enum used for font weight.
  /// </summary>
  public enum FontWeight
  {
    /// <summary>
    /// thin font weight
    /// </summary>
    Thin,
    /// <summary>
    /// light font weight
    /// </summary>
    Light,
    /// <summary>
    /// regular font weight
    /// </summary>
    Regular,
    /// <summary>
    /// medium font weight
    /// </summary>
    Medium,
    /// <summary>
    /// bold font weight
    /// </summary>
    Bold,
    /// <summary>
    /// heavy font weight
    /// </summary>
    Heavy
  }

  /// <summary>
  /// Each corner of the panel can be rounded.
  /// </summary>
  public sealed class Corner : Element
  {
    internal Corner(Inv.Control Control)
      : base(Control)
    {
    }

    /// <summary>
    /// Top left corner in logical points.
    /// </summary>
    public int TopLeft { get; private set; }
    /// <summary>
    /// Top right corner in logical points.
    /// </summary>
    public int TopRight { get; private set; }
    /// <summary>
    /// Bottom right corner in logical points.
    /// </summary>
    public int BottomRight { get; private set; }
    /// <summary>
    /// Bottom left corner in logical points.
    /// </summary>
    public int BottomLeft { get; private set; }
    /// <summary>
    /// Ask if any corners are set.
    /// </summary>
    public bool IsSet
    {
      get
      {
        RequireThreadAffinity();

        return TopLeft != 0 || TopRight != 0 || BottomRight != 0 || BottomLeft != 0;
      }
    }
    /// <summary>
    /// Ask if the corners are uniform.
    /// Returns true if the corners are not set.
    /// </summary>
    public bool IsUniform
    {
      get
      {
        RequireThreadAffinity();

        return TopLeft == TopRight && TopRight == BottomRight && BottomRight == BottomLeft;
      }
    }

    /// <summary>
    /// Clear all corners.
    /// </summary>
    public void Clear()
    {
      Set(0, 0, 0, 0);
    }
    /// <summary>
    /// Set all corners to a uniform value.
    /// </summary>
    /// <param name="Value"></param>
    public void Set(int Value)
    {
      Set(Value, Value, Value, Value);
    }
    /// <summary>
    /// Set the individual corners.
    /// </summary>
    /// <param name="TopLeft"></param>
    /// <param name="TopRight"></param>
    /// <param name="BottomRight"></param>
    /// <param name="BottomLeft"></param>
    public void Set(int TopLeft, int TopRight, int BottomRight, int BottomLeft)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(TopLeft >= 0 && TopRight >= 0 && BottomRight >= 0 && BottomLeft >= 0, "Corners must greater than or equal to zero.");

      if (this.TopLeft != TopLeft || this.TopRight != TopRight || this.BottomRight != BottomRight || this.BottomLeft != BottomLeft)
      {
        this.TopLeft = TopLeft;
        this.TopRight = TopRight;
        this.BottomRight = BottomRight;
        this.BottomLeft = BottomLeft;

        Change();
      }
    }
    /// <summary>
    /// Ask if the corners are conformant to a uniform value.
    /// </summary>
    /// <param name="Size"></param>
    /// <returns></returns>
    public bool Is(int Size)
    {
      RequireThreadAffinity();

      return Is(Size, Size, Size, Size);
    }
    /// <summary>
    /// Ask if the individual corners are conformant.
    /// </summary>
    /// <param name="TopLeft"></param>
    /// <param name="TopRight"></param>
    /// <param name="BottomRight"></param>
    /// <param name="BottomLeft"></param>
    /// <returns></returns>
    public bool Is(int TopLeft, int TopRight, int BottomRight, int BottomLeft)
    {
      RequireThreadAffinity();

      return this.TopLeft == TopLeft && this.TopRight == TopRight && this.BottomRight == BottomRight && this.BottomLeft == BottomLeft;
    }

    internal override ElementType ElementType => Inv.ElementType.Corner;
  }

  /// <summary>
  /// The outside spacing for a panel on all four sides.
  /// </summary>
  public sealed class Margin : Edge
  {
    internal Margin(Inv.Element Element)
      : base(Element)
    {
    }
    internal Margin(Inv.Control Control)
      : base(Control)
    {
    }

    internal override ElementType ElementType => Inv.ElementType.Margin;
  }

  /// <summary>
  /// The inside spacing for a panel on all four sides.
  /// </summary>
  public sealed class Padding : Edge
  {
    internal Padding(Inv.Element Element)
      : base(Element)
    {
    }
    internal Padding(Inv.Control Control)
      : base(Control)
    {
    }

    internal override ElementType ElementType => Inv.ElementType.Padding;
  }

  /// <summary>
  /// This is the relative depth between panels along the z-axis.
  /// </summary>
  public sealed class Elevation : Element
  {
    internal Elevation(Inv.Control Control)
      : base(Control)
    {
    }

    /// <summary>
    /// Reset the elevation to zero.
    /// </summary>
    public void Clear()
    {
      Set(0);
    }
    /// <summary>
    /// Get the elevation in logical points.
    /// </summary>
    /// <returns></returns>
    public int Get()
    {
      return Depth;
    }
    /// <summary>
    /// Set the elevation in logical points.
    /// </summary>
    /// <param name="Depth"></param>
    public void Set(int Depth)
    {
      RequireThreadAffinity();

      if (this.Depth != Depth)
      {
        this.Depth = Depth;
        Change();
      }
    }

    internal override ElementType ElementType => Inv.ElementType.Elevation;

    private int Depth;
  }

  internal enum TableAxisLength
  {
    Star,
    Auto,
    Fixed
  }

  /// <summary>
  /// Represents a row or column in a table.
  /// </summary>
  public abstract class TableAxis : Element
  {
    internal TableAxis(Table Table, int Index, Action CollectionChange)
      : base(Table)
    {
      this.Table = Table;
      this.LengthType = TableAxisLength.Auto;
      this.LengthValue = 0;
      this.Index = Index;
      this.ContentSingleton = new Singleton<Inv.Panel>(Table);
      this.CollectionChange = CollectionChange;
    }

    /// <summary>
    /// Index location of the row or column.
    /// </summary>
    public int Index { get; internal set; }
    /// <summary>
    /// Content for the entire row or column.
    /// This is placed behind the cell content.
    /// </summary>
    public Inv.Panel Content
    {
      get => ContentSingleton.Data;
      set
      {
        RequireThreadAffinity();

        if (ContentSingleton.Data != value)
        {
          if (ContentSingleton.Data != null)
          {
            Table.RemoveChild(ContentSingleton.Data);
            ContentSingleton.Data = null;
          }

          if (value != null)
          {
            Table.AddChild(value);
            ContentSingleton.Data = value;
          }

          CollectionChange();
        }
      }
    }

    /// <summary>
    /// Set the row or column to auto size.
    /// </summary>
    public void Auto()
    {
      Set(TableAxisLength.Auto, 0);
    }
    /// <summary>
    /// Set the row or column to fixed size.
    /// </summary>
    /// <param name="Points"></param>
    public void Fixed(int Points)
    {
      Set(TableAxisLength.Fixed, Points);
    }
    /// <summary>
    /// Set the row or column to star size.
    /// </summary>
    /// <param name="Units"></param>
    public void Star(int Units = 1)
    {
      Set(TableAxisLength.Star, Units);
    }

    internal Table Table { get; }
    internal TableAxisLength LengthType { get; private set; }
    internal int LengthValue { get; private set; }
    internal Singleton<Inv.Panel> ContentSingleton { get; }

    internal void Set(TableAxisLength Type, int Value)
    {
      RequireThreadAffinity();

      if (Type != this.LengthType || Value != this.LengthValue)
      {
        this.LengthType = Type;
        this.LengthValue = Value;

        CollectionChange();
      }
    }

    private readonly Action CollectionChange;
  }

  /// <summary>
  /// Row in a table.
  /// </summary>
  public sealed class TableRow : TableAxis
  {
    internal TableRow(Table Table, int Index)
      : base(Table, Index, Table.RowCollection.Change)
    {
    }

    /// <summary>
    /// Enumerate the cells in this row.
    /// </summary>
    /// <returns></returns>
    public IEnumerable<TableCell> GetCells()
    {
      return Table.CellCollection.Where(C => C.Row == this);
    }

    /// <summary>
    /// Apply the panels to the cells in this row.
    /// </summary>
    /// <param name="PanelArray"></param>
    public void Compose(params Inv.Panel[] PanelArray)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(PanelArray != null && PanelArray.Length == Table.ColumnCount, "Row.Compose must have panels for exactly each column.");
        Inv.Assert.Check(PanelArray.ExceptNull().IsDistinct(), "Row.Compose must be distinct panels without any spanning cells.");
      }

      foreach (var Cell in GetCells())
      {
        if (PanelArray != null && Cell.Column.Index < PanelArray.Length)
          Cell.Content = PanelArray[Cell.Column.Index];
        else
          Cell.Content = null;
      }
    }

    internal override ElementType ElementType => Inv.ElementType.TableRow;
  }

  /// <summary>
  /// Column in a table.
  /// </summary>
  public sealed class TableColumn : TableAxis
  {
    internal TableColumn(Table Table, int Index)
      : base(Table, Index, Table.ColumnCollection.Change)
    {
    }

    /// <summary>
    /// Enumerate the cells in this column.
    /// </summary>
    /// <returns></returns>
    public IEnumerable<TableCell> GetCells()
    {
      return Table.CellCollection.Where(C => C.Column == this);
    }

    /// <summary>
    /// Apply the panels to the cells in this column.
    /// </summary>
    /// <param name="PanelArray"></param>
    public void Compose(params Inv.Panel[] PanelArray)
    {
      RequireThreadAffinity();

      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(PanelArray != null && PanelArray.Length == Table.RowCount, "Row.Compose must have panels for exactly each row.");
        Inv.Assert.Check(PanelArray.ExceptNull().IsDistinct(), "Row.Compose must be distinct panels without any spanning cells.");
      }

      foreach (var Cell in GetCells())
      {
        if (PanelArray != null && Cell.Row.Index < PanelArray.Length)
          Cell.Content = PanelArray[Cell.Row.Index];
        else
          Cell.Content = null;
      }
    }

    internal override ElementType ElementType => Inv.ElementType.TableColumn;
  }

  /// <summary>
  /// Cell in a table.
  /// </summary>
  public sealed class TableCell : Element
  {
    internal TableCell(Table Table, TableColumn Column, TableRow Row)
      : base(Table)
    {
      this.Table = Table;
      this.Column = Column;
      this.Row = Row;
      this.ContentSingleton = new Singleton<Inv.Panel>(Table);
    }

    /// <summary>
    /// X position of the cell.
    /// </summary>
    public int X => Column.Index;
    /// <summary>
    /// Y position of the cell.
    /// </summary>
    public int Y => Row.Index;
    /// <summary>
    /// Owning column of the cell.
    /// </summary>
    public TableColumn Column { get; }
    /// <summary>
    /// Owning row of the cell.
    /// </summary>
    public TableRow Row { get; }
    /// <summary>
    /// Content in the cell.
    /// </summary>
    public Inv.Panel Content
    {
      get => ContentSingleton.Data;
      set
      {
        RequireThreadAffinity();

        if (ContentSingleton.Data != value)
        {
          if (ContentSingleton.Data != null)
          {
            Table.RemoveChild(ContentSingleton.Data);
            ContentSingleton.Data = null;
          }

          if (value != null)
          {
            Table.AddChild(value);
            ContentSingleton.Data = value;
          }

          Table.CellCollection.Change();
        }
      }
    }

    internal override ElementType ElementType => Inv.ElementType.TableCell;
    internal Singleton<Inv.Panel> ContentSingleton { get; }

    private readonly Table Table;
  }

  /// <summary>
  /// Focus manager for a panel.
  /// </summary>
  public sealed class Focus : Element
  {
    internal Focus(Inv.Control Control)
      : base(Control)
    {
      this.Control = Control;
    }

    /// <summary>
    /// Handle to be notified when the panel receives focus.
    /// </summary>
    public event Action GotEvent
    {
      add
      {
        RequireThreadAffinity();
        GotDelegate += value;
        Change();
      }
      remove
      {
        RequireThreadAffinity();
        GotDelegate -= value;
        Change();
      }
    }
    /// <summary>
    /// Returns true if the GotEvent is handled on this panel.
    /// </summary>
    public bool HasGot()
    {
      return GotDelegate != null;
    }
    /// <summary>
    /// Handle to be notified when the panel loses focus.
    /// </summary>
    public event Action LostEvent
    {
      add
      {
        RequireThreadAffinity();
        LostDelegate += value;
        Change();
      }
      remove
      {
        RequireThreadAffinity();
        LostDelegate -= value;
        Change();
      }
    }
    /// <summary>
    /// Returns true if the LostEvent is handled on this panel.
    /// </summary>
    public bool HasLost()
    {
      return LostDelegate != null;
    }

    /// <summary>
    /// Gets whether this panel currently has logical focus.
    /// </summary>
    public bool Has()
    {
      RequireThreadAffinity();

      return HasField;
    }
    /// <summary>
    /// Set the focus to this panel.
    /// </summary>
    public void Set()
    {
      RequireThreadAffinity();

      Control.Window.SetFocus(Control);
    }

    internal override ElementType ElementType => Inv.ElementType.Focus;

    internal void GotInvoke()
    {
      RequireThreadAffinity();

      this.HasField = true;

      if (GotDelegate != null)
        GotDelegate();
    }
    internal void LostInvoke()
    {
      RequireThreadAffinity();

      this.HasField = false;

      if (LostDelegate != null)
        LostDelegate();
    }
    internal void SetGot(Action Delegate)
    {
      this.GotDelegate = Delegate;
    }
    internal void SetLost(Action Delegate)
    {
      this.LostDelegate = Delegate;
    }

    private readonly Inv.Control Control;
    private Action GotDelegate;
    private Action LostDelegate;
    private bool HasField;
  }

  /// <summary>
  /// Tooltip that appears when a user hovers over a panel with a mouse pointer.
  /// </summary>
  public sealed class Tooltip : Element
  {
    internal Tooltip(Inv.Control Control)
      : base(Control)
    {
    }

    /// <summary>
    /// Handle to be notified when the tooltip is shown.
    /// </summary>
    public event Action ShowEvent
    {
      add
      {
        RequireThreadAffinity();
        this.ShowDelegate += value;
        Change();
      }
      remove
      {
        RequireThreadAffinity();
        this.ShowDelegate -= value;
        Change();
      }
    }
    /// <summary>
    /// Returns true if the ShowEvent is handled on this tooltip.
    /// </summary>
    public bool HasShow()
    {
      return ShowDelegate != null;
    }
    /// <summary>
    /// Handle to be notified when the tooltip is hidden.
    /// </summary>
    public event Action HideEvent
    {
      add
      {
        RequireThreadAffinity();
        this.HideDelegate += value;
        Change();
      }
      remove
      {
        RequireThreadAffinity();
        this.HideDelegate -= value;
        Change();
      }
    }
    /// <summary>
    /// Returns true if the HideEvent is handled on this tooltip.
    /// </summary>
    public bool HasHide()
    {
      return HideDelegate != null;
    }
    /// <summary>
    /// Returns true if the tooltip is currently shown.
    /// </summary>
    public bool IsActive
    {
      get
      {
        RequireThreadAffinity();

        return ActiveField;
      }
    }    
    /// <summary>
    /// Content of the tooltip.
    /// </summary>
    public Inv.Panel Content
    {
      get
      {
        RequireThreadAffinity();

        return ContentField;
      }
      set
      {
        RequireThreadAffinity();

        if (ContentField != value)
        {
          this.ContentField = value;
          Change();
        }
      }
    }

    internal override ElementType ElementType => Inv.ElementType.Tooltip;
    internal bool IsBound => ShowDelegate != null || HideDelegate != null || ContentField != null;

    internal void ShowInvoke()
    {
      RequireThreadAffinity();

      this.ActiveField = true;

      if (ShowDelegate != null)
        ShowDelegate();
    }
    internal void HideInvoke()
    {
      RequireThreadAffinity();

      this.ActiveField = false;

      if (HideDelegate != null)
        HideDelegate();
    }
    internal void SetShow(Action Delegate)
    {
      this.ShowDelegate = Delegate;
    }
    internal void SetHide(Action Delegate)
    {
      this.HideDelegate = Delegate;
    }

    private Inv.Panel ContentField;
    private Action ShowDelegate;
    private Action HideDelegate;
    private bool ActiveField;
  }
}