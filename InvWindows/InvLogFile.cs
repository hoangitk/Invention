﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading;
using System.Diagnostics;
using System.Globalization;
using System.Security;
using System.Text.RegularExpressions;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// Represents a record-based logfile on disk.
  /// </summary>
  /// <remarks>
  /// <para><see cref="LogFile"/> provides methods to create and manipulate record-based log files on disk and optionally duplicates log output
  /// to the console.</para>
  /// </remarks>
  /// <example>
  /// <code>
  /// LogFile log = new LogFile("C:\\Temp\\temp.log");
  /// 
  /// log.Create();
  /// log.ConsoleTracing = true;
  /// 
  /// log.WriteRecord (new string[1] { "new log record" });
  /// 
  /// log.Close();
  /// </code>
  /// <para>This example produces output both on the console and in the log file C:\Temp\temp.log:</para>
  /// <code language="none">
  /// 2010-09-10 15:37:33.985         new log record
  /// </code>
  /// </example>
  public sealed class LogFile : IDisposable
  {
    /// <summary>
    /// Initializes a new <see cref="LogFile"/> object with the specified file path.
    /// </summary>
    /// <param name="Path">The path to the log file.</param>
    public LogFile(string Path)
    {
      this.Path = Path;
    }
    public void Dispose()
    {
      if (IsActive)
        Close();
    }

    /// <summary>
    /// Gets a value indicating the path of the log file.
    /// </summary>
    public string Path { get; private set; }
    /// <summary>
    /// Gets or sets a value indicating whether to duplicate logged messages to the console.
    /// </summary>
    public bool ConsoleTracing { get; set; }

    /// <summary>
    /// Creates the log file on disk.
    /// </summary>
    /// <remarks>The log file must not be active when <see cref="Create"/> is called.</remarks>
    /// <exception cref="SecurityException">Thrown if a security restriction prevents the creation of the file.</exception>
    public void Create()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(!IsActive, "File must not be active.");

      try
      {
        FileStream = new FileStream(Path, FileMode.Create, FileAccess.Write, FileShare.Read, 4096);
      }
      catch (SecurityException E)
      {
        throw new SecurityException(string.Format("Unable to create log file '{0}' due to security restrictions: '{1}'", Path, E.Message));
      }

      StreamWriter = new StreamWriter(FileStream);

      IsActive = true;
    }
    /// <summary>
    /// Opens an existing log file on disk for appending.
    /// </summary>
    /// <remarks>The log file must not be active when <see cref="Append"/> is called.</remarks>
    /// <exception cref="SecurityException">Thrown if a security restriction prevents the file from being opened for appending.</exception>
    public void Append()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(!IsActive, "File must not be active.");

      try
      {
        FileStream = new FileStream(Path, FileMode.Append, FileAccess.Write, FileShare.Read, 4096);
      }
      catch (SecurityException E)
      {
        throw new SecurityException(string.Format("Unable to append log file '{0}' due to security restrictions: '{1}'", Path, E.Message));
      }

      StreamWriter = new StreamWriter(FileStream);

      IsActive = true;
    }
    /// <summary>
    /// Close the log file.
    /// </summary>
    /// <remarks>The log file must be active when <see cref="Close"/> is called.</remarks>
    public void Close()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(IsActive, "File must be active.");

      this.IsActive = false;

      if (StreamWriter != null)
      {
        StreamWriter.Dispose();
        StreamWriter = null;
      }

      if (FileStream != null)
      {
        FileStream.Dispose();
        FileStream = null;
      }
    }
    /// <summary>
    /// Delete the log file from the disk.
    /// </summary>
    /// <remarks>The log file must not be active when <see cref="Delete"/> is called.</remarks>
    public void Delete()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(!IsActive, "File must not be active.");

      if (File.Exists(Path))
        File.Delete(Path);
    }
    /// <summary>
    /// Write the beginning of a record to the log file.
    /// </summary>
    public void WriteRecordBegin()
    {
      WriteRecordField(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff", CultureInfo.CurrentCulture));
      WriteRecordTab();
      WriteRecordField(Inv.ThreadGovernor.ActiveIdentity());
    }
    /// <summary>
    /// Write the end of a record to the log file and flush the stream.
    /// </summary>
    public void WriteRecordEnd()
    {
      if (ConsoleTracing)
        Console.WriteLine();

      StreamWriter.WriteLine();
      StreamWriter.Flush();
    }
    /// <summary>
    /// Write a string field to the log file.
    /// </summary>
    /// <param name="Field">The string field to write.</param>
    public void WriteRecordField(string Field)
    {
      if (ConsoleTracing)
        Console.Write(Field);

      StreamWriter.Write(Field);
    }
    /// <summary>
    /// Write a tab to the log file.
    /// </summary>
    public void WriteRecordTab()
    {
      if (ConsoleTracing)
        Console.Write("\t");

      StreamWriter.Write("\t");
    }
    /// <summary>
    /// Write an array of fields to the log file.
    /// </summary>
    /// <remarks>
    /// The array will be written tab-delimited to the log file.
    /// </remarks>
    /// <param name="FieldArray">The array to write to the log file.</param>
    public void WriteRecord(params string[] FieldArray)
    {
      WriteRecordBegin();

      foreach (var Field in FieldArray) 
      {
        WriteRecordTab();
        WriteRecordField(Field);
      }

      WriteRecordEnd();
    }

    private bool IsActive;
    private FileStream FileStream;
    private StreamWriter StreamWriter;
  }

  /// <summary>
  /// Represents a folder containing multiple log file series.
  /// </summary>
  /// <remarks>
  /// <para>After creating a <see cref="LogFolder"/> object, create one or more <see cref="LogSeries"/> objects using <see cref="AddSeries"/> and then call <see cref="LogSeries.Acquire"/>
  /// on the <see cref="LogSeries"/> object(s) to receive <see cref="LogHandle"/> objects which can be used to write to disk.</para>
  /// <para>Unlike <see cref="LogFile"/>, actual writes to the log file are unmanaged; <see cref="LogHandle"/> directly exposes a <see cref="FileStream"/>, so for logs that comprise mostly
  /// string records, <see cref="LogFile"/> will probably be more useful.</para>
  /// </remarks>
  /// <example>
  /// <para>This example will write a single log entry to the 'series1' log series in C:\Temp. Each time the code is run, a new log file will be generated with 
  /// an incrementing filename; files older than 30 days (by default) will be automatically deleted.</para>
  /// <code>
  /// LogFolder folder = new LogFolder("C:\\Temp");
  /// LogSeries series = folder.AddSeries("series1");
  ///
  /// folder.Open();
  ///
  /// using (LogContext ctx = series.AcquireContext())
  /// {
  ///   StreamWriter writer = new StreamWriter(ctx.FileStream);
  ///
  ///   writer.WriteLine("Log entry");
  ///
  ///   writer.Close();
  /// }
  /// </code>
  /// </example>
  public sealed class LogDirectory : ILogDirectory
  {
    /// <summary>
    /// Initializes a new <see cref="LogFolder"/> with the specified path.
    /// </summary>
    /// <param name="Path">The path for the log folder.</param>
    public LogDirectory(string Path)
    {
      this.Path = Path;
      this.Directory = new DirectoryInfo(Path);
   }

    /// <summary>
    /// Gets a value indicating the path for the <see cref="LogFolder"/>.
    /// </summary>
    public string Path { get; private set; }

    public Inv.LogFolder ToFolder()
    {
      return new Inv.LogFolder(this);
    }
    public IEnumerable<string> GetFiles(string SearchMask)
    {
      Directory.Create(); // force the folder to exist.

      return Directory.GetFiles(SearchMask, SearchOption.TopDirectoryOnly).Select(F => F.Name);
    }
    public Stream StartFile(string Name)
    {
      Directory.Create(); // force the folder to exist.

      return new FileStream(System.IO.Path.Combine(Path, Name), FileMode.CreateNew, FileAccess.Write, FileShare.Read, 4096);
    }
    public Stream OpenFile(string Name)
    {
      return new FileStream(System.IO.Path.Combine(Path, Name), FileMode.Open);
    }
    public bool ExistsFile(string Name)
    {
      return System.IO.File.Exists(System.IO.Path.Combine(Path, Name));
    }
    public void DeleteFile(string Name)
    {
      System.IO.File.Delete(System.IO.Path.Combine(Path, Name));
    }

    private DirectoryInfo Directory;
  }
}
