﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using Inv.Support;

namespace Inv
{
  public sealed class SocketHost
  {
    public SocketHost(byte[] CertHash)
    {
      this.CertHash = CertHash;
      this.ChannelList = new Inv.DistinctList<SocketChannel>();
    }

    public bool IsActive { get; private set; }
    public int Port { get; set; }
    public System.Security.Cryptography.X509Certificates.X509Certificate2 Certificate { get; private set; }
    public event Action StartEvent;
    public event Action StopEvent;
    public event Action<Exception> FaultEvent;
    public event Action<SocketChannel> AcceptEvent;
    public event Action<SocketChannel> RejectEvent;

    public void Start()
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Port != 0, "Port must be specified.");
        //Inv.Assert.Check(Certificate != null, "Certificate must be specified.");
      }

      if (!IsActive)
      {
        if (CertHash != null)
        {
          foreach (var StoreLocation in new[] { System.Security.Cryptography.X509Certificates.StoreLocation.CurrentUser, System.Security.Cryptography.X509Certificates.StoreLocation.LocalMachine })
          {
            var Store = new System.Security.Cryptography.X509Certificates.X509Store(System.Security.Cryptography.X509Certificates.StoreName.My, StoreLocation);
            Store.Open(System.Security.Cryptography.X509Certificates.OpenFlags.ReadOnly);
            try
            {
              foreach (var Cert in Store.Certificates)
              {
                if (Cert.GetCertHash().ShallowEqualTo(CertHash))
                {
                  this.Certificate = Cert;
                  break;
                }
              }
            }
            finally
            {
              Store.Close();
            }

            if (this.Certificate != null)
              break;
          }

          if (Inv.Assert.IsEnabled)
            Inv.Assert.Check(Certificate == null || Certificate.GetCertHash().ShallowEqualTo(CertHash), "Certificate not found or CertHash mismatch.");
        }

        // cannot start without SSL certificate, we cannot allow an insecure socket to start listening.
        if (CertHash == null || Certificate != null)
        {
          this.TcpServer = new System.Net.Sockets.TcpListener(System.Net.IPAddress.Any, Port);
          TcpServer.Start();
          TcpServer.BeginAcceptTcpClient(AcceptTcpClient, null);

          this.IsActive = true;

          if (StartEvent != null)
            StartEvent();
        }
      }
    }
    public void Stop()
    {
      if (IsActive)
      {
        this.IsActive = false;

        if (StopEvent != null)
          StopEvent();

        if (TcpServer != null)
        {
          TcpServer.Stop();
          this.TcpServer = null;
        }

        foreach (var Channel in ChannelList)
        {
          try
          {
            Channel.Drop();
          }
          catch
          {
          }
        }

        ChannelList.Clear();
      }
    }

    internal void CloseChannel(SocketChannel Channel)
    {
      try
      {
        Channel.ClosedInvoke();

        if (RejectEvent != null)
          RejectEvent(Channel);
      }
      finally
      {
        Channel.Drop();

        lock (ChannelList)
          ChannelList.Remove(Channel);
      }
    }

    private void HandleFault(Exception Exception)
    {
      try
      {
        if (FaultEvent != null)
          FaultEvent(Exception);
      }
      catch
      {
        if (Debugger.IsAttached)
          Debugger.Break();
      }
    }
    private void AcceptTcpClient(IAsyncResult Result)
    {
      try
      {
        // immediately wait for the next client.
        if (IsActive)
        {
          TcpServer.BeginAcceptTcpClient(AcceptTcpClient, null);

          var TcpClient = EndAcceptTcpClient(Result);

          if (TcpClient != null)
          {
            TcpClient.NoDelay = true;
            TcpClient.ReceiveBufferSize = Inv.TransportFoundation.ReceiveBufferSize;
            TcpClient.SendBufferSize = Inv.TransportFoundation.SendBufferSize;

            var Channel = new SocketChannel(this, TcpClient);

            lock (ChannelList)
              ChannelList.Add(Channel);

            if (AcceptEvent != null)
              AcceptEvent(Channel);
          }
        }
      }
      catch (Exception Exception)
      {
        HandleFault(Exception);
      }
    }
    [DebuggerNonUserCode]
    private TcpClient EndAcceptTcpClient(IAsyncResult Result)
    {
      try
      {
        return TcpServer.EndAcceptTcpClient(Result);
      }
      catch (System.Net.Sockets.SocketException Exception)
      {
        // A blocking operation was interrupted by a call to WSACancelBlockingCall"
        if (Exception.SocketErrorCode == SocketError.Interrupted && Exception.ErrorCode == 10004)
          return null;
        else
          throw Exception.Preserve();
      }
    }

    private readonly byte[] CertHash;
    private System.Net.Sockets.TcpListener TcpServer;
    private Inv.DistinctList<SocketChannel> ChannelList;
  }

  public sealed class SocketChannel : Inv.TransportConnection
  {
    internal SocketChannel(SocketHost Host, System.Net.Sockets.TcpClient TcpClient)
    {
      this.Host = Host;
      this.TcpClient = TcpClient;

      this.TcpStream = TcpClient.GetStream();

      if (Host.Certificate != null)
      {
        var SecureStream = new System.Net.Security.SslStream(TcpStream, false);
        SecureStream.AuthenticateAsServer(Host.Certificate, false, System.Security.Authentication.SslProtocols.Tls12, true);
        //SecureStream.ReadTimeout = 5000;
        //SecureStream.WriteTimeout = 5000;

        this.TcpStream = SecureStream;
      }

      this.Flow = new Inv.TransportFlow(TcpStream);

      this.IsActive = true;
    }

    public SocketHost Host { get; private set; }
    public bool IsActive { get; private set; }
    public System.Net.IPAddress RemoteIPAddress
    {
      get { return ((System.Net.IPEndPoint)TcpClient.Client.RemoteEndPoint).Address; }
    }
    public Inv.TransportFlow Flow { get; private set; }
    public event Action ClosedEvent;

    public void Drop()
    {
      this.IsActive = false;
      this.TcpStream.Dispose();
      this.TcpClient.Close();
    }

    internal void ClosedInvoke()
    {
      if (ClosedEvent != null)
        ClosedEvent();
    }

    private System.Net.Sockets.TcpClient TcpClient;
    private System.IO.Stream TcpStream;
  }

  public sealed class SocketLink
  {
    public SocketLink(byte[] CertHash)
    {
      this.Factory = new Inv.SocketFactory(CertHash);
      this.Base = new TransportLink(Factory);
    }

    public string Host
    {
      get { return Factory.Host; }
      set { Factory.Host = value; }
    }
    public int Port
    {
      get { return Factory.Port; }
      set { Factory.Port = value; }
    }
    public bool IsActive
    {
      get { return Base.IsActive; }
    }
    public event Action<Exception> FaultBeginEvent
    {
      add { Base.FaultBeginEvent += value; }
      remove { Base.FaultBeginEvent -= value; }
    }
    public event Action<Exception> FaultContinueEvent
    {
      add { Base.FaultContinueEvent += value; }
      remove { Base.FaultContinueEvent -= value; }
    }
    public event Action<Exception> FaultEndEvent
    {
      add { Base.FaultEndEvent += value; }
      remove { Base.FaultEndEvent -= value; }
    }

    public void Connect()
    {
      Base.Connect();
    }
    public void Disconnect()
    {
      Base.Disconnect();
    }
    public Inv.SocketConnection TakeConnection()
    {
      return (Inv.SocketConnection)Base.TakeConnection();
    }
    public void ReturnConnection(Inv.SocketConnection Connection)
    {
      Base.ReturnConnection(Connection);
    }
    public void Exchange(Action<Inv.TransportConnection> Action)
    {
      Base.Exchange(Action);
    }
    public T Exchange<T>(Func<Inv.TransportConnection, T> Func)
    {
      return Base.Exchange(Func);
    }

    public static implicit operator Inv.TransportLink(SocketLink Self) => Self?.Base;

    private Inv.TransportLink Base;
    private SocketFactory Factory;
  }

  public sealed class SocketFactory : Inv.TransportFactory
  {
    public SocketFactory(byte[] CertHash)
    {
      this.CertHash = CertHash;
    }

    public string Host { get; set; }
    public int Port { get; set; }

    TransportConnection TransportFactory.JoinConnection()
    {
      return new SocketConnection(Host, Port, CertHash);
    }

    private readonly byte[] CertHash;
  }

  public sealed class SocketConnection : Inv.TransportConnection
  {
    public SocketConnection(string Host, int Port, byte[] CertHash)
    {
      this.TcpClient = new System.Net.Sockets.TcpClient();
      TcpClient.SendBufferSize = Inv.TransportFoundation.SendBufferSize;
      TcpClient.ReceiveBufferSize = Inv.TransportFoundation.ReceiveBufferSize;
      TcpClient.NoDelay = true;

      try
      {
        TcpClient.Connect(Host, Port);

        this.TcpStream = TcpClient.GetStream();

        if (CertHash != null)
        {
          var SecureStream = new System.Net.Security.SslStream(TcpStream, true, (sender, cert, chain, sslPolicy) => CertHash == null || cert.GetCertHash().ShallowEqualTo(CertHash));
          SecureStream.AuthenticateAsClient(Host, null, System.Security.Authentication.SslProtocols.Tls12, false);
          //SecureStream.ReadTimeout = 5000;
          //SecureStream.WriteTimeout = 5000;

          this.TcpStream = SecureStream;
        }

        this.Flow = new Inv.TransportFlow(TcpStream);
      }
      catch
      {
        if (TcpStream != null)
        {
          TcpStream.Dispose();
          this.TcpStream = null;
        }

        TcpClient.Close();

        throw;
      }
    }

    public Inv.TransportFlow Flow { get; private set; }

    public void Drop()
    {
      if (TcpStream != null)
      {
        try
        {
          TcpStream.Dispose();
        }
        catch
        {
          // TODO: is this allowed?
        }

        this.TcpStream = null;
      }

      try
      {
        TcpClient.Close();
      }
      catch
      {
        // TODO: is this allowed?
      }
    }

    private System.Net.Sockets.TcpClient TcpClient;
    private System.IO.Stream TcpStream;
  }
}