﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Reflection;
using System.Diagnostics;
using Inv.Support;

namespace Inv.Persist
{
  /// <summary>
  /// The governor is a registration of classes for object persistence (aka serialization).
  /// All classes involved must be registered with the governor.
  /// There is shorthand for registration of polymorphic classes and custom serialization.
  /// </summary>
  public sealed class Governor
  {
    /// <summary>
    /// Create a new persistence governor.
    /// </summary>
    public Governor()
    {
      this.RegisterList = new DistinctList<Register>();
      this.RegisterDictionary = new Dictionary<Type, Register>();
    }

    /// <summary>
    /// Register a class for persistence.
    /// </summary>
    /// <typeparam name="TRegister"></typeparam>
    /// <returns></returns>
    public Register<TRegister> Register<TRegister>()
    {
      return new Register<TRegister>(AddRegister(typeof(TRegister)));
    }
    /// <summary>
    /// Validate this class and dependencies are all registered with the governor.
    /// </summary>
    /// <typeparam name="TRoot"></typeparam>
    public void Validate<TRoot>()
    {
      var VisitSet = new HashSet<Register>();

      // recursively check the registrations.
      void Check(Register Register)
      {
        if (VisitSet.Add(Register))
        {
          if (!Register.IsCustom)
          {
            if (Register.RegisterType.GetReflectionInfo().IsClass && Register.RegisterType.ReflectionDefaultConstructor() == null)
              throw new Exception(Register.RegisterType.FullName + " must have a default constructor.");

            foreach (var Serial in Register.SerialList)
            {
              if (Serial.Ignored)
                continue;

              if (Serial.Primitive == null && !Serial.IsEnum)
              {
                var FieldRegister = GetRegisterOrDefault(Serial.ItemType);
                if (FieldRegister == null)
                  throw new Exception(Register.RegisterType.FullName + " serialises " + Serial.ItemType.FullName + " but there is no register.");

                Check(FieldRegister);
              }
            }
          }

          if (Register.PolymorphList != null)
          {
            foreach (var Polymorph in Register.PolymorphList)
              Check(Polymorph);
          }
        }
      }

      var Root = GetRegister(typeof(TRoot));

      Check(Root);
    }
    /// <summary>
    /// Save a record to the stream.
    /// </summary>
    /// <typeparam name="TRegister"></typeparam>
    /// <param name="Record"></param>
    /// <param name="Stream"></param>
    public void Save<TRegister>(TRegister Record, Stream Stream)
    {
      var Register = GetRegister(typeof(TRegister));
      if (Register.IsCustom)
        throw new Exception("Cannot save a custom register as the root object.");

      using (var Context = new Save(this, Stream))
      {
        Context.Writer.WriteInt32(Register.RegisterIndex);

        Context.WriteRoot(Register, Record);
      }
    }
    /// <summary>
    /// Load a record from the stream.
    /// </summary>
    /// <typeparam name="TRegister"></typeparam>
    /// <param name="Record"></param>
    /// <param name="Stream"></param>
    public void Load<TRegister>(TRegister Record, Stream Stream)
    {
      var Register = GetRegister(typeof(TRegister));
      if (Register.IsCustom)
        throw new Exception("Cannot load a custom register as the root object.");

      using (var Context = new Load(this, Stream))
      {
        var RootIndex = Context.Reader.ReadInt32();

        if (RootIndex != Register.RegisterIndex)
          throw new Exception("Root object load known index mismatch.");

        Context.ReadRoot(Register, Record);
      }
    }

    internal Register AddRegister(Type RegisterType)
    {
      var Result = new Register(this, RegisterType, RegisterDictionary.Count + 1);

      RegisterList.Add(Result);
      RegisterDictionary.Add(RegisterType, Result);

      return Result;
    }
    internal Register GetRegisterOrDefault(Type Type)
    {
      return RegisterDictionary.GetValueOrDefault(Type);
    }
    internal Register GetRegister(Type Type)
    {
      var Result = GetRegisterOrDefault(Type);

      if (Result == null)
        throw new Exception("Register not found for type: " + Type);

      return Result;
    }

    private readonly Dictionary<Type, Register> RegisterDictionary;
    private readonly Inv.DistinctList<Register> RegisterList;
  }

  /// <summary>
  /// Represents a single class that is registered for persistence.
  /// By default, the fields are persisted using reflection.
  /// </summary>
  /// <typeparam name="TRegister"></typeparam>
  public sealed class Register<TRegister>
  {
    internal Register(Register Base)
    {
      this.Base = Base;
    }

    /// <summary>
    /// Custom serialization for this class.
    /// </summary>
    /// <param name="SaveAction"></param>
    /// <param name="LoadFunction"></param>
    public void AsCustom(Action<Inv.CompactWriter, TRegister> SaveAction, Func<Inv.CompactReader, TRegister> LoadFunction)
    {
      Base.AsCustom((S, R) => SaveAction(S.Writer, (TRegister)R), (L) => LoadFunction(L.Reader));
    }
    /// <summary>
    /// The class is serialized using a lookup dictionary.
    /// That is, only the identifying string is written to the stream.
    /// </summary>
    /// <param name="Registers"></param>
    /// <param name="HandleFunction"></param>
    public void AsLookup(IEnumerable<TRegister> Registers, Func<TRegister, string> HandleFunction)
    {
      var Dictionary = new Dictionary<string, TRegister>(StringComparer.OrdinalIgnoreCase);
      foreach (var Register in Registers)
      {
        var Handle = HandleFunction(Register);
        Dictionary.Add(Handle, Register);
      }

      AsCustom
      (
        (Save, R) =>
        {
          var Handle = HandleFunction(R);
          Save.WriteString(Handle);
        },
        (Load) =>
        {
          var Handle = Load.ReadString();
          var Result = Dictionary.GetValueOrDefault(Handle);

          if (Result == null)
            throw new Exception(Base.Name + " lookup for '" + Handle + "' had no entry.");

          return Result;
        }
      );
    }
    /// <summary>
    /// Register this class and all immediate subclasses.
    /// </summary>
    public void AsPolymorph()
    {
      Base.AsPolymorph();
    }
    /// <summary>
    /// Register this class as having only one instance referenced in the object graph.
    /// </summary>
    /// <param name="Singleton"></param>
    public void AsSingleton(TRegister Singleton)
    {
      Base.AsSingleton(Singleton);
    }
    /// <summary>
    /// Exclude a field on the registered class from the serialization.
    /// </summary>
    /// <param name="Name"></param>
    public void Ignore(string Name)
    {
      Base.Ignore(Name);
    }
    /// <summary>
    /// Exclude a set of fields on the registered class from the serialization.
    /// </summary>
    /// <param name="NameArray"></param>
    public void Ignore(params string[] NameArray)
    {
      Base.Ignore(NameArray);
    }

    private readonly Register Base;
  }

  internal sealed class Register
  {
    internal Register(Governor Governor, Type RegisterType, int RegisterIndex)
    {
      this.Governor = Governor;
      this.RegisterType = RegisterType;
      this.RegisterIndex = RegisterIndex;

      var RegisterInfo = RegisterType.GetReflectionInfo();

      this.SerialList = new DistinctList<Serial>();

      // NOTE: all data properties have backing fields - don't need to double up.
      //foreach (var ReflectionProperty in RegisterInfo.GetReflectionProperties().Where(P => P.GetReflectionGetMethod() != null && !P.GetReflectionGetMethod().IsStatic && P.GetReflectionSetMethod() != null && !P.GetReflectionSetMethod().IsStatic))
      //  SerialList.Add(new Serial(ReflectionProperty));

      // all non-static fields.
      foreach (var ReflectionField in RegisterInfo.GetReflectionFields().Where(F => !F.IsStatic).OrderBy(F => F.Name, StringComparer.OrdinalIgnoreCase))
        SerialList.Add(new Serial(ReflectionField));

      this.PolymorphList = null;
    }

    internal string Name
    {
      get { return RegisterType.Name; }
    }
    internal Type RegisterType { get; private set; }
    internal int RegisterIndex { get; private set; }
    internal bool IsCustom
    {
      get { return Custom != null; }
    }
    internal Custom Custom { get; private set; }
    internal Inv.DistinctList<Serial> SerialList { get; private set; }
    internal Inv.DistinctList<Register> PolymorphList { get; private set; }

    public void AsCustom(Action<Save, object> SaveAction, Func<Load, object> LoadFunction)
    {
      this.Custom = new Custom(SaveAction, LoadFunction);
    }
    public void AsPolymorph()
    {
      var TypeList = RegisterType.GetReflectionInfo().Assembly.GetReflectionTypes().Where(T => T.GetReflectionInfo().BaseType == RegisterType).OrderBy(T => T.Name, StringComparer.OrdinalIgnoreCase).ToDistinctList();
      this.PolymorphList = TypeList.Select(P =>
      {
        var PolymorphRegister = Governor.AddRegister(P);

        // inherited fields.
        if (SerialList.Count > 0)
          PolymorphRegister.SerialList.InsertRange(0, SerialList);

        return PolymorphRegister;
      }).ToDistinctList();

      AsCustom
      (
        (Save, R) =>
        {
          var PolymorphIndex = TypeList.IndexOf(R.GetType());
          Save.Writer.WriteInt32(PolymorphIndex);

          var PolymorphRegister = PolymorphList[PolymorphIndex];
          Save.WriteRecord(PolymorphRegister, R);
        },
        (Load) =>
        {
          var PolymorphIndex = Load.Reader.ReadInt32();
          var PolymorphInstance = TypeList[PolymorphIndex].ReflectionCreate();

          var PolymorphRegister = PolymorphList[PolymorphIndex];
          Load.ReadRecord(PolymorphRegister, PolymorphInstance);

          return PolymorphInstance;
        }
      );
    }
    public void AsSingleton(object Singleton)
    {
      AsCustom((Save, Context) =>
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Check(Singleton == Context, "Class was registered as a singleton and must have only one instance in the persisted object graph.");
      }, (Load) =>
      {
        return Singleton;
      });
    }
    public void Ignore(params string[] NameArray)
    {
      foreach (var Name in NameArray)
        Ignore(Name);
    }
    public void Ignore(string Name)
    {
      var Serial = SerialList.Find(S => S.Name == Name);

      if (Serial == null)
        Serial = SerialList.Find(S => S.Name == "<" + Name + ">k__BackingField");

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Serial != null, "Serial not found: {0}", Name);

      if (Serial != null)
        Serial.Ignored = true;
    }

    private readonly Governor Governor;
  }

  internal sealed class Serial
  {
    internal Serial(PropertyInfo PropertyInfo)
      : this(PropertyInfo.Name, PropertyInfo.PropertyType)
    {
      this.PropertyInfo = PropertyInfo;
    }
    internal Serial(FieldInfo FieldInfo)
      : this (FieldInfo.Name, FieldInfo.FieldType)
    {
      this.FieldInfo = FieldInfo;
    }
    internal Serial(string Name, Type Type)
    {
      this.Name = Name;
      this.Type = Type;
      this.IsNullable = Type.IsNullable();
      this.IsGrid = Type.IsSubclassOfRawGeneric(typeof(Inv.Grid<>));
      this.IsHashSet = Type.IsSubclassOfRawGeneric(typeof(HashSet<>));
      //this.IsDictionary = SerialType.IsSubclassOfRawGeneric(typeof(Dictionary<,>));
      this.IsDistinctList = Type.IsSubclassOfRawGeneric(typeof(Inv.DistinctList<>));
      this.IsEnumArray = Type.IsSubclassOfRawGeneric(typeof(Inv.EnumArray<,>));
      this.IsArray = Type.IsArray && Type != typeof(byte[]);

      if (IsDistinctList)
        this.ItemType = Type.GetReflectionGenericTypeArguments()[0];
      else if (IsGrid)
        this.ItemType = Type.GetReflectionGenericTypeArguments()[0];
      else if (IsHashSet)
        this.ItemType = Type.GetReflectionGenericTypeArguments()[0];
      else if (IsEnumArray)
        this.ItemType = Type.GetReflectionGenericTypeArguments()[1];
      else if (IsArray)
        this.ItemType = Type.GetElementType();
      else if (Type.GetReflectionInfo().IsGenericType && Type.GetGenericTypeDefinition() == typeof(Nullable<>))
        this.ItemType = Nullable.GetUnderlyingType(Type);
      else
        this.ItemType = Type;

      this.Primitive = Foundation.PrimitiveCustomDictionary.GetValueOrDefault(ItemType);
      this.IsEnum = ItemType.GetReflectionInfo().IsEnum;
    }

    internal readonly string Name;
    internal readonly Type Type;
    internal readonly bool IsNullable;
    internal readonly bool IsDistinctList;
    internal readonly bool IsArray;
    internal readonly bool IsEnum;
    internal readonly bool IsGrid;
    internal readonly bool IsHashSet;
    internal readonly bool IsEnumArray;
    internal readonly Type ItemType;
    internal readonly Custom Primitive;

    internal bool Ignored { get; set; }

    internal object GetFunction(object Self)
    {
      if (PropertyInfo != null)
        return PropertyInfo.GetReflectionValue(Self);
      else
        return FieldInfo.GetReflectionValue(Self);
    }
    internal void SetAction(object Self, object Value)
    {
      if (PropertyInfo != null)
        PropertyInfo.SetReflectionValue(Self, Value);
      else
        FieldInfo.SetReflectionValue(Self, Value);
    }

    private readonly PropertyInfo PropertyInfo;
    private readonly FieldInfo FieldInfo;
  }

  internal sealed class Custom
  {
    internal Custom(Action<Save, object> SaveAction, Func<Load, object> LoadFunction)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.CheckNotNull(SaveAction, nameof(SaveAction));
        Inv.Assert.CheckNotNull(LoadFunction, nameof(LoadFunction));
      }

      this.SaveAction = SaveAction;
      this.LoadFunction = LoadFunction;
    }

    internal readonly Action<Save, object> SaveAction;
    internal readonly Func<Load, object> LoadFunction;
  }

  internal sealed class Save : IDisposable
  {
    internal Save(Governor Governor, Stream Stream)
    {
      this.Governor = Governor;
      this.Writer = new Inv.CompactWriter(Stream, false);
      this.HandleDictionary = new Dictionary<object, uint>(256 * 1024); // 256 * 1024 * 8 = 2MB (the 8 is the key-value entry).
    }
    public void Dispose()
    {
      Writer.Dispose();
    }

    public Governor Governor { get; private set; }
    public Inv.CompactWriter Writer { get; private set; }

    internal Dictionary<object, uint> HandleDictionary { get; private set; }

    internal void WriteRoot(Register Register, object Record)
    {
      HandleDictionary.Clear();

      var Handle = (uint)HandleDictionary.Count;
      HandleDictionary.Add(Record, Handle);

      WriteHandle(Handle);
      WriteRecord(Register, Record);
    }
    internal void WriteRecord(Register Register, object Record)
    {
      //Debug.WriteLine(Register.Name);
      //WriteInt32(Register.RegisterIndex);

      foreach (var Serial in Register.SerialList)
      {
        try
        {
          if (Serial.Ignored)
            continue;

          var DataValue = Serial.GetFunction(Record);

          if (Serial.IsNullable)
            Writer.WriteBoolean(DataValue == null);

          if (DataValue != null)
          {
            if (Serial.IsDistinctList)
            {
              var List = (Inv.IDistinctList)DataValue;
              Writer.WriteInt32(List.Count);

              if (Serial.Primitive != null)
              {
                foreach (var Item in List)
                  Serial.Primitive.SaveAction(this, Item);
              }
              else if (Serial.IsEnum)
              {
                foreach (var Item in List)
                  Writer.WriteInt32((int)Item);
              }
              else
              {
                var ItemRegister = Governor.GetRegister(Serial.ItemType);

                foreach (var Item in List)
                  WriteRegister(ItemRegister, Item);
              }
            }
            else if (Serial.IsHashSet)
            {
              var HashSet = ((System.Collections.IEnumerable)DataValue).ToEnumerableArray();

              Writer.WriteInt32(HashSet.Length);

              if (Serial.Primitive != null)
              {
                foreach (var Item in HashSet)
                  Serial.Primitive.SaveAction(this, Item);
              }
              else if (Serial.IsEnum)
              {
                foreach (var Item in HashSet)
                  Writer.WriteInt32((int)Item);
              }
              else
              {
                var ItemRegister = Governor.GetRegister(Serial.ItemType);

                foreach (var HashItem in HashSet)
                  WriteRegister(ItemRegister, HashItem);
              }
            }
            else if (Serial.IsGrid)
            {
              var Grid = (Inv.IGrid)DataValue;

              Writer.WriteInt32(Grid.Width);
              Writer.WriteInt32(Grid.Height);

              var ItemRegister = Governor.GetRegister(Serial.ItemType);

              for (var Y = 0; Y < Grid.Height; Y++)
              {
                for (var X = 0; X < Grid.Width; X++)
                {
                  var ItemValue = Grid[X, Y];
                  var ItemIsNull = ItemValue == null;

                  Writer.WriteBoolean(ItemIsNull);

                  if (!ItemIsNull)
                    WriteRegister(ItemRegister, ItemValue);
                }
              }
            }
            else if (Serial.IsEnumArray)
            {
              var EnumArray = (Inv.IEnumArray)DataValue;

              if (Serial.Primitive != null)
              {
                foreach (var Tuple in EnumArray.GetTuples())
                  Serial.Primitive.SaveAction(this, Tuple.Value);
              }
              else
              {
                var ItemRegister = Governor.GetRegister(Serial.ItemType);

                foreach (var Tuple in EnumArray.GetTuples())
                {
                  var ItemIsNull = Tuple.Value == null;

                  Writer.WriteBoolean(ItemIsNull);

                  if (!ItemIsNull)
                    WriteRegister(ItemRegister, Tuple.Value);
                }
              }
            }
            else if (Serial.IsArray)
            {
              var Array = (Array)DataValue;

              Writer.WriteInt32(Array.Length);

              if (Serial.Primitive != null)
              {
                foreach (var Item in Array)
                  Serial.Primitive.SaveAction(this, Item);
              }
              else if (Serial.IsEnum)
              {
                foreach (var Item in Array)
                  Writer.WriteInt32((int)Item);
              }
              else
              {
                var ItemRegister = Governor.GetRegister(Serial.ItemType);

                foreach (var Item in Array)
                  WriteRegister(ItemRegister, Item);
              }
            }
            else if (Serial.Primitive != null)
            {
              Serial.Primitive.SaveAction(this, DataValue);
            }
            else if (Serial.IsEnum)
            {
              Writer.WriteInt32((int)DataValue);
            }
            else
            {
              var FieldRegister = Governor.GetRegister(Serial.ItemType);
              WriteRegister(FieldRegister, DataValue);
            }
          }
        }
        catch (Exception Exception)
        {
          if (Exception is WriteException)
            throw;

          throw new WriteException(Register.Name + ": failed to write serial " + Serial.Name, Exception.Preserve());
        }
      }
    }
    internal void WriteRegister(Register Register, object DataValue)
    {
      if (Register.IsCustom)
      {
        Register.Custom.SaveAction(this, DataValue);
      }
      else
      {
        if (HandleDictionary.TryGetValue(DataValue, out uint Handle))
        {
          WriteHandle(Handle);
        }
        else
        {
          Handle = (uint)HandleDictionary.Count;
          HandleDictionary.Add(DataValue, Handle);

          WriteHandle(Handle);
          WriteRecord(Register, DataValue);
        }
      }
    }
    internal void WriteHandle(uint Handle)
    {
      // TODO: optimise to byte -> ushort -> uint -> ulong?

      Writer.WriteUInt32(Handle);
    }
  }

  internal sealed class Load : IDisposable
  {
    internal Load(Governor Governor, Stream Stream)
    {
      this.Governor = Governor;
      this.Reader = new Inv.CompactReader(Stream, false);
      this.LookupDictionary = new Dictionary<uint, object>(256 * 1024);
    }
    public void Dispose()
    {
      Reader.Dispose();
    }

    public Governor Governor { get; private set; }
    public Inv.CompactReader Reader { get; private set; }

    internal Dictionary<uint, object> LookupDictionary { get; private set; }

    internal void ReadRoot(Register Register, object Record)
    {
      LookupDictionary.Clear();

      var Handle = ReadHandle(Register);
      LookupDictionary.Add(Handle, Record);
      ReadRecord(Register, Record);
    }
    internal void ReadRecord(Register Register, object Record)
    {
      //Debug.WriteLine(Register.Name);
      //var RecordIndex = ReadInt32();
      //if (RecordIndex != Register.RegisterIndex)
      //  throw new Exception("Register mismatch.");

      foreach (var Serial in Register.SerialList)
      {
        if (Serial.Ignored)
          continue;

        bool IsNull;

        if (Serial.IsNullable)
          IsNull = Reader.ReadBoolean();
        else
          IsNull = false;

        object DataValue;

        if (IsNull)
        {
          DataValue = null;
        }
        else if (Serial.IsDistinctList)
        {
          var ListCount = Reader.ReadInt32();

          var List = (Inv.IDistinctList)Serial.Type.ReflectionCreate(typeof(int), ListCount);
          DataValue = List;

          if (ListCount > 0)
          {
            List.Capacity = ListCount;

            if (Serial.Primitive != null)
            {
              foreach (var Index in ListCount.NumberSeries())
              {
                var Item = Serial.Primitive.LoadFunction(this);
                List.Add(Item);
              }
            }
            else if (Serial.IsEnum)
            {
              foreach (var Index in ListCount.NumberSeries())
              {
                var Item = Reader.ReadInt32();
                List.Add(Item);
              }
            }
            else
            {
              var ItemRegister = Governor.GetRegister(Serial.ItemType);

              foreach (var Index in ListCount.NumberSeries())
              {
                var ListItem = ReadRegister(ItemRegister);
                List.Add(ListItem);
              }
            }
          }
        }
        else if (Serial.IsHashSet)
        {
          var HashCount = Reader.ReadInt32();

          var DistinctListType = typeof(Inv.DistinctList<>).MakeGenericType(Serial.ItemType);

          var HashList = (Inv.IDistinctList)DistinctListType.ReflectionCreate(typeof(int), HashCount);

          if (HashCount > 0)
          {
            HashList.Capacity = HashCount;

            if (Serial.Primitive != null)
            {
              foreach (var Index in HashCount.NumberSeries())
              {
                var Item = Serial.Primitive.LoadFunction(this);
                HashList.Add(Item);
              }
            }
            else if (Serial.IsEnum)
            {
              foreach (var Index in HashCount.NumberSeries())
              {
                var Item = Reader.ReadInt32();
                HashList.Add(Item);
              }
            }
            else
            {
              var ItemRegister = Governor.GetRegister(Serial.ItemType);

              foreach (var Index in HashCount.NumberSeries())
              {
                var HashItem = ReadRegister(ItemRegister);
                HashList.Add(HashItem);
              }
            }
          }

          var HashType = typeof(IEnumerable<>).MakeGenericType(Serial.ItemType);
          DataValue = Serial.Type.ReflectionCreate(HashType, HashList);
        }
        else if (Serial.IsGrid)
        {
          var Width = Reader.ReadInt32();
          var Height = Reader.ReadInt32();

          var Grid = (Inv.IGrid)Serial.Type.ReflectionCreate();
          DataValue = Grid;
          Grid.Resize(Width, Height);

          var ItemRegister = Governor.GetRegister(Serial.ItemType);

          for (var Y = 0; Y < Grid.Height; Y++)
          {
            for (var X = 0; X < Grid.Width; X++)
            {
              var ItemIsNull = Reader.ReadBoolean();

              if (!ItemIsNull)
                Grid[X, Y] = ReadRegister(ItemRegister);
            }
          }
        }
        else if (Serial.IsArray)
        {
          var ArrayLength = Reader.ReadInt32();

          var ItemArray = System.Array.CreateInstance(Serial.ItemType, ArrayLength);
          DataValue = ItemArray;

          if (ArrayLength > 0)
          {
            var ItemList = new List<object>(ArrayLength);

            if (Serial.Primitive != null)
            {
              foreach (var Index in ArrayLength.NumberSeries())
              {
                var Item = Serial.Primitive.LoadFunction(this);
                ItemList.Add(Item);
              }
            }
            else if (Serial.IsEnum)
            {
              foreach (var Index in ArrayLength.NumberSeries())
              {
                var Ordinal = Reader.ReadInt32();
                var Item = Enum.ToObject(Serial.ItemType, Ordinal);
                ItemList.Add(Item);
              }
            }
            else
            {
              var ItemRegister = Governor.GetRegister(Serial.ItemType);

              foreach (var Index in ArrayLength.NumberSeries())
              {
                var Item = ReadRegister(ItemRegister);
                ItemList.Add(Item);
              }
            }

            ((System.Collections.IList)ItemList).CopyTo(ItemArray, 0);
          }
        }
        else if (Serial.IsEnumArray)
        {
          var EnumArray = (Inv.IEnumArray)Serial.Type.ReflectionCreate();
          DataValue = EnumArray;

          if (Serial.Primitive != null)
          {
            foreach (var Tuple in EnumArray.GetTuples())
            {
              var Item = Serial.Primitive.LoadFunction(this);
              EnumArray.SetValue(Tuple.Key, Item);
            }
          }
          else
          {
            var ItemRegister = Governor.GetRegister(Serial.ItemType);

            foreach (var Tuple in EnumArray.GetTuples())
            {
              var ItemIsNull = Reader.ReadBoolean();

              if (!ItemIsNull)
                EnumArray.SetValue(Tuple.Key, ReadRegister(ItemRegister));
            }
          }
        }
        else if (Serial.Primitive != null)
        {
          DataValue = Serial.Primitive.LoadFunction(this);
        }
        else if (Serial.IsEnum)
        {
          DataValue = Enum.ToObject(Serial.ItemType, Reader.ReadInt32());
        }
        else
        {
          var FieldRegister = Governor.GetRegister(Serial.ItemType);
          DataValue = ReadRegister(FieldRegister);
        }

        if (DataValue != null)
          Serial.SetAction(Record, DataValue);
      }
    }
    internal object ReadRegister(Register Register)
    {
      object DataValue;

      if (Register.IsCustom)
      {
        DataValue = Register.Custom.LoadFunction(this);
      }
      else
      {
        var Handle = ReadHandle(Register);

        if (!LookupDictionary.TryGetValue(Handle, out DataValue))
        {
          DataValue = Register.RegisterType.ReflectionCreate();
          LookupDictionary.Add(Handle, DataValue);

          ReadRecord(Register, DataValue);
        }
      }

      return DataValue;
    }
    internal uint ReadHandle(Register Register)
    {
      // TODO: optimise to byte -> ushort -> uint -> ulong?

      try
      {
        return Reader.ReadUInt32();
      }
      catch (Exception Exception)
      {
        // NOTE: this is here to improve the error message in case of an 'end of stream' bug.
        throw new Exception("Register '" + Register.RegisterType.Name + "': " + Exception.Message, Exception);
      }
    }
  }

  internal sealed class WriteException : Exception
  {
    public WriteException(string Message, Exception InnerException)
      : base(Message, InnerException)
    {
    }
  }

  internal static class Foundation
  {
    static Foundation()
    {
      PrimitiveCustomDictionary = new Dictionary<Type, Custom>();

      AddPrimitiveCustom<string>((S, R) => S.WriteString(R), (L) => L.ReadString());
      AddPrimitiveCustom<byte>((S, R) => S.WriteUInt8(R), (L) => L.ReadUInt8());
      AddPrimitiveCustom<ushort>((S, R) => S.WriteUInt16(R), (L) => L.ReadUInt16());
      AddPrimitiveCustom<int>((S, R) => S.WriteInt32(R), (L) => L.ReadInt32());
      AddPrimitiveCustom<long>((S, R) => S.WriteInt64(R), (L) => L.ReadInt64());
      AddPrimitiveCustom<bool>((S, R) => S.WriteBoolean(R), (L) => L.ReadBoolean());
      AddPrimitiveCustom<char>((S, R) => S.WriteCharacter(R), (L) => L.ReadCharacter());
      AddPrimitiveCustom<decimal>((S, R) => S.WriteDecimal(R), (L) => L.ReadDecimal());
      AddPrimitiveCustom<float>((S, R) => S.WriteFloat(R), (L) => L.ReadFloat());
      AddPrimitiveCustom<double>((S, R) => S.WriteDouble(R), (L) => L.ReadDouble());
      AddPrimitiveCustom<byte[]>((S, R) => S.WriteByteArray(R), (L) => L.ReadByteArray());
      AddPrimitiveCustom<DateTimeOffset>((S, R) => S.WriteDateTimeOffset(R), (L) => L.ReadDateTimeOffset());
      AddPrimitiveCustom<Inv.Binary>((S, R) => S.WriteBinary(R), (L) => L.ReadBinary());
      AddPrimitiveCustom<Inv.Colour>((S, R) => S.WriteColour(R), (L) => L.ReadColour());
      AddPrimitiveCustom<Inv.Image>((S, R) => S.WriteImage(R), (L) => L.ReadImage());
      AddPrimitiveCustom<Inv.Sound>((S, R) => S.WriteSound(R), (L) => L.ReadSound());
    }

    internal static readonly Dictionary<Type, Custom> PrimitiveCustomDictionary;

    internal static bool IsSubclassOfRawGeneric(this Type toCheck, Type generic)
    {
      while (toCheck != null && toCheck != typeof(object))
      {
        var cur = toCheck.GetReflectionInfo().IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;

        if (generic == cur)
          return true;

        toCheck = toCheck.GetReflectionInfo().BaseType;
      }

      return false;
    }
    internal static bool IsNullable(this Type Type)
    {
      if (!Type.GetReflectionInfo().IsValueType)
        return true; // ref-type

      if (Nullable.GetUnderlyingType(Type) != null)
        return true; // Nullable<T>

      return false; // value-type
    }

    private static void AddPrimitiveCustom<TPrimitive>(Action<Inv.CompactWriter, TPrimitive> SaveAction, Func<Inv.CompactReader, TPrimitive> LoadFunction)
    {
      var Custom = new Custom((S, R) => SaveAction(S.Writer, (TPrimitive)R), (L) => LoadFunction(L.Reader));

      PrimitiveCustomDictionary.Add(typeof(TPrimitive), Custom);
    }
  }
}