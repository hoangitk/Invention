﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  internal abstract class BezierPathBuilder<TOutput>
  {
    public abstract void MoveTo(BezierPoint Point);
    public abstract void AddLineTo(BezierPoint Point);
    public abstract void AddCurveTo(BezierPoint EndPoint, BezierPoint ControlPoint1, BezierPoint ControlPoint2);
    public abstract void Close();
    public abstract TOutput Render();

    public void AddRoundedRectMinusBorder(BezierSize Size, BezierCornerRadius CornerRadius, BezierBorderThickness BorderThickness)
    {
      // Inner shape
      AddRoundedRect(Size, CornerRadius, BorderThickness, false, true, false, true);
    }
    public void AddRoundedRectBorder(BezierSize Size, BezierCornerRadius CornerRadius, BezierBorderThickness BorderThickness)
    {
      // Border shape
      AddRoundedRect(Size, CornerRadius, BorderThickness, true, true, false, false);
      AddRoundedRect(Size, CornerRadius, BorderThickness, false, false, true, true);
    }
    public void AddRoundedRect(BezierSize Size, BezierCornerRadius CornerRadius, BezierBorderThickness BorderThickness)
    {
      // Inner shape + Border shape
      AddRoundedRect(Size, CornerRadius, BorderThickness, true, true, false, true);
    }

    private void AddRoundedRect(BezierSize Size, BezierCornerRadius CornerRadius, BezierBorderThickness BorderThickness, bool IsOuterBorder, bool IsClockwise, bool ContinuePath, bool ClosePath)
    {
      var Offset = IsOuterBorder ? BezierPoint.Empty : new BezierPoint(BorderThickness.Left, BorderThickness.Top);
      var Width = Size.Width - (IsOuterBorder ? 0 : BorderThickness.Left + BorderThickness.Right);
      var Height = Size.Height - (IsOuterBorder ? 0 : BorderThickness.Top + BorderThickness.Bottom);

      if (Width <= 0 || Height <= 0)
        return;

      var TopLeftStart = new BezierPoint(Offset.X, Offset.Y + CornerRadius.TopLeft);
      var TopLeftEnd = new BezierPoint(Offset.X + CornerRadius.TopLeft, Offset.Y);

      var TopRightStart = new BezierPoint(Offset.X + Width - CornerRadius.TopRight, Offset.Y);
      var TopRightEnd = new BezierPoint(Offset.X + Width, Offset.Y + CornerRadius.TopRight);

      var BottomRightStart = new BezierPoint(Offset.X + Width, Offset.Y + Height - CornerRadius.BottomRight);
      var BottomRightEnd = new BezierPoint(Offset.X + Width - CornerRadius.BottomRight, Offset.Y + Height);

      var BottomLeftStart = new BezierPoint(Offset.X + CornerRadius.BottomLeft, Offset.Y + Height);
      var BottomLeftEnd = new BezierPoint(Offset.X, Offset.Y + Height - CornerRadius.BottomLeft);

      // Adjust points for conflicting radii
      if (TopLeftStart.Y > BottomLeftEnd.Y)
      {
        var LeftY = Offset.Y + Height * CornerRadius.TopLeft / (CornerRadius.TopLeft + CornerRadius.BottomLeft);
        TopLeftStart.Y = LeftY;
        BottomLeftEnd.Y = LeftY;
      }

      if (TopLeftEnd.X > TopRightStart.X)
      {
        var TopX = Offset.X + Width * CornerRadius.TopLeft / (CornerRadius.TopLeft + CornerRadius.TopRight);
        TopLeftEnd.X = TopX;
        TopRightStart.X = TopX;
      }

      if (TopRightEnd.Y > BottomRightStart.Y)
      {
        var RightY = Offset.Y + Height * CornerRadius.TopRight / (CornerRadius.TopRight + CornerRadius.BottomRight);
        TopRightEnd.Y = RightY;
        BottomRightStart.Y = RightY;
      }

      if (BottomRightEnd.X < BottomLeftStart.X)
      {
        var BottomX = Offset.X + Width * CornerRadius.BottomLeft / (CornerRadius.BottomLeft + CornerRadius.BottomRight);
        BottomRightEnd.X = BottomX;
        BottomLeftStart.X = BottomX;
      }

      // Control points
      var K = (float)BezierCircleApproximationFactor;
      var TopLeftPoint1 = new BezierPoint(TopLeftStart.X, TopLeftStart.Y - (TopLeftStart.Y - TopLeftEnd.Y) * K);
      var TopLeftPoint2 = new BezierPoint(TopLeftEnd.X - (TopLeftEnd.X - TopLeftStart.X) * K, TopLeftEnd.Y);
      var TopRightPoint1 = new BezierPoint(TopRightStart.X + (TopRightEnd.X - TopRightStart.X) * K, TopRightStart.Y);
      var TopRightPoint2 = new BezierPoint(TopRightEnd.X, TopRightEnd.Y - (TopRightEnd.Y - TopRightStart.Y) * K);
      var BottomRightPoint1 = new BezierPoint(BottomRightStart.X, BottomRightStart.Y + (BottomRightEnd.Y - BottomRightStart.Y) * K);
      var BottomRightPoint2 = new BezierPoint(BottomRightEnd.X + (BottomRightStart.X - BottomRightEnd.X) * K, BottomRightEnd.Y);
      var BottomLeftPoint1 = new BezierPoint(BottomLeftStart.X - (BottomLeftStart.X - BottomLeftEnd.X) * K, BottomLeftStart.Y);
      var BottomLeftPoint2 = new BezierPoint(BottomLeftEnd.X, BottomLeftEnd.Y + (BottomLeftStart.Y - BottomLeftEnd.Y) * K);

      if (ContinuePath)
        AddLineTo(TopLeftStart);
      else
        MoveTo(TopLeftStart);

      if (IsClockwise)
      {
        if (!TopLeftStart.Equals(TopLeftEnd))
          AddCurveTo(TopLeftEnd, TopLeftPoint1, TopLeftPoint2);
        AddLineTo(TopRightStart);
        if (!TopRightStart.Equals(TopRightEnd))
          AddCurveTo(TopRightEnd, TopRightPoint1, TopRightPoint2);
        AddLineTo(BottomRightStart);
        if (!BottomRightStart.Equals(BottomRightEnd))
          AddCurveTo(BottomRightEnd, BottomRightPoint1, BottomRightPoint2);
        AddLineTo(BottomLeftStart);
        if (!BottomLeftStart.Equals(BottomLeftEnd))
          AddCurveTo(BottomLeftEnd, BottomLeftPoint1, BottomLeftPoint2);
        AddLineTo(TopLeftStart);
      }
      else
      {
        AddLineTo(BottomLeftEnd);
        if (!BottomLeftEnd.Equals(BottomLeftStart))
          AddCurveTo(BottomLeftStart, BottomLeftPoint2, BottomLeftPoint1);
        AddLineTo(BottomRightEnd);
        if (!BottomRightEnd.Equals(BottomRightStart))
          AddCurveTo(BottomRightStart, BottomRightPoint2, BottomRightPoint1);
        AddLineTo(TopRightEnd);
        if (!TopRightEnd.Equals(TopRightStart))
          AddCurveTo(TopRightStart, TopRightPoint2, TopRightPoint1);
        AddLineTo(TopLeftEnd);
        if (!TopLeftEnd.Equals(TopLeftStart))
          AddCurveTo(TopLeftStart, TopLeftPoint2, TopLeftPoint1);
      }

      if (ClosePath)
        Close();
    }

    private const double BezierCircleApproximationFactor = 0.551915024494; // bezier circle approximation constant via http://spencermortensen.com/articles/bezier-circle/
  }

  public struct BezierPoint : IEquatable<BezierPoint>
  {
    public BezierPoint(float X, float Y)
      : this()
    {
      this.X = X;
      this.Y = Y;
    }

    public float X { get; set; }
    public float Y { get; set; }

    public static BezierPoint Empty
    {
      get { return new BezierPoint(0, 0); }
    }

    public bool Equals(BezierPoint Other)
    {
      return X == Other.X && Y == Other.Y;
    }
    public override bool Equals(object Other)
    {
      if (Other is BezierPoint)
        return Equals((BezierPoint)Other);

      return false;
    }
    public override int GetHashCode()
    {
      return Tuple.Create(X, Y).GetHashCode();
    }
  }

  public struct BezierSize
  {
    public BezierSize(float Width, float Height)
      : this()
    {
      this.Width = Width;
      this.Height = Height;
    }

    public float Width { get; set; }
    public float Height { get; set; }
  }

  public struct BezierCornerRadius
  {
    public BezierCornerRadius(float TopLeft, float TopRight, float BottomRight, float BottomLeft)
      : this()
    {
      this.TopLeft = TopLeft;
      this.TopRight = TopRight;
      this.BottomRight = BottomRight;
      this.BottomLeft = BottomLeft;
    }

    public float TopLeft { get; set; }
    public float TopRight { get; set; }
    public float BottomRight { get; set; }
    public float BottomLeft { get; set; }
  }

  public struct BezierBorderThickness
  {
    public BezierBorderThickness(float Left, float Top, float Right, float Bottom)
      : this()
    {
      this.Left = Left;
      this.Top = Top;
      this.Right = Right;
      this.Bottom = Bottom;
    }

    public float Left { get; set; }
    public float Top { get; set; }
    public float Right { get; set; }
    public float Bottom { get; set; }
  }
}
