﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Inv
{
  public static class Luhn
  {
    public static bool IsValid(string CardNumber)
    {
      var DigitArray = CardNumber.Select(c => c - '0').ToArray();

      if (DigitArray.Any(D => D < 0 || D > 9))
        return false;

      var Index = 0;
      var LengthMod = DigitArray.Length % 2;

      var Result = DigitArray.Sum(d => Index++ % 2 == LengthMod ? results[d] : d) % 10;

      return Result == 0;
    }
    public static bool IsDinersOrAmex(string CardNumber)
    {
      // Minimum valid card is 7 (6 digit IIN plus 1 check digit). 
      if (CardNumber.Length < 7)
        return false;

      // DINNERS CLUB start with 300 through 305, 36 or 38.
      // AMEX start with 34 or 37.
      return Regex.IsMatch(CardNumber, "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$") || Regex.IsMatch(CardNumber, "^3[47][0-9]{5,}$");
    }

    private static readonly int[] results = { 0, 2, 4, 6, 8, 1, 3, 5, 7, 9 };
  }
}
