﻿using System;
using Android.App;
using Android.Runtime;
using Inv.Support;

namespace InvTest
{
  [Activity(Label = "Invention Test", Theme = "@android:style/Theme.NoTitleBar", MainLauncher = true, NoHistory = true)]
  public sealed class SplashActivity : Inv.AndroidSplashActivity
  {
    protected override void Install()
    {
      this.ImageResourceId = InvTest.Resource.Drawable.splash;
      this.LaunchActivity = typeof(MainActivity);
    }
  }
  [Activity(
    Label = "Invention Test", 
    MainLauncher = false,
    Icon = "@drawable/icon", 
    ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
  public sealed class MainActivity : Inv.AndroidActivity
  {
    protected override void Install(Inv.Application Application)
    {
      InvTest.Shell.Install(Application);

      // for debugging GREFs.
      var c = JNIEnv.FindClass("android/os/Debug");
      var m = JNIEnv.GetStaticMethodID(c, "dumpReferenceTables", "()V");
      InvTest.Shell.DumpEvent = () =>
      {
        GC.Collect(GC.MaxGeneration);

        JNIEnv.CallStaticVoidMethod(c, m);
      };
      InvTest.Shell.InstrumentationClearEvent = () => Inv.AndroidInstrumentation.Clear();
      InvTest.Shell.InstrumentationPrintEvent = () => Inv.AndroidInstrumentation.Print();
      /*
      var Surface = Application.Window.NewSurface();

      var Frame = Surface.NewFrame();
      Surface.Content = Frame;

      var AndroidFrame = Bridge.GetFrame(Frame);

      var ProgressBar = new Android.Widget.ProgressBar(this);
      AndroidFrame.SetContentElement(ProgressBar);
      ProgressBar.Max = 100;
      ProgressBar.SetProgress(50, true);
      ProgressBar.LayoutParameters = new Inv.AndroidFrame.LayoutParams(500, 200);

      Application.Window.Transition(Surface);
      */
    }
  }

#if DEBUG
  [Activity(Label = "InvAndroidTestNative", MainLauncher = false, Icon = "@drawable/icon", ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
  public sealed class NativeActivity : Inv.AndroidDebugNativeActivity
  {  
  }

  [Activity(Label = "InvAndroidTestOpenGL", MainLauncher = false, Icon = "@drawable/icon", ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
  public abstract class OpenGLActivity : Inv.AndroidDebugOpenGLActivity
  {
  }

  [Activity(Label = "InvAndroidTestRender", MainLauncher = false, Icon = "@drawable/icon", ConfigurationChanges = Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]
  public sealed class RenderActivity : Inv.AndroidDebugRenderActivity
  {
  }
#endif
}