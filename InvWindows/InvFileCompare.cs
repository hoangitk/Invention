﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using Inv.Support;

namespace Inv
{
  public static class FileCompareHelper
  {
    public static void OpenWinMergeDownloadPage()
    {
      Process.Start("http://www.kestral.com.au/winmerge");
    }
  }

  public sealed class FileCompare
  {
    public FileCompare()
    {
      this.ParameterFormat = "\"$S\" \"$T\"";

      try
      {
        if (File.Exists(BeyondComparePath))
          this.ExecutablePath = BeyondComparePath;
        else if (File.Exists(WinMergePath))
          this.ExecutablePath = WinMergePath;
      }
      catch
      {
        // ignore file access issues.
      }
    }

    public event Action ComparisonClosedEvent;
    public event Action ComparisonExecutableMissingEvent;

    public FileCompareProcess Compare(string SourceFilePath, string TargetFilePath)
    {
      if (string.IsNullOrWhiteSpace(ExecutablePath) || !File.Exists(ExecutablePath))
      {
        if (ComparisonClosedEvent != null)
          ComparisonClosedEvent();

        if (ComparisonExecutableMissingEvent != null)
          ComparisonExecutableMissingEvent();
        else
          throw new Exception("No comparison tool could be detected");

        return new FileCompareProcess(null);
      }
      else
      {
        var Execution = Process.Start(ExecutablePath, ProduceParameters(SourceFilePath, TargetFilePath));
        Execution.EnableRaisingEvents = true;
        Execution.Exited += (Sender, E) =>
        {
          if (ComparisonClosedEvent != null)
            ComparisonClosedEvent();
        };
        return new FileCompareProcess(Execution);
      }
    }

    private string ProduceParameters(string SourceFilePath, string TargetFilePath)
    {
      if (ParameterFormat.NullAsEmpty().IsWhitespace())
        throw new Exception("ParameterFormat is empty");

      return ParameterFormat.Replace("$S", SourceFilePath).Replace("$T", TargetFilePath);
    }

    private readonly string ExecutablePath;
    private readonly string ParameterFormat;

    private const string WinMergePath = @"C:\Program Files (x86)\WinMerge\WinMergeU.exe";
    private const string BeyondComparePath = @"C:\Program Files\Beyond Compare 4\BComp.exe";
  }

  public sealed class FileCompareProcess
  {
    public FileCompareProcess(Process Process)
    {
      this.Process = Process;
    }

    public void Wait()
    {
      Process?.WaitForExit();
    }

    private readonly Process Process;
  }

  public sealed class StringCompare
  {
    public StringCompare()
    {
      this.FileCompare = new FileCompare();
    }

    public event Action ComparisonExecutableMissingEvent
    {
      add { FileCompare.ComparisonExecutableMissingEvent += value; }
      remove { FileCompare.ComparisonExecutableMissingEvent -= value; }
    }
    
    public void Compare(string SourceString, string TargetString, string SourceDescription = null, string TargetDescription = null)
    {
      var SourceTempPath = Path.GetTempFileName();
      var TargetTempPath = Path.GetTempFileName();

      var Directory = Path.GetDirectoryName(SourceTempPath);
      var Filename = Path.GetFileNameWithoutExtension(SourceTempPath);
      var Extra = SourceDescription == null ? "" : "-" + SourceDescription;

      var SourceFilePath = Path.Combine(Directory, Filename + Extra + Path.GetExtension(SourceTempPath));

      Directory = Path.GetDirectoryName(TargetTempPath);
      Filename = Path.GetFileNameWithoutExtension(TargetTempPath);
      Extra = TargetDescription == null ? "" : "-" + TargetDescription;

      var TargetFilePath = Path.Combine(Directory, Filename + Extra + Path.GetExtension(TargetTempPath));
      
      File.Delete(SourceTempPath);
      File.Delete(TargetTempPath);

      System.IO.File.WriteAllText(SourceFilePath, SourceString);
      System.IO.File.WriteAllText(TargetFilePath, TargetString);

      FileCompare.ComparisonClosedEvent += () =>
      {
        File.Delete(SourceFilePath);
        File.Delete(TargetFilePath);
      };

      FileCompare.Compare(SourceFilePath, TargetFilePath);
    }

    private FileCompare FileCompare;
  }
}