﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace InvTest
{
  public sealed class NavigationConsole
  {
    public NavigationConsole(Inv.Application Base, NavigationTheme Theme)
    {
      this.Base = Base;
      this.Theme = Theme;

      Base.Window.Background.Colour = Inv.Colour.Black;
    }

    public NavigationTheme Theme { get; private set; }
    public string Title
    {
      get { return Base.Title; }
      set { Base.Title = value; }
    }
    public Inv.Directory Directory
    {
      get { return Base.Directory; }
    }
    public Inv.Audio Audio
    {
      get { return Base.Audio; }
    }
    public Inv.Graphics Graphics
    {
      get { return Base.Graphics; }
    }
    public Inv.Location Location
    {
      get { return Base.Location; }
    }
    public Inv.Process Process
    {
      get { return Base.Process; }
    }
    public Inv.Window Window
    {
      get { return Base.Window; }
    }
    public event Action StartEvent
    {
      add { Base.StartEvent += value; }
      remove { Base.StartEvent -= value; }
    }
    public event Action StopEvent
    {
      add { Base.StopEvent += value; }
      remove { Base.StopEvent -= value; }
    }
    public event Action SuspendEvent
    {
      add { Base.SuspendEvent += value; }
      remove { Base.SuspendEvent -= value; }
    }
    public event Action ResumeEvent
    {
      add { Base.ResumeEvent += value; }
      remove { Base.ResumeEvent -= value; }
    }
    public event Action<Exception> HandleExceptionEvent
    {
      add { Base.HandleExceptionEvent += value; }
      remove { Base.HandleExceptionEvent -= value; }
    }
    public event Func<bool> ExitQuery
    {
      add { Base.ExitQuery += value; }
      remove { Base.ExitQuery -= value; }
    }

    public void Exit()
    {
      Base.Exit();
    }

    public NavigationSurface NewNavigationSurface()
    {
      return new NavigationSurface(this);
    }

    internal Inv.Application Base { get; private set; }

    public static implicit operator Inv.Application(NavigationConsole Application)
    {
      return Application?.Base;
    }
  }

  public sealed class NavigationTheme
  {
    public NavigationTheme(Inv.Colour Colour)
    {
      this.Colour = Colour;
    }

    public Inv.Colour Colour { get; private set; }
  }

  public sealed class NavigationSurface : Inv.Mimic<Inv.Surface>
  {
    internal NavigationSurface(NavigationConsole Application)
    {
      this.Application = Application;
      this.Base = Application.Window.NewSurface();
      Base.KeystrokeEvent += (Keystroke) =>
      {
        if (Keystroke.Key == Inv.Key.Escape)
          MenuScrim.Dismiss();
      };

      var MainOverlay = Base.NewOverlay();
      Base.Content = MainOverlay;

      var MainDock = Base.NewHorizontalDock();
      MainOverlay.AddPanel(MainDock);
      MainDock.Background.Colour = Inv.Colour.WhiteSmoke;

      var DrawerDock = Base.NewVerticalDock();
      DrawerDock.Alignment.StretchLeft();

      this.TitleLabel = Base.NewLabel();
      DrawerDock.AddHeader(TitleLabel);
      TitleLabel.Padding.Set(16, 0, 16, 0);
      TitleLabel.Background.Colour = Inv.Colour.WhiteSmoke;
      TitleLabel.Size.SetHeight(56);
      TitleLabel.Font.Medium();
      TitleLabel.Font.Size = 28;
      TitleLabel.Font.Colour = Inv.Colour.DimGray;

      var TitleDivider = Base.NewFrame();
      DrawerDock.AddHeader(TitleDivider);
      TitleDivider.Size.SetHeight(1);
      TitleDivider.Background.Colour = Inv.Colour.LightGray;

      this.Drawer = new NavigationDrawer(Application, Base);
      DrawerDock.AddClient(Drawer);

      var DrawerDivider = Base.NewFrame();
      DrawerDivider.Size.SetWidth(1);
      DrawerDivider.Background.Colour = Inv.Colour.LightGray;

      var ClientOverlay = Base.NewOverlay();
      MainDock.AddClient(ClientOverlay);

      this.ClientFrame = Base.NewFrame();
      ClientOverlay.AddPanel(ClientFrame);
      ClientFrame.Margin.Set(0, 56, 0, 0);

      // toolbar is 'on top' of client frame.
      this.Toolbar = new NavigationToolbar(Application, Base);
      ClientOverlay.AddPanel(Toolbar);
      Toolbar.Alignment.TopStretch();

      this.MenuScrim = new NavigationScrim(Base);
      MainOverlay.AddPanel(MenuScrim);
      MenuScrim.DismissedEvent += () =>
      {
        MenuScrim.Transition(null).CarouselNext();
      };

      this.MenuIcon = Toolbar.AddLeadingIcon();
      MenuIcon.Hint = "Main Menu";
      MenuIcon.Image = Resources.Images.ViewHeadlineWhite;
      MenuIcon.SingleTapEvent += () =>
      {
        if (!IsWide())
        {
          MenuScrim.Show();
          MenuScrim.Transition(DrawerDock).CarouselPrevious();
        }
      };

      Base.ArrangeEvent += () =>
      {
        var ArrangeWide = IsWide();

        DrawerDock.Size.SetWidth(Math.Min(300, Application.Window.Width - 64));

        MenuIcon.Visibility.Set(!ArrangeWide);
        MenuIcon.Background.Colour = Toolbar.BackgroundColour;

        MainDock.RemoveHeaders();

        if (ArrangeWide)
        {
          MenuScrim.Dismiss();

          MainDock.AddHeader(DrawerDock);
          MainDock.AddHeader(DrawerDivider);
        }

        Toolbar.Padding.Set(ArrangeWide ? 16 : 0, 0, ArrangeWide ? 16 : 0, 0);
      };
    }

    public string Title
    {
      get { return TitleLabel.Text; }
      set { TitleLabel.Text = value; }
    }
    public NavigationToolbar Toolbar { get; private set; }
    public NavigationDrawer Drawer { get; private set; }
    public event Action ArrangeEvent
    {
      add { Base.ArrangeEvent += value; }
      remove { Base.ArrangeEvent -= value; }
    }
    public event Action ComposeEvent
    {
      add { Base.ComposeEvent += value; }
      remove { Base.ComposeEvent -= value; }
    }
    public event Action GestureBackwardEvent
    {
      add { Base.GestureBackwardEvent += value; }
      remove { Base.GestureBackwardEvent -= value; }
    }
    public event Action GestureForwardEvent
    {
      add { Base.GestureForwardEvent += value; }
      remove { Base.GestureForwardEvent -= value; }
    }
    public event Action<Inv.Keystroke> KeystrokeEvent
    {
      add { Base.KeystrokeEvent += value; }
      remove { Base.KeystrokeEvent -= value; }
    }

    public void Rearrange()
    {
      Base.Rearrange();
    }
    public void GestureBackward()
    {
      Base.GestureBackward();
    }
    public void GestureForward()
    {
      Base.GestureForward();
    }
    public Inv.Transition TransitionClient(Inv.Panel Panel)
    {
      MenuScrim.Dismiss();

      return ClientFrame.Transition(Panel);
    }

    private bool IsWide()
    {
      return Application.Window.Width > 1000;
    }

    private readonly NavigationConsole Application;
    private readonly NavigationButton MenuIcon;
    private readonly Inv.Label TitleLabel;
    private readonly Inv.Frame ClientFrame;
    private readonly NavigationScrim MenuScrim;
  }

  public sealed class NavigationToolbar : Inv.Panel<Inv.Dock>
  {
    public NavigationToolbar(NavigationConsole Application, Inv.Surface Surface)
    {
      this.Surface = Surface;

      this.Base = Surface.NewHorizontalDock();
      Base.Background.Colour = Application.Theme.Colour;
      Base.Size.SetHeight(56);
      Base.Elevation.Set(2);

      this.LeadingStack = Surface.NewHorizontalStack();
      Base.AddHeader(LeadingStack);

      this.TitleLabel = Surface.NewLabel();
      Base.AddClient(TitleLabel);
      TitleLabel.Font.Colour = Inv.Colour.White;
      TitleLabel.Font.Size = 20;
      TitleLabel.Font.Medium();
      TitleLabel.Alignment.CenterLeft();

      this.TrailingStack = Surface.NewHorizontalStack();
      Base.AddFooter(TrailingStack);

      this.IconButtonList = new Inv.DistinctList<NavigationButton>();
    }

    public string Title
    {
      get { return TitleLabel.Text; }
      set { TitleLabel.Text = value; }
    }
    public Inv.Colour BackgroundColour
    {
      get { return Base.Background.Colour; }
      set
      {
        Base.Background.Colour = value;
        foreach (var IconButton in IconButtonList)
          IconButton.Background.Colour = value;
      }
    }
    public Inv.Colour ForegroundColour
    {
      get { return TitleLabel.Font.Colour; }
      set { TitleLabel.Font.Colour = value; }
    }

    public NavigationButton AddLeadingIcon(Inv.Image Image = null)
    {
      var Result = new NavigationButton(Surface);
      Result.Image = Image;
      Result.Background.Colour = BackgroundColour;

      LeadingStack.AddPanel(Result);

      return Result;
    }
    public NavigationButton AddTrailingIcon(Inv.Image Image = null)
    {
      var Result = new NavigationButton(Surface);
      Result.Image = Image;
      Result.Background.Colour = BackgroundColour;

      TrailingStack.AddPanel(Result);

      return Result;
    }

    internal Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    internal Inv.Padding Padding
    {
      get { return Base.Padding; }
    }

    private readonly Inv.Surface Surface;
    private readonly Inv.Label TitleLabel;
    private readonly Inv.Stack LeadingStack;
    private readonly Inv.Stack TrailingStack;
    private readonly Inv.DistinctList<NavigationButton> IconButtonList;
  }
    
  public sealed class NavigationButton : Inv.Panel<Inv.Button>
  {
    public NavigationButton(Inv.Surface Surface)
    {
      this.Base = Surface.NewFlatButton();
      Base.Size.Set(56, 56);
      Base.Margin.Set(0, 0, 10, 0);

      this.Graphic = Surface.NewGraphic();
      Base.Content = Graphic;
      Graphic.Size.Set(32, 32);
      Graphic.Alignment.Center();
    }

    public Inv.Image Image
    {
      get => Graphic.Image;
      set => Graphic.Image = value;
    }
    public event Action SingleTapEvent
    {
      add => Base.SingleTapEvent += value;
      remove => Base.SingleTapEvent -= value;
    }
    public string Hint
    {
      get => Base.Hint;
      set => Base.Hint = value;
    }

    internal Inv.Alignment Alignment => Base.Alignment;
    internal Inv.Margin Margin => Base.Margin;
    internal Inv.Background Background => Base.Background;
    internal Inv.Visibility Visibility => Base.Visibility;

    private readonly Inv.Graphic Graphic;
  }

  public sealed class NavigationScrim : Inv.Panel<Inv.Overlay>
  {
    public NavigationScrim(Inv.Surface Surface)
    {
      this.Base = Surface.NewOverlay();

      this.Shade = Surface.NewStarkButton();
      Base.AddPanel(Shade);
      Shade.Visibility.Collapse();
      Shade.Background.Colour = Inv.Colour.Black.Opacity(1.0F / 3.0F);
      Shade.SingleTapEvent += () => Dismiss();

      this.Frame = Surface.NewFrame();
      Base.AddPanel(Frame);

      this.FadeInAnimation = Surface.NewAnimation();
      FadeInAnimation.AddTarget(Shade).FadeOpacityIn(Surface.Window.DefaultTransitionDuration);
      FadeInAnimation.CommenceEvent += () =>
      {
        if (ShowEvent != null)
          ShowEvent();

        Shade.Visibility.Show();
      };
      FadeInAnimation.CompleteEvent += () =>
      {
        if (ShowedEvent != null)
          ShowedEvent();
      };

      this.FadeOutAnimation = Surface.NewAnimation();
      FadeOutAnimation.AddTarget(Shade).FadeOpacityOut(Surface.Window.DefaultTransitionDuration);
      FadeOutAnimation.CommenceEvent += () =>
      {
        if (DismissEvent != null)
          DismissEvent();
      };
      FadeOutAnimation.CompleteEvent += () =>
      {
        Shade.Visibility.Collapse();

        if (DismissedEvent != null)
          DismissedEvent();
      };
    }

    public event Action ShowEvent;
    public event Action ShowedEvent;
    public event Action DismissEvent;
    public event Action DismissedEvent;

    public void Show()
    {
      FadeOutAnimation.Stop();

      if (!Shade.Visibility.Get() && !FadeInAnimation.IsActive)
        FadeInAnimation.Start();
    }
    public void Dismiss()
    {
      FadeInAnimation.Stop();

      if (Shade.Visibility.Get() && !FadeOutAnimation.IsActive)
        FadeOutAnimation.Start();
    }

    public Inv.Transition Transition(Inv.Panel Panel)
    {
      return Frame.Transition(Panel);
    }

    private readonly Inv.Button Shade;
    private readonly Inv.Frame Frame;
    private readonly Inv.Animation FadeInAnimation;
    private readonly Inv.Animation FadeOutAnimation;
  }
     
  public sealed class NavigationDrawer : Inv.Panel<Inv.Flow>
  {
    public NavigationDrawer(NavigationConsole Application, Inv.Surface Surface)
    {
      this.Application = Application;
      this.Surface = Surface;

      this.Base = Surface.NewFlow();
      Base.Padding.Set(0, 8, 0, 8);
      Base.Background.Colour = Inv.Colour.WhiteSmoke;

      this.Section = Base.AddSection();
      Section.ItemQuery += (Item) => ItemList[Item];

      this.ItemList = new Inv.DistinctList<NavigationDrawerItem>();
    }

    internal Inv.Surface Surface { get; private set; }

    public NavigationDrawerItem AddItem()
    {
      var Result = new NavigationDrawerItem(Application, Surface);
      ItemList.Add(Result);
      Result.BackgroundColour = Base.Background.Colour;

      Section.SetItemCount(ItemList.Count);

      return Result;
    }
    public NavigationDrawerItem AddItem(string Caption)
    {
      var Result = AddItem();
      Result.Caption = Caption;
      return Result;
    }
    public void ScrollItem(NavigationDrawerItem Item)
    {
      Section.ScrollToItemAtIndex(ItemList.IndexOf(Item));
    }

    private readonly NavigationConsole Application;
    private readonly Inv.DistinctList<NavigationDrawerItem> ItemList;
    private readonly Inv.FlowSection Section;
  }

  public sealed class NavigationDrawerItem : Inv.Panel<Inv.Button>
  {
    internal NavigationDrawerItem(NavigationConsole Application, Inv.Surface Surface)
    {
      this.Application = Application;

      this.Base = Surface.NewFlatButton();
      Base.Padding.Set(16, 0, 16, 0);
      Base.Size.SetHeight(48);
      Base.IsFocusable = false;

      this.CaptionLabel = Surface.NewLabel();
      Base.Content = CaptionLabel;
      CaptionLabel.Font.Regular();
      CaptionLabel.Font.Size = 16;
      CaptionLabel.Font.Colour = Inv.Colour.Black;
      CaptionLabel.Margin.Set(16, 0, 0, 0);
    }

    public string Caption
    {
      get { return CaptionLabel.Text; }
      set { CaptionLabel.Text = value; }
    }
    public bool IsHighlighted
    {
      get { return IsHighlightedField; }
      set
      {
        if (IsHighlightedField != value)
        {
          this.IsHighlightedField = value;

          if (IsHighlightedField)
          {
            CaptionLabel.Font.Colour = Application.Theme.Colour;
            CaptionLabel.Font.Medium();
          }
          else
          {
            CaptionLabel.Font.Colour = Inv.Colour.Black;
            CaptionLabel.Font.Regular();
          }
        }
      }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }

    public void SingleTap()
    {
      Base.SingleTap();
    }

    internal Inv.Colour BackgroundColour
    {
      get { return Base.Background.Colour; }
      set { Base.Background.Colour = value ?? Inv.Colour.WhiteSmoke; }
    }

    private readonly NavigationConsole Application;
    private readonly Inv.Label CaptionLabel;
    private bool IsHighlightedField;
  }
}