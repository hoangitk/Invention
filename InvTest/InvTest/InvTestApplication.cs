﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using Inv.Support;

namespace InvTest
{
  public static class Shell
  {
    public static void Install(Inv.Application Application)
    {
      InstrumentationClearEvent = () => { };
      InstrumentationPrintEvent = () => { };
      DumpEvent = () => { };
      new TestConsole(Application);
    }

    public static Action DumpEvent;
    public static Action InstrumentationClearEvent;
    public static Action InstrumentationPrintEvent;
  }

  public sealed class TestConsole : Inv.Mimic<NavigationConsole>
  {
    internal TestConsole(Inv.Application Application)
    {
      this.LogoImage = Resources.Images.PhoenixLogo960x540;

      this.BookmarkFile = Application.Directory.RootFolder.NewFile("Bookmark.txt");

      this.Base = new NavigationConsole(Application, new NavigationTheme(Inv.Colour.HotPink));
      Base.Title = "Invention Test";
      Base.HandleExceptionEvent += (Exception) =>
      {
        Debug.WriteLine(Exception.Describe());
        Debug.WriteLine(Exception.StackTrace);
      };
      Base.StartEvent += () =>
      {
        Compile(AnimationsRotate, LoadBookmark(), IsDirect: false);

#if DEBUG
        //throw new Exception("Start fail test.");
#endif
      };

      this.CloseSurface = Base.Window.NewSurface();

      Base.ExitQuery += () =>
      {
        ExitCheck();

        return false;
      };
    }

    private Inv.Panel About(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.DimGray.Darken(0.50F);

      var Dock = Surface.NewVerticalDock();
      Overlay.AddPanel(Dock);
      Dock.Alignment.Center();

      var ButtonDock = Surface.NewHorizontalDock();
      Dock.AddFooter(ButtonDock);

      var Scroll = Surface.NewVerticalScroll();
      Dock.AddClient(Scroll);
      Scroll.Alignment.Center();

      var Stack = Surface.NewVerticalStack();
      Scroll.Content = Stack;

      var TitleLabel = Surface.NewLabel();
      Dock.AddHeader(TitleLabel);
      TitleLabel.Padding.Set(16);
      TitleLabel.Font.Colour = Inv.Colour.White;
      TitleLabel.Font.Light();
      TitleLabel.Font.ExtraMassive();
      TitleLabel.Text = "About!";

      var EmailButton = new AboutButton(Surface);
      Stack.AddPanel(EmailButton);

      EmailButton.LogoImage = Resources.Images.Water128x128;
      EmailButton.TitleText = "Send me an email";
      EmailButton.ActionText = "hodgskin.callan@gmail.com";
      EmailButton.SingleTapEvent += () =>
      {
        var EmailMessage = Surface.Window.Application.Email.NewMessage();
        EmailMessage.To("Callan Hodgskin", "hodgskin.callan@gmail.com");
        EmailMessage.Subject = "Testing the Subject";
        EmailMessage.Body = "All my body";
        var AttachmentFile = Surface.Window.Application.Directory.RootFolder.NewFile("Attachment.txt");
        AttachmentFile.AsText().WriteAll("Attachment file okay.");
        EmailMessage.Attach("Attachment.txt", AttachmentFile);
        var Result = EmailMessage.Send();

        if (Result)
          EmailButton.Colour = Inv.Colour.Green;
        else
          EmailButton.Colour = Inv.Colour.Red;
      };

      if (Surface.Window.Width > 380)
        Scroll.Size.SetWidth(380);
      else
        Scroll.Size.AutoWidth();

      return Overlay;
    }
    private Inv.Panel Accessibility(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.DimGray.Darken(0.50F);

      var Stack = Surface.NewVerticalStack();
      Overlay.AddPanel(Stack);
      Stack.Alignment.Center();

      var FocusButton = Surface.NewFlatButton();
      Stack.AddPanel(FocusButton);
      FocusButton.Background.Colour = Inv.Colour.DarkRed;
      FocusButton.Padding.Set(50);
      FocusButton.Hint = "Focus";

      var AnnounceEdit = Surface.NewTextEdit();
      Stack.AddPanel(AnnounceEdit);
      AnnounceEdit.Background.Colour = Inv.Colour.White;
      AnnounceEdit.Padding.Set(20);
      AnnounceEdit.Font.Size = 20;
      AnnounceEdit.Text = "Hello World";
      
      var AnnounceButton = Surface.NewFlatButton();
      Stack.AddPanel(AnnounceButton);
      AnnounceButton.Background.Colour = Inv.Colour.DarkGreen;
      AnnounceButton.Padding.Set(50);
      AnnounceButton.Hint = "Announce";
      AnnounceButton.SingleTapEvent += () =>
      {
        Surface.Window.RunTask(Thread =>
        {
          Thread.Sleep(TimeSpan.FromMilliseconds(1000));
          Thread.Post(() => Surface.Window.Accessibility.Announce(AnnounceEdit.Text));
          Thread.Sleep(TimeSpan.FromMilliseconds(1000));
          Thread.Post(() => Surface.Window.Accessibility.SetFocus(FocusButton));
        });
      };

      return Overlay;
    }
    private Inv.Panel ActionFlyout(Inv.Surface Surface)
    {
      var Flyout = Surface.NewOverlay();

      var ActionPanel = new ActionPanel(Surface);
      Flyout.AddPanel(ActionPanel);
      ActionPanel.Alignment.BottomStretch();

      for (var Index = 0; Index < 5; Index++)
      {
        var LeftButton = ActionPanel.AddHeaderButton();
        LeftButton.Caption = "LEFTY" + Index;
      }

      for (var Index = 0; Index < 5; Index++)
      {
        var RightButton = ActionPanel.AddFooterButton();
        RightButton.Caption = "RIGHTY" + Index;
      }

      Flyout.AdjustEvent += () =>
      {
        var Dimension = Flyout.GetDimension();
        ActionPanel.Compose(Dimension.Width, Dimension.Height);
      };

      return Flyout;
    }
    private Inv.Panel AnimationsCombination(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.Black;

      var StartButton = Surface.NewFlatButton();
      Overlay.AddPanel(StartButton);
      StartButton.Alignment.BottomLeft();
      StartButton.Background.Colour = Inv.Colour.DarkGreen;
      StartButton.Padding.Set(20);

      var StartLabel = Surface.NewLabel();
      StartButton.Content = StartLabel;
      StartLabel.Font.Size = 20;
      StartLabel.Justify.Center();
      StartLabel.Font.Colour = Inv.Colour.White;
      StartLabel.Text = "START\nANIMATION";

      var StopButton = Surface.NewFlatButton();
      Overlay.AddPanel(StopButton);
      StopButton.Alignment.BottomRight();
      StopButton.Background.Colour = Inv.Colour.DarkRed;
      StopButton.Padding.Set(20);

      var StopLabel = Surface.NewLabel();
      StopButton.Content = StopLabel;
      StopLabel.Font.Size = 20;
      StopLabel.Font.Colour = Inv.Colour.White;
      StopLabel.Justify.Center();
      StopLabel.Text = "STOP\nANIMATION";

      var RestartButton = Surface.NewFlatButton();
      Overlay.AddPanel(RestartButton);
      RestartButton.Alignment.BottomCenter();
      RestartButton.Background.Colour = Inv.Colour.Purple;
      RestartButton.Padding.Set(20);

      var RestartLabel = Surface.NewLabel();
      RestartButton.Content = RestartLabel;
      RestartLabel.Font.Size = 20;
      RestartLabel.Font.Colour = Inv.Colour.White;
      RestartLabel.Justify.Center();
      RestartLabel.Text = "RESTART\nANIMATION";

      var TargetButton = Surface.NewFlatButton();
      Overlay.AddPanel(TargetButton);
      TargetButton.Alignment.Center();
      TargetButton.Background.Colour = Inv.Colour.LightBlue;
      TargetButton.Padding.Set(20);

      var TargetLabel = Surface.NewLabel();
      TargetButton.Content = TargetLabel;
      TargetLabel.Font.Size = 60;
      TargetLabel.Justify.Center();
      TargetLabel.Text = "ANIMATION TARGET";

      var Animation = Surface.NewAnimation();

      StartButton.SingleTapEvent += () =>
      {
        Animation.RemoveTargets();

        var Duration = TimeSpan.FromSeconds(1);

        var Target = Animation.AddTarget(TargetButton);
        Target.FadeOpacity(0.0F, 1.0F, Duration);
        Target.RotateAngle(0.0F, 360.0F, Duration);
        Target.TranslatePosition(-50, +50, Duration);
        Target.ScaleSize(0.50F, 1.00F, Duration);

        Animation.Start();
      };

      StopButton.SingleTapEvent += () =>
      {
        Animation.Stop();
      };

      RestartButton.SingleTapEvent += () =>
      {
        StopButton.SingleTap();
        StartButton.SingleTap();
      };

      TargetButton.SingleTapEvent += () =>
      {
        if (Animation.IsActive)
        {
          Animation.Stop();
        }
        else
        {
          Animation.RemoveTargets();
          var Target = Animation.AddTarget(TargetButton);
          var Opacity = TargetLabel.Opacity.Get();

          if (Opacity < 1.0F)
            Target.FadeOpacity(Opacity, 1.0F, TimeSpan.FromSeconds(1.0F * (1.0F - Opacity)));
          else
            Target.FadeOpacity(Opacity, 0.0F, TimeSpan.FromSeconds(1.0F * Opacity));

          Animation.Start();
        }
      };

      return Overlay;
    }
    private Inv.Panel AnimationsFade(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.Black;

      var StartButton = Surface.NewFlatButton();
      Overlay.AddPanel(StartButton);
      StartButton.Alignment.BottomLeft();
      StartButton.Background.Colour = Inv.Colour.DarkGreen;
      StartButton.Padding.Set(20);

      var StartLabel = Surface.NewLabel();
      StartButton.Content = StartLabel;
      StartLabel.Font.Size = 20;
      StartLabel.Justify.Center();
      StartLabel.Font.Colour = Inv.Colour.White;
      StartLabel.Text = "START\nANIMATION";

      var StopButton = Surface.NewFlatButton();
      Overlay.AddPanel(StopButton);
      StopButton.Alignment.BottomRight();
      StopButton.Background.Colour = Inv.Colour.DarkRed;
      StopButton.Padding.Set(20);

      var StopLabel = Surface.NewLabel();
      StopButton.Content = StopLabel;
      StopLabel.Font.Size = 20;
      StopLabel.Font.Colour = Inv.Colour.White;
      StopLabel.Justify.Center();
      StopLabel.Text = "STOP\nANIMATION";

      var RestartButton = Surface.NewFlatButton();
      Overlay.AddPanel(RestartButton);
      RestartButton.Alignment.BottomCenter();
      RestartButton.Background.Colour = Inv.Colour.Purple;
      RestartButton.Padding.Set(20);

      var RestartLabel = Surface.NewLabel();
      RestartButton.Content = RestartLabel;
      RestartLabel.Font.Size = 20;
      RestartLabel.Font.Colour = Inv.Colour.White;
      RestartLabel.Justify.Center();
      RestartLabel.Text = "RESTART\nANIMATION";

      var TargetButton = Surface.NewFlatButton();
      Overlay.AddPanel(TargetButton);
      TargetButton.Alignment.Center();
      TargetButton.Background.Colour = Inv.Colour.LightBlue;
      TargetButton.Padding.Set(20);

      var TargetLabel = Surface.NewLabel();
      TargetButton.Content = TargetLabel;
      TargetLabel.Font.Size = 60;
      TargetLabel.Justify.Center();
      TargetLabel.Text = "ANIMATION TARGET";

      var OutAnimation = Surface.NewAnimation();
      var OutTarget = OutAnimation.AddTarget(TargetButton);
      OutTarget.FadeOpacity(1.0F, 0.0F, TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(1));

      var InAnimation = Surface.NewAnimation();
      var InTarget = InAnimation.AddTarget(TargetButton);
      InTarget.FadeOpacity(1.0F, TimeSpan.FromSeconds(0.5F));

      StartButton.SingleTapEvent += () =>
      {
        InAnimation.Stop();
        OutAnimation.Stop();

        OutAnimation.Start();
      };

      StopButton.SingleTapEvent += () =>
      {
        InAnimation.Stop();
        OutAnimation.Stop();
      };

      RestartButton.SingleTapEvent += () =>
      {
        StopButton.SingleTap();
        StartButton.SingleTap();
      };

      TargetButton.SingleTapEvent += () =>
      {
        Debug.WriteLine("TARGET");

        InAnimation.Stop();
        OutAnimation.Stop();

        InAnimation.Start();
      };

      return Overlay;
    }
    private Inv.Panel AnimationsRotate(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.Black;

      var BusyButton = Surface.NewFlatButton();
      Overlay.AddPanel(BusyButton);
      BusyButton.Alignment.TopCenter();
      BusyButton.Background.Colour = Inv.Colour.DarkSlateBlue;
      BusyButton.Padding.Set(20);
      BusyButton.SingleTapEvent += () =>
      {
        var BusySurface = new BusySurface(Surface.Window.NewSurface());
        BusySurface.Title = "Please wait";
        BusySurface.Description = "Loading remote resources";
        BusySurface.Become();
      };

      var BusyLabel = Surface.NewLabel();
      BusyButton.Content = BusyLabel;
      BusyLabel.Font.Size = 20;
      BusyLabel.Justify.Center();
      BusyLabel.Font.Colour = Inv.Colour.White;
      BusyLabel.Text = "BUSY";

      var StartButton = Surface.NewFlatButton();
      Overlay.AddPanel(StartButton);
      StartButton.Alignment.BottomLeft();
      StartButton.Background.Colour = Inv.Colour.DarkGreen;
      StartButton.Padding.Set(20);

      var StartLabel = Surface.NewLabel();
      StartButton.Content = StartLabel;
      StartLabel.Font.Size = 20;
      StartLabel.Justify.Center();
      StartLabel.Font.Colour = Inv.Colour.White;
      StartLabel.Text = "START\nANIMATION";

      var StopButton = Surface.NewFlatButton();
      Overlay.AddPanel(StopButton);
      StopButton.Alignment.BottomRight();
      StopButton.Background.Colour = Inv.Colour.DarkRed;
      StopButton.Padding.Set(20);

      var StopLabel = Surface.NewLabel();
      StopButton.Content = StopLabel;
      StopLabel.Font.Size = 20;
      StopLabel.Font.Colour = Inv.Colour.White;
      StopLabel.Justify.Center();
      StopLabel.Text = "STOP\nANIMATION";

      var RestartButton = Surface.NewFlatButton();
      Overlay.AddPanel(RestartButton);
      RestartButton.Alignment.BottomCenter();
      RestartButton.Background.Colour = Inv.Colour.Purple;
      RestartButton.Padding.Set(20);

      var RestartLabel = Surface.NewLabel();
      RestartButton.Content = RestartLabel;
      RestartLabel.Font.Size = 20;
      RestartLabel.Font.Colour = Inv.Colour.White;
      RestartLabel.Justify.Center();
      RestartLabel.Text = "RESTART\nANIMATION";

      var TargetButton = Surface.NewFlatButton();
      Overlay.AddPanel(TargetButton);
      TargetButton.Padding.Set(20);
      TargetButton.Alignment.Center();
      TargetButton.Background.Colour = Inv.Colour.DodgerBlue;

      var TargetLabel = Surface.NewLabel();
      TargetButton.Content = TargetLabel;
      TargetLabel.Font.Size = 60;
      TargetLabel.Font.Colour = Inv.Colour.White;
      TargetLabel.Justify.Center();
      TargetLabel.Text = "ROTATE ANGLE";

      var AnimationDelay = TimeSpan.FromMilliseconds(2000);

      var PressAnimation = Surface.NewAnimation();
      PressAnimation.AddTarget(TargetButton).RotateAngle(180F, AnimationDelay);

      var ReleaseAnimation = Surface.NewAnimation();
      ReleaseAnimation.AddTarget(TargetButton).RotateAngle(0F, AnimationDelay);

      TargetButton.PressEvent += () =>
      {
        ReleaseAnimation.Stop();
        PressAnimation.Start();
      };
      TargetButton.ReleaseEvent += () =>
      {
        PressAnimation.Stop();
        ReleaseAnimation.Start();
      };

      var Animation = Surface.NewAnimation();
      var Target = Animation.AddTarget(TargetButton);
      Target.RotateAngle(0.0F, 360F, TimeSpan.FromSeconds(2));

      StartButton.SingleTapEvent += () => Animation.Start();
      StopButton.SingleTapEvent += () => Animation.Stop();
      RestartButton.SingleTapEvent += () => Animation.Restart();

      return Overlay;
    }
    private Inv.Panel AnimationsScale(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.Black;

      var StartButton = Surface.NewFlatButton();
      Overlay.AddPanel(StartButton);
      StartButton.Alignment.BottomLeft();
      StartButton.Background.Colour = Inv.Colour.DarkGreen;
      StartButton.Padding.Set(20);

      var StartLabel = Surface.NewLabel();
      StartButton.Content = StartLabel;
      StartLabel.Font.Size = 20;
      StartLabel.Justify.Center();
      StartLabel.Font.Colour = Inv.Colour.White;
      StartLabel.Text = "START\nANIMATION";

      var StopButton = Surface.NewFlatButton();
      Overlay.AddPanel(StopButton);
      StopButton.Alignment.BottomRight();
      StopButton.Background.Colour = Inv.Colour.DarkRed;
      StopButton.Padding.Set(20);

      var StopLabel = Surface.NewLabel();
      StopButton.Content = StopLabel;
      StopLabel.Font.Size = 20;
      StopLabel.Font.Colour = Inv.Colour.White;
      StopLabel.Justify.Center();
      StopLabel.Text = "STOP\nANIMATION";

      var RestartButton = Surface.NewFlatButton();
      Overlay.AddPanel(RestartButton);
      RestartButton.Alignment.BottomCenter();
      RestartButton.Background.Colour = Inv.Colour.Purple;
      RestartButton.Padding.Set(20);

      var RestartLabel = Surface.NewLabel();
      RestartButton.Content = RestartLabel;
      RestartLabel.Font.Size = 20;
      RestartLabel.Font.Colour = Inv.Colour.White;
      RestartLabel.Justify.Center();
      RestartLabel.Text = "RESTART\nANIMATION";

      var TargetButton = Surface.NewFlatButton();
      Overlay.AddPanel(TargetButton);
      TargetButton.Padding.Set(20);
      TargetButton.Alignment.Center();
      TargetButton.Background.Colour = Inv.Colour.DodgerBlue;

      var TargetLabel = Surface.NewLabel();
      TargetButton.Content = TargetLabel;
      TargetLabel.Font.Size = 60;
      TargetLabel.Font.Colour = Inv.Colour.White;
      TargetLabel.Justify.Center();
      TargetLabel.Text = "SCALE SIZE";

      var AnimationDelay = TimeSpan.FromMilliseconds(500);

      var PressAnimation = Surface.NewAnimation();
      PressAnimation.AddTarget(TargetButton).ScaleSize(0.50F, AnimationDelay);

      var ReleaseAnimation = Surface.NewAnimation();
      ReleaseAnimation.AddTarget(TargetButton).ScaleSize(1.00F, AnimationDelay);

      TargetButton.PressEvent += () =>
      {
        ReleaseAnimation.Stop();
        PressAnimation.Start();
      };
      TargetButton.ReleaseEvent += () =>
      {
        PressAnimation.Stop();
        ReleaseAnimation.Start();          
      };

      var Animation = Surface.NewAnimation();
      var Target = Animation.AddTarget(TargetButton);
      Target.ScaleSize(1.0F, 0.1F, TimeSpan.FromSeconds(2));

      StartButton.SingleTapEvent += () => Animation.Start();
      StopButton.SingleTapEvent += () => Animation.Stop();
      RestartButton.SingleTapEvent += () => Animation.Restart();

      return Overlay;
    }
    private Inv.Panel AnimationsTranslate(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.Black;

      var StartButton = Surface.NewFlatButton();
      Overlay.AddPanel(StartButton);
      StartButton.Alignment.BottomLeft();
      StartButton.Background.Colour = Inv.Colour.DarkGreen;
      StartButton.Padding.Set(20);

      var StartLabel = Surface.NewLabel();
      StartButton.Content = StartLabel;
      StartLabel.Font.Size = 20;
      StartLabel.Justify.Center();
      StartLabel.Font.Colour = Inv.Colour.White;
      StartLabel.Text = "START\nANIMATION";

      var StopButton = Surface.NewFlatButton();
      Overlay.AddPanel(StopButton);
      StopButton.Alignment.BottomRight();
      StopButton.Background.Colour = Inv.Colour.DarkRed;
      StopButton.Padding.Set(20);

      var StopLabel = Surface.NewLabel();
      StopButton.Content = StopLabel;
      StopLabel.Font.Size = 20;
      StopLabel.Font.Colour = Inv.Colour.White;
      StopLabel.Justify.Center();
      StopLabel.Text = "STOP\nANIMATION";

      var RestartButton = Surface.NewFlatButton();
      Overlay.AddPanel(RestartButton);
      RestartButton.Alignment.BottomCenter();
      RestartButton.Background.Colour = Inv.Colour.Purple;
      RestartButton.Padding.Set(20);

      var RestartLabel = Surface.NewLabel();
      RestartButton.Content = RestartLabel;
      RestartLabel.Font.Size = 20;
      RestartLabel.Font.Colour = Inv.Colour.White;
      RestartLabel.Justify.Center();
      RestartLabel.Text = "RESTART\nANIMATION";

      var TargetButton = Surface.NewFlatButton();
      Overlay.AddPanel(TargetButton);
      TargetButton.Padding.Set(20);
      TargetButton.Alignment.Center();
      TargetButton.Background.Colour = Inv.Colour.DodgerBlue;

      var TargetLabel = Surface.NewLabel();
      TargetButton.Content = TargetLabel;
      TargetLabel.Font.Size = 60;
      TargetLabel.Font.Colour = Inv.Colour.White;
      TargetLabel.Justify.Center();
      TargetLabel.Text = "TRANSLATE POSITION";

      var AnimationDelay = TimeSpan.FromMilliseconds(2000);

      var PressAnimation = Surface.NewAnimation();
      PressAnimation.AddTarget(TargetButton).TranslatePosition(-50, AnimationDelay);

      var ReleaseAnimation = Surface.NewAnimation();
      ReleaseAnimation.AddTarget(TargetButton).TranslatePosition(0, AnimationDelay);

      TargetButton.PressEvent += () =>
      {
        ReleaseAnimation.Stop();
        PressAnimation.Start();
      };
      TargetButton.ReleaseEvent += () =>
      {
        PressAnimation.Stop();
        ReleaseAnimation.Start();
      };

      var Animation = Surface.NewAnimation();

      var Target = Animation.AddTarget(TargetButton);
      Target.TranslateX(0, 50, TimeSpan.FromSeconds(2));

      StartButton.SingleTapEvent += () => Animation.Start();
      StopButton.SingleTapEvent += () => Animation.Stop();
      RestartButton.SingleTapEvent += () => Animation.Restart();

      return Overlay;
    }
    private Inv.Panel Banner(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.WhiteSmoke;

      var Graphic = Surface.NewGraphic();
      Overlay.AddPanel(Graphic);
      Graphic.Margin.Set(0, 10, 0, 0);
      Graphic.Background.Colour = Inv.Colour.Red;
      Graphic.Alignment.TopStretch();
      Graphic.Image = Resources.Images.WavesBox;

      return Overlay;
    }
    private Inv.Panel Blank(Inv.Surface Surface)
    {
      return Surface.NewVerticalDock();
    }
    private Inv.Panel Block(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.White;

      var TitleBlock = Surface.NewBlock();
      Overlay.AddPanel(TitleBlock);
      TitleBlock.Background.Colour = Inv.Colour.WhiteSmoke;
      TitleBlock.Alignment.Center();
      TitleBlock.Size.Set(400, 400);
      TitleBlock.Padding.Set(10);
      TitleBlock.Font.Monospaced();
      TitleBlock.Font.Thin();
      TitleBlock.Font.ExtraLarge();
      TitleBlock.Font.Colour = Inv.Colour.Black;
      TitleBlock.AddRun("Main Heading", S =>
      {
        S.Font.ExtraMassive();
        S.Font.Bold();
        S.Font.Colour = Inv.Colour.DodgerBlue;
      });
      TitleBlock.AddBreak();
      TitleBlock.AddRun("Subheading", S =>
      {
        S.Font.Massive();
        S.Font.Colour = Inv.Colour.DimGray;
      });
      TitleBlock.AddBreak();
      TitleBlock.AddBreak();
      TitleBlock.AddRun("This is a long paragraph or nothing so much, ");
      var HighlightRun = TitleBlock.AddRun("we just want to see some word wrapping and ", S => S.Background.Colour = Inv.Colour.Yellow);
      TitleBlock.AddRun("some coloured and bold text ", S =>
      {
        S.Font.Bold();
        S.Font.Colour = Inv.Colour.HotPink;
      });

      var ItalicsRun = TitleBlock.AddRun("and some italics text ", S => S.Font.Italics());
      var UnderlineRun = TitleBlock.AddRun("and don't forget underlined text", S => S.Font.Underlined());
      var SmallCapsRun = TitleBlock.AddRun(". And lastly, what about SmallCaps?", S =>
      {
        S.Font.Proportional();
        S.Font.SmallCaps();
      });

      var RestyleLabel = Surface.NewLabel();
      Overlay.AddPanel(RestyleLabel);
      RestyleLabel.Alignment.BottomCenter();
      RestyleLabel.Font.Monospaced();
      RestyleLabel.Font.Thin();
      RestyleLabel.Font.ExtraLarge();
      RestyleLabel.Font.SmallCaps();
      RestyleLabel.Font.Italics();
      RestyleLabel.Font.Underlined();
      RestyleLabel.Text = "Restyling example text";

      var BlockButton = Surface.NewFlatButton();
      Overlay.AddPanel(BlockButton);
      BlockButton.Alignment.BottomLeft();
      BlockButton.Size.Set(64, 64);
      BlockButton.Background.Colour = Inv.Colour.HotPink;
      BlockButton.SingleTapEvent += () =>
      {
        TitleBlock.Font.Size++;
      };

      var RestyleButton = Surface.NewFlatButton();
      Overlay.AddPanel(RestyleButton);
      RestyleButton.Alignment.BottomRight();
      RestyleButton.Size.Set(64, 64);
      RestyleButton.Background.Colour = Inv.Colour.DodgerBlue;
      RestyleButton.SingleTapEvent += () =>
      {
        bool? TristateAdvance(bool? Data) => Data == null ? true : Data.Value ? false : (bool?)null;

        RestyleLabel.Font.IsSmallCaps = TristateAdvance(RestyleLabel.Font.IsSmallCaps);
        SmallCapsRun.Font.IsSmallCaps = TristateAdvance(SmallCapsRun.Font.IsSmallCaps);

        RestyleLabel.Font.IsItalics = TristateAdvance(RestyleLabel.Font.IsItalics);
        ItalicsRun.Font.IsItalics = TristateAdvance(ItalicsRun.Font.IsItalics);

        RestyleLabel.Font.IsUnderlined = TristateAdvance(RestyleLabel.Font.IsUnderlined);
        UnderlineRun.Font.IsUnderlined = TristateAdvance(UnderlineRun.Font.IsUnderlined);

        HighlightRun.Background.Colour = HighlightRun.Background.Colour == null ? Inv.Colour.Yellow : HighlightRun.Background.Colour == Inv.Colour.Yellow ? Inv.Colour.AliceBlue : null;

        if (TitleBlock.Font.Name == null)
          TitleBlock.Font.Monospaced();
        else if (TitleBlock.Font.Name == Surface.Window.Application.Device.MonospacedFontName)
          TitleBlock.Font.Proportional();
        else
          TitleBlock.Font.Name = null;

        if (RestyleLabel.Font.Name == null)
          RestyleLabel.Font.Monospaced();
        else if (RestyleLabel.Font.Name == Surface.Window.Application.Device.MonospacedFontName)
          RestyleLabel.Font.Proportional();
        else
          RestyleLabel.Font.Name = null;
      };

      return Overlay;
    }
    private Inv.Panel BrowseFile(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.Black;

      var TempFolder = Surface.Window.Application.Directory.NewFolder("Temp");

      var PDFButton = Surface.NewFlatButton();
      Overlay.AddPanel(PDFButton);
      PDFButton.Alignment.BottomLeft();
      PDFButton.Background.Colour = Inv.Colour.DarkGreen;
      PDFButton.Padding.Set(20);

      var PDFLabel = Surface.NewLabel();
      PDFButton.Content = PDFLabel;
      PDFLabel.Font.Size = 30;
      PDFLabel.Justify.Center();
      PDFLabel.Font.Colour = Inv.Colour.White;
      PDFLabel.Text = "BROWSE PDF";

      PDFButton.SingleTapEvent += () =>
      {
        var TempFile = TempFolder.NewFile("TempFile.pdf");
        TempFile.WriteAllBytes(Resources.Documents.PhoenixPlan.GetBuffer());

        //TempFile.SetLastWriteTimeUtc(new DateTime(2016, 01, 01));
        //PDFLabel.Text = TempFile.GetLastWriteTimeUtc().ToString();

        Surface.Window.Browse(TempFile);
      };

      var DOCButton = Surface.NewFlatButton();
      Overlay.AddPanel(DOCButton);
      DOCButton.Alignment.BottomRight();
      DOCButton.Background.Colour = Inv.Colour.Purple;
      DOCButton.Padding.Set(20);
      DOCButton.SingleTapEvent += () =>
      {
        var TempFile = TempFolder.NewFile("TempFile.docx");
        TempFile.WriteAllBytes(Resources.Documents.PhoenixLogoDoc.GetBuffer());

        Surface.Window.Browse(TempFile);
      };

      var DOCLabel = Surface.NewLabel();
      DOCButton.Content = DOCLabel;
      DOCLabel.Font.Size = 30;
      DOCLabel.Justify.Center();
      DOCLabel.Font.Colour = Inv.Colour.White;
      DOCLabel.Text = "BROWSE DOC";

      var CreditsButton = Surface.NewFlatButton();
      Overlay.AddPanel(CreditsButton);
      CreditsButton.Alignment.TopLeft();
      CreditsButton.Background.Colour = Inv.Colour.DarkCyan;
      CreditsButton.Padding.Set(20);

      var CreditsLabel = Surface.NewLabel();
      CreditsButton.Content = CreditsLabel;
      CreditsLabel.Font.Size = 30;
      CreditsLabel.Justify.Center();
      CreditsLabel.Font.Colour = Inv.Colour.White;
      CreditsLabel.Text = "SHARE CREDITS";

      CreditsButton.SingleTapEvent += () =>
      {
        var TempFile = TempFolder.NewFile("TempFile.Credits");
        TempFile.AsText().WriteAll(Resources.Texts.Credits);

        Surface.Window.Share(TempFile);
      };

      return Overlay;
    }
    private Inv.Panel ButtonGrid(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var CustomQuery = new CustomQuery(Surface);
      Overlay.AddPanel(CustomQuery);
      CustomQuery.Alignment.StretchCenter();

      var ActionPanel = new ActionPanel(Surface);
      CustomQuery.Content = ActionPanel;
      ActionPanel.Caption = "ACTION PANEL";
      ActionPanel.Alignment.BottomStretch();

      var OneButton = ActionPanel.AddHeaderButton(); // o
      OneButton.Caption = "ONE";
      OneButton.IsVisible = true;

      var TwoButton = ActionPanel.AddHeaderButton(); // c
      TwoButton.Caption = "TWO";
      TwoButton.IsVisible = false;

      var ThreeButton = ActionPanel.AddHeaderButton(); // l
      ThreeButton.Caption = "THREE";
      ThreeButton.IsVisible = false;

      var SevenButton = ActionPanel.AddHeaderButton(); // u
      SevenButton.Caption = "SEVEN";
      SevenButton.IsVisible = false;

      var FourButton = ActionPanel.AddFooterButton(); // k
      FourButton.Caption = "FOUR";
      FourButton.IsEnabled = false;

      var FiveButton = ActionPanel.AddFooterButton(); // d
      FiveButton.Caption = "FIVE";
      FiveButton.IsEnabled = false;

      var SixButton = ActionPanel.AddFooterButton(); // h
      SixButton.Caption = "SIX";

      ActionPanel.Compose(Surface.Window.Width, Surface.Window.Height);

      CustomQuery.Size.SetMinimumWidth(Surface.Window.Width);
      CustomQuery.Show();

      return Overlay;
    }
    private Inv.Panel ButtonRound(Inv.Surface Surface)
    {
      var Button = Surface.NewFlatButton();
      Button.Background.Colour = Inv.Colour.FromArgb(0x80, 0xFF, 0x00, 0x00);
      Button.Margin.Set(20);
      Button.Padding.Set(20);
      Button.Size.Set(200, 200);
      Button.Alignment.Center();
      Button.Border.Colour = Inv.Colour.FromArgb(0x80, 0x00, 0xFF, 0x00);

      var InnerBox = Surface.NewFrame();
      InnerBox.Size.AutoMaximum();
      InnerBox.Background.Colour = Inv.Colour.FromArgb(0x80, 0xFF, 0xFF, 0xFF);
      Button.Content = InnerBox;

      var Configurations = new Action[]
      {
        () =>
        {
          Button.Border.Set(20, 10, 5, 0);
          Button.Corner.Set(100, 100, 100, 0);
        },
        () =>
        {
          Button.Border.Set(0);
          Button.Corner.Set(100, 100, 100, 0);
        },
        () =>
        {
          Button.Border.Set(0);
          Button.Corner.Set(0);
        },
        () =>
        {
          // TODO: Just changing the thickness like this does not trigger an update
          Button.Border.Set(100, 0, 50, 10);
          Button.Corner.Set(0);
        },
        () =>
        {
          Button.Border.Set(50, 10, 5, 0);
          Button.Corner.Set(100, 400, 100, 0);
        },
        () =>
        {
          Button.Border.Set(50, 10, 5, 5);
          Button.Corner.Set(800, 800, 100, 800);
        },
        () =>
        {
          Button.Border.Set(50, 10, 5, 5);
          Button.Corner.Set(50);
        },
        () =>
        {
          Button.Border.Set(10);
          Button.Corner.Set(50);
        }
      };

      var Index = 0;
      void SwitchButton() => Configurations[Index++ % Configurations.Length]();

      Button.SingleTapEvent += SwitchButton;
      SwitchButton();

      return Button;
    }
    private Inv.Panel ButtonSymmetry(Inv.Surface Surface)
    {
      var Stack = Surface.NewVerticalStack();
      Stack.Alignment.Center();

      var B1 = Surface.NewFlatButton();
      Stack.AddPanel(B1);
      B1.Corner.Set(50, 50, 0, 0);
      B1.Background.Colour = Inv.Colour.DarkGreen;
      B1.Size.Set(200, 100);

      var B2 = Surface.NewFlatButton();
      Stack.AddPanel(B2);
      B2.Corner.Set(0, 0, 50, 50);
      B2.Background.Colour = Inv.Colour.ForestGreen;
      B2.Size.Set(200, 100);

      return Stack;
    }
    private Inv.Panel ButtonComplex(Inv.Surface Surface)
    {
      var Button = new ComplexButton(Surface);
      Button.Alignment.TopStretch();

      return Button;
    }
    private Inv.Panel ButtonLeak(Inv.Surface Surface)
    {
      var Stack = Surface.NewVerticalStack();
      Stack.Alignment.Center();

      var DumpButton = Surface.NewFlatButton();
      Stack.AddPanel(DumpButton);
      DumpButton.Padding.Set(50);
      DumpButton.Background.Colour = Inv.Colour.Brown;
      DumpButton.SingleTapEvent += () =>
      {
        Shell.DumpEvent();
      };

      var AddButton = Surface.NewFlatButton();
      Stack.AddPanel(AddButton);
      AddButton.Padding.Set(50);
      AddButton.Background.Colour = Inv.Colour.DarkGreen;
      AddButton.SingleTapEvent += () =>
      {
        var RemoveButton = Surface.NewFlatButton();
        Stack.AddPanel(RemoveButton);
        RemoveButton.Padding.Set(50);
        RemoveButton.Background.Colour = Inv.Colour.DarkRed;
        RemoveButton.SingleTapEvent += () =>
        {
          Stack.RemovePanel(RemoveButton);
        };
      };

      return Stack;
    }
    private Inv.Panel ButtonCall(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      //var ContactScroll = Surface.NewVerticalScroll();
      //Overlay.AddElement(ContactScroll);
      //ContactScroll.Alignment.Center();

      var ContactStack = Surface.NewVerticalStack();
      //ContactScroll.Content = ContactStack;
      Overlay.AddPanel(ContactStack);

      var EmailButton = Surface.NewFlatButton();
      ContactStack.AddPanel(EmailButton);
      EmailButton.Background.Colour = Inv.Colour.DarkOrange;
      EmailButton.SingleTapEvent += () =>
      {
        EmailButton.Background.Colour = Inv.Colour.Red;
        EmailButton.Visibility.Collapse();

        var Shade = Surface.NewFlatButton();
        Overlay.AddPanel(Shade);
        Shade.Background.Colour = Inv.Colour.Black.Opacity(0.50F);
        Shade.SingleTapEvent += () => Overlay.RemovePanel(Shade);
        //Application.Email.NewMessage().Send();
      };

      var EmailLabel = Surface.NewLabel();
      EmailButton.Content = EmailLabel;
      EmailLabel.Alignment.Center();
      EmailLabel.Font.ExtraLarge();
      EmailLabel.Font.Colour = Inv.Colour.White;
      EmailLabel.Padding.Set(15);
      EmailLabel.Text = "john.carey@kestral.com.au";

      var CallButton = Surface.NewFlatButton();
      ContactStack.AddPanel(CallButton);
      CallButton.Background.Colour = Inv.Colour.DarkGreen;
      CallButton.SingleTapEvent += () =>
      {
        CallButton.Visibility.Collapse();
        /*
        if (CallButton.Margin.IsSet)
          CallButton.Margin.Reset();
        else
          CallButton.Margin.Set(20);*/
      };

      var CallLabel = Surface.NewLabel();
      CallButton.Content = CallLabel;
      //CallLabel.Alignment.Center();
      CallLabel.Font.Size = 30;
      CallLabel.Font.Colour = Inv.Colour.White;
      CallLabel.Padding.Set(30);
      CallLabel.Text = "CALL\n+61412171734";

      var SMSButton = Surface.NewFlatButton();
      ContactStack.AddPanel(SMSButton);
      SMSButton.Background.Colour = Inv.Colour.DarkGreen;
      SMSButton.IsEnabled = false;
      SMSButton.SingleTapEvent += () =>
      {
        SMSButton.Visibility.Collapse();
        /*
        if (SMSButton.Padding.IsSet)
          SMSButton.Padding.Reset();
        else
          SMSButton.Padding.Set(20);*/
      };

      var SMSLabel = Surface.NewLabel();
      SMSButton.Content = SMSLabel;
      SMSLabel.Alignment.Center();
      SMSLabel.Font.Size = 30;
      SMSLabel.Font.Colour = Inv.Colour.White;
      SMSLabel.Padding.Set(30);
      SMSLabel.Text = "SMS\n+61412171734";

      var ResetButton = Surface.NewFlatButton();
      ContactStack.AddPanel(ResetButton);
      ResetButton.Background.Colour = Inv.Colour.DarkGreen;
      ResetButton.SingleTapEvent += () =>
      {
        EmailButton.Visibility.Show();
        CallButton.Visibility.Show();
        SMSButton.Visibility.Show();
      };

      var ResetLabel = Surface.NewLabel();
      ResetButton.Content = ResetLabel;
      //ResetLabel.Alignment.Center();
      ResetLabel.Font.Size = 30;
      ResetLabel.Font.Colour = Inv.Colour.White;
      ResetLabel.Padding.Set(30);
      ResetLabel.Text = "RESET";

      return Overlay;
    }
    private Inv.Panel ButtonExclusive(Inv.Surface Surface)
    {
      var Board = Surface.NewBoard();

      var B1 = Surface.NewFlatButton();
      B1.Background.Colour = Inv.Colour.Red;
      B1.PressEvent += () => Board.RemovePin(B1);
      B1.ReleaseEvent += () => AddB1();
      B1.SingleTapEvent += () =>
      {
        B1.Background.Colour = B1.Background.Colour == Inv.Colour.Red ? Inv.Colour.Blue : Inv.Colour.Red;
      };

      var B2 = Surface.NewFlatButton();
      B2.Background.Colour = Inv.Colour.Red;
      B2.PressEvent += () => Board.RemovePin(B2);
      B2.ReleaseEvent += () => AddB2();
      B2.SingleTapEvent += () =>
      {
        B2.Background.Colour = B2.Background.Colour == Inv.Colour.Red ? Inv.Colour.Blue : Inv.Colour.Red;
      };

      void AddB1() => Board.AddPin(B1, new Inv.Rect(50, 50, 200, 200));
      void AddB2() => Board.AddPin(B2, new Inv.Rect(50, 300, 200, 200));

      AddB1();
      AddB2();

      var B3 = Surface.NewFlatButton();
      Board.AddPin(B3, new Inv.Rect(50, 600, 200, 200));
      B3.Background.Colour = Inv.Colour.Green;
      B3.SingleTapEvent += () =>
      {
        if (!Board.HasPanel(B1))
          AddB1();

        if (!Board.HasPanel(B2))
          AddB2();
      };

      return Board;
    }
    private Inv.Panel ButtonElevation(Inv.Surface Surface)
    {
      var Frame = Surface.NewFrame();
      Frame.Background.Colour = Inv.Colour.DimGray;

      var Button = Surface.NewFlatButton();
      Frame.Content = Button;

      Button.Background.Colour = Inv.Colour.SteelBlue;
      Button.Alignment.Center();
      Button.Size.Set(200, 200);
      Button.Elevation.Set(5);
      Button.SingleTapEvent += () =>
      {
        if (Frame.Alignment.Get() == Inv.Placement.Stretch)
        {
          Frame.Alignment.Center();
        }
        else
        {
          if (Frame.Background.Colour == Inv.Colour.Black)
            Frame.Background.Colour = Inv.Colour.DimGray;
          else
            Frame.Background.Colour = Inv.Colour.Black;
          Frame.Alignment.Stretch();
        }
      };

      return Frame;
    }
    private Inv.Panel CenterInScroll(Inv.Surface Surface)
    {
      var Scroll = Surface.NewVerticalScroll();
      
      var Label = Surface.NewLabel();
      Scroll.Content = Label;
      Label.Alignment.Center();
      Label.Font.Colour = Inv.Colour.DimGray;
      Label.Text = "SHOULD BE HORIZONTALLY AND VERTICALLY CENTRED";

      return Scroll;
    }
    private Inv.Panel CrashParenting(Inv.Surface Surface)
    {
      var Frame = Surface.NewFrame();

      var ParentStack = Surface.NewVerticalStack();
      Frame.Content = ParentStack;

      var Button = Surface.NewFlatButton();
      ParentStack.AddPanel(Button);
      Button.Background.Colour = Inv.Colour.SteelBlue;
      Button.Padding.Set(50);

      var ChildStack = Surface.NewHorizontalStack();
      ParentStack.AddPanel(ChildStack);

      var Label = Surface.NewLabel();
      Button.Content = Label;
      Label.Font.Colour = Inv.Colour.White;
      Label.Text = "HELLO";

      Button.SingleTapEvent += () =>
      {
        if (Label.Text == "HELLO")
        {
          Label.Text = "WORLD";
          ChildStack.Margin.Set(50);

          ParentStack.RemovePanel(Button);
          ChildStack.AddPanel(Button);
        }
        else
        {
          Label.Text = "HELLO";
          ChildStack.Margin.Clear();

          ChildStack.RemovePanel(Button);
          ParentStack.AddPanel(Button);
        }
      };

      return Frame;
    }
    private Inv.Panel Credits(Inv.Surface Surface)
    {
      var CreditQuery = new CustomQuery(Surface);
      CreditQuery.ShadeColour = Inv.Colour.Black.Opacity(0.75F);

      var CreditPanel = new CustomPanel(Surface);
      CreditQuery.Content = CreditPanel;
      CreditPanel.Alignment.Center();
      CreditPanel.Caption = "CREDITS";

      var CreditDock = Surface.NewVerticalDock();
      CreditPanel.Content = CreditDock;

      var CreditMemo = Surface.NewMemo();
      CreditDock.AddClient(CreditMemo);
      //CreditMemo.Alignment.Center(); // Required due to Uwa memo sizing bug.
      CreditMemo.Padding.Set(20);
      CreditMemo.Font.ExtraLarge();
      CreditMemo.Font.Colour = Inv.Colour.LightGray;
      CreditMemo.IsReadOnly = true;
      CreditMemo.Text = Resources.Texts.Credits;

      var CloseButton = new CaptionButton(Surface);
      CreditDock.AddFooter(CloseButton);
      CloseButton.Alignment.Center();
      CloseButton.Colour = Inv.Colour.DimGray;
      CloseButton.Text = "CLOSE";
      CloseButton.Size.Set(200, 40);
      CloseButton.SingleTapEvent += () => CreditQuery.Hide();

      CreditQuery.Show();

      return CreditQuery;
    }
    private Inv.Panel CustomChart(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var Dictionary = new Dictionary<int, int>();
      Dictionary.Add(0, 80);
      Dictionary.Add(1, 100);
      Dictionary.Add(2, 108);
      Dictionary.Add(3, 94);
      Dictionary.Add(4, 102);
      Dictionary.Add(5, 101);
      Dictionary.Add(6, 109);
      Dictionary.Add(7, 94);
      Dictionary.Add(8, 54);
      Dictionary.Add(9, 79);
      Dictionary.Add(10, 88);

      var Canvas = Surface.NewCanvas();
      Overlay.AddPanel(Canvas);
      //Canvas.Margin.Set(8);
      Canvas.Background.Colour = Inv.Colour.WhiteSmoke;
      Canvas.SingleTapEvent += (Point) =>
      {
      };
      Canvas.DoubleTapEvent += (Point) =>
      {
      };
      Canvas.ContextTapEvent += (Point) =>
      {
      };
      Canvas.PressEvent += (Point) =>
      {
      };
      Canvas.MoveEvent += (Point) =>
      {
      };
      Canvas.ReleaseEvent += (Point) =>
      {
      };
      Canvas.DrawEvent += (DC) =>
      {
        var Dimension = Canvas.GetDimension();

        if (Dimension == Inv.Dimension.Zero)
        {
          Canvas.Draw();
          return;
        }

        var LabelPadding = 10;
        var LabelWidth = 20;

        int MinDimensionValue;
        int TotalHeight;
        int TotalWidth;
        if (Dimension.Height > Dimension.Width)
        {
          MinDimensionValue = Dimension.Width - 24;
          TotalWidth = MinDimensionValue;
          TotalHeight = (int)(MinDimensionValue * (double)9 / 16);
        }
        else
        {
          MinDimensionValue = Dimension.Height - 24;
          TotalWidth = (int)(MinDimensionValue * (double)16 / 9);
          TotalHeight = MinDimensionValue;
        }

        var MarginX = (Dimension.Width - TotalWidth) / 2;
        var MarginY = (Dimension.Height - TotalHeight) / 2;

        var LabelX = MarginX + LabelWidth;
        var ChartWidth = TotalWidth - LabelWidth - LabelPadding;
        var MaxX = MarginX + LabelWidth + LabelPadding + ChartWidth;

        var OriginPoint = new Inv.Point(LabelX + LabelPadding, TotalHeight + MarginY);

        // y axis.
        //DC.DrawLine(Inv.Colour.DarkGray, 1, OriginPoint, new Inv.Point(WidthMargin, HeightMargin));

        // x axis.
        DC.DrawLine(Inv.Colour.DimGray, 1, OriginPoint, new Inv.Point(MaxX, OriginPoint.Y));

        var MinKey = Dictionary.Min(P => P.Key);
        var MaxKey = Dictionary.Max(P => P.Key);
        var MinValue = Dictionary.Min(P => P.Value);
        var MaxValue = Dictionary.Max(P => P.Value);
        MinValue = Math.Max(0, MinValue - MaxValue / 6);
        MaxValue += MaxValue / 6;

        int GetX(int Value)
        {
          return OriginPoint.X + (int)(ChartWidth * (Value / (double)MaxKey));
        }
        int GetY(int Value)
        {
          return OriginPoint.Y - (int)(TotalHeight * ((double)(Value - MinValue) / ((double)MaxValue - MinValue)));
        };

        var GridLine = 10;
        var GridIndex = GridLine;
        while (GridIndex < MinValue)
          GridIndex += GridLine;

        while (GridIndex < MaxValue)
        {
          var GridY = GetY(GridIndex);

          DC.DrawLine(Inv.Colour.LightGray, 1, new Inv.Point(OriginPoint.X, GridY), new Inv.Point(MaxX, GridY));

          DC.DrawText(GridIndex.ToString(), "Roboto", 12, Inv.FontWeight.Thin, Inv.Colour.DimGray, new Inv.Point(LabelX, GridY), Inv.HorizontalPosition.Right, Inv.VerticalPosition.Center);

          GridIndex += GridLine;
        }

        var LastPoint = Inv.Point.Zero;
        foreach (var Point in Dictionary.OrderBy(P => P.Key))
        {
          var Key = Point.Key;
          var Value = Point.Value;

          var KeyX = GetX(Key);
          var ValueY = GetY(Value);
          var DataPoint = new Inv.Point(KeyX, ValueY);

          //DC.DrawEllipse(Inv.Colour.Blue.Lighten(0.20F), Inv.Colour.Blue.Lighten(0.20F), 1, DataPoint, new Inv.Point(1, 1));

          if (LastPoint != Inv.Point.Zero)
            DC.DrawLine(Inv.Colour.Blue.Lighten(0.10F), 1, LastPoint, DataPoint);

          LastPoint = DataPoint;
        }

        //DC.
      };

      Canvas.Draw();
      //Surface.ComposeEvent += () => Canvas.Draw();
      Surface.ArrangeEvent += () => Canvas.Draw();

      return Overlay;
    }
    private Inv.Panel CustomCanvas(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      
      var PressPoint = (Inv.Point?)null;

      var Canvas = Surface.NewCanvas();
      Overlay.AddPanel(Canvas);
      Canvas.Background.Colour = Inv.Colour.WhiteSmoke;
      Canvas.SingleTapEvent += (Point) =>
      {
        Canvas.Background.Colour = Inv.Colour.Yellow;
        Debug.WriteLine("single");
      };
      Canvas.DoubleTapEvent += (Point) =>
      {
        Canvas.Background.Colour = Inv.Colour.Purple;
        Debug.WriteLine("double");
      };
      Canvas.ContextTapEvent += (Point) =>
      {
        Canvas.Background.Colour = Inv.Colour.SteelBlue;
        Debug.WriteLine("context");
      };
      Canvas.PressEvent += (Point) =>
      {
        PressPoint = Point;
      };
      Canvas.MoveEvent += (Point) =>
      {
        PressPoint = Point;
      };
      Canvas.ReleaseEvent += (Point) =>
      {
        PressPoint = null;
      };

      var ZoomFactor = 0;
      var ZoomPoint = (Inv.Point?)null;

      Canvas.ZoomEvent += (Zoom) =>
      {
        ZoomPoint = Zoom.Point;
        ZoomFactor += Zoom.Delta > 0 ? +10 : -10;
      };

      var Position = 0;

      var Image1Rect = new Inv.Rect(0, 100, 320, 180);
      var Image2Rect = new Inv.Rect(0, 500, 320, 180);
      var Image3Rect = new Inv.Rect(400, 300, 320, 180);
      var Image4Rect = new Inv.Rect(400, 500, 320, 180);
      var Image5Rect = new Inv.Rect(400, 40, 320, 180);
      var Image6Rect = new Inv.Rect(20, 640, 320, 180);
      var Image7Rect = new Inv.Rect(300, 640, 320, 180);
      var Image8Rect = new Inv.Rect(600, 640, 320, 180);

      Canvas.QueryEvent += (Query) =>
      {
        Query.AddRegion(Image1Rect, "Image 1");
        Query.AddRegion(Image2Rect, "Image 2");
        Query.AddRegion(Image3Rect, "Image 3");
        Query.AddRegion(Image4Rect, "Image 4");
        Query.AddRegion(Image5Rect, "Image 5");
        Query.AddRegion(Image6Rect, "Image 6");
        Query.AddRegion(Image7Rect, "Image 7");
        Query.AddRegion(Image8Rect, "Image 8");

        Query.AddRegion(new Inv.Rect(50, 290, 100, 20), "Hello World");
      };

      Canvas.DrawEvent += (DC) =>
      {
        var DCWidth = Surface.Window.Width;
        var DCHeight = Surface.Window.Height;

        DC.DrawText(Surface.Window.DisplayRate.PerSecond.ToString() + " fps", "", 20, Inv.FontWeight.Regular, Inv.Colour.Black, new Inv.Point(DCWidth - 2, DCHeight - 2), Inv.HorizontalPosition.Right, Inv.VerticalPosition.Bottom);

        var Opacity = Position++ / 1000.0F;
        if (Opacity > 1.0F)
          Opacity = 1.0F;

        var ZoomedSize = Math.Max(10, 100 + ZoomFactor);

        DC.DrawRectangle(Inv.Colour.DarkGreen, Inv.Colour.Red, 3, new Inv.Rect(280, 280, ZoomedSize, ZoomedSize));
        DC.DrawRectangle(Inv.Colour.DarkGreen, Inv.Colour.Blue, 3, new Inv.Rect(280, 380 + ZoomFactor, ZoomedSize, ZoomedSize));
        DC.DrawRectangle(Inv.Colour.DarkGreen, Inv.Colour.Purple, 3, new Inv.Rect(Math.Max(0, 380 + ZoomFactor), Math.Max(0, 280 + ZoomFactor), ZoomedSize, ZoomedSize));
        DC.DrawRectangle(Inv.Colour.DarkGreen, Inv.Colour.Orange, 3, new Inv.Rect(Math.Max(0, 380 + ZoomFactor), Math.Max(0, 380 + ZoomFactor), ZoomedSize, ZoomedSize));

        DC.DrawRectangle(Inv.Colour.Red.Opacity(Opacity), Inv.Colour.Blue.Opacity(Opacity), 1, new Inv.Rect(80, 80, 100, 100));
        DC.DrawEllipse(Inv.Colour.Green.Opacity(Opacity), Inv.Colour.Pink.Opacity(Opacity), 10, new Inv.Point(180, 180), new Inv.Point(50, 50));

        var TintOpacity = 1.00F;

        DC.DrawImage(LogoImage, Image1Rect, Opacity, Inv.Colour.Red.Opacity(TintOpacity), Inv.Mirror.Horizontal);
        DC.DrawImage(LogoImage, Image2Rect, Opacity, Inv.Colour.Green.Opacity(TintOpacity), Inv.Mirror.Vertical);
        DC.DrawImage(LogoImage, Image3Rect, Opacity, Inv.Colour.Yellow.Opacity(TintOpacity));
        DC.DrawImage(LogoImage, Image4Rect, Opacity, Inv.Colour.Blue.Opacity(TintOpacity));
        DC.DrawImage(LogoImage, Image5Rect, Opacity, Inv.Colour.Purple.Opacity(TintOpacity));

        DC.DrawText("Hello World", "", 20, Inv.FontWeight.Regular, Inv.Colour.Purple.Opacity(Opacity), new Inv.Point(100, 300), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Center);

        var LabelBrush = Inv.Colour.DarkRed.Opacity(0.75F);
        var LabelPen = Inv.Colour.DarkRed.Darken(0.25F);
        var LabelRect = new Inv.Rect(600, 600, 300, 35);
        DC.DrawRectangle(LabelBrush, LabelPen, 1, LabelRect);
        DC.DrawText("AbcdefghijklmnopqrstuvwxyZ", "", 20, Inv.FontWeight.Regular, Inv.Colour.Black, new Inv.Point(LabelRect.Left + LabelRect.Width / 2, LabelRect.Top + (LabelRect.Height / 2)), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Center);

        DC.DrawLine(Inv.Colour.Black, 5, new Inv.Point(50, 50), new Inv.Point(100, 100), new Inv.Point(50, 100), new Inv.Point(100, 50), new Inv.Point(50, 50));

        DC.DrawArc(Inv.Colour.Red, Inv.Colour.Blue, 5, new Inv.Point(200, 100), new Inv.Point(50, 50), 0, 90);

        //RC.DrawEllipse(Inv.Colour.Red, Inv.Colour.Blue, 5, new Inv.Point(200, 100), new Inv.Point(5, 5));

        if (PressPoint != null)
          DC.DrawEllipse(Inv.Colour.DarkOrange.Opacity(0.50F), Inv.Colour.Orange.Opacity(0.50F), 5, PressPoint.Value, new Inv.Point(25, 25));

        if (ZoomPoint != null)
          DC.DrawEllipse(Inv.Colour.Pink.Opacity(0.50F), Inv.Colour.HotPink.Opacity(0.50F), 5, ZoomPoint.Value, new Inv.Point(5, 5));

        var Score = 55;
        var FontName = Base.Window.Application.Device.MonospacedFontName;
        var FontSize = 30;
        var ScorePoint = new Inv.Point(200, 200);
        DC.DrawText("+" + Score.ToString(), FontName, FontSize, Inv.FontWeight.Heavy, Inv.Colour.Black.Opacity(0.50F), ScorePoint + new Inv.Point(1, 1), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Bottom);
        DC.DrawText("+" + Score.ToString(), FontName, FontSize, Inv.FontWeight.Heavy, Inv.Colour.White.Opacity(0.50F), ScorePoint + new Inv.Point(-1, -1), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Bottom);
        DC.DrawText("+" + Score.ToString(), FontName, FontSize, Inv.FontWeight.Heavy, Inv.Colour.Red, ScorePoint, Inv.HorizontalPosition.Center, Inv.VerticalPosition.Bottom);

        DC.DrawRectangle(null, Inv.Colour.Red, 1, Image6Rect);
        DC.DrawRectangle(null, Inv.Colour.Red, 1, Image7Rect);
        DC.DrawRectangle(null, Inv.Colour.Red, 1, Image8Rect);

        DC.DrawImage(LogoImage, Image6Rect, 1.0F, null, null, Position % 360);
        DC.DrawImage(LogoImage, Image7Rect, 1.0F, null, Inv.Mirror.Horizontal, Position % 360);
        DC.DrawImage(LogoImage, Image8Rect, 1.0F, null, Inv.Mirror.Vertical, Position % 360);
      };

      Canvas.Draw();
      Surface.ComposeEvent += () => Canvas.Draw();

      var Button = Surface.NewFlatButton();
      Overlay.AddPanel(Button);
      Button.Background.Colour = Inv.Colour.Orange;
      Button.Padding.Set(20);
      Button.Alignment.TopStretch();
      Button.SingleTapEvent += () =>
      {
        var TransitionSurface = Base.Window.NewSurface();
        Base.Window.Transition(TransitionSurface);

        var TransitionPoint = Inv.Point.Zero;

        var TransitionCanvas = TransitionSurface.NewCanvas();
        TransitionSurface.Content = TransitionCanvas;
        TransitionCanvas.Background.Colour = Inv.Colour.WhiteSmoke;
        TransitionCanvas.DrawEvent += (DC) =>
        {
          var DCWidth = TransitionSurface.Window.Width;
          var DCHeight = TransitionSurface.Window.Height;

          DC.DrawText(string.Format("{0}, {1}", TransitionPoint.X, TransitionPoint.Y), "", 20, Inv.FontWeight.Regular, Inv.Colour.Blue, new Inv.Point(DCWidth / 2, DCHeight / 2), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Center);
          //RC.DrawText(string.Format("{0}, {1}", Application.Window.Width, Application.Window.Height), "", 20, Inv.Colour.Red, new Point(RC.Width / 2, RC.Height / 2 + 50), HorizontalPosition.Left, VerticalPosition.Top);

          DC.DrawText("TOP LEFT", "", 30, Inv.FontWeight.Thin, Inv.Colour.Red, new Inv.Point(0, 0), Inv.HorizontalPosition.Left, Inv.VerticalPosition.Top);
          DC.DrawText("TOP MIDDLE", "", 30, Inv.FontWeight.Light, Inv.Colour.Green, new Inv.Point(DCWidth / 2, 0), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Top);
          DC.DrawText("TOP RIGHT", "", 30, Inv.FontWeight.Regular, Inv.Colour.Blue, new Inv.Point(DCWidth, 0), Inv.HorizontalPosition.Right, Inv.VerticalPosition.Top);

          DC.DrawText("BOT LEFT", "", 30, Inv.FontWeight.Medium, Inv.Colour.Red, new Inv.Point(0, DCHeight), Inv.HorizontalPosition.Left, Inv.VerticalPosition.Bottom);
          DC.DrawText("BOT MIDDLE", "", 30, Inv.FontWeight.Bold, Inv.Colour.Green, new Inv.Point(DCWidth / 2, DCHeight), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Bottom);
          DC.DrawText("BOT RIGHT", "", 30, Inv.FontWeight.Heavy, Inv.Colour.Blue, new Inv.Point(DCWidth, DCHeight), Inv.HorizontalPosition.Right, Inv.VerticalPosition.Bottom);
        };
        TransitionCanvas.SingleTapEvent += (Point) =>
        {
          Debug.WriteLine(string.Format("{0}, {1}", Point.X, Point.Y));

          TransitionPoint = Point;

          TransitionCanvas.Draw();
        };
      };
      return Overlay;
    }
    private Inv.Panel CustomStroke(Inv.Surface Surface)
    {
      var Canvas = Surface.NewCanvas();
      Canvas.Background.Colour = Inv.Colour.Black;

      var BorderWidth = 0;

      Canvas.DrawEvent += (RC) =>
      {
        if (BorderWidth == 3)
          throw new Exception("failing inside draw event.");

        var BackBrush = Inv.Colour.Yellow;
        var BackRect = new Inv.Rect(100, 100, 200, 200);
        RC.DrawRectangle(BackBrush, null, 0, BackRect);

        var TopLeftCornerRect = new Inv.Rect(80, 80, 20, 20);
        RC.DrawRectangle(BackBrush, null, 0, TopLeftCornerRect);

        var TopRightCornerRect = new Inv.Rect(300, 80, 20, 20);
        RC.DrawRectangle(BackBrush, null, 0, TopRightCornerRect);

        var BottomLeftCornerRect = new Inv.Rect(80, 300, 20, 20);
        RC.DrawRectangle(BackBrush, null, 0, BottomLeftCornerRect);

        var BottomRightCornerRect = new Inv.Rect(300, 300, 20, 20);
        RC.DrawRectangle(BackBrush, null, 0, BottomRightCornerRect);

        var LeftBrush = Inv.Colour.DarkRed;
        var LeftRect = new Inv.Rect(100, 100, 30, 200);
        RC.DrawRectangle(LeftBrush, Inv.Colour.Red, BorderWidth, LeftRect);

        var RightBrush = Inv.Colour.DarkBlue;
        var RightRect = new Inv.Rect(270, 100, 30, 200);
        RC.DrawRectangle(RightBrush, Inv.Colour.Blue, BorderWidth, RightRect);
      };

      Canvas.SingleTapEvent += (Point) =>
      {
        BorderWidth++;

        Canvas.Draw();
      };

      Canvas.SingleTap(new Inv.Point(0, 0));

      return Canvas;
    }
    private Inv.Panel DockMargins(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.Yellow;

      var VerticalDock = Surface.NewVerticalDock();
      Overlay.AddPanel(VerticalDock);

      var HorizontalDock = Surface.NewHorizontalDock();
      VerticalDock.AddHeader(HorizontalDock);
      HorizontalDock.Margin.Set(4);
      HorizontalDock.Background.Colour = Inv.Colour.SteelBlue;

      var Button = Surface.NewFlatButton();
      HorizontalDock.AddHeader(Button);
      Button.Size.Set(64, 64);
      Button.Background.Colour = Inv.Colour.Red;

      var Bottom = Surface.NewFrame();
      Overlay.AddPanel(Bottom);
      Bottom.Border.Set(1);
      Bottom.Border.Colour = Inv.Colour.White;
      Bottom.Background.Colour = Inv.Colour.Green;
      Bottom.Margin.Set(0, 72, 0, 0);

      return Overlay;
    }
    private Inv.Panel DockOnOverlay(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var Scroll = Surface.NewVerticalScroll();
      Overlay.AddPanel(Scroll);

      var Stack = Surface.NewVerticalStack();
      Scroll.Content = Stack;

      for (var Index = 0; Index < 1; Index++)
      {
        var IndexButton = Surface.NewFlatButton();
        Stack.AddPanel(IndexButton);
        IndexButton.Background.Colour = Inv.Colour.SteelBlue;
        IndexButton.Padding.Set(10);
        IndexButton.Margin.Set(5);

        var IndexLabel = Surface.NewLabel();
        IndexButton.Content = IndexLabel;
        IndexLabel.Alignment.Center();
        IndexLabel.Text = "INDEX " + Index;
        IndexLabel.Size.SetHeight(1000);
      }

      var LeftButton = Surface.NewFlatButton();
      Overlay.AddPanel(LeftButton);
      LeftButton.Alignment.TopLeft();
      LeftButton.Background.Colour = Inv.Colour.Orange;
      LeftButton.Size.Set(200, 100);

      var Dock = Surface.NewVerticalDock();
      Overlay.AddPanel(Dock);
      Dock.Alignment.StretchLeft();

      //var TopButton = Surface.NewFlatButton();
      //Dock.AddClient(TopButton);
      //TopButton.Background.Colour = Inv.Colour.Pink;

      var RightButton = Surface.NewFlatButton();
      Dock.AddFooter(RightButton);
      RightButton.Background.Colour = Inv.Colour.Purple;
      RightButton.Size.Set(200, 100);

      return Overlay;
    }
    private Inv.Panel EmptyLabel(Inv.Surface Surface)
    {
      var Stack = Surface.NewVerticalStack();
      Stack.Alignment.Center();

      var L1 = Surface.NewLabel();
      Stack.AddPanel(L1);
      L1.Text = "This is the first line";

      var L2 = Surface.NewLabel();
      Stack.AddPanel(L2);
      L2.Text = "This is the second line";

      var L3 = Surface.NewLabel();
      Stack.AddPanel(L3);
      L3.Text = "The next line is blank:";

      var L4 = Surface.NewLabel();
      Stack.AddPanel(L4);
      L4.Text = "";

      var L5 = Surface.NewLabel();
      Stack.AddPanel(L5);
      L5.Text = "Did you see a blank line above here?";

      return Stack;
    }
    private Inv.Panel FreezeInput(Inv.Surface Surface)
    {
      var Scroll = Surface.NewVerticalScroll();

      var Stack = Surface.NewVerticalStack();
      Scroll.Content = Stack;
      Stack.Alignment.Center();

      var L1 = Surface.NewLabel();
      Stack.AddPanel(L1);
      L1.Justify.Center();
      L1.Font.Size = 30;
      L1.LineWrapping = true;
      L1.Font.Bold();
      L1.Text = "";

      var B1 = Surface.NewFlatButton();
      Stack.AddPanel(B1);
      B1.Size.Set(300, 100);
      B1.Margin.Set(0, 20, 0, 20);
      B1.Background.Colour = Inv.Colour.Green;
      B1.IsFocusable = false;

      var E1 = Surface.NewTextEdit();
      Stack.AddPanel(E1);
      E1.Border.Set(2);
      E1.Border.Colour = Inv.Colour.Orange;
      E1.Font.Size = 30;
      E1.Text = "type?";
      E1.ChangeEvent += () =>
      {
        if (L1.Text != "")
          E1.Background.Colour = Inv.Colour.Red;
      };

      var E2 = Surface.NewPasswordEdit();
      Stack.AddPanel(E2);
      E2.Border.Set(2);
      E2.Border.Colour = Inv.Colour.Purple;
      E2.Font.Size = 30;
      E2.Text = "pwd?";
      E2.ChangeEvent += () =>
      {
        if (L1.Text != "")
          E2.Background.Colour = Inv.Colour.Red;
      };

      var E3 = Surface.NewSearchEdit();
      Stack.AddPanel(E3);
      E3.Border.Set(2);
      E3.Border.Colour = Inv.Colour.Blue;
      E3.Font.Size = 30;
      E3.Text = "search?";
      E3.ChangeEvent += () =>
      {
        if (L1.Text != "")
          E3.Background.Colour = Inv.Colour.Red;
      };

      var M1 = Surface.NewMemo();
      Stack.AddPanel(M1);
      M1.Border.Set(2);
      M1.Border.Colour = Inv.Colour.DarkTurquoise;
      M1.Font.Size = 30;
      M1.Text = "memo?";
      M1.ChangeEvent += () =>
      {
        if (L1.Text != "")
          M1.Background.Colour = Inv.Colour.Red;
      };

      var L2 = Surface.NewLabel();
      B1.Content = L2;
      L2.Font.Colour = Inv.Colour.White;
      L2.Font.Size = 20;
      L2.Font.Bold();
      L2.Justify.Center();
      L2.Text = "FREEZE INPUT";

      B1.SingleTapEvent += () =>
      {
        if (L1.Text == "")
        {
          Surface.Window.PreventInput();

          L1.Text = "STOP!\nHAMMERTIME!";

          Surface.Window.RunTask(T =>
          {
            T.Yield(TimeSpan.FromMilliseconds(5000));

            T.Post(() =>
            {
              Surface.Window.AllowInput();
              L1.Text = "";
            });
          });
        }
        else
        {
          // should not be able to tap this button.
          B1.Background.Colour = Inv.Colour.Red;
        }
      };

      return Scroll;
    }
    private Inv.Panel HelpFlyout(Inv.Surface Surface)
    {
      var Flyout = Surface.NewOverlay();

      var Memo = Surface.NewMemo();
      Flyout.AddPanel(Memo);
      Memo.Background.Colour = Inv.Colour.Black;
      Memo.Alignment.TopLeft();
      Memo.Margin.Set(50);
      Memo.Padding.Set(50);
      Memo.Font.Colour = Inv.Colour.White;
      Memo.Font.Size = 20;
      Memo.IsReadOnly = true;
      Memo.Text = "1. This is some help\n2. It is multi line\n3. The rest should be obvious\n4. Read this all again and it makes sense\n5. Last line";

      var Button = Surface.NewFlatButton();
      Flyout.AddPanel(Button);
      Button.Background.Colour = Inv.Colour.Blue;
      Button.Size.SetHeight(200);
      Button.Alignment.BottomStretch();
      Button.SingleTapEvent += () =>
      {
        Surface.Rearrange();

        var FadeOutAnimation = Surface.NewAnimation();
        FadeOutAnimation.AddTarget(Memo).FadeOpacityOut(TimeSpan.FromSeconds(1));
        FadeOutAnimation.Start();
      };

      var FadeInAnimation = Surface.NewAnimation();
      FadeInAnimation.AddTarget(Memo).FadeOpacityIn(TimeSpan.FromSeconds(1));
      FadeInAnimation.Start();
      return Flyout;
    }
    private Inv.Panel HiddenOverlays(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var Button = new ComplexButton(Surface);
      Overlay.AddPanel(Button);
      Button.Alignment.Center();

      var Label = Surface.NewFlatButton();
      Overlay.AddPanel(Label);
      Label.Background.Colour = Inv.Colour.Black;
      Label.Visibility.Collapse();

      return Overlay;
    }
    private Inv.Panel CenteredScrollingStack(Inv.Surface Surface)
    {
      var Scroll = Surface.NewVerticalScroll();
      Scroll.Background.Colour = Inv.Colour.Yellow;
      Scroll.Alignment.Center();

      var Stack = Surface.NewVerticalStack();
      Scroll.Content = Stack;
      Stack.Background.Colour = Inv.Colour.Green;
      Stack.Padding.Set(10);

      var Button = Surface.NewFlatButton();
      Stack.AddPanel(Button);
      Button.Background.Colour = Inv.Colour.DarkGray;
      //Button.Size.SetHeight(100);

      var HelloLabel = Surface.NewLabel();
      Stack.AddPanel(HelloLabel);
      HelloLabel.Text = "Hello";
      HelloLabel.Font.Size = 30;
      HelloLabel.Background.Colour = Inv.Colour.Orange;

      var WorldLabel = Surface.NewLabel();
      Stack.AddPanel(WorldLabel);
      WorldLabel.Text = "World";
      WorldLabel.Font.Size = 30;
      WorldLabel.Background.Colour = Inv.Colour.Pink;

      return Scroll;
    }
    private Inv.Panel ClippedCanvas(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();

      var Button = Surface.NewFlatButton();
      Dock.AddHeader(Button);
      Button.Background.Colour = Inv.Colour.White;
      Button.Border.Colour = Inv.Colour.DarkGray;
      Button.Border.Set(1);
      Button.Margin.Set(10);

      var Label = Surface.NewLabel();
      Button.Content = Label;
      Label.Text = "click to draw";
      Label.Font.Size = 30;
      Label.Font.Light();
      Label.Margin.Set(10);
      Label.Alignment.Center();

      var Frame = Surface.NewFrame();
      Dock.AddClient(Frame);
      Frame.Size.Set(200, 100);
      Frame.Alignment.Center();
      Frame.Background.Colour = Inv.Colour.White;

      var Canvas = Surface.NewCanvas();
      Frame.Content = Canvas;

      Canvas.DrawEvent += (DC) => DC.DrawEllipse(Inv.Colour.Pink, Inv.Colour.Transparent, 0, new Inv.Point(100, 50), new Inv.Point(100, 100));

      Button.SingleTapEvent += () => Canvas.Draw();

      return Dock;
    }
    private Inv.Panel ComplexLayout(Inv.Surface Surface)
    {
      var D1 = Surface.NewVerticalDock();

      var D2 = Surface.NewHorizontalDock();
      D1.AddHeader(D2);

      D2.AddHeader(NewLabel(Surface, "D2.H1", Inv.Colour.Blue));
      D2.AddClient(NewLabel(Surface, "D2.C1", Inv.Colour.Green));
      D2.AddFooter(NewLabel(Surface, "D2.F1", Inv.Colour.Red));

      var Scroll = Surface.NewVerticalScroll();
      D1.AddClient(Scroll);

      var S1 = Surface.NewVerticalStack();
      Scroll.Content = S1;
      S1.AddPanel(NewLabel(Surface, "S1.E1", Inv.Colour.Yellow));
      S1.AddPanel(NewLabel(Surface, "S1.E2", Inv.Colour.Gray));
      S1.AddPanel(NewLabel(Surface, "S1.E3", Inv.Colour.Turquoise));
      S1.AddPanel(NewLabel(Surface, "S1.E4", Inv.Colour.Violet));

      var S2 = Surface.NewHorizontalStack();
      D1.AddFooter(S2);
      S2.AddPanel(NewLabel(Surface, "S2.E1", Inv.Colour.Orange));
      S2.AddPanel(NewLabel(Surface, "S2.E2", Inv.Colour.OldLace));
      S2.AddPanel(NewLabel(Surface, "S2.E3", Inv.Colour.Peru));

      return D1;
    }
    private Inv.Panel ComplexMarginAndPadding(Inv.Surface Surface)
    {
      var Stack = Surface.NewVerticalStack();

      var Test1Button = new ComplexButton(Surface);
      Stack.AddPanel(Test1Button);
      Test1Button.Margin.Set(20);

      var Test2Button = new ComplexButton(Surface);
      Stack.AddPanel(Test2Button);
      Test2Button.Margin.Set(20);

      return Stack;
    }
    private Inv.Panel CornerGraphic(Inv.Surface Surface)
    {
      var Frame = Surface.NewFrame();
      Frame.Corner.Set(40);
      Frame.Size.Set(80, 80);
      Frame.Background.Colour = Inv.Colour.Red;
      Frame.Elevation.Set(5);

      var Graphic = Surface.NewGraphic();
      Frame.Content = Graphic;
      Graphic.Corner.Set(40);
      Graphic.Image = Resources.Images.Water128x128;

      return Frame;
    }
    private Inv.Panel DefaultBackground(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();
      Dock.Padding.Set(10);
      Dock.Background.Colour = Inv.Colour.Yellow;

      Dock.AddClient(Surface.NewBoard());
      Dock.AddClient(Surface.NewFlatButton());

      // WPF Browser background is white.
      // Android Browser is white and incorrectly stretches to the full height.
      // iOS Browser background is white.
      // Uwa Browser seems to be the only one that is correct.
      //var Browser = Surface.NewBrowser();
      //Dock.AddClient(Browser);
      //Browser.Background.Colour = Inv.Colour.Red;

      Dock.AddClient(Surface.NewCanvas());
      Dock.AddClient(Surface.NewFlow());
      Dock.AddClient(Surface.NewFrame());
      Dock.AddClient(Surface.NewGraphic());
      Dock.AddClient(Surface.NewHorizontalDock());

      var TextEdit = Surface.NewTextEdit();
      Dock.AddClient(TextEdit);
      TextEdit.Text = "TextEdit";
      var SearchEdit = Surface.NewSearchEdit();
      Dock.AddClient(SearchEdit);
      SearchEdit.Text = "SearchEdit";
      //SearchEdit.Background.Colour = Inv.Colour.LightGray;
      //SearchEdit.Padding.Set(5);
      //SearchEdit.Visibility.Collapse();

      var PasswordEdit = Surface.NewPasswordEdit();
      Dock.AddClient(PasswordEdit);
      PasswordEdit.Text = "PasswordEdit";
      //PasswordEdit.ChangeEvent += () => SearchEdit.Visibility.Show();

      var Label = Surface.NewLabel();
      Dock.AddClient(Label);
      Label.Text = "Label";

      var Memo = Surface.NewMemo();
      Dock.AddClient(Memo);
      Memo.Text = "Memo";

      Dock.AddClient(Surface.NewOverlay());
      Dock.AddClient(Surface.NewHorizontalScroll());
      Dock.AddClient(Surface.NewHorizontalStack());
      Dock.AddClient(Surface.NewTable());

      return Dock;
    }
    //private Inv.Panel MaterialScrollTransition(Inv.Surface Surface)
    //{
    //}
    private Inv.Panel MaterialFlyoutAlignment(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var Button = Inv.Button.NewFlat();
      Overlay.AddPanel(Button);
      Button.Background.Colour = Inv.Colour.Green;
      Button.SingleTapEvent += () =>
      {
        var Dialog = new MaterialDialog(Overlay);

        //var DialogScroll = Inv.Scroll.NewVertical();
        //Dialog.Content = DialogScroll;

        var DialogFlow = Inv.Flow.New();
        Dialog.Content = DialogFlow;

        var FlowSection = DialogFlow.AddSection();
        FlowSection.ItemQuery += (Index) =>
        {
          var DialogLabel = Inv.Label.New();
          DialogLabel.Text = Index.ToString();
          DialogLabel.Padding.Set(8);
          DialogLabel.Font.Size = 16;
          return DialogLabel;
        };

        FlowSection.SetItemCount(6);

        Dialog.Show();
      };

      return Overlay;
    }
    private Inv.Panel MaterialPropertySheet(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var Stack = Inv.Stack.NewVertical();
      Overlay.AddPanel(Stack);
      Stack.Alignment.Stretch();

      var TextField = new MaterialTextField();
      Stack.AddPanel(TextField);
      TextField.CaptionText = "Test";

      var MemoField = new MaterialMemoField();
      Stack.AddPanel(MemoField);
      MemoField.CaptionText = "Memo";

      return Overlay;
    }
    private Inv.Panel MaterialDataTable(Inv.Surface Surface)
    {
      var DessertArray = new[]
      {
        new { Name = "Frozen yogurt",      Calories = 159, Fat =  6.0m, Carbs = 24, Protein = 4.0m },
        new { Name = "Ice cream sandwich", Calories = 237, Fat =  9.0m, Carbs = 37, Protein = 4.3m },
        new { Name = "Eclair",             Calories = 262, Fat = 16.0m, Carbs = 24, Protein = 6.0m },
        new { Name = "Cupcake",            Calories = 305, Fat =  3.7m, Carbs = 67, Protein = 3.9m },
        new { Name = "Gingerbread",        Calories = 356, Fat = 16.0m, Carbs = 49, Protein = 0.0m },
        new { Name = "Jelly bean",         Calories = 375, Fat =  0.0m, Carbs = 94, Protein = 0m },
        new { Name = "Lollipop",           Calories = 392, Fat =  0.2m, Carbs = 98, Protein = 6.5m },
        new { Name = "Honeycomb",          Calories = 408, Fat =  3.2m, Carbs = 87, Protein = 4.9m },
      };

      var Frame = Inv.Frame.New();
      Frame.Background.Colour = Inv.Colour.LightGray;

      var Card = new Inv.Material.Card();
      Frame.Content = Card;
      Card.Alignment.Center();

      var DataTable = new Inv.Material.DataTable();
      Card.Content = DataTable;

      var DessertColumn = DataTable.AddStringColumn("Dessert (100g serving)", 200);
      DessertColumn.Justify.Left();
      DessertColumn.CellQuery += Index => DessertArray[Index].Name;

      var CaloriesColumn = DataTable.AddDecimalColumn("Calories", 80);
      CaloriesColumn.Justify.Right();
      CaloriesColumn.Definition = "The total amount of food energy in the given serving size.";
      CaloriesColumn.CellQuery += Index => DessertArray[Index].Calories;

      var FatColumn = DataTable.AddDecimalColumn("Fat (g)", 80);
      FatColumn.Justify.Right();
      FatColumn.CellQuery += Index => DessertArray[Index].Fat;

      var CarbsColumn = DataTable.AddDecimalColumn("Carbs (g)", 80);
      CarbsColumn.Justify.Right();
      CarbsColumn.CellQuery += Index => DessertArray[Index].Carbs;

      var ProteinColumn = DataTable.AddDecimalColumn("Protein (g)", 80);
      ProteinColumn.Justify.Right();
      ProteinColumn.CellQuery += Index => DessertArray[Index].Protein;

      DataTable.SetRowCount(DessertArray.Length);

      return Frame;
    }
    private Inv.Panel DeepNested(Inv.Surface Surface)
    {
      var Button = Surface.NewFlatButton();

      var LastFrame = (Inv.Frame)null;
      var Index = 0;

      Button.SingleTapEvent += () =>
      {
        // NOTE: testing for Android 4.x

        var Frame = Surface.NewFrame();
        Frame.Background.Colour = Index++ % 2 == 0 ? Inv.Colour.Red : Inv.Colour.Blue;
        Frame.Padding.Set(1);

        if (LastFrame == null)
          Button.Content = Frame;
        else
          LastFrame.Content = Frame;

        LastFrame = Frame;
      };

      Button.SingleTap();
      Button.SingleTap();
      Button.SingleTap();
      Button.SingleTap();

      return Button;
    }
    private Inv.Panel VideoLaunch(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var Video = Surface.NewVideo();
      Overlay.AddPanel(Video);
      Video.Background.Colour = Inv.Colour.DodgerBlue.Darken(0.50F);
      Video.SetSourceAsset(Surface.Window.Application.Directory.NewAsset("launch.mp4"));

      var HeaderFrame = Surface.NewFrame();
      Overlay.AddPanel(HeaderFrame);
      HeaderFrame.Alignment.TopStretch();
      HeaderFrame.Background.Colour = Inv.Colour.White.Opacity(0.50F);
      HeaderFrame.Corner.Set(0, 0, 0, 0);
      HeaderFrame.Size.SetHeight(64);
      //HeaderFrame.Elevation.Set(4);

      var HeaderLabel = Surface.NewLabel();
      HeaderFrame.Content = HeaderLabel;
      HeaderLabel.Alignment.Center();
      HeaderLabel.Font.ExtraMassive();
      HeaderLabel.Font.Thin();
      HeaderLabel.Font.Colour = Inv.Colour.DodgerBlue.Darken(0.50F);
      HeaderLabel.Text = "Phoenix Mobile";

      var FooterFrame = Surface.NewFrame();
      Overlay.AddPanel(FooterFrame);
      FooterFrame.Alignment.BottomStretch();
      FooterFrame.Background.Colour = Inv.Colour.White.Opacity(0.75F);
      FooterFrame.Corner.Set(0, 0, 0, 0);
      FooterFrame.Size.SetHeight(32);

      var FooterLabel = Surface.NewLabel();
      FooterFrame.Content = FooterLabel;
      FooterLabel.Alignment.Center();
      FooterLabel.Font.Size = 16;
      FooterLabel.Font.Colour = Inv.Colour.DodgerBlue.Darken(0.50F);
      FooterLabel.Text = "Copyright © " + Inv.Date.Now.Year + " Kestral";

      var Graphic = Surface.NewGraphic();
      Overlay.AddPanel(Graphic);
      //Graphic.Background.Colour = Inv.Colour.White;
      //Graphic.Corner.Set(128);
      //Graphic.Padding.Set(16);
      Graphic.Alignment.Center();
      Graphic.Size.Set(256, 256);
      Graphic.Image = Resources.Images.PhoenixLogo500x500;
      //Graphic.Opacity.Set(0);
      Graphic.Visibility.Collapse();

      var Button = Surface.NewFlatButton();
      Overlay.AddPanel(Button);
      Button.Alignment.BottomCenter();
      Button.Margin.Set(0, 0, 0, 128);
      Button.Border.Set(4);
      Button.Border.Colour = Inv.Colour.DodgerBlue.Darken(0.50F);
      Button.Background.Colour = Inv.Colour.DodgerBlue.Opacity(0.75F);
      Button.Corner.Set(4);
      Button.Size.Set(256, 64);
      Button.Visibility.Collapse();

      var Label = Surface.NewLabel();
      Button.Content = Label;
      Label.Font.Size = 24;
      Label.Font.Colour = Inv.Colour.White;
      //Label.Font.Thin();
      Label.Justify.Center();
      Label.Text = "CONNECT";

      // TODO: ROTATE, TRANSLATE, SCALE ANIMATIONS ARE NOT YET WORKING ON ANDROID/IOS.

      var HalfDuration = TimeSpan.FromMilliseconds(500);
      var FullDuration = TimeSpan.FromMilliseconds(1000);
      var Animation = Surface.NewAnimation();
      var GraphicTarget = Animation.AddTarget(Graphic);
      GraphicTarget.FadeOpacityIn(FullDuration);
      //GraphicTarget.RotateAngle(0, 10, HalfDuration);
      //GraphicTarget.RotateAngle(10, 0, HalfDuration, HalfDuration);
      //GraphicTarget.TranslateY(-64, 0, FullDuration);
      //GraphicTarget.ScaleSize(0.75F, 1.0F, FullDuration);

      var ButtonTarget = Animation.AddTarget(Button);
      ButtonTarget.FadeOpacityIn(FullDuration);
      //ButtonTarget.TranslateY(64, 0, FullDuration);

      Video.Play();

      void Animate()
      {
        Graphic.Visibility.Show();
        Button.Visibility.Show();

        Animation.Restart();
      }

      Surface.Window.Post(() => Animate());

      Button.SingleTapEvent += () => Animate();

      return Overlay;
    }
    private Inv.Panel VideoPlayback(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var Video = Surface.NewVideo();
      Overlay.AddPanel(Video);
      Video.Background.Colour = Inv.Colour.SteelBlue.Darken(0.50F);

      var VideoAsset = Surface.Window.Application.Directory.NewAsset("big_buck_bunny.mp4");
      var VideoFile = Surface.Window.Application.Directory.RootFolder.NewFile("big_buck_bunny.mp4");
      var VideoUri = new Uri(@"http://clips.vorwaerts-gmbh.de/VfE_html5.mp4");

      VideoAsset.Copy(VideoFile);

      var ButtonStack = Surface.NewVerticalStack();
      Overlay.AddPanel(ButtonStack);
      ButtonStack.Alignment.BottomCenter();

      var AssetButton = new CaptionButton(Surface);
      ButtonStack.AddPanel(AssetButton);
      AssetButton.Padding.Set(5);
      AssetButton.Text = "ASSET";
      AssetButton.SingleTapEvent += () => Video.SetSourceAsset(VideoAsset);

      var FileButton = new CaptionButton(Surface);
      ButtonStack.AddPanel(FileButton);
      FileButton.Padding.Set(5);
      FileButton.Text = "FILE";
      FileButton.SingleTapEvent += () => Video.SetSourceFile(VideoFile);

      var UriButton = new CaptionButton(Surface);
      ButtonStack.AddPanel(UriButton);
      UriButton.Padding.Set(5);
      UriButton.Text = "URI";
      UriButton.SingleTapEvent += () => Video.SetSourceUri(VideoUri);

      var PauseButton = Surface.NewFlatButton();
      Overlay.AddPanel(PauseButton);
      PauseButton.Alignment.BottomLeft();
      PauseButton.Size.Set(96, 96);
      PauseButton.Background.Colour = Inv.Colour.DodgerBlue;
      PauseButton.SingleTapEvent += () =>
      {
        if (Video.IsPlaying)
          Video.Pause();
        else
          Video.Play();
      };

      var StopButton = Surface.NewFlatButton();
      Overlay.AddPanel(StopButton);
      StopButton.Alignment.BottomRight();
      StopButton.Size.Set(96, 96);
      StopButton.Background.Colour = Inv.Colour.DarkRed;
      StopButton.SingleTapEvent += () =>
      {
        Video.Stop();
      };

      AssetButton.SingleTap();

      return Overlay;
    }
    private Inv.Panel TestDropbox(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();
      // TODO: Inv.Dropbox.Test();
      return Dock;
    }
    private Inv.Panel KeyModifiers(Inv.Surface Surface)
    {
      var Result = Surface.NewLabel();
      Result.Alignment.Center();
      Result.Font.Size = 30;
      Result.Text = "Press and hold ctrl, alt or shift keys.";

      Surface.Window.KeyModifierEvent += () =>
      {
        Result.Text = Surface.Window.KeyModifier.ToString();
      };

      return Result;
    }
    private Inv.Panel BasicMarginAndPadding(Inv.Surface Surface)
    {
      var Button = Surface.NewFlatButton();
      Button.Alignment.Center();
      Button.Background.Colour = Inv.Colour.Red;
      Button.Size.Set(100, 100);
      Button.Margin.Set(10);
      Button.Padding.Set(20, 0, 0, 0);
      Button.SingleTapEvent += () => Surface.Rearrange();

      var Label = Surface.NewLabel();
      Button.Content = Label;
      Label.Padding.Set(10);
      Label.Alignment.Center();
      Label.Background.Colour = Inv.Colour.Green;
      Label.Font.Colour = Inv.Colour.White;
      Label.LineWrapping = true;
      Label.Text = "Hello Mr Wordwrap";
      return Button;
    }
    private Inv.Panel ExchangeItems(Inv.Surface Surface)
    {
      var LayoutScroll = Surface.NewVerticalScroll();
      LayoutScroll.Alignment.TopStretch();
      LayoutScroll.Size.SetHeight(305);
      LayoutScroll.Background.Colour = Inv.Colour.SteelBlue;

      var LayoutStack = Surface.NewVerticalStack();
      LayoutScroll.Content = LayoutStack;
      LayoutStack.Margin.Set(10);

      for (var i = 0; i < 10; i++)
      {
        var ItemButton = new CaptionButton(Surface);
        LayoutStack.AddPanel(ItemButton);
        ItemButton.Margin.Set(5);
        ItemButton.Padding.Set(5);
        ItemButton.Text = "Item#" + i;
      }

      return LayoutScroll;
      
      /*
      var ItemPanel = new TilePanel(Surface);
      ItemPanel.Caption = "EXCHANGE";
      ItemPanel.Alignment.BottomStretch();
      ItemPanel.Size.SetHeight(305);
      for (var i = 0; i < 10; i++)
        ItemPanel.AddButton().Set(null, "Item #" + i, null);
      return ItemPanel;
      */
      /*
      var ExchangePanel = new ExchangePanel(Surface);

      ExchangePanel.LeftPanel.Caption = "LEFT";
      ExchangePanel.RightPanel.Caption = "RIGHT";
      ExchangePanel.CommandButton.Text = "SWAP";
      ExchangePanel.LeftButton.Text = "INCLUDE";
      ExchangePanel.RightButton.Text = "EXCLUDE";
      ExchangePanel.Compose(400);
      ExchangePanel.Alignment.BottomStretch();

      for (var i = 0; i < 10; i++)
      {
        ExchangePanel.LeftPanel.AddButton().Set(null, "Lefty #" + i, null);
        ExchangePanel.RightPanel.AddButton().Set(null, "Right #" + i, null);
      }

      return ExchangePanel;*/
    }
    private Inv.Panel FlowBasic(Inv.Surface Surface)
    {
      var Flow = Surface.NewFlow();
      Flow.Background.Colour = Inv.Colour.Purple;

      var Section = Flow.AddSection();
      Section.ItemQuery += (Item) =>
      {
        Debug.WriteLine("FETCH ITEM: " + Item);

        var Label = Surface.NewLabel();
        Label.Background.Colour = Inv.Colour.LightSteelBlue;
        Label.Text = string.Format("{0:N0} - {1:T}", Item, DateTimeOffset.Now);
        return Label;
      };
      Section.RecycleEvent += (Item, Panel) =>
      {
        Debug.WriteLine("RECYCLE ITEM: " + Item);
      };
      Section.SetItemCount(500);

      return Flow;
    }
    private Inv.Panel FlowAlter(Inv.Surface Surface)
    {
      var ItemCount = 1;

      var Stack = Surface.NewVerticalStack();
      Stack.Background.Colour = Inv.Colour.WhiteSmoke;

      var Button = Surface.NewFlatButton();
      Stack.AddPanel(Button);
      Button.Background.Colour = Inv.Colour.SteelBlue;
      Button.Padding.Set(10);
      Button.Margin.Set(10);

      var Label = Surface.NewLabel();
      Button.Content = Label;
      Label.Text = "Blah";

      var Flow = Surface.NewFlow();
      Stack.AddPanel(Flow);
      Flow.Background.Colour = Inv.Colour.HotPink;

      var Section = Flow.AddSection();

      var HeaderLabel = Surface.NewLabel();
      Section.SetHeader(HeaderLabel);
      HeaderLabel.Text = "HEADER LABEL!";
      HeaderLabel.Font.Size = 30;

      Section.ItemQuery += (Item) =>
      {
        var ItemLabel = Surface.NewLabel();
        ItemLabel.Text = string.Format("{0:N0} - {1:T}", Item, DateTimeOffset.Now);
        ItemLabel.Background.Colour = Inv.Colour.LightSteelBlue;
        return ItemLabel;
      };
      Section.SetItemCount(ItemCount);

      Button.SingleTapEvent += () =>
      {
        ItemCount = (ItemCount + 1) % 3;
        Section.SetItemCount(ItemCount);
        Section.Reload();
      };
      Button.ContextTapEvent += () =>
      {
        HeaderLabel.Text += "!";
      };

      return Stack;
    }
    private Inv.Panel FlowPaged(Inv.Surface Surface)
    {
      var Flow = Surface.NewFlow();
      Flow.Background.Colour = Inv.Colour.Purple;

      var Random = new Random();

      var Section = Flow.AddPagedSection<string>(20);
      Section.Template(() =>
      {
        var Result = Inv.Label.New();
        Result.Background.Colour = Inv.Colour.Red;
        Result.Font.Large();
        Result.Padding.Set(5);
        return Result;
      }, (Tile, Item) =>
      {
        Tile.Text = Item;

        if (Item == null)
          Tile.Background.Colour = Inv.Colour.Blue;
        else
          Tile.Background.Colour = int.Parse(Item.Substring("Item ".Length)) % 2 == 0 ? Inv.Colour.SteelBlue : Inv.Colour.AliceBlue;
      });
      Section.RequestEvent += (StartIndex, EndIndex, Return) =>
      {
        Debug.WriteLine($"REQUEST {StartIndex} - {EndIndex}");

        Surface.Window.RunTask(Thread =>
        {
          Thread.Yield(TimeSpan.FromMilliseconds(Random.Next(50, 1000)));

          var Result = new string[EndIndex - StartIndex + 1];

          for (var CurrentIndex = StartIndex; CurrentIndex <= EndIndex; CurrentIndex++)
            Result[CurrentIndex - StartIndex] = "Item " + CurrentIndex;

          Thread.Post(() => Return(Result));
        });
      };
      Section.Load(1000);

      return Flow;
    }
    private Inv.Panel FlowSearch(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();
      Dock.Background.Colour = Inv.Colour.DodgerBlue.Darken(0.50F);

      var Button = Surface.NewFlatButton();
      Dock.AddClient(Button);
      Button.Size.Set(400, 400);
      Button.Background.Colour = Inv.Colour.ForestGreen;
      Button.SingleTapEvent += () =>
      {
        var Search = Surface.NewSearchEdit();
        Dock.AddHeader(Search);
        Search.Font.Size = 30;
        Search.Font.Colour = Inv.Colour.White;
        Search.Background.Colour = Inv.Colour.Black;
        //Search.Text = "HELLO WORLD";
      };

      var Label = Surface.NewLabel();
      Button.Content = Label;
      Label.Font.Size = 20;
      Label.Font.Colour = Inv.Colour.White;
      Label.Text = "Tap this button to create a search edit at the top. This should NOT focus the search edit and open the soft keyboard.";

      return Dock;
    }
    private Inv.Panel FlowTiles(Inv.Surface Surface)
    {
      var Frame = Surface.NewFrame();
      //Frame.Background.Colour = Inv.Colour.White;
      //Frame.Background.Colour = Inv.Colour.LightGray;
      Frame.Background.Colour = Inv.Colour.Black;

      var Flow = Surface.NewFlow();
      Frame.Content = Flow;

      var ActionSection = Flow.AddSection();
      {
        var Button = Surface.NewFlatButton();
        Button.Background.Colour = Inv.Colour.DodgerBlue;
        Button.SingleTapEvent += () => Flow.Reload();

        var Label = Surface.NewLabel();
        Button.Content = Label;
        Label.Text = "Action section";
        Label.Font.Bold();
        Label.Justify.Right();
        Label.Font.Colour = Inv.Colour.White;
        Label.Alignment.Stretch();
        ActionSection.SetHeader(Button);
      }

      var EmptySection = Flow.AddSection();
      {
        var Label = Surface.NewLabel();
        Label.Text = "Empty section";
        Label.Font.Bold();
        Label.Justify.Right();
        Label.Font.Colour = Inv.Colour.White;
        Label.Background.Colour = Inv.Colour.SteelBlue;
        Label.Alignment.Stretch();
        EmptySection.SetHeader(Label);
      }

      var MySection = Flow.AddSection();
      {
        var Label = Surface.NewLabel();
        Label.Text = "My section";
        Label.Font.Bold();
        Label.Justify.Right();
        Label.Font.Colour = Inv.Colour.White;
        Label.Background.Colour = Inv.Colour.Orange;
        Label.Alignment.Stretch();
        MySection.SetHeader(Label);
      }

      {
        var Label = Surface.NewLabel();
        Label.Text = "Total items: " + MySection.ItemCount;
        Label.Font.Bold();
        Label.Justify.Left();
        Label.Font.Colour = Inv.Colour.White;
        Label.Background.Colour = Inv.Colour.Orange;
        Label.Alignment.Stretch();
        MySection.SetFooter(Label);
      }

      MySection.ItemQuery += (Item) =>
      {
        var Button = Surface.NewFlatButton();
        Button.Background.Colour = Item % 2 == 0 ? Inv.Colour.DimGray : Inv.Colour.DarkGray;

        var Label = Surface.NewLabel();
        Button.Content = Label;
        Label.Text = Item.ToString();
        Label.Font.Colour = Inv.Colour.White;
        Label.Font.Size = 6 + Item;

        return Button;
      };
      MySection.SetItemCount(200);

      var LastSection = Flow.AddSection();
      {
        var Label = Surface.NewLabel();
        Label.Text = "Footer only section";
        Label.Font.Bold();
        Label.Justify.Right();
        Label.Font.Colour = Inv.Colour.White;
        Label.Background.Colour = Inv.Colour.HotPink;
        Label.Alignment.Stretch();
        LastSection.SetFooter(Label);
      }
      LastSection.ItemQuery += (Item) =>
      {
        var Button = Surface.NewFlatButton();
        Button.Background.Colour = Inv.Colour.DimGray;

        var Label = Surface.NewLabel();
        Button.Content = Label;
        Label.Text = Item.ToString();
        Label.Font.Colour = Inv.Colour.White;

        return Button;
      };
      LastSection.SetItemCount(1);

      var Random = new Random();

      Flow.RefreshEvent += (Refresh) =>
      {
        Surface.Window.RunTask((Thread) =>
        {
          Thread.Yield(TimeSpan.FromSeconds(2));
          Thread.Post(() =>
          {
            MySection.SetItemCount(Random.Next(100) + 1);

            Refresh.Complete();
          });
        });
      };

      return Frame;
    }
    private Inv.Panel LayoutBoard(Inv.Surface Surface)
    {
      var Board = Surface.NewBoard();
      Board.Background.Colour = Inv.Colour.Black;

      var Header1Label = Surface.NewLabel();
      Board.AddPin(Header1Label, new Inv.Rect(100, 100, 200, 200));
      Header1Label.Background.Colour = Inv.Colour.Blue;
      Header1Label.Justify.Center();
      Header1Label.Font.Colour = Inv.Colour.Red;
      Header1Label.Font.Size = 30;
      Header1Label.Padding.Set(20);
      Header1Label.Margin.Set(10);
      Header1Label.Text = "Header1";
      return Board;
    }
    private Inv.Panel LayoutDock(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();
      //Dock.Alignment.Center();
      //Dock.Padding.Set(1);

      const int HeadingFontSize = 30;

      var Header1Label = Surface.NewLabel();
      Dock.AddHeader(Header1Label);
      //Header1Label.Alignment.Center();
      Header1Label.Background.Colour = Inv.Colour.Blue;
      Header1Label.Justify.Center();
      Header1Label.Font.Colour = Inv.Colour.Red;
      Header1Label.Font.Size = HeadingFontSize;
      Header1Label.Padding.Set(20);
      Header1Label.Margin.Set(10);
      Header1Label.Text = "Header1";
      //Header1Label.Visibility.Collapse();

      var Header2Label = Surface.NewLabel();
      Dock.AddHeader(Header2Label);
      Header2Label.Background.Colour = Inv.Colour.SteelBlue;
      Header2Label.Justify.Center();
      Header2Label.Font.Colour = Inv.Colour.DarkBlue;
      Header2Label.Font.Size = HeadingFontSize;
      Header2Label.Padding.Set(10);
      Header2Label.Text = "Header2";

      var Client1Label = Surface.NewLabel();
      Dock.AddClient(Client1Label);
      Client1Label.Background.Colour = Inv.Colour.Pink;
      Client1Label.Justify.Center();
      Client1Label.Font.Colour = Inv.Colour.DarkRed;
      Client1Label.Font.Size = HeadingFontSize;
      Client1Label.Text = "Client1";

      var Client2Label = Surface.NewLabel();
      Dock.AddClient(Client2Label);
      Client2Label.Background.Colour = Inv.Colour.WhiteSmoke;
      Client2Label.Justify.Center();
      Client2Label.Font.Colour = Inv.Colour.BlueViolet;
      Client2Label.Font.Size = HeadingFontSize;
      Client2Label.Text = "Client2";
      Client2Label.Visibility.Collapse();

      var Footer1Label = Surface.NewLabel();
      Dock.AddFooter(Footer1Label);
      Footer1Label.Background.Colour = Inv.Colour.Yellow;
      Footer1Label.Justify.Center();
      Footer1Label.Font.Colour = Inv.Colour.Purple;
      Footer1Label.Font.Size = HeadingFontSize;
      Footer1Label.Padding.Set(10);
      Footer1Label.Text = "Footer1";
      //Footer1Label.Visibility.Collapse();

      var Footer2Label = Surface.NewLabel();
      Dock.AddFooter(Footer2Label);
      Footer2Label.Background.Colour = Inv.Colour.DarkGreen;
      Footer2Label.Justify.Center();
      Footer2Label.Font.Colour = Inv.Colour.White;
      Footer2Label.Font.Size = HeadingFontSize;
      Footer2Label.Padding.Set(20);
      Footer2Label.Text = "Footer2";

      var ToggleButton = Surface.NewFlatButton();
      Dock.AddFooter(ToggleButton);
      ToggleButton.Background.Colour = Inv.Colour.Purple;
      ToggleButton.Size.Set(100, 100);
      ToggleButton.SingleTapEvent += () =>
      {
        if (Dock.IsHorizontal)
          Dock.SetVertical();
        else
          Dock.SetHorizontal();
      };

      return Dock;
    }
    private Inv.Panel LayoutStack(Inv.Surface Surface)
    {
      var Stack = Surface.NewVerticalStack();
      //Dock.Alignment.Center();
      //Dock.Padding.Set(1);

      const int HeadingFontSize = 30;

      var Header1Label = Surface.NewLabel();
      Stack.AddPanel(Header1Label);
      //Header1Label.Alignment.Center();
      Header1Label.Background.Colour = Inv.Colour.Blue;
      Header1Label.Justify.Center();
      Header1Label.Font.Colour = Inv.Colour.Red;
      Header1Label.Font.Size = HeadingFontSize;
      Header1Label.Padding.Set(20);
      Header1Label.Margin.Set(10);
      Header1Label.Text = "Header1";
      //Header1Label.Visibility.Collapse();

      var Header2Label = Surface.NewLabel();
      Stack.AddPanel(Header2Label);
      Header2Label.Background.Colour = Inv.Colour.SteelBlue;
      Header2Label.Justify.Center();
      Header2Label.Font.Colour = Inv.Colour.DarkBlue;
      Header2Label.Font.Size = HeadingFontSize;
      Header2Label.Padding.Set(10);
      Header2Label.Text = "Header2";

      var Client1Label = Surface.NewLabel();
      Stack.AddPanel(Client1Label);
      Client1Label.Background.Colour = Inv.Colour.Pink;
      Client1Label.Justify.Center();
      Client1Label.Font.Colour = Inv.Colour.DarkRed;
      Client1Label.Font.Size = HeadingFontSize;
      Client1Label.Text = "Client1";

      var Client2Label = Surface.NewLabel();
      Stack.AddPanel(Client2Label);
      Client2Label.Background.Colour = Inv.Colour.WhiteSmoke;
      Client2Label.Justify.Center();
      Client2Label.Font.Colour = Inv.Colour.BlueViolet;
      Client2Label.Font.Size = HeadingFontSize;
      Client2Label.Text = "Client2";

      var Footer1Label = Surface.NewLabel();
      Stack.AddPanel(Footer1Label);
      Footer1Label.Background.Colour = Inv.Colour.Yellow;
      Footer1Label.Justify.Center();
      Footer1Label.Font.Colour = Inv.Colour.Purple;
      Footer1Label.Font.Size = HeadingFontSize;
      Footer1Label.Padding.Set(10);
      Footer1Label.Text = "Footer1";
      //Footer1Label.Visibility.Collapse();

      var Footer2Label = Surface.NewLabel();
      Stack.AddPanel(Footer2Label);
      Footer2Label.Background.Colour = Inv.Colour.DarkGreen;
      Footer2Label.Justify.Center();
      Footer2Label.Font.Colour = Inv.Colour.White;
      Footer2Label.Font.Size = HeadingFontSize;
      Footer2Label.Padding.Set(20);
      Footer2Label.Text = "Footer2";

      var ToggleButton = Surface.NewFlatButton();
      Stack.AddPanel(ToggleButton);
      ToggleButton.Background.Colour = Inv.Colour.Purple;
      ToggleButton.Size.Set(100, 100);
      ToggleButton.SingleTapEvent += () =>
      {
        if (Stack.IsHorizontal)
          Stack.SetVertical();
        else
          Stack.SetHorizontal();
      };

      return Stack;
    }
    private Inv.Panel LayoutScroll(Inv.Surface Surface)
    {
      var Scroll = Surface.NewVerticalScroll();

      var Stack = Surface.NewVerticalStack();
      Scroll.Content = Stack;
      //Dock.Alignment.Center();
      //Dock.Padding.Set(1);

      const int HeadingFontSize = 30;

      var Header1Label = Surface.NewLabel();
      Stack.AddPanel(Header1Label);
      //Header1Label.Alignment.Center();
      Header1Label.Background.Colour = Inv.Colour.Blue;
      Header1Label.Justify.Center();
      Header1Label.Font.Colour = Inv.Colour.Red;
      Header1Label.Font.Size = HeadingFontSize;
      Header1Label.Padding.Set(20);
      Header1Label.Margin.Set(10);
      Header1Label.Text = "Header1";
      //Header1Label.Visibility.Collapse();

      var Header2Label = Surface.NewLabel();
      Stack.AddPanel(Header2Label);
      Header2Label.Background.Colour = Inv.Colour.SteelBlue;
      Header2Label.Justify.Center();
      Header2Label.Font.Colour = Inv.Colour.DarkBlue;
      Header2Label.Font.Size = HeadingFontSize;
      Header2Label.Padding.Set(10);
      Header2Label.Text = "Header2";

      var Header3Label = Surface.NewLabel();
      Stack.AddPanel(Header3Label);
      Header3Label.Background.Colour = Inv.Colour.HotPink;
      Header3Label.Justify.Center();
      Header3Label.Font.Colour = Inv.Colour.White;
      Header3Label.Font.Size = HeadingFontSize;
      Header3Label.Padding.Set(10);
      Header3Label.Text = "Header3";

      var Client1Label = Surface.NewLabel();
      Stack.AddPanel(Client1Label);
      Client1Label.Background.Colour = Inv.Colour.Pink;
      Client1Label.Justify.Center();
      Client1Label.Font.Colour = Inv.Colour.DarkRed;
      Client1Label.Font.Size = HeadingFontSize;
      Client1Label.Text = "Client1";

      var Client2Label = Surface.NewLabel();
      Stack.AddPanel(Client2Label);
      Client2Label.Background.Colour = Inv.Colour.WhiteSmoke;
      Client2Label.Justify.Center();
      Client2Label.Font.Colour = Inv.Colour.BlueViolet;
      Client2Label.Font.Size = HeadingFontSize;
      Client2Label.Text = "Client2";

      var Footer1Label = Surface.NewLabel();
      Stack.AddPanel(Footer1Label);
      Footer1Label.Background.Colour = Inv.Colour.Yellow;
      Footer1Label.Justify.Center();
      Footer1Label.Font.Colour = Inv.Colour.Purple;
      Footer1Label.Font.Size = HeadingFontSize;
      Footer1Label.Padding.Set(10);
      Footer1Label.Text = "Footer1";
      //Footer1Label.Visibility.Collapse();

      var Footer2Label = Surface.NewLabel();
      Stack.AddPanel(Footer2Label);
      Footer2Label.Background.Colour = Inv.Colour.DarkGreen;
      Footer2Label.Justify.Center();
      Footer2Label.Font.Colour = Inv.Colour.White;
      Footer2Label.Font.Size = HeadingFontSize;
      Footer2Label.Padding.Set(20);
      Footer2Label.Text = "Footer2";

      var Footer3Label = Surface.NewLabel();
      Stack.AddPanel(Footer3Label);
      Footer3Label.Background.Colour = Inv.Colour.Orange;
      Footer3Label.Justify.Center();
      Footer3Label.Font.Colour = Inv.Colour.White;
      Footer3Label.Font.Size = HeadingFontSize;
      Footer3Label.Padding.Set(30);
      Footer3Label.Text = "Footer3";

      var ToggleButton = Surface.NewFlatButton();
      Stack.AddPanel(ToggleButton);
      ToggleButton.Background.Colour = Inv.Colour.Purple;
      ToggleButton.Size.Set(100, 100);
      ToggleButton.SingleTapEvent += () =>
      {
        if (Stack.IsHorizontal)
          Stack.SetVertical();
        else
          Stack.SetHorizontal();

        Scroll.SetOrientation(Stack.Orientation);
      };

      return Scroll;
    }
    private Inv.Panel LabelDockLayout(Inv.Surface Surface)
    {
      var HorizontalDock = Surface.NewHorizontalDock();
      HorizontalDock.Background.Colour = Inv.Colour.DimGray;
      HorizontalDock.Alignment.TopStretch();

      var IconGraphic = Surface.NewGraphic();
      HorizontalDock.AddHeader(IconGraphic);
      IconGraphic.Margin.Set(0, 0, 4, 0);

      var VerticalStack = Surface.NewVerticalStack();
      HorizontalDock.AddClient(VerticalStack);
      VerticalStack.Alignment.CenterStretch();

      var DescriptionLabel = Surface.NewLabel();
      VerticalStack.AddPanel(DescriptionLabel);
      DescriptionLabel.Font.Colour = Inv.Colour.WhiteSmoke;
      DescriptionLabel.Text = "This is a long trailer text that should wrap over to the next line. Let's see what happens when we add even more text to this paragraph. Perhaps a third line? Or a fourth?";

      return HorizontalDock;
    }
    private Inv.Panel LabelWordwrap(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();
      Dock.Background.Colour = Inv.Colour.White;
      Dock.Padding.Set(0, 24, 0, 0);
      Dock.Alignment.Center();
      Dock.Elevation.Set(24);
      //Dock.Size.SetWidth(300);

      var TitleLabel = Surface.NewLabel();
      Dock.AddHeader(TitleLabel);
      TitleLabel.Padding.Set(24, 0, 24, 20);
      TitleLabel.Justify.Left();
      TitleLabel.Font.Colour = Inv.Colour.Black;
      TitleLabel.Font.Size = 20;
      TitleLabel.Font.Medium();
      TitleLabel.Text = "Use Google's location service?";

      var ContentFrame = Surface.NewFrame();
      Dock.AddClient(ContentFrame);

      var ContentLabel = Surface.NewLabel();
      ContentFrame.Content = ContentLabel;
      ContentLabel.Justify.Left();
      ContentLabel.Font.Colour = Inv.Colour.DimGray;
      ContentLabel.Font.Size = 16;
      ContentLabel.Padding.Set(24, 0, 24, 24);
      ContentLabel.Text = "Let Google help apps determine location.  This means sending anonymous location data to Google, even when no apps are running.";

      var ActionDock = Surface.NewHorizontalDock();
      Dock.AddFooter(ActionDock);
      ActionDock.Margin.Set(24, 4, 4, 4);

      return Dock;
    }
    private Inv.Panel LayeredCanvas(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var Position = 0;
      var Reverse = false;

      var BackgroundCanvas = Surface.NewCanvas();
      Overlay.AddPanel(BackgroundCanvas);
      BackgroundCanvas.DrawEvent += (DC) =>
      {
        var DCWidth = Surface.Window.Width;
        var DCHeight = Surface.Window.Height;

        if (Position <= 0)
          Reverse = false;
        else if (Position >= DCWidth)
          Reverse = true;

        if (Reverse)
          Position--;
        else
          Position++;

        var Opacity = Position / 100.0F;
        if (Opacity > 1.0F)
          Opacity = 1.0F;

        DC.DrawEllipse(Inv.Colour.Pink.Opacity(Opacity), Inv.Colour.Transparent, 0, new Inv.Point(Position, 150), new Inv.Point(100, 100));
        DC.DrawText(Surface.Window.DisplayRate.PerSecond.ToString() + " fps", "", 20, Inv.FontWeight.Regular, Inv.Colour.Black, new Inv.Point(DCWidth - 2, DCHeight - 2), Inv.HorizontalPosition.Right, Inv.VerticalPosition.Bottom);
      };

      var ForegroundCanvas = Surface.NewCanvas();
      Overlay.AddPanel(ForegroundCanvas);
      ForegroundCanvas.DrawEvent += (DC) =>
      {
        DC.DrawEllipse(Inv.Colour.Purple.Opacity(0.50F), Inv.Colour.Transparent, 0, new Inv.Point(300, 200), new Inv.Point(100, 100));
      };
      ForegroundCanvas.Draw();

      Surface.ComposeEvent += () =>
      {
        BackgroundCanvas.Draw();
      };

      return Overlay;
    }
    private Inv.Panel LetterQuery(Inv.Surface Surface)
    {
      var Scroll = Surface.NewVerticalScroll();
      Scroll.Padding.Set(20);
      Scroll.Size.SetWidth(Math.Min(500, Surface.Window.Width - 10));

      var Label = Surface.NewLabel();
      Scroll.Content = Label;
      Label.Font.Colour = Inv.Colour.White;
      Label.Margin.Set(0);
      Label.Font.Size = 24;
      Label.Text =
        "Aaaa Aaaaaaaaaa" + Environment.NewLine + Environment.NewLine +
        "Aaaaaaaaaaaaaaa aa aaaaaa aa aa aaa aaaaa aaaaa aaa aaaaaaaaa aaaa aaaaaaa" + Environment.NewLine + Environment.NewLine +
        "aa aaaaaaaa aaaa aaaa aaa aaaa aa aaaaae aaa aaaaaan aa aaa aaaaa aaaaa" + Environment.NewLine + Environment.NewLine +
        "A aaaa aaa aaa aaaaaaaa aaaa aaaaaaaaaa aaa aaaaaa aaaa aaaa aa aaaaaaa aa aaaa aaaa aaaaaaaa aaa bbbbbbbbbbb" + Environment.NewLine + Environment.NewLine +
        "- Cccccc Cccccccc" + Environment.NewLine;

      return Scroll;
    }
    private Inv.Panel Login(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.White;

      var TitleLabel = Surface.NewLabel();
      Overlay.AddPanel(TitleLabel);
      TitleLabel.Alignment.TopLeft();
      TitleLabel.Margin.Set(10);
      TitleLabel.Font.Thin();
      TitleLabel.Font.Size = 60;
      TitleLabel.Text = "HQ";

      var VersionLabel = Surface.NewLabel();
      Overlay.AddPanel(VersionLabel);
      VersionLabel.Alignment.TopRight();
      VersionLabel.Font.Colour = Inv.Colour.DimGray;
      VersionLabel.Text = "1.0.0.1";

      var CopyrightLabel = Surface.NewLabel();
      Overlay.AddPanel(CopyrightLabel);
      CopyrightLabel.Alignment.BottomCenter();
      CopyrightLabel.Font.Colour = Inv.Colour.DimGray;
      CopyrightLabel.Text = "Copyright © " + DateTime.Today.Year + " Kestral. All rights reserved.";

      var Scroll = Surface.NewVerticalScroll();
      Overlay.AddPanel(Scroll);

      var Dock = Surface.NewVerticalDock();
      Scroll.Content = Dock;
      Dock.Margin.Set(10);
      Dock.Alignment.Center();
      Dock.Size.SetWidth(300);

      var DomainLabel = Surface.NewLabel();
      Dock.AddHeader(DomainLabel);
      DomainLabel.Font.Colour = Inv.Colour.DimGray;
      DomainLabel.Text = "domain";

      var DomainEdit = Surface.NewTextEdit();
      Dock.AddHeader(DomainEdit);
      DomainEdit.Background.Colour = Inv.Colour.WhiteSmoke;
      DomainEdit.Border.Set(2);
      DomainEdit.Border.Colour = Inv.Colour.Black;
      DomainEdit.Padding.Set(5);
      DomainEdit.Margin.Set(5, 0, 5, 5);
      DomainEdit.Font.Size = 30;
      DomainEdit.Text = "kestral";

      var UsernameLabel = Surface.NewLabel();
      Dock.AddHeader(UsernameLabel);
      UsernameLabel.Font.Colour = Inv.Colour.DimGray;
      UsernameLabel.Text = "username";

      var UsernameEdit = Surface.NewTextEdit();
      Dock.AddHeader(UsernameEdit);
      UsernameEdit.Background.Colour = Inv.Colour.WhiteSmoke;
      UsernameEdit.Border.Set(2);
      UsernameEdit.Border.Colour = Inv.Colour.Black;
      UsernameEdit.Padding.Set(5);
      UsernameEdit.Margin.Set(5, 0, 5, 5);
      UsernameEdit.Font.Size = 30;
      UsernameEdit.Text = "";

      var PasswordLabel = Surface.NewLabel();
      Dock.AddHeader(PasswordLabel);
      PasswordLabel.Font.Colour = Inv.Colour.DimGray;
      PasswordLabel.Text = "password";

      var PasswordEdit = Surface.NewPasswordEdit();
      Dock.AddHeader(PasswordEdit);
      PasswordEdit.Background.Colour = Inv.Colour.WhiteSmoke;
      PasswordEdit.Border.Set(2);
      PasswordEdit.Border.Colour = Inv.Colour.Black;
      PasswordEdit.Padding.Set(5);
      PasswordEdit.Margin.Set(5, 0, 5, 5);
      PasswordEdit.Font.Size = 30;
      PasswordEdit.Text = "";

      var ConnectButton = Surface.NewFlatButton();
      Dock.AddFooter(ConnectButton);
      ConnectButton.Margin.Set(5, 15, 5, 5);
      ConnectButton.Padding.Set(5);

      var ConnectLabel = Surface.NewLabel();
      ConnectButton.Content = ConnectLabel;
      ConnectLabel.Font.Size = 30;
      ConnectLabel.Font.Colour = Inv.Colour.White;
      ConnectLabel.Justify.Center();
      ConnectLabel.Text = "CONNECT";

      var MessageLabel = Surface.NewLabel();
      Dock.AddFooter(MessageLabel);
      MessageLabel.Font.Colour = Inv.Colour.DarkRed;
      MessageLabel.Font.Size = 16;
      MessageLabel.LineWrapping = true;

      return Overlay;
    }
    private Inv.Panel LongScrollingForm(Inv.Surface Surface)
    {
      var Scroll = Surface.NewVerticalScroll();

      var Stack = Surface.NewVerticalStack();
      Scroll.Content = Stack;
      Stack.Margin.Set(10);

      var LastEdit = (Inv.Edit)null;

      for (var I = 0; I < 30; I++)
      {
        var Label = Surface.NewLabel();
        Stack.AddPanel(Label);
        Label.Font.Colour = Inv.Colour.DimGray;
        Label.Text = string.Format("field {0}", I + 1);

        var Edit = Surface.NewTextEdit();
        Stack.AddPanel(Edit);
        Edit.Background.Colour = Inv.Colour.WhiteSmoke;
        Edit.Border.Set(2);
        Edit.Border.Colour = Inv.Colour.Black;
        Edit.Padding.Set(5);
        Edit.Margin.Set(5, 0, 5, 5);
        Edit.Font.Size = 30;

        if (LastEdit != null)
          LastEdit.ReturnEvent += () => Edit.Focus.Set();

        LastEdit = Edit;
      }

      return Scroll;
    }
    private Inv.Panel LongMemo(Inv.Surface Surface)
    {
      var Memo = Surface.NewMemo();
      Memo.IsReadOnly = true;
      Memo.Background.Colour = Inv.Colour.LightGray;

      var StringBuilder = new StringBuilder();
      for (var Index = 0; Index < 24; Index++)
        StringBuilder.AppendLine("LINE " + Index);

      Memo.Text = StringBuilder.ToString();

      return Memo;
    }
    private Inv.Panel EditAndMemo(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();
      Dock.Background.Colour = Inv.Colour.White;

      var Button = Surface.NewFlatButton();
      Dock.AddHeader(Button);
      Button.Background.Colour = Inv.Colour.Green;
      Button.Size.SetHeight(100);

      var TextEdit = Surface.NewTextEdit();
      Dock.AddHeader(TextEdit);
      TextEdit.Margin.Set(10);
      //TextEdit.Padding.Set(10);
      TextEdit.Background.Colour = Inv.Colour.LightGray;
      TextEdit.Font.Colour = Inv.Colour.Black;
      TextEdit.Font.Size = 18;
      TextEdit.Text = "";

      var TextEditLabel = Surface.NewLabel();
      Dock.AddHeader(TextEditLabel);
      TextEditLabel.Background.Colour = Inv.Colour.Black;
      TextEditLabel.Font.Colour = Inv.Colour.White;
      TextEditLabel.Font.Size = 30;

      TextEdit.ChangeEvent += () => TextEditLabel.Text = TextEdit.Text;

      var SearchEdit = Surface.NewSearchEdit();
      Dock.AddHeader(SearchEdit);
      SearchEdit.Margin.Set(10);
      SearchEdit.Padding.Set(5);
      SearchEdit.Background.Colour = Inv.Colour.DimGray;
      SearchEdit.Border.Set(0, 0, 0, 5);
      SearchEdit.Border.Colour = Inv.Colour.DodgerBlue;
      SearchEdit.Font.Colour = Inv.Colour.White;
      SearchEdit.Font.Size = 18;
      SearchEdit.Text = "";

      var SearchEditLabel = Surface.NewLabel();
      Dock.AddHeader(SearchEditLabel);
      SearchEditLabel.Background.Colour = Inv.Colour.Black;
      SearchEditLabel.Font.Colour = Inv.Colour.White;
      SearchEditLabel.Font.Size = 30;

      SearchEdit.ChangeEvent += () => SearchEditLabel.Text = SearchEdit.Text;

      var PasswordEdit = Surface.NewPasswordEdit();
      Dock.AddHeader(PasswordEdit);
      PasswordEdit.Background.Colour = Inv.Colour.WhiteSmoke;
      PasswordEdit.Font.Colour = Inv.Colour.Black;
      PasswordEdit.Font.Size = 30;

      var PasswordEditLabel = Surface.NewLabel();
      Dock.AddHeader(PasswordEditLabel);
      PasswordEditLabel.Background.Colour = Inv.Colour.Black;
      PasswordEditLabel.Font.Colour = Inv.Colour.White;
      PasswordEditLabel.Font.Size = 30;

      PasswordEdit.ChangeEvent += () => PasswordEditLabel.Text = PasswordEdit.Text;

      var PhoneEdit = Surface.NewPhoneEdit();
      Dock.AddHeader(PhoneEdit);
      PhoneEdit.Margin.Set(10);
      //PhoneEdit.Padding.Set(10);
      PhoneEdit.Background.Colour = Inv.Colour.Pink;
      PhoneEdit.Font.Colour = Inv.Colour.Black;
      PhoneEdit.Font.Size = 18;
      PhoneEdit.Text = "+61 407 141 102";

      var PhoneEditLabel = Surface.NewLabel();
      Dock.AddHeader(PhoneEditLabel);
      PhoneEditLabel.Background.Colour = Inv.Colour.Black;
      PhoneEditLabel.Font.Colour = Inv.Colour.White;
      PhoneEditLabel.Font.Size = 30;

      PhoneEdit.ChangeEvent += () => PhoneEditLabel.Text = PhoneEdit.Text;

      var Memo = Surface.NewMemo();
      Dock.AddClient(Memo);
      Memo.Background.Colour = Inv.Colour.LightGray;

      var MemoLabel = Surface.NewLabel();
      Dock.AddFooter(MemoLabel);
      MemoLabel.Background.Colour = Inv.Colour.Black;
      MemoLabel.Font.Colour = Inv.Colour.White;
      MemoLabel.Font.Size = 30;

      Memo.ChangeEvent += () => MemoLabel.Text = Memo.Text;

      Button.SingleTapEvent += () => SearchEdit.Focus.Set();

      SearchEdit.ReturnEvent += () => PasswordEdit.Focus.Set();

      SearchEdit.Focus.Set();

      return Dock;
    }
    private Inv.Panel FileIO(Inv.Surface Surface)
    {
      var TilePanel = new TilePanel(Surface);
      TilePanel.Caption = "DEVICE NAME: " + Surface.Window.Application.Device.Name + Environment.NewLine + Surface.Window.Application.Directory.Installation + Environment.NewLine + "PROCESS ID: " + Surface.Window.Application.Process.Id;
      TilePanel.Footer = "DEVICE MODEL/SYSTEM: " + (Surface.Window.Application.Device.Manufacturer + " " + Surface.Window.Application.Device.Model).Trim() + " ... " + Surface.Window.Application.Device.System;

      var Folder = Base.Directory.NewFolder("Data");
      var File = Folder.NewFile("Myfile.dat");

      var CreateButton = TilePanel.AddButton();
      CreateButton.Set(null, "Create file", null);
      CreateButton.SingleTapEvent += () =>
      {
        File.AsText().WriteAll("Hello World");
      };

      var ExistsButton = TilePanel.AddButton();
      ExistsButton.Set(null, "Exists file", null);
      ExistsButton.SingleTapEvent += () =>
      {
        ExistsButton.Set(null, "Exists file", File.Exists() ? "YES" : "NO");
      };

      var ReadButton = TilePanel.AddButton();
      ReadButton.Set(null, "Read file", null);
      ReadButton.SingleTapEvent += () =>
      {
        ReadButton.Set(null, "Read file", File.AsText().ReadAll());
      };

      var DeleteButton = TilePanel.AddButton();
      DeleteButton.Set(null, "Delete file", null);
      DeleteButton.SingleTapEvent += () =>
      {
        File.Delete();
      };

      var CopyButton = TilePanel.AddButton();
      CopyButton.Set(null, "Copy file", null);
      CopyButton.SingleTapEvent += () =>
      {
        var TargetFile = Folder.NewFile("Mycopy.dat");

        File.CopyReplace(TargetFile);
      };

      var MoveButton = TilePanel.AddButton();
      MoveButton.Set(null, "Move file", null);
      MoveButton.SingleTapEvent += () =>
      {
        var TargetFile = Folder.NewFile("Mymove.dat");

        File.MoveReplace(TargetFile);
      };

      var BinaryButton = TilePanel.AddButton();
      BinaryButton.Set(null, "Binary file", null);
      BinaryButton.SingleTapEvent += () =>
      {
        var TargetFile = Folder.NewFile("binary.dat");

        TargetFile.WriteAllBytes(Resources.Images.PhoenixLogo500x500.GetBuffer());

        var DataBuffer = TargetFile.ReadAllBytes();

        if (!DataBuffer.ShallowEqualTo(Resources.Images.PhoenixLogo500x500.GetBuffer()))
          BinaryButton.Set(null, "Binary file", "ERROR BINARY FILE DID NOT ROUNDTRIP");
        else
          BinaryButton.Set(null, "Binary file", "SUCCESS: " + TargetFile.GetSize());
      };

      return TilePanel;
    }
    private Inv.Panel MarginAndVisibility(Inv.Surface Surface)
    {
      var Stack = Surface.NewVerticalStack();

      var FirstButton = Surface.NewFlatButton();
      Stack.AddPanel(FirstButton);
      FirstButton.Background.Colour = Inv.Colour.DarkRed;
      FirstButton.Margin.Set(50);
      FirstButton.Size.SetHeight(200);
      FirstButton.Visibility.Collapse();

      var SecondButton = Surface.NewFlatButton();
      Stack.AddPanel(SecondButton);
      SecondButton.Background.Colour = Inv.Colour.DarkGreen;
      SecondButton.Margin.Set(50);
      SecondButton.Size.SetHeight(200);

      FirstButton.SingleTapEvent += () => SecondButton.Visibility.Toggle();
      SecondButton.SingleTapEvent += () => FirstButton.Visibility.Toggle();

      return Stack;
    }
    private Inv.Panel OverlayAlignments(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      foreach (var Placement in Inv.Support.EnumHelper.GetEnumerable<Inv.Placement>())
      {
        var Label = Surface.NewLabel();
        Label.Font.Colour = Inv.Colour.Black;
        Label.Alignment.Set(Placement);

        var LabelPlacement = Label.Alignment.Get();

        if (IsVerticalStretch(LabelPlacement) && IsHorizontalStretch(LabelPlacement))
        {
          Label.Background.Colour = Inv.Colour.Green.Opacity(0.50F);
          Label.Margin.Set(100);
        }
        else if (IsVerticalStretch(LabelPlacement))
        {
          Label.Background.Colour = Inv.Colour.Red.Opacity(0.50F);
          Label.Size.SetWidth(50);
        }
        else if (IsHorizontalStretch(LabelPlacement))
        {
          Label.Background.Colour = Inv.Colour.Blue.Opacity(0.50F);
          Label.Size.SetHeight(50);
        }
        else
        {
          Label.Text = Placement.ToString().Strip(C => char.IsLower(C));
        }

        Overlay.AddPanel(Label);
      }
      return Overlay;
    }
    private Inv.Panel OverlayAndMargins(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var LeftButton = Surface.NewFlatButton();
      Overlay.AddPanel(LeftButton);
      LeftButton.Alignment.BottomLeft();
      LeftButton.Background.Colour = Inv.Colour.Orange;
      LeftButton.Size.Set(150, 50);

      var RightButton = Surface.NewFlatButton();
      Overlay.AddPanel(RightButton);
      RightButton.Alignment.BottomRight();
      RightButton.Background.Colour = Inv.Colour.Purple;
      RightButton.Size.Set(150, 50);
      RightButton.Margin.Set(0, 0, 50, 0);

      return Overlay;
    }
    private Inv.Panel OverlayButtons(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var LeftButton = Surface.NewFlatButton();
      Overlay.AddPanel(LeftButton);
      LeftButton.Alignment.Center();
      LeftButton.Background.Colour = Inv.Colour.Green;
      LeftButton.Border.Colour = Inv.Colour.Orange.Opacity(0.50F);
      LeftButton.Border.Set(10, 10, 10, 0);
      LeftButton.Size.Set(200, 200);
      LeftButton.SingleTapEvent += () =>
      {
        LeftButton.Background.Colour = Inv.Colour.Red;
        LeftButton.Border.Set(LeftButton.Border.Left + 1);
      };

      var RightButton = Surface.NewFlatButton();
      Overlay.AddPanel(RightButton);
      RightButton.Alignment.Center();
      RightButton.Background.Colour = Inv.Colour.Purple;
      RightButton.Size.Set(100, 100);
      RightButton.SingleTapEvent += () =>
      {
        RightButton.Background.Colour = Inv.Colour.Blue;
      };

      return Overlay;
    }
    private Inv.Panel DockLabelAlignments(Inv.Surface Surface)
    {
      var Dock = Surface.NewHorizontalDock();
      Dock.Background.Colour = Inv.Colour.Black.Opacity(0.75F);

      var CaptionLabel = Surface.NewLabel();
      Dock.AddHeader(CaptionLabel);
      CaptionLabel.Text = "UPPER";
      CaptionLabel.Font.Size = 24;
      CaptionLabel.Font.Colour = Inv.Colour.WhiteSmoke;
      CaptionLabel.Padding.Set(10, 10, 0, 0);
      CaptionLabel.Alignment.TopLeft();

      var DescriptionLabel = Surface.NewLabel();
      Dock.AddClient(DescriptionLabel);
      DescriptionLabel.Text = "description";
      DescriptionLabel.Font.Size = 24;
      DescriptionLabel.Font.Colour = Inv.Colour.WhiteSmoke;
      DescriptionLabel.Padding.Set(0, 10, 10, 0);
      DescriptionLabel.Alignment.TopRight();

      return Dock;
    }
    private Inv.Panel NavigateDrawerIssue(Inv.Surface Surface)
    {
      return new NavigationDrawerScreen(Surface);
    }
    private Inv.Panel NavigateCacheIssue(Inv.Surface Surface)
    {
      return new NavigationCacheScreen(Surface);
    }
    private Inv.Panel OverlayRegions(Inv.Surface Surface)
    {
      var Base = Surface.NewOverlay();
      Base.Alignment.BottomCenter();
      Base.Size.SetHeight(Surface.Window.Height);

      var Button = Surface.NewFlatButton();
      Base.AddPanel(Button);
      Button.Alignment.Stretch();
      Button.Background.Colour = Inv.Colour.Red.Opacity(0.50F);
      Button.SingleTapEvent += () => Base.Visibility.Collapse();

      var Label = Surface.NewLabel();
      Base.AddPanel(Label);
      Label.Background.Colour = Inv.Colour.SteelBlue;
      Label.Text = "HELLO";
      Label.Alignment.BottomStretch();

      return Base;
    }
    private Inv.Panel OverlayDimensions(Inv.Surface Surface)
    {
      var Base = Surface.NewOverlay();

      var DimensionLabel = Surface.NewLabel();
      Base.AddPanel(DimensionLabel);
      DimensionLabel.Font.Size = 30;
      DimensionLabel.Alignment.TopCenter();

      var HelloLabel = Surface.NewLabel();
      Base.AddPanel(HelloLabel);
      HelloLabel.Background.Colour = Inv.Colour.SteelBlue;
      HelloLabel.Alignment.Center();
      HelloLabel.Text = "Hello World";
      HelloLabel.AdjustEvent += () => DimensionLabel.Text = HelloLabel.GetDimension().ToString();

      var CheckButton = Surface.NewFlatButton();
      Base.AddPanel(CheckButton);
      CheckButton.Background.Colour = Inv.Colour.DarkOliveGreen;
      CheckButton.Alignment.BottomCenter();
      CheckButton.Margin.Set(50);
      CheckButton.Size.Set(100, 100);
      CheckButton.SingleTapEvent += () => HelloLabel.Padding.Set(HelloLabel.Padding.Top + 5);

      var CheckLabel = Surface.NewLabel();
      CheckButton.Content = CheckLabel;
      CheckLabel.Justify.Center();
      CheckLabel.Text = "Check Size";

      return Base;
    }
    private Inv.Panel Pickers(Inv.Surface Surface)
    {
      var Stack = Surface.NewVerticalStack();
      Stack.Background.Colour = Inv.Colour.WhiteSmoke;

      var PickedDateTime = DateTime.Now;

      var DateTimeButton = Surface.NewFlatButton();
      Stack.AddPanel(DateTimeButton);

      var DateTimeLabel = Surface.NewLabel();
      DateTimeButton.Content = DateTimeLabel;
      DateTimeLabel.Alignment.Center();
      DateTimeLabel.Font.Size = 20;
      DateTimeLabel.Font.Colour = Inv.Colour.White;
      DateTimeLabel.Text = "DATE TIME";

      DateTimeButton.Background.Colour = Inv.Colour.SteelBlue;
      DateTimeButton.Margin.Set(20);
      DateTimeButton.Padding.Set(20);
      DateTimeButton.Size.SetWidth(300);
      DateTimeButton.Border.Colour = Inv.Colour.Blue;
      DateTimeButton.Alignment.Center();
      DateTimeButton.Elevation.Set(10);
      DateTimeButton.SingleTapEvent += () =>
      {
        var DatePicker = Surface.Window.Application.Calendar.NewDateTimePicker();
        DatePicker.Value = PickedDateTime;
        DatePicker.SelectEvent += () =>
        {
          PickedDateTime = DatePicker.Value;
          DateTimeLabel.Text = PickedDateTime.ToString();
        };
        DatePicker.Show();
      };

      var PickedDate = DateTime.Now.Date;

      var DateButton = Surface.NewFlatButton();
      Stack.AddPanel(DateButton);

      var DateLabel = Surface.NewLabel();
      DateButton.Content = DateLabel;
      DateLabel.Alignment.Center();
      DateLabel.Font.Size = 20;
      DateLabel.Font.Colour = Inv.Colour.White;
      DateLabel.Text = "DATE";

      DateButton.Background.Colour = Inv.Colour.SteelBlue;
      DateButton.Margin.Set(20);
      DateButton.Padding.Set(20);
      DateButton.Size.SetWidth(300);
      DateButton.Border.Colour = Inv.Colour.Blue;
      DateButton.Alignment.Center();
      DateButton.Elevation.Set(10);
      DateButton.SingleTapEvent += () =>
      {
        var DatePicker = Surface.Window.Application.Calendar.NewDatePicker();
        DatePicker.Value = PickedDate;
        DatePicker.SelectEvent += () =>
        {
          PickedDate = DatePicker.Value;
          DateLabel.Text = PickedDate.ToString("dd/MM/yyyy");
        };
        DatePicker.Show();
      };

      var PickedTime = DateTime.MinValue + DateTime.Now.TimeOfDay;

      var TimeButton = Surface.NewFlatButton();
      Stack.AddPanel(TimeButton);

      var TimeLabel = Surface.NewLabel();
      TimeButton.Content = TimeLabel;
      TimeLabel.Alignment.Center();
      TimeLabel.Font.Size = 20;
      TimeLabel.Font.Colour = Inv.Colour.White;
      TimeLabel.Text = "TIME";

      TimeButton.Background.Colour = Inv.Colour.SteelBlue;
      TimeButton.Margin.Set(20);
      TimeButton.Padding.Set(20);
      TimeButton.Size.SetWidth(300);
      TimeButton.Border.Colour = Inv.Colour.Blue;
      TimeButton.Alignment.Center();
      TimeButton.Elevation.Set(10);
      TimeButton.SingleTapEvent += () =>
      {
        var TimePicker = Surface.Window.Application.Calendar.NewTimePicker();
        TimePicker.Value = PickedTime;
        TimePicker.SelectEvent += () =>
        {
          PickedTime = TimePicker.Value;
          TimeLabel.Text = PickedTime.ToString("HH:mm");
        };
        TimePicker.Show();
      };

      var ImageFileButton = Surface.NewFlatButton();
      Stack.AddPanel(ImageFileButton);

      var ImageFileLabel = Surface.NewLabel();
      ImageFileButton.Content = ImageFileLabel;
      ImageFileLabel.Alignment.Center();
      ImageFileLabel.Font.Size = 20;
      ImageFileLabel.Font.Colour = Inv.Colour.White;
      ImageFileLabel.Text = "IMAGE FILE";

      ImageFileButton.Background.Colour = Inv.Colour.SteelBlue;
      ImageFileButton.Margin.Set(20);
      ImageFileButton.Padding.Set(20);
      ImageFileButton.Size.SetWidth(300);
      ImageFileButton.Border.Colour = Inv.Colour.Blue;
      ImageFileButton.Alignment.Center();
      ImageFileButton.Elevation.Set(10);
      ImageFileButton.SingleTapEvent += () =>
      {
        var ImageFilePicker = Surface.Window.Application.Directory.NewImageFilePicker();
        ImageFilePicker.Title = "Select Image";
        ImageFilePicker.SelectEvent += (Image) =>
        {
          var FileGraphic = Surface.NewGraphic();
          ImageFileButton.Content = FileGraphic;
          FileGraphic.Image = Image;
        };
        ImageFilePicker.Show();
      };

      var AnyFileButton = Surface.NewFlatButton();
      Stack.AddPanel(AnyFileButton);

      var AnyFileLabel = Surface.NewLabel();
      AnyFileButton.Content = AnyFileLabel;
      AnyFileLabel.Alignment.Center();
      AnyFileLabel.Font.Size = 20;
      AnyFileLabel.Font.Colour = Inv.Colour.White;
      AnyFileLabel.Text = "ANY FILE";

      AnyFileButton.Background.Colour = Inv.Colour.SteelBlue;
      AnyFileButton.Margin.Set(20);
      AnyFileButton.Padding.Set(20);
      AnyFileButton.Size.SetWidth(300);
      AnyFileButton.Border.Colour = Inv.Colour.Blue;
      AnyFileButton.Alignment.Center();
      AnyFileButton.Elevation.Set(10);
      AnyFileButton.SingleTapEvent += () =>
      {
        var AnyFilePicker = Surface.Window.Application.Directory.NewAnyFilePicker();
        AnyFilePicker.Title = "Select File";
        AnyFilePicker.SelectEvent += (Pick) =>
        {
          var FileLabel = Surface.NewLabel();
          AnyFileButton.Content = FileLabel;
          FileLabel.Font.Colour = Inv.Colour.White;
          FileLabel.Text = Pick.Name;

          var Binary = Pick.ReadBinary();

          FileLabel.Text += Environment.NewLine + Binary.GetSize().ToString();
        };
        AnyFilePicker.Show();
      };
      return Stack;
    }
    private Inv.Panel Progressing(Inv.Surface Surface)
    {
      var Canvas = Surface.NewCanvas();
      Canvas.Background.Colour = Inv.Colour.DimGray.Darken(0.50F);
      

      Canvas.DrawEvent += (DC) =>
      {
      };
      Canvas.Draw();

      return Canvas;
    }
    private Inv.Panel Polygons(Inv.Surface Surface)
    {
      var Canvas = Surface.NewCanvas();
      Canvas.Background.Colour = Inv.Colour.DimGray.Darken(0.50F);

      // DSL?
      // Image support?
      // Designer?

      var Scale = 2;

      var SwimSchoolStyle = new Style("Swim School", Inv.Colour.White, Inv.Colour.DarkOrange);
      var EducationDeptStyle = new Style("Education Department", Inv.Colour.White, Inv.Colour.DarkSeaGreen.Darken(0.25F));
      var PublicAreaStyle = new Style("Public Area", Inv.Colour.White, Inv.Colour.DodgerBlue);
      var StyleArray = new Style[]
      {
        SwimSchoolStyle,
        EducationDeptStyle,
        PublicAreaStyle,
      };

      var ZoneArray = new Zone[]
      {
        new Zone("Zone 1", SwimSchoolStyle, new Inv.Rect(10, 10, 100, 150)),
        new Zone("Zone 2", SwimSchoolStyle, new Inv.Rect(10, 160, 100, 150)),
        new Zone("Ed Dept.", EducationDeptStyle, new Inv.Rect(10, 310, 100, 150)),
        new Zone("P.L", PublicAreaStyle, new Inv.Rect(110, 10, 40, 450)),
        new Zone("P.L", PublicAreaStyle, new Inv.Rect(150, 10, 40, 450)),
        new Zone("P.A", PublicAreaStyle, new Inv.Rect(190, 160, 70, 110)),
        new Zone("Adventure Pool", PublicAreaStyle, new Inv.Point(365, 10), new Inv.Point(460, 10), new Inv.Point(460, 195), new Inv.Point(260, 195), new Inv.Point(260, 160), new Inv.Point(290, 160), new Inv.Point(290, 110), new Inv.Point(365, 110)),
        new Zone("Box A", SwimSchoolStyle, new Inv.Rect(190, 10, 100, 75)),
        new Zone("Box B", SwimSchoolStyle, new Inv.Rect(190, 85, 100, 75)),
        new Zone("Box C", SwimSchoolStyle, new Inv.Rect(290, 10, 75, 100)),
        new Zone("Zone 5", SwimSchoolStyle, new Inv.Rect(190, 270, 70, 75)),
        new Zone("Zone 6", SwimSchoolStyle, new Inv.Rect(260, 270, 200, 75)),
        new Zone("Zone 7", SwimSchoolStyle, new Inv.Rect(260, 195, 200, 75)),
      };
     
      Canvas.DrawEvent += (DC) =>
      {
        foreach (var Zone in ZoneArray)
        {
          DC.DrawPolygon(Zone.Style.FillColour, Zone.Style.StrokeColour, Scale, Inv.LineJoin.Round, Zone.StartPoint, Zone.PointArray);

          DC.DrawText(Zone.Name, "", 20, Inv.FontWeight.Regular, Zone.Style.StrokeColour, Zone.CenterPoint, Inv.HorizontalPosition.Center, Inv.VerticalPosition.Center);
        }

        if (StyleArray.Length > 0)
        {
          var StyleWidth = Surface.Window.Width / StyleArray.Length;

          var StyleLeft = 0;
          var StyleTop = 600;
          var StyleHeight = 40;

          foreach (var Style in StyleArray)
          {
            DC.DrawRectangle(Style.FillColour, Style.StrokeColour, Scale, new Inv.Rect(StyleLeft, StyleTop, StyleWidth, StyleHeight));

            DC.DrawText(Style.Name, "", 16, Inv.FontWeight.Regular, Style.StrokeColour, new Inv.Point(StyleLeft + StyleWidth / 2, StyleTop + StyleHeight / 2), Inv.HorizontalPosition.Center, Inv.VerticalPosition.Center);

            StyleLeft += StyleWidth;
          }
        }
      };
      Canvas.Draw();

      return Canvas;
    }
    private Inv.Panel Portal(Inv.Surface Surface)
    {
      var TilePanel = new TilePanel(Surface);

      for (var Index = 1; Index <= 20; Index++)
      {
        var TileButton = TilePanel.AddButton();
        TileButton.Set(LogoImage, Index + ". TITLE", "description" + Index);
        TileButton.SingleTapEvent += () =>
        {
          TilePanel.RemoveButton(TileButton);
          Surface.Rearrange();
        };
      }

      return TilePanel;
    }
    private Inv.Panel Records(Inv.Surface Surface)
    {
      var NavigateDock = Surface.NewVerticalDock();
      
      NavigateDock.Background.Colour = Inv.Colour.DimGray.Darken(0.50F);

      var NavigateLabel = Surface.NewLabel();
      NavigateDock.AddHeader(NavigateLabel);
      NavigateLabel.Font.Colour = Inv.Colour.White;
      NavigateLabel.Font.Size = 24;
      NavigateLabel.Text = "Records";
      NavigateLabel.Margin.Set(10);

      var RecordFrame = Surface.NewFrame();
      NavigateDock.AddClient(RecordFrame);

      var RecordPanel = new RecordPanel(Surface);
      RecordFrame.Content = RecordPanel;
      RecordPanel.Alignment.Center();

      for (var Index = 0; Index < 20; Index++)
      {
        var RecordButton = RecordPanel.AddButton();
        RecordButton.Rank = Index;
        RecordButton.Identity = "Identity" + Index;
        RecordButton.Summary = "long summary text";
        RecordButton.Fame = (10 - Index + 1) * 1000;
        RecordButton.AddImage(LogoImage);
      }

      var BackButton = new CaptionButton(Surface);
      NavigateDock.AddFooter(BackButton);
      BackButton.Colour = Inv.Colour.DarkGreen;
      BackButton.Text = "BACK";
      BackButton.Margin.Set(10);
      BackButton.Size.SetHeight(60);

      return NavigateDock;
    }
    private Inv.Panel Reclamation(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();

      var Graphic = Surface.NewGraphic();
      Dock.AddHeader(Graphic);
      Graphic.Alignment.Center();
      Graphic.Image = Resources.Images.PhoenixLogo500x500;

      var Button = Surface.NewFlatButton();
      Dock.AddClient(Button);
      Button.Background.Colour = Inv.Colour.DarkRed;
      Button.Margin.Set(20);
      Button.Padding.Set(20);
      Button.Size.Set(300, 300);
      Button.Alignment.Center();
      Button.SingleTapEvent += () =>
      {
        Surface.Window.Application.Process.MemoryReclamation();

        // forces the graphic to arrange.
        if (Graphic.Margin.IsSet)
          Graphic.Margin.Clear();
        else
          Graphic.Margin.Set(50);
      };

      var Label = Surface.NewLabel();
      Button.Content = Label;
      Label.Text = "RECLAMATION";
      Label.Font.Size = 24;
      Label.Font.Colour = Inv.Colour.WhiteSmoke;
      Label.Padding.Set(0, 10, 10, 0);
      Label.Justify.Center();

      return Dock;
    }
    private Inv.Panel RoundedGraphic(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var Graphic = Surface.NewGraphic();
      Overlay.AddPanel(Graphic);
      Graphic.Alignment.Center();
      Graphic.Corner.Set(32);
      Graphic.Size.Set(32, 32);
      Graphic.Image = Resources.Images.ClearWhite;
      Graphic.Background.Colour = Inv.Colour.FromArgb(0xFFE57373);
      Graphic.Padding.Set(4);

      var Label = Surface.NewLabel();
      Overlay.AddPanel(Label);
      Label.Alignment.BottomStretch();
      Label.Text =
        "NOTE: The corner and size are both set to 32 which doesn't exactly make sense. iOS will display a rounded diamond but the other platforms display a circle. However, the corner was set to 16 it would display a consistent circle on all platforms.";

      return Overlay;
    }
    private Inv.Panel FrameFeatures(Inv.Surface Surface)
    {
      var Frame = Surface.NewFrame();
      Frame.Alignment.Center();
      Frame.Size.Set(200, 200);
      Frame.Margin.Set(10);
      Frame.Padding.Set(10);
      Frame.Corner.Set(10);
      Frame.Elevation.Set(10);
      Frame.Border.Colour = Inv.Colour.Red;
      Frame.Border.Set(10);
      Frame.Background.Colour = Inv.Colour.Green;

      var Label = Surface.NewLabel();
      Frame.Content = Label;
      Label.Alignment.Stretch();
      Label.Background.Colour = Inv.Colour.Purple;

      return Frame;
    }
    private Inv.Panel ScrollButtons(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.Black;

      var CustomQuery = new CustomQuery(Surface);
      Overlay.AddPanel(CustomQuery);
      CustomQuery.ShadeColour = Inv.Colour.Black.Opacity(0.75F);

      var CustomPanel = new CustomPanel(Surface);
      CustomQuery.Content = CustomPanel;
      CustomPanel.Alignment.Center();
      CustomPanel.Caption = "HELLO WORLD";

      var ShadeButton = new ShadeButton(Surface);
      CustomPanel.Content = ShadeButton;
      ShadeButton.Border.Set(2);
      ShadeButton.Border.Colour = Inv.Colour.Transparent;
      ShadeButton.SingleTapEvent += () => CustomQuery.Hide();

      var Frame = Surface.NewFrame();
      ShadeButton.Content = Frame;

      var RecordPanel = new RecordPanel(Surface);
      Frame.Content = RecordPanel;
      RecordPanel.Alignment.Center();

      for (var Index = 0; Index < 20; Index++)
      {
        var RecordButton = RecordPanel.AddButton();
        RecordButton.Rank = Index;
        RecordButton.Identity = "Identity" + Index;
        RecordButton.Summary = "long summary text";
        RecordButton.Fame = (10 - Index + 1) * 1000;
        RecordButton.AddImage(LogoImage);
      }

      CustomQuery.HideEvent += () => Overlay.RemovePanel(CustomQuery);
      CustomQuery.Show();

      return Overlay;
    }
    private Inv.Panel ServerCase(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var Frame = Surface.NewFlatButton();
      Overlay.AddPanel(Frame);
      Frame.Background.Colour = Inv.Colour.HotPink;
      Frame.Size.Set(88, 88);
      Frame.Alignment.TopLeft();

      var Button = Surface.NewFlatButton();
      Overlay.AddPanel(Button);

      Button.Background.Colour = Inv.Colour.SteelBlue;
      Button.Alignment.Center();
      Button.Size.Set(200, 200);
      Button.Elevation.Set(5);
      Button.SingleTapEvent += () =>
      {
        if (Button.IsEnabled)
        {
          Button.IsEnabled = false;
          Button.Background.Colour = Inv.Colour.Blue;

          Surface.Window.Sleep(TimeSpan.FromMilliseconds(1000));
        }
        else
        {
          // should not be possible.
          Button.Background.Colour = Inv.Colour.Red;
        }
      };

      Frame.SingleTapEvent += () =>
      {
        Button.IsEnabled = true;
        Button.Background.Colour = Inv.Colour.SteelBlue;
      };

      return Overlay;
    }
    private Inv.Panel SettingsFlyout(Inv.Surface Surface)
    {
      var Flyout = Surface.NewOverlay();

      var Scroll = Surface.NewVerticalScroll();
      Flyout.AddPanel(Scroll);
      Scroll.Alignment.StretchLeft();
      Scroll.Background.Colour = Inv.Colour.DimGray.Darken(0.50F);
      Scroll.Size.SetWidth(250);
      Scroll.Padding.Set(20);

      var Dock = Surface.NewVerticalDock();
      //Flyout.AddElement(Dock);
      Scroll.Content = Dock;
      //Dock.Alignment.StretchLeft();

      var ButtonHeight = 100;
      var ButtonMargin = 10;

      var TitleLabel = Surface.NewLabel();
      Dock.AddFooter(TitleLabel);
      TitleLabel.Alignment.Center();
      TitleLabel.Margin.Set(ButtonMargin);
      TitleLabel.Font.Colour = Inv.Colour.White;
      TitleLabel.Font.Size = 30;
      TitleLabel.Text = "SETTINGS";

      var LanguageStack = Surface.NewVerticalStack();
      Dock.AddFooter(LanguageStack);
      LanguageStack.Margin.Set(ButtonMargin);

      var LanguageArray = new[]
        {
          new
          {
            Code = "en",
            Name = "English"
          },
          new
          {
            Code = "fr",
            Name = "Français"
          }
        };

      var LanguageButtonList = new Inv.DistinctList<CaptionButton>(LanguageArray.Length);

      foreach (var Language in LanguageArray)
      {
        var LanguageButton = new CaptionButton(Surface);
        LanguageStack.AddPanel(LanguageButton);
        LanguageButton.Padding.Set(20);
        LanguageButton.SingleTapEvent += () =>
        {
          foreach (var EachButton in LanguageButtonList)
            EachButton.Colour = LanguageButton == EachButton ? Inv.Colour.Purple : Inv.Colour.DimGray;
        };
        LanguageButton.Colour = Language.Code == System.Globalization.CultureInfo.CurrentCulture.TwoLetterISOLanguageName ? Inv.Colour.Purple : Inv.Colour.DimGray;
        LanguageButton.Text = Language.Name;

        LanguageButtonList.Add(LanguageButton);
      }

      var SoundButton = Surface.NewFlatButton();
      Dock.AddFooter(SoundButton);
      SoundButton.Alignment.Center();
      SoundButton.Size.Set(100, 100);
      SoundButton.Corner.Set(25);
      SoundButton.Margin.Set(0, 10, 0, 10);

      var SoundGraphic = Surface.NewGraphic();
      SoundButton.Content = SoundGraphic;
      SoundGraphic.Size.Set(64, 64);

      var Muted = false;

      SoundButton.SingleTapEvent += () =>
      {
        Muted = !Muted;

        SoundButton.Background.Colour = Muted ? Inv.Colour.DimGray : Inv.Colour.DimGray;
        SoundGraphic.Image = Muted ? Resources.Images.SoundOff256x256 : Resources.Images.SoundOn256x256;
      };

      SoundButton.Background.Colour = Muted ? Inv.Colour.DimGray : Inv.Colour.DimGray;
      SoundGraphic.Image = Muted ? Resources.Images.SoundOff256x256 : Resources.Images.SoundOn256x256;

      var DiagnosticsButton = new CaptionButton(Surface);
      Dock.AddFooter(DiagnosticsButton);
      DiagnosticsButton.Colour = Inv.Colour.DimGray;
      DiagnosticsButton.Size.SetHeight(ButtonHeight / 2);
      DiagnosticsButton.Margin.Set(ButtonMargin);
      DiagnosticsButton.Text = "DIAGNOSTICS";
      DiagnosticsButton.SingleTapEvent += () =>
      {
      };

      return Flyout;
    }
    private Inv.Panel SideFlyout(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = null;

      var SideDock = Surface.NewVerticalDock();
      Overlay.AddPanel(SideDock);
      SideDock.Alignment.Stretch();
      SideDock.Background.Colour = Inv.Colour.Black;
      SideDock.Size.SetWidth(420);

      var FooterDock = Surface.NewVerticalDock();
      SideDock.AddClient(FooterDock);
      FooterDock.Background.Colour = Inv.Colour.Green;
      FooterDock.Alignment.BottomStretch();

      var TrailerLabel = Surface.NewLabel();
      SideDock.AddFooter(TrailerLabel);
      TrailerLabel.Background.Colour = Inv.Colour.Pink;
      TrailerLabel.Font.Size = 20;
      TrailerLabel.Text = "TRAILER";

      var FooterTitle = Surface.NewLabel();
      FooterDock.AddHeader(FooterTitle);
      FooterTitle.Font.Colour = Inv.Colour.White;
      FooterTitle.Font.Size = 30;
      FooterTitle.Text = "FOOTER";

      //var FooterScroll = Surface.NewVerticalScroll();
      //FooterDock.AddClient(FooterScroll);

      var FooterStack = Surface.NewVerticalStack();
      FooterDock.AddClient(FooterStack);
      //FooterScroll.Content = FooterStack;

      var SummaryButton = Surface.NewFlatButton();
      FooterStack.AddPanel(SummaryButton);

      var SummaryDock = Surface.NewHorizontalDock();
      SummaryButton.Content = SummaryDock;

      var SummaryGraphic = Surface.NewGraphic();
      SummaryDock.AddHeader(SummaryGraphic);
      SummaryGraphic.Size.Set(64, 64);
      SummaryGraphic.Image = LogoImage;

      var SummaryStack = Surface.NewVerticalStack();
      SummaryDock.AddClient(SummaryStack);
      SummaryStack.Background.Colour = Inv.Colour.Orange;
      SummaryStack.Padding.Set(10);

      var SummaryTitle = Surface.NewLabel();
      SummaryStack.AddPanel(SummaryTitle);
      SummaryTitle.Background.Colour = Inv.Colour.Blue;
      SummaryTitle.Font.Colour = Inv.Colour.White;
      SummaryTitle.Font.Size = 30;
      SummaryTitle.Text = "TITLE";

      var SummaryDescription = Surface.NewLabel();
      SummaryStack.AddPanel(SummaryDescription);
      SummaryDescription.Background.Colour = Inv.Colour.Red;
      SummaryDescription.Font.Colour = Inv.Colour.White;
      SummaryDescription.Font.Size = 30;
      SummaryDescription.Text = "description";

      return Overlay;
    }
    private Inv.Panel SoundSpam(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.DarkGray;

      var Random = new Random();

      var Sounds = Resources.Sounds;
      var SoundsType = Sounds.GetType();

      var SoundList = new Inv.DistinctList<Inv.Sound>();
      foreach (var SoundsField in SoundsType.GetReflectionFields())
        SoundList.Add(((Inv.Resource.SoundReference)SoundsField.GetValue(Sounds)).Load());

      var Button = Surface.NewFlatButton();
      Overlay.AddPanel(Button);
      Button.Alignment.Center();
      Button.Padding.Set(50);
      Button.Background.Colour = Inv.Colour.Green;
      Button.SingleTapEvent += () =>
      {
        var Repeat = 0;

        void Spam()
        {
          for (var Index = 0; Index < 1000; Index++)
            Base.Window.Application.Audio.Play(Random.NextItem(SoundList), 1.0F, 1.0F + Random.Next(-6, +6) / 100.0F);

          Repeat++;

          if (Repeat < 100)
            Surface.Window.Post(() => Spam());
        }

        Spam();
      };

      return Overlay;
    }
    private Inv.Panel SplitScreen(Inv.Surface Surface)
    {
      var Dock = Surface.NewHorizontalDock();

      var LeftSelected = false;

      var LeftCanvas = Surface.NewCanvas();
      Dock.AddClient(LeftCanvas);
      LeftCanvas.Border.Set(0, 0, 1, 0);
      LeftCanvas.Border.Colour = Inv.Colour.Black;
      LeftCanvas.PressEvent += (Point) =>
      {
        LeftSelected = true;
        LeftCanvas.Draw();
      };
      LeftCanvas.ReleaseEvent += (Point) =>
      {
        LeftSelected = false;
        LeftCanvas.Draw();
      };
      LeftCanvas.DrawEvent += (DC) => DC.DrawEllipse(LeftSelected ? Inv.Colour.Purple : Inv.Colour.Pink, Inv.Colour.Transparent, 0, new Inv.Point(100, 100), new Inv.Point(100, 100));
      LeftCanvas.Draw();

      var RightSelected = false;

      var RightCanvas = Surface.NewCanvas();
      Dock.AddClient(RightCanvas);
      RightCanvas.Border.Set(1, 0, 0, 0);
      RightCanvas.Border.Colour = Inv.Colour.Black;
      RightCanvas.PressEvent += (Point) =>
      {
        RightSelected = true;
        RightCanvas.Draw();
      };
      RightCanvas.ReleaseEvent += (Point) =>
      {
        RightSelected = false;
        RightCanvas.Draw();
      };
      RightCanvas.DrawEvent += (DC) => DC.DrawEllipse(RightSelected ? Inv.Colour.DarkOliveGreen : Inv.Colour.Orange, Inv.Colour.Transparent, 0, new Inv.Point(100, 100), new Inv.Point(100, 100));
      RightCanvas.Draw();

      return Dock;
    }
    private Inv.Panel SpriteLoadTesting(Inv.Surface Surface)
    {
      var Canvas = Surface.NewCanvas();

      Canvas.DrawEvent += (RC) =>
      {
        var RCWidth = Surface.Window.Width;
        var RCHeight = Surface.Window.Height;
        if (RCWidth == 0 || RCHeight == 0)
          return;

        RC.DrawRectangle(Inv.Colour.Black, null, 0, new Inv.Rect(0, 0, RCWidth, RCHeight));

        var TileSize = 16;
        var BoardWidth = RCWidth / TileSize;
        var BoardHeight = RCHeight / TileSize;

        var CellY = 0;

        for (var Y = 0; Y <= BoardHeight; Y++)
        {
          var CellX = 0;

          for (var X = 0; X <= BoardWidth; X++)
          {
            var Rect = new Inv.Rect(CellX, CellY, TileSize, TileSize);

            RC.DrawImage(Resources.Images.Water128x128, Rect);

            CellX += TileSize;
          }

          CellY += TileSize;
        }

        RC.DrawText("Inv (" + BoardWidth + " by " + BoardHeight + " @ " + TileSize + "x" + TileSize + ") " + Surface.Window.DisplayRate.PerSecond + " fps", "", 20, Inv.FontWeight.Regular, Inv.Colour.White, new Inv.Point(2, RCHeight - 2), Inv.HorizontalPosition.Left, Inv.VerticalPosition.Bottom);
      };

      Surface.ComposeEvent += () => Canvas.Draw();

      return Canvas;
    }
    private Inv.Panel StackAlignmentHorizontal(Inv.Surface Surface)
    {
      var VerticalStack = Surface.NewVerticalStack();
      VerticalStack.Background.Colour = Inv.Colour.Black;

      var H1 = Surface.NewHorizontalStack();
      VerticalStack.AddPanel(H1);
      H1.Background.Colour = Inv.Colour.WhiteSmoke;
      H1.Margin.Set(5);
      H1.Alignment.StretchLeft();

      var B1 = Surface.NewFlatButton();
      H1.AddPanel(B1);
      B1.Background.Colour = Inv.Colour.Blue;
      B1.Size.Set(140, 140);
      B1.Margin.Set(5);

      var B2 = Surface.NewFlatButton();
      H1.AddPanel(B2);
      B2.Background.Colour = Inv.Colour.Green;
      B2.Size.Set(140, 140);
      B2.Margin.Set(5);

      var H2 = Surface.NewHorizontalStack();
      VerticalStack.AddPanel(H2);
      H2.Background.Colour = Inv.Colour.Wheat;
      H1.Margin.Set(5);
      H2.Alignment.StretchRight();

      var B3 = Surface.NewFlatButton();
      H2.AddPanel(B3);
      B3.Background.Colour = Inv.Colour.Red;
      B3.Size.Set(140, 140);
      B3.Margin.Set(5);

      var B4 = Surface.NewFlatButton();
      H2.AddPanel(B4);
      B4.Background.Colour = Inv.Colour.Yellow;
      B4.Size.Set(140, 140);
      B4.Margin.Set(5);

      return VerticalStack;
    }
    private Inv.Panel StackAlignmentVertical(Inv.Surface Surface)
    {
      var HorizontalStack = Surface.NewHorizontalStack();
      HorizontalStack.Background.Colour = Inv.Colour.Black;

      var V1 = Surface.NewVerticalStack();
      HorizontalStack.AddPanel(V1);
      V1.Background.Colour = Inv.Colour.WhiteSmoke;
      V1.Margin.Set(5);
      V1.Alignment.TopStretch();

      var B1 = Surface.NewFlatButton();
      V1.AddPanel(B1);
      B1.Background.Colour = Inv.Colour.Blue;
      B1.Size.Set(140, 140);
      B1.Margin.Set(5);

      var B2 = Surface.NewFlatButton();
      V1.AddPanel(B2);
      B2.Background.Colour = Inv.Colour.Green;
      B2.Size.Set(140, 140);
      B2.Margin.Set(5);

      var V2 = Surface.NewVerticalStack();
      HorizontalStack.AddPanel(V2);
      V2.Background.Colour = Inv.Colour.Wheat;
      V1.Margin.Set(5);
      V2.Alignment.BottomStretch();

      var B3 = Surface.NewFlatButton();
      V2.AddPanel(B3);
      B3.Background.Colour = Inv.Colour.Red;
      B3.Size.Set(140, 140);
      B3.Margin.Set(5);

      var B4 = Surface.NewFlatButton();
      V2.AddPanel(B4);
      B4.Background.Colour = Inv.Colour.Yellow;
      B4.Size.Set(140, 140);
      B4.Margin.Set(5);

      return HorizontalStack;
    }
    private Inv.Panel StackInStack(Inv.Surface Surface)
    {
      var S1 = Surface.NewHorizontalStack();
      S1.Background.Colour = Inv.Colour.Blue;
      S1.Alignment.TopStretch();
      S1.Margin.Set(10);
      S1.Padding.Set(10);

      var OneLabel = Surface.NewLabel();
      S1.AddPanel(OneLabel);
      OneLabel.Background.Colour = Inv.Colour.White;
      OneLabel.Text = "One";
      OneLabel.Padding.Set(10);
      OneLabel.Margin.Set(10);

      var TwoLabel = Surface.NewLabel();
      S1.AddPanel(TwoLabel);
      TwoLabel.Background.Colour = Inv.Colour.White;
      TwoLabel.LineWrapping = true;
      TwoLabel.Text = "Two\nwith a second\nline...";
      TwoLabel.Padding.Set(10);
      TwoLabel.Margin.Set(10);

      var S2 = Surface.NewVerticalStack();
      S1.AddPanel(S2);
      S2.Background.Colour = Inv.Colour.Orange;
      S2.Padding.Set(10);
      S2.Margin.Set(10);

      var ALabel = Surface.NewLabel();
      S2.AddPanel(ALabel);
      ALabel.Background.Colour = Inv.Colour.White;
      ALabel.Text = "A";
      ALabel.Padding.Set(10);
      ALabel.Margin.Set(10);

      var BLabel = Surface.NewLabel();
      S2.AddPanel(BLabel);
      BLabel.Background.Colour = Inv.Colour.White;
      BLabel.Text = "B";
      BLabel.Padding.Set(10);
      BLabel.Margin.Set(10);

      return S1;
    }
    private Inv.Panel SyntaxHighlight(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();

      var Button = Surface.NewFlatButton();
      Dock.AddHeader(Button);
      Button.Size.SetHeight(200);
      Button.Background.Colour = Inv.Colour.DarkGreen;

      var Memo = Surface.NewMemo();
      Dock.AddClient(Memo);
      Memo.Background.Colour = Inv.Colour.WhiteSmoke;
      Memo.Font.Colour = Inv.Colour.Purple;
      Memo.Font.Bold();
      Memo.Font.Size = 20;
      Memo.Font.Monospaced();
      Memo.ChangeEvent += () => Memo.Font.Size++;

      Button.SingleTapEvent += () =>
      {
        Memo.Text = "The quick_brown fox jumps:over the lazy dog.";

        var RedMarkup = Memo.AddMarkup();
        RedMarkup.Font.Proportional();
        RedMarkup.Font.Colour = Inv.Colour.Red;
        RedMarkup.Font.Size = 40;
        RedMarkup.Font.Thin();
        RedMarkup.AddRange(4, 5);

        var BlueMarkup = Memo.AddMarkup();
        BlueMarkup.Font.Proportional();
        BlueMarkup.Font.Colour = Inv.Colour.Blue;
        BlueMarkup.Font.Size = 30;
        BlueMarkup.Font.Heavy();
        BlueMarkup.AddRange(20, 5);

        var GreenMarkup = Memo.AddMarkup();
        GreenMarkup.Font.Proportional();
        GreenMarkup.Font.Colour = Inv.Colour.Green;
        GreenMarkup.Font.Size = 10;
        GreenMarkup.Font.Medium();
        GreenMarkup.AddRange(40, 3);
      };

      // NOTE: Uwa is not displaying the markup when it is first loaded (reason unknown, workaround not found).
      Button.SingleTap();

      return Dock;
    }
    private Inv.Panel SwitchOnOff(Inv.Surface Surface)
    {
      var S1 = Surface.NewVerticalStack();
      S1.Background.Colour = Inv.Colour.Orange;

      var Switch = Surface.NewSwitch();
      S1.AddPanel(Switch);
      Switch.Background.Colour = Inv.Colour.Red;
      Switch.ChangeEvent += () =>
      {
        if (Switch.IsOn)
          S1.Background.Colour = Inv.Colour.Purple;
        else
          S1.Background.Colour = Inv.Colour.Orange;
      };

      return S1;
    }
    private Inv.Panel TableAdjustments(Inv.Surface Surface)
    {
      var Scroll = Surface.NewVerticalScroll();
      Scroll.Background.Colour = Inv.Colour.White;

      var Dock = Surface.NewVerticalDock();
      Scroll.Content = Dock;

      var TableScroll = Surface.NewHorizontalScroll();
      Dock.AddClient(TableScroll);

      var Table = Surface.NewTable();
      TableScroll.Content = Table;
      Table.Margin.Set(4);
      Table.Compose(3, 3);

      void ResetCells(Inv.TableColumn HighlightColumn, Inv.TableRow HighlightRow)
      {
        foreach (var Cell in Table.GetCells())
        {
          var Label = Surface.NewLabel();
          Cell.Content = Label;
          Label.Text = string.Format("({0}, {1})", Cell.X, Cell.Y);
          Label.Font.Size = 20;
          Label.Padding.Set(10);

          if (Cell.Column == HighlightColumn || Cell.Row == HighlightRow)
          {
            Label.Font.Colour = Inv.Colour.White;
            Label.Background.Colour = Inv.Colour.Red;
          }
        }
      }

      ResetCells(null, null);

      var Rand = new Random();

      for (var I = 0; I < 4; I++)
      {
        var Button = Surface.NewFlatButton();
        Dock.AddHeader(Button);
        Button.Alignment.TopStretch();
        Button.Margin.Set(4, 4, 4, 0);
        Button.Background.Colour = Inv.Colour.SkyBlue;
        Button.Padding.Set(10);

        var ButtonLabel = Surface.NewLabel();
        Button.Content = ButtonLabel;
        ButtonLabel.Alignment.Center();
        ButtonLabel.Font.Colour = Inv.Colour.White;
        ButtonLabel.Font.Size = 14;

        switch (I)
        {
          case 0:
            ButtonLabel.Text = "Insert Row";
            Button.SingleTapEvent += () =>
            {
              var Row = Table.InsertRow(Rand.Next(Table.RowCount + 1));
              ResetCells(null, Row);
            };
            break;

          case 1:
            ButtonLabel.Text = "Remove Row";
            Button.SingleTapEvent += () =>
            {
              if (Table.RowCount > 0)
              {
                Table.RemoveRow(Table.GetRow(Rand.Next(Table.RowCount)));
                ResetCells(null, null);
              }
            };
            break;

          case 2:
            ButtonLabel.Text = "Insert Column";
            Button.SingleTapEvent += () =>
            {
              var Column = Table.InsertColumn(Rand.Next(Table.ColumnCount + 1));
              ResetCells(Column, null);
            };
            break;

          case 3:
            ButtonLabel.Text = "Remove Column";
            Button.SingleTapEvent += () =>
            {
              if (Table.ColumnCount > 0)
              {
                Table.RemoveColumn(Table.GetColumn(Rand.Next(Table.ColumnCount)));
                ResetCells(null, null);
              }
            };
            break;
        }
      }

      return Scroll;
    }
    private Inv.Panel TableArrangement(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();

      var TableCount = 3;
      var ClientCount = 2;
      var TableSize = 3;

      for (var Index = 0; Index < TableCount; Index++)
      {
        var Table = Surface.NewTable();
        switch (Index % 3)
        {
          case 0:
            Table.Background.Colour = Inv.Colour.Red;
            break;
          case 1:
            Table.Background.Colour = Inv.Colour.Green;
            break;
          case 2:
            Table.Background.Colour = Inv.Colour.Blue;
            break;
        }
        Table.Margin.Set(5);
        Table.Compose(TableSize, TableSize);

        foreach (var Cell in Table.GetCells())
        {
          var Label = Surface.NewLabel();
          Cell.Content = Label;
          Label.Text = string.Join("\n", Enumerable.Repeat(new string('x', Cell.Y + 1), Cell.X + 1));
          Label.Font.Size = 20;
          Label.Font.Colour = Inv.Colour.White;
          Label.Margin.Set(10);
        }

        if (Index >= TableCount - ClientCount)
          Dock.AddClient(Table);
        else
          Dock.AddHeader(Table);
      }

      return Dock;
    }
    private Inv.Panel TableDashboard(Inv.Surface Surface)
    {
      var Scroll = Surface.NewVerticalScroll();

      var Table = Surface.NewTable();
      Scroll.Content = Table;

      Table.Background.Colour = Inv.Colour.LightGray;
      Table.Padding.Set(0, 0, 10, 10);

      const int ContentWidth = 400;
      const int ContentHeight = 400;

      var TableWidth = Math.Max(Surface.Window.Width / ContentWidth, 1);
      var TableHeight = (int)Math.Ceiling(6.0 / TableWidth);

      Table.Compose(TableWidth, TableHeight);

      foreach (var Column in Table.GetColumns())
        Column.Star();

      foreach (var Row in Table.GetRows())
        Row.Auto();

      foreach (var Cell in Table.GetCells())
      {
        var Button = Surface.NewFlatButton();
        Cell.Content = Button;
        Button.Margin.Set(10, 10, 0, 0);
        Button.Background.Colour = Inv.Colour.White;

        var Dock = Surface.NewVerticalDock();
        Button.Content = Dock;

        var HeadingLabel = Surface.NewLabel();
        Dock.AddHeader(HeadingLabel);
        HeadingLabel.Padding.Set(10, 10, 0, 0);
        HeadingLabel.Font.Size = 24;
        HeadingLabel.Font.Medium();
        HeadingLabel.Text = "Title " + Cell.X + ":" + Cell.Y;

        var SubheadingLabel = Surface.NewLabel();
        Dock.AddHeader(SubheadingLabel);
        SubheadingLabel.Padding.Set(10, 0, 10, 10);
        SubheadingLabel.Font.Size = 16;
        SubheadingLabel.Font.Regular();
        SubheadingLabel.Font.Colour = Inv.Colour.DimGray;
        SubheadingLabel.Text = "SUBHEADING";

        var ContentFrame = Surface.NewFrame();
        Dock.AddClient(ContentFrame);
        ContentFrame.Border.Set(0, 1, 0, 0);
        ContentFrame.Border.Colour = Inv.Colour.LightGray;

        var ContentCanvas = Surface.NewCanvas();
        ContentFrame.Content = ContentCanvas;
        ContentCanvas.Size.Set(400, 400);
        ContentCanvas.DrawEvent += (RC) =>
        {
          RC.DrawRectangle(Inv.Colour.White, null, 0, new Inv.Rect(0, 0, ContentWidth, ContentHeight));

          var BarHeight = 50;
          var BarGap = 10;
          var BarY = BarGap;

          for (var Bar = 0; Bar < 6; Bar++)
          {
            RC.DrawRectangle(Inv.Colour.SteelBlue, Inv.Colour.AliceBlue, 1, new Inv.Rect(50, BarY, 250, BarHeight));

            BarY += BarHeight + BarGap;
          }
        };
        ContentCanvas.Draw();
      }

      return Scroll;
    }
    private Inv.Panel TableDockOverlayLayoutIssue(Inv.Surface Surface)
    {
      var LayoutDock = Surface.NewVerticalDock();

      var HeaderTable = Surface.NewTable();
      LayoutDock.AddHeader(HeaderTable);

      var TimerTable = Surface.NewTable();
      TimerTable.Alignment.Stretch();
      TimerTable.Size.SetHeight(48);
      TimerTable.Background.Colour = Inv.Colour.Red;

      var LeftFrame = Surface.NewFrame();
      LeftFrame.Background.Colour = Inv.Colour.Blue;

      var RightFrame = Surface.NewFrame();

      TimerTable.Compose(new Inv.Panel[,]
      {
        { LeftFrame, RightFrame }
      });

      TimerTable.GetRow(0).Star();
      TimerTable.GetColumn(0).Star(85);
      TimerTable.GetColumn(1).Star(15);

      var Label = Surface.NewLabel();
      Label.Background.Colour = Inv.Colour.SteelBlue;
      Label.Text = "Spacer";

      HeaderTable.Compose(new Inv.Panel[,]
      {
        { TimerTable },
        { Label },
      });

      HeaderTable.GetColumn(0).Star();
      HeaderTable.GetRow(0).Star();
      HeaderTable.GetRow(1).Star();

      return LayoutDock;
    }
    private Inv.Panel TableIssue(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();

      var TimeOverlay = Surface.NewOverlay();
      Dock.AddHeader(TimeOverlay);
      TimeOverlay.Margin.Set(50);
      TimeOverlay.Size.SetHeight(100);
      TimeOverlay.Border.Set(1);
      TimeOverlay.Border.Colour = Inv.Colour.Red;

      var ProgressTable = Surface.NewTable();
      TimeOverlay.AddPanel(ProgressTable);
      ProgressTable.Alignment.Stretch();

      var SingleRow = ProgressTable.AddFixedRow(100);

      var LeftColumn = ProgressTable.AddAutoColumn();

      var LeftFrame = Surface.NewFrame();
      ProgressTable.GetCell(LeftColumn, SingleRow).Content = LeftFrame;

      var RightColumn = ProgressTable.AddAutoColumn();

      var RightFrame = Surface.NewFrame();
      ProgressTable.GetCell(RightColumn, SingleRow).Content = RightFrame;

      LeftColumn.Star(50);
      LeftFrame.Background.Colour = Inv.Colour.Blue;

      RightColumn.Star(50);
      RightFrame.Background.Colour = Inv.Colour.Green;

      var TimeLabel = Surface.NewLabel();
      TimeOverlay.AddPanel(TimeLabel);
      TimeLabel.Font.Colour = Inv.Colour.Black;
      TimeLabel.Font.Size = 16;
      TimeLabel.Alignment.Center();
      TimeLabel.Text = string.Format("Ended {0} hrs ago", 23);

      return Dock;
    }
    private Inv.Panel TableLabelIssue(Inv.Surface Surface)
    {
      // TODO: this doesn't work correctly on Android (correct everywhere else).

      var Table = Surface.NewTable();
      Table.Alignment.Center();

      var Column = Table.AddFixedColumn(100);

      var Row = Table.AddStarRow();

      var Label = Surface.NewLabel();
      Label.Background.Colour = Inv.Colour.Green;
      Label.Text = "TILE";

      Table.GetCell(Column, Row).Content = Label;

      return Table;
    }
    private Inv.Panel TableMaterialCard(Inv.Surface Surface)
    {
      // Note: Performance isn't great on Android when this Scroll isn't present, but it is significantly better.
      var Scroll = Surface.NewVerticalScroll();
      Scroll.Background.Colour = Inv.Colour.DodgerBlue;
      //Scroll.Padding.Set(0, 44, 0, 44);
      //Scroll.AdjustEvent += () =>
      //{
      //  var Dimension = Scroll.GetDimension();
      //
      //  if (Dimension.Width < Dimension.Height)
      //    Scroll.Size.Set(484, 860);
      //  else
      //    Scroll.Size.Set(860, 484);
      //};

      var Table = Surface.NewTable();
      Scroll.Content = Table;
      Table.Margin.Set(4, 0, 4, 4);
      Table.AddStarColumn();
      Table.AddStarColumn();

      var TileCount = 0;

      void AddTile()
      {
        var RowIndex = TileCount / (int)2;
        var ColumnIndex = TileCount % (int)2;

        if (Table.RowCount < RowIndex + 1)
          Table.AddStarRow();

        var Card = new MaterialCard();
        Table.GetCell(ColumnIndex, RowIndex).Content = Card;
        Card.Size.SetMinimumHeight(110);

        var Button = Inv.Button.NewFlat();
        Card.AddCustom(Button);
        Button.SingleTapEvent += () =>
        {
        };

        var Dock = Inv.Dock.NewHorizontal();
        Button.Content = Dock;
        Dock.Alignment.CenterStretch();

        var Icon = new Inv.Graphic();
        Dock.AddHeader(Icon);
        Icon.Margin.Set(16, 0, 16, 0);
        Icon.Alignment.Center();
        Icon.Image = Resources.Images.HelpBlack;
        Icon.Size.Set(32);

        var Label = Inv.Label.New();
        Dock.AddClient(Label);
        Label.Margin.Set(0, 0, 16, 0);
        Label.Font.Name = "Roboto";
        Label.Font.Size = 14;
        Label.Font.Medium();
        Label.Text = "[" + TileCount.ToString() + "] TEST TEXT";
        Label.LineWrapping = true;
        Label.Alignment.CenterLeft();
        Label.Justify.Left();

        TileCount++;
      }

      for (var Index = 0; Index < 16; Index++)
        AddTile();

      return Scroll;
    }
    private Inv.Panel TableNumberedList(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var Table = Surface.NewTable();
      Overlay.AddPanel(Table);
      Table.Background.Colour = Inv.Colour.DimGray.Darken(0.50F);

      var NumberColumn = Table.AddAutoColumn();

      var TextColumn = Table.AddStarColumn();

      var PointArray = new[]
      {
        new { Number = 7, Text = "Change the <TargetFrameworkVersion> element from 'v6.0' to 'v7.0'" },
        new { Number = 11, Text = "" }
      };

      Inv.Label LastLabel = null;

      foreach (var Point in PointArray)
      {
        var Row = Table.AddAutoRow();

        var NumberLabel = Surface.NewLabel();
        Table.GetCell(NumberColumn, Row).Content = NumberLabel;
        NumberLabel.Alignment.TopLeft();
        NumberLabel.Margin.Set(20, 0, 0, 0);
        NumberLabel.Padding.Set(0, 0, 10, 0);
        NumberLabel.Font.Colour = Inv.Colour.WhiteSmoke;
        NumberLabel.Font.Size = 20;
        NumberLabel.Font.Weight = Inv.FontWeight.Light;
        NumberLabel.Text = Point.Number + ".";

        var TextLabel = Surface.NewLabel();
        Table.GetCell(TextColumn, Row).Content = TextLabel;
        TextLabel.Margin.Set(0, 0, 20, 0);
        TextLabel.Font.Colour = Inv.Colour.WhiteSmoke;
        TextLabel.Font.Size = 20;
        TextLabel.Font.Weight = Inv.FontWeight.Light;
        TextLabel.LineWrapping = true;
        TextLabel.Text = Point.Text;

        LastLabel = TextLabel;
      }

      var TextEdit = Surface.NewTextEdit();
      Overlay.AddPanel(TextEdit);
      TextEdit.Alignment.BottomStretch();
      TextEdit.Font.Size = 20;
      TextEdit.Font.Colour = Inv.Colour.WhiteSmoke;
      TextEdit.Text = LastLabel.Text;
      TextEdit.ChangeEvent += () => LastLabel.Text = TextEdit.Text;

      var AndroidButton = Surface.NewFlatButton();
      Overlay.AddPanel(AndroidButton);
      AndroidButton.Alignment.Center();
      AndroidButton.Size.Set(80, 80);
      AndroidButton.Background.Colour = Inv.Colour.ForestGreen;
      AndroidButton.SingleTapEvent += () =>
      {
        TextEdit.Text = "Select template 'Class Library' (Portable) but the it gun yfghh yfghh buying ytyghhh gas";
        LastLabel.Text = TextEdit.Text;
      };

      return Overlay;
    }
    private Inv.Panel TableProperty(Inv.Surface Surface)
    {
      var Table = Surface.NewTable();
      Table.Background.Colour = Inv.Colour.Black;
      Table.Alignment.Center();

      var KeyColumn = Table.AddAutoColumn();
      var ValueColumn = Table.AddStarColumn();

      void AddRow(string Key, string Value)
      {
        var Row = Table.AddAutoRow();

        var KeyLabel = Surface.NewLabel();
        Table.GetCell(KeyColumn, Row).Content = KeyLabel;
        KeyLabel.Font.Size = 16;
        KeyLabel.Font.Colour = Inv.Colour.Yellow;
        KeyLabel.Background.Colour = Inv.Colour.Blue;
        KeyLabel.Text = Key;

        var ValueLabel = Surface.NewLabel();
        Table.GetCell(ValueColumn, Row).Content = ValueLabel;
        ValueLabel.Font.Size = 16;
        ValueLabel.Font.Colour = Inv.Colour.White;
        ValueLabel.Justify.Right();
        ValueLabel.Text = Value;
      }

      void AddSeparator()
      {
        var Row = Table.AddAutoRow();

        var Frame = Surface.NewFrame();
        Row.Content = Frame;
        Frame.Background.Colour = Inv.Colour.DimGray;
        Frame.Margin.Set(0, 4, 0, 2);
        Frame.Size.SetHeight(2);
      }

      AddRow("success", "10");
      AddSeparator();
      AddRow("failurationathopalamus", "5");

      return Table;
    }
    private Inv.Panel TableScoring(Inv.Surface Surface)
    {
      var ScoreTable = new ScoreTable(Surface);

      ScoreTable.AddColumn().Set(false, "ONE", "1");
      ScoreTable.AddColumn().Set(false, "TWO", "2");
      ScoreTable.AddColumn().Set(false, "THR", "3");
      ScoreTable.AddColumn().Set(false, "FOU", "4");
      ScoreTable.AddColumn().Set(false, "FIV", "5");
      ScoreTable.AddColumn().Set(false, "SIX", "6");

      return ScoreTable;
    }
    private Inv.Panel TableStarredOnOverlay(Inv.Surface Surface)
    {
      var Graphic = Surface.NewGraphic();
      Graphic.Image = Resources.Images.Waves;
      Graphic.Border.Set(1);
      Graphic.Border.Colour = Inv.Colour.Black;

      var Overlay = Surface.NewOverlay();
      Overlay.Alignment.Center();
      Overlay.Background.Colour = Inv.Colour.Pink;
      Overlay.Margin.Set(10);

      var Frame = Surface.NewFrame();
      Overlay.AddPanel(Graphic);

      var Table = Surface.NewTable();
      Overlay.AddPanel(Table);
      Table.AddStarRow();
      Table.AddStarColumn();

      return Overlay;
    }
    private Inv.Panel TableUniform(Inv.Surface Surface)
    {
      var Table = Surface.NewTable();
      Table.Background.Colour = Inv.Colour.Black;
      Table.Margin.Set(5);
      Table.Compose(9, 9);

      Table.GetColumn(0).Star();
      Table.GetColumn(8).Star();

      var ColumnFrame = Surface.NewFrame();
      Table.GetColumn(1).Content = ColumnFrame;
      ColumnFrame.Background.Colour = Inv.Colour.Pink.Opacity(0.50F);

      var RowFrame = Surface.NewFrame();
      Table.GetRow(1).Content = RowFrame;
      RowFrame.Background.Colour = Inv.Colour.Orange.Opacity(0.50F);

      Table.GetColumn(4).Fixed(150);
      Table.GetRow(4).Fixed(150);

      Table.GetRow(0).Star();
      Table.GetRow(8).Star();

      Table.GetCells().ForEach(Cell =>
      {
        var Button = Surface.NewFlatButton();
        Button.Background.Colour = Inv.Colour.DimGray.Opacity(0.50F);
        Button.Margin.Set(5);
        Button.SingleTapEvent += () => Button.Background.Colour = Inv.Colour.Green;

        var Label = Surface.NewLabel();
        Button.Content = Label;
        Label.Alignment.Center();
        Label.Font.Colour = Inv.Colour.White;
        Label.Font.Size = 20;
        Label.Text = "[" + Cell.X + ", " + Cell.Y + "]";

        Cell.Content = Button;
      });

      return Table;
    }
    private Inv.Panel TopStretchStack(Inv.Surface Surface)
    {
      var S1 = Surface.NewHorizontalStack();
      S1.Background.Colour = Inv.Colour.Orange;
      S1.Alignment.TopStretch();
      S1.Padding.Set(7, 7, 7, 0);

      var Button = Surface.NewFlatButton();
      S1.AddPanel(Button);
      Button.Background.Colour = Inv.Colour.Red;
      Button.Size.Set(48, 48);

      var Graphic = Surface.NewGraphic();
      //S1.AddElement(Graphic);
      Graphic.Background.Colour = Inv.Colour.Red;
      Graphic.Size.Set(48, 48);
      Graphic.Image = LogoImage;

      var DescriptionLabel = Surface.NewLabel();
      //S1.AddElement(DescriptionLabel);
      DescriptionLabel.Alignment.CenterLeft();
      DescriptionLabel.Background.Colour = Inv.Colour.Blue;
      DescriptionLabel.Font.Size = 20;
      //DescriptionLabel.Margin.Set(5, 0, 5, 0);
      DescriptionLabel.Text = "Description goes here";
      return S1;
    }
    private Inv.Panel TimerCountdown(Inv.Surface Surface)
    {
      var Countdown = 10;

      var Label = Surface.NewLabel();
      Label.Alignment.Center();
      Label.Background.Colour = Inv.Colour.Green;
      Label.Size.Set(100, 100);
      Label.Corner.Set(50);
      Label.Justify.Center();
      Label.Elevation.Set(5);
      Label.Font.Colour = Inv.Colour.White;
      Label.Font.Size = 50;
      Label.Text = Countdown.ToString();

      var Timer = Base.Window.NewTimer();
      Timer.IntervalTime = TimeSpan.FromSeconds(0.5);
      Timer.IntervalEvent += () =>
      {
        Countdown--;

        if (Countdown <= 0)
          Timer.Stop();

        Label.Text = Countdown.ToString();
      };
      Timer.Start();

      return Label;
    }
    private Inv.Panel TestPattern(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();
      Overlay.Background.Colour = Inv.Colour.DarkGray;

      var LogicalSizeLabel = Surface.NewLabel();
      Overlay.AddPanel(LogicalSizeLabel);
      LogicalSizeLabel.Alignment.BottomLeft();
      LogicalSizeLabel.Font.Size = 18;

      var FrameRateLabel = Surface.NewLabel();
      Overlay.AddPanel(FrameRateLabel);
      FrameRateLabel.Alignment.BottomRight();
      FrameRateLabel.Font.Size = 18;

      var LocationLabel = Surface.NewLabel();
      Overlay.AddPanel(LocationLabel);
      LocationLabel.Alignment.TopCenter();
      LocationLabel.Font.Size = 18;
      LocationLabel.Text = "CLANG: " + Surface.Window.Application.Audio.GetLength(Resources.Sounds.Clang);
      LocationLabel.Margin.Set(40);

      var Graphic = Surface.NewGraphic();
      Overlay.AddPanel(Graphic);
      Graphic.Alignment.BottomCenter();
      Graphic.Image = Base.Graphics.Tint(LogoImage, Inv.Colour.Red.Opacity(0.50F));
      Graphic.Padding.Set(20);

      var Button = Surface.NewFlatButton();
      Overlay.AddPanel(Button);
      //Button.Visibility.Collapse();
      Button.Alignment.Center();
      //Button.Size.Set(320, 320);
      Button.Padding.Set(50);
      Button.Background.Colour = Inv.Colour.Green;
      Button.SingleTapEvent += () =>
      {
        Base.Window.Application.Audio.Play(Resources.Sounds.Clang, 1, 1, -1);
        Debug.WriteLine("left click");
      };
      Button.ContextTapEvent += () =>
      {
        Base.Window.Application.Audio.Play(Resources.Sounds.Clang, 1, 1, +1);
        Debug.WriteLine("right click");
      };

      var Stack = Surface.NewVerticalStack();
      Button.Content = Stack;
      Stack.Alignment.Center();

      var HeadingFontSize = 72;

      var FirstLabel = Surface.NewLabel();
      Stack.AddPanel(FirstLabel);
      FirstLabel.Background.Colour = Inv.Colour.Blue;
      FirstLabel.Justify.Center();
      FirstLabel.Font.Colour = Inv.Colour.Red;
      FirstLabel.Font.Size = HeadingFontSize;
      FirstLabel.Text = "HW";

      var SecondLabel = Surface.NewLabel();
      Stack.AddPanel(SecondLabel);
      SecondLabel.Background.Colour = Inv.Colour.Yellow;
      SecondLabel.Justify.Center();
      SecondLabel.Font.Colour = Inv.Colour.Purple;
      SecondLabel.Font.Size = HeadingFontSize;
      SecondLabel.Text = "GW";

      //Base.Location.ChangeEvent += (Coordinate) =>
      //{
      //  LocationLabel.Text = Coordinate.Latitude + ", " + Coordinate.Longitude + ", " + Coordinate.Altitude;
      //  /*
      //  var PlacemarkList = Application.Location.Lookup(Coordinate).GetPlacemarks().ToDistinctList();
      //
      //  MobileBroker.Call(C => C.UpdateLocationCoordinate(MobileContext, new Kestral.HQ.Mobile.LocationCoordinate()
      //  {
      //    Latitude = Coordinate.Latitude,
      //    Longitude = Coordinate.Longitude,
      //    Description = PlacemarkList.Count > 0 ? PlacemarkList[0].Name : null
      //  }));*/
      //};

      Surface.ComposeEvent += () =>
      {
        var Dimension = Overlay.GetDimension();

        // compare total window width/height to client area.
        LogicalSizeLabel.Text =
          Dimension.Width + " x " + Dimension.Height + Environment.NewLine +
          Base.Window.Width + " x " + Base.Window.Height;

        FrameRateLabel.Text = string.Format("{0} FPS | {1} PC | {2:F1} MB", Base.Window.DisplayRate.PerSecond, Surface.GetPanelCount(), Base.Process.GetMemoryUsage());
      };

      return Overlay;
    }
    private Inv.Panel Typography(Inv.Surface Surface)
    {
      var Stack = Surface.NewVerticalStack();
      Stack.Alignment.Center();

      var L1 = Surface.NewLabel();
      Stack.AddPanel(L1);
      L1.Text = "This is strikethrough text";
      L1.Font.Strikethrough();
      L1.Font.Size = 20;

      var L2 = Surface.NewLabel();
      Stack.AddPanel(L2);
      L2.Text = "This is SmallCaps text with numbers 0123456789";
      L2.Font.SmallCaps();
      L2.Font.Size = 20;

      var L3 = Surface.NewLabel();
      Stack.AddPanel(L3);
      L3.Text = "This is underline text";
      L3.Font.Underlined();
      L3.Font.Size = 20;

      var L4 = Surface.NewLabel();
      Stack.AddPanel(L4);
      L4.Text = "This is italic text";
      L4.Font.Italics();
      L4.Font.Size = 20;

      var L5 = Surface.NewLabel();
      Stack.AddPanel(L5);
      L5.Text = "This is bold italic text";
      L5.Font.Italics();
      L5.Font.Bold();
      L5.Font.Size = 20;

      var L6 = Surface.NewLabel();
      Stack.AddPanel(L6);
      L6.Text = "This is bold italic underline text";
      L6.Font.Italics();
      L6.Font.Underlined();
      L6.Font.Bold();
      L6.Font.Size = 20;

      var L7 = Surface.NewLabel();
      Stack.AddPanel(L7);
      L7.Text = "This is bold italic underline strikethrough text";
      L7.Font.Italics();
      L7.Font.Underlined();
      L7.Font.Strikethrough();
      L7.Font.Bold();
      L7.Font.Size = 20;

      var L8 = Surface.NewLabel();
      Stack.AddPanel(L8);
      L8.Text = "This is bold italic underline strikethrough smallcaps text";
      //L8.Font.Name = Surface.Window.Application.Device.Target == Inv.PlatformTarget.WindowsDesktop ? "Pescadero" : null; // NOTE: WPF Segoe UI doesn't support this combination of styles (UWP Segoe UI _does_ support this.
      L8.Font.Italics();
      L8.Font.Underlined();
      L8.Font.Strikethrough();
      L8.Font.SmallCaps();
      L8.Font.Bold();
      L8.Font.Size = 20;

      var E3 = Surface.NewTextEdit();
      Stack.AddPanel(E3);
      E3.Text = "This is an underline text edit";
      E3.Font.IsUnderlined = true;
      E3.Font.Size = 20;

      return Stack;
    }
    private Inv.Panel ImageViewer(Inv.Surface Surface)
    {
      var Viewer = new ImageViewer(Surface);

      Viewer.Image = Resources.Images.RequestForm2;
      //Viewer.Image = Resources.Images.PhoenixLogo960x540;

      return Viewer;
    }
    private Inv.Panel ImageScaling(Inv.Surface Surface)
    {
      // This test case compares the rendering of a graphic to a canvas DrawImage. They should be identical.

      var Dock = Surface.NewVerticalDock();

      var Image = Resources.Images.PhoenixLogo500x500;
      var Size = 64;
      var ScaledImage = Surface.Window.Application.Graphics.Resize(Image, new Inv.Dimension(64, 64));

      var Graphic = Surface.NewGraphic();
      Dock.AddHeader(Graphic);
      Graphic.Alignment.Center();
      Graphic.Size.Set(Size, Size);
      Graphic.Image = Image;

      var Canvas = Surface.NewCanvas();
      Dock.AddHeader(Canvas);
      Canvas.Alignment.Center();
      Canvas.Size.Set(Size, Size * 2);
      Canvas.DrawEvent += (DC) =>
      {
        DC.DrawImage(Image, new Inv.Rect(0, 0, Size, Size));

        DC.DrawImage(ScaledImage, new Inv.Rect(0, Size, Size, Size));
      };
      Canvas.Draw();

      return Dock;
    }
    private Inv.Panel TablePadding(Inv.Surface Surface)
    {
      var Table = Inv.Table.New();
      Table.Padding.Set(50);
      Table.Background.Colour = Inv.Colour.LightGray;

      var Column = Table.AddStarColumn();

      var ColumnLabel = Inv.Label.New();
      Column.Content = ColumnLabel;
      ColumnLabel.Background.Colour = Inv.Colour.Purple.Opacity(0.50F);

      var Row = Table.AddStarRow();

      var RowLabel = Inv.Label.New();
      Row.Content = RowLabel;
      RowLabel.Background.Colour = Inv.Colour.Green.Opacity(0.50F);

      var CellLabel = Inv.Label.New();
      Table.GetCell(Column, Row).Content = CellLabel;
      CellLabel.Text = "Test";
      CellLabel.Font.Colour = Inv.Colour.DimGray;
      CellLabel.Font.Size = 85;
      CellLabel.Background.Colour = Inv.Colour.Yellow.Opacity(0.50F);
      CellLabel.Font.Name = "Questrial";
      CellLabel.Justify.Center();

      return Table;
    }
    private Inv.Panel Tooltips(Inv.Surface Surface)
    {
      var Button = Surface.NewFlatButton();
      Button.Alignment.Center();
      Button.Size.Set(200, 200);
      Button.Background.Colour = Inv.Colour.DarkGreen;
      Button.Border.Set(10);
      Button.Border.Colour = Inv.Colour.ForestGreen;
      Button.IsFocusable = true;
      //Button.IsEnabled = false;

      Button.Tooltip.ShowEvent += () =>
      {
        var Label = Surface.NewLabel();
        Button.Tooltip.Content = Label;
        Label.Justify.Center();
        Label.Font.Size = 14;
        Label.Text = "Hello Tooltip!";
      };
      Button.Tooltip.HideEvent += () =>
      {
        Button.Tooltip.Content = null;
      };

      return Button;
    }
    private Inv.Panel TransitionClipping(Inv.Surface Surface)
    {
      // NOTE: transitions before the panels are in the visual tree does not work on any platform.

      var Dock = Surface.NewVerticalStack();

      var FirstFrame = Surface.NewFrame();
      Dock.AddPanel(FirstFrame);
      FirstFrame.Background.Colour = Inv.Colour.DarkRed;
      FirstFrame.Size.Set(200, 200);

      var FirstLabel = Surface.NewLabel();
      FirstLabel.Background.Colour = Inv.Colour.LightSalmon;
      FirstLabel.Text = "FIRST LABEL";

      var SecondFrame = Surface.NewFrame();
      Dock.AddPanel(SecondFrame);
      SecondFrame.Background.Colour = Inv.Colour.DarkGreen;
      SecondFrame.Size.Set(200, 200);

      var SecondLabel = Surface.NewLabel();
      SecondLabel.Background.Colour = Inv.Colour.LightGray;
      SecondLabel.Text = "SECOND LABEL";

      var Button = Surface.NewFlatButton();
      Dock.AddPanel(Button);
      Button.Background.Colour = Inv.Colour.Purple;
      Button.Size.Set(200, 200);
      Button.SingleTapEvent += () =>
      {
        FirstFrame.Transition(FirstFrame.Content == null ? FirstLabel : null).CarouselDescend();
        SecondFrame.Transition(SecondFrame.Content == null ? SecondLabel : null).CarouselAscend();
      };
      Button.SingleTap();

      return Dock;
    }
    private Inv.Panel TransitionReentry(Inv.Surface Surface)
    {
      var FirstButton = Surface.NewFlatButton();
      FirstButton.Size.Set(128, 128);
      FirstButton.Alignment.TopCenter();
      FirstButton.Background.Colour = Inv.Colour.DarkGreen;

      var Dialog = Surface.Window.NewSurface();

      var SecondButton = Dialog.NewFlatButton();
      Dialog.Content = SecondButton;
      SecondButton.Size.Set(128, 128);
      SecondButton.Alignment.BottomCenter();
      SecondButton.Background.Colour = Inv.Colour.DarkRed;

      FirstButton.SingleTapEvent += () =>
      {
        if (Surface.Window.ActiveSurface == Dialog)
          throw new Exception("Should not be able to double transition to the same surface.");

        var Transition = Surface.Window.Transition(Dialog);
        Transition.Duration = TimeSpan.FromSeconds(2);
        Transition.Fade();
      };
      SecondButton.SingleTapEvent += () =>
      {
        if (Surface.Window.ActiveSurface == Surface)
          throw new Exception("Should not be able to double transition to the same surface.");

        var Transition = Surface.Window.Transition(Surface);
        Transition.Duration = TimeSpan.FromSeconds(2);
        Transition.Fade();
      };

      return FirstButton;
    }
    private Inv.Panel TransitionTypes(Inv.Surface Surface)
    {
      var Dock = Surface.NewHorizontalDock();
      Dock.Background.Colour = Inv.Colour.Black;

      var FadeFrame = Surface.NewFrame();
      Dock.AddClient(FadeFrame);

      var FadeFirstButton = Surface.NewFlatButton();
      var FadeSecondButton = Surface.NewFlatButton();

      FadeFirstButton.SingleTapEvent += () => FadeFrame.Transition(FadeSecondButton).Fade();
      FadeSecondButton.SingleTapEvent += () => FadeFrame.Transition(FadeFirstButton).Fade();

      var FadeFirstLabel = Surface.NewLabel();
      FadeFirstButton.Content = FadeFirstLabel;
      FadeFirstLabel.Background.Colour = Inv.Colour.LightSlateGray;
      FadeFirstLabel.Font.Size = 30;
      FadeFirstLabel.Justify.Center();
      FadeFirstLabel.Text = "FIRST FADE";

      var FadeSecondLabel = Surface.NewLabel();
      FadeSecondButton.Content = FadeSecondLabel;
      FadeSecondLabel.Background.Colour = Inv.Colour.LightSeaGreen;
      FadeSecondLabel.Font.Size = 30;
      FadeSecondLabel.Justify.Center();
      FadeSecondLabel.Text = "SECOND FADE";

      FadeFrame.Transition(FadeFirstButton);

      var CarouselFrame = Surface.NewFrame();
      Dock.AddClient(CarouselFrame);

      var CarouselNextButton = Surface.NewFlatButton();
      var CarouselBackButton = Surface.NewFlatButton();
      var CarouselAscendButton = Surface.NewFlatButton();
      var CarouselDescendButton = Surface.NewFlatButton();

      CarouselNextButton.SingleTapEvent += () => CarouselFrame.Transition(CarouselBackButton).CarouselNext();
      CarouselBackButton.SingleTapEvent += () => CarouselFrame.Transition(CarouselAscendButton).CarouselPrevious();
      CarouselAscendButton.SingleTapEvent += () => CarouselFrame.Transition(CarouselDescendButton).CarouselAscend();
      CarouselDescendButton.SingleTapEvent += () => CarouselFrame.Transition(CarouselNextButton).CarouselDescend();

      var CarouselNextLabel = Surface.NewLabel();
      CarouselNextButton.Content = CarouselNextLabel;
      CarouselNextLabel.Background.Colour = Inv.Colour.LightBlue;
      CarouselNextLabel.Font.Size = 30;
      CarouselNextLabel.Justify.Center();
      CarouselNextLabel.Text = "NEXT CAROUSEL";

      var CarouselBackLabel = Surface.NewLabel();
      CarouselBackButton.Content = CarouselBackLabel;
      CarouselBackLabel.Background.Colour = Inv.Colour.LightGreen;
      CarouselBackLabel.Font.Size = 30;
      CarouselBackLabel.Justify.Center();
      CarouselBackLabel.Text = "BACK CAROUSEL";

      var CarouselAscendLabel = Surface.NewLabel();
      CarouselAscendButton.Content = CarouselAscendLabel;
      CarouselAscendLabel.Background.Colour = Inv.Colour.LightCoral;
      CarouselAscendLabel.Font.Size = 30;
      CarouselAscendLabel.Justify.Center();
      CarouselAscendLabel.Text = "ASCEND CAROUSEL";

      var CarouselDescendLabel = Surface.NewLabel();
      CarouselDescendButton.Content = CarouselDescendLabel;
      CarouselDescendLabel.Background.Colour = Inv.Colour.LightPink;
      CarouselDescendLabel.Font.Size = 30;
      CarouselDescendLabel.Justify.Center();
      CarouselDescendLabel.Text = "DESCEND CAROUSEL";

      CarouselFrame.Transition(CarouselNextButton);

      return Dock;
    }
    private Inv.Panel UnhandledException(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();
      //Dock.Background.Colour = Inv.Colour.SteelBlue;

      var Button = Surface.NewFlatButton();
      Dock.AddClient(Button);
      Button.Background.Colour = Inv.Colour.DarkGray;
      Button.Padding.Set(100);

      var Label = Surface.NewLabel();
      Button.Content = Label;
      Label.Text = "Press for an exception";

      var Memo = Surface.NewMemo();
      Dock.AddClient(Memo);
      Memo.IsReadOnly = true;
      Memo.Font.Monospaced();
      Memo.Font.Colour = Inv.Colour.White;
      Memo.Text = "<stack trace>";

      Button.SingleTapEvent += () =>
      {
        try
        {
          Base.Directory.NewAsset("TEST.TExt").Open();

          throw new Exception("i'm an exception");
        }
        catch (Exception Ex)
        {
          Memo.Text = Ex.StackTrace;
        }

        var FaultLogFile = Surface.Window.Application.Directory.NewFolder("Logs").NewFile("Fault.log");
        FaultLogFile.AsText().WriteAll(Memo.Text);

        var EmailMessage = Surface.Window.Application.Email.NewMessage();
        EmailMessage.Subject = "Fault";
        EmailMessage.Attach("Fault.log", FaultLogFile);
        EmailMessage.Send();
      };

      return Dock;
    }
    private Inv.Panel Vault(Inv.Surface Surface)
    {
      var Button = Surface.NewFlatButton();
      Button.Background.Colour = Inv.Colour.DarkGray;
      Button.Padding.Set(100);

      var Label = Surface.NewLabel();
      Button.Content = Label;
      Label.Text = "Save secret";

      Button.SingleTapEvent += () =>
      {
        var Secret = Surface.Window.Application.Vault.NewSecret("Credentials");

        //Secret.Delete();

        Secret.Load();
        
        Secret.Properties["Domain"] = "kestral";
        Secret.Properties["Username"] = "phxdemo";
        Secret.Properties["Password"] = "lartsek137!";
        
        Secret.Save();
      };

      return Button;
    }
    private Inv.Panel WebBrowser(Inv.Surface Surface)
    {
      var Dock = Surface.NewVerticalDock();

      var Browser = Surface.NewBrowser();
      Dock.AddClient(Browser);
      Browser.Alignment.BottomStretch();
      Browser.Background.Colour = Inv.Colour.DarkGray;
      Browser.FetchEvent += (Fetch) =>
      {
        var UriText = Fetch.Uri.ToString();

        Debug.WriteLine(UriText);

        if (UriText.StartsWith("https://arstechnica.com", StringComparison.OrdinalIgnoreCase))
        {
          Fetch.Cancel();

          Browser.LoadUri(new Uri("https://mercury.postlight.com/amp?url=" + UriText));
        }
      };

      foreach (var UriText in new[] { "https://www.google.com/", "http://www.kestral.com.au/", "http://m.bom.gov.au/wa/perth/", "https://mercury.postlight.com/amp?url=https://arstechnica.com/gaming/2017/08/n64-classic-goldeneye-007-turns-20-today-and-so-does-my-awful-review-of-it/" })
      {
        var Uri = new Uri(UriText);

        var Button = Surface.NewFlatButton();
        Dock.AddHeader(Button);
        Button.Border.Set(2);
        Button.Border.Colour = Inv.Colour.DimGray;
        Button.Background.Colour = Inv.Colour.SteelBlue;
        Button.Size.SetHeight(44);
        Button.SingleTapEvent += () => Browser.LoadUri(Uri);

        var Label = Surface.NewLabel();
        Button.Content = Label;
        Label.Font.Colour = Inv.Colour.White;
        Label.Text = Uri.Host;
      }

      //var Buffer = Resources.Documents.Guide.GetBuffer();
      //Browser.LoadHtml(System.Text.Encoding.UTF8.GetString(Buffer, 0, Buffer.Length));

      //Browser.LoadHtml("<p>Howdy!</p>");

      Browser.LoadHtml(
@"<html>
<head>
  <meta name=viewport content=\""width=device-width, initial-scale=1\""/>
  <style>
    P { font-size: 24; font: -apple-system-body}
  </style>
</head>
<body>
<p>Big-time BLT believers live among us. And they know what they want.</p>
<p>Big-time BLT believers live among us. And they know what they want.</p>
</body>
</html>");

      return Dock;
    }
    private Inv.Panel WrappingPanel(Inv.Surface Surface)
    {
      var Scroll = Surface.NewHorizontalScroll();

      var Wrap = Surface.NewVerticalWrap();
      Wrap.Background.Colour = Inv.Colour.Orange;
      Wrap.Alignment.Stretch();
      Wrap.Padding.Set(10);

      for (var i = 0; i < 100; i++)
      {
        var DescriptionLabel = Surface.NewLabel();
        Wrap.AddPanel(DescriptionLabel);
        DescriptionLabel.Alignment.CenterLeft();
        DescriptionLabel.Background.Colour = Inv.Colour.Blue;
        DescriptionLabel.Justify.Center();
        DescriptionLabel.Font.Size = 20;
        DescriptionLabel.Font.Colour = Inv.Colour.White;
        DescriptionLabel.Size.Set(48 + i, 48 + i);
        DescriptionLabel.Text = i.ToString();
      }

      //return Wrap;

      Scroll.Content = Wrap;

      return Scroll;
    }
    private Inv.Panel ScrollClickThrough(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var Button = Surface.NewFlatButton();
      Overlay.AddPanel(Button);
      Button.Background.Colour = Inv.Colour.DarkGray;
      Button.SingleTapEvent += () =>
      {
        Button.Background.Colour = Inv.Colour.Green;
      };

      var Scroll = Surface.NewVerticalScroll();
      Overlay.AddPanel(Scroll);

      var Label = Surface.NewLabel();
      Scroll.Content = Label;
      Label.Background.Colour = Inv.Colour.White;
      Label.Alignment.Center();
      Label.Text =
        "This is an overlay with a button and a vertical scroll. " +
        "Inside the scroll is this label. " +
        "You should be able to tap outside of the label to reach the button. " +
        "When the button is pressed it will change colour to green. " +
        "IN FACT, none of platforms support this test case. " +
        "Well... at least it is consistent?";

      return Overlay;
    }
    private Inv.Panel StackRebuild(Inv.Surface Surface)
    {
      var Overlay = Surface.NewOverlay();

      var Button = Surface.NewFlatButton();
      Overlay.AddPanel(Button);
      Button.Background.Colour = Inv.Colour.DarkCyan;
      Button.Alignment.Center();
      Button.Padding.Set(50);

      var FirstStack = Surface.NewHorizontalStack();
      Overlay.AddPanel(FirstStack);
      FirstStack.Alignment.TopCenter();

      Button.SingleTapEvent += () =>
      {
        FirstStack.RemovePanels();

        var SecondStack = Surface.NewVerticalStack();
        Overlay.AddPanel(SecondStack);

        var Label = Surface.NewLabel();
        SecondStack.AddPanel(Label);
        Label.Background.Colour = Inv.Colour.White;
        Label.Alignment.Center();
        Label.Text =
          "This label will be removed and added back to the stack.";
      };

      return Overlay;
    }
    private Inv.Panel Palette(Inv.Surface Surface)
    {
      var Palette = new CustomPalette(Surface);

      var Dictionary = new Dictionary<int, PaletteFilter>();

      void Compose()
      {
        Palette.Compose(() =>
        {
          for (var i = 0; i < 80; i++)
          {
            var Filter = Dictionary.GetOrAdd(i, Key => Palette.NewFilter(Key.ToString(), null));
            Filter.Text = i.ToString();
            Palette.AddFilter(Filter);
          }
        });
      }

      Compose();

      Palette.AddBand().AddToggle("REFRESH").SingleTapEvent += () => Compose();

      return Palette;
    }

    private string LoadBookmark()
    {
      try
      {
        return (BookmarkFile.Exists() ? BookmarkFile.AsText().ReadAll() : "").Trim().EmptyAsNull();
      }
      catch
      {
        return null;
      }
    }
    private void SaveBookmark()
    {
      try
      {
        if (MethodItemList != null)
          BookmarkFile.AsText().WriteAll(MethodItemList[MethodIndex].DrawerItem.Caption);
      }
      catch
      {
        // NOTE: nothing to do if we fail to save the bookmark.
      }
    }
    private Inv.Transition TransitionMethod(MethodItem MethodItem)
    {
      var ReflectionSurface = (Inv.Surface)NavigationSurface;

      var MethodPanel = (Inv.Panel)MethodItem.MethodInfo.Invoke(this, new object[] { ReflectionSurface });

      NavigationSurface.Toolbar.Title = MethodItem.MethodInfo.Name.PascalCaseToTitleCase();
      var Result = NavigationSurface.TransitionClient(MethodPanel);
      NavigationSurface.Rearrange();

      foreach (var OtherItem in MethodItemList)
        OtherItem.DrawerItem.IsHighlighted = OtherItem == MethodItem;

      var Position = MethodItemList.IndexOf(MethodItem);

      if (MethodIndex != Position)
      {
        this.MethodIndex = Position;
        SaveBookmark(); // save the bookmark on transition to a different item.
      }

      return Result;
    }
    private void Compile(Func<Inv.Surface, Inv.Panel> Default, string Bookmark, bool IsDirect)
    {
      if (IsDirect)
      {
        var DirectSurface = Base.Window.NewSurface();

        DirectSurface.Content = Default(DirectSurface);

        DirectSurface.KeystrokeEvent += (Keystroke) =>
        {
          if (Keystroke.Modifier.IsNone)
          {
            switch (Keystroke.Key)
            {
              case Inv.Key.F12:
                DirectSurface.Content = Default(DirectSurface);
                DirectSurface.Rearrange();
                break;
            }
          }

          Debug.WriteLine("KEYSTROKE: " + Keystroke.ToString());
        };

        InvTest.Shell.InstrumentationClearEvent();

        var FirstFrame = true;
        var FirstSW = new Stopwatch();
        DirectSurface.ArrangeEvent += () =>
        {
          Debug.WriteLine("direct loading...");
          FirstFrame = true;
        };
        DirectSurface.ComposeEvent += () =>
        {
          if (FirstFrame)
          {
            FirstFrame = false;
            FirstSW.Restart();
          }
          else if (FirstSW.IsRunning)
          { 
            FirstSW.Stop();
            Debug.WriteLine("direct loaded..." + FirstSW.ElapsedMilliseconds + " ms");

            InvTest.Shell.InstrumentationPrintEvent();
          }
        };

        Base.Window.Transition(DirectSurface);
      }
      else
      {
        var TypeInfo = typeof(TestConsole).GetReflectionInfo();
        var MethodList = TypeInfo.GetReflectionMethods().Where(M => M.GetParameters().Length == 1 && M.GetParameters()[0].ParameterType == typeof(Inv.Surface) && M.ReturnType == typeof(Inv.Panel)).ToDistinctList();
        MethodList.Sort((A, B) => string.Compare(A.Name, B.Name));

        var DefaultMethodInfo = Default?.GetReflectionInfo();

        this.MethodItemList = new Inv.DistinctList<MethodItem>(MethodList.Count);

        this.NavigationSurface = Base.NewNavigationSurface();

        NavigationSurface.Title = "Invention";
        NavigationSurface.Toolbar.Title = "Material";
        //NavigationSurface.Toolbar.AddTrailingIcon(Resources.Images.SearchWhite);
        NavigationSurface.KeystrokeEvent += (Keystroke) =>
        {
          if (Keystroke.Modifier.IsNone)
          {
            switch (Keystroke.Key)
            {
              case Inv.Key.Escape:
                ExitCheck();
                break;

              case Inv.Key.F12:
                TransitionMethod(MethodItemList[MethodIndex]);
                break;

              case Inv.Key.Up:
                NavigationSurface.GestureBackward();
                break;

              case Inv.Key.Down:
                NavigationSurface.GestureForward();
                break;

              case Inv.Key.Period:
                NavigationSurface.Drawer.ScrollItem(MethodItemList[MethodIndex].DrawerItem);
                break;
            }
          }

          Debug.WriteLine("KEYSTROKE: " + Keystroke.ToString());
        };
        NavigationSurface.GestureBackwardEvent += () =>
        {
          var BackwardPosition = MethodIndex - 1;
          if (BackwardPosition < 0)
            BackwardPosition = MethodList.Count - 1;

          TransitionMethod(MethodItemList[BackwardPosition]).CarouselPrevious();
        };
        NavigationSurface.GestureForwardEvent += () =>
        {
          var ForwardPosition = MethodIndex + 1;
          if (ForwardPosition >= MethodItemList.Count)
            ForwardPosition = 0;

          TransitionMethod(MethodItemList[ForwardPosition]).CarouselNext();
        };

        var FirstFrame = true;
        var FirstSW = new Stopwatch();
        NavigationSurface.ArrangeEvent += () =>
        {
          var MethodInfo = MethodList[MethodIndex];

          Debug.WriteLine(MethodInfo.Name + ": loading...");
          FirstFrame = true;

          InvTest.Shell.InstrumentationClearEvent();
        };
        NavigationSurface.ComposeEvent += () =>
        {
          var MethodInfo = MethodList[MethodIndex];

          if (FirstFrame)
          {
            FirstFrame = false;
            FirstSW.Restart();
          }
          else if (FirstSW.IsRunning)
          {
            // SecondFrame.
            FirstSW.Stop();
            Debug.WriteLine(MethodInfo.Name + ": " + FirstSW.ElapsedMilliseconds + " ms");

            InvTest.Shell.InstrumentationPrintEvent();
          }
        };

        var DefaultPosition = -1;

        foreach (var MethodInfo in MethodList)
        {
          var DrawerItem = NavigationSurface.Drawer.AddItem(MethodInfo.Name.PascalCaseToTitleCase());

          var MethodItem = new MethodItem(DrawerItem, MethodInfo);

          DrawerItem.SingleTapEvent += () => TransitionMethod(MethodItem).Fade();

          if (Bookmark != null && DrawerItem.Caption == Bookmark)
            DefaultPosition = MethodItemList.Count; // always go with the bookmark if possible.
          else if (Default != null && Default.GetReflectionInfo() == MethodInfo && DefaultPosition < 0)
            DefaultPosition = MethodItemList.Count; // otherwise fallback to the default method.

          MethodItemList.Add(MethodItem);
        }

        if (DefaultPosition < 0)
          DefaultPosition = 0;

        var DefaultItem = MethodItemList[DefaultPosition];
        TransitionMethod(DefaultItem);

        NavigationSurface.Drawer.ScrollItem(DefaultItem.DrawerItem);

        Base.Window.Transition(NavigationSurface);
      }
    }
    private Inv.Label NewLabel(Inv.Surface Surface, string Text, Inv.Colour Colour)
    {
      var Result = Surface.NewLabel();
      Result.Text = Text;
      Result.Font.Size = 20;
      Result.Background.Colour = Colour;
      return Result;
    }
    private bool IsHorizontalStretch(Inv.Placement Placement)
    {
      return Placement == Inv.Placement.Stretch || Placement == Inv.Placement.TopStretch || Placement == Inv.Placement.CenterStretch || Placement == Inv.Placement.BottomStretch;
    }
    private bool IsVerticalStretch(Inv.Placement Placement)
    {
      return Placement == Inv.Placement.Stretch || Placement == Inv.Placement.StretchLeft || Placement == Inv.Placement.StretchCenter || Placement == Inv.Placement.StretchRight;
    }
    private void ExitCheck()
    {
      var LastSurface = Base.Window.ActiveSurface;

      if (LastSurface != CloseSurface)
      {
        if (CloseSurface.Content == null)
        {
          var CloseStack = CloseSurface.NewVerticalStack();
          CloseSurface.Content = CloseStack;
          CloseStack.Alignment.Center();

          var CloseButton = CloseSurface.NewFlatButton();
          CloseStack.AddPanel(CloseButton);
          CloseButton.Background.Colour = Inv.Colour.Red;
          CloseButton.Padding.Set(20);
          CloseButton.Margin.Set(20);
          CloseButton.SingleTapEvent += () => Base.Exit();

          var CloseLabel = CloseSurface.NewLabel();
          CloseButton.Content = CloseLabel;
          CloseLabel.Font.Size = 40;
          CloseLabel.Font.Colour = Inv.Colour.White;
          CloseLabel.Justify.Center();
          CloseLabel.Text = "CLOSE";

          var CancelButton = CloseSurface.NewFlatButton();
          CloseStack.AddPanel(CancelButton);
          CancelButton.Background.Colour = Inv.Colour.WhiteSmoke;
          CancelButton.Padding.Set(20);
          CancelButton.Margin.Set(20);
          CancelButton.SingleTapEvent += () => Base.Window.Transition(LastSurface).Fade();

          var CancelLabel = CloseSurface.NewLabel();
          CancelButton.Content = CancelLabel;
          CancelLabel.Font.Size = 40;
          CancelLabel.Font.Colour = Inv.Colour.Black;
          CancelLabel.Justify.Center();
          CancelLabel.Text = "CANCEL";
        }

        var Transition = Base.Window.Transition(CloseSurface);
        //Transition.Duration = TimeSpan.FromSeconds(2);
        Transition.Fade();
      }
    }

    private Inv.Surface CloseSurface;
    private readonly Inv.Image LogoImage;
    private NavigationSurface NavigationSurface;
    private Inv.DistinctList<MethodItem> MethodItemList;
    private int MethodIndex;
    private Inv.File BookmarkFile;

    public sealed class MaterialCard : Inv.Panel<Inv.Frame>
    {
      public MaterialCard()
      {
        // Constant width, variable height.

        // Max height is the maximum available height of the platform (i.e. screen height)

        // Cards don't scroll internally (they expand) on mobile, but can scroll on desktop.

        // https://material.io/guidelines/components/cards.html#cards-actions
        // Content
        // - Optional header
        // - Rich media
        // - Supporting text 
        // - Supplemental actions (action area 4)

        this.Base = Inv.Frame.New();

        this.Dock = Inv.Dock.NewVertical();
        Base.Content = Dock;
        Dock.Background.Colour = Inv.Colour.White;
        Dock.Corner.Set(2);
        Dock.Margin.Set(8);
        Dock.Elevation.Set(2);
      }

      public Inv.Size Size => Base.Size;

      public void AddCustom(Inv.Panel Content)
      {
        Dock.AddHeader(Content);
      }

      private Inv.Dock Dock;
    }

    public sealed class MaterialFieldContainer : Inv.Panel<Inv.Stack>
    {
      public MaterialFieldContainer()
      {
        this.IsEmpty = true;
        this.IsFocused = false;

        this.Base = Inv.Stack.NewVertical();

        this.ContentOverlay = Inv.Overlay.New();
        Base.AddPanel(ContentOverlay);
        ContentOverlay.Size.SetMinimumHeight(56);
        ContentOverlay.Corner.Set(4, 4, 0, 0);
        ContentOverlay.Background.Colour = Inv.Colour.WhiteSmoke;
        ContentOverlay.Border.Set(0, 0, 0, 1);
        ContentOverlay.Border.Colour = Inv.Colour.DarkGray;
        ContentOverlay.Padding.Set(12, 0, 0, 0);

        this.GhostLabel = Inv.Label.New();
        ContentOverlay.AddPanel(GhostLabel);
        GhostLabel.Font.Name = "Roboto";
        GhostLabel.Font.Size = 16;
        GhostLabel.Font.Colour = Inv.Colour.DimGray.Lighten(0.05F);
        GhostLabel.Alignment.CenterStretch();
        GhostLabel.Margin.Set(0, 0, 0, 1);

        this.ClientDock = Inv.Dock.NewVertical();
        ContentOverlay.AddPanel(ClientDock);
        ClientDock.Margin.Set(0, 0, 0, 1);

        this.CaptionLabel = Inv.Label.New();
        ClientDock.AddHeader(CaptionLabel);
        CaptionLabel.Margin.Set(0, 8, 0, 0);
        //CaptionLabel.Font.Bold();
        CaptionLabel.Font.Name = "Roboto";
        CaptionLabel.Font.Size = 12;
        //CaptionLabel.Size.SetHeight(20);
        //CaptionLabel.Alignment.BottomLeft();

        var ContentDock = Inv.Dock.NewHorizontal();
        ClientDock.AddClient(ContentDock);

        this.ContentFrame = Inv.Frame.New();
        ContentDock.AddClient(ContentFrame);
        //ContentFrame.Size.SetHeight(24);
        ContentFrame.Padding.Set(0, 4, 0, 4);
        ContentFrame.Border.Set(0, 0, 0, 2);
        //ContentFrame.Border.Colour = Theme.PrimaryColour;

        this.ErrorIcon = new MaterialIcon();
        ContentDock.AddFooter(ErrorIcon);
        ErrorIcon.Size.Set(24);
        ErrorIcon.Colour = Inv.Colour.Red;
        ErrorIcon.Image = Resources.Images.AccountBoxBlack;
        ErrorIcon.Margin.Set(8, 0, 8, 8);
        ErrorIcon.Alignment.Center();

        this.HelperLabel = Inv.Label.New();
        Base.AddPanel(HelperLabel);
        HelperLabel.Margin.Set(12, 6, 0, 0);
        //HelperLabel.Font.Bold();
        HelperLabel.Font.Name = "Roboto";
        HelperLabel.Font.Size = 11;
        //HelperLabel.Font.Colour = Theme.Adjust(Inv.Colour.Black, 0.54F);
        HelperLabel.Size.SetHeight(16);
        HelperLabel.Alignment.BottomLeft();

        Refresh();
      }

      public string CaptionText
      {
        get { return CaptionLabel.Text; }
        set
        {
          CaptionLabel.Text = value;
          GhostLabel.Text = value;
        }
      }
      public string ErrorText
      {
        get { return ErrorTextField; }
        set
        {
          this.ErrorTextField = value;

          Refresh();
        }
      }
      public string HelperText
      {
        get { return HelperTextField; }
        set
        {
          this.HelperTextField = value;

          Refresh();
        }
      }
      public Inv.Panel Content
      {
        set { ContentFrame.Content = value; }
      }
      public Inv.Margin Margin => Base.Margin;
      public Inv.Alignment Alignment => Base.Alignment;
      public Inv.Visibility Visibility => Base.Visibility;

      internal bool HasFocus()
      {
        return IsFocused;
      }
      internal void GotFocus()
      {
        if (!IsFocused)
        {
          this.IsFocused = true;

          Refresh();
        }
      }
      internal void LostFocus()
      {
        if (IsFocused)
        {
          this.IsFocused = false;

          Refresh();
        }
      }
      internal void SetEmpty(bool IsEmpty)
      {
        this.IsEmpty = IsEmpty;

        Refresh();
      }

      private void Refresh()
      {
        GhostLabel.Visibility.Set(IsEmpty && !IsFocused);

        CaptionLabel.Visibility.Set(!IsEmpty || IsFocused);
        CaptionLabel.Font.Colour = !string.IsNullOrWhiteSpace(ErrorText) ? Inv.Colour.Red : IsFocused ? Inv.Colour.DarkBlue : Inv.Colour.DarkGray;

        HelperLabel.Text = string.IsNullOrWhiteSpace(ErrorTextField) ? HelperTextField : ErrorTextField;
        HelperLabel.Font.Colour = string.IsNullOrWhiteSpace(ErrorTextField) ? Inv.Colour.DarkGray : Inv.Colour.Red;
        ErrorIcon.Visibility.Set(IsFocused && !string.IsNullOrWhiteSpace(ErrorTextField));

        if (!string.IsNullOrWhiteSpace(ErrorTextField))
          ContentOverlay.Border.Colour = Inv.Colour.Red;
        else
          ContentOverlay.Border.Colour = IsFocused ? Inv.Colour.DarkBlue : Inv.Colour.DarkGray;

        ContentOverlay.Border.Set(0, 0, 0, IsFocused ? 2 : 1);
        GhostLabel.Margin.Set(0, 0, 0, IsFocused ? 0 : 1);
        ClientDock.Margin.Set(0, 0, 0, IsFocused ? 0 : 1);
      }

      private Inv.Overlay ContentOverlay;
      private Inv.Label GhostLabel;
      private Inv.Dock ClientDock;
      private Inv.Label CaptionLabel;
      private Inv.Frame ContentFrame;
      private MaterialIcon ErrorIcon;
      private Inv.Label HelperLabel;
      private bool IsEmpty;
      private bool IsFocused;
      private string ErrorTextField;
      private string HelperTextField;
    }

    public sealed class MaterialIcon : Inv.Panel<Inv.Frame>
    {
      public MaterialIcon()
      {
        this.Base = Inv.Frame.New();

        this.Graphic = Inv.Graphic.New();
        Base.Content = Graphic;
        Graphic.Size.Set(24);

        this.ColourField = Inv.Colour.DarkBlue;
      }

      public Inv.Size Size => Graphic.Size;
      public Inv.Margin Margin => Base.Margin;
      public Inv.Padding Padding => Base.Padding;
      public Inv.Alignment Alignment => Base.Alignment;
      public Inv.Visibility Visibility => Base.Visibility;
      public Inv.Image Image
      {
        get { return ImageField; }
        set
        {
          ImageField = value;

          Refresh();
        }
      }
      public Inv.Colour Colour
      {
        get { return ColourField; }
        set
        {
          if (ColourField != value)
          {
            ColourField = value;

            Refresh();
          }
        }
      }

      private void Refresh()
      {
        if (ImageField != null)
          Graphic.Image = Inv.Application.Access().Graphics.Tint(ImageField, ColourField);
      }

      private Inv.Colour ColourField;
      private Inv.Image ImageField;
      private Inv.Graphic Graphic;
    }

    public sealed class MaterialTextField : Inv.Panel<MaterialFieldContainer>
    {
      public MaterialTextField()
      {
        this.Base = new MaterialFieldContainer();

        this.Edit = Inv.Edit.NewText();
        Base.Content = Edit;
        Edit.Background.Colour = Inv.Colour.Yellow.Opacity(0.50F);
        Edit.Font.Name = "Roboto";
        Edit.Font.Size = 16;
        Edit.ChangeEvent += () => Base.SetEmpty(IsEmpty());
        Edit.Focus.GotEvent += () => Base.GotFocus();
        Edit.Focus.LostEvent += () => Base.LostFocus();
      }

      public string CaptionText
      {
        get { return Base.CaptionText; }
        set { Base.CaptionText = value; }
      }
      public string Text
      {
        get { return Edit.Text; }
        set { Edit.Text = value; }
      }
      public string ErrorText
      {
        get { return Base.ErrorText; }
        set { Base.ErrorText = value; }
      }
      public string HelperText
      {
        set { Base.HelperText = value; }
      }
      public Inv.Margin Margin => Base.Margin;
      public Inv.Alignment Alignment => Base.Alignment;
      public Inv.Visibility Visibility => Base.Visibility;
      public event Action ReturnEvent
      {
        add { Edit.ReturnEvent += value; }
        remove { Edit.ReturnEvent -= value; }
      }
      public event Action ChangeEvent
      {
        add { Edit.ChangeEvent += value; }
        remove { Edit.ChangeEvent -= value; }
      }

      public Inv.Panel GetFocus() => Edit;
      public bool IsEmpty() => string.IsNullOrWhiteSpace(Edit.Text);

      private Inv.Edit Edit;
    }

    public sealed class MaterialMemoField : Inv.Panel<MaterialFieldContainer>
    {
      public MaterialMemoField()
      {
        this.Base = new MaterialFieldContainer();

        this.Memo = Inv.Memo.New();
        Base.Content = Memo;
        Memo.Font.Name = "Roboto";
        Memo.Font.Size = 16;
        Memo.ChangeEvent += () => Base.SetEmpty(string.IsNullOrEmpty(Memo.Text));
        Memo.Focus.GotEvent += () => Base.GotFocus();
        Memo.Focus.LostEvent += () => Base.LostFocus();
      }

      public string CaptionText
      {
        get { return Base.CaptionText; }
        set { Base.CaptionText = value; }
      }
      public string Text
      {
        get { return Memo.Text; }
        set { Memo.Text = value; }
      }
      public string HelperText
      {
        set { Base.HelperText = value; }
      }
      public string ErrorText
      {
        get { return Base.ErrorText; }
        set { Base.ErrorText = value; }
      }
      public Inv.Margin Margin => Base.Margin;
      public Inv.Alignment Alignment => Base.Alignment;
      public Inv.Visibility Visibility => Base.Visibility;
      public event Action ChangeEvent
      {
        add { Memo.ChangeEvent += value; }
        remove { Memo.ChangeEvent -= value; }
      }

      public Inv.Panel GetFocus() => Memo;
      public bool IsEmpty() => string.IsNullOrWhiteSpace(Memo.Text);

      private Inv.Memo Memo;
    }

    internal sealed class MaterialDialog
    {
      internal MaterialDialog(Inv.Overlay SurfaceOverlay)
      {
        this.SurfaceOverlay = SurfaceOverlay;

        this.Overlay = Inv.Overlay.New();

        var Scrim = Inv.Button.NewStark();
        Overlay.AddPanel(Scrim);
        Scrim.Background.Colour = Inv.Colour.Black.AdjustAlpha(56);
        Scrim.SingleTapEvent += () =>
        {
          Hide();
        };

        var ContentDock = Inv.Dock.NewVertical();
        Overlay.AddPanel(ContentDock);
        ContentDock.Margin.Set(25);
        ContentDock.Alignment.CenterStretch();
        ContentDock.Background.Colour = Inv.Colour.White;
        ContentDock.Elevation.Set(4);
        ContentDock.Corner.Set(2);

        this.ContentFrame = Inv.Frame.New();
        ContentDock.AddClient(ContentFrame);
      }

      public Inv.Panel Content
      {
        set { ContentFrame.Content = value; }
      }
      public event Action ShowEvent;
      public event Action HideEvent;

      public void Show()
      {
        SurfaceOverlay.AddPanel(Overlay);

        if (ShowEvent != null)
          ShowEvent();
      }
      public void Hide()
      {
        SurfaceOverlay.RemovePanel(Overlay);

        if (HideEvent != null)
          HideEvent();
      }

      private Inv.Overlay SurfaceOverlay;
      private Inv.Overlay Overlay;
      private Inv.Frame ContentFrame;
    }
  }

  internal sealed class Style
  {
    public Style(string Name, Inv.Colour StrokeColour, Inv.Colour FillColour)
    {
      this.Name = Name;
      this.StrokeColour = StrokeColour;
      this.FillColour = FillColour;
    }

    public string Name { get; }
    public Inv.Colour StrokeColour { get; }
    public Inv.Colour FillColour { get; }
  }

  internal sealed class Zone
  {
    public Zone(string Name, Style Style, Inv.Rect Rect)
      : this(Name, Style, Rect.TopLeft(), Rect.TopRight(), Rect.BottomRight(), Rect.BottomLeft())
    {
    }
    public Zone(string Name, Style Style, Inv.Point StartPoint, params Inv.Point[] PointArray)
    {
      this.Name = Name;
      this.Style = Style;
      this.StartPoint = StartPoint;
      this.PointArray = PointArray;
      this.CenterPoint = GetCentroid(GetPoints());
    }

    public string Name { get; }
    public Style Style { get; }
    public Inv.Point StartPoint { get; }
    public Inv.Point CenterPoint { get; }
    public Inv.Point[] PointArray { get; }

    public IEnumerable<Inv.Point> GetPoints()
    {
      yield return StartPoint;
      foreach (var Point in PointArray)
        yield return Point;
    }

    public static Inv.Point GetCentroid(IEnumerable<Inv.Point> Points)
    {
      var poly = Points.ToArray();

      var accumulatedArea = 0;
      var centerX = 0;
      var centerY = 0;

      for (int i = 0, j = poly.Length - 1; i < poly.Length; j = i++)
      {
        var temp = poly[i].X * poly[j].Y - poly[j].X * poly[i].Y;
        accumulatedArea += temp;
        centerX += (poly[i].X + poly[j].X) * temp;
        centerY += (poly[i].Y + poly[j].Y) * temp;
      }

      if (Math.Abs(accumulatedArea) < 1E-7f)
        return Inv.Point.Zero;  // Avoid division by zero

      accumulatedArea *= 3;
      return new Inv.Point(centerX / accumulatedArea, centerY / accumulatedArea);
    }
  }

  internal sealed class MethodItem
  {
    internal MethodItem(NavigationDrawerItem DrawerItem, MethodInfo MethodInfo)
    {
      this.DrawerItem = DrawerItem;
      this.MethodInfo = MethodInfo;
    }

    public NavigationDrawerItem DrawerItem { get; private set; }
    public MethodInfo MethodInfo { get; private set; }
  }

  internal sealed class ImageViewer : Inv.Panel<Inv.Canvas>
  {
    public ImageViewer(Inv.Surface Surface)
    {
      this.Surface = Surface;
      this.Base = Surface.NewCanvas();
      Base.Background.Colour = Inv.Colour.White;
      Base.AdjustEvent += Base.Draw;

      var PivotPoint = Inv.Point.Zero;

      Base.DrawEvent += (DC) =>
      {
        // TODO: panning bounce?

        if (ImageField != null)
        {
          var ViewDimension = Base.GetDimension();
          var ViewWidth = ViewDimension.Width;
          var ViewHeight = ViewDimension.Height;

          var ImageDimension = Surface.Window.Application.Graphics.GetDimension(Image);
          var ImageWidth = (int)(ImageDimension.Width * ZoomFactor);
          var ImageHeight = (int)(ImageDimension.Height * ZoomFactor);
          
          if (ImageWidth < ViewWidth)
          {
            if (ImageX + ImageWidth > ViewWidth)
              ImageX = ViewWidth - ImageWidth;
            else if (ImageX < 0)
              ImageX = 0;
          } 
          else
          {
            if (ImageX > 0)
              ImageX = 0;
            else if (ImageX < ViewWidth - ImageWidth)
              ImageX = ViewWidth - ImageWidth;
          }

          if (ImageHeight < ViewHeight)
          {
            if (ImageY + ImageHeight > ViewHeight)
              ImageY = ViewHeight - ImageHeight;
            else if (ImageY < 0)
              ImageY = 0;
          }
          else
          {
            if (ImageY > 0)
              ImageY = 0;
            else if (ImageY < ViewHeight - ImageHeight)
              ImageY = ViewHeight - ImageHeight;
          }
          
          var ImageRect = new Inv.Rect(ImageX, ImageY, ImageWidth, ImageHeight);
          DC.DrawImage(ImageField, ImageRect);

#if DEBUG
          if (ZoomPoint != null)
            DC.DrawEllipse(Inv.Colour.Red, null, 0, ZoomPoint.Value, new Inv.Point(5, 5));
#endif

          //DC.DrawText(ImageDimension.ToString(), "", 20, Inv.FontWeight.Bold, Inv.Colour.Red, new Inv.Point(0, 0), Inv.HorizontalPosition.Left, Inv.VerticalPosition.Top);
          //DC.DrawText(ViewDimension.ToString(), "", 20, Inv.FontWeight.Bold, Inv.Colour.Red, new Inv.Point(ViewWidth, ViewHeight), Inv.HorizontalPosition.Right, Inv.VerticalPosition.Bottom);
        }
      };
      Base.DoubleTapEvent += (Point) =>
      {
        Fit();
      };
      Base.PressEvent += (Point) =>
      {
        PivotPoint = Point;
      };
      Base.ReleaseEvent += (Point) =>
      {
        this.ZoomPoint = null;
      };
      Base.MoveEvent += (Point) =>
      {
        this.ImageX += Point.X - PivotPoint.X;
        this.ImageY += Point.Y - PivotPoint.Y;
        PivotPoint = Point;
        Base.Draw();
      };
      Base.ZoomEvent += (Zoom) =>
      {
        // TODO: zoom on point would be better?
        this.ZoomPoint = Zoom.Point;

        var OldImageDimension = Surface.Window.Application.Graphics.GetDimension(Image);
        var OldImageWidth = (int)(OldImageDimension.Width * ZoomFactor);
        var OldImageHeight = (int)(OldImageDimension.Height * ZoomFactor);

        ZoomFactor += Zoom.Delta * 0.05F;

        if (ZoomFactor > 4.0F)
          ZoomFactor = 4.0F;
        else if (ZoomFactor < 0.1F)
          ZoomFactor = 0.1F;

        var NewImageDimension = Surface.Window.Application.Graphics.GetDimension(Image);
        var NewImageWidth = (int)(NewImageDimension.Width * ZoomFactor);
        var NewImageHeight = (int)(NewImageDimension.Height * ZoomFactor);

        this.ImageX -= (NewImageWidth - OldImageWidth) / 2;
        this.ImageY -= (NewImageHeight - OldImageHeight) / 2;

        Base.Draw();
      };

      this.ZoomFactor = 1.0F; 
      this.ImageX = 0;
      this.ImageY = 0;
    }

    public float ZoomFactor { get; private set; }
    public Inv.Image Image
    {
      get { return ImageField; }
      set
      {
        if (ImageField != value)
        {
          this.ImageField = value;
          Base.Draw();
        }
      }
    }

    public void Zoom(float Factor)
    {
      if (ZoomFactor != Factor)
      {
        this.ZoomFactor = Factor;
        Base.Draw();
      }
    }
    public void Fit()
    {
      // TODO: defer Fit to draw phase, if not ready yet.

      if (ImageField != null)
      {
        var ViewDimension = Base.GetDimension();
        var ViewWidth = ViewDimension.Width;
        var ViewHeight = ViewDimension.Height;

        var ImageDimension = Surface.Window.Application.Graphics.GetDimension(ImageField);

        var ZoomXFactor = (float)ViewWidth / (float)ImageDimension.Width;
        var ZoomYFactor = (float)ViewHeight / (float)ImageDimension.Height;

        this.ZoomFactor = Math.Min(ZoomXFactor, ZoomYFactor);

        var ImageWidth = (int)(ImageDimension.Width * ZoomFactor);
        var ImageHeight = (int)(ImageDimension.Height * ZoomFactor);

        this.ImageX = (ViewWidth - ImageWidth) / 2;
        this.ImageY = (ViewHeight - ImageHeight) / 2;

        Base.Draw();
      }
    }

    private Inv.Surface Surface;
    private Inv.Image ImageField;
    private int ImageX;
    private int ImageY;
    private Inv.Point? ZoomPoint;
  }

  internal sealed class ComplexButton : Inv.Panel<Inv.Button>
  {
    public ComplexButton(Inv.Surface Surface)
    {
      this.Base = Surface.NewFlatButton();
      Base.Padding.Set(10);
      Base.Background.Colour = Inv.Colour.WhiteSmoke;
      Base.SingleTapEvent += () =>
      {
        Debug.WriteLine("test");
      };
      
      var TestDock = Surface.NewHorizontalDock();
      Base.Content = TestDock;

      var TestFrame = Surface.NewFrame();
      TestDock.AddHeader(TestFrame);
      TestFrame.Background.Colour = Inv.Colour.DarkOrange;
      TestFrame.Corner.Set(10);
      TestFrame.Size.Set(44, 44);
      TestFrame.Margin.Set(10);

      var TypeLabel = Surface.NewLabel();
      TestFrame.Content = TypeLabel;
      TypeLabel.Font.Size = 25;
      TypeLabel.Font.Colour = Inv.Colour.White;
      TypeLabel.Alignment.Center();
      TypeLabel.Text = "A";

      var StaffLabel = Surface.NewLabel();
      TestDock.AddClient(StaffLabel);
      StaffLabel.Font.Size = 22;
      StaffLabel.Text = "Callan Hodgskin";
      StaffLabel.Alignment.CenterLeft();
    }

    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }
  }

  internal sealed class CustomPanel : Inv.Panel<Inv.Dock>
  {
    public CustomPanel(Inv.Surface Surface)
    {
      this.Base = Surface.NewVerticalDock();
      Base.Background.Colour = Inv.Colour.Black.Opacity(0.75F);

      var HeaderDock = Surface.NewHorizontalDock();
      Base.AddHeader(HeaderDock);

      this.CaptionLabel = Surface.NewLabel();
      HeaderDock.AddHeader(CaptionLabel);
      CaptionLabel.Visibility.Collapse();
      CaptionLabel.Font.Size = 24;
      CaptionLabel.Font.Colour = Inv.Colour.WhiteSmoke;
      CaptionLabel.Padding.Set(10, 10, 0, 0);
      CaptionLabel.Alignment.TopLeft();

      this.DescriptionLabel = Surface.NewLabel();
      HeaderDock.AddClient(DescriptionLabel);
      DescriptionLabel.Visibility.Collapse();
      DescriptionLabel.Font.Size = 24;
      DescriptionLabel.Font.Colour = Inv.Colour.WhiteSmoke;
      DescriptionLabel.Padding.Set(0, 10, 10, 0);
      DescriptionLabel.Alignment.TopRight();

      this.FooterLabel = Surface.NewLabel();
      Base.AddFooter(FooterLabel);
      FooterLabel.Margin.Set(10);
      FooterLabel.Font.Colour = Inv.Colour.WhiteSmoke;
      FooterLabel.Font.Size = 14;
      FooterLabel.Visibility.Collapse();
    }

    public string Caption
    {
      get { return CaptionLabel.Text; }
      set
      {
        CaptionLabel.Text = value;
        CaptionLabel.Visibility.Set(value != null);
      }
    }
    public string Description
    {
      get { return DescriptionLabel.Text; }
      set
      {
        DescriptionLabel.Text = value;
        DescriptionLabel.Visibility.Set(value != null);
      }
    }
    public string Footer
    {
      set
      {
        FooterLabel.Text = value;
        FooterLabel.Visibility.Set(value != null);
      }
    }
    public Inv.Background Background
    {
      get { return Base.Background; }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }
    public Inv.Panel Content
    {
      get { return ContentPanel; }
      set
      {
        if (ContentPanel != value)
        {
          if (ContentPanel != null)
            Base.RemoveClient(ContentPanel);

          this.ContentPanel = value;

          if (ContentPanel != null)
            Base.AddClient(ContentPanel);
        }
      }
    }

    private Inv.Label CaptionLabel;
    private Inv.Label DescriptionLabel;
    private Inv.Label FooterLabel;
    private Inv.Panel ContentPanel;
  }

  public sealed class CustomButton : Inv.Panel<Inv.Button>
  {
    public CustomButton(Inv.Surface Surface)
    {
      this.Base = Surface.NewFlatButton();
      Base.SingleTapEvent += () =>
      {
        if (IsClickable && SingleTapEvent != null)
          SingleTapEvent();
      };
      Base.ContextTapEvent += () =>
      {
        if (IsClickable && ContextTapEvent != null)
          ContextTapEvent();
      };

      this.IsClickable = true;
    }

    public Inv.Panel Content
    {
      get { return Base.Content; }
      set { Base.Content = value; }
    }
    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }
    public bool IsFocused
    {
      get { return Base.IsFocusable; }
      set { Base.IsFocusable = value; }
    }
    public bool IsClickable
    {
      get { return IsClickableField; }
      set
      {
        if (IsClickableField != value)
        {
          this.IsClickableField = value;

          Base.Background.Colour = IsClickableField ? Inv.Colour.DimGray : Inv.Colour.Transparent;
        }
      }
    }
    public Inv.Focus Focus => Base.Focus;
    public Inv.Size Size => Base.Size;
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Padding Padding
    {
      get { return Base.Padding; }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }
    public Inv.Corner Corner
    {
      get { return Base.Corner; }
    }
    public Inv.Border Border
    {
      get { return Base.Border; }
    }
    public Inv.Background Background
    {
      get { return Base.Background; }
    }
    public event Action SingleTapEvent;
    public event Action ContextTapEvent;

    public void SingleTap()
    {
      Base.SingleTap();
    }

    private bool IsClickableField;
  }

  internal sealed class CustomQuery : Inv.Panel<Inv.Overlay>
  {
    public CustomQuery(Inv.Surface Surface)
    {
      this.Base = Surface.NewOverlay();
      Base.Visibility.Collapse();

      this.Button = new ShadeButton(Surface);
      Base.AddPanel(Button);
      Button.SingleTapEvent += () => Hide();
      Button.ContextTapEvent += () => Hide();
    }

    public event Action ShowEvent;
    public event Action HideEvent;
    public Inv.Colour ShadeColour
    {
      // NOTE: don't use Button.ShadeColour because it will give the shade a hover effect.
      set { Base.Background.Colour = value; }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Panel Content
    {
      get { return ContentPanel; }
      set
      {
        if (ContentPanel != value)
        {
          if (ContentPanel != null)
            Base.RemovePanel(ContentPanel);

          this.ContentPanel = value;

          if (ContentPanel != null)
            Base.AddPanel(ContentPanel);
        }
      }
    }

    public void Show()
    {
      if (!Base.Visibility.Get())
      {
        Base.Visibility.Show();

        if (ShowEvent != null)
          ShowEvent();
      }
    }
    public void Hide()
    {
      if (Base.Visibility.Get())
      {
        Base.Visibility.Collapse();

        if (HideEvent != null)
          HideEvent();
      }
    }

    private ShadeButton Button;
    private Inv.Panel ContentPanel;
  }

  internal sealed class ShadeButton : Inv.Panel<Inv.Button>
  {
    public ShadeButton(Inv.Surface Surface)
    {
      this.Base = Surface.NewStarkButton();
      Base.Alignment.Stretch();

      ShadeColour = Inv.Colour.Transparent;
    }

    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public float Opacity
    {
      get { return Base.Opacity.Get(); }
      set { Base.Opacity.Set(value); }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Border Border
    {
      get { return Base.Border; }
    }
    public Inv.Colour ShadeColour
    {
      set { Base.Background.Colour = value; }
    }
    public Inv.Panel Content
    {
      get { return Base.Content; }
      set { Base.Content = value; }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }
  }

  internal sealed class RecordPanel : Inv.Panel<Inv.Scroll>
  {
    public RecordPanel(Inv.Surface Surface)
    {
      this.Surface = Surface;

      this.Base = Surface.NewVerticalScroll();

      this.LayoutStack = Surface.NewVerticalStack();
      Base.Content = LayoutStack;

      this.ButtonList = new Inv.DistinctList<RecordButton>();
    }

    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Margin Padding
    {
      get { return Base.Margin; }
    }

    public bool HasButtons()
    {
      return ButtonList.Count > 0;
    }
    public void RemoveButtons()
    {
      ButtonList.Clear();
      LayoutStack.RemovePanels();
    }
    public RecordButton AddButton()
    {
      var Result = new RecordButton(this);

      LayoutStack.AddPanel(Result);
      ButtonList.Add(Result);

      return Result;
    }

    internal Inv.Surface Surface { get; private set; }

    private Inv.Stack LayoutStack;
    private Inv.DistinctList<RecordButton> ButtonList;
  }

  internal sealed class RecordButton : Inv.Panel<Inv.Dock>
  {
    internal RecordButton(RecordPanel Panel)
    {
      this.Panel = Panel;

      this.Base = Panel.Surface.NewHorizontalDock();
      Base.Margin.Set(0, 0, 0, 10);

      this.RankLabel = Panel.Surface.NewLabel();
      Base.AddHeader(RankLabel);
      RankLabel.Font.Colour = Inv.Colour.LightGray;
      RankLabel.Alignment.Center();
      RankLabel.Corner.Set(25);
      RankLabel.Background.Colour = Inv.Colour.Transparent;
      RankLabel.Border.Set(2);
      RankLabel.Border.Colour = Inv.Colour.LightGray;
      RankLabel.Justify.Center();
      RankLabel.Size.Set(44, 44);

      this.FameLabel = Panel.Surface.NewLabel();
      Base.AddFooter(FameLabel);
      FameLabel.Alignment.Center();
      FameLabel.Font.Colour = Inv.Colour.White;
      FameLabel.Margin.Set(0, 0, 5, 0);

      var IdentityStack = Panel.Surface.NewVerticalStack();
      Base.AddClient(IdentityStack);
      IdentityStack.Alignment.CenterStretch();

      this.IdentityLabel = Panel.Surface.NewLabel();
      IdentityStack.AddPanel(IdentityLabel);
      IdentityLabel.LineWrapping = true;
      IdentityLabel.Font.Colour = Inv.Colour.White;
      IdentityLabel.Margin.Set(10, 0, 10, 0);

      this.SummaryLabel = Panel.Surface.NewLabel();
      IdentityStack.AddPanel(SummaryLabel);
      SummaryLabel.LineWrapping = true;
      SummaryLabel.Margin.Set(10, 0, 10, 0);
      SummaryLabel.Font.Colour = Inv.Colour.DarkGray;
      SummaryLabel.Visibility.Collapse();
    }

    public bool IsHighlighted
    {
      set { Base.Background.Colour = value ? Inv.Colour.Purple : Inv.Colour.Transparent; }
    }
    public int Rank
    {
      set { RankLabel.Text = value.ToString("N0"); }
    }
    public int Fame
    {
      set { FameLabel.Text = value.ToString("N0"); }
    }
    public string Identity
    {
      set { IdentityLabel.Text = value; }
    }
    public string Summary
    {
      set
      {
        SummaryLabel.Text = value;
        SummaryLabel.Visibility.Set(value != null);
      }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }

    public void SetFontSize(bool IsNarrow)
    {
      RankLabel.Font.Size = IsNarrow ? 20 : 28;
      FameLabel.Font.Size = IsNarrow ? 20 : 28;
      IdentityLabel.Font.Size = IsNarrow ? 16 : 24;
      SummaryLabel.Font.Size = IsNarrow ? 10 : 14;
    }
    public void AddImage(Inv.Image Image)
    {
      var Graphic = Panel.Surface.NewGraphic();
      Base.AddFooter(Graphic);
      Graphic.Size.Set(48, 48);
      Graphic.Image = Image;
    }

    private Inv.Label RankLabel;
    private Inv.Label IdentityLabel;
    private Inv.Label FameLabel;
    private Inv.Label SummaryLabel;
    private RecordPanel Panel;
  }

  internal sealed class TilePanel : Inv.Panel<CustomPanel>
  {
    public TilePanel(Inv.Surface Surface)
    {
      this.Surface = Surface;

      this.Base = new CustomPanel(Surface);

      this.LayoutScroll = Surface.NewVerticalScroll();
      Base.Content = LayoutScroll;

      this.LayoutStack = Surface.NewVerticalStack();
      LayoutScroll.Content = LayoutStack;
      LayoutStack.Margin.Set(10, 0, 10, 10);

      this.ItemList = new Inv.DistinctList<TileItem>();
    }

    public string Caption
    {
      set { Base.Caption = value; }
    }
    public string Description
    {
      set { Base.Description = value; }
    }
    public string Footer
    {
      set { Base.Footer = value; }
    }
    public Inv.Background Background
    {
      get { return Base.Background; }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }
    public Inv.Margin Padding
    {
      get { return LayoutScroll.Margin; }
    }

    public void Compose(Inv.DistinctList<TileItem> ItemList)
    {
      if (!this.ItemList.ShallowEqualTo(ItemList))
      {
        LayoutStack.RemovePanels();
        foreach (var Item in ItemList)
          LayoutStack.AddPanel(Item.Base);

        this.ItemList = ItemList;
      }
    }
    public bool HasItems()
    {
      return ItemList.Count > 0;
    }
    public void RemoveItems()
    {
      ItemList.Clear();
      LayoutStack.RemovePanels();
    }
    public void RemoveButton(TileButton Button)
    {
      RemoveItem(Button);
    }
    public TileButton AddButton()
    {
      var Result = new TileButton(this);

      AddItem(Result);

      return Result;
    }
    public TileOption AddOption()
    {
      var Result = new TileOption(this);

      AddItem(Result);

      return Result;
    }
    public TileTable AddTable()
    {
      var Result = new TileTable(this);

      AddItem(Result);

      return Result;
    }
    public void AddButton(TileButton Button)
    {
      AddItem(Button);
    }
    public void AddSeparator(string Title)
    {
      var Result = new TileSeparator(this);

      Result.Text = Title;

      AddItem(Result);
    }
    
    internal Inv.Surface Surface { get; private set; }

    private void AddItem(TileItem Item)
    {
      LayoutStack.AddPanel(Item.Base);
      ItemList.Add(Item);
    }
    private void RemoveItem(TileItem Item)
    {
      LayoutStack.RemovePanel(Item.Base);
      ItemList.Remove(Item);
    }

    private Inv.Stack LayoutStack;
    private Inv.Scroll LayoutScroll;
    private Inv.DistinctList<TileItem> ItemList;
  }

  public interface TileItem
  {
    Inv.Panel Base { get; }
  }

  internal sealed class TileTable : TileItem
  {
    public TileTable(TilePanel Panel)
    {
      this.Panel = Panel;
      this.Base = Panel.Surface.NewHorizontalDock();
    }

    public TileColumn AddColumn()
    {
      var Result = new TileColumn(Panel.Surface);

      Base.AddClient(Result);

      return Result;
    }

    Inv.Panel TileItem.Base
    {
      get { return Base; }
    }

    private Inv.Dock Base;
    private TilePanel Panel;
  }

  internal sealed class TileColumn : Inv.Panel<CustomButton>
  {
    public TileColumn(Inv.Surface Surface)
    {
      this.Surface = Surface;

      this.Base = new CustomButton(Surface);
      Base.Margin.Set(1);

      var Stack = Surface.NewVerticalStack();
      Base.Content = Stack;

      this.TitleLabel = Surface.NewLabel();
      Stack.AddPanel(TitleLabel);
      TitleLabel.Font.Colour = Inv.Colour.Yellow;
      TitleLabel.Justify.Center();

      this.ContentLabel = Surface.NewLabel();
      Stack.AddPanel(ContentLabel);
      ContentLabel.Font.Colour = Inv.Colour.WhiteSmoke;
      ContentLabel.Justify.Center();
    }

    public bool IsClickable
    {
      get { return Base.IsClickable; }
      set { Base.IsClickable = value; }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }

    public void Set(string Title, string Content)
    {
      var IsSmall = Surface.Window.Width < 768;

      TitleLabel.Font.Size = IsSmall ? 16 : 18;
      TitleLabel.Text = Title;

      ContentLabel.Font.Size = IsSmall ? 16 : 18;
      ContentLabel.Text = Content;
    }

    private Inv.Surface Surface;
    private Inv.Label TitleLabel;
    private Inv.Label ContentLabel;
  }

  internal sealed class TileSeparator : TileItem
  {
    public TileSeparator(TilePanel Panel)
    {
      this.Base = Panel.Surface.NewLabel();
      Base.Margin.Set(0, 0, 5, 0);
      Base.Justify.Right();
      Base.Font.Colour = Inv.Colour.White;
      Base.Font.Size = 12;
    }

    public string Text
    {
      get { return Base.Text; }
      set { Base.Text = value; }
    }

    Inv.Panel TileItem.Base
    {
      get { return Base; }
    }

    private Inv.Label Base;
  }

  internal sealed class TileOption : TileItem
  {
    public TileOption(TilePanel Panel)
    {
      this.Panel = Panel;

      this.Base = new CustomButton(Panel.Surface);
      Base.Margin.Set(1);

      this.HorizontalDock = Panel.Surface.NewHorizontalDock();
      Base.Content = HorizontalDock;

      this.IconGraphic = Panel.Surface.NewGraphic();
      HorizontalDock.AddHeader(IconGraphic);
      IconGraphic.Margin.Set(0, 0, 4, 0);

      this.DescriptionLabel = Panel.Surface.NewLabel();
      HorizontalDock.AddClient(DescriptionLabel);
      DescriptionLabel.Font.Colour = Inv.Colour.WhiteSmoke;

      this.TrailerLabel = Panel.Surface.NewLabel();
      HorizontalDock.AddFooter(TrailerLabel);
      TrailerLabel.Font.Colour = Inv.Colour.Yellow;
      TrailerLabel.Margin.Set(0, 0, 4, 0);
    }

    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Colour Colour
    {
      set
      {
        if (this.ColourField != value)
        {
          this.ColourField = value;

          Base.Background.Colour = IsCheckedField ? ColourField : IsClickable ? Inv.Colour.DimGray : Inv.Colour.Transparent;
        }
      }
    }
    public bool IsChecked
    {
      get { return IsCheckedField; }
      set
      {
        this.IsCheckedField = value;

        Base.Background.Colour = IsCheckedField ? ColourField : IsClickable ? Inv.Colour.DimGray : Inv.Colour.Transparent;
      }
    }
    public bool IsVisible
    {
      get { return Base.Visibility.Get(); }
      set { Base.Visibility.Set(value); }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }
    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }
    public bool IsClickable
    {
      get { return Base.IsClickable; }
      set { Base.IsClickable = value; }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }

    public void Set(Inv.Image Icon, string Description, string Trailer)
    {
      var GlyphSize = Panel.Surface.Window.Width < 768 ? 32 : 48;

      IconGraphic.Visibility.Set(Icon != null);

      if (Icon != null)
      {
        IconGraphic.Size.Set(GlyphSize, GlyphSize);
        IconGraphic.Image = Icon;
      }

      DescriptionLabel.Visibility.Set(Description != null);
      DescriptionLabel.Padding.Set(Icon == null ? 5 : 0);

      if (Description != null)
      {
        DescriptionLabel.Font.Size = GlyphSize < 48 ? 14 : 16;
        DescriptionLabel.Text = Description;
      }

      TrailerLabel.Visibility.Set(Trailer != null);

      if (Trailer != null)
      {
        TrailerLabel.Font.Size = GlyphSize < 48 ? 16 : 18;
        TrailerLabel.Text = Trailer;
      }
    }
    public void SingleTap()
    {
      Base.SingleTap();
    }

    Inv.Panel TileItem.Base
    {
      get { return Base; }
    }

    private TilePanel Panel;
    private CustomButton Base;
    private Inv.Graphic IconGraphic;
    private Inv.Label DescriptionLabel;
    private bool IsCheckedField;
    private Inv.Colour ColourField;
    private Inv.Dock HorizontalDock;
    private Inv.Label TrailerLabel;
  }

  internal sealed class TileButton : TileItem
  {
    public TileButton(TilePanel Panel)
    {
      this.Panel = Panel;

      this.Base = new CustomButton(Panel.Surface);
      Base.Margin.Set(1);

      this.HorizontalDock = Panel.Surface.NewHorizontalDock();
      Base.Content = HorizontalDock;

      this.IconGraphic = Panel.Surface.NewGraphic();
      HorizontalDock.AddHeader(IconGraphic);
      IconGraphic.Margin.Set(0, 0, 4, 0);

      this.VerticalStack = Panel.Surface.NewVerticalStack();
      HorizontalDock.AddClient(VerticalStack);
      VerticalStack.Alignment.CenterStretch();

      this.TitleLabel = Panel.Surface.NewLabel();
      VerticalStack.AddPanel(TitleLabel);
      TitleLabel.Font.Colour = Inv.Colour.Yellow;

      this.DescriptionLabel = Panel.Surface.NewLabel();
      VerticalStack.AddPanel(DescriptionLabel);
      DescriptionLabel.Font.Colour = Inv.Colour.WhiteSmoke;
    }

    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Colour Colour
    {
      set
      {
        if (this.ColourField != value)
        {
          this.ColourField = value;

          Base.Background.Colour = IsCheckedField ? ColourField : IsClickable ? Inv.Colour.DimGray : Inv.Colour.Transparent;
        }
      }
    }
    public bool IsChecked
    {
      get { return IsCheckedField; }
      set
      {
        this.IsCheckedField = value;

        Base.Background.Colour = IsCheckedField ? ColourField : IsClickable ? Inv.Colour.DimGray : Inv.Colour.Transparent;
      }
    }
    public bool IsVisible
    {
      get { return Base.Visibility.Get(); }
      set { Base.Visibility.Set(value); }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }
    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }
    public bool IsClickable
    {
      get { return Base.IsClickable; }
      set { Base.IsClickable = value; }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }

    public void SetTrailer(string Text)
    {
      if (Text == null)
      {
        if (TrailerLabel != null)
        {
          HorizontalDock.RemoveFooter(TrailerLabel);
          this.TrailerLabel = null;
        }
      }
      else
      {
        if (TrailerLabel == null)
        {
          this.TrailerLabel = Panel.Surface.NewLabel();
          HorizontalDock.AddFooter(TrailerLabel);
          TrailerLabel.Font.Colour = Inv.Colour.White;
          TrailerLabel.LineWrapping = true;
          TrailerLabel.Margin.Set(0, 0, 4, 0);
        }

        TrailerLabel.Font.Size = Panel.Surface.Window.Width < 768 ? 16 : 18;
        TrailerLabel.Text = Text;
      }
    }
    public void Set(Inv.Image Icon, string Title, string Description)
    {
      var GlyphSize = Panel.Surface.Window.Width < 768 ? 32 : 48;

      IconGraphic.Visibility.Set(Icon != null);

      if (Icon != null)
      {
        IconGraphic.Size.Set(GlyphSize, GlyphSize);
        IconGraphic.Image = Icon;
      }

      TitleLabel.Visibility.Set(Title != null);

      if (Title != null)
      {
        TitleLabel.Font.Size = GlyphSize < 48 ? 16 : 18;
        TitleLabel.Text = Title;
      }

      DescriptionLabel.Visibility.Set(Description != null);

      if (Description != null)
      {
        DescriptionLabel.Font.Size = GlyphSize < 48 ? 14 : 16;
        DescriptionLabel.Text = Description;
      }

      VerticalStack.Padding.Set(Icon == null ? 5 : 0);
    }
    public void SingleTap()
    {
      Base.SingleTap();
    }

    Inv.Panel TileItem.Base
    {
      get { return Base; }
    }

    private TilePanel Panel;
    private CustomButton Base;
    private Inv.Graphic IconGraphic;
    private Inv.Label TitleLabel;
    private Inv.Label DescriptionLabel;
    private bool IsCheckedField;
    private Inv.Colour ColourField;
    private Inv.Dock HorizontalDock;
    private Inv.Stack VerticalStack;
    private Inv.Label TrailerLabel;
  }

  public sealed class MenuPanel : Inv.Panel<Inv.Dock>
  {
    public MenuPanel(Inv.Surface Surface)
    {
      this.Surface = Surface;

      this.Base = Surface.NewVerticalDock();
      Base.Background.Colour = null;

      this.ButtonStack = Surface.NewHorizontalStack();
      Base.AddFooter(ButtonStack);
    }

    public Inv.Colour BackgroundColour
    {
      get { return Base.Background.Colour; }
      set { Base.Background.Colour = value; }
    }
    public Inv.Panel Content
    {
      get { return ContentPanel; }
      set
      {
        if (ContentPanel != value)
        {
          if (ContentPanel != null)
            Base.RemoveClient(ContentPanel);

          this.ContentPanel = value;

          if (ContentPanel != null)
            Base.AddClient(ContentPanel);
        }
      }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }

    public bool HasButtons()
    {
      return ButtonStack.HasPanels();
    }
    public void RemoveButtons()
    {
      ButtonStack.RemovePanels();
    }
    public MenuButton AddButton()
    {
      var Result = new MenuButton(this);

      ButtonStack.AddPanel(Result);

      return Result;
    }

    internal Inv.Surface Surface { get; private set; }

    private Inv.Stack ButtonStack;
    private Inv.Panel ContentPanel;
  }

  public sealed class MenuButton : Inv.Panel<CustomButton>
  {
    public MenuButton(MenuPanel Panel)
    {
      var ButtonSize = (Panel.Size.Width.Value / 4) - 10;
      var ImageSize = ButtonSize / 2;

      this.Base = new CustomButton(Panel.Surface);
      Base.Size.Set(ButtonSize, ButtonSize);
      Base.Corner.Set(ImageSize);
      Base.Background.Colour = Inv.Colour.DimGray;
      Base.Margin.Set(8, 8, 0, 8);

      this.Graphic = Panel.Surface.NewGraphic();
      Base.Content = Graphic;
      Graphic.Size.Set(ImageSize, ImageSize);
      Graphic.Alignment.Center();
      Graphic.Visibility.Collapse();
    }

    public Inv.Image Image
    {
      set
      {
        Graphic.Image = value;
        Graphic.Visibility.Set(value != null);
      }
    }
    public bool IsChecked
    {
      get { return IsCheckedField; }
      set
      {
        if (IsCheckedField != value)
        {
          this.IsCheckedField = value;

          Base.Background.Colour = value ? Inv.Colour.DarkGreen : Inv.Colour.DimGray;
        }
      }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }
    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }

    private bool IsCheckedField;
    private Inv.Graphic Graphic;
  }

  internal sealed class ActionPanel : Inv.Panel<CustomPanel>
  {
    public ActionPanel(Inv.Surface Surface)
    {
      this.Surface = Surface;
      this.Base = new CustomPanel(Surface);
      Base.Background.Colour = Inv.Colour.Black;

      this.Graphic = Surface.NewGraphic();
      Graphic.Alignment.Center();
      Graphic.Size.Set(128, 128);

      this.HeaderStack = Surface.NewHorizontalStack();
      HeaderStack.Alignment.StretchLeft();

      this.FooterStack = Surface.NewHorizontalStack();
      FooterStack.Alignment.StretchRight();

      this.HeaderButtonList = new Inv.DistinctList<ActionButton>();
      this.FooterButtonList = new Inv.DistinctList<ActionButton>();

      // NOTE: this is unnecessary if you remember to call Compose before it is shown (which is necessary for it to arrange correctly).
      //       however, it might alleviate some bugs where you forget to call Compose.
      Compose(Surface.Window.Width, Surface.Window.Height);
    }

    public string Caption
    {
      get { return Base.Caption; }
      set { Base.Caption = value; }
    }
    public string Description
    {
      get { return Base.Description; }
      set { Base.Description = value; }
    }
    public Inv.Colour BackgroundColour
    {
      get { return Base.Background.Colour; }
      set { Base.Background.Colour = value; }
    }
    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }
    public Inv.Image Image
    {
      set { Graphic.Image = value; }
    }

    public ActionButton AddHeaderButton()
    {
      var Result = new ActionButton(this);

      HeaderButtonList.Add(Result);

      HeaderStack.AddPanel(Result);

      return Result;
    }
    public ActionButton AddFooterButton()
    {
      var Result = new ActionButton(this);

      FooterButtonList.Add(Result);

      FooterStack.AddPanel(Result);

      return Result;
    }
    public void Compose(int ViewWidth, int ViewHeight)
    {
      var IsWide = ViewWidth >= 1280;
      var IsReduced = ViewWidth < 1024;
      var IsNarrow = ViewWidth < 768;
      var IsPortrait = ViewWidth < ViewHeight;
      var IsStacked = IsNarrow && IsPortrait;
      var IsConverting = LayoutDock == null || (IsStacked && ButtonStack == null) || (!IsStacked && ButtonStack != null);
      var HeaderCount = HeaderButtonList.Count(B => B.IsVisible);
      var FooterCount = FooterButtonList.Count(B => B.IsVisible);
      var ButtonCount = IsNarrow && IsPortrait ? Math.Max(HeaderCount, Math.Max(FooterCount, 4)) : Math.Max(HeaderCount + FooterCount, 8);

      this.ButtonMargin = IsReduced || IsNarrow ? 4 : IsWide ? 16 : 8;
      this.ButtonSize = Math.Min(((ViewWidth - ButtonMargin - 2 - (IsReduced ? 0 : 180) - (ButtonCount * ButtonMargin)) / ButtonCount), Math.Min(ViewHeight / 3, 160));

      var ButtonRange = ViewWidth - ((ButtonSize + ButtonMargin) * ButtonCount) - ButtonMargin;
      if (ButtonRange < ButtonSize)
      {
        HeaderStack.Margin.Set(ButtonRange / 2, 0, 0, 0);
        FooterStack.Margin.Set(0, 0, (ButtonRange / 2) + (ButtonRange % 2), 0);
      }
      else
      {
        HeaderStack.Margin.Clear();
        FooterStack.Margin.Clear();
      }

      if (IsConverting && LayoutDock != null)
      {
        LayoutDock.RemovePanels();
        this.LayoutDock = null;
      }

      if (IsConverting && ButtonStack != null)
      {
        ButtonStack.RemovePanels();
        this.ButtonStack = null;
      }

      if (IsConverting)
      {
        Debug.Assert(LayoutDock == null);
        Debug.Assert(ButtonStack == null);

        this.LayoutDock = Surface.NewHorizontalDock();
        Base.Content = LayoutDock;
        LayoutDock.AddHeader(Graphic);

        if (IsStacked)
        {
          this.ButtonStack = Surface.NewVerticalStack();
          LayoutDock.AddClient(ButtonStack);
          ButtonStack.AddPanel(HeaderStack);
          ButtonStack.AddPanel(FooterStack);
        }
        else
        {
          LayoutDock.AddHeader(HeaderStack);
          LayoutDock.AddFooter(FooterStack);
        }
      }

      LayoutDock.Margin.Set(ButtonMargin, ButtonMargin, 0, 0);

      Graphic.Size.Set(ButtonSize, ButtonSize);
      Graphic.Visibility.Set(!IsReduced);
      Graphic.Margin.Set(0, 0, ButtonMargin, 0);

      this.LabelSize = IsWide ? 24 : IsNarrow ? 14 : IsReduced ? 16 : 18;

      foreach (var HeaderButton in HeaderButtonList)
        HeaderButton.Compose();

      foreach (var FooterButton in FooterButtonList)
        FooterButton.Compose();

      // focus.
      var FocusButton = HeaderButtonList.Find(H => H.IsVisible) ?? FooterButtonList.Find(H => H.IsVisible);
      if (FocusButton != null)
        FocusButton.Focus.Set();
    }

    internal Inv.Surface Surface { get; private set; }
    internal int ButtonSize { get; private set; }
    internal int ButtonMargin { get; private set; }
    internal int LabelSize { get; private set; }

    private Inv.Graphic Graphic;
    private Inv.Stack ButtonStack;
    private Inv.Stack HeaderStack;
    private Inv.Stack FooterStack;
    private Inv.Dock LayoutDock;
    private Inv.DistinctList<ActionButton> HeaderButtonList;
    private Inv.DistinctList<ActionButton> FooterButtonList;
  }

  internal sealed class ActionButton : Inv.Panel<CustomButton>
  {
    public ActionButton(ActionPanel Panel)
    {
      this.Panel = Panel;

      this.Base = new CustomButton(Panel.Surface);
      Base.IsFocused = true;

      this.CaptionLabel = Panel.Surface.NewLabel();
      Base.Content = CaptionLabel;
      CaptionLabel.Alignment.Stretch();
      CaptionLabel.Justify.Center();
      CaptionLabel.Font.Colour = Inv.Colour.White;
      CaptionLabel.LineWrapping = true;

      Compose(); // see NOTE in ActionPanel constructor.
    }

    public string Caption
    {
      get { return CaptionLabel.Text; }
      set { CaptionLabel.Text = value; }
    }
    public Inv.Colour Colour
    {
      get { return Base.Background.Colour; }
      set { Base.Background.Colour = value; }
    }
    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }
    public bool IsVisible
    {
      get { return Base.Visibility.Get(); }
      set { Base.Visibility.Set(value); }
    }
    public Inv.Focus Focus => Base.Focus;
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }

    internal void Compose()
    {
      Base.Margin.Set(0, 0, Panel.ButtonMargin, Panel.ButtonMargin);
      Base.Size.Set(Panel.ButtonSize, Panel.ButtonSize);
      CaptionLabel.Font.Size = Panel.LabelSize;
    }

    private ActionPanel Panel;
    private Inv.Label CaptionLabel;
  }

  public sealed class CaptionButton : Inv.Panel<CustomButton>
  {
    public CaptionButton(Inv.Surface Surface)
    {
      this.Base = new CustomButton(Surface);

      this.Label = Surface.NewLabel();
      Base.Content = Label;
      Label.Justify.Center();
      Label.Font.Size = 18;
      Label.Font.Colour = Inv.Colour.White;
      Label.LineWrapping = true;
    }

    public Inv.Size Size
    {
      get { return Base.Size; }
    }
    public Inv.Visibility Visibility
    {
      get { return Base.Visibility; }
    }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public Inv.Margin Margin
    {
      get { return Base.Margin; }
    }
    public Inv.Padding Padding
    {
      get { return Base.Padding; }
    }
    public Inv.Colour Colour
    {
      set
      {
        if (this.ColourField != value)
        {
          this.ColourField = value;

          Base.Background.Colour = value;
        }
      }
    }
    public string Text
    {
      set { Label.Text = value; }
    }
    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }

    public void SingleTap()
    {
      Base.SingleTap();
    }

    private Inv.Label Label;
    private Inv.Colour ColourField;
  }

  internal sealed class AboutButton : Inv.Panel<Inv.Button>
  {
    public AboutButton(Inv.Surface Surface)
    {
      this.Base = Surface.NewFlatButton();
      Base.Background.Colour = Inv.Colour.Yellow;
      Base.Border.Set(1);
      Base.Border.Colour = Inv.Colour.Red;
      //Base.Margin.Set(4, 0, 4, 4);

      var Dock = Surface.NewHorizontalDock();
      Base.Content = Dock;

      this.Graphic = Surface.NewGraphic();
      Dock.AddHeader(Graphic);
      Graphic.Padding.Set(4);
      Graphic.Size.Set(64, 64);
      Graphic.Alignment.TopLeft();

      var Stack = Surface.NewVerticalStack();
      Dock.AddClient(Stack);
      Stack.Alignment.CenterStretch();

      this.TitleLabel = Surface.NewLabel();
      Stack.AddPanel(TitleLabel);
      TitleLabel.Font.Colour = Inv.Colour.Black;
      TitleLabel.Font.Light();
      TitleLabel.Font.Size = 22;

      this.ActionLabel = Surface.NewLabel();
      Stack.AddPanel(ActionLabel);
      ActionLabel.Font.Colour = Inv.Colour.Black;
      ActionLabel.Font.Medium();
      ActionLabel.Font.Size = 14;

      this.SupportingLabel = Surface.NewLabel();
      Stack.AddPanel(SupportingLabel);
      SupportingLabel.Padding.Set(0, 4, 0, 0);
      SupportingLabel.Font.Colour = Inv.Colour.Black;
      SupportingLabel.Font.Size = 14;
      SupportingLabel.Visibility.Collapse();
    }

    public Inv.Colour Colour
    {
      set { Base.Background.Colour = value; }
    }
    public Inv.Image LogoImage
    {
      set { Graphic.Image = value; }
    }
    public string TitleText
    {
      set { TitleLabel.Text = value; }
    }
    public string ActionText
    {
      set { ActionLabel.Text = value; }
    }
    public string SupportingText
    {
      set
      {
        SupportingLabel.Text = value;
        SupportingLabel.Visibility.Set(value != null);
      }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }

    private Inv.Graphic Graphic;
    private Inv.Label TitleLabel;
    private Inv.Label ActionLabel;
    private Inv.Label SupportingLabel;
  }

  internal sealed class ExchangePanel : Inv.Panel<Inv.Dock>
  {
    public ExchangePanel(Inv.Surface Surface)
    {
      this.Surface = Surface;

      this.LeftPanel = new TilePanel(Surface);
      this.RightPanel = new TilePanel(Surface);
      this.LeftButton = new CaptionButton(Surface);
      this.CommandButton = new CaptionButton(Surface);
      this.RightButton = new CaptionButton(Surface);
    }

    public TilePanel LeftPanel { get; private set; }
    public TilePanel RightPanel { get; private set; }
    public CaptionButton LeftButton { get; private set; }
    public CaptionButton RightButton { get; private set; }
    public CaptionButton CommandButton { get; private set; }
    public Inv.Alignment Alignment
    {
      get { return Base.Alignment; }
    }
    public event Action CloseEvent;

    public void Compose(int EdgeWidth)
    {
      if (Base != null)
        Base.RemovePanels();

      if (ButtonDock != null)
        ButtonDock.RemovePanels();

      var IsPortrait = Surface.Window.Width < Surface.Window.Height;
      var IsNarrow = Surface.Window.Width < 1024;
      var LayoutMargin = IsNarrow ? 5 : 10;
      var IsSingle = IsNarrow && IsPortrait;

      var CommandSize = IsNarrow ? 150 : 200;
      int PanelSize;
      if (IsSingle)
      {
        PanelSize = EdgeWidth - (CommandSize / 2) - (LayoutMargin * 4);

        var Gap = Surface.Window.Height - (PanelSize + CommandSize - 50);

        if (Gap > 0 && Gap < 40)
          PanelSize -= (40 - Gap) / 2;
      }
      else
      {
        PanelSize = Math.Min(400, (Surface.Window.Width - CommandSize - (LayoutMargin * 4)) / 2);
      }

      this.Base = IsSingle ? Surface.NewVerticalDock() : Surface.NewHorizontalDock();
      Base.Alignment.BottomStretch();
      Base.Background.Colour = Inv.Colour.Black;

      if (IsSingle)
      {
        Base.Size.SetMaximumWidth(Math.Min(420, Surface.Window.Width));
        Base.Size.AutoHeight();

        var CloseButton = new CaptionButton(Surface);
        Base.AddHeader(CloseButton);
        CloseButton.Colour = Inv.Colour.DimGray;
        CloseButton.Text = "CLOSE";
        CloseButton.Size.SetHeight(40);
        CloseButton.SingleTapEvent += () =>
        {
          if (CloseEvent != null)
            CloseEvent();
        };
      }
      else
      {
        Base.Size.AutoMaximumWidth();
        Base.Size.SetHeight(Math.Min(Surface.Window.Height - 40, Math.Max(EdgeWidth, (CommandSize * 2) + (LayoutMargin * 4))));
      }

      Base.AddHeader(LeftPanel);
      if (IsSingle)
      {
        LeftPanel.Size.AutoWidth();
        LeftPanel.Size.SetHeight(PanelSize);
      }
      else
      {
        LeftPanel.Size.SetWidth(PanelSize);
        LeftPanel.Size.AutoHeight();
      }
      LeftPanel.Background.Colour = Inv.Colour.Transparent;

      Base.AddFooter(RightPanel);
      if (IsSingle)
      {
        RightPanel.Size.AutoWidth();
        RightPanel.Size.SetHeight(PanelSize);
      }
      else
      {
        RightPanel.Size.SetWidth(PanelSize);
        RightPanel.Size.AutoHeight();
      }
      RightPanel.Background.Colour = Inv.Colour.Transparent;

      this.ButtonDock = IsSingle ? Surface.NewHorizontalDock() : Surface.NewVerticalDock();
      Base.AddClient(ButtonDock);
      if (IsSingle)
      {
        ButtonDock.Size.AutoWidth();
        ButtonDock.Size.SetHeight(CommandSize - 50);
      }
      else
      {
        ButtonDock.Size.SetWidth(CommandSize);
        ButtonDock.Size.AutoHeight();
      }

      ButtonDock.AddHeader(LeftButton);
      LeftButton.Margin.Set(LayoutMargin);
      LeftButton.Padding.Set(LayoutMargin);
      if (IsSingle)
      {
        LeftButton.Size.SetWidth(CommandSize / 2);
        LeftButton.Size.AutoHeight();
      }
      else
      {
        LeftButton.Size.AutoWidth();
        LeftButton.Size.SetHeight(CommandSize / 2);
      }

      ButtonDock.AddClient(CommandButton);
      CommandButton.Padding.Set(LayoutMargin);
      if (IsSingle)
      {
        CommandButton.Margin.Set(0, LayoutMargin, LayoutMargin, LayoutMargin);
        //CommandButton.Size.SetWidth(CommandSize);
      }
      else
      {
        CommandButton.Margin.Set(LayoutMargin, 0, LayoutMargin, LayoutMargin);
        //CommandButton.Size.SetHeight(CommandSize);
      }

      ButtonDock.AddFooter(RightButton);
      RightButton.Padding.Set(LayoutMargin);
      if (IsSingle)
      {
        RightButton.Margin.Set(0, LayoutMargin, LayoutMargin, LayoutMargin);
        RightButton.Size.SetWidth(CommandSize / 2);
        RightButton.Size.AutoHeight();
      }
      else
      {
        RightButton.Margin.Set(LayoutMargin, 0, LayoutMargin, LayoutMargin);
        RightButton.Size.AutoWidth();
        RightButton.Size.SetHeight(CommandSize / 2);
      }
    }

    private Inv.Surface Surface;
    private Inv.Dock ButtonDock;
  }

  internal sealed class ScoreTable : Inv.Panel<Inv.Table>
  {
    public ScoreTable(Inv.Surface Surface)
    {
      this.Surface = Surface;
      this.Base = Surface.NewTable();

      this.TitleRow = Base.AddAutoRow();
      this.ContentRow = Base.AddAutoRow();
    }

    public ScoreColumn AddColumn()
    {
      return new ScoreColumn(this, Surface, Base.AddStarColumn());
    }

    internal readonly Inv.TableRow TitleRow;
    internal readonly Inv.TableRow ContentRow;

    internal void SetCell(Inv.TableColumn Column, Inv.TableRow Row, Inv.Panel Panel)
    {
      Base.GetCell(Column, Row).Content = Panel;
    }

    private readonly Inv.Surface Surface;
  }

  internal sealed class ScoreColumn
  {
    internal ScoreColumn(ScoreTable Table, Inv.Surface Surface, Inv.TableColumn Column)
    {
      this.Button = new CustomButton(Surface);
      Column.Content = Button;
      Button.Margin.Set(1);

      this.TitleLabel = Surface.NewLabel();
      Table.SetCell(Column, Table.TitleRow, TitleLabel);
      TitleLabel.Font.Colour = Inv.Colour.Yellow;
      TitleLabel.Justify.Center();

      this.ContentLabel = Surface.NewLabel();
      Table.SetCell(Column, Table.ContentRow, ContentLabel);
      ContentLabel.Font.Colour = Inv.Colour.WhiteSmoke;
      ContentLabel.Justify.Center();
    }

    public bool IsClickable
    {
      get { return Button.IsClickable; }
      set { Button.IsClickable = value; }
    }
    public Inv.Colour Colour
    {
      set { Button.Background.Colour = IsClickable ? value : Inv.Colour.Transparent; }
    }
    public event Action SingleTapEvent
    {
      add { Button.SingleTapEvent += value; }
      remove { Button.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Button.ContextTapEvent += value; }
      remove { Button.ContextTapEvent -= value; }
    }

    public void Set(bool IsSmall, string Title, string Content)
    {
      TitleLabel.Font.Size = IsSmall ? 16 : 18;
      TitleLabel.Text = Title;

      ContentLabel.Font.Size = IsSmall ? 16 : 18;
      ContentLabel.Text = Content;
    }

    private CustomButton Button;
    private Inv.Label TitleLabel;
    private Inv.Label ContentLabel;
  }

  internal sealed class NavigationDrawerScreen : Inv.Panel<Inv.Overlay>
  {
    public NavigationDrawerScreen(Inv.Surface Surface)
    {
      this.Surface = Surface;

      this.Base = Surface.NewOverlay();
      DefaultLayer = Surface.NewFrame();
      PopUpLayer = Surface.NewFrame();

      Base.AddPanel(DefaultLayer);
      Base.AddPanel(PopUpLayer);

      var label = Surface.NewLabel();
      label.Background.Colour = Inv.Colour.DodgerBlue;
      label.Padding.Set(8);
      label.Font.Size = 24;
      label.Font.Colour = Inv.Colour.White;
      label.Alignment.Center();
      label.Justify.Center();
      label.Text = "Click Me";

      var button = Surface.NewFlatButton();
      button.Content = label;
      button.Alignment.Stretch();
      button.Background.Colour = Inv.Colour.WhiteSmoke;
      button.SingleTapEvent += () =>
      {
        var popup = new NavigationDrawerPopUpGraphic(this, Resources.Images.PhoenixLogo500x500);
        PopUpLayer.Transition(popup).Fade();
      };
      DefaultLayer.Transition(button);
    }

    public Inv.Scroll DrawerScroll { get; set; }
    public Inv.Frame DefaultLayer { get; set; }
    public Inv.Frame PopUpLayer { get; set; }
    public Inv.Surface Surface { get; private set; }
  }

  internal sealed class NavigationDrawerPopUpGraphic : Inv.Panel<Inv.Overlay>
  {
    public NavigationDrawerPopUpGraphic(NavigationDrawerScreen navigationSurface, Inv.Image image)
    {
      var surface = navigationSurface.Surface;
      Base = surface.NewOverlay();
      var scrim = surface.NewFrame();
      scrim.Alignment.Stretch();
      scrim.Background.Colour = Inv.Colour.Black;
      scrim.Opacity.Set(1f);
      Base.AddPanel(scrim);

      var graphic = surface.NewGraphic();
      graphic.Image = image;
      graphic.Alignment.Center();
      graphic.Margin.Set(24);
      Base.AddPanel(graphic);

      var button = surface.NewFlatButton();
      button.Background.Colour = Inv.Colour.Transparent; // NOTE: without this, the button can't be tapped on UWP (every other platform works with null background colour).
      button.Alignment.Stretch();
      button.SingleTapEvent += () =>
      {
        navigationSurface.PopUpLayer.Transition(null).Fade();
      };
      Base.AddPanel(button);
    }
  }

  internal sealed class NavigationCacheScreen : Inv.Panel<Inv.Overlay>
  {
    public NavigationCacheScreen(Inv.Surface Surface)
    {
      this.Surface = Surface;
      this.Base = Surface.NewOverlay();

      DefaultLayer = Surface.NewFrame();
      PopUpLayer = Surface.NewFrame();

      Base.AddPanel(DefaultLayer);
      Base.AddPanel(PopUpLayer);

      var contentStack = Surface.NewVerticalStack();
      contentStack.Alignment.Stretch();
      contentStack.Background.Colour = Inv.Colour.WhiteSmoke;

      var label2 = Surface.NewLabel();
      label2.Background.Colour = Inv.Colour.DodgerBlue;
      label2.Padding.Set(8);
      label2.Font.Size = 24;
      label2.Font.Colour = Inv.Colour.White;
      label2.Alignment.Center();
      label2.Justify.Center();
      label2.Text = "Droid Render Test";

      var button2 = Surface.NewFlatButton();
      button2.Padding.Set(8);
      button2.Content = label2;
      button2.Alignment.Stretch();
      button2.Background.Colour = Inv.Colour.WhiteSmoke;
      button2.SingleTapEvent += () =>
      {
        var page = new NavigationCachePagePanel(this);
        PopUpLayer.Transition(page).Fade();
      };
      contentStack.AddPanel(button2);

      DefaultLayer.Transition(contentStack);
    }

    public Inv.Scroll DrawerScroll { get; set; }
    public Inv.Frame DefaultLayer { get; set; }
    public Inv.Frame PopUpLayer { get; set; }
    public Inv.Surface Surface { get; private set; }
  }

  internal sealed class NavigationCachePagePanel : Inv.Panel<Inv.Scroll>
  {
    public NavigationCachePagePanel(NavigationCacheScreen navigationSurface)
    {
      var surface = navigationSurface.Surface;
      this.Base = surface.NewVerticalScroll();

      var contentStack = surface.NewVerticalStack();
      Base.Content = contentStack;
      contentStack.Background.Colour = Inv.Colour.WhiteSmoke;

      var button = surface.NewFlatButton();
      button.SingleTapEvent += () => navigationSurface.PopUpLayer.Transition(null).Fade();
      var buttonLabel = surface.NewLabel();
      buttonLabel.Background.Colour = Inv.Colour.DodgerBlue;
      buttonLabel.Font.Colour = Inv.Colour.White;
      buttonLabel.Justify.Center();
      buttonLabel.Text = "Close";
      button.Content = buttonLabel;
      contentStack.AddPanel(button);

      var webGraphic = new WebGraphic(surface, "http://unsplash.it/600/380?id=999");
      webGraphic.Size.SetWidth(surface.Window.Width);

      // uncomment me and everything works fine
      //webGraphic.Size.SetHeight(webGraphic.AspectHeight(surface.Window.Width));
      contentStack.AddPanel(webGraphic);

      // OR set this to one and everything works fine
      for (int i = 0; i < 10; i++)
      {
        var label = surface.NewLabel();
        label.Text = "Invention rocks!";
        label.Font.Size = 24;
        label.Justify.Center();
        contentStack.AddPanel(label);
      }

      var browser = surface.NewBrowser();
      contentStack.AddPanel(browser);
      browser.LoadHtml(GenerateHtml(15));
    }

    string GenerateHtml(int paragraphs)
    {
      string result = "";
      var paragraph = string.Join(" ", Inv.Colour.All.Take(50));
      for (int i = 0; i < paragraphs; i++)
      {
        result += $"<p>{paragraph}</p>";
      }
      return result;
    }
  }

  public class WebGraphic : Inv.Panel<Inv.Graphic>
  {
    public WebGraphic(Inv.Surface surface, string uri)
    {
      var application = surface.Window.Application;
      Base = surface.NewGraphic();

      var cacheFileName = GetMD5Hash(uri) + ".jpg";

      var cacheFolder = application.Directory.NewFolder("cache");

      var cacheFile = cacheFolder.NewFile(cacheFileName);
      if (cacheFile.Exists())
      {
        var thebytes = cacheFile.ReadAllBytes();
        var image = new Inv.Image(thebytes, ".jpg");
        Base.Image = image;
        var dimensions = application.Graphics.GetDimension(Base.Image);
        AspectRatio = dimensions.Height / (float)dimensions.Width;

        var FadeInAnimation = surface.NewAnimation();
        var target = FadeInAnimation.AddTarget(Base);
        target.FadeOpacityIn(TimeSpan.FromSeconds(1));
        FadeInAnimation.Start();
        return;
      }

      surface.Window.RunTask(Thread =>
      {
        var WebBroker = application.Web.NewBroker();

        using (var request = WebBroker.GET(uri))
        using (var response = request.Send())
        using (var downloadStream = response.AsStream())
        using (var memoryStream = new System.IO.MemoryStream((int)downloadStream.Length))
        {
          downloadStream.CopyTo(memoryStream);

          memoryStream.Flush();

          var image = new Inv.Image(memoryStream.ToArray(), ".png");

          var newCacheFile = cacheFolder.NewFile(cacheFileName);
          newCacheFile.WriteAllBytes(memoryStream.ToArray());

          Thread.Post(() =>
          {
            Base.Image = image;
            var dimensions = application.Graphics.GetDimension(Base.Image);
            AspectRatio = dimensions.Height / dimensions.Width;
            Base.Readjust();
          });
        }
      });
    }

    public double? AspectRatio { get; set; }

    public int AspectHeight(int width) => (int)(AspectRatio * width);

    public static String GetMD5Hash(string TextToHash)
    {
      return System.IO.Path.GetFileName(TextToHash.Strip(System.IO.Path.GetInvalidFileNameChars()));

      /*
      var md5 = new MD5CryptoServiceProvider();
      byte[] textToHash = System.Text.Encoding.UTF8.GetBytes(TextToHash);
      byte[] result = md5.ComputeHash(textToHash);
      return BitConverter.ToString(result);
      */
    }
    public Inv.Size Size => Base.Size;
  }

  internal sealed class CustomPalette : Inv.Panel<Inv.Dock>
  {
    public CustomPalette(Inv.Surface Surface)
    {
      this.Surface = Surface;
      this.FilterList = new Inv.DistinctList<PaletteFilter>();
      this.ColumnStackList = new Inv.DistinctList<Inv.Stack>();
      this.CacheStackList = new Inv.DistinctList<Inv.Stack>();

      this.Base = Surface.NewVerticalDock();
      Base.Background.Colour = Inv.Colour.Black;
      Base.Padding.Set(0, 0, 0, 4);

      this.ListDock = Surface.NewHorizontalDock();
      Base.AddClient(ListDock);

      var FilterScroll = Surface.NewVerticalScroll();
      ListDock.AddHeader(FilterScroll);

      this.FilterStack = Surface.NewHorizontalStack();
      FilterScroll.Content = FilterStack;

      this.FirstColumnStack = Surface.NewVerticalStack();
      FirstColumnStack.Background.Colour = Inv.Colour.DimGray.Darken(0.25F);
      FirstColumnStack.Padding.Set(4, 4, 0, 4);

      var TileDock = Surface.NewVerticalDock();
      ListDock.AddClient(TileDock);
      TileDock.Margin.Set(4, 0, 0, 0);
      TileDock.Padding.Set(4, 0, 4, 0);
      TileDock.Background.Colour = Inv.Colour.DimGray;

      this.FilterEdit = Surface.NewSearchEdit();
      TileDock.AddHeader(FilterEdit);
      FilterEdit.Background.Colour = Inv.Colour.Black;
      FilterEdit.Font.Size = 16;
      FilterEdit.Font.Colour = Inv.Colour.White;
      FilterEdit.Margin.Set(0, 4, 0, 4);
      FilterEdit.Border.Set(4);
      FilterEdit.Padding.Set(4);
      FilterEdit.ChangeEvent += () =>
      {
        if (!string.IsNullOrWhiteSpace(FilterEdit.Text))
          this.ActiveFilter = AllFilter;
        Refresh();
      };

      var TileFlow = Surface.NewFlow();
      TileDock.AddClient(TileFlow);
      TileFlow.Padding.Set(4, 0, 4, 0);

      this.TileSection = TileFlow.AddSection();
      TileSection.ItemQuery += (Index) => ComposeTile(Index);

      this.ActiveFilter = AllFilter;
    }

    public string Caption
    {
      set
      {
        if (CaptionLabel == null)
        {
          this.CaptionLabel = Surface.NewLabel();
          Base.AddHeader(CaptionLabel);
          CaptionLabel.Background.Colour = Inv.Colour.DimGray.Darken(0.50F);
          CaptionLabel.Font.Colour = Inv.Colour.WhiteSmoke;
          CaptionLabel.Padding.Set(5);
          CaptionLabel.LineWrapping = false;
        }

        CaptionLabel.Font.Size = Surface.Window.Width < 768 || Surface.Window.Height < 768 ? 20 / 2 : 20;
        CaptionLabel.Visibility.Set(value != null);
        CaptionLabel.Text = value;
      }
    }
    public Inv.Margin Margin => Base.Margin;
    public Inv.Alignment Alignment => Base.Alignment;
    public Inv.Size Size => Base.Size;
    public PaletteFilter AllFilter { get; private set; }

    public void AddFilter(PaletteFilter Filter)
    {
      if (LastColumnStack.PanelCount >= 20)
      {
        if (CacheStackList.Count == 0)
        {
          this.LastColumnStack = Surface.NewVerticalStack();
          LastColumnStack.Padding.Set(0, 52, 0, 4); // +48 top padding to have a leading space equivalent to the ALL button.
          LastColumnStack.Background.Colour = Inv.Colour.DimGray.Darken(0.25F);
        }
        else
        {
          LastColumnStack = CacheStackList.RemoveFirst(); // must take from the front of the queue.
          LastColumnStack.RemovePanels();
        }

        FilterStack.AddPanel(LastColumnStack);

        ColumnStackList.Add(LastColumnStack);
      }

      LastColumnStack.AddPanel(Filter);
      FilterList.Add(Filter);
    }
    public PaletteFilter NewFilter(string Handle, Inv.Image Image)
    {
      var Result = new PaletteFilter(Surface, Handle, Image);
      Result.SingleTapEvent += () =>
      {
        this.ActiveFilter = Result;
        FilterEdit.Text = "";
        Refresh();
      };
      return Result;
    }
    public PaletteFilter AddFilter(string Handle, Inv.Image Image)
    {
      var Result = NewFilter(Handle, Image);

      AddFilter(Result);

      return Result;
    }
    public PaletteBand AddBand()
    {
      var Result = new PaletteBand(Surface);

      Base.AddFooter(Result);

      return Result;
    }
    public void Compose(Action Action)
    {
      FilterList.Clear();

      // remove all column stacks.
      FilterStack.RemovePanels();

      // prepare first stack.
      FirstColumnStack.RemovePanels();
      FilterStack.AddPanel(FirstColumnStack);
      this.LastColumnStack = FirstColumnStack;

      // clear the used column stacks and put back into the cache.
      foreach (var ColumnStack in ColumnStackList)
        ColumnStack.RemovePanels();
      CacheStackList.AddRange(ColumnStackList);
      ColumnStackList.Clear();

      if (AllFilter == null)
        this.AllFilter = NewFilter("_ALL_", Resources.Images.BookmarkWhite);
      AddFilter(AllFilter);

      Action();

      if (ActiveFilter != null)
        this.ActiveFilter = FilterList.Find(F => F.Handle == ActiveFilter.Handle);

      if (ActiveFilter == null)
        this.ActiveFilter = AllFilter;

      Refresh();
    }

    private void Refresh()
    {
      if (ActiveFilter != null && !FilterList.Contains(ActiveFilter))
        this.ActiveFilter = AllFilter;

      foreach (var Filter in FilterList)
        Filter.Colour = ActiveFilter == Filter ? Inv.Colour.DodgerBlue : Inv.Colour.Transparent;

      var FilterText = FilterEdit.Text;

      IEnumerable<PaletteTile> TileQuery;
      if (ActiveFilter == AllFilter)
        TileQuery = FilterList.SelectMany(G => G.GetTileList()).OrderBy(I => I.Text);
      else
        TileQuery = ActiveFilter.GetTileList();

      if (!string.IsNullOrWhiteSpace(FilterText))
      {
        var FragmentArray = FilterText.Split(' ');

        FilterEdit.Border.Colour = Inv.Colour.DodgerBlue;
        TileQuery = TileQuery.Where(T => T.Text.ContainsAll(FragmentArray, StringComparison.CurrentCultureIgnoreCase));
      }
      else
      {
        FilterEdit.Border.Colour = Inv.Colour.Transparent;
      }

      this.ActiveTileList = TileQuery.ToDistinctList();
      TileSection.SetItemCount(ActiveTileList.Count);
      TileSection.Reload();
    }
    private PaletteTile ComposeTile(int Index)
    {
      var Result = ActiveTileList[Index];

      Result.ComposeInvoke();

      return Result;
    }

    private Inv.DistinctList<PaletteFilter> FilterList;
    private Inv.DistinctList<PaletteTile> ActiveTileList;
    private Inv.Surface Surface;
    private Inv.Stack FilterStack;
    private Inv.Stack FirstColumnStack;
    private Inv.Stack LastColumnStack;
    private Inv.DistinctList<Inv.Stack> ColumnStackList;
    private Inv.DistinctList<Inv.Stack> CacheStackList;
    private Inv.FlowSection TileSection;
    private Inv.Edit FilterEdit;
    private PaletteFilter ActiveFilter;
    private Inv.Dock ListDock;
    private Inv.Label CaptionLabel;
  }

  internal sealed class PaletteFilter : Inv.Panel<CustomButton>
  {
    public PaletteFilter(Inv.Surface Surface, string Handle, Inv.Image Image)
    {
      this.Surface = Surface;
      this.Handle = Handle;
      this.Image = Image;

      this.TileList = new Inv.DistinctList<PaletteTile>();
      this.Base = new CustomButton(Surface);
      Base.Border.Set(4);
      Base.Border.Colour = Inv.Colour.Transparent;
      Base.Background.Colour = Inv.Colour.Black;

      if (Image == null)
      {
        this.Label = Surface.NewLabel();
        Base.Content = Label;
        Label.Size.Set(40, 40);
        Label.Padding.Set(4);
        Label.Justify.Center();
        Label.Font.Monospaced();
        Label.Font.Colour = Inv.Colour.White;
        Label.Font.Size = 16;
      }
      else
      {
        var Graphic = Surface.NewGraphic();
        Base.Content = Graphic;
        Graphic.Size.Set(40, 40);
        Graphic.Padding.Set(4);
        Graphic.Image = Image;
      }
    }

    public Inv.Image Image { get; private set; }
    public string Handle { get; private set; }
    public string Text
    {
      get { return Label.Text; }
      set { Label.Text = value; }
    }
    public Inv.Colour Colour
    {
      get { return Base.Border.Colour; }
      set { Base.Border.Colour = value; }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }

    public PaletteTile NewTile(string Handle)
    {
      return new PaletteTile(Surface, Handle);
    }
    public PaletteTile AddTile(string Handle)
    {
      var Result = NewTile(Handle);

      AddTile(Result);

      return Result;
    }
    public void AddTile(PaletteTile Tile)
    {
      TileList.Add(Tile);
    }
    public Inv.DistinctList<PaletteTile> GetTileList()
    {
      return TileList;
    }
    public void RemoveTiles()
    {
      TileList.Clear();
    }

    private readonly Inv.Surface Surface;
    private Inv.Label Label;
    private Inv.DistinctList<PaletteTile> TileList;
  }

  internal sealed class PaletteTile : Inv.Panel<CustomButton>
  {
    public PaletteTile(Inv.Surface Surface, string Handle)
    {
      this.Handle = Handle;

      this.Base = new CustomButton(Surface);

      this.Dock = Surface.NewHorizontalDock();
      Base.Content = Dock;

      this.Graphic = Surface.NewGraphic();
      Dock.AddHeader(Graphic);
      Graphic.Size.Set(40, 40);
      Graphic.Margin.Set(2);

      this.Label = Surface.NewLabel();
      Dock.AddClient(Label);
      Label.Font.Colour = Inv.Colour.White;
      Label.Font.Size = 16;
    }

    public string Handle { get; private set; }
    public string Text
    {
      get { return Label.Text; }
      set { Label.Text = value; }
    }
    public Inv.Image Image
    {
      get { return Graphic.Image; }
      set { Graphic.Image = value; }
    }
    public event Action ComposeEvent;
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }
    public event Action ContextTapEvent
    {
      add { Base.ContextTapEvent += value; }
      remove { Base.ContextTapEvent -= value; }
    }

    public void AddFooter(Inv.Panel Panel)
    {
      Dock.AddFooter(Panel);
    }

    internal void ComposeInvoke()
    {
      if (ComposeEvent != null)
        ComposeEvent();
    }

    private Inv.Dock Dock;
    private Inv.Graphic Graphic;
    private Inv.Label Label;
  }

  internal sealed class PaletteBand : Inv.Panel<Inv.Dock>
  {
    public PaletteBand(Inv.Surface Surface)
    {
      this.Surface = Surface;
      this.Base = Surface.NewHorizontalDock();
      Base.Padding.Set(0, 0, 4, 0);
    }

    public PaletteToggle AddToggle(string Text)
    {
      var Result = new PaletteToggle(Surface, Text);

      Base.AddClient(Result);

      return Result;
    }

    private readonly Inv.Surface Surface;
  }

  internal sealed class PaletteToggle : Inv.Panel<CustomButton>
  {
    public PaletteToggle(Inv.Surface Surface, string Text)
    {
      this.Base = new CustomButton(Surface);
      Base.Margin.Set(4, 4, 0, 0);
      Base.Border.Colour = Inv.Colour.Transparent;
      Base.Background.Colour = Inv.Colour.Black;
      Base.SingleTapEvent += () => IsChecked = !IsChecked;

      this.Label = Surface.NewLabel();
      Base.Content = Label;
      Label.Padding.Set(4);
      Label.Justify.Center();
      Label.Font.Colour = Inv.Colour.White;
      Label.Font.Size = 12;
      Label.Text = Text;
    }

    public string Text
    {
      get { return Label.Text; }
      set { Label.Text = value; }
    }
    public bool IsEnabled
    {
      get { return Base.IsEnabled; }
      set { Base.IsEnabled = value; }
    }
    public bool IsChecked
    {
      get { return Base.Background.Colour == Inv.Colour.DodgerBlue; }
      set { Base.Background.Colour = value ? Inv.Colour.DodgerBlue : Inv.Colour.Black; }
    }
    public event Action SingleTapEvent
    {
      add { Base.SingleTapEvent += value; }
      remove { Base.SingleTapEvent -= value; }
    }

    private Inv.Label Label;
  }

  internal sealed class BusySurface : Inv.Mimic<Inv.Surface>
  {
    public BusySurface(Inv.Surface Surface)
    {
      this.Base = Surface;

      var OuterStack = Base.NewVerticalStack();
      Base.Content = OuterStack;
      OuterStack.Alignment.CenterStretch();

      var LogoGraphic = Base.NewGraphic();
      OuterStack.AddPanel(LogoGraphic);
      LogoGraphic.Size.Set(196);
      LogoGraphic.Padding.Set(32);
      LogoGraphic.Image = Resources.Images.PhoenixLogo500x500;

      var InnerStack = Base.NewVerticalStack();
      OuterStack.AddPanel(InnerStack);
      InnerStack.Alignment.CenterStretch();

      this.TitleLabel = Base.NewLabel();
      InnerStack.AddPanel(TitleLabel);
      TitleLabel.Font.Colour = Inv.Colour.White;
      TitleLabel.Justify.Center();

      this.DescriptionLabel = Base.NewLabel();
      InnerStack.AddPanel(DescriptionLabel);
      DescriptionLabel.Font.Colour = Inv.Colour.LightGray;
      DescriptionLabel.Justify.Center();

      var Animation = Base.NewAnimation();
      Animation.AddTarget(LogoGraphic).RotateAngle(0, 360, TimeSpan.FromSeconds(30));

      Base.KeystrokeEvent += (Keystroke) =>
      {
        Return();
      };
      Base.EnterEvent += () =>
      {
        Animation.Start();
      };
      Base.LeaveEvent += () =>
      {
        Animation.Stop();
      };
      Base.ArrangeEvent += () =>
      {
        if (Base.Window.Width >= 600 && Base.Window.Height >= 600)
          OuterStack.SetVertical();
        else if (Base.Window.Width < Base.Window.Height) // portrait
          OuterStack.SetVertical();
        else
          OuterStack.SetHorizontal();

        TitleLabel.Font.Size = Math.Min(100, Base.Window.Width / 12);
        DescriptionLabel.Font.Size = TitleLabel.Font.Size / 2;
      };
    }

    public string Title
    {
      get { return TitleLabel.Text; }
      set
      {
        TitleLabel.Text = value;
        TitleLabel.Visibility.Set(Title != null);
      }
    }
    public string Description
    {
      get { return DescriptionLabel.Text; }
      set
      {
        DescriptionLabel.Text = value;
        DescriptionLabel.Visibility.Set(Description != null);
      }
    }

    public void Become()
    {
      this.Parent = Base.Window.ActiveSurface;
      Base.Window.Transition(Base).Fade();
    }
    public void Return()
    {
      if (Parent != null)
      {
        Base.Window.Transition(Parent).Fade();
        this.Parent = null;
      }
    }

    private Inv.Surface Parent;
    private Inv.Label TitleLabel;
    private Inv.Label DescriptionLabel;
  }
}