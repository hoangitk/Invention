﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Globalization;

namespace Inv
{
  /// <summary>
  /// Associates with the current thread to allow affinity checks.
  /// </summary>
  /// <remarks>
  /// <para>Use instances of <see cref="ThreadAffinity"/> to ensure that classes are only being accessed from the thread they were started on.</para>
  /// <example>
  /// <para>This example uses <see cref="ThreadAffinity"/> to protect the Data property of MyClass from being
  /// accessed by threads other than the thread that instantiated the MyClass object.</para>
  /// <code>
  /// public sealed class MyClass
  /// {
  ///   private Inv.ThreadRequirement ThreadRequirement = new Inv.ThreadRequirement();
  ///   private int DataField = 0;
  ///   
  ///   public Data
  ///   {
  ///     get
  ///     {
  ///       // Will throw an InvalidOperationException if the thread accessing Data is not
  ///       // the thread that created the instance of MyClass.
  ///       ThreadRequirement.Check();
  ///       
  ///       return DataField;
  ///     }
  ///     
  ///     set
  ///     {
  ///       // Will throw an InvalidOperationException if the thread accessing Data is not
  ///       // the thread that created the instance of MyClass.
  ///       ThreadRequirement.Check();
  ///       
  ///       this.DataField = value;
  ///     }
  ///   }
  /// }
  /// </code>
  /// </example>
  /// </remarks>
  public sealed class ThreadAffinity
  {
    /// <summary>
    /// Initialises a new instance of the <see cref="ThreadAffinity"/> class with affinity to the currently executing thread.
    /// </summary>
    public ThreadAffinity()
    {
      this.ThreadID = Thread.CurrentThread.ManagedThreadId;
    }
    
    /// <summary>
    /// Throw an exception if we are NOT executing on the affinity thread.
    /// </summary>
    public void Require()
    {
      if (Thread.CurrentThread.ManagedThreadId != ThreadID)
        throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "Access prevented to the affinity thread [{0}] from thread [{1}].", ThreadID, Thread.CurrentThread.ManagedThreadId));
    }
    /// <summary>
    /// Throw an exception if we are executing on the affinity thread.
    /// </summary>
    public void Prevent()
    {
      if (Thread.CurrentThread.ManagedThreadId == ThreadID)
        throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "Access prevented from the affinity thread [{0}].", ThreadID));
    }

    private readonly int ThreadID;
  }
}