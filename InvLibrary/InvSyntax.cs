﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.IO;
using Inv.Support;

namespace Inv.Syntax
{
  public static class Foundation
  {
    public static void WriteTextString(Inv.Syntax.Grammar SyntaxGrammar, out string TextString, Action<Inv.Syntax.Writer> Action)
    {
      using (var StringWriter = new StringWriter())
      {
        var SyntaxWriter = new Inv.Syntax.Writer(new Inv.Syntax.TextWriteService(StringWriter, SyntaxGrammar));

        SyntaxWriter.Start();

        Action(SyntaxWriter);

        SyntaxWriter.Stop();

        StringWriter.Flush();

        TextString = StringWriter.ToString();
      }
    }
    public static void ReadTextString(Inv.Syntax.Grammar SyntaxGrammar, string TextString, Action<Inv.Syntax.Reader> Action)
    {
      using (var StringReader = new StringReader(TextString))
      {
        var SyntaxReader = new Inv.Syntax.Reader(new Inv.Syntax.TextReadService(StringReader, SyntaxGrammar));

        SyntaxReader.Start();

        Action(SyntaxReader);

        SyntaxReader.Stop();
      }
    }
    public static void WriteTextStream(Inv.Syntax.Grammar SyntaxGrammar, StreamWriter StreamWriter, Action<Inv.Syntax.Writer> Action)
    {
      var SyntaxWriter = new Inv.Syntax.Writer(new Inv.Syntax.TextWriteService(StreamWriter, SyntaxGrammar));

      SyntaxWriter.Start();

      Action(SyntaxWriter);

      SyntaxWriter.Stop();

      StreamWriter.Flush();
    }
    public static void WriteTextStream(Inv.Syntax.Grammar SyntaxGrammar, Stream Stream, Action<Inv.Syntax.Writer> Action)
    {
      using (var StreamWriter = new StreamWriter(Stream, Encoding.UTF8, 65536, true))
      {
        WriteTextStream(SyntaxGrammar, StreamWriter, Action);
      }
    }
    public static void ReadTextStream(Inv.Syntax.Grammar SyntaxGrammar, Stream Stream, Action<Inv.Syntax.Reader> Action, string FileName = null)
    {
      using (var StreamReader = new StreamReader(Stream, Encoding.UTF8, true, 65536, true))
      {
        ReadTextStream(SyntaxGrammar, StreamReader, Action, FileName);
      }
    }
    public static void ReadTextStream(Inv.Syntax.Grammar SyntaxGrammar, StreamReader StreamReader, Action<Inv.Syntax.Reader> Action, string FileName = null)
    {
      var SyntaxReader = new Inv.Syntax.Reader(new Inv.Syntax.TextReadService(StreamReader, SyntaxGrammar, FileName));

      SyntaxReader.Start();

      Action(SyntaxReader);

      SyntaxReader.Stop();
    }

    public static void WriteBinaryBuffer(Inv.Syntax.Grammar SyntaxGrammar, out byte[] Buffer, Action<Inv.Syntax.Writer> Action)
    {
      using (var MemoryStream = new MemoryStream())
      {
        WriteBinaryStream(SyntaxGrammar, MemoryStream, Action);

        Buffer = MemoryStream.ToArray();
      }
    }
    public static void ReadBinaryBuffer(Inv.Syntax.Grammar SyntaxGrammar, byte[] Buffer, Action<Inv.Syntax.Reader> Action)
    {
      using (var MemoryStream = new MemoryStream(Buffer))
        ReadBinaryStream(SyntaxGrammar, MemoryStream, Action);
    }
    public static void WriteBinaryStream(Inv.Syntax.Grammar SyntaxGrammar, Stream Stream, Action<Inv.Syntax.Writer> Action)
    {
      using (var BinaryWriter = new System.IO.BinaryWriter(Stream, Encoding.UTF8, true))
      {
        var SyntaxWriter = new Inv.Syntax.Writer(new Inv.Syntax.BinaryWriteService(BinaryWriter));

        SyntaxWriter.Start();

        Action(SyntaxWriter);

        SyntaxWriter.Stop();
      }
    }
    public static void ReadBinaryStream(Inv.Syntax.Grammar SyntaxGrammar, Stream Stream, Action<Inv.Syntax.Reader> Action)
    {
      using (var BinaryReader = new System.IO.BinaryReader(Stream, Encoding.UTF8, true))
      {
        var SyntaxReader = new Inv.Syntax.Reader(new Inv.Syntax.BinaryReadService(BinaryReader));

        SyntaxReader.Start();

        Action(SyntaxReader);

        SyntaxReader.Stop();
      }
    }

    public static Inv.Syntax.Document WriteSyntaxDocument(Inv.Syntax.Grammar SyntaxGrammar, Action<Inv.Syntax.Writer> Action)
    {
      var SyntaxDocument = new Inv.Syntax.Document(SyntaxGrammar);

      var SyntaxWriter = new Inv.Syntax.Writer(new Inv.Syntax.DocumentWriteService(SyntaxDocument));

      SyntaxWriter.Start();

      Action(SyntaxWriter);

      SyntaxWriter.Stop();

      return SyntaxDocument;
    }
    public static string ConvertSyntaxDocumentToTextString(Inv.Syntax.Document SyntaxDocument)
    {
      using (var StringWriter = new StringWriter())
      {
        var TextService = new Inv.Syntax.TextWriteService(StringWriter, SyntaxDocument.Grammar);

        var WriteContract = (Inv.Syntax.WriteContract)TextService;
        WriteContract.Start();

        foreach (var SyntaxToken in SyntaxDocument.TokenList)
          WriteContract.WriteToken(SyntaxToken);

        WriteContract.Stop();

        StringWriter.Flush();

        return StringWriter.ToString();
      }
    }
    public static Inv.Syntax.Document ConvertTextStringToSyntaxDocument(Inv.Syntax.Grammar SyntaxGrammar, string TextString)
    {
      var SyntaxDocument = new Inv.Syntax.Document(SyntaxGrammar);

      using (var StringReader = new StringReader(TextString))
      {
        var TextService = new Inv.Syntax.TextReadService(StringReader, SyntaxGrammar);

        var ReadContract = (Inv.Syntax.ReadContract)TextService;
        ReadContract.Start();

        // NOTE: adding the return from ReadToken() is currently correct for the text service (FYI: binary service doesn't work this way).
        while (ReadContract.More())
        {
          SyntaxDocument.TokenList.Add(ReadContract.PeekToken());
          ReadContract.SkipToken();
        }

        ReadContract.Stop();
      }

      return SyntaxDocument;
    }
    public static Inv.Syntax.Writer StartTextSyntaxWriter(Inv.Syntax.Grammar SyntaxGrammar, StreamWriter StreamWriter)
    {
      var SyntaxWriter = new Inv.Syntax.Writer(new Inv.Syntax.TextWriteService(StreamWriter, SyntaxGrammar));

      SyntaxWriter.Start();

      return SyntaxWriter;
    }
    public static void StopSyntaxWriter(Inv.Syntax.Writer SyntaxWriter)
    {
      SyntaxWriter.Stop();
    }

    public static bool IsIdentifierSymbol(char Symbol)
    {
      return SymbolTable.IdentifierCharacterSet.ContainsValue(Symbol);
    }
    public static bool TranslateTokenType(char Symbol, out TokenType TokenType)
    {
      return SymbolTable.CharacterTokenTypeIndex.TryGetByLeft(Symbol, out TokenType);
    }
    public static char RepresentTokenType(TokenType TokenType)
    {
      return SymbolTable.CharacterTokenTypeIndex.GetByRight(TokenType);
    }
  }

  internal interface WriteContract
  {
    void Start();
    void Stop();
    void WriteToken(Inv.Syntax.Token Token);
    string Publish();
  }

  internal interface ReadContract
  {
    void Start();
    void Stop();
    bool More();
    Inv.Syntax.Token PeekToken();
    void SkipToken();
    int GetPosition();
    ReadException Interrupt(string Message);
  }

  internal sealed class DocumentWriteService : WriteContract
  {
    public DocumentWriteService(Inv.Syntax.Document ActiveDocument)
    {
      this.ActiveDocument = ActiveDocument;
    }

    void WriteContract.Start()
    {
      ActiveDocument.TokenList.Clear();
      LineCount = 0;
    }
    void WriteContract.Stop()
    {
      LineCount = -1;
    }
    void WriteContract.WriteToken(Inv.Syntax.Token Token)
    {
      if (Token.Type == Inv.Syntax.TokenType.Newline)
        LineCount++;

      var CloneToken = new Inv.Syntax.Token();

      switch (Token.Type)
      {
        case Inv.Syntax.TokenType.Newline:
          CloneToken.Newline = Token.Newline;
          break;

        case Inv.Syntax.TokenType.Whitespace:
          CloneToken.Whitespace = Token.Whitespace;
          break;

        case Inv.Syntax.TokenType.Identifier:
          CloneToken.Identifier = Token.Identifier;
          break;

        case Inv.Syntax.TokenType.Keyword:
          CloneToken.Keyword = Token.Keyword;
          break;

        case Inv.Syntax.TokenType.Number:
          CloneToken.Number = Token.Number;
          break;

        case Inv.Syntax.TokenType.String:
          CloneToken.String = Token.String;
          break;

        case Inv.Syntax.TokenType.DateTime:
          CloneToken.DateTime = Token.DateTime;
          break;

        case Inv.Syntax.TokenType.Timestamp:
          CloneToken.Timestamp = Token.Timestamp;
          break;

        case Inv.Syntax.TokenType.TimestampLegacy:
          CloneToken.TimestampLegacy = Token.TimestampLegacy;
          break;

        case Inv.Syntax.TokenType.Date:
          CloneToken.Date = Token.Date;
          break;

        case Inv.Syntax.TokenType.Time:
          CloneToken.Time = Token.Time;
          break;

        case Inv.Syntax.TokenType.TimePeriod:
          CloneToken.TimePeriod = Token.TimePeriod;
          break;

        case Inv.Syntax.TokenType.TimeSpan:
          CloneToken.TimeSpan = Token.TimeSpan;
          break;

        case Inv.Syntax.TokenType.Base64:
          CloneToken.Base64 = Token.Base64;
          break;

        case Inv.Syntax.TokenType.Hexadecimal:
          CloneToken.Hexadecimal = Token.Hexadecimal;
          break;

        default:
          CloneToken.Type = Token.Type;
          break;
      }

      ActiveDocument.TokenList.Add(CloneToken);
    }
    string WriteContract.Publish()
    {
      return $"Line {LineCount}";
    }

    private readonly Inv.Syntax.Document ActiveDocument;
    private int LineCount;
  }

  internal sealed class DocumentReadService : ReadContract
  {
    public DocumentReadService(Inv.Syntax.Document Active)
    {
      this.ActiveDocument = Active;
    }

    void ReadContract.Start()
    {
      CurrentLineIndex = 1;
      CurrentTokenIndex = -1;

      AdvanceToken();
    }
    void ReadContract.Stop()
    {
      CurrentTokenIndex = -1;
      CurrentLineIndex = 0;
    }
    bool ReadContract.More()
    {
      return CurrentTokenIndex < ActiveDocument.TokenList.Count;
    }
    Inv.Syntax.Token ReadContract.PeekToken()
    {
      if (CurrentTokenIndex < ActiveDocument.TokenList.Count)
        return ActiveDocument.TokenList[CurrentTokenIndex];
      else
        return null;
    }
    void ReadContract.SkipToken()
    {
      if (CurrentTokenIndex >= ActiveDocument.TokenList.Count)
        throw new Exception($"Line {CurrentLineIndex}: expected a token but found the end of the document.");

      AdvanceToken();
    }
    int ReadContract.GetPosition()
    {
      return CurrentTokenIndex;
    }
    ReadException ReadContract.Interrupt(string Message)
    {
      return new ReadException($"Line {CurrentLineIndex}: {Message}", CurrentLineIndex);
    }

    private void AdvanceToken()
    {
      CurrentTokenIndex++;

      while (CurrentTokenIndex < ActiveDocument.TokenList.Count && ActiveDocument.TokenList[CurrentTokenIndex].Type == Inv.Syntax.TokenType.Newline)
      {
        CurrentLineIndex++;

        CurrentTokenIndex++;
      }
    }

    private readonly Inv.Syntax.Document ActiveDocument;
    private int CurrentTokenIndex;
    private int CurrentLineIndex;
  }

  internal static class TextFormat
  {
    public const string DateTimeOffsetLegacyFormat = "yyyy-MM-dd HH:mm:ss.ffffff zzz";
    public const string DateTimeOffsetFormat = "yyyy-MM-dd HH:mm:ss.fffffff zzz";
    public const string DateTimeFormat = "yyyy-MM-dd HH:mm:ss.fffffff";
    public const string DateFormat = "yyyy-MM-dd";
    public const string TimeFormat = "HH:mm:ss.fffffff";
    public const string TimeSpanFormat = @"hh\:mm\:ss\.fffffff";
    public const string TimeLegacyFormat = "HH:mm:ss.ffffff";

    public static readonly string[] DateTimeOffsetFormatArray = new string[] { DateTimeOffsetFormat, DateTimeOffsetLegacyFormat };
    public static readonly string[] TimeFormatArray = new string[] { TimeFormat, TimeLegacyFormat, "HH:mm:ss", "HH:mm" };
    public static readonly string[] TimeSpanFormatArray = new string[] { TimeSpanFormat, "hh:mm:ss", "hh:mm" }; 
  }

  public sealed class TextWriteService : WriteContract
  {
    public TextWriteService(System.IO.TextWriter ActiveWriter, Inv.Syntax.Grammar SyntaxGrammar)
    {
      this.ActiveWriter = ActiveWriter;
      this.SyntaxGrammar = SyntaxGrammar;
    }

    void WriteContract.Start()
    {
      this.GrammarManager = new GrammarManager(SyntaxGrammar);

      LineCount = 1;
    }
    void WriteContract.Stop()
    {
      this.GrammarManager = null;

      LineCount = 0;
    }
    void WriteContract.WriteToken(Inv.Syntax.Token Token)
    {
      if (Token.Type == Inv.Syntax.TokenType.Newline)
      {
        for (var NewlineCount = 0; NewlineCount < Token.Newline; NewlineCount++)
        {
          ActiveWriter.WriteLine();
          LineCount++;
        }
      }
      else
      {
        string Representation;

        switch (Token.Type)
        {
          case Syntax.TokenType.Whitespace:
            Representation = Token.Whitespace;
            break;

          case Syntax.TokenType.Keyword:
            Representation = Token.Keyword;
            break;

          case Syntax.TokenType.Identifier:
            Representation = Token.Identifier;

            // NOTE: we want blank identifiers to be rendered as [] - as this is hack for the Forge Data DSL aggregates.
            if ((Representation.Length == 0 || SyntaxGrammar.AlwaysQuoteIdentifier || !SymbolTable.IdentifierCharacterSet.Conforms(Representation) || GrammarManager.ContainsKeyword(Representation) || SymbolTable.NumberCharacterSet.Contains(Representation[0]) || Representation[0] == '_') && SyntaxGrammar.IdentifierOpenQuote != '\0')
            {
              if (Representation.Contains(SyntaxGrammar.IdentifierOpenQuote, SyntaxGrammar.IdentifierCloseQuote))
                throw new Exception($"Identifier must not contain the open symbol '{SyntaxGrammar.IdentifierOpenQuote}' or close symbol '{SyntaxGrammar.IdentifierCloseQuote}'.");

              Representation = SyntaxGrammar.IdentifierOpenQuote + Representation + SyntaxGrammar.IdentifierCloseQuote;
            }
            break;

          case Syntax.TokenType.Comment:
            Representation = SyntaxGrammar.SingleLineCommentPrefix + Token.Comment;
            break;

          case Syntax.TokenType.String:
            bool StringVerbatim;
            var TokenString = Token.String;

            if (TokenString == "")
            {
              Representation = "";
              StringVerbatim = false;
            }
            else if (SyntaxGrammar.StringEscape != null)
            {
              if (TokenString.Contains('\n') || TokenString.Contains('\r') || TokenString.Contains('\t') || TokenString.Contains(SyntaxGrammar.StringAroundQuote.Value))
              {
                var StringBuilder = new StringBuilder();

                foreach (var TokenCharacter in TokenString)
                {
                  switch (TokenCharacter)
                  {
                    case '\r':
                      StringBuilder.Append(SyntaxGrammar.StringEscape + "r");
                      break;

                    case '\n':
                      StringBuilder.Append(SyntaxGrammar.StringEscape + "n");
                      break;

                    case '\t':
                      StringBuilder.Append(SyntaxGrammar.StringEscape + "t");
                      break;

                    default:
                      if (TokenCharacter == SyntaxGrammar.StringAroundQuote)
                        StringBuilder.Append(new string(SyntaxGrammar.StringEscape.Value, 1) + new string(SyntaxGrammar.StringAroundQuote.Value, 1));
                      else if (TokenCharacter == SyntaxGrammar.StringEscape)
                        StringBuilder.Append(new string(SyntaxGrammar.StringEscape.Value, 2));
                      else
                        StringBuilder.Append(TokenCharacter);
                      break;
                  }
                }

                Representation = StringBuilder.ToString();

                StringVerbatim = false;
              }
              else
              {
                Representation = TokenString ?? "";
                StringVerbatim = SyntaxGrammar.StringVerbatim != null && Representation.Contains(SyntaxGrammar.StringEscape.Value);

                if (!StringVerbatim)
                  Representation = Representation.Replace(SyntaxGrammar.StringEscape.ToString(), SyntaxGrammar.StringEscape.ToString() + SyntaxGrammar.StringEscape.ToString());
              }
            }
            else
            {
              Representation = TokenString.Replace(SyntaxGrammar.StringAroundQuote.ToString(), SyntaxGrammar.StringAroundQuote.ToString() + SyntaxGrammar.StringAroundQuote.ToString());
              StringVerbatim = false;
            }

            Representation = SyntaxGrammar.StringAroundQuote + Representation + SyntaxGrammar.StringAroundQuote;

            if (StringVerbatim)
              Representation = SyntaxGrammar.StringVerbatim.ToString() + Representation;
            break;

          case Syntax.TokenType.Number:
            Representation = Token.Number;
            break;

          case Inv.Syntax.TokenType.DateTime:
            Representation = Token.DateTime.ToString(TextFormat.DateTimeFormat, System.Globalization.CultureInfo.InvariantCulture);
            if (SyntaxGrammar.IdentifierOpenQuote != '\0')
              Representation = SyntaxGrammar.IdentifierOpenQuote + SyntaxGrammar.DateTimePrefix + Representation + SyntaxGrammar.IdentifierCloseQuote;
            break;

          case Inv.Syntax.TokenType.Timestamp:
            Representation = Token.Timestamp.ToString(TextFormat.DateTimeOffsetFormat, System.Globalization.CultureInfo.InvariantCulture);
            if (SyntaxGrammar.IdentifierOpenQuote != '\0')
              Representation = SyntaxGrammar.IdentifierOpenQuote + SyntaxGrammar.DateTimePrefix + Representation + SyntaxGrammar.IdentifierCloseQuote;
            break;

          case Inv.Syntax.TokenType.TimestampLegacy:
            Representation = Token.TimestampLegacy.ToString(TextFormat.DateTimeOffsetLegacyFormat, System.Globalization.CultureInfo.InvariantCulture);
            if (SyntaxGrammar.IdentifierOpenQuote != '\0')
              Representation = SyntaxGrammar.IdentifierOpenQuote + SyntaxGrammar.DateTimePrefix + Representation + SyntaxGrammar.IdentifierCloseQuote;
            break;

          case Inv.Syntax.TokenType.Date:
            Representation = Token.Date.ToString(TextFormat.DateFormat);
            if (SyntaxGrammar.IdentifierOpenQuote != '\0')
              Representation = SyntaxGrammar.IdentifierOpenQuote + SyntaxGrammar.DatePrefix + Representation + SyntaxGrammar.IdentifierCloseQuote;
            break;

          case Inv.Syntax.TokenType.Time:
            Representation = Token.Time.ToString(TextFormat.TimeFormat);
            if (SyntaxGrammar.IdentifierOpenQuote != '\0')
              Representation = SyntaxGrammar.IdentifierOpenQuote + SyntaxGrammar.TimePrefix + Representation + SyntaxGrammar.IdentifierCloseQuote;
            break;

          case Inv.Syntax.TokenType.TimePeriod:
            Representation = Token.TimePeriod.ToString();
            if (SyntaxGrammar.IdentifierOpenQuote != '\0')
              Representation = SyntaxGrammar.IdentifierOpenQuote + SyntaxGrammar.TimePeriodPrefix + Representation + SyntaxGrammar.IdentifierCloseQuote;
            break;

          case Inv.Syntax.TokenType.TimeSpan:
            Representation = Token.TimeSpan.ToString(TextFormat.TimeSpanFormat, System.Globalization.CultureInfo.InvariantCulture);
            if (SyntaxGrammar.IdentifierOpenQuote != '\0')
              Representation = SyntaxGrammar.IdentifierOpenQuote + SyntaxGrammar.TimeSpanPrefix + Representation + SyntaxGrammar.IdentifierCloseQuote;
            break;

          case Syntax.TokenType.Base64:
            Representation = Convert.ToBase64String(Token.Base64.GetBuffer());
            if (SyntaxGrammar.UseBase64Prefix)
              Representation = "0b" + Representation;
            break;

          case Syntax.TokenType.Hexadecimal:
            if (SyntaxGrammar.UseHexadecimalPrefix)
              Representation = Token.Hexadecimal.GetBuffer().ToHexadecimal("0x");
            else
              Representation = Token.Hexadecimal.GetBuffer().ToHexadecimal();
            break;

          default:
            Representation = Inv.Syntax.Foundation.RepresentTokenType(Token.Type.Value).ToString();
            break;
        }

        ActiveWriter.Write(Representation);
      }
    }
    string WriteContract.Publish()
    {
      return $"Line {LineCount}";
    }

    private readonly Inv.Syntax.Grammar SyntaxGrammar;
    private readonly System.IO.TextWriter ActiveWriter;
    private GrammarManager GrammarManager;
    private int LineCount;
  }

  public sealed class TextReadService : ReadContract
  {
    public TextReadService(TextReader ActiveReader, Inv.Syntax.Grammar SyntaxGrammar, string FilePath = null)
    {
      this.ActiveReader = ActiveReader;
      this.SyntaxGrammar = SyntaxGrammar;
      this.FilePath = FilePath;
    }

    void ReadContract.Start()
    {
      this.GrammarManager = new GrammarManager(SyntaxGrammar);
      this.TextParser = new Inv.TextParser(ActiveReader);
      this.LineCount = 1;
      this.Position = -1;

      AdvanceToken();
    }
    void ReadContract.Stop()
    {
      this.GrammarManager = null;

      if (TextParser != null)
      {
        TextParser.Dispose();
        this.TextParser = null;
      }

      this.NextToken = null;
      this.LineCount = 0;
    }
    bool ReadContract.More()
    {
      return NextToken != null;
    }
    Inv.Syntax.Token ReadContract.PeekToken()
    {
      return NextToken;
    }
    void ReadContract.SkipToken()
    {
#if DEBUG_WRITE_TOKENS
      switch (NextToken.Type.Value)
      {
        case TokenType.Identifier: Debug.Write(NextToken.Identifier); break;
        case TokenType.Keyword: Debug.Write(NextToken.Keyword); break;
        case TokenType.Number: Debug.Write(NextToken.Number); break;
        case TokenType.String: Debug.Write('\'' + NextToken.String + '\''); break;
        case TokenType.Whitespace: Debug.Write(NextToken.Whitespace); break;
        case TokenType.Newline: for (var Index = 0; Index < NextToken.Newline; Index++) Debug.WriteLine(""); break;
        case TokenType.Hexadecimal: Debug.Write("0x" + NextToken.Hexadecimal); break;
        case TokenType.DateTime: Debug.Write(NextToken.DateTime); break;
        case TokenType.Base64: Debug.Write(NextToken.Base64); break;
        case TokenType.TimeSpan: Debug.Write(NextToken.TimeSpan); break;
        case TokenType.Comment: Debug.Write("-- " + NextToken.Comment); break;
        default:
          Debug.WriteLine(SymbolTable.CharacterTokenTypeIndex.GetByRight(NextToken.Type.Value).ToString());
          break;
      }
#endif

      AdvanceToken();
    }
    int ReadContract.GetPosition()
    {
      return Position;
    }
    ReadException ReadContract.Interrupt(string Message)
    {
      return NewInterrupt(Message);
    }

    private void AdvanceToken()
    {
      NextToken = null;

      while (NextToken == null && TextParser.More())
      {
        var TokenCharacter = TextParser.NextCharacter();

        if (SymbolTable.WhitespaceCharacterSet.ContainsValue(TokenCharacter))
        {
          var WhitespaceString = TextParser.ReadWhileCharacterSet(SymbolTable.WhitespaceCharacterSet);
          Position += WhitespaceString.Length;

          foreach (var WhitespaceChar in WhitespaceString)
          {
            if (WhitespaceChar == '\n')
              LineCount++;
          }

          if (SyntaxGrammar.IgnoreWhitespace)
          {
            NextToken = null;
          }
          else
          {
            NextToken = new Inv.Syntax.Token();
            NextToken.Whitespace = WhitespaceString;
          }
        }
        else if (TextParser.NextIsString(SyntaxGrammar.SingleLineCommentPrefix))
        {
          TextParser.ReadString(SyntaxGrammar.SingleLineCommentPrefix);
          Position += SyntaxGrammar.SingleLineCommentPrefix.Length;

          var SingleLineComment = TextParser.ReadUntilCharacterSet(SymbolTable.VerticalWhitespaceCharacterSet);
          Position += SingleLineComment.Length;

          if (SyntaxGrammar.IgnoreComment)
          {
            NextToken = null;
          }
          else
          {
            NextToken = new Inv.Syntax.Token();
            NextToken.Comment = SingleLineComment;
          }
        }
        else if (TextParser.NextIsString(SyntaxGrammar.MultiLineCommentStart))
        {
          TextParser.ReadString(SyntaxGrammar.MultiLineCommentStart);
          Position += SyntaxGrammar.MultiLineCommentStart.Length;

          var Count = 0;
          do
          {
            var MultiLineComment = TextParser.ReadUntilString(SyntaxGrammar.MultiLineCommentFinish);
            Position += MultiLineComment.Length;

            Count += MultiLineComment.Split(SyntaxGrammar.MultiLineCommentStart, StringSplitOptions.RemoveEmptyEntries).Length - 1;

            foreach (var MultiLineIndex in MultiLineComment)
            {
              if (MultiLineIndex == '\n')
                LineCount++;
            }

            if (SyntaxGrammar.IgnoreComment)
            {
              NextToken = null;
            }
            else
            {
              NextToken = new Inv.Syntax.Token();
              NextToken.Comment = MultiLineComment;
            }

            TextParser.ReadString(SyntaxGrammar.MultiLineCommentFinish);
            Position += SyntaxGrammar.MultiLineCommentFinish.Length;
            Count--;

          } while (Count >= 0);
        }
        else
        {
          NextToken = new Inv.Syntax.Token();

          if (SymbolTable.NumberCharacterSet.ContainsValue(TokenCharacter))
          {
            var NumberValue = TextParser.ReadWhileCharacterSet(SymbolTable.NumberCharacterSet);
            Position += NumberValue.Length;

            if (SyntaxGrammar.UseHexadecimalPrefix && NumberValue == "0" && Char.ToLower(TextParser.NextCharacter()) == 'x')
            {
              TextParser.ReadCharacter();
              Position++;

              var HexadecimalString = TextParser.ReadWhileCharacterSet(SymbolTable.HexadecimalCharacterSet);
              var HexadecimalLength = HexadecimalString.Length;
              var HexadecimalBuffer = new byte[HexadecimalLength / 2];
              Position += HexadecimalLength;

              for (var HexadecimalIndex = 0; HexadecimalIndex < HexadecimalBuffer.Length; HexadecimalIndex++)
                HexadecimalBuffer[HexadecimalIndex] = Convert.ToByte(HexadecimalString.Substring(HexadecimalIndex * 2, 2), 16);

              NextToken.Hexadecimal = new Inv.Binary(HexadecimalBuffer, "");
            }
            else if (SyntaxGrammar.UseBase64Prefix && NumberValue == "0" && Char.ToLower(TextParser.NextCharacter()) == 'b')
            {
              TextParser.ReadCharacter();
              Position++;

              var Base64String = TextParser.ReadWhileCharacterSet(SymbolTable.Base64CharacterSet);
              Position += Base64String.Length;
              var Base64Buffer = Convert.FromBase64String(Base64String);

              NextToken.Base64 = new Inv.Binary(Base64Buffer, "");
            }
            else
            {
              NextToken.Number = NumberValue;
            }
          }
          else if (TokenCharacter != '_' && SymbolTable.IdentifierCharacterSet.ContainsValue(TokenCharacter))
          {
            // NOTE: identifiers cannot start with an underscore but can include an underscore.

            var IdentifierString = TextParser.ReadWhileCharacterSet(SymbolTable.IdentifierCharacterSet);
            Position += IdentifierString.Length;

            if (GrammarManager.ContainsKeyword(IdentifierString))
            {
              if (SyntaxGrammar.KeywordCaseSensitive)
                NextToken.Keyword = IdentifierString;
              else
                NextToken.Keyword = IdentifierString.ToLower();
            }
            else
            {
              NextToken.Identifier = IdentifierString;
            }
          }
          else if (SyntaxGrammar.StringVerbatim != null && TokenCharacter == SyntaxGrammar.StringVerbatim)
          {
            TextParser.ReadCharacter(SyntaxGrammar.StringVerbatim.Value);
            TextParser.ReadCharacter(SyntaxGrammar.StringAroundQuote.Value);
            Position += 2;

            NextToken.String = TextParser.ReadUntilCharacter(SyntaxGrammar.StringAroundQuote.Value);
            Position += NextToken.String.Length;

            TextParser.ReadCharacter(SyntaxGrammar.StringAroundQuote.Value);
            Position++;
          }
          else if (TokenCharacter == SyntaxGrammar.StringAroundQuote)
          {
            TextParser.ReadCharacter(SyntaxGrammar.StringAroundQuote.Value);
            Position++;

            var StringBuilder = new StringBuilder();

            do
            {
              if (TextParser.NextCharacter() == SyntaxGrammar.StringAroundQuote)
              {
                TextParser.ReadCharacter(SyntaxGrammar.StringAroundQuote.Value);
                Position++;

                if (SyntaxGrammar.StringEscape == null && TextParser.NextCharacter() == SyntaxGrammar.StringAroundQuote)
                {
                  TextParser.ReadCharacter(SyntaxGrammar.StringAroundQuote.Value);
                  Position++;

                  StringBuilder.Append(SyntaxGrammar.StringAroundQuote.Value);
                }
                else
                {
                  break;
                }
              }
              else if (TextParser.More())
              {
                var StringEntry = TextParser.ReadCharacter();
                Position++;

                switch (StringEntry)
                {
                  case '\r':
                  case '\n':
                    if (!SyntaxGrammar.AllowMultiLineString)
                      throw new Exception($"String {SyntaxGrammar.StringAroundQuote}{StringBuilder}{SyntaxGrammar.StringAroundQuote} was not correctly terminated.");

                    if (StringEntry == '\n')
                      LineCount++;

                    StringBuilder.Append(StringEntry);
                    break;

                  default:
                    if (StringEntry == SyntaxGrammar.StringEscape)
                    {
                      StringEntry = TextParser.ReadCharacter();
                      Position++;

                      if (StringEntry == SyntaxGrammar.StringEscape)
                      {
                        StringBuilder.Append(StringEntry);
                      }
                      else
                      {
                        switch (StringEntry)
                        {
                          case 'r':
                            StringBuilder.Append('\r');
                            break;

                          case 'n':
                            StringBuilder.Append('\n');
                            break;

                          case 't':
                            StringBuilder.Append('\t');
                            break;

                          default:
                            if (StringEntry == SyntaxGrammar.StringAroundQuote)
                              StringBuilder.Append(StringEntry);
                            else
                              StringBuilder.Append("\\" + StringEntry);
                            break;
                        }
                      }
                    }
                    else
                    {
                      StringBuilder.Append(StringEntry);
                    }
                    break;
                }
              }
            }
            while (TextParser.More());

            NextToken.String = StringBuilder.ToString();
          }
          else if (TokenCharacter == SyntaxGrammar.IdentifierOpenQuote && SyntaxGrammar.IdentifierOpenQuote != '\0')
          {
            TextParser.ReadCharacter(SyntaxGrammar.IdentifierOpenQuote);
            Position++;

            var IdentifierString = TextParser.ReadUntilCharacter(SyntaxGrammar.IdentifierCloseQuote);
            Position += IdentifierString.Length;

            TextParser.ReadCharacter(SyntaxGrammar.IdentifierCloseQuote);
            Position++;

            if (SyntaxGrammar.DateTimePrefix != null && IdentifierString.StartsWith(SyntaxGrammar.DateTimePrefix))
            {
              var ParseString = IdentifierString.Substring(SyntaxGrammar.DateTimePrefix.Length);

              if (DateTimeOffset.TryParseExact(ParseString, TextFormat.DateTimeOffsetFormatArray, CultureInfo.InvariantCulture, DateTimeStyles.None, out var ResultTimestamp))
                NextToken.Timestamp = ResultTimestamp;
              else if (DateTime.TryParseExact(ParseString, TextFormat.DateTimeFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out var ResultDateTime))
                NextToken.DateTime = ResultDateTime;
              else
                throw new Exception("Unable to parse date time format: " + ParseString);
            }
            else if (SyntaxGrammar.DatePrefix != null && IdentifierString.StartsWith(SyntaxGrammar.DatePrefix))
              NextToken.Date = Inv.Date.ParseExact(IdentifierString.Substring(SyntaxGrammar.DatePrefix.Length), TextFormat.DateFormat);
            else if (SyntaxGrammar.TimePeriodPrefix != null && IdentifierString.StartsWith(SyntaxGrammar.TimePeriodPrefix))
              NextToken.TimePeriod = Inv.TimePeriod.Parse(IdentifierString.Substring(SyntaxGrammar.TimePeriodPrefix.Length));
            else if (SyntaxGrammar.TimePrefix != null && IdentifierString.StartsWith(SyntaxGrammar.TimePrefix))
              NextToken.Time = Inv.Time.ParseExact(IdentifierString.Substring(SyntaxGrammar.TimePrefix.Length), TextFormat.TimeFormatArray);
            else if (SyntaxGrammar.TimeSpanPrefix != null && IdentifierString.StartsWith(SyntaxGrammar.TimeSpanPrefix))
              NextToken.TimeSpan = TimeSpan.ParseExact(IdentifierString.Substring(SyntaxGrammar.TimeSpanPrefix.Length), TextFormat.TimeSpanFormatArray, CultureInfo.InvariantCulture);
            else
              NextToken.Identifier = IdentifierString;
          }
          else
          {
            TextParser.ReadCharacter(TokenCharacter);
            Position++;

            if (Inv.Syntax.Foundation.TranslateTokenType(TokenCharacter, out var TokenType))
            {
              NextToken.Type = TokenType;
            }
            else
            {
              NextToken.Type = null;

              throw NewInterrupt($"Character '{TokenCharacter}' (0x{((int)TokenCharacter).ToString()}) is not a valid token in the syntax.");
            }
          }
        }
      }
    }
    private ReadException NewInterrupt(string Message)
    {
      if (FilePath != null)
        return new ReadException($"{Path.GetFileName(FilePath)} Line {LineCount}: {Message}", LineCount);
      else
        return new ReadException($"Line {LineCount}: {Message}", LineCount);
    }

    private readonly string FilePath;
    private readonly TextReader ActiveReader;
    private readonly Inv.Syntax.Grammar SyntaxGrammar;
    private GrammarManager GrammarManager;
    private Inv.TextParser TextParser;
    private int LineCount;
    private int Position;
    private Inv.Syntax.Token NextToken;
  }

  internal static class BinaryFormat
  {
    static BinaryFormat()
    {
      TokenTagDictionary = new Dictionary<Inv.Syntax.TokenType, byte>()
      {
        { Inv.Syntax.TokenType.Whitespace, WhitespaceTag },
        { Inv.Syntax.TokenType.Newline, NewlineTag },
        { Inv.Syntax.TokenType.Keyword, KeywordStartTag },
        { Inv.Syntax.TokenType.Identifier, IdentifierStartTag },
        { Inv.Syntax.TokenType.String, StringTag },
        { Inv.Syntax.TokenType.Number, NumberTag },
        { Inv.Syntax.TokenType.Timestamp, TimestampTag },
        { Inv.Syntax.TokenType.Date, DateTag },
        { Inv.Syntax.TokenType.Time, TimeTag },
        { Inv.Syntax.TokenType.TimePeriod, TimePeriodTag },
        { Inv.Syntax.TokenType.TimeSpan, TimeSpanTag },
        { Inv.Syntax.TokenType.Comment, CommentTag },
        { Inv.Syntax.TokenType.Base64, Base64Tag },
        { Inv.Syntax.TokenType.Hexadecimal, HexadecimalTag },
        { Inv.Syntax.TokenType.OpenRound, 20 },
        { Inv.Syntax.TokenType.CloseRound, 21 },
        { Inv.Syntax.TokenType.OpenAngle, 22 },
        { Inv.Syntax.TokenType.CloseAngle, 23 },
        { Inv.Syntax.TokenType.OpenBrace, 24 },
        { Inv.Syntax.TokenType.CloseBrace, 25 },
        { Inv.Syntax.TokenType.Comma, 26 },
        { Inv.Syntax.TokenType.Period, 27 },
        { Inv.Syntax.TokenType.Semicolon, 28 },
        { Inv.Syntax.TokenType.Colon, 29 },
        { Inv.Syntax.TokenType.EqualSign, 30 },
        { Inv.Syntax.TokenType.ExclamationPoint, 31 },
        { Inv.Syntax.TokenType.QuestionMark, 32 },
        { Inv.Syntax.TokenType.Ampersand, 33 },
        { Inv.Syntax.TokenType.Pipe, 34 },
        { Inv.Syntax.TokenType.Plus, 35 },
        { Inv.Syntax.TokenType.Minus, 36 },
        { Inv.Syntax.TokenType.Percent, 37 },
        { Inv.Syntax.TokenType.Asterisk, 38 },
        { Inv.Syntax.TokenType.BackSlash, 39 },
        { Inv.Syntax.TokenType.ForwardSlash, 40 },
        { Inv.Syntax.TokenType.At, 41 },
        { Inv.Syntax.TokenType.Caret, 42 },
        { Inv.Syntax.TokenType.Tilde, 43 },
        { Inv.Syntax.TokenType.DollarSign, 44 },
        { Inv.Syntax.TokenType.Hash, 45 },
        { Inv.Syntax.TokenType.SingleQuote, 46 },
        { Inv.Syntax.TokenType.DoubleQuote, 48 },
        { Inv.Syntax.TokenType.BackQuote, 49 },
        { Inv.Syntax.TokenType.OpenSquare, 50 },
        { Inv.Syntax.TokenType.CloseSquare, 51 },
        { Inv.Syntax.TokenType.Underscore, 53 },
        { Inv.Syntax.TokenType.TimestampLegacy, TimestampLegacyTag },
        { Inv.Syntax.TokenType.DateTime, DateTimeTag },
      };

      if (Inv.Assert.IsEnabled)
      {
        foreach (var TokenTypeValue in EnumHelper.GetEnumerable<Inv.Syntax.TokenType>())
          Inv.Assert.Check(TokenTagDictionary.ContainsKey(TokenTypeValue), "Inv.Syntax.TokenType not handled: {0}", TokenTypeValue);
      }

      // NOTE: this will fail if there are any duplicate tags.
      TagTokenDictionary = new Dictionary<byte, Inv.Syntax.TokenType>();
      foreach (var TokenTagEntry in TokenTagDictionary)
        TagTokenDictionary.Add(TokenTagEntry.Value, TokenTagEntry.Key);
    }

    internal const byte StartTag = 0x00;
    internal const byte WhitespaceTag = 1;
    internal const byte NewlineTag = 2;
    internal const byte KeywordStartTag = 3;
    internal const byte KeywordRepeatTag = 4;
    internal const byte IdentifierStartTag = 5;
    internal const byte IdentifierRepeatTag = 6;
    internal const byte StringTag = 7;
    internal const byte NumberTag = 8;
    internal const byte TimestampTag = 9;
    internal const byte DateTag = 10;
    internal const byte TimeTag = 11;
    internal const byte TimePeriodTag = 52;
    internal const byte TimeSpanTag = 12;
    internal const byte CommentTag = 13;
    internal const byte HexadecimalTag = 14;
    internal const byte Base64Tag = 15;
    internal const byte TimestampLegacyTag = 54;
    internal const byte DateTimeTag = 55;
    internal const byte StopTag = 0xFF;

    internal static byte ResolveTag(Inv.Syntax.TokenType TokenType)
    {
      return TokenTagDictionary[TokenType];
    }
    internal static Inv.Syntax.TokenType ResolveToken(byte TokenTag)
    {
      return TagTokenDictionary[TokenTag];
    }

    private static readonly Dictionary<Inv.Syntax.TokenType, byte> TokenTagDictionary;
    private static readonly Dictionary<byte, Inv.Syntax.TokenType> TagTokenDictionary;
  }

  public sealed class BinaryWriteService : WriteContract
  {
    public BinaryWriteService(System.IO.BinaryWriter ActiveWriter)
    {
      this.ActiveWriter = ActiveWriter;
      this.KeywordDictionary = new Dictionary<string, int>();
      this.IdentifierDictionary = new Dictionary<string, int>();
    }

    void WriteContract.Start()
    {
      KeywordDictionary.Clear();
      IdentifierDictionary.Clear();

      ActiveWriter.Write(BinaryFormat.StartTag);
    }
    void WriteContract.Stop()
    {
      ActiveWriter.Write(BinaryFormat.StopTag);

      KeywordDictionary.Clear();
      IdentifierDictionary.Clear();
    }
    void WriteContract.WriteToken(Inv.Syntax.Token Token)
    {
      switch (Token.Type)
      {
        case Inv.Syntax.TokenType.Whitespace:
          //ActiveWriter.Write(BinaryTag.Whitespace);
          //ActiveWriter.Write(Token.Whitespace);
          break;

        case Inv.Syntax.TokenType.Newline:
          //ActiveWriter.Write(BinaryFormat.NewlineTag);
          //ActiveWriter.Write(Token.Newline);
          break;

        case Inv.Syntax.TokenType.Comment:
          //ActiveWriter.Write(BinaryFormat.CommentTag);
          //ActiveWriter.Write(Token.Comment);
          break;

        case Inv.Syntax.TokenType.Identifier:
          int IdentifierID;
          var TokenIdentifier = Token.Identifier;

          if (IdentifierDictionary.TryGetValue(TokenIdentifier, out IdentifierID))
          {
            ActiveWriter.Write(BinaryFormat.IdentifierRepeatTag);
            ActiveWriter.Write(IdentifierID);
          }
          else
          {
            ActiveWriter.Write(BinaryFormat.IdentifierStartTag);
            ActiveWriter.Write(TokenIdentifier);

            IdentifierDictionary.Add(TokenIdentifier, IdentifierDictionary.Count);
          }
          break;

        case Inv.Syntax.TokenType.Keyword:
          int KeywordID;
          var TokenKeyword = Token.Keyword;

          if (KeywordDictionary.TryGetValue(TokenKeyword, out KeywordID))
          {
            ActiveWriter.Write(BinaryFormat.KeywordRepeatTag);
            ActiveWriter.Write(KeywordID);
          }
          else
          {
            ActiveWriter.Write(BinaryFormat.KeywordStartTag);
            ActiveWriter.Write(TokenKeyword);

            KeywordDictionary.Add(TokenKeyword, KeywordDictionary.Count);
          }
          break;

        case Inv.Syntax.TokenType.String:
          ActiveWriter.Write(BinaryFormat.StringTag);
          ActiveWriter.Write(Token.String);
          break;

        case Inv.Syntax.TokenType.Number:
          ActiveWriter.Write(BinaryFormat.NumberTag);
          ActiveWriter.Write(Token.Number);
          break;

        case Inv.Syntax.TokenType.DateTime:
          ActiveWriter.Write(BinaryFormat.DateTimeTag);
          ActiveWriter.Write(Token.DateTime.Ticks);
          break;

        case Inv.Syntax.TokenType.Timestamp:
          ActiveWriter.Write(BinaryFormat.TimestampTag);
          ActiveWriter.Write(Token.Timestamp.DateTime.Ticks);
          ActiveWriter.Write((short)Token.Timestamp.Offset.TotalMinutes);
          break;

        case Inv.Syntax.TokenType.TimestampLegacy:
          ActiveWriter.Write(BinaryFormat.TimestampLegacyTag);
          ActiveWriter.Write(Token.TimestampLegacy.DateTime.Ticks);
          ActiveWriter.Write((short)Token.TimestampLegacy.Offset.TotalMinutes);
          break;

        case Inv.Syntax.TokenType.Date:
          ActiveWriter.Write(BinaryFormat.DateTag);
          ActiveWriter.Write(Token.Date.ToDateTime().Ticks);
          break;

        case Inv.Syntax.TokenType.Time:
          ActiveWriter.Write(BinaryFormat.TimeTag);
          ActiveWriter.Write(Token.Time.ToDateTime().Ticks);
          break;

        case Inv.Syntax.TokenType.TimePeriod:
          ActiveWriter.Write(BinaryFormat.TimePeriodTag);
          ActiveWriter.Write(Token.TimePeriod.ToString());
          break;

        case Inv.Syntax.TokenType.TimeSpan:
          ActiveWriter.Write(BinaryFormat.TimeSpanTag);
          ActiveWriter.Write(Token.TimeSpan.Ticks);
          break;

        case Inv.Syntax.TokenType.Hexadecimal:
          var HexadecimalBuffer = Token.Hexadecimal.GetBuffer();

          ActiveWriter.Write(BinaryFormat.HexadecimalTag);
          ActiveWriter.Write(HexadecimalBuffer.Length);
          ActiveWriter.Write(HexadecimalBuffer);
          break;

        case Inv.Syntax.TokenType.Base64:
          var Base64Buffer = Token.Base64.GetBuffer();

          ActiveWriter.Write(BinaryFormat.Base64Tag);
          ActiveWriter.Write(Base64Buffer.Length);
          ActiveWriter.Write(Base64Buffer);
          break;

        default:
          ActiveWriter.Write(BinaryFormat.ResolveTag(Token.Type.Value));
          break;
      }
    }
    string WriteContract.Publish()
    {
      return $"Byte {ActiveWriter.BaseStream.Position} of {ActiveWriter.BaseStream.Length}";
    }

    private readonly System.IO.BinaryWriter ActiveWriter;
    private readonly Dictionary<string, int> KeywordDictionary;
    private readonly Dictionary<string, int> IdentifierDictionary;
  }

  public sealed class BinaryReadService : ReadContract
  {
    public BinaryReadService(System.IO.BinaryReader ActiveReader)
    {
      this.ActiveReader = ActiveReader;
      this.ActiveToken = new Inv.Syntax.Token();
      this.KeywordDictionary = new Dictionary<int, string>();
      this.IdentifierDictionary = new Dictionary<int, string>();
    }

    void ReadContract.Start()
    {
      KeywordDictionary.Clear();
      IdentifierDictionary.Clear();

      if (ActiveReader.ReadByte() != BinaryFormat.StartTag)
        throw new Exception("Expected a start tag.");

      NextToken = ActiveToken;
      AdvanceToken();
    }
    void ReadContract.Stop()
    {
      if (NextToken != null)
      {
        if (ActiveReader.ReadByte() != BinaryFormat.StopTag)
          throw new Exception("Expected a stop tag.");

        NextToken = null;
      }

      KeywordDictionary.Clear();
      IdentifierDictionary.Clear();
    }
    bool ReadContract.More()
    {
      return NextToken != null;
    }
    Inv.Syntax.Token ReadContract.PeekToken()
    {
      return NextToken;
    }
    void ReadContract.SkipToken()
    {
      AdvanceToken();
    }
    int ReadContract.GetPosition()
    {
      return (int)ActiveReader.BaseStream.Position;
    }
    ReadException ReadContract.Interrupt(string Message)
    {
      var Position = ActiveReader.BaseStream.Position;

      return new ReadException($"Byte {Position}: {Message}", Position);
    }

    private void AdvanceToken()
    {
      var NextTag = ActiveReader.ReadByte();

      switch (NextTag)
      {
        case BinaryFormat.StartTag:
          throw new Exception("Unexpected start tag.");

        case BinaryFormat.StopTag:
          NextToken = null;
          break;

        default:
          if (Inv.Assert.IsEnabled)
            Inv.Assert.Check(NextToken == ActiveToken, "Stream must not be finished.");

          NextToken.Type = null;

          switch (NextTag)
          {
            case BinaryFormat.WhitespaceTag:
              NextToken.Whitespace = ActiveReader.ReadString();
              break;

            case BinaryFormat.KeywordStartTag:
              var TokenKeyword = ActiveReader.ReadString();

              NextToken.Keyword = TokenKeyword;
              KeywordDictionary.Add(KeywordDictionary.Count, TokenKeyword);
              break;

            case BinaryFormat.KeywordRepeatTag:
              NextToken.Keyword = KeywordDictionary[ActiveReader.ReadInt32()];
              break;

            case BinaryFormat.IdentifierStartTag:
              var TokenIdentifier = ActiveReader.ReadString();

              NextToken.Identifier = TokenIdentifier;
              IdentifierDictionary.Add(IdentifierDictionary.Count, TokenIdentifier);
              break;

            case BinaryFormat.IdentifierRepeatTag:
              NextToken.Identifier = IdentifierDictionary[ActiveReader.ReadInt32()];
              break;

            case BinaryFormat.StringTag:
              NextToken.String = ActiveReader.ReadString();
              break;

            case BinaryFormat.NumberTag:
              NextToken.Number = ActiveReader.ReadString();
              break;

            case BinaryFormat.DateTimeTag:
              var DateTimeTicks = ActiveReader.ReadInt64();

              NextToken.DateTime = new DateTime(DateTimeTicks);
              break;

            case BinaryFormat.TimestampLegacyTag:
              var TimestampLegacyTicks = ActiveReader.ReadInt64();
              var TimestampLegacyOffsetTicks = ActiveReader.ReadInt16();

              NextToken.TimestampLegacy = new DateTimeOffset(TimestampLegacyTicks, TimeSpan.FromMinutes(TimestampLegacyOffsetTicks));
              break;

            case BinaryFormat.TimestampTag:
              var TimestampTicks = ActiveReader.ReadInt64();
              var TimestampOffsetTicks = ActiveReader.ReadInt16();

              NextToken.Timestamp = new DateTimeOffset(TimestampTicks, TimeSpan.FromMinutes(TimestampOffsetTicks));
              break;

            case BinaryFormat.DateTag:
              var DateTicks = ActiveReader.ReadInt64();

              NextToken.Date = new DateTime(DateTicks).AsDate();
              break;

            case BinaryFormat.TimeTag:
              var TimeTicks = ActiveReader.ReadInt64();

              NextToken.Time = new Inv.Time(new DateTime(TimeTicks));
              break;

            case BinaryFormat.TimePeriodTag:
              var TimePeriodText = ActiveReader.ReadString();

              NextToken.TimePeriod = Inv.TimePeriod.Parse(TimePeriodText);
              break;

            case BinaryFormat.TimeSpanTag:
              NextToken.TimeSpan = new TimeSpan(ActiveReader.ReadInt64());
              break;

            case BinaryFormat.HexadecimalTag:
              var HexadecimalLength = ActiveReader.ReadInt32();
              NextToken.Hexadecimal = new Inv.Binary(ActiveReader.ReadBytes(HexadecimalLength), "");
              break;

            case BinaryFormat.Base64Tag:
              var Base64Length = ActiveReader.ReadInt32();
              NextToken.Base64 = new Inv.Binary(ActiveReader.ReadBytes(Base64Length), "");
              break;

            default:
              NextToken.Type = BinaryFormat.ResolveToken(NextTag);
              break;
          }
          break;
      }
    }

    private readonly System.IO.BinaryReader ActiveReader;
    private readonly Dictionary<int, string> KeywordDictionary;
    private readonly Dictionary<int, string> IdentifierDictionary;
    private readonly Inv.Syntax.Token ActiveToken;
    private Inv.Syntax.Token NextToken;
  }

  public sealed class Grammar
  {
    public Grammar()
    {
      this.Keyword = new Inv.DistinctList<string>();
    }

    public Inv.DistinctList<string> Keyword { get; private set; }
    public bool IgnoreWhitespace { get; set; }
    public bool IgnoreComment { get; set; }
    public bool KeywordCaseSensitive { get; set; }
    public bool AllowMultiLineString { get; set; }
    public bool UseHexadecimalPrefix { get; set; }
    public bool UseBase64Prefix { get; set; }
    public string SingleLineCommentPrefix { get; set; }
    public string MultiLineCommentStart { get; set; }
    public string MultiLineCommentFinish { get; set; }
    public char? StringAroundQuote { get; set; }
    public char? StringEscape { get; set; }
    public char? StringVerbatim { get; set; }
    public char IdentifierOpenQuote { get; set; }
    public char IdentifierCloseQuote { get; set; }
    public bool AlwaysQuoteIdentifier { get; set; }
    public string DateTimePrefix { get; set; }
    public string TimeSpanPrefix { get; set; }
    public string TimePeriodPrefix { get; set; }
    public string DatePrefix { get; set; }
    public string TimePrefix { get; set; }
  }

  public enum TokenType
  {
    Newline,
    Whitespace,
    Comment,
    Identifier,
    Keyword,
    Number,
    String,
    Date,
    Timestamp,
    TimestampLegacy,
    Time,
    TimePeriod,
    TimeSpan,
    Hexadecimal,
    Base64,
    Colon,
    Semicolon,
    Comma,
    Period,
    EqualSign,
    Plus,
    Minus,
    Asterisk,
    DollarSign,
    Caret,
    Tilde,
    ExclamationPoint,
    QuestionMark,
    Pipe,
    Ampersand,
    Percent,
    ForwardSlash,
    BackSlash,
    At,
    OpenRound,
    CloseRound,
    OpenBrace,
    CloseBrace,
    OpenAngle,
    CloseAngle,
    Hash,
    SingleQuote,
    DoubleQuote,
    BackQuote,
    OpenSquare,
    CloseSquare,
    Underscore,
    DateTime
  }

  public sealed class Token
  {
    public Token()
    {
      this.Backing_Type = new Inv.Choice<object, Inv.Syntax.TokenType>(Sharing_TypeClassList);
    }

    public Inv.Syntax.TokenType? Type
    {
      get { return Backing_Type.Index; }
      set { Backing_Type.Index = value; }
    }
    public long Newline
    {
      get { return Backing_Type.Retrieve<long>(Inv.Syntax.TokenType.Newline); }
      set { Backing_Type.Store<long>(Inv.Syntax.TokenType.Newline, value); }
    }
    public string Whitespace
    {
      get { return Backing_Type.Retrieve<string>(Inv.Syntax.TokenType.Whitespace); }
      set { Backing_Type.Store<string>(Inv.Syntax.TokenType.Whitespace, value); }
    }
    public string Comment
    {
      get { return Backing_Type.Retrieve<string>(Inv.Syntax.TokenType.Comment); }
      set { Backing_Type.Store<string>(Inv.Syntax.TokenType.Comment, value); }
    }
    public string Identifier
    {
      get { return Backing_Type.Retrieve<string>(Inv.Syntax.TokenType.Identifier); }
      set { Backing_Type.Store<string>(Inv.Syntax.TokenType.Identifier, value); }
    }
    public string Keyword
    {
      get { return Backing_Type.Retrieve<string>(Inv.Syntax.TokenType.Keyword); }
      set { Backing_Type.Store<string>(Inv.Syntax.TokenType.Keyword, value); }
    }
    public string Number
    {
      get { return Backing_Type.Retrieve<string>(Inv.Syntax.TokenType.Number); }
      set { Backing_Type.Store<string>(Inv.Syntax.TokenType.Number, value); }
    }
    public string String
    {
      get { return Backing_Type.Retrieve<string>(Inv.Syntax.TokenType.String); }
      set { Backing_Type.Store<string>(Inv.Syntax.TokenType.String, value); }
    }
    public Inv.Date Date
    {
      get { return Backing_Type.Retrieve<Inv.Date>(Inv.Syntax.TokenType.Date); }
      set { Backing_Type.Store<Inv.Date>(Inv.Syntax.TokenType.Date, value); }
    }
    public DateTime DateTime
    {
      get { return Backing_Type.Retrieve<DateTime>(Inv.Syntax.TokenType.DateTime); }
      set { Backing_Type.Store<DateTime>(Inv.Syntax.TokenType.DateTime, value); }
    }
    public DateTimeOffset Timestamp
    {
      get { return Backing_Type.Retrieve<DateTimeOffset>(Inv.Syntax.TokenType.Timestamp); }
      set { Backing_Type.Store<DateTimeOffset>(Inv.Syntax.TokenType.Timestamp, value); }
    }
    public DateTimeOffset TimestampLegacy
    {
      get { return Backing_Type.Retrieve<DateTimeOffset>(Inv.Syntax.TokenType.TimestampLegacy); }
      set { Backing_Type.Store<DateTimeOffset>(Inv.Syntax.TokenType.TimestampLegacy, value); }
    }
    public Inv.Time Time
    {
      get { return Backing_Type.Retrieve<Inv.Time>(Inv.Syntax.TokenType.Time); }
      set { Backing_Type.Store<Inv.Time>(Inv.Syntax.TokenType.Time, value); }
    }
    public Inv.TimePeriod TimePeriod
    {
      get { return Backing_Type.Retrieve<Inv.TimePeriod>(Inv.Syntax.TokenType.TimePeriod); }
      set { Backing_Type.Store<Inv.TimePeriod>(Inv.Syntax.TokenType.TimePeriod, value); }
    }
    public TimeSpan TimeSpan
    {
      get { return Backing_Type.Retrieve<TimeSpan>(Inv.Syntax.TokenType.TimeSpan); }
      set { Backing_Type.Store<TimeSpan>(Inv.Syntax.TokenType.TimeSpan, value); }
    }
    public Inv.Binary Hexadecimal
    {
      get { return Backing_Type.Retrieve<Inv.Binary>(Inv.Syntax.TokenType.Hexadecimal); }
      set { Backing_Type.Store<Inv.Binary>(Inv.Syntax.TokenType.Hexadecimal, value); }
    }
    public Inv.Binary Base64
    {
      get { return Backing_Type.Retrieve<Inv.Binary>(Inv.Syntax.TokenType.Base64); }
      set { Backing_Type.Store<Inv.Binary>(Inv.Syntax.TokenType.Base64, value); }
    }

    public override string ToString()
    {
      var Result = Type.ToString();

      switch (Type)
      {
        case TokenType.Identifier:
          return Result + ":" + Identifier;

        case TokenType.Keyword:
          return Result + ":" + Keyword;

        case TokenType.String:
          return Result + ":" + String;

        case TokenType.Number:
          return Result + ":" + Number;

        case TokenType.Comment:
          return Result + ":" + Comment;

        case TokenType.Date:
          return Result + ":" + Date;

        case TokenType.Time:
          return Result + ":" + Time;

        case TokenType.DateTime:
          return Result + ":" + DateTime;

        case TokenType.Timestamp:
          return Result + ":" + Timestamp;

        case TokenType.TimestampLegacy:
          return Result + ":" + TimestampLegacy;

        case TokenType.TimePeriod:
          return Result + ":" + TimePeriod;

        case TokenType.TimeSpan:
          return Result + ":" + TimeSpan;

        case TokenType.Hexadecimal:
          return Result + ":" + Hexadecimal;

        case TokenType.Base64:
          return Result + ":" + Base64;

        case TokenType.Whitespace:
          return Result + ":" + Whitespace;

        case TokenType.Newline:
          return Result + ":" + Newline;

        default:
          return Result;
      }
    }

    private Inv.Choice<object, Inv.Syntax.TokenType> Backing_Type;

    private static readonly Type[] Sharing_TypeClassList = new Type[] { typeof(long), typeof(string), typeof(string), typeof(string), typeof(string), typeof(string), typeof(string), typeof(Inv.Date), typeof(System.DateTimeOffset), typeof(System.DateTimeOffset), typeof(Inv.Time), typeof(Inv.TimePeriod), typeof(System.TimeSpan), typeof(Inv.Binary), typeof(Inv.Binary), null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, typeof(System.DateTime) };
  }

  public sealed class Document
  {
    public Document(Grammar Grammar)
    {
      this.Grammar = Grammar;
      this.TokenList = new Inv.DistinctList<Token>();
    }

    public Grammar Grammar { get; private set; }
    public Inv.DistinctList<Token> TokenList { get; private set; }
  }

  public sealed class GrammarManager
  {
    public GrammarManager(Inv.Syntax.Grammar SyntaxGrammar)
    {
      if (SyntaxGrammar.KeywordCaseSensitive)
        KeywordHashSet = new HashSet<string>(StringComparer.CurrentCulture);
      else
        KeywordHashSet = new HashSet<string>(StringComparer.CurrentCultureIgnoreCase);

      foreach (var Keyword in SyntaxGrammar.Keyword)
      {
        if (!KeywordHashSet.Add(Keyword))
          throw new System.Exception("Duplicate keyword: " + Keyword);
      }
    }

    public bool ContainsKeyword(string Keyword)
    {
      return KeywordHashSet.Contains(Keyword);
    }

    private readonly HashSet<string> KeywordHashSet;
  }

  public sealed class Writer
  {
    internal Writer(Inv.Syntax.WriteContract WriteContract)
    {
      this.WriteContract = WriteContract;
      this.CacheToken = new Inv.Syntax.Token();
    }

    internal Inv.Syntax.WriteContract WriteContract { get; private set; }

    internal void Start()
    {
      WriteContract.Start();

      IndentLevel = 0;
    }
    internal void Stop()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(IndentLevel == 0, "IndentLevel must return to zero on finish.");

      WriteContract.Stop();
    }

    public void IncreaseIndent()
    {
      IndentLevel++;
    }
    public void DecreaseIndent()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(IndentLevel > 0, "IndentLevel must be greater than zero.");

      IndentLevel--;
    }
    public void WriteToken(Inv.Syntax.TokenType SyntaxTokenType)
    {
      CacheToken.Type = SyntaxTokenType;

      WriteContract.WriteToken(CacheToken);
    }
    public void WriteIdentifierChainArray(string[] IdentifierArray, Inv.Syntax.TokenType SeparatorTokenType)
    {
      var IdentifierCount = IdentifierArray.Length;

      for (var IdentifierIndex = 0; IdentifierIndex < IdentifierCount; IdentifierIndex++)
      {
        var Identifier = IdentifierArray[IdentifierIndex];

        if (Identifier != "")
          WriteIdentifier(Identifier);

        if (IdentifierIndex != IdentifierCount - 1)
          WriteToken(SeparatorTokenType);
      }
    }
    public void WriteIdentifier(string IdentifierValue)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(IdentifierValue, nameof(IdentifierValue));

      CacheToken.Identifier = IdentifierValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteKeyword(string KeywordValue)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(KeywordValue, nameof(KeywordValue));

      CacheToken.Keyword = KeywordValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteString(string StringValue)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(StringValue, nameof(StringValue));

      CacheToken.String = StringValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteCharacter(char CharacterValue)
    {
      WriteString(CharacterValue.ToString());
    }
    public void WriteGuid(Guid GuidValue)
    {
      WriteString(GuidValue.ToString());
    }
    public void WriteInteger(long IntegerValue)
    {
      WriteNumber(IntegerValue.ToString());
    }
    public void WriteDecimal(decimal DecimalValue)
    {
      WriteNumber(DecimalValue.ToString());
    }
    public void WriteReal(double RealValue)
    {
      WriteNumber(RealValue.ToString(System.Globalization.CultureInfo.InvariantCulture));
    }
    public void WriteBoolean(bool BooleanValue)
    {
      switch (BooleanValue)
      {
        case true:
          WriteIdentifier("true");
          break;

        case false:
          WriteIdentifier("false");
          break;

        default:
          throw new WriteException("Unhandled boolean value.");
      }
    }
    public void WriteDateTime(DateTime DateTimeValue)
    {
      CacheToken.DateTime = DateTimeValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteDateTimeOffset(DateTimeOffset TimestampValue)
    {
      CacheToken.Timestamp = TimestampValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteDateTimeOffsetLegacy(DateTimeOffset DateTimeValue)
    {
      CacheToken.TimestampLegacy = DateTimeValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteDate(Inv.Date DateValue)
    {
      CacheToken.Date = DateValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteTime(Inv.Time TimeValue)
    {
      CacheToken.Time = TimeValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteTimePeriod(Inv.TimePeriod TimePeriodValue)
    {
      CacheToken.TimePeriod = TimePeriodValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteTimeSpan(TimeSpan TimeSpanValue)
    {
      CacheToken.TimeSpan = TimeSpanValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteBase64(byte[] BinaryValue)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(BinaryValue, nameof(BinaryValue));

      CacheToken.Base64 = new Inv.Binary(BinaryValue, "");
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteHexadecimal(byte[] BinaryValue)
    {
      Inv.Assert.CheckNotNull(BinaryValue, nameof(BinaryValue));

      CacheToken.Hexadecimal = new Inv.Binary(BinaryValue, "");
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteColour(Inv.Colour Colour)
    {
      var KnownName = Colour.Name;

      if (KnownName == Colour.Hex)
        WriteHexadecimal(BitConverter.GetBytes(Colour.RawValue).Reverse().ToArray());
      else
        WriteIdentifier(KnownName);
    }
    public void WriteWhitespace(string WhitespaceValue)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(WhitespaceValue, nameof(WhitespaceValue));

      CacheToken.Whitespace = WhitespaceValue;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteOpenBrace()
    {
      WriteToken(Inv.Syntax.TokenType.OpenBrace);
    }
    public void WriteCloseBrace()
    {
      WriteToken(Inv.Syntax.TokenType.CloseBrace);
    }
    public void WriteOpenSquare()
    {
      WriteToken(Inv.Syntax.TokenType.OpenSquare);
    }
    public void WriteCloseSquare()
    {
      WriteToken(Inv.Syntax.TokenType.CloseSquare);
    }
    public void WriteOpenRound()
    {
      WriteToken(Inv.Syntax.TokenType.OpenRound);
    }
    public void WriteCloseRound()
    {
      WriteToken(Inv.Syntax.TokenType.CloseRound);
    }
    public void WritePercent()
    {
      WriteToken(Inv.Syntax.TokenType.Percent);
    }
    public void WriteOpenAngle()
    {
      WriteToken(Inv.Syntax.TokenType.OpenAngle);
    }
    public void WriteCloseAngle()
    {
      WriteToken(Inv.Syntax.TokenType.CloseAngle);
    }
    public void WriteSemicolon()
    {
      WriteToken(Inv.Syntax.TokenType.Semicolon);
    }
    public void WriteColon()
    {
      WriteToken(Inv.Syntax.TokenType.Colon);
    }
    public void WriteComma()
    {
      WriteToken(Inv.Syntax.TokenType.Comma);
    }
    public void WritePeriod()
    {
      WriteToken(Inv.Syntax.TokenType.Period);
    }
    public void WriteEqualSign()
    {
      WriteToken(Inv.Syntax.TokenType.EqualSign);
    }
    public void WriteExclamationPoint()
    {
      WriteToken(Inv.Syntax.TokenType.ExclamationPoint);
    }
    public void WriteQuestionMark()
    {
      WriteToken(Inv.Syntax.TokenType.QuestionMark);
    }
    public void WriteAt()
    {
      WriteToken(Inv.Syntax.TokenType.At);
    }
    public void WritePlus()
    {
      WriteToken(Inv.Syntax.TokenType.Plus);
    }
    public void WriteMinus()
    {
      WriteToken(Inv.Syntax.TokenType.Minus);
    }
    public void WriteHash()
    {
      WriteToken(Inv.Syntax.TokenType.Hash);
    }
    public void WriteAsterisk()
    {
      WriteToken(Inv.Syntax.TokenType.Asterisk);
    }
    public void WriteAmpersand()
    {
      WriteToken(Inv.Syntax.TokenType.Ampersand);
    }
    public void WritePipe()
    {
      WriteToken(Inv.Syntax.TokenType.Pipe);
    }
    public void WriteUnderscore()
    {
      WriteToken(Inv.Syntax.TokenType.Underscore);
    }
    public void WriteForwardSlash()
    {
      WriteToken(Inv.Syntax.TokenType.ForwardSlash);
    }
    public void WriteBackSlash()
    {
      WriteToken(Inv.Syntax.TokenType.BackSlash);
    }
    public void WriteCaret()
    {
      WriteToken(Inv.Syntax.TokenType.Caret);
    }
    public void WriteTilde()
    {
      WriteToken(Inv.Syntax.TokenType.Tilde);
    }
    public void WriteDoubleQuote()
    {
      WriteToken(Inv.Syntax.TokenType.DoubleQuote);
    }
    public void WriteSpace()
    {
      WriteWhitespace(" ");
    }
    public void WriteLine()
    {
      CacheToken.Newline = 1;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteIndent()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(IndentLevel >= 0, "IndentLevel must be greater than zero.");

      WriteWhitespace(new string(' ', IndentLevel * 2));
    }
    public void WriteLineAndIndent()
    {
      WriteLine();
      WriteIndent();
    }
    public void WriteOpenScope()
    {
      WriteIndent();
      WriteOpenBrace();
      IncreaseIndent();
      WriteLine();
    }
    public void WriteCloseScope()
    {
      DecreaseIndent();
      WriteIndent();
      WriteCloseBrace();
      WriteLine();
    }
    public void WriteComment(string Value)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Value, nameof(Value));

      CacheToken.Comment = Value;
      WriteContract.WriteToken(CacheToken);
    }
    public void WriteOpenScopeString(string Keyword, string Identifier)
    {
      WriteIndent();
      WriteKeyword(Keyword);
      WriteSpace();
      WriteString(Identifier);
      WriteLine();
      WriteOpenScope();
    }
    public void WriteOpenScopeIdentifier(string Keyword, string Identifier)
    {
      WriteIndent();
      WriteKeyword(Keyword);
      WriteSpace();
      WriteIdentifier(Identifier);
      WriteLine();
      WriteOpenScope();
    }
    public void WriteOpenScopeKeyword(string Keyword)
    {
      WriteIndent();
      WriteKeyword(Keyword);
      WriteLine();
      WriteOpenScope();
    }
    public void WriteOpenCollectionKeyword(string Keyword)
    {
      WriteIndent();
      WriteKeyword(Keyword);
      WriteLineAndIndent();
      WriteOpenRound();
      IncreaseIndent();
      WriteLine();
    }
    public void WriteOpenCollectionIdentifier(string Keyword, string Identifier)
    {
      WriteIndent();
      WriteKeyword(Keyword);
      WriteSpace();
      WriteIdentifier(Identifier);
      WriteLineAndIndent();
      WriteOpenRound();
      IncreaseIndent();
      WriteLine();
    }
    public void WriteCloseCollection()
    {
      DecreaseIndent();
      WriteIndent();
      WriteCloseRound();
      WriteLine();
    }
    public void WriteOptionalStringAttribute(string Keyword, string StringValue)
    {
      if (StringValue != null)
      {
        WriteIndent();
        WriteKeyword(Keyword);
        WriteSpace();
        WriteEqualSign();
        WriteSpace();
        WriteString(StringValue);
        WriteSemicolon();
        WriteLine();
      }
    }
    public void WriteOptionalDateTimeAttribute(string Keyword, DateTimeOffset? DateTimeValue)
    {
      if (DateTimeValue.HasValue)
      {
        WriteIndent();
        WriteKeyword(Keyword);
        WriteSpace();
        WriteEqualSign();
        WriteSpace();
        WriteDateTimeOffset(DateTimeValue.Value);
        WriteSemicolon();
        WriteLine();
      }
    }
    public void WriteOptionalDecimalAttribute(string Keyword, decimal? DecimalValue)
    {
      if (DecimalValue.HasValue)
      {
        WriteIndent();
        WriteKeyword(Keyword);
        WriteSpace();
        WriteEqualSign();
        WriteSpace();
        WriteDecimal(DecimalValue.Value);
        WriteSemicolon();
        WriteLine();
      }
    }
    public void WriteOptionalMoneyAttribute(string Keyword, Inv.Money? MoneyValue)
    {
      if (MoneyValue != null)
        WriteOptionalDecimalAttribute(Keyword, MoneyValue.Value.GetAmount());
    }
    public void WriteOptionalIntegerAttribute(string Keyword, long? IntegerValue)
    {
      if (IntegerValue.HasValue)
      {
        WriteIndent();
        WriteKeyword(Keyword);
        WriteSpace();
        WriteEqualSign();
        WriteSpace();
        WriteInteger(IntegerValue.Value);
        WriteSemicolon();
        WriteLine();
      }
    }
    public void WriteOptionalBooleanAttribute(string Keyword, bool? BooleanValue)
    {
      if (BooleanValue.HasValue && BooleanValue.Value)
      {
        WriteIndent();
        WriteKeyword(Keyword);
        WriteSpace();
        WriteEqualSign();
        WriteSpace();
        WriteBoolean(BooleanValue.Value);
        WriteSemicolon();
        WriteLine();
      }
    }
    public void WriteOptionalIdentifierAttribute(string Keyword, string IdentifierValue)
    {
      if (IdentifierValue != null)
      {
        WriteIndent();
        WriteKeyword(Keyword);
        WriteSpace();
        WriteEqualSign();
        WriteSpace();
        WriteIdentifier(IdentifierValue);
        WriteSemicolon();
        WriteLine();
      }
    }
    public void WriteNumber(string NumberValue)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(NumberValue, nameof(NumberValue));

      CacheToken.Number = NumberValue;
      WriteContract.WriteToken(CacheToken);
    }

    private readonly Inv.Syntax.Token CacheToken;
    private int IndentLevel;
  }

  public sealed class Reader
  {
    internal Reader(Inv.Syntax.ReadContract ReadContract)
    {
      this.ReadContract = ReadContract;
    }

    internal Inv.Syntax.ReadContract ReadContract { get; private set; }

    internal void Start()
    {
      ReadContract.Start();
    }
    internal void Stop()
    {
      if (ReadContract.More())
        throw ReadContract.Interrupt("The syntax document was longer than is valid.");

      ReadContract.Stop();
    }

    public ReadException Interrupt(string Message)
    {
      return ReadContract.Interrupt(Message);
    }
    public bool More()
    {
      return ReadContract.More();
    }
    public TokenType? PeekTokenType()
    {
      var Token = ReadContract.PeekToken();

      if (Token == null)
        return null;
      else
        return Token.Type;
    }
    public Token PeekToken()
    {
      return ReadContract.PeekToken();
    }
    public bool NextIsTokenType(Inv.Syntax.TokenType TokenType)
    {
      var SyntaxToken = ReadContract.PeekToken();

      return SyntaxToken != null && SyntaxToken.Type == TokenType;
    }
    public bool NextIsTokenTypeArray(params Inv.Syntax.TokenType[] TokenTypeArray)
    {
      var SyntaxToken = ReadContract.PeekToken();

      return SyntaxToken != null && TokenTypeArray.Contains(SyntaxToken.Type.Value);
    }
    public void SkipToken()
    {
      ReadContract.SkipToken();
    }
    public int GetPosition()
    {
      return ReadContract.GetPosition();
    }
    public Inv.Syntax.TokenType ReadTokenTypeArray(params Inv.Syntax.TokenType[] TokenTypeArray)
    {
      var SyntaxToken = ReadContract.PeekToken();

      if (SyntaxToken == null)
        throw ReadContract.Interrupt($"expected token '{TokenTypeArray.Select(Index => Index.ToString()).AsSeparatedText(", ")}' but found the end of the document.");

      var SyntaxTokenType = SyntaxToken.Type.Value;

      if (!TokenTypeArray.Contains(SyntaxTokenType))
        throw ReadContract.Interrupt($"expected token '{TokenTypeArray.Select(Index => Index.ToString()).AsSeparatedText(", ")}' but found token '{SyntaxToken}'.");

      ReadContract.SkipToken();

      return SyntaxTokenType;
    }
    public Inv.Syntax.TokenType ReadTokenType()
    {
      var SyntaxToken = ReadContract.PeekToken();

      if (SyntaxToken == null)
        throw ReadContract.Interrupt("expected a token but found the end of the document.");

      var SyntaxTokenType = SyntaxToken.Type.Value;

      ReadContract.SkipToken();

      return SyntaxTokenType;
    }
    public void ReadTokenType(Inv.Syntax.TokenType TokenType)
    {
      var SyntaxToken = ReadContract.PeekToken();

      if (SyntaxToken == null)
        throw ReadContract.Interrupt($"expected token '{TokenType}' but found the end of the document.");

      if (TokenType != SyntaxToken.Type)
        throw ReadContract.Interrupt($"expected token '{TokenType}' but found token '{SyntaxToken}'.");

      ReadContract.SkipToken();
    }
    public bool ReadOptionalTokenType(Inv.Syntax.TokenType TokenType)
    {
      var Result = NextIsTokenType(TokenType);

      if (Result)
        ReadContract.SkipToken();

      return Result;
    }
    public bool NextIsIdentifierValue(string ExpectedIdentifier)
    {
      var SyntaxToken = ReadContract.PeekToken();

      return SyntaxToken != null && SyntaxToken.Type == Inv.Syntax.TokenType.Identifier && string.Equals(SyntaxToken.Identifier, ExpectedIdentifier, StringComparison.CurrentCultureIgnoreCase);
    }
    public bool NextIsIdentifier()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Identifier);
    }
    public bool NextIsString()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.String);
    }
    public bool NextIsColour()
    {
      return NextIsTokenType(TokenType.Hexadecimal);
    }
    public bool NextIsTime()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Time);
    }
    public bool NextIsTimePeriod()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.TimePeriod);
    }
    public bool NextIsTimeSpan()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.TimeSpan);
    }
    public bool NextIsNumber()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Number);
    }
    public bool NextIsBase64()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Base64);
    }
    public bool NextIsHexadecimal()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Hexadecimal);
    }
    public bool NextIsBackSlash()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.BackSlash);
    }
    public bool NextIsPeriod()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Period);
    }
    public bool NextIsForwardSlash()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.ForwardSlash);
    }
    public bool NextIsExclamationPoint()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.ExclamationPoint);
    }
    public bool NextIsBoolean()
    {
      if (NextIsIdentifier())
      {
        var SyntaxToken = ReadContract.PeekToken();
        return bool.TryParse(SyntaxToken.Identifier, out var Result);
      }
      else
        return false;
    }
    public bool NextIsOpenBrace()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.OpenBrace);
    }
    public bool NextIsCloseBrace()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.CloseBrace);
    }
    public bool NextIsOpenAngle()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.OpenAngle);
    }
    public bool NextIsCloseAngle()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.CloseAngle);
    }
    public bool NextIsOpenRound()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.OpenRound);
    }
    public bool NextIsCloseRound()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.CloseRound);
    }
    public bool NextIsOpenScope()
    {
      return NextIsOpenBrace();
    }
    public bool NextIsCloseScope()
    {
      return NextIsCloseBrace();
    }
    public bool NextIsSemicolon()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Semicolon);
    }
    public bool NextIsUnderscore()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Underscore);
    }
    public bool NextIsHash()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Hash);
    }
    public bool NextIsColon()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Colon);
    }
    public bool NextIsEqualSign()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.EqualSign);
    }
    public string PeekIdentifier()
    {
      return PeekToken(Inv.Syntax.TokenType.Identifier).Identifier;
    }
    public string ReadIdentifierOrKeyword()
    {
      return NextIsIdentifier() ? ReadIdentifier() : ReadKeyword();
    }
    public string ReadIdentifierOrNumber()
    {
      return NextIsIdentifier() ? ReadIdentifier() : ReadNumber();
    }
    public string ReadIdentifierOrString()
    {
      return NextIsIdentifier() ? ReadIdentifier() : ReadString();
    }
    public string ReadIdentifier()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Identifier).Identifier;

      ReadContract.SkipToken();

      return Result;
    }
    public string ReadOptionalIdentifier()
    {
      if (NextIsIdentifier())
        return ReadIdentifier();
      else
        return null;
    }
    public bool ReadOptionalIdentifierValue(string ExpectedIdentifier)
    {
      var Result = NextIsIdentifierValue(ExpectedIdentifier);

      if (Result)
        ReadContract.SkipToken();

      return Result;
    }
    public string ReadIdentifierValue(string ExpectedIdentifier)
    {
      return ReadIdentifierValueArray(ExpectedIdentifier);
    }
    public string ReadIdentifierValueArray(params string[] ExpectedIdentifierArray)
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Identifier).Identifier;

      if (!ExpectedIdentifierArray.Contains(Result, StringComparer.CurrentCultureIgnoreCase))
        throw ReadContract.Interrupt($"expected identifier '{ExpectedIdentifierArray.AsSeparatedText(" or ")}' but found identifier '{Result}'.");

      ReadContract.SkipToken();

      return Result;
    }
    public string[] ReadIdentifierChainArray(Inv.Syntax.TokenType SeparatorTokenType)
    {
      var Result = new Inv.DistinctList<string>()
      {
        ReadIdentifier()
      };

      while (NextIsTokenType(SeparatorTokenType))
      {
        ReadContract.SkipToken();

        Result.Add(ReadIdentifier());
      }

      return Result.ToArray();
    }
    public string ReadIdentifierChainString(Inv.Syntax.TokenType SeparatorTokenType)
    {
      var Result = new StringBuilder(ReadIdentifier());
      var Separator = Inv.Syntax.Foundation.RepresentTokenType(SeparatorTokenType);

      while (NextIsTokenType(SeparatorTokenType))
      {
        ReadContract.SkipToken();

        Result.Append(Separator + ReadIdentifier());
      }

      return Result.ToString();
    }
    public List<string> ReadIdentifierChainStringList(Inv.Syntax.TokenType SeparatorTokenType)
    {
      var Result = new List<string>()
      {
        ReadIdentifier()
      };

      while (NextIsTokenType(SeparatorTokenType))
      {
        ReadContract.SkipToken();

        Result.Add(ReadIdentifier());
      }

      return Result;
    }
    public string PeekKeyword()
    {
      return PeekToken(Inv.Syntax.TokenType.Keyword).Keyword;
    }
    public string PeekKeywordOrIdentifier()
    {
      if (NextIsKeyword())
        return PeekKeyword();
      else
        return PeekIdentifier();
    }
    public bool NextIsKeywordValue(string ExpectedKeyword)
    {
      var SyntaxToken = ReadContract.PeekToken();

      return SyntaxToken != null && SyntaxToken.Type == Inv.Syntax.TokenType.Keyword && string.Equals(SyntaxToken.Keyword, ExpectedKeyword, StringComparison.CurrentCultureIgnoreCase);
    }
    public bool NextIsKeyword()
    {
      return NextIsTokenType(Inv.Syntax.TokenType.Keyword);
    }
    public bool NextIsKeywordValueArray(params string[] KeywordArray)
    {
      var NextKeywordFound = false;

      if (NextIsKeyword())
      {
        var Keyword = PeekKeyword();

        var KeywordIndex = 0;
        while (!NextKeywordFound && (KeywordIndex < KeywordArray.Length))
        {
          NextKeywordFound = string.Equals(KeywordArray[KeywordIndex], Keyword, StringComparison.CurrentCultureIgnoreCase);
          KeywordIndex++;
        }
      }

      return NextKeywordFound;
    }
    public string ReadKeyword()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Keyword).Keyword;

      ReadContract.SkipToken();

      return Result;
    }
    public void ReadKeywordValue(string ExpectedKeyword)
    {
      ReadKeywordValueArray(ExpectedKeyword);
    }
    public string ReadKeywordValueArray(params string[] ExpectedKeywordArray)
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Keyword).Keyword;

      if (!ExpectedKeywordArray.Contains(Result, StringComparer.CurrentCultureIgnoreCase))
        throw ReadContract.Interrupt($"expected keyword '{ExpectedKeywordArray.AsSeparatedText(" or ")}' but found keyword '{Result}'.");

      ReadContract.SkipToken();

      return Result;
    }
    public bool NextIsNumberValue(string ExpectedNumber)
    {
      var SyntaxToken = ReadContract.PeekToken();

      return SyntaxToken != null && SyntaxToken.Type == Inv.Syntax.TokenType.Number && string.Equals(SyntaxToken.Number, ExpectedNumber, StringComparison.CurrentCultureIgnoreCase);
    }
    public void ReadNumberValue(string ExpectedNumber)
    {
      ReadNumberValueArray(ExpectedNumber);
    }
    public string ReadNumberValueArray(params string[] ExpectedNumberArray)
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Number).Number;

      if (!ExpectedNumberArray.Contains(Result, StringComparer.CurrentCultureIgnoreCase))
        throw ReadContract.Interrupt($"expected number '{ExpectedNumberArray.AsSeparatedText(" or ")}' but found number '{Result}'.");

      ReadContract.SkipToken();

      return Result;
    }
    public bool ReadOptionalNumberValue(string ExpectedNumber)
    {
      var Result = NextIsNumberValue(ExpectedNumber);

      if (Result)
        ReadNumberValue(ExpectedNumber);

      return Result;
    }
    public string ReadOptionalKeyword()
    {
      if (NextIsKeyword())
        return ReadKeyword();
      else
        return null;
    }
    public bool ReadOptionalKeywordValueArray(params string[] ExpectedKeywordArray)
    {
      var Result = NextIsKeywordValueArray(ExpectedKeywordArray);

      if (Result)
        ReadKeywordValueArray(ExpectedKeywordArray);

      return Result;
    }
    public bool ReadOptionalKeywordValue(string ExpectedKeyword)
    {
      var Result = NextIsKeywordValue(ExpectedKeyword);

      if (Result)
        ReadKeywordValue(ExpectedKeyword);

      return Result;
    }
    public string ReadOptionalString()
    {
      if (NextIsString())
        return ReadString();
      else
        return null;
    }
    public Inv.Colour ReadOptionalColour()
    {
      if (NextIsColour())
        return ReadColour();
      else
        return null;
    }
    public bool ReadOptionalAmpersand()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Ampersand);
    }
    public bool ReadOptionalAsterisk()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Asterisk);
    }
    public bool ReadOptionalAt()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.At);
    }
    public bool ReadOptionalComma()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Comma);
    }
    public bool ReadOptionalColon()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Colon);
    }
    public bool ReadOptionalPipe()
    {
      return ReadOptionalTokenType(TokenType.Pipe);
    }
    public bool ReadOptionalExclamationPoint()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.ExclamationPoint);
    }
    public bool ReadOptionalPeriod()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Period);
    }
    public bool ReadOptionalHash()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Hash);
    }
    public bool ReadOptionalQuestionMark()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.QuestionMark);
    }
    public bool ReadOptionalPlus()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Plus);
    }
    public bool ReadOptionalMinus()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Minus);
    }
    public bool ReadOptionalOpenRound()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.OpenRound);
    }
    public bool ReadOptionalCloseRound()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.CloseRound);
    }
    public bool ReadOptionalOpenSquare()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.OpenSquare);
    }
    public bool ReadOptionalCloseSquare()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.CloseSquare);
    }
    public bool ReadOptionalOpenBrace()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.OpenBrace);
    }
    public bool ReadOptionalCloseBrace()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.CloseBrace);
    }
    public bool ReadOptionalOpenAngle()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.OpenAngle);
    }
    public bool ReadOptionalCloseAngle()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.CloseAngle);
    }
    public bool ReadOptionalTilde()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Tilde);
    }
    public bool ReadOptionalSemicolon()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Semicolon);
    }
    public bool ReadOptionalEqualSign()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.EqualSign);
    }
    public bool ReadOptionalForwardSlash()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.ForwardSlash);
    }
    public bool ReadOptionalUnderscore()
    {
      return ReadOptionalTokenType(Inv.Syntax.TokenType.Underscore);
    }
    public string ReadString()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.String).String;

      ReadContract.SkipToken();

      return Result;
    }
    public char ReadCharacter()
    {
      return ReadString()[0];
    }
    public Guid ReadGuid()
    {
      var StringValue = ReadString();

      if (!Guid.TryParse(StringValue, out Guid Value))
        ReadContract.Interrupt($"String '{StringValue}' is not a value Guid value.");

      return Value;
    }
    public System.DateTime ReadDateTime()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.DateTime).DateTime;

      ReadContract.SkipToken();

      return Result;
    }
    public System.DateTimeOffset ReadDateTimeOffset()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Timestamp).Timestamp;

      ReadContract.SkipToken();

      return Result;
    }
    public Inv.Date ReadDate()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Date).Date;

      ReadContract.SkipToken();

      return Result;
    }
    public Inv.Time ReadTime()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Time).Time;

      ReadContract.SkipToken();

      return Result;
    }
    public Inv.TimePeriod ReadTimePeriod()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.TimePeriod).TimePeriod;

      ReadContract.SkipToken();

      return Result;
    }
    public System.TimeSpan ReadTimeSpan()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.TimeSpan).TimeSpan;

      ReadContract.SkipToken();

      return Result;
    }
    public bool ReadBoolean()
    {
      var Identifier = ReadIdentifier();

      if ("true".Equals(Identifier, StringComparison.OrdinalIgnoreCase))
        return true;

      if ("false".Equals(Identifier, StringComparison.OrdinalIgnoreCase))
        return false;

      throw ReadContract.Interrupt($"Identifier '{Identifier}' is not an expected boolean value.");
    }
    public string ReadNumber()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Number).Number;

      ReadContract.SkipToken();

      return Result;
    }
    public double ReadReal()
    {
      var Sign = 1;
      if (ReadOptionalTokenType(Inv.Syntax.TokenType.Minus))
        Sign = -1;

      var RealText = ReadNumber();

      if (ReadOptionalPeriod())
        RealText += "." + ReadNumber();

      return Convert.ToDouble(RealText, System.Globalization.CultureInfo.InvariantCulture) * Sign;
    }
    public long ReadInteger64()
    {
      return Convert.ToInt64(ReadOptionalMinus() ? "-" + ReadNumber() : ReadNumber());
    }
    public int ReadInteger32()
    {
      return Convert.ToInt32(ReadOptionalMinus() ? "-" + ReadNumber() : ReadNumber());
    }
    public decimal ReadDecimal()
    {
      var Sign = 1;
      if (ReadOptionalTokenType(Inv.Syntax.TokenType.Minus))
        Sign = -1;

      var DecimalText = ReadNumber();
      if (ReadOptionalPeriod())
        DecimalText += "." + ReadNumber();

      return Convert.ToDecimal(DecimalText, System.Globalization.CultureInfo.InvariantCulture) * Sign;
    }
    public byte[] ReadBase64()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Base64).Base64.GetBuffer();

      ReadContract.SkipToken();

      return Result;
    }
    public byte[] ReadHexadecimal()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Hexadecimal).Hexadecimal.GetBuffer();

      ReadContract.SkipToken();

      return Result;
    }
    public Inv.Colour ReadColour()
    {
      if (NextIsIdentifier())
      {
        var Identifier = ReadIdentifier();
        var Result = Inv.Colour.FromName(Identifier);

        if (Result == null)
          throw ReadContract.Interrupt($"'{Identifier}' is not a registered colour name.");

        return Result;
      }
      else
      {
        var Hex = ReadHexadecimal();

        if (Hex.Length == 4)
          return Inv.Colour.FromArgb(BitConverter.ToInt32(Hex.Reverse().ToArray(), 0));
        else
          throw ReadContract.Interrupt($"'{Hex}' must be a valid four-part colour hexadecimal.");
      }
    }
    public void ReadOpenBrace()
    {
      ReadTokenType(Inv.Syntax.TokenType.OpenBrace);
    }
    public void ReadCloseBrace()
    {
      ReadTokenType(Inv.Syntax.TokenType.CloseBrace);
    }
    public void ReadOpenSquare()
    {
      ReadTokenType(Inv.Syntax.TokenType.OpenSquare);
    }
    public void ReadCloseSquare()
    {
      ReadTokenType(Inv.Syntax.TokenType.CloseSquare);
    }
    public void ReadOpenAngle()
    {
      ReadTokenType(Inv.Syntax.TokenType.OpenAngle);
    }
    public void ReadCloseAngle()
    {
      ReadTokenType(Inv.Syntax.TokenType.CloseAngle);
    }
    public void ReadOpenRound()
    {
      ReadTokenType(Inv.Syntax.TokenType.OpenRound);
    }
    public void ReadCloseRound()
    {
      ReadTokenType(Inv.Syntax.TokenType.CloseRound);
    }
    public void ReadSemicolon()
    {
      ReadTokenType(Inv.Syntax.TokenType.Semicolon);
    }
    public void ReadColon()
    {
      ReadTokenType(Inv.Syntax.TokenType.Colon);
    }
    public void ReadPercent()
    {
      ReadTokenType(Inv.Syntax.TokenType.Percent);
    }
    public void ReadEqualSign()
    {
      ReadTokenType(Inv.Syntax.TokenType.EqualSign);
    }
    public void ReadExclamationPoint()
    {
      ReadTokenType(Inv.Syntax.TokenType.ExclamationPoint);
    }
    public void ReadQuestionMark()
    {
      ReadTokenType(Inv.Syntax.TokenType.QuestionMark);
    }
    public void ReadUnderscore()
    {
      ReadTokenType(Inv.Syntax.TokenType.Underscore);
    }
    public void ReadAmpersand()
    {
      ReadTokenType(Inv.Syntax.TokenType.Ampersand);
    }
    public void ReadPipe()
    {
      ReadTokenType(Inv.Syntax.TokenType.Pipe);
    }
    public void ReadComma()
    {
      ReadTokenType(Inv.Syntax.TokenType.Comma);
    }
    public void ReadPeriod()
    {
      ReadTokenType(Inv.Syntax.TokenType.Period);
    }
    public void ReadPlus()
    {
      ReadTokenType(Inv.Syntax.TokenType.Plus);
    }
    public void ReadMinus()
    {
      ReadTokenType(Inv.Syntax.TokenType.Minus);
    }
    public void ReadHash()
    {
      ReadTokenType(Inv.Syntax.TokenType.Hash);
    }
    public void ReadForwardSlash()
    {
      ReadTokenType(Inv.Syntax.TokenType.ForwardSlash);
    }
    public void ReadBackSlash()
    {
      ReadTokenType(Inv.Syntax.TokenType.BackSlash);
    }
    public void ReadCaret()
    {
      ReadTokenType(Inv.Syntax.TokenType.Caret);
    }
    public void ReadTilde()
    {
      ReadTokenType(Inv.Syntax.TokenType.Tilde);
    }
    public void ReadAsterisk()
    {
      ReadTokenType(Inv.Syntax.TokenType.Asterisk);
    }
    public void ReadAt()
    {
      ReadTokenType(Inv.Syntax.TokenType.At);
    }
    public void ReadDollarSign()
    {
      ReadTokenType(Inv.Syntax.TokenType.DollarSign);
    }
    public string ReadWhiteSpace()
    {
      var Result = PeekToken(Inv.Syntax.TokenType.Whitespace).Whitespace;

      ReadContract.SkipToken();

      return Result;
    }
    public string ReadOpenScopeString(string Keyword)
    {
      ReadKeywordValue(Keyword);
      var Identifier = ReadString();
      ReadOpenScope();
      return Identifier;
    }
    public string ReadOpenScopeIdentifier(string Keyword)
    {
      ReadKeywordValue(Keyword);
      var Identifier = ReadIdentifier();
      ReadOpenScope();
      return Identifier;
    }
    public void ReadOpenScopeKeyword(string Keyword)
    {
      ReadKeywordValue(Keyword);
      ReadOpenScope();
    }
    public bool ReadOptionalOpenScope()
    {
      return ReadOptionalOpenBrace();
    }
    public void ReadOpenScope()
    {
      ReadOpenBrace();
    }
    public void ReadCloseScope()
    {
      ReadCloseBrace();
    }
    public void ReadOpenCollectionKeyword(string Keyword)
    {
      ReadKeywordValue(Keyword);
      ReadOpenRound();
    }
    public string ReadOpenCollectionIdentifier(string Keyword)
    {
      ReadKeywordValue(Keyword);
      var Identifier = ReadIdentifier();
      ReadOpenRound();
      return Identifier;
    }
    public string ReadOptionalStringAttribute(string Keyword)
    {
      if (NextIsKeywordValue(Keyword))
      {
        ReadKeywordValue(Keyword);
        ReadEqualSign();
        var StringValue = ReadString();
        ReadSemicolon();
        return StringValue;
      }
      else
      {
        return null;
      }
    }
    public string ReadOptionalIdentifierAttribute(string Keyword)
    {
      if (NextIsKeywordValue(Keyword))
      {
        ReadKeywordValue(Keyword);
        ReadEqualSign();
        var StringValue = ReadIdentifier();
        ReadSemicolon();
        return StringValue;
      }
      else
      {
        return null;
      }
    }
    public DateTimeOffset? ReadOptionalDateTimeAttribute(string Keyword)
    {
      if (NextIsKeywordValue(Keyword))
      {
        ReadKeywordValue(Keyword);
        ReadEqualSign();
        var DateTimeValue = ReadDateTimeOffset();
        ReadSemicolon();
        return DateTimeValue;
      }
      else
      {
        return null;
      }
    }
    public decimal? ReadOptionalDecimalAttribute(string Keyword)
    {
      if (NextIsKeywordValue(Keyword))
      {
        ReadKeywordValue(Keyword);
        ReadEqualSign();
        var DecimalValue = ReadDecimal();
        ReadSemicolon();
        return DecimalValue;
      }
      else
      {
        return null;
      }
    }
    public Inv.Money? ReadOptionalMoneyAttribute(string Keyword)
    {
      var Result = ReadOptionalDecimalAttribute(Keyword);

      if (Result == null)
        return null;
      else
        return new Inv.Money(Result.Value);
    }
    public long? ReadOptionalInteger64Attribute(string Keyword)
    {
      if (NextIsKeywordValue(Keyword))
      {
        ReadKeywordValue(Keyword);
        ReadEqualSign();
        var IntegerValue = ReadInteger32();
        ReadSemicolon();
        return IntegerValue;
      }
      else
      {
        return null;
      }
    }
    public bool? ReadOptionalBooleanAttribute(string Keyword)
    {
      if (NextIsKeywordValue(Keyword))
      {
        ReadKeywordValue(Keyword);
        ReadEqualSign();
        var BooleanValue = ReadBoolean();
        ReadSemicolon();
        return BooleanValue;
      }
      else
      {
        return null;
      }
    }

    private Inv.Syntax.Token PeekToken(Inv.Syntax.TokenType TokenType)
    {
      var SyntaxToken = ReadContract.PeekToken();

      if (SyntaxToken == null)
        throw ReadContract.Interrupt($"expected token '{TokenType}' but found the end of the document.");

      if (SyntaxToken.Type != TokenType)
        throw ReadContract.Interrupt($"expected token '{TokenType}' but found token '{SyntaxToken}'.");

      return SyntaxToken;
    }
  }

  public sealed class WriteException : System.Exception
  {
    public WriteException(string message)
      : base(message)
    {
    }
  }

  public sealed class ReadException : System.Exception
  {
    public ReadException(string message, long? Location)
      : base(message)
    {
      this.Location = Location;
    }
    public ReadException(string message, long? Location, Exception innerException)
      : base(message, innerException)
    {
      this.Location = Location;
    }

    public long? Location { get; private set; }
  }

  internal static class SymbolTable
  {
    public static readonly Inv.Bidictionary<char, Syntax.TokenType> CharacterTokenTypeIndex;
    public static readonly Inv.CharacterSet WhitespaceCharacterSet;
    public static readonly Inv.CharacterSet IdentifierCharacterSet;
    public static readonly Inv.CharacterSet NumberCharacterSet;
    public static readonly Inv.CharacterSet HexadecimalCharacterSet;
    public static readonly Inv.CharacterSet Base64CharacterSet;
    public static readonly Inv.CharacterSet VerticalWhitespaceCharacterSet;

    static SymbolTable()
    {
      CharacterTokenTypeIndex = new Inv.Bidictionary<char, Syntax.TokenType>()
      {
        { '(', Inv.Syntax.TokenType.OpenRound },
        { ')', Inv.Syntax.TokenType.CloseRound },
        { '<', Inv.Syntax.TokenType.OpenAngle },
        { '>', Inv.Syntax.TokenType.CloseAngle },
        { '{', Inv.Syntax.TokenType.OpenBrace },
        { '}', Inv.Syntax.TokenType.CloseBrace },
        { ',', Inv.Syntax.TokenType.Comma },
        { '.', Inv.Syntax.TokenType.Period },
        { ';', Inv.Syntax.TokenType.Semicolon },
        { ':', Inv.Syntax.TokenType.Colon },
        { '=', Inv.Syntax.TokenType.EqualSign },
        { '!', Inv.Syntax.TokenType.ExclamationPoint },
        { '?', Inv.Syntax.TokenType.QuestionMark },
        { '&', Inv.Syntax.TokenType.Ampersand },
        { '|', Inv.Syntax.TokenType.Pipe },
        { '+', Inv.Syntax.TokenType.Plus },
        { '-', Inv.Syntax.TokenType.Minus },
        { '%', Inv.Syntax.TokenType.Percent },
        { '*', Inv.Syntax.TokenType.Asterisk },
        { '\\', Inv.Syntax.TokenType.BackSlash },
        { '/', Inv.Syntax.TokenType.ForwardSlash },
        { '@', Inv.Syntax.TokenType.At },
        { '^', Inv.Syntax.TokenType.Caret },
        { '~', Inv.Syntax.TokenType.Tilde },
        { '$', Inv.Syntax.TokenType.DollarSign },
        { '#', Inv.Syntax.TokenType.Hash },
        { '\'', Inv.Syntax.TokenType.SingleQuote },
        { '"', Inv.Syntax.TokenType.DoubleQuote },
        { '`', Inv.Syntax.TokenType.BackQuote },
        { '[', Inv.Syntax.TokenType.OpenSquare },
        { ']', Inv.Syntax.TokenType.CloseSquare },
        { '_', Inv.Syntax.TokenType.Underscore },
      };

      WhitespaceCharacterSet = new Inv.CharacterSet();
      WhitespaceCharacterSet.AddRange('\x0000', ' ');

      IdentifierCharacterSet = new Inv.CharacterSet();
      IdentifierCharacterSet.AddRange('a', 'z');
      IdentifierCharacterSet.AddRange('A', 'Z');
      IdentifierCharacterSet.AddRange('0', '9');
      IdentifierCharacterSet.Add('_');

      NumberCharacterSet = new Inv.CharacterSet();
      NumberCharacterSet.AddRange('0', '9');

      VerticalWhitespaceCharacterSet = new Inv.CharacterSet();
      VerticalWhitespaceCharacterSet.Add('\n');
      VerticalWhitespaceCharacterSet.Add('\r');

      HexadecimalCharacterSet = new Inv.CharacterSet();
      HexadecimalCharacterSet.AddRange('0', '9');
      HexadecimalCharacterSet.AddRange('A', 'F');
      HexadecimalCharacterSet.AddRange('a', 'f');

      Base64CharacterSet = new Inv.CharacterSet();
      Base64CharacterSet.AddRange('0', '9');
      Base64CharacterSet.AddRange('A', 'Z');
      Base64CharacterSet.AddRange('a', 'z');
      Base64CharacterSet.Add('+');
      Base64CharacterSet.Add('/');
      Base64CharacterSet.Add('=');
    }
  }
}