﻿using System;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Diagnostics;
using System.Collections.Generic;
using Inv.Support;

namespace Inv.Database
{
  public sealed class Engine
  {
    public Engine(Inv.Database.Platform Platform)
    {
      this.Platform = Platform;
    }

    public bool Exists()
    {
      return Platform.Exists();
    }
    public void Create()
    {
      Platform.Create();
    }
    public void Destroy()
    {
      Platform.Drop();
    }
    public Connection<TSchematic> Connect<TSchematic>(TSchematic Schematic)
      where TSchematic : Contract<Schematic>
    {
      return new Connection<TSchematic>(this, Schematic, Platform);
    }

    [Conditional("DEBUG")]
    public static void WriteTrace(string QueryText)
    {
      Debug.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
      Debug.WriteLine(QueryText);
    }
    [Conditional("DEBUG")]
    public static void WriteTraceParameter(Inv.Database.Parameter Parameter)
    {
      Debug.WriteLine($"{ Parameter.Variable.Name } = { (Parameter.Value == null ? "null" : Parameter.Value.ToString()) } ({ Parameter.ColumnType.ToString() })");
    }

    private Inv.Database.Platform Platform;
  }

  public sealed class Connection<TSchematic> : IDisposable
      where TSchematic : Contract<Schematic>
  {
    internal Connection(Inv.Database.Engine Engine, TSchematic Schematic, Inv.Database.Platform Platform)
    {
      this.Engine = Engine;
      this.Schematic = Schematic;
      this.Base = Platform.NewConnection();

      var Retry = false;
      var Attempt = 0;
      do
      {
        try
        {
          Base.Open();

          break;
        }
        catch
        {
          // some kind of exception.
          Attempt++;
          Retry = Attempt < 10;

          if (Retry)
            Base.RetrySleep(1000);
          else
            throw;
        }
      }
      while (Retry);

      this.Query = new Query<TSchematic>(Schematic);
    }
    public void Dispose()
    {
      Base.Dispose();
    }

    public Query<TSchematic> Query { get; private set; }

    public void CreateSchematic()
    {
      new CreateSchematicConductor<TSchematic>(Base, Schematic);
    }
    public Inv.Database.HealthCheck HealthCheck()
    {
      var Result = new Inv.Database.HealthCheck();

      Base.HealthCheck(Result);

      if (Base.Platform.UseManualDataTypeCheck)
      {
        var PrimaryKeyGroup = Result.AddGroup("Primary Keys");
        var DataTypeGroup = Result.AddGroup("Data Types");
        var ForeignKeyGroup = Result.AddGroup("Foreign Keys");

        var TableList = Schematic.Base.GetTables().ToDistinctList();

        var ReferenceValueDictionary = new Dictionary<Inv.Database.ForeignKeyColumn, ReferenceValue>();
        var ReferenceTableDictionary = new Dictionary<Inv.Database.Table, ReferenceTable>();

        foreach (var Table in TableList)
        {
          var ReferenceTable = new ReferenceTable(Table);
          ReferenceTableDictionary.Add(Table, ReferenceTable);

          var ColumnList = Table.GetColumns().ToDistinctList();

          var SourceTable = Query.SourceTable(Table).As("R");

          var SelectClause = Query.SelectClause();
          SelectClause.From(SourceTable);

          var Select = Query.Select(SelectClause);

          var ColumnIndex = 0;
          var PrimaryKeyColumnIndex = (int?)null;
          foreach (var Column in ColumnList)
          {
            SelectClause.Select(SourceTable[Column.Base]).As(Column.Base.Name);

            if (Column.Base.Type == ColumnType.PrimaryKey)
              PrimaryKeyColumnIndex = ColumnIndex;

            ColumnIndex++;
          }

          if (PrimaryKeyColumnIndex == null)
          {
            PrimaryKeyGroup.AddResult($"{ Table.Name }: No primary key column found.");

            continue;
          }

          using (var Reader = SelectReader(Select))
          {
            while (Reader.Read())
            {
              ColumnIndex = 0;

              var PrimaryKeyValue = Reader.GetInteger32(PrimaryKeyColumnIndex.Value);
              ReferenceTable.PrimaryKeySet.Add(PrimaryKeyValue);

              foreach (var Column in ColumnList)
              {
                var BaseColumn = Column.Base;
                var ColumnType = BaseColumn.Type;

                if (PrimaryKeyColumnIndex == ColumnIndex)
                {
                  ColumnIndex++;
                  continue;
                }

                if (BaseColumn.Nullable && Reader.IsNull(ColumnIndex))
                {
                  ColumnIndex++;
                  continue;
                }

                if (BaseColumn.Type == ColumnType.ForeignKey)
                {
                  var ForeignKeyColumn = (Inv.Database.ForeignKeyColumn)Column;

                  var ReferenceValue = ReferenceValueDictionary.GetValueOrDefault(ForeignKeyColumn);
                  if (ReferenceValue == null)
                  {
                    ReferenceValue = new ReferenceValue(ForeignKeyColumn);
                    ReferenceValueDictionary.Add(ForeignKeyColumn, ReferenceValue);
                  }

                  var ForeignKeyValue = Reader.GetInteger32(ColumnIndex);
                  ReferenceValue.KeyValueDictionary.Add(PrimaryKeyValue, ForeignKeyValue);
                }

                Base.DataTypeCheck(DataTypeGroup, Table, PrimaryKeyValue, BaseColumn, Reader.GetString(ColumnIndex));

                ColumnIndex++;
              }
            }
          }
        }

        foreach (var ReferenceValue in ReferenceValueDictionary.OrderBy(P => P.Key.Table.Name).ThenBy(P => P.Key.Name))
        {
          var Column = ReferenceValue.Key;
          var Table = Column.Table;

          var ReferenceTable = ReferenceTableDictionary[Column.ReferenceTable];

          foreach (var KeyValuePair in ReferenceValue.Value.KeyValueDictionary)
          {
            var ReferenceKey = KeyValuePair.Key;
            var PrimaryKeyValue = KeyValuePair.Value;

            if (ReferenceTable.PrimaryKeySet.Contains(PrimaryKeyValue))
              continue;

            ForeignKeyGroup.AddResult($"{ Table.Name }.{ Column.Name } violates referential integrity (row { ReferenceKey.ToString() }, invalid reference { PrimaryKeyValue.ToString() })");
          }
        }

        if (!PrimaryKeyGroup.GetResults().Any())
          Result.RemoveGroup(PrimaryKeyGroup);

        if (!DataTypeGroup.GetResults().Any())
          Result.RemoveGroup(DataTypeGroup);

        if (!ForeignKeyGroup.GetResults().Any())
          Result.RemoveGroup(ForeignKeyGroup);
      }

      return Result;
    }
    public int Insert<TTable>(Insert<TTable> Insert)
      where TTable : Inv.Database.Contract<Inv.Database.Table>
    {
      return new InsertConductor<TTable>(Base, Insert).Key;
    }
    public int Update<TTable>(Update<TTable> Update)
      where TTable : Inv.Database.Contract<Inv.Database.Table>
    {
      return new UpdateConductor<TTable>(Base, Update).AffectedRowCount;
    }
    public int Delete<TTable>(Delete<TTable> Delete)
      where TTable : Inv.Database.Contract<Inv.Database.Table>
    {
      return new DeleteConductor<TTable>(Base, Delete).AffectedRowCount;
    }
    public SelectReader SelectReader(Select Select)
    {
      return new SelectConductor(Base, Select).Reader;
    }
    public Inv.Database.Transaction BeginTransaction()
    {
      return Base.BeginTransaction();
    }
    public Inv.Database.Catalog GetDetectedCatalog()
    {
      return Base.Platform.GetCatalog(Base);
    }
    public Inv.Database.Catalog GetExpectedCatalog()
    {
      var BaseSchematic = Schematic.Base;

      var Result = new Inv.Database.Catalog();

      var CatalogTableNameDictionary = new Dictionary<string, Inv.Database.CatalogTable>();
      var SchematicCatalogColumnDictionary = new Dictionary<Inv.Database.Contract<Inv.Database.Column>, CatalogColumn>();

      foreach (var SchematicTable in BaseSchematic.GetTables())
      {
        var CatalogTable = Result.AddTable();
        CatalogTable.Name = SchematicTable.Name;
        CatalogTableNameDictionary.Add(CatalogTable.Name, CatalogTable);

        if (SchematicTable.PrimaryKeyColumn != null)
        {
          var CatalogPrimaryKeyColumn = CatalogTable.AddColumn();
          CatalogPrimaryKeyColumn.Name = SchematicTable.PrimaryKeyColumn.Name;
          CatalogPrimaryKeyColumn.Nullable = false;
          CatalogPrimaryKeyColumn.DataType = Base.Platform.GetCatalogColumnDataType(SchematicTable.PrimaryKeyColumn);
          CatalogTable.PrimaryKeyColumn = CatalogPrimaryKeyColumn;

          var CatalogPrimaryKeyIndex = CatalogTable.AddIndex();
          CatalogPrimaryKeyIndex.Name = Base.Platform.GetPrimaryKeyName(SchematicTable.PrimaryKeyColumn);
          CatalogPrimaryKeyIndex.IsUnique = true;
          CatalogPrimaryKeyIndex.ColumnList.Add(CatalogPrimaryKeyColumn);
        }

        foreach (var SchematicColumn in SchematicTable.GetColumns())
        {
          var BaseColumn = SchematicColumn.Base;

          if (BaseColumn.Name.Equals(SchematicTable.PrimaryKeyColumn?.Name))
            continue;

          var CatalogColumn = CatalogTable.AddColumn();
          CatalogColumn.Name = BaseColumn.Name;
          CatalogColumn.Nullable = BaseColumn.Nullable;
          CatalogColumn.DataType = Base.Platform.GetCatalogColumnDataType(SchematicColumn);

          SchematicCatalogColumnDictionary.Add(BaseColumn, CatalogColumn);
        }
      }

      foreach (var SchematicTable in BaseSchematic.GetTables())
      {
        var CatalogTable = CatalogTableNameDictionary.GetValueOrDefault(SchematicTable.Name);
        if (CatalogTable == null)
          throw new Exception($"Could not find catalog table '{ CatalogTable.Name }'");

        var SchematicIndexList = SchematicTable.GetIndexes().Select(I => I.Base).ToDistinctList();
        var SchematicForeignKeyColumnList = SchematicTable.GetForeignKeyColumns();

        foreach (var SchematicForeignKeyColumn in SchematicForeignKeyColumnList)
        {
          var BaseColumn = ((Inv.Database.Contract<Column>)SchematicForeignKeyColumn).Base;

          var CatalogColumn = SchematicCatalogColumnDictionary[BaseColumn];

          var CatalogReferenceTable = CatalogTableNameDictionary.GetValueOrDefault(SchematicForeignKeyColumn.ReferenceTable.Name);
          if (CatalogReferenceTable == null)
            throw new Exception($"Could not find catalog table '{ SchematicForeignKeyColumn.ReferenceTable.Name }'");

          if (Base.Platform.UseForeignKeys)
          {
            var CatalogForeignKey = new CatalogForeignKey();
            CatalogColumn.ForeignKey = CatalogForeignKey;
            CatalogForeignKey.Name = Syntax.ForeignKeyName(SchematicForeignKeyColumn);
            CatalogForeignKey.ReferenceTable = CatalogReferenceTable;
            CatalogForeignKey.ReferenceColumn = CatalogReferenceTable.PrimaryKeyColumn;
          }

          var SchematicForeignKeyIndex = SchematicIndexList.Find(I => I.StartsWith(SchematicForeignKeyColumn));
          if (SchematicForeignKeyIndex == null)
          {
            var CatalogForeignKeyIndex = CatalogTable.AddIndex();
            CatalogForeignKeyIndex.Name = Syntax.IndexName(SchematicForeignKeyColumn);
            CatalogForeignKeyIndex.IsUnique = false;
            CatalogForeignKeyIndex.ColumnList.Add(CatalogColumn);
          }
        }

        foreach (var SchematicIndex in SchematicIndexList)
        {
          var CatalogIndex = CatalogTable.AddIndex();
          CatalogIndex.Name = Syntax.IndexName(SchematicIndex);
          CatalogIndex.IsUnique = SchematicIndex.IsUnique;

          foreach (var SchematicIndexColumn in SchematicIndex.GetColumns())
            CatalogIndex.ColumnList.Add(SchematicCatalogColumnDictionary[SchematicIndexColumn]);
        }
      }

      foreach (var SchematicView in BaseSchematic.GetViews())
      {
        var CatalogView = Result.AddView();
        CatalogView.Name = SchematicView.Name;

        // TODO.
        // CatalogView.Specification = "";

        foreach (var SchematicColumn in SchematicView.GetColumns().Select(C => C.Base))
        {
          var CatalogColumn = CatalogView.AddColumn();
          CatalogColumn.Name = SchematicColumn.Name;
          CatalogColumn.Nullable = SchematicColumn.Nullable;
          CatalogColumn.DataType = Base.Platform.GetCatalogColumnDataType(SchematicColumn);
        }
      }

      return Result;
    }
    public void ExecuteRaw(string RawText)
    {
      using (var Command = Base.NewCommand())
      {
        Command.SetText(RawText);
        Command.ExecuteNonQuery();
      }
    }

    private Inv.Database.Engine Engine;
    private TSchematic Schematic;
    private Inv.Database.Connection Base;

    private sealed class ReferenceTable
    {
      public ReferenceTable(Inv.Database.Table Table)
      {
        this.Table = Table;
        this.PrimaryKeySet = new HashSet<int>();
      }

      public Inv.Database.Table Table { get; private set; }
      public HashSet<int> PrimaryKeySet { get; private set; }
    }

    private sealed class ReferenceValue
    {
      public ReferenceValue(Inv.Database.Column Column)
      {
        this.Column = Column;
        this.KeyValueDictionary = new Dictionary<int, int>();
      }

      public Inv.Database.Column Column { get; private set; }
      public Dictionary<int, int> KeyValueDictionary { get; private set; }
    }
  }

  internal sealed class CreateSchematicConductor<TSchematic>
    where TSchematic : Inv.Database.Contract<Inv.Database.Schematic>
  {
    internal CreateSchematicConductor(Inv.Database.Connection Connection, TSchematic Schematic)
    {
      this.Connection = Connection;

      var TableList = Schematic.Base.GetTables().ToDistinctList();
      var ViewList = Schematic.Base.GetViews().ToDistinctList();

      // Create tables.
      foreach (var Table in TableList)
        CreateTable(Table);

      // Create foreign keys and indexes.
      foreach (var Table in TableList)
      {
        var IndexList = Table.GetIndexes().Select(I => I.Base).ToDistinctList();

        var ForeignKeyColumnList = Table.GetForeignKeyColumns().ToDistinctList();
        foreach (var ForeignKeyColumn in ForeignKeyColumnList)
        {
          var ForeignKeyIndex = IndexList.Find(I => I.StartsWith(ForeignKeyColumn));

          // MySQL creates indexes on all foreign keys, for completeness we do this for all platforms.
          if (ForeignKeyIndex == null)
          {
            CreateIndex(Table, ForeignKeyColumn);
          }
          else
          {
            CreateIndex(Table, ForeignKeyIndex);
            IndexList.Remove(ForeignKeyIndex);
          }

          if (Connection.Platform.UseForeignKeys)
            AlterTable(Table, $"add constraint { Connection.Platform.GetForeignKeyConstraintDefinition(ForeignKeyColumn) }");
        }

        // Create the remaining indexes.
        foreach (var Index in IndexList)
          CreateIndex(Table, Index);
      }

      // TODO: Determine dependencies somehow and create in required order.
      foreach (var View in ViewList)
      {
        var StringBuilder = new System.Text.StringBuilder();
        StringBuilder.AppendLine($"create view { Connection.Escape(View.Name) }");
        StringBuilder.AppendLine("  as");
        StringBuilder.AppendLine(SelectClauseFormatter.Format(View.SelectClause, Connection) + ";");

        Connection.ExecuteNonQuery(StringBuilder.ToString());
      }
    }

    private void CreateTable(Inv.Database.Table Table)
    {
      var PrimaryKeyDefinitionText = Connection.Platform.GetPrimaryKeyConstraintDefinition(Table.PrimaryKeyColumn);
      var HasPrimaryKeyDefinitionText = !string.IsNullOrWhiteSpace(PrimaryKeyDefinitionText);

      var StringBuilder = new System.Text.StringBuilder();
      StringBuilder.AppendLine($"create table { Connection.Escape(Table.Name) }");
      StringBuilder.AppendLine("(");

      var ColumnList = Table.GetColumns().ToDistinctList();
      foreach (var Column in ColumnList)
      {
        var ColumnStringBuilder = new System.Text.StringBuilder();
        ColumnStringBuilder.Append("  ");
        ColumnStringBuilder.Append($"{ Connection.Escape(Column.Base.Name) } { Connection.Platform.GetColumnDefinition(Column) }");

        if (HasPrimaryKeyDefinitionText || Column != ColumnList.Last())
          ColumnStringBuilder.Append(",");

        StringBuilder.AppendLine(ColumnStringBuilder.ToString());
      }

      if (HasPrimaryKeyDefinitionText)
        StringBuilder.AppendLine("  " + PrimaryKeyDefinitionText);

      StringBuilder.AppendLine(")");

      Connection.ExecuteNonQuery(StringBuilder.ToString());
    }
    private void CreateIndex(Inv.Database.Table Table, Inv.Database.ForeignKeyColumn ForeignKeyColumn)
    {
      Connection.ExecuteNonQuery($"create index { Syntax.IndexName(ForeignKeyColumn) } on { Connection.Escape(Table.Name) } ({ Connection.Escape(ForeignKeyColumn.Name) })");
    }
    private void CreateIndex(Inv.Database.Table Table, Inv.Database.Index Index)
    {
      if (Index.IsUnique && Connection.Platform.UseAlterTableAddUniqueConstraint)
        AlterTable(Table, $"add constraint { Connection.Platform.GetUniqueConstraintDefinition(Index) } ({ Index.GetColumns().Select(C => Connection.Escape(C.Name)).AsSeparatedText(", ") })");
      else
        Connection.ExecuteNonQuery($"create { (Index.IsUnique ? "unique " : "") }index { Syntax.IndexName(Index) } on { Connection.Escape(Table.Name) } ({ Index.GetColumns().Select(C => Connection.Escape(C.Name)).AsSeparatedText(", ") })");
    }
    private void AlterTable(Inv.Database.Table Table, string DefinitionText)
    {
      var StringBuilder = new System.Text.StringBuilder();
      StringBuilder.AppendLine($"alter table { Connection.Escape(Table.Name) }");
      StringBuilder.AppendLine($"  { DefinitionText }");
      Connection.ExecuteNonQuery(StringBuilder.ToString());
    }

    private Inv.Database.Connection Connection;
  }

  internal sealed class InsertConductor<TTable>
    where TTable : Inv.Database.Contract<Inv.Database.Table>
  {
    internal InsertConductor(Inv.Database.Connection Connection, Insert<TTable> Insert)
    {
      var StringBuilder = new System.Text.StringBuilder();
      StringBuilder.AppendLine($"insert into { Connection.Escape(Insert.SourceTable.Table.Base.Name) }"); // Don't use the Alias here as it is not valid syntax. Should Insert.Table be a direct table reference??
      StringBuilder.AppendLine("(");

      var ColumnArray = Insert.Assignment.GetColumns().ToArray();
      foreach (var Column in ColumnArray)
      {
        var ColumnText = $"{ Connection.Escape(Column.Base.Name) }";

        if (Column != ColumnArray.Last())
          ColumnText += ",";

        StringBuilder.AppendLine("  " + ColumnText);
      }

      StringBuilder.AppendLine(")");
      StringBuilder.AppendLine("values");
      StringBuilder.AppendLine("(");

      var ExpressionArray = Insert.Assignment.GetValues().ToArray();
      foreach (var Expression in ExpressionArray)
      {
        var ExpressionText = ExpressionFormatter.Format(Expression, Connection);

        if (Expression != ExpressionArray.Last())
          ExpressionText += ",";

        StringBuilder.AppendLine($"  { ExpressionText }");
      }

      StringBuilder.AppendLine(")");

      using (var Command = Connection.NewCommand())
      {
        // May need to use alternative SqlParameter constructor to correctly set enum values to use the right SqlDbType.
        foreach (var Parameter in Insert.Assignment.GetParameters())
        {
          Inv.Database.Engine.WriteTraceParameter(Parameter);
          Command.SetParameter(Parameter.Variable.Name, Parameter.Index, Parameter.ColumnType, Parameter.Value);
        }

        var QueryText = StringBuilder.ToString();
        Inv.Database.Engine.WriteTrace(QueryText);
        Command.SetText(QueryText);

        Command.ExecuteNonQuery();
      }

      var LastIdentityText = Connection.Platform.GetLastIdentityCommandText();
      if (!string.IsNullOrWhiteSpace(LastIdentityText))
      {
        using (var Command = Connection.NewCommand())
        {
          Inv.Database.Engine.WriteTrace(LastIdentityText);
          Command.SetText(LastIdentityText);

          using (var Reader = Command.ExecuteReader())
          {
            if (Reader.Read())
              this.Key = Reader.GetInteger32(0);
          }
        }
      }
    }

    public int Key { get; private set; }
  }

  internal sealed class UpdateConductor<TTable>
    where TTable : Inv.Database.Contract<Inv.Database.Table>
  {
    internal UpdateConductor(Inv.Database.Connection Connection, Update<TTable> Update)
    {
      var StringBuilder = new System.Text.StringBuilder();

      if (Connection.Platform.UseUpdateFromSyntax)
      {
        StringBuilder.AppendLine($"update { Connection.Escape(Update.SourceTable.Alias) }");
        StringBuilder.AppendLine("set");
      }
      else
      {
        StringBuilder.Append($"update { Connection.Escape(Update.SourceTable.Table.Base.Name) }");
        
        if (Connection.Platform.UseUpdateAliases)
          StringBuilder.Append($" as { Connection.Escape(Update.SourceTable.Alias) }");

        StringBuilder.AppendLine();
        StringBuilder.AppendLine("set");
      }

      var SetList = Update.Assignment.GetSets().ToDistinctList();
      foreach (var Set in SetList)
      {
        // TODO: This table alias prefix will need to match the column being set when multiple update tables are supported in the API.
        var SetText = $"{ Connection.Escape(Set.Column.Base.Name) } = { ExpressionFormatter.Format(Set.Parameter.Variable, Connection) }";

        if (Connection.Platform.UseUpdateAliases)
          SetText = $"{ Connection.Escape(Update.SourceTable.Alias) }.{ SetText }";

        if (Set != SetList.Last())
          SetText += ",";

        StringBuilder.AppendLine($"  { SetText }");
      }

      if (Connection.Platform.UseUpdateFromSyntax)
      {
        StringBuilder.AppendLine("from");

        StringBuilder.Append($"  { Connection.Escape(Update.SourceTable.Table.Base.Name) }");

        if (Connection.Platform.UseUpdateAliases)
          StringBuilder.Append($"as { Connection.Escape(Update.SourceTable.Alias) }");

        StringBuilder.AppendLine();
      }

      var WhereParameterList = new Inv.DistinctList<Inv.Database.Parameter>();
      if (Update.HasWhereExpression())
      {
        StringBuilder.AppendLine("where");
        StringBuilder.AppendLine("  " + ExpressionFormatter.Format(Update.GetWhereExpression(), Connection, WhereParameterList, Connection.Platform.UseUpdateAliases));
      }

      using (var Command = Connection.NewCommand())
      {
        var AssignmentParameterList = Update.Assignment.GetParameters().ToDistinctList();
        foreach (var Parameter in AssignmentParameterList)
        {
          Inv.Database.Engine.WriteTraceParameter(Parameter);
          Command.SetParameter(Parameter.Variable.Name, Parameter.Index, Parameter.ColumnType, Parameter.Value);
        }

        foreach (var Parameter in WhereParameterList)
        {
          Inv.Database.Engine.WriteTraceParameter(Parameter);
          Command.SetParameter(Parameter.Variable.Name, Parameter.Index + AssignmentParameterList.Count, Parameter.ColumnType, Parameter.Value);
        }

        var QueryText = StringBuilder.ToString();
        Inv.Database.Engine.WriteTrace(QueryText);
        Command.SetText(QueryText);

        AffectedRowCount = Command.ExecuteNonQuery();
      }
    }

    public int AffectedRowCount { get; private set; }
  }

  internal sealed class DeleteConductor<TTable>
    where TTable : Inv.Database.Contract<Inv.Database.Table>
  {
    internal DeleteConductor(Inv.Database.Connection Connection, Delete<TTable> Delete)
    {
      var StringBuilder = new System.Text.StringBuilder();

      if (Connection.Platform.UseUpdateFromSyntax)
      {
        StringBuilder.AppendLine($"delete { Connection.Escape(Delete.SourceTable.Alias) }");
        StringBuilder.AppendLine("from");
        StringBuilder.AppendLine($"  { Connection.Escape(Delete.SourceTable.Table.Base.Name) } as { Connection.Escape(Delete.SourceTable.Alias) }");
      }
      else
      {
        StringBuilder.AppendLine($"delete from { Connection.Escape(Delete.SourceTable.Table.Base.Name) }");

        //if (Connection.Platform.UseUpdateAliases)
        //  StringBuilder.Append($" as { Connection.Escape(Delete.SourceTable.Alias) }");
      }

      var WhereParameterList = new Inv.DistinctList<Inv.Database.Parameter>();
      if (Delete.HasWhereExpression())
      {
        StringBuilder.AppendLine("where");
        StringBuilder.AppendLine("  " + ExpressionFormatter.Format(Delete.GetWhereExpression(), Connection, WhereParameterList, Connection.Platform.UseUpdateFromSyntax));
      }

      using (var Command = Connection.NewCommand())
      {
        foreach (var Parameter in WhereParameterList)
        {
          Inv.Database.Engine.WriteTraceParameter(Parameter);
          Command.SetParameter(Parameter.Variable.Name, Parameter.Index, Parameter.ColumnType, Parameter.Value);
        }

        var QueryText = StringBuilder.ToString();
        Inv.Database.Engine.WriteTrace(QueryText);
        Command.SetText(QueryText);

        AffectedRowCount = Command.ExecuteNonQuery();
      }
    }

    public int AffectedRowCount { get; private set; }
  }

  internal sealed class SelectConductor
  {
    internal SelectConductor(Inv.Database.Connection Connection, Select Select)
    {
      var SelectClause = Select.Expression as SelectClause;

      if (SelectClause == null)
        throw new Exception("No select clause was provided.");

      var ParameterList = new Inv.DistinctList<Inv.Database.Parameter>();

      var QueryText = SelectClauseFormatter.Format(SelectClause, Connection, ParameterList);
      Inv.Database.Engine.WriteTrace(QueryText);

      var Command = Connection.NewCommand();
      Command.SetText(QueryText);

      foreach (var Parameter in ParameterList)
      {
        Inv.Database.Engine.WriteTraceParameter(Parameter);
        Command.SetParameter(Parameter.Variable.Name, Parameter.Index, Parameter.ColumnType, Parameter.Value);
      }

      // Command can't be disposed, as it will close the reader.
      this.Reader = new Inv.Database.SelectReader(Command, Command.ExecuteReader());
    }

    public Inv.Database.SelectReader Reader { get; private set; }
  }

  public sealed class SelectReader : IDisposable
  {
    internal SelectReader(Inv.Database.Command Command, Inv.Database.Reader Base)
    {
      this.Command = Command;
      this.Base = Base;
    }
    public void Dispose()
    {
      Command.Dispose();
      Base.Dispose();
    }

    public bool this[Target<LogicalExpression> Target]
    {
      get { return Base.GetBoolean(Target.Index); }
    }
    public int this[Target<Integer32Expression> Target]
    {
      get { return Base.GetInteger32(Target.Index); }
    }
    public string this[Target<StringExpression> Target]
    {
      get { return Base.GetString(Target.Index); }
    }

    public bool Read()
    {
      return Base.Read();
    }
    public bool IsNull(Target Target)
    {
      return Base.IsNull(Target.Index);
    }
    public bool IsNull(int ColumnIndex)
    {
      return Base.IsNull(ColumnIndex);
    }
    public bool GetBoolean(int ColumnIndex)
    {
      return Base.GetBoolean(ColumnIndex);
    }
    public byte GetByte(int ColumnIndex)
    {
      return Base.GetByte(ColumnIndex);
    }
    public Inv.Date GetDate(int ColumnIndex)
    {
      return Base.GetDate(ColumnIndex);
    }
    public DateTime GetDateTime(int ColumnIndex)
    {
      return Base.GetDateTime(ColumnIndex);
    }
    public DateTimeOffset GetDateTimeOffset(int ColumnIndex)
    {
      return Base.GetDateTimeOffset(ColumnIndex);
    }
    public decimal GetDecimal(int ColumnIndex)
    {
      return Base.GetDecimal(ColumnIndex);
    }
    public double GetDouble(int ColumnIndex)
    {
      return Base.GetDouble(ColumnIndex);
    }
    public float GetFloat(int ColumnIndex)
    {
      return Base.GetFloat(ColumnIndex);
    }
    public Guid GetGuid(int ColumnIndex)
    {
      return Base.GetGuid(ColumnIndex);
    }
    public Inv.Image GetImage(int ColumnIndex)
    {
      return Base.GetImage(ColumnIndex);
    }
    public int GetInteger32(int ColumnIndex)
    {
      return Base.GetInteger32(ColumnIndex);
    }
    public long GetInteger64(int ColumnIndex)
    {
      return Base.GetInteger64(ColumnIndex);
    }
    public Inv.Money GetMoney(int ColumnIndex)
    {
      return Base.GetMoney(ColumnIndex);
    }
    public Inv.Time GetTime(int ColumnIndex)
    {
      return Base.GetTime(ColumnIndex);
    }
    public TimeSpan GetTimeSpan(int ColumnIndex)
    {
      return Base.GetTimeSpan(ColumnIndex);
    }
    public string GetString(int ColumnIndex)
    {
      return Base.GetString(ColumnIndex);
    }

    private Inv.Database.Command Command;
    private Inv.Database.Reader Base;
  }

  internal static class SelectClauseFormatter
  {
    public static string Format(SelectClause SelectClause, Inv.Database.Connection Connection, Inv.DistinctList<Inv.Database.Parameter> ParameterList = null)
    {
      var StringBuilder = new System.Text.StringBuilder();

      string FormatSourceJoinType(SourceJoinType SourceJoinType)
      {
        switch (SourceJoinType)
        {
          case SourceJoinType.CrossJoin:
            return "cross join";

          case SourceJoinType.InnerJoin:
            return "inner join";

          case SourceJoinType.LeftOuterJoin:
            return "left outer join";

          default:
            throw new NotImplementedException(SourceJoinType.ToString());
        }
      }

      string FormatSource(Source Source)
      {
        var SourceJoin = Source as SourceJoin;
        if (SourceJoin != null)
        {
          var Result = FormatSource(SourceJoin.Left) + " " + FormatSourceJoinType(SourceJoin.Type) + " " + FormatSource(SourceJoin.Right);

          if (SourceJoin.Expression != null)
            Result += " on " + ExpressionFormatter.Format(SourceJoin.Expression, Connection, ParameterList);

          Debug.Assert(SourceJoin.Type == SourceJoinType.CrossJoin || SourceJoin.Expression != null, "Only cross joins are permitted to have no on clause");

          return Result;
        }

        var SourceTable = Source as SourceTable;
        if (SourceTable != null)
          return SourceTableFormatter.Format(SourceTable, Connection);

        var SourceSelect = Source as SourceSelect;
        if (SourceSelect != null)
        {
          var SourceSelectBuilder = new StringBuilder();
          SourceSelectBuilder.AppendLine("(");
          SourceSelectBuilder.AppendLine(SelectExpressionFormatter.Format(SourceSelect.SelectExpression, Connection, ParameterList));
          SourceSelectBuilder.AppendLine($") as { Connection.Escape(SourceSelect.Alias) }");

          return SourceSelectBuilder.ToString();
        }

        throw new Exception("Source not handled: " + Source.GetType().FullName);
      }

      StringBuilder.AppendLine("select" + (SelectClause.IsDistinct ? " distinct" : ""));

      var TopLimit = SelectClause.TopLimit;
      if (TopLimit != null && !Connection.Platform.UseLimitSyntax)
        StringBuilder.AppendLine("  top " + TopLimit.Value.ToString());

      var FirstTarget = true;
      foreach (var Target in SelectClause.GetTargets())
      {
        if (FirstTarget)
          FirstTarget = false;
        else
          StringBuilder.AppendLine(",");

        StringBuilder.Append("  " + ExpressionFormatter.Format(Target.Expression, Connection) + (Target.Alias != null ? $" as { Connection.Escape(Target.Alias) }" : ""));
      }

      StringBuilder.AppendLine();

      if (SelectClause.HasSources())
      {
        StringBuilder.AppendLine("from");

        var FirstSource = true;

        StringBuilder.Append("  ");

        foreach (var Source in SelectClause.GetSources())
        {
          if (FirstSource)
            FirstSource = false;
          else
            StringBuilder.Append(", ");

          StringBuilder.Append(FormatSource(Source));
        }

        StringBuilder.AppendLine();
      }

      if (SelectClause.HasWhereExpression())
      {
        StringBuilder.AppendLine("where");
        StringBuilder.AppendLine("  " + ExpressionFormatter.Format(SelectClause.GetWhereExpression(), Connection, ParameterList));
      }

      if (SelectClause.HasGroupBys())
      {
        StringBuilder.AppendLine("group by");

        var FirstGroupBy = true;

        StringBuilder.Append("  ");

        foreach (var GroupBy in SelectClause.GetGroupBys())
        {
          if (FirstGroupBy)
            FirstGroupBy = false;
          else
            StringBuilder.Append(", ");

          StringBuilder.Append(ExpressionFormatter.Format(GroupBy, Connection));
        }

        StringBuilder.AppendLine();
      }

      if (SelectClause.HasOrderBys())
      {
        StringBuilder.AppendLine("order by");

        var FirstOrderBy = true;

        StringBuilder.Append("  ");

        foreach (var OrderBy in SelectClause.GetOrderBys())
        {
          if (FirstOrderBy)
            FirstOrderBy = false;
          else
            StringBuilder.Append(", ");

          StringBuilder.Append(ExpressionFormatter.Format(OrderBy.Expression, Connection) + " " + (OrderBy.IsAscending ? "asc" : "desc"));
        }

        StringBuilder.AppendLine();
      }

      if (TopLimit != null && Connection.Platform.UseLimitSyntax)
        StringBuilder.AppendLine("  limit " + TopLimit.Value.ToString());

      return StringBuilder.ToString();
    }
  }

  internal static class SelectJoinFormatter
  {
    public static string Format(SelectJoin SelectJoin, Inv.Database.Connection Connection, Inv.DistinctList<Inv.Database.Parameter> ParameterList = null)
    {
      var StringBuilder = new StringBuilder();

      StringBuilder.Append(ExpressionFormatter.Format(SelectJoin.Left, Connection, ParameterList));
      StringBuilder.AppendLine();

      switch (SelectJoin.Type)
      {
        case SelectJoinType.Except:
          StringBuilder.Append("except");
          break;

        case SelectJoinType.Intersect:
          StringBuilder.Append("intersect");
          break;

        case SelectJoinType.Union:
          StringBuilder.Append("union");
          break;

        default:
          throw new Exception($"Unexpected SelectJoinType { SelectJoin.Type.ToString() }");
      }

      StringBuilder.AppendLine();
      StringBuilder.Append(ExpressionFormatter.Format(SelectJoin.Right, Connection, ParameterList));

      return StringBuilder.ToString();
    }
  }

  internal static class SelectExpressionFormatter
  {
    public static string Format(SelectExpression SelectExpression, Inv.Database.Connection Connection, Inv.DistinctList<Inv.Database.Parameter> ParameterList = null)
    {
      var SelectJoin = SelectExpression as SelectJoin;
      if (SelectJoin != null)
        return SelectJoinFormatter.Format(SelectJoin, Connection, ParameterList);

      var SelectClause = SelectExpression as SelectClause;
      if (SelectClause != null)
        return SelectClauseFormatter.Format(SelectClause, Connection, ParameterList);

      return "";
    }
  }

  internal static class SourceTableFormatter
  {
    public static string Format(SourceTable SourceTable, Inv.Database.Connection Connection)
    {
      return $"{ Connection.Escape(SourceTable.Table.Name) } as { Connection.Escape(SourceTable.Alias) }";
    }
  }

  internal static class ExpressionFormatter
  {
    public static string Format(Expression Expression, Inv.Database.Connection Connection, Inv.DistinctList<Inv.Database.Parameter> ParameterList = null, bool UseColumnAliases = true)
    {
      string FormatUnaryType(UnaryOperatorType UnaryType)
      {
        switch (UnaryType)
        {
          case UnaryOperatorType.LogicalNot:
            return "not";

          case UnaryOperatorType.LogicalIsNull:
            return "is null";

          case UnaryOperatorType.LogicalIsNotNull:
            return "is not null";

          default:
            throw new NotImplementedException(UnaryType.ToString());
        }
      }

      string FormatAggregateType(AggregateType AggregateType)
      {
        switch (AggregateType)
        {
          case AggregateType.Count:
            return "count";

          case AggregateType.CountDistinct:
            return "count";

          case AggregateType.Min:
            return "min";

          case AggregateType.Max:
            return "max";

          case AggregateType.Sum:
            return "sum";

          case AggregateType.Average:
            return "avg";

          default:
            throw new NotImplementedException(AggregateType.ToString());
        }
      }

      string FormatBinaryType(BinaryOperatorType BinaryType)
      {
        switch (BinaryType)
        {
          case BinaryOperatorType.ComparisonEqual:
            return "=";

          case BinaryOperatorType.ComparisonNotEqual:
            return "<>";

          case BinaryOperatorType.ComparisonGreaterThan:
            return ">";

          case BinaryOperatorType.ComparisonGreaterThanOrEqual:
            return ">=";

          case BinaryOperatorType.ComparisonLessThan:
            return "<";

          case BinaryOperatorType.ComparisonLessThanOrEqual:
            return "<=";

          case BinaryOperatorType.LogicalAnd:
            return "and";

          case BinaryOperatorType.LogicalOr:
            return "or";

          case BinaryOperatorType.LogicalIn:
            return "in";

          default:
            throw new NotImplementedException(BinaryType.ToString());
        }
      }

      string FormatLiteral(object Value)
      {
        if (ParameterList == null)
        {
          if (Value == null)
            return "null";
          else if (Value is string)
            return "'" + Value + "'";
          else if (Value is bool)
            return (bool)Value ? "1" : "0";
          else if (Value.GetType().GetTypeInfo().IsEnum)
            return ((int)(Value)).ToString();
          else if (Value is int || Value is long)
            return Value.ToString();

          throw new Exception($"Unsupported literal expression type [{ Value.GetType().Name }] - parameter needed.");
        }
        else
        {
          Inv.Database.ColumnType ColumnType;
          if (Value == null || Value is int)
            ColumnType = ColumnType.Integer32; // ColumnType isn't used in any meaningful way for nulls.
          else if (Value is long)
            ColumnType = ColumnType.Integer64;
          else if (Value is bool)
            ColumnType = ColumnType.Boolean;
          else if (Value is DateTimeOffset)
            ColumnType = ColumnType.DateTimeOffset;
          else if (Value is DateTime)
            ColumnType = ColumnType.DateTime;
          else if (Value is Inv.Date)
            ColumnType = ColumnType.Date;
          else if (Value is Inv.Time)
            ColumnType = ColumnType.Time;
          else if (Value is TimeSpan)
            ColumnType = ColumnType.TimeSpan;
          else if (Value is Inv.Money)
            ColumnType = ColumnType.Money;
          else if (Value is decimal)
            ColumnType = ColumnType.Decimal;
          else if (Value is double)
            ColumnType = ColumnType.Double;
          else if (Value.GetType().GetTypeInfo().IsEnum)
            ColumnType = ColumnType.Enum;
          else if (Value is Guid)
            ColumnType = ColumnType.Guid;
          else if (Value is string)
            ColumnType = ColumnType.String;
          else if (Value is Inv.Image)
            ColumnType = ColumnType.Image;
          else
            ColumnType = ColumnType.Binary;

          var ParameterIndex = ParameterList.Count;
          var ParameterVariable = Expression.Variable($"_L{ ParameterIndex.ToString() }");

          ParameterList.Add(new Inv.Database.Parameter(ParameterVariable, ParameterIndex, ColumnType, Value));

          return ExpressionFormatter.Format(ParameterVariable, Connection, null, UseColumnAliases);
        }
      }

      string FormatCollection(object[] ValueArray)
      {
        return "(" + ValueArray.Select(V => V.ToString()).AsSeparatedText(", ") + ")";
      }

      var Wrapper = Expression as WrapperExpression;
      if (Wrapper != null)
        return ExpressionFormatter.Format(Wrapper.Expression, Connection, ParameterList, UseColumnAliases);

      var Source = Expression as SourceColumnExpression;
      if (Source != null)
      {
        var Result = Connection.Escape(Source.Column.Name);

        if (UseColumnAliases)
          Result = Source.Source.Alias + "." + Result;

        return Result;
      }

      var SourceSelectField = Expression as SourceSelectField;
      if (SourceSelectField != null)
        return SourceSelectField.SourceSelect.Alias + "." + SourceSelectField.Alias;

      var Aggregate = Expression as AggregateExpression;
      if (Aggregate != null)
        return FormatAggregateType(Aggregate.Type) + "(" + (Aggregate.Type == AggregateType.CountDistinct ? "distinct " : "") + ExpressionFormatter.Format(Aggregate.Expression, Connection, ParameterList, UseColumnAliases) + ")";

      var Unary = Expression as UnaryOperatorExpression;
      if (Unary != null)
      {
        var UnaryTypeFormat = FormatUnaryType(Unary.Type);
        var UnaryExpression = ExpressionFormatter.Format(Unary.Expression, Connection, ParameterList, UseColumnAliases);

        return (Unary.Type.IsPrefix() ? UnaryTypeFormat + " " : "") + ExpressionFormatter.Format(Unary.Expression, Connection, ParameterList, UseColumnAliases) + (Unary.Type.IsPrefix() ? "" : " " + UnaryTypeFormat);
      }

      var Binary = Expression as BinaryOperatorExpression;
      if (Binary != null)
      {
        var BinaryResult = "";

        if (Binary.Left is LogicalExpression && ((LogicalExpression)Binary.Left).Expression is BinaryOperatorExpression)
          BinaryResult += "(" + ExpressionFormatter.Format(Binary.Left, Connection, ParameterList, UseColumnAliases) + ")";
        else
          BinaryResult += ExpressionFormatter.Format(Binary.Left, Connection, ParameterList, UseColumnAliases);

        BinaryResult += " " + FormatBinaryType(Binary.Type) + " ";

        if (Binary.Right is LogicalExpression && ((LogicalExpression)Binary.Right).Expression is BinaryOperatorExpression)
          BinaryResult += "(" + ExpressionFormatter.Format(Binary.Right, Connection, ParameterList, UseColumnAliases) + ")";
        else
          BinaryResult += ExpressionFormatter.Format(Binary.Right, Connection, ParameterList, UseColumnAliases);

        return BinaryResult;
      }

      var Variable = Expression as VariableExpression;
      if (Variable != null)
        return Variable.Name;

      var Literal = Expression as LiteralExpression;
      if (Literal != null)
        return FormatLiteral(Literal.Value);

      var Collection = Expression as CollectionExpression;
      if (Collection != null)
        return FormatCollection(Collection.ValueArray);

      var SourceSelectTarget = Expression as SourceSelectTargetExpression;
      if (SourceSelectTarget != null)
        return Connection.Escape(SourceSelectTarget.SourceAlias) + "." + Connection.Escape(SourceSelectTarget.TargetAlias);

      var DateFunction = Expression as AsDateFunctionExpression;
      if (DateFunction != null)
        return Connection.Platform.GetAsDateFunctionCall(ExpressionFormatter.Format(DateFunction.Base, Connection, ParameterList, UseColumnAliases));

      var DateAddFunction = Expression as DateAddFunctionExpression;
      if (DateAddFunction != null)
        return Connection.Platform.GetDateAddFunctionCall(ExpressionFormatter.Format(DateAddFunction.Base, Connection, ParameterList, UseColumnAliases), DateAddFunction.Interval, ExpressionFormatter.Format(DateAddFunction.Value, Connection, ParameterList, UseColumnAliases));

      var SelectClause = Expression as SelectClause;
      if (SelectClause != null)
        return SelectClauseFormatter.Format(SelectClause, Connection, ParameterList);

      throw new Exception("Expression not handled: " + Expression.GetType().FullName);
    }
  }

  internal static class ConnectionHelper
  {
    public static string Escape(this Inv.Database.Connection Connection, string Identifier)
    {
      return Connection.Platform.GetEscapeOpen() + Identifier + Connection.Platform.GetEscapeClose();
    }
    public static Inv.Database.Command NewCommand(this Inv.Database.Connection Connection)
    {
      var Result = Connection.NewCommand();
      Result.SetTimeout(60);
      return Result;
    }
    public static void ExecuteNonQuery(this Inv.Database.Connection Connection, string QueryText)
    {
      using (var Command = Connection.NewCommand())
      {
#if DEBUG
        Inv.Database.Engine.WriteTrace(QueryText);
#endif

        Command.SetText(QueryText);
        Command.ExecuteNonQuery();
      }
    }
  }

  public static class Syntax
  {
    public static string PrimaryKeyName(Inv.Database.PrimaryKeyColumn Column)
    {
      return "PK_" + Column.Table.Name + "_" + Column.Name;
    }
    public static string ForeignKeyName(Inv.Database.ForeignKeyColumn ForeignKeyColumn)
    {
      return "FK_" + ForeignKeyColumn.Table.Name + "_" + ForeignKeyColumn.Name;
    }
    public static string IndexName(Inv.Database.ForeignKeyColumn ForeignKeyColumn)
    {
      return "DK_" + ForeignKeyColumn.Table.Name + "_" + ForeignKeyColumn.Name;
    }
    public static string IndexName(Inv.Database.Index Index)
    {
      return (Index.IsUnique ? "U" : "D") + "K_" + Index.Table.Name + "_" + Index.GetName();
    }
  }

  public static class IntegrityEngine
  {
    public static void Execute
    (
      Inv.Database.Catalog ExpectedCatalog,
      Inv.Database.Catalog DetectedCatalog,
      out Inv.DistinctList<string> IssueList
    )
    {
      IssueList = new Inv.DistinctList<string>();

      // TODO: Should this be case-sensitive comparison for MySQL?
      foreach (var TableJoin in DetectedCatalog.TableList.FullOuterJoin(ExpectedCatalog.TableList, (A, B) => A.Name.Equals(B.Name, StringComparison.CurrentCultureIgnoreCase)))
      {
        var DetectedTable = TableJoin.Left;
        var ExpectedTable = TableJoin.Right;

        if (DetectedTable == null)
        {
          IssueList.Add($"Table { ExpectedTable.Name } was not found");
          continue;
        }

        if (ExpectedTable == null)
        {
          IssueList.Add($"Table { DetectedTable.Name } was not expected");
          continue;
        }

        foreach (var ColumnJoin in DetectedTable.ColumnList.FullOuterJoin(ExpectedTable.ColumnList, (A, B) => A.Name.Equals(B.Name, StringComparison.CurrentCultureIgnoreCase)))
        {
          var DetectedColumn = ColumnJoin.Left;
          var ExpectedColumn = ColumnJoin.Right;

          if (DetectedColumn == null)
          {
            IssueList.Add($"Column { DetectedTable.Name }.{ ExpectedColumn.Name } was not found");
            continue;
          }

          if (ExpectedColumn == null)
          {
            IssueList.Add($"Column { DetectedTable.Name }.{ DetectedColumn.Name } was not expected");
            continue;
          }

          if (!DetectedColumn.DataType.Equals(ExpectedColumn.DataType))
            IssueList.Add($"Column { DetectedTable.Name }.{ DetectedColumn.Name } expected datatype '{ ExpectedColumn.DataType }' but found '{ DetectedColumn.DataType }'");

          if (DetectedColumn.Nullable != ExpectedColumn.Nullable)
            IssueList.Add($"Column { DetectedTable.Name }.{ DetectedColumn.Name } expected nullability '{ ExpectedColumn.Nullable }' but found '{ DetectedColumn.Nullable }'");

          var DetectedForeignKey = DetectedColumn.ForeignKey;
          var ExpectedForeignKey = ExpectedColumn.ForeignKey;
          if (DetectedForeignKey != null && ExpectedForeignKey == null)
          {
            IssueList.Add($"Column { DetectedTable.Name }.{ DetectedColumn.Name } foreign key '{ DetectedForeignKey.Name }' was not expected");
          }
          else if (DetectedForeignKey == null && ExpectedForeignKey != null)
          {
            IssueList.Add($"Column { DetectedTable.Name }.{ DetectedColumn.Name } foreign key '{ ExpectedForeignKey.Name }' was not found");
          }
          else if (DetectedForeignKey != null && ExpectedForeignKey != null)
          {
            if (!DetectedForeignKey.Name.Equals(ExpectedForeignKey.Name))
              IssueList.Add($"Column { DetectedTable.Name }.{ DetectedColumn.Name } foreign key expected name '{ ExpectedForeignKey.Name }' but found '{ DetectedForeignKey.Name }'");

            if (!DetectedForeignKey.ReferenceTable.Name.Equals(ExpectedForeignKey.ReferenceTable.Name, StringComparison.CurrentCultureIgnoreCase))
              IssueList.Add($"Column { DetectedTable.Name }.{ DetectedColumn.Name } foreign key expected reference table '{ ExpectedForeignKey.ReferenceTable.Name }' but found '{ DetectedForeignKey.ReferenceTable.Name }'");

            if (!DetectedForeignKey.ReferenceColumn.Name.Equals(ExpectedForeignKey.ReferenceColumn.Name, StringComparison.CurrentCultureIgnoreCase))
              IssueList.Add($"Column { DetectedTable.Name }.{ DetectedColumn.Name } foreign key expected reference column '{ ExpectedForeignKey.ReferenceColumn.Name }' but found '{ DetectedForeignKey.ReferenceColumn.Name }'");
          }
        }

        foreach (var IndexJoin in DetectedTable.IndexList.FullOuterJoin(ExpectedTable.IndexList, (A, B) => A.Name.Equals(B.Name, StringComparison.CurrentCultureIgnoreCase)))
        {
          var DetectedIndex = IndexJoin.Left;
          var ExpectedIndex = IndexJoin.Right;

          if (DetectedIndex == null)
          {
            IssueList.Add($"Index { DetectedTable.Name }.{ ExpectedIndex.Name } was not found");
            continue;
          }

          if (ExpectedIndex == null)
          {
            IssueList.Add($"Index { DetectedTable.Name }.{ DetectedIndex.Name } was not expected");
            continue;
          }

          if (DetectedIndex.IsUnique != ExpectedIndex.IsUnique)
            IssueList.Add($"Index { DetectedTable.Name }.{ DetectedIndex.Name } expected uniqueness '{ ExpectedIndex.IsUnique }' but found '{ DetectedIndex.IsUnique }'");

          var IsColumnMatch = DetectedIndex.ColumnList.Count == ExpectedIndex.ColumnList.Count;
          if (IsColumnMatch)
          {
            for (var DetectedColumnIndex = 0; DetectedColumnIndex < DetectedIndex.ColumnList.Count; DetectedColumnIndex++)
            {
              var DetectedColumn = DetectedIndex.ColumnList[DetectedColumnIndex];
              var ExpectedColumn = ExpectedIndex.ColumnList[DetectedColumnIndex];

              if (!DetectedColumn.Name.Equals(ExpectedColumn.Name))
              {
                IsColumnMatch = false;
                break;
              }
            }
          }

          if (!IsColumnMatch)
            IssueList.Add($"Index { DetectedTable.Name }.{ DetectedIndex.Name } expected columns ({ ExpectedIndex.ColumnList.Select(C => C.Name).AsSeparatedText(", ") }) but found ({ DetectedIndex.ColumnList.Select(C => C.Name).AsSeparatedText(", ") })");
        }
      }
    }
  }
}