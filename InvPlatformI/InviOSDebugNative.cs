using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Foundation;
using UIKit;

#if DEBUG
namespace Inv
{
  public static class iOSDebugNative
  {
    public static void Run(params string[] args)
    {
      UIApplication.Main(args, null, "iOSDebugNativeAppDelegate");
    }
  }

  [Register("iOSDebugNativeAppDelegate")]
  internal class iOSDebugNativeAppDelegate : UIApplicationDelegate
  {
    public override UIWindow Window { get; set; }

    public override bool FinishedLaunching(UIApplication application, NSDictionary launchOptions)
    {
      Window = new UIWindow(UIScreen.MainScreen.Bounds);

      var iOSRootController = new UIKit.UINavigationController();
      Window.RootViewController = iOSRootController;
      iOSRootController.NavigationBarHidden = true;

      var LayoutController = NativeTests.Layouts();
      iOSRootController.PushViewController(LayoutController, false);

      Window.MakeKeyAndVisible();

      return true;
    }
  }

  internal static class NativeTests
  {
    public static UIViewController Layouts()
    {
      var Result = new iOSController();
      //Result.View.BackgroundColor = UIColor.Gray;
      Result.LoadEvent += () =>
      {
        Result.View.BackgroundColor = UIColor.Gray;

        var LogoImage = (UIImage)null;// UIKit.UIImage.LoadFromData(Foundation.NSData.FromArray(InvTest.Resources.Images.PhoenixLogo960x540.GetBuffer()), 3.0F);

        var Surface = new iOSSurface();
        Result.SetSurface(Surface);

        var OverlayContainer = new iOSContainer(null);
        Surface.SetContent(OverlayContainer);

        var Overlay = new iOSOverlay();
        OverlayContainer.SetContentElement(Overlay);

        var LogoContainer = new iOSContainer(null);
        LogoContainer.SetContentWidth(400);
        LogoContainer.SetContentHeight(400);
        LogoContainer.SetContentAlignment(iOSVertical.Stretch, iOSHorizontal.Left);

        var Logo = new iOSGraphic();
        LogoContainer.SetContentElement(Logo);
        Logo.Image = LogoImage;
        Logo.BackgroundColor = UIColor.Orange;

        var Container = new iOSContainer(null);
        Container.SetContentAlignment(iOSVertical.Stretch, iOSHorizontal.Center);

        Overlay.Compose(new[] { LogoContainer, Container });

        var Dock = new iOSDock();
        Container.SetContentElement(Dock);
        Dock.SetOrientation(iOSOrientation.Vertical);

        var Label1Container = new iOSContainer(null);
        Label1Container.SetContentMinimumHeight(100);
        Label1Container.LayoutMargins = new UIEdgeInsets(20, 20, 20, 20);
        //LabelContainer.SetContentVisiblity(false);

        var Label1 = new iOSLabel();
        Label1Container.SetContentElement(Label1);
        Label1.Text = "Label 1";
        Label1.BackgroundColor = UIColor.Green;
        Label1.LayoutMargins = new UIEdgeInsets(20, 20, 20, 20);

        var Label2Container = new iOSContainer(null);
        Label2Container.SetContentMinimumHeight(100);
        Label2Container.LayoutMargins = new UIEdgeInsets(20, 20, 20, 20);

        var Label2 = new iOSLabel();
        Label2Container.SetContentElement(Label2);
        Label2.Text = "Label 2";
        Label2.LayoutMargins = new UIEdgeInsets(20, 20, 20, 20);
        Label2.BackgroundColor = UIColor.Blue;

        var Label3Container = new iOSContainer(null);
        Label3Container.SetContentMinimumHeight(100);
        Label3Container.LayoutMargins = new UIEdgeInsets(20, 20, 20, 20);

        var Label3 = new iOSLabel();
        Label3Container.SetContentElement(Label3);
        Label3.Text = "Label 3";
        Label3.BackgroundColor = UIColor.Red;

        var Graphic1Container = new iOSContainer(null);

        var Graphic1 = new iOSGraphic();
        Graphic1Container.SetContentElement(Graphic1);
        Graphic1.Image = LogoImage;
        Graphic1.BackgroundColor = UIColor.Purple;

        Dock.Compose(new iOSContainer[] { Label1Container, Label2Container }, new iOSContainer[] { Graphic1Container }, new iOSContainer[] { Label3Container });

        var SideContainer = new iOSContainer(null);
        Overlay.Compose(new[] { SideContainer });
        SideContainer.LayoutMargins = new UIEdgeInsets(50, 50, 50, 50);

        var Scroll = new iOSScroll();
        SideContainer.SetContentElement(Scroll);
        SideContainer.SetContentAlignment(iOSVertical.Stretch, iOSHorizontal.Right);
        /*
        var Stack = new iOSStack();
        Scroll.SetContentElement(Stack);
        Stack.SetOrientation(iOSOrientation.Vertical);
        Scroll.BackgroundColor = UIColor.White;

        var Label4 = new iOSLabel();
        Label4.Text = "L4";
        Label4.Font = UIFont.SystemFontOfSize(250);
        Label4.BackgroundColor = UIColor.Green;

        var Label5 = new iOSLabel();
        Label5.Text = "L5";
        Label5.Font = UIFont.SystemFontOfSize(250);
        Label5.BackgroundColor = UIColor.Blue;

        var Label6 = new iOSLabel();
        Label6.Text = "L6";
        Label6.Font = UIFont.SystemFontOfSize(250);
        Label6.BackgroundColor = UIColor.Red;

        Stack.ComposeElements(new[] { Label4, Label5, Label6 });
        */
      };

      return Result;
    }
  }
}
#endif