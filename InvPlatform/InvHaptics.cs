﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Inv
{
  /// <summary>
  /// See <see cref="Application.Haptics"/>
  /// </summary>
  public sealed class Haptics
  {
    internal Haptics(Application Application)
    {
      this.Application = Application;
    }

    /// <summary>
    /// Informs if the current device supports haptic feedback.
    /// </summary>
    public bool IsSupported { get; internal set; }

    /// <summary>
    /// iOS: Triggers a light impact haptic feedback
    /// <para />
    /// AOSP: Triggers a ContextClick haptic feedback
    /// </summary>
    public void LightImpact()
    {
      Feedback(HapticFeedback.LightImpact);
    }
    /// <summary>
    /// iOS: Triggers a medium impact haptic feedback
    /// <para />
    /// AOSP: Triggers a KeyboardTap haptic feedback
    /// </summary>
    public void MediumImpact()
    {
      Feedback(HapticFeedback.MediumImpact);
    }
    /// <summary>
    /// iOS: Triggers a heavy impact haptic feedback
    /// <para />
    /// AOSP: Triggers a LongPress haptic feedback
    /// </summary>
    public void HeavyImpact()
    {
      Feedback(HapticFeedback.HeavyImpact);
    }
    /// <summary>
    /// Trigger haptic feedback with type specified by the parameter.
    /// </summary>
    /// <param name="Feedback"></param>
    public void Feedback(HapticFeedback Feedback)
    {
      Application.Platform.HapticFeedback(Feedback);
    }

    private readonly Application Application;
  }

  public enum HapticFeedback
  {
    LightImpact,
    MediumImpact,
    HeavyImpact
    // TODO: notifications, selection changed?
  }
}
