﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Diagnostics;
using Inv.Support;

namespace Inv
{
  [DataContract]
  public enum AnyType
  {
    [EnumMember]
    Integer8,
    [EnumMember]
    Integer16,
    [EnumMember]
    Integer32,
    [EnumMember]
    Integer64,
    [EnumMember]
    Boolean,
    [EnumMember]
    Character,
    [EnumMember]
    String,
    [EnumMember]
    Date,
    [EnumMember]
    Time,
    [EnumMember]
    DateTime,
    [EnumMember]
    DateTimeOffset,
    [EnumMember]
    TimeSpan,
    [EnumMember]
    RealSingle,
    [EnumMember]
    RealDouble,
    [EnumMember]
    Decimal,
    [EnumMember]
    DataSize,
    [EnumMember]
    Money,
    [EnumMember]
    Binary,
    [EnumMember]
    Image,
    [EnumMember]
    Sound,
    [EnumMember]
    Colour,
    [EnumMember]
    Xml
  }

  /// <summary>
  /// Primitive type variant class.
  /// </summary>
  [DataContract]
  public sealed class Any : IComparable, IComparable<Inv.Any>, IEquatable<Inv.Any>
  {
    [Obsolete("Cannot wrap an Any with another Any", true)]
    public Any(Inv.Any Value)
      : this()
    {
    }
    public Any(object Value)
      : this()
    {
      this.AsObject = Value;
    }
    public Any(byte Value)
      : this()
    {
      this.AsInteger8 = Value;
    }
    public Any(short Value)
      : this()
    {
      this.AsInteger16 = Value;
    }
    public Any(int Value)
      : this()
    {
      this.AsInteger32 = Value;
    }
    public Any(long Value)
      : this()
    {
      this.AsInteger64 = Value;
    }
    public Any(bool Value)
      : this()
    {
      this.AsBoolean = Value;
    }
    public Any(char Value)
      : this()
    {
      this.AsCharacter = Value;
    }
    public Any(string Value)
      : this()
    {
      this.AsString = Value;
    }
    public Any(Inv.Date Value)
      : this()
    {
      this.AsDate = Value;
    }
    public Any(Inv.Time Value)
      : this()
    {
      this.AsTime = Value;
    }
    public Any(DateTime Value)
      : this()
    {
      this.AsDateTime = Value;
    }
    public Any(DateTimeOffset Value)
      : this()
    {
      this.AsDateTimeOffset = Value;
    }
    public Any(TimeSpan Value)
      : this()
    {
      this.AsTimeSpan = Value;
    }
    public Any(float Value)
      : this()
    {
      this.AsRealSingle = Value;
    }
    public Any(double Value)
      : this()
    {
      this.AsRealDouble = Value;
    }
    public Any(decimal Value)
      : this()
    {
      this.AsDecimal = Value;
    }
    public Any(Inv.DataSize Value)
      : this()
    {
      this.AsDataSize = Value;
    }
    public Any(Inv.Money Value)
      : this()
    {
      this.AsMoney = Value;
    }
    public Any(Inv.Binary Value)
      : this()
    {
      this.AsBinary = Value;
    }
    public Any(Inv.Image Value)
      : this()
    {
      this.AsImage = Value;
    }
    public Any(Inv.Sound Value)
      : this()
    {
      this.AsSound = Value;
    }
    public Any(Inv.Colour Value)
      : this()
    {
      this.AsColour = Value;
    }
    public Any(Inv.Xml Value)
      : this()
    {
      this.AsXml = Value;
    }
    private Any()
    {
      this.Choice = new Choice<object, AnyType>(AnyTypeArray);
    }

    public AnyType? Type
    {
      get { return Choice.Index; }
    }
    public object AsObject
    {
      get { return Choice.Option; }
      set
      {
        if (value == null)
        {
          Choice.Index = null;
        }
        else
        {
          if (value is long)
            AsInteger64 = (long)value;
          else if (value is int)
            AsInteger32 = (int)value;
          else if (value is short)
            AsInteger16 = (short)value;
          else if (value is byte)
            AsInteger8 = (byte)value;
          else if (value is bool)
            AsBoolean = (bool)value;
          else if (value is char)
            AsCharacter = (char)value;
          else if (value is string)
            AsString = (string)value;
          else if (value is Inv.Date)
            AsDate = (Inv.Date)value;
          else if (value is Inv.Time)
            AsTime = (Inv.Time)value;
          else if (value is DateTime)
            AsDateTime = (DateTime)value;
          else if (value is DateTimeOffset)
            AsDateTimeOffset = (DateTimeOffset)value;
          else if (value is TimeSpan)
            AsTimeSpan = (TimeSpan)value;
          else if (value is float)
            AsRealSingle = (float)value;
          else if (value is double)
            AsRealDouble = (double)value;
          else if (value is decimal)
            AsDecimal = (decimal)value;
          else if (value is Inv.DataSize)
            AsDataSize = (Inv.DataSize)value;
          else if (value is Inv.Money)
            AsMoney = (Inv.Money)value;
          else if (value is Inv.Binary)
            AsBinary = (Inv.Binary)value;
          else if (value is Inv.Image)
            AsImage = (Inv.Image)value;
          else if (value is Inv.Sound)
            AsSound = (Inv.Sound)value;
          else if (value is Inv.Colour)
            AsColour = (Inv.Colour)value;
          else if (value is Inv.Xml)
            AsXml = (Inv.Xml)value;
          else
            throw new Exception("Value type not handled: " + value.GetType());
        }
      }
    }
    public long AsInteger
    {
      get
      {
        switch (Type)
        {
          case AnyType.Integer8:
            return AsInteger8;
          case AnyType.Integer16:
            return AsInteger16;
          case AnyType.Integer32:
            return AsInteger32;
          case AnyType.Integer64:
            return AsInteger64;

          default:
            throw new Exception("Inv.AnyType not handled.");
        }
      }
      set
      {
        if (value >= byte.MinValue && value <= byte.MaxValue)
          AsInteger8 = (byte)value;
        else if (value >= short.MinValue && value <= short.MaxValue)
          AsInteger16 = (short)value;
        else if (value >= int.MinValue && value <= int.MaxValue)
          AsInteger32 = (int)value;
        else
          AsInteger64 = value;
      }
    }
    public byte AsInteger8
    {
      get { return Choice.Retrieve<byte>(AnyType.Integer8); }
      set { Choice.Store(AnyType.Integer8, value); }
    }
    public short AsInteger16
    {
      get { return Choice.Retrieve<short>(AnyType.Integer16); }
      set { Choice.Store(AnyType.Integer16, value); }
    }
    public int AsInteger32
    {
      get { return Choice.Retrieve<int>(AnyType.Integer32); }
      set { Choice.Store(AnyType.Integer32, value); }
    }
    public long AsInteger64
    {
      get { return Choice.Retrieve<long>(AnyType.Integer64); }
      set { Choice.Store(AnyType.Integer64, value); }
    }
    public bool AsBoolean
    {
      get { return Choice.Retrieve<bool>(AnyType.Boolean); }
      set { Choice.Store(AnyType.Boolean, value); }
    }
    public char AsCharacter
    {
      get { return Choice.Retrieve<char>(AnyType.Character); }
      set { Choice.Store(AnyType.Character, value); }
    }
    public string AsString
    {
      get { return Choice.Retrieve<string>(AnyType.String); }
      set { Choice.Store(AnyType.String, value); }
    }
    public Inv.Date AsDate
    {
      get { return Choice.Retrieve<Inv.Date>(AnyType.Date); }
      set { Choice.Store(AnyType.Date, value); }
    }
    public Inv.Time AsTime
    {
      get { return Choice.Retrieve<Inv.Time>(AnyType.Time); }
      set { Choice.Store(AnyType.Time, value); }
    }
    public DateTime AsDateTime
    {
      get { return Choice.Retrieve<DateTime>(AnyType.DateTime); }
      set { Choice.Store(AnyType.DateTime, value); }
    }
    public DateTimeOffset AsDateTimeOffset
    {
      get { return Choice.Retrieve<DateTimeOffset>(AnyType.DateTimeOffset); }
      set { Choice.Store(AnyType.DateTimeOffset, value); }
    }
    public TimeSpan AsTimeSpan
    {
      get { return Choice.Retrieve<TimeSpan>(AnyType.TimeSpan); }
      set { Choice.Store(AnyType.TimeSpan, value); }
    }
    public float AsRealSingle
    {
      get { return Choice.Retrieve<float>(AnyType.RealSingle); }
      set { Choice.Store(AnyType.RealSingle, value); }
    }
    public double AsRealDouble
    {
      get { return Choice.Retrieve<double>(AnyType.RealDouble); }
      set { Choice.Store(AnyType.RealDouble, value); }
    }
    public decimal AsDecimal
    {
      get { return Choice.Retrieve<decimal>(AnyType.Decimal); }
      set { Choice.Store(AnyType.Decimal, value); }
    }
    public Inv.DataSize AsDataSize
    {
      get { return Choice.Retrieve<Inv.DataSize>(AnyType.DataSize); }
      set { Choice.Store(AnyType.DataSize, value); }
    }
    public Inv.Money AsMoney
    {
      get { return Choice.Retrieve<Inv.Money>(AnyType.Money); }
      set { Choice.Store(AnyType.Money, value); }
    }
    public Inv.Binary AsBinary
    {
      get { return Choice.Retrieve<Inv.Binary>(AnyType.Binary); }
      set { Choice.Store(AnyType.Binary, value); }
    }
    public Inv.Image AsImage
    {
      get { return Choice.Retrieve<Inv.Image>(AnyType.Image); }
      set { Choice.Store(AnyType.Image, value); }
    }
    public Inv.Sound AsSound
    {
      get { return Choice.Retrieve<Inv.Sound>(AnyType.Sound); }
      set { Choice.Store(AnyType.Sound, value); }
    }
    public Inv.Colour AsColour
    {
      get { return Choice.Retrieve<Inv.Colour>(AnyType.Colour); }
      set { Choice.Store(AnyType.Colour, value); }
    }
    public Inv.Xml AsXml
    {
      get { return Choice.Retrieve<Inv.Xml>(AnyType.Xml); }
      set { Choice.Store(AnyType.Xml, value); }
    }

    // FromObject and ToObject are used for SQLVariant support. These CAN result result in a lossy type change for the non-primitive types.
    // For instance, Inv.Date Inv.DateTime collapse to DateTime. So when restoring either from the database you will only get receive a Inv.DateTime.

    public static Inv.Any FromObject(object Value)
    {
      if (Value is int)
        return new Inv.Any((int)Value);
      else if (Value is string)
        return new Inv.Any((string)Value);
      else if (Value is long)
        return new Inv.Any((long)Value);
      else if (Value is byte)
        return new Inv.Any((byte)Value);
      else if (Value is short)
        return new Inv.Any((short)Value);
      else if (Value is decimal)
        return new Inv.Any((decimal)Value);
      else if (Value is double)
        return new Inv.Any((double)Value);
      else if (Value is float)
        return new Inv.Any((float)Value);
      else if (Value is bool)
        return new Inv.Any((bool)Value);
      else if (Value is char)
        return new Inv.Any((char)Value);
      else if (Value is Inv.Date)
        return new Inv.Any((Inv.Date)Value);
      else if (Value is DateTimeOffset)
        return new Inv.Any((DateTimeOffset)Value);
      else if (Value is DateTime)
        return new Inv.Any((DateTime)Value);
      else if (Value is TimeSpan)
        return new Inv.Any((TimeSpan)Value);
      else if (Value is Inv.Time)
        return new Inv.Any((Inv.Time)Value);
      else if (Value is byte[])
        return new Inv.Any((byte[])Value);
      else if (Value is Inv.Money)
        return new Inv.Any((Inv.Money)Value);
      else
        throw new NotImplementedException("Unimplemented AnyType:" + Value.GetType());
    }

    public object ToObject()
    {
      if (!Type.HasValue)
        return null;

      switch (Type.Value)
      {
        case AnyType.Boolean:
          return AsBoolean;

        case AnyType.Character:
          return AsCharacter;

        case AnyType.DataSize:
          return AsDataSize.TotalBytes;

        case AnyType.Date:
          return AsDate.ToDateTime();

        case AnyType.DateTime:
          return AsDateTime;

        case AnyType.Decimal:
          return AsDecimal;

        case AnyType.Integer8:
          return AsInteger8;

        case AnyType.Integer16:
          return AsInteger16;

        case AnyType.Integer32:
          return AsInteger32;

        case AnyType.Integer64:
          return AsInteger64;

        case AnyType.Money:
          return AsMoney.GetAmount();

        case AnyType.RealDouble:
          return AsRealDouble;

        case AnyType.RealSingle:
          return AsRealSingle;

        case AnyType.String:
          return AsString;

        case AnyType.Time:
          return AsTime.ToTimeSpan();

        case AnyType.TimeSpan:
          return AsTimeSpan;

        case AnyType.Binary:
          return AsBinary.GetBuffer();

        case AnyType.Colour:
          return AsColour.RawValue;

        case AnyType.Image:
          return AsImage.GetBuffer();

        case AnyType.Sound:
          return AsSound.GetBuffer();

        case AnyType.DateTimeOffset:
          //return AsDateTimeOffset.DateTime;
        case AnyType.Xml:
          throw new NotSupportedException("Unsupported AnyType:" + Type.Value);

        default:
          throw new NotImplementedException("Unimplemented AnyType:" + Type.Value);
      }
    }
    public int CompareTo(Inv.Any Value)
    {
      if (Value == null)
        return 1;

      if (Choice.Index == null && Value.Choice.Index == null)
        return 0;

      if (Choice.Index != null && Value.Choice.Index == null)
        return 1;

      if (Choice.Index == null && Value.Choice.Index != null)
        return -1;

      if (Choice.Index != Value.Choice.Index)
        return Choice.Index.Value.CompareTo(Value.Choice.Index.Value);

      switch (Type)
      {
        case AnyType.Integer8: 
          return AsInteger8.CompareTo(Value.AsInteger8);
        case AnyType.Integer16:
          return AsInteger16.CompareTo(Value.AsInteger16);
        case AnyType.Integer32:
          return AsInteger32.CompareTo(Value.AsInteger32);
        case AnyType.Integer64:
          return AsInteger64.CompareTo(Value.AsInteger64);
        case AnyType.Boolean:
          return AsBoolean.CompareTo(Value.AsBoolean);
        case AnyType.Character:
          return AsCharacter.CompareTo(Value.AsCharacter);
        case AnyType.String:
          return AsString.CompareTo(Value.AsString);
        case AnyType.Date:
          return AsDate.CompareTo(Value.AsDate);
        case AnyType.Time:
          return AsTime.CompareTo(Value.AsTime);
        case AnyType.DateTime:
          return AsDateTime.CompareTo(Value.AsDateTime);
        case AnyType.DateTimeOffset:
          return AsDateTimeOffset.CompareTo(Value.AsDateTimeOffset);
        case AnyType.TimeSpan:
          return AsTimeSpan.CompareTo(Value.AsTimeSpan);
        case AnyType.RealSingle:
          return AsRealSingle.CompareTo(Value.AsRealSingle);
        case AnyType.RealDouble:
          return AsRealDouble.CompareTo(Value.AsRealDouble);
        case AnyType.Decimal:
          return AsDecimal.CompareTo(Value.AsDecimal);
        case AnyType.Money:
          return AsMoney.CompareTo(Value.AsMoney);
        case AnyType.DataSize:
          return AsDataSize.CompareTo(Value.AsDataSize);
        case AnyType.Binary:
          return AsBinary.CompareTo(Value.AsBinary);
        case AnyType.Image:
          return AsImage.CompareTo(Value.AsImage);
        case AnyType.Sound:
          return AsSound.CompareTo(Value.AsSound);
        case AnyType.Colour:
          return AsColour.CompareTo(Value.AsColour);
        case AnyType.Xml:
          return AsXml.CompareTo(Value.AsXml);

        default:
          throw new Exception("Inv.AnyType not handled.");
      }
    }
    public bool EqualTo(Inv.Any value)
    {
      return CompareTo(value) == 0;
    }

    public override bool Equals(object value)
    {
      var Source = value as Inv.Any;

      return Source != null && EqualTo(Source);
    }
    public override int GetHashCode()
    {
      if (Choice.Option == null)
        return base.GetHashCode();
      else
        return Choice.Option.GetHashCode();
    }
    public override string ToString()
    {
      if (Choice.Index == null)
        return "";
      else
        return Choice.Option.ToString();
    }

    int IComparable.CompareTo(object obj)
    {
      return CompareTo((Inv.Any)obj);
    }
    int IComparable<Inv.Any>.CompareTo(Inv.Any other)
    {
      return CompareTo(other);
    }
    bool IEquatable<Inv.Any>.Equals(Inv.Any other)
    {
      return EqualTo(other);
    }

    [OnDeserialized]
    [DebuggerNonUserCode]
    private void OnDeserialized(StreamingContext c)
    {
      Choice.TypeArray = AnyTypeArray;
    }

    [DataMember]
    private Inv.Choice<object, AnyType> Choice;

    private static Type[] AnyTypeArray = new Type[]
    {
      typeof(byte), typeof(short), typeof(int), typeof(long), typeof(bool), typeof(char), typeof(string), typeof(Inv.Date), typeof(Inv.Time), typeof(DateTime), typeof(DateTimeOffset), typeof(TimeSpan), typeof(float), typeof(double), typeof(decimal), typeof(Inv.DataSize), typeof(Inv.Money), typeof(Inv.Binary), typeof(Inv.Image), typeof(Inv.Sound), typeof(Inv.Colour), typeof(Inv.Xml)
    };
  }

  public class AnyComparer : IEqualityComparer<Inv.Any>
  {
    bool IEqualityComparer<Inv.Any>.Equals(Inv.Any x, Inv.Any y)
    {
      return x.Equals(y);
    }
    int IEqualityComparer<Inv.Any>.GetHashCode(Inv.Any obj)
    {
      return obj.GetHashCode();
    }
  }
}
