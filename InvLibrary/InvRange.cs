﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  public interface IDiscrete
  {
    object PreviousValue();
    object NextValue();
  }

  public sealed class RangeSet<T> : IEnumerable<Inv.Range<T>>
    where T : struct, IComparable, IDiscrete
  {
    public RangeSet(IEnumerable<Inv.Range<T>> Ranges)
      : this(Ranges.ToArray())
    {
    }
    public RangeSet(params Inv.Range<T>[] RangeArray)
    {
      this.IntersectOnBoundary = true;

      var UnionSet = NewRangeSet();
      foreach (var Range in RangeArray)
        UnionSet = UnionSet.Union(Range);

      this.RangeList = UnionSet.RangeList;
    }

    public int Count
    {
      get { return RangeList.Count; }
    }
    public Inv.Range<T> FirstRange
    {
      get { return RangeList.FirstOrDefault(); }
    }
    public Inv.Range<T> LastRange
    {
      get { return RangeList.LastOrDefault(); }
    }
    public bool IntersectOnBoundary { get; set; }

    public bool IsEmpty()
    {
      return RangeList.Count == 0;
    }
    public bool IsUniversal()
    {
      return RangeList.Count == 1 && RangeList[0].From.Index == null && RangeList[0].Until.Index == null;
    }
    public bool IsEqualTo(Inv.RangeSet<T> CompareSet)
    {
      var Result = RangeList.Count == CompareSet.Count;

      if (Result)
      {
        for (var RangeIndex = 0; RangeIndex < RangeList.Count; RangeIndex++)
        {
          var Range = RangeList[RangeIndex];
          var CompareRange = CompareSet.RangeList[RangeIndex];

          Result =
            (Range.From.Index.HasValue == CompareRange.From.Index.HasValue) && (Range.From.Index == null || Range.From == CompareRange.From) &&
            (Range.Until.Index.HasValue == CompareRange.Until.Index.HasValue) && (Range.Until.Index == null || Range.Until == CompareRange.Until);

          if (!Result)
            break;
        }
      }

      return Result;
    }
    public bool Intersects(Inv.Range<T> Range)
    {
      var RangeSet = new Inv.RangeSet<T>(Range);

      return Intersect(RangeSet).Count > 0;
    }
    public bool Intersects(T? From, T? Until)
    {
      return Intersects(new Inv.Range<T>(From, Until));
    }
    public bool Includes(Inv.Range<T> Range)
    {
      var RangeSet = new Inv.RangeSet<T>(Range);

      return Intersect(RangeSet).IsEqualTo(RangeSet);
    }
    public bool Includes(T? From, T? Until)
    {
      return Includes(new Inv.Range<T>(From, Until));
    }
    public Inv.RangeSet<T> Invert()
    {
      if (IsEmpty())
      {
        return NewUniversalSet();
      }
      else if (IsUniversal())
      {
        return NewEmptySet();
      }
      else
      {
        var Result = NewRangeSet();

        if (FirstRange.From.Index != null)
        {
          var FirstValue = FirstRange.From.Index.Value;

          Result.RangeList.Add(NewRange(null, IntersectOnBoundary ? (T)FirstValue.PreviousValue() : FirstValue));
        }

        var LastField = FirstRange.Until;

        foreach (var Range in RangeList.Skip(1))
        {
          var FromValue = LastField.Index.Value;
          var UntilValue = Range.From.Index.Value;

          Result.RangeList.Add(NewRange(IntersectOnBoundary ? (T)FromValue.NextValue() : FromValue, IntersectOnBoundary ? (T)UntilValue.PreviousValue() : UntilValue));

          LastField = Range.Until;
        }

        if (LastField.Index != null)
        {
          var LastValue = LastField.Index.Value;

          Result.RangeList.Add(NewRange(IntersectOnBoundary ? (T)LastValue.NextValue() : LastValue, null));
        }

        Result.Verify();

        return Result;
      }
    }
    public Inv.RangeSet<T> Intersect(T? From, T? Until)
    {
      var Result = NewRangeSet();
      Result.RangeList.Add(NewRange(From, Until));

      return Intersect(Result);
    }
    public Inv.RangeSet<T> Intersect(Inv.Range<T> Range)
    {
      return Intersect(Range.From.Index, Range.Until.Index);
    }
    public Inv.RangeSet<T> Intersect(Inv.RangeSet<T> IntersectSet)
    {
      if (IntersectSet.IsEmpty() || IsEmpty())
      {
        return NewEmptySet();
      }
      else if (IntersectSet.IsUniversal())
      {
        return this;
      }
      else if (IsUniversal())
      {
        return IntersectSet;
      }
      else
      {
        var Result = NewRangeSet();

        var InputIndex = 0;
        var IntersectIndex = 0;
        while (InputIndex < RangeList.Count && IntersectIndex < IntersectSet.RangeList.Count)
        {
          var InputRange = RangeList[InputIndex];
          var IntersectRange = IntersectSet.RangeList[IntersectIndex];

          if (InputRange.From.Index == null && IntersectRange.From.Index == null)
          {
            T Until;

            if (InputRange.Until <= IntersectRange.Until)
            {
              Until = InputRange.Until.Index.Value;
              InputIndex++;
            }
            else
            {
              Until = IntersectRange.Until.Index.Value;
              IntersectIndex++;
            }

            Result.RangeList.Add(NewRange(null, Until));
          }
          else if (InputRange.Until.Index == null && IntersectRange.Until.Index == null)
          {
            T From;

            if (InputRange.From >= IntersectRange.From)
            {
              From = InputRange.From.Index.Value;
              IntersectIndex++;
            }
            else
            {
              From = IntersectRange.From.Index.Value;
              InputIndex++;
            }

            Result.RangeList.Add(NewRange(From, null));
          }
          else if (InputRange.From.Index == null && IntersectRange.Until.Index == null) // InputRange.Until != null, IntersectRange.From != null
          {
            if (IntersectRange.From < InputRange.Until || (IntersectOnBoundary && IntersectRange.From == InputRange.Until))
              Result.RangeList.Add(NewRange(IntersectRange.From.Index, InputRange.Until.Index));

            InputIndex++;
          }
          else if (InputRange.Until.Index == null && IntersectRange.From.Index == null) // InputRange.From != null, IntersectRange.Until != null
          {
            if (InputRange.From < IntersectRange.Until || (IntersectOnBoundary && InputRange.From == IntersectRange.Until))
              Result.RangeList.Add(NewRange(InputRange.From.Index, IntersectRange.Until.Index));

            IntersectIndex++;
          }
          else if (InputRange.Until.Index == null) // InputRange.From != null, IntersectRange.From != null, IntersectRange.Until != null
          {
            if (IntersectRange.Until < InputRange.From || (!IntersectOnBoundary && IntersectRange.Until == InputRange.From))
            {
              IntersectIndex++;
            }
            else
            {
              Result.RangeList.Add(NewRange(Max(IntersectRange.From, InputRange.From).Index, IntersectRange.Until.Index));

              IntersectIndex++;
            }
          }
          else if (InputRange.From.Index == null) // InputRange.Until != null, IntersectRange.From != null, IntersectRange.Until != null
          {
            if (IntersectRange.From > InputRange.Until || (!IntersectOnBoundary && IntersectRange.From == InputRange.Until))
            {
              InputIndex++;
            }
            else
            {
              T Until;

              if (IntersectRange.Until <= InputRange.Until)
              {
                Until = IntersectRange.Until.Index.Value;
                IntersectIndex++;

                if (IntersectRange.Until == InputRange.Until)
                  InputIndex++;
              }
              else
              {
                Until = InputRange.Until.Index.Value;
                InputIndex++;
              }

              Result.RangeList.Add(NewRange(IntersectRange.From.Index, Until));
            }
          }
          else if (IntersectRange.Until.Index == null) // InputRange.From != null, InputRange.Until != null, IntersectRange.From != null
          {
            if (InputRange.Until < IntersectRange.From || (!IntersectOnBoundary && IntersectRange.From == InputRange.Until))
            {
              InputIndex++;
            }
            else
            {
              Result.RangeList.Add(NewRange(Max(InputRange.From, IntersectRange.From).Index, InputRange.Until.Index));

              InputIndex++;
            }
          }
          else if (IntersectRange.From.Index == null) // InputRange.From != null, InputRange.Until != null, IntersectRange.Until != null
          {
            if (InputRange.From > IntersectRange.Until || (!IntersectOnBoundary && InputRange.From == IntersectRange.Until))
            {
              IntersectIndex++;
            }
            else
            {
              T Until;

              if (InputRange.Until <= IntersectRange.Until)
              {
                Until = InputRange.Until.Index.Value;
                InputIndex++;

                if (InputRange.Until == IntersectRange.Until)
                  IntersectIndex++;
              }
              else
              {
                Until = IntersectRange.Until.Index.Value;
                IntersectIndex++;
              }

              Result.RangeList.Add(NewRange(InputRange.From.Index, Until));
            }
          }
          else if (InputRange.From > IntersectRange.Until || (!IntersectOnBoundary && InputRange.From == IntersectRange.Until))
          {
            IntersectIndex++;
          }
          else if (IntersectRange.From > InputRange.Until || (!IntersectOnBoundary && IntersectRange.From == InputRange.Until))
          {
            InputIndex++;
          }
          else if (InputRange.From >= IntersectRange.From && InputRange.From <= IntersectRange.Until)
          {
            T Until;

            if (InputRange.Until <= IntersectRange.Until)
            {
              Until = InputRange.Until.Index.Value;
              InputIndex++;
            }
            else
            {
              Until = IntersectRange.Until.Index.Value;
              IntersectIndex++;
            }

            Result.RangeList.Add(NewRange(InputRange.From.Index, Until));
          }
          else if (IntersectRange.From >= InputRange.From && IntersectRange.From <= InputRange.Until)
          {
            T Until;

            if (IntersectRange.Until <= InputRange.Until)
            {
              Until = IntersectRange.Until.Index.Value;
              IntersectIndex++;
            }
            else
            {
              Until = InputRange.Until.Index.Value;
              InputIndex++;
            }

            Result.RangeList.Add(NewRange(IntersectRange.From.Index, Until));
          }
          else
          {
            throw new Exception("Unexpected algorithm path: RangeSet<T>.Intersect.");
          }
        }

        Result.Verify();

        return Result;
      }
    }
    public Inv.RangeSet<T> Union(T? From, T? Until)
    {
      var Result = NewRangeSet();
      Result.RangeList.Add(NewRange(From, Until));

      return Union(Result);
    }
    public Inv.RangeSet<T> Union(Inv.Range<T> Range)
    {
      return Union(Range.From.Index, Range.Until.Index);
    }
    public Inv.RangeSet<T> Union(Inv.RangeSet<T> UnionSet)
    {
      if (IsEmpty())
      {
        return UnionSet;
      }
      else if (UnionSet.IsEmpty())
      {
        return this;
      }
      else if (IsUniversal() || UnionSet.IsUniversal())
      {
        return NewUniversalSet();
      }
      else
      {
        var Result = NewRangeSet();

        var InputIndex = 0;
        var UnionIndex = 0;
        while (InputIndex < RangeList.Count || UnionIndex < UnionSet.RangeList.Count)
        {
          var InputRange = RangeList.ExistsAt(InputIndex) ? RangeList[InputIndex] : null;
          var UnionRange = UnionSet.RangeList.ExistsAt(UnionIndex) ? UnionSet.RangeList[UnionIndex] : null;

          if (UnionRange == null)
          {
            Result.RangeList.Add(NewRange(InputRange.From.Index, InputRange.Until.Index));

            InputIndex++;
          }
          else if (InputRange == null)
          {
            Result.RangeList.Add(NewRange(UnionRange.From.Index, UnionRange.Until.Index));

            UnionIndex++;
          }
          else if (InputRange.From.Index == null && UnionRange.From.Index == null)
          {
            if (InputRange.Until == UnionRange.Until)
            {
              Result.RangeList.Add(NewRange(null, InputRange.Until.Index));

              UnionIndex++;
              InputIndex++;
            }
            else if (InputRange.Until >= UnionRange.Until)
            {
              UnionIndex++;
            }
            else if (InputRange.Until <= UnionRange.Until)
            {
              InputIndex++;
            }
          }
          else if (InputRange.Until.Index == null && UnionRange.Until.Index == null)
          {
            Result.RangeList.Add(NewRange(Min(InputRange.From, UnionRange.From).Index, null));

            break;
          }
          else if (InputRange.From.Index == null && UnionRange.Until.Index == null) // InputRange.Until != null, UnionRange.From != null
          {
            if (InputRange.Until < UnionRange.From)
            {
              Result.RangeList.Add(NewRange(null, InputRange.Until.Index));

              InputIndex++;
            }
            else
            {
              Result.RangeList.Add(NewRange(null, null));

              break;
            }
          }
          else if (InputRange.Until.Index == null && UnionRange.From.Index == null) // InputRange.From != null, UnionRange.Until != null
          {
            if (UnionRange.Until < InputRange.From)
            {
              Result.RangeList.Add(NewRange(null, UnionRange.Until.Index));

              UnionIndex++;
            }
            else
            {
              Result.RangeList.Add(NewRange(null, null));

              break;
            }
          }
          else if (InputRange.Until.Index == null) // InputRange.From != null, UnionRange.From != null, UnionRange.Until != null
          {
            if (UnionRange.Until < InputRange.From.Previous())
            {
              Result.RangeList.Add(NewRange(UnionRange.From.Index, UnionRange.Until.Index));

              UnionIndex++;
            }
            else
            {
              Result.RangeList.Add(NewRange(Min(InputRange.From, UnionRange.From).Index, null));

              break;
            }
          }
          else if (InputRange.From.Index == null) // InputRange.Until != null, UnionRange.From != null, UnionRange.Until != null
          {
            if (UnionRange.From > InputRange.Until)
            {
              Result.RangeList.Add(NewRange(null, InputRange.Until.Index));

              InputIndex++;
            }
            else if (UnionRange.Until == InputRange.Until)
            {
              Result.RangeList.Add(NewRange(null, InputRange.Until.Index));

              UnionIndex++;
              InputIndex++;
            }
            else
            {
              bool IterateInputList;
              Inv.RangeField<T> Until;

              if (InputRange.Until > UnionRange.Until)
              {
                Until = InputRange.Until;

                InputIndex++;
                IterateInputList = false;
              }
              else
              {
                Until = UnionRange.Until;

                UnionIndex++;
                IterateInputList = true;
              }

              while (IterateInputList ? InputIndex < RangeList.Count : UnionIndex < UnionSet.RangeList.Count)
              {
                var CurrentRange = IterateInputList ? RangeList[InputIndex] : UnionSet.RangeList[UnionIndex];

                if (CurrentRange.From <= Until)
                {
                  if (CurrentRange.Until.Index == null)
                  {
                    Until = new Inv.RangeField<T>(null);

                    break;
                  }
                  else if (CurrentRange.Until < Until)
                  {
                    if (IterateInputList)
                      InputIndex++;
                    else
                      UnionIndex++;
                  }
                  else if (CurrentRange.Until == Until)
                  {
                    InputIndex++;
                    UnionIndex++;

                    break;
                  }
                  else
                  {
                    Until = CurrentRange.Until;

                    IterateInputList = !IterateInputList;

                    if (IterateInputList)
                      InputIndex++;
                    else
                      UnionIndex++;
                  }
                }
                else
                {
                  if (IterateInputList)
                    UnionIndex++;
                  else
                    InputIndex++;

                  break;
                }
              }

              if (IterateInputList && InputIndex >= RangeList.Count)
                UnionIndex++;
              else if (!IterateInputList && UnionIndex >= UnionSet.RangeList.Count)
                InputIndex++;

              Result.RangeList.Add(NewRange(null, Until.Index));

              if (Until.Index == null)
                break;
            }
          }
          else if (UnionRange.Until.Index == null) // InputRange.From != null, InputRange.Until != null, UnionRange.From != null
          {
            if (InputRange.Until < UnionRange.From.Previous())
            {
              Result.RangeList.Add(NewRange(InputRange.From.Index, InputRange.Until.Index));

              InputIndex++;
            }
            else
            {
              Result.RangeList.Add(NewRange(Min(InputRange.From, UnionRange.From).Index, null));

              break;
            }
          }
          else if (UnionRange.From.Index == null) // InputRange.From != null, InputRange.Until != null, UnionRange.Until != null
          {
            if (InputRange.From > UnionRange.Until)
            {
              Result.RangeList.Add(NewRange(null, UnionRange.Until.Index));

              UnionIndex++;
            }
            else if (InputRange.Until == UnionRange.Until)
            {
              Result.RangeList.Add(NewRange(null, UnionRange.Until.Index));

              InputIndex++;
              UnionIndex++;
            }
            else
            {
              bool IterateUnionList;
              Inv.RangeField<T> Until;

              if (InputRange.Until > UnionRange.Until)
              {
                Until = InputRange.Until;

                InputIndex++;
                IterateUnionList = true;
              }
              else
              {
                Until = UnionRange.Until;

                UnionIndex++;
                IterateUnionList = false;
              }

              while (IterateUnionList ? UnionIndex < UnionSet.RangeList.Count : InputIndex < RangeList.Count)
              {
                var CurrentRange = IterateUnionList ? UnionSet.RangeList[UnionIndex] : RangeList[InputIndex];

                if (CurrentRange.From <= Until)
                {
                  if (CurrentRange.Until.Index == null)
                  {
                    Until = new Inv.RangeField<T>(null);

                    break;
                  }
                  else if (CurrentRange.Until < Until)
                  {
                    if (IterateUnionList)
                      UnionIndex++;
                    else
                      InputIndex++;
                  }
                  else if (CurrentRange.Until == Until)
                  {
                    UnionIndex++;
                    InputIndex++;

                    break;
                  }
                  else
                  {
                    Until = CurrentRange.Until;

                    IterateUnionList = !IterateUnionList;

                    if (IterateUnionList)
                      UnionIndex++;
                    else
                      InputIndex++;
                  }
                }
                else
                {
                  if (IterateUnionList)
                    InputIndex++;
                  else
                    UnionIndex++;

                  break;
                }
              }

              if (IterateUnionList && UnionIndex >= UnionSet.RangeList.Count)
                InputIndex++;
              else if (!IterateUnionList && InputIndex >= RangeList.Count)
                UnionIndex++;

              Result.RangeList.Add(NewRange(null, Until.Index));

              if (Until.Index == null)
                break;
            }
          }
          else if (InputRange.From > UnionRange.Until.Next())
          {
            Result.RangeList.Add(NewRange(UnionRange.From.Index, UnionRange.Until.Index));

            UnionIndex++;
          }
          else if (UnionRange.From > InputRange.Until.Next())
          {
            Result.RangeList.Add(NewRange(InputRange.From.Index, InputRange.Until.Index));

            InputIndex++;
          }
          else
          {
            var From = Min(InputRange.From, UnionRange.From).Index;

            Inv.RangeField<T> Until;

            if (InputRange.Until == UnionRange.Until)
            {
              Until = UnionRange.Until;

              InputIndex++;
              UnionIndex++;
            }
            else
            {
              bool IterateUnionList;

              if (InputRange.Until > UnionRange.Until)
              {
                Until = InputRange.Until;

                InputIndex++;
                IterateUnionList = true;
              }
              else
              {
                Until = UnionRange.Until;

                UnionIndex++;
                IterateUnionList = false;
              }

              while (IterateUnionList ? UnionIndex < UnionSet.RangeList.Count : InputIndex < RangeList.Count)
              {
                var CurrentRange = IterateUnionList ? UnionSet.RangeList[UnionIndex] : RangeList[InputIndex];

                if (CurrentRange.From <= Until.Next())
                {
                  if (CurrentRange.Until.Index == null)
                  {
                    Until = new Inv.RangeField<T>(null);

                    break;
                  }
                  else if (CurrentRange.Until < Until)
                  {
                    if (IterateUnionList)
                      UnionIndex++;
                    else
                      InputIndex++;
                  }
                  else if (CurrentRange.Until == Until)
                  {
                    UnionIndex++;
                    InputIndex++;

                    break;
                  }
                  else
                  {
                    Until = CurrentRange.Until;

                    if (IterateUnionList)
                      UnionIndex++;
                    else
                      InputIndex++;

                    IterateUnionList = !IterateUnionList;
                  }
                }
                else
                {
                  if (IterateUnionList)
                    InputIndex++;
                  else
                    UnionIndex++;

                  break;
                }
              }
            }

            Result.RangeList.Add(NewRange(From, Until.Index));

            if (Until.Index == null)
              break;
          }
        }

        Result.Verify();

        return Result;
      }
    }
    public Inv.RangeSet<T> Subtract(T? From, T? Until)
    {
      var Result = NewRangeSet();
      Result.RangeList.Add(NewRange(From, Until));

      return Subtract(Result);
    }
    public Inv.RangeSet<T> Subtract(Inv.RangeSet<T> SubtractSet)
    {
      if (IsEmpty() || SubtractSet.IsEmpty())
        return this;
      else if (SubtractSet.IsUniversal())
        return NewEmptySet();
      else
        return SubtractSet.Invert().Intersect(this);
    }

    private RangeSet()
    {
      this.RangeList = new Inv.DistinctList<Inv.Range<T>>();

      // NOTE: order of periods must be:
      //
      // (FROM, UNTIL)
      // #1 (null, A)
      // #2 (B, C)
      // #3 (D, null)
      //
    }
    private Inv.RangeSet<T> NewRangeSet()
    {
      var Result = new Inv.RangeSet<T>();
      Result.IntersectOnBoundary = IntersectOnBoundary;

      return Result;
    }
    private Inv.RangeSet<T> NewEmptySet()
    {
      return NewRangeSet();
    }
    private Inv.RangeSet<T> NewUniversalSet()
    {
      var Result = new Inv.RangeSet<T>(new Inv.Range<T>(null, null));
      Result.IntersectOnBoundary = IntersectOnBoundary;

      return Result;
    }

    [Conditional("DEBUG")]
    private void Verify()
    {
      if (RangeList.Count > 0)
      {
        var ErrorList = new Inv.DistinctList<string>();

        for (var RangeIndex = 0; RangeIndex < RangeList.Count; RangeIndex++)
        {
          var Range = RangeList[RangeIndex];

          if (Range.From.Index != null && Range.Until.Index != null && Range.From > Range.Until)
            ErrorList.Add(string.Format("Entry {0} from {1} must be earlier than or equal to until {2}.", RangeIndex, Range.From, Range.Until));

          if (Range.From.Index == null && RangeIndex != 0)
            ErrorList.Add("Null from must be first entry in list.");

          if (Range.Until.Index == null && RangeIndex != RangeList.Count - 1)
            ErrorList.Add("Null until must be last entry in list.");
        }

        if (ErrorList.Count > 0)
          throw new Exception(ErrorList.AsSeparatedText("\r\n"));

        var PreviousRange = RangeList[0];

        var IsOrdered = true;
        var TimeRangeIndex = 1;
        while (IsOrdered && TimeRangeIndex < RangeList.Count)
        {
          var CurrentRange = RangeList[TimeRangeIndex];

          IsOrdered = (CurrentRange.From != null) && (PreviousRange.Until != null) && PreviousRange.Until < CurrentRange.From;

          PreviousRange = CurrentRange;

          TimeRangeIndex++;
        }

        if (!IsOrdered)
          throw new Exception(string.Format("Entries must be in ascending order without any overlap: {0}", RangeList.Select(R => "(" + R.From.ToString() + "-" + R.Until.ToString() + ")").AsSeparatedText(", ")));
      }
    }

    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      return RangeList.GetEnumerator();
    }
    System.Collections.Generic.IEnumerator<Inv.Range<T>> System.Collections.Generic.IEnumerable<Inv.Range<T>>.GetEnumerator()
    {
      return RangeList.GetEnumerator();
    }

    private Inv.DistinctList<Inv.Range<T>> RangeList;

    private static Inv.RangeField<T> Min(Inv.RangeField<T> A, Inv.RangeField<T> B)
    {
      if (A < B)
        return A;
      else
        return B;
    }
    private static Inv.RangeField<T> Max(Inv.RangeField<T> A, Inv.RangeField<T> B)
    {
      if (A > B)
        return A;
      else
        return B;
    }
    private static Inv.Range<T> NewRange(T? From, T? Until)
    {
      return new Inv.Range<T>(From, Until);
    }
  }

  public sealed class Range<T>
     where T : struct, IComparable, IDiscrete
  {
    public Range(T? FromValue, T? UntilValue)
    {
      this.From = new Inv.RangeField<T>(FromValue);
      this.Until = new Inv.RangeField<T>(UntilValue);

      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(From.Index == null || Until.Index == null || From <= Until, "From must be less than or equal to Until.");
    }

    public Inv.RangeField<T> From { get; private set; }
    public Inv.RangeField<T> Until { get; private set; }

    public override bool Equals(object obj)
    {
      var Source = obj as Range<T>;

      if (Source == null)
        return false;
      else
        return From.Equals(Source.From) && Until.Equals(Source.Until);
    }
    public override int GetHashCode()
    {
      return From.GetHashCode() ^ Until.GetHashCode();
    }
    public override string ToString()
    {
      return From + " - " + Until;
    }

    public bool Overlaps(Inv.Range<T> Range)
    {
      return (From == Range.From) || (From > Range.From ? From < Range.Until : Range.From < Until);
    }
  }

  public struct RangeField<T>
    where T : struct, IComparable, IDiscrete
  {
    public RangeField(T? IndexValue)
      : this()
    {
      Index = IndexValue;
    }

    public T? Index { get; private set; }

    public bool EqualTo(T Value)
    {
      return Compare(this, new RangeField<T>(Value)) == 0;
    }
    public int CompareTo(T Value)
    {
      return Compare(this, new RangeField<T>(Value));
    }
    public RangeField<T> Previous()
    {
      return new Inv.RangeField<T>((T)Index.Value.PreviousValue());
    }
    public RangeField<T> Next()
    {
      return new Inv.RangeField<T>((T)Index.Value.NextValue());
    }

    public override bool Equals(object obj)
    {
      if (obj == null || obj is RangeField<T>)
        return Compare(this, (RangeField<T>)obj) == 0;
      else
        return false;
    }
    public override int GetHashCode()
    {
      return Index.GetHashCode();
    }
    public override string ToString()
    {
      return Index.ToString();
    }

    public static bool operator ==(RangeField<T> Left, RangeField<T> Right)
    {
      return Compare(Left, Right) == 0;
    }
    public static bool operator ==(RangeField<T> Left, T? Right)
    {
      return Compare(Left, Right) == 0;
    }
    public static bool operator !=(RangeField<T> Left, T? Right)
    {
      return Compare(Left, Right) != 0;
    }
    public static bool operator !=(RangeField<T> Left, RangeField<T> Right)
    {
      return Compare(Left, Right) != 0;
    }
    public static bool operator <=(RangeField<T> Left, RangeField<T> Right)
    {
      return Compare(Left, Right) <= 0;
    }
    public static bool operator >=(RangeField<T> Left, RangeField<T> Right)
    {
      return Compare(Left, Right) >= 0;
    }
    public static bool operator <(RangeField<T> Left, RangeField<T> Right)
    {
      return Compare(Left, Right) < 0;
    }
    public static bool operator >(RangeField<T> Left, RangeField<T> Right)
    {
      return Compare(Left, Right) > 0;
    }

    private static int Compare(RangeField<T> Left, RangeField<T> Right)
    {
      return Compare(Left.Index, Right.Index);
    }
    private static int Compare(T? Left, RangeField<T> Right)
    {
      return Compare(Left, Right.Index);
    }
    private static int Compare(RangeField<T> Left, T? Right)
    {
      return Compare(Left.Index, Right);
    }
    private static int Compare(T? Left, T? Right)
    {
      if (Left == null && Right == null)
        return 0;
      else if (Left == null)
        return -1;
      else if (Right == null)
        return +1;
      else
        return ((IComparable)Left.Value).CompareTo(Right.Value);
    }
  }
}