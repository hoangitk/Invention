﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InvMoney
{
  [TestClass]
  public class TruncateTests
  {
    [TestMethod]
    public void ToZero1()
    {
      var RawValue = 0.000001M;
      Assert.AreEqual(0M, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void ToZero2()
    {
      var RawValue = -0.000001M;
      Assert.AreEqual(0M, new Inv.Money(RawValue).GetAmount());
    }

    [TestMethod]
    public void Overflow1()
    {
      var RawValue = 9999999999999999999999999999M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Overflow2()
    {
      var RawValue = -9999999999999999999999999999M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }

    [TestMethod]
    public void To4DP1()
    {
      var RawValue = 10.03492M;
      Assert.AreEqual(10.0349M, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void To4DP2()
    {
      var RawValue = -10.03492M;
      Assert.AreEqual(-10.0349M, new Inv.Money(RawValue).GetAmount());
    }

    [TestMethod]
    public void Unchanged1()
    {
      var RawValue = 10.034M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged2()
    {
      var RawValue = -10.034M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged3()
    {
      var RawValue = 10.0341M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged4()
    {
      var RawValue = 10.0342M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged5()
    {
      var RawValue = 10.0343M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged6()
    {
      var RawValue = 10.0344M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged7()
    {
      var RawValue = 10.0345M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged8()
    {
      var RawValue = 10.0346M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged9()
    {
      var RawValue = 10.0347M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged10()
    {
      var RawValue = 10.0348M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged11()
    {
      var RawValue = 10.0349M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged12()
    {
      var RawValue = 10.0340M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged13()
    {
      var RawValue = -10.0341M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged14()
    {
      var RawValue = -10.0342M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged15()
    {
      var RawValue = -10.0343M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged16()
    {
      var RawValue = -10.0344M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged17()
    {
      var RawValue = -10.0345M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged18()
    {
      var RawValue = -10.0346M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged19()
    {
      var RawValue = -10.0347M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged20()
    {
      var RawValue = -10.0348M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged21()
    {
      var RawValue = -10.0349M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }
    [TestMethod]
    public void Unchanged22()
    {
      var RawValue = -10.0340M;
      Assert.AreEqual(RawValue, new Inv.Money(RawValue).GetAmount());
    }

    [TestMethod]
    public void Round0()
    {
      var RawValue = new Inv.Money(4M);
      Assert.AreEqual(RawValue.GetAmount().ToString(), "4");
    }
    [TestMethod]
    public void Round1()
    {
      var RawValue = new Inv.Money(4.00M);
      Assert.AreEqual(RawValue.GetAmount().ToString(), "4");
    }
  }
}