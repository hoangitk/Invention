﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Inv.Support;

namespace Inv
{
  public sealed class Icon
  {
    public Icon(string FilePath)
    {
      this.FilePath = FilePath;
    }

    public string FilePath { get; private set; }

    public Inv.Image LoadImage(int Width, int Height)
    {
      return LoadSource(Width, Height).ConvertToInvImagePng();
    }
    public System.Windows.Media.Imaging.BitmapSource LoadSource(int Width, int Height)
    {
      if (Environment.OSVersion.Platform == PlatformID.Win32NT && Environment.OSVersion.Version.Major < 6)
      {
        using (var FileIcon = System.Drawing.Icon.ExtractAssociatedIcon(FilePath))
        {
          var Result = System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(FileIcon.Handle, System.Windows.Int32Rect.Empty, System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());

          Result.Freeze();

          return Result;
        }
      }
      else
      {
        Win32.Shell32.SHCreateItemFromParsingName(FilePath, IntPtr.Zero, Win32.Shell32.IID_IShellItem, out var ShellItem);
        var ImageFactory = (Win32.IShellItemImageFactory)ShellItem;
        ImageFactory.GetImage(new Win32.Shell32.SIZE() { cx = Width, cy = Height }, Win32.Shell32.SIIGBF.SIIGBF_ICONONLY | Win32.Shell32.SIIGBF.SIIGBF_BIGGERSIZEOK, out var HBitmap);

        var Result = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(HBitmap, IntPtr.Zero, System.Windows.Int32Rect.Empty, System.Windows.Media.Imaging.BitmapSizeOptions.FromEmptyOptions());

        Win32.Gdi32.DeleteObject(HBitmap);

        Result.Freeze();

        return Result;
      }
    }
  }
}
