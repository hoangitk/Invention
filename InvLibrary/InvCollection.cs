﻿//#define INV_COLLECTION_FRAMEWORKLIST
using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Inv.Support;

namespace Inv
{
  /// <summary>
  /// Represents a collection of distinct items.
  /// </summary>
  public interface IDistinctList : System.Collections.IList
  {
    /// <summary>
    /// Capacity of the distinct list.
    /// </summary>
    int Capacity { get; set; }

    /// <summary>
    /// Add a range of items to the distinct list.
    /// </summary>
    /// <param name="Items"></param>
    void AddRange(System.Collections.IEnumerable Items);
  }

  internal sealed class DistinctListDebugView<T>
  {
    public DistinctListDebugView(DistinctList<T> List)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(List, nameof(List));

      this.List = List;
    }

    [DebuggerBrowsable(DebuggerBrowsableState.RootHidden)]
    public T[] Items
    {
      get
      {
        var Result = new T[this.List.Count];

        this.List.CopyTo(Result);

        return Result;
      }
    }

    private readonly DistinctList<T> List;
  }

  /// <summary>
  /// Represents a strongly typed list of distinct objects that can be accessed by index. Provides methods to search, sort and manipulate lists.
  /// </summary>
  /// <remarks>
  /// <para>The <see cref="DistinctList{T}"/> class extends the functionality of <see cref="List{T}"/> by preventing duplicate items from being
  /// inserted into the list.</para>
  /// <para>The <see cref="DistinctList{T}"/> is not guaranteed to be sorted. You must sort the <see cref="DistinctList{T}"/> before performing operations that
  /// require the <see cref="DistinctList{T}"/> to be sorted.</para>
  /// </remarks>
  /// <typeparam name="T">The type of elements in the list.</typeparam>
  [DebuggerTypeProxy(typeof(DistinctListDebugView<>)), DebuggerDisplay("Count = {Count}")]
#if DEBUG
  public sealed class DistinctList<T> : IList<T>, ICollection<T>, IEnumerable<T>, IDistinctList, IProtectedCollection
#else
  public sealed class DistinctList<T> : IList<T>, ICollection<T>, IEnumerable<T>, IDistinctList
#endif
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="DistinctList{T}"/> class that is empty, has the default initial capacity and the default equality comparer.
    /// </summary>
    public DistinctList()
    {
      this.ItemArray = DistinctList<T>.EmptyArray;
    }
    /// <summary>
    /// Initializes a new instance of the <see cref="DistinctList{T}"/> class that is empty, has the default initial capacity and a specified default equality comparer.
    /// </summary>
    public DistinctList(IEqualityComparer<T> EqualityComparer)
    {
      this.ItemArray = DistinctList<T>.EmptyArray;
      this.EqualityComparer = EqualityComparer;
    }
    /// <summary>
    /// Initializes a new instance of the <see cref="DistinctList{T}"/> class that is empty and has the specified initial capacity.
    /// </summary>
    /// <param name="InitialCapacity">The number of elements that the new list can initially store.</param>
    public DistinctList(int InitialCapacity)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(InitialCapacity >= 0, "Initial capacity must be greater than zero.");

      this.ItemArray = new T[InitialCapacity];
    }
    /// <summary>
    /// Initializes a new instance of <see cref="DistinctList{T}"/> with that contains elements copied from the specified enumerable.
    /// </summary>
    /// <param name="Range">The enumerable whose elements are copied to the new list.</param>
    public DistinctList(System.Collections.IEnumerable Range)
      : this(Range.Convert<T>())
    {
    }
    /// <summary>
    /// Initializes a new instance of the <see cref="DistinctList{T}"/> class that contains elements copied from the specified collection
    /// and has sufficient capacity to accommodate the number of elements copied.
    /// </summary>
    /// <param name="Range">The collection whose elements are copied to the new list.</param>
    public DistinctList(IEnumerable<T> Range)
    {
      var Collection = Range as ICollection<T>;

      if (Collection != null)
      {
#if DEBUG
        VerifyDistinctRange(Collection);
#endif

        var CollectionCount = Collection.Count;

        this.ItemArray = new T[CollectionCount];

        Collection.CopyTo(this.ItemArray, 0);

        this.ItemCount = CollectionCount;

#if DEBUG
        VerifyIncludeRange(Collection);
#endif
      }
      else
      {
        this.ItemCount = 0;
        this.ItemArray = new T[DefaultCapacity];

        using (var enumerator = Range.GetEnumerator())
        {
          while (enumerator.MoveNext())
            this.Add(enumerator.Current);
        }
      }
    }

    /// <summary>
    /// Gets or sets the total number of elements the internal data structure can hold without resizing.
    /// </summary>
    /// <value>The number of elements that the <see cref="DistinctList{T}"/> can hold before resizing is required.</value>
    /// <remarks>
    /// <para>Capacity is the number of elements that the <see cref="DistinctList{T}"/> can hold before resizing is required, while <see cref="Count"/> is the number of elements
    /// that are actually in the <see cref="DistinctList{T}"/>.</para>
    /// <para>Capacity is always greater than or equal to <see cref="Count"/>.</para>
    /// </remarks>
    public int Capacity
    {
      get
      {
#if DEBUG
        VerifyReadAccess();
#endif

        return this.ItemArray.Length;
      }
      set
      {
#if DEBUG
        VerifyWriteAccess();
#endif

        if (value != this.ItemArray.Length)
        {
          if (Inv.Assert.IsEnabled)
            Inv.Assert.Check(value >= this.ItemCount, "Attempted to set capacity {0} below active count {1}.", value, this.ItemCount);

          if (value > 0)
          {
            ExpandCapacity(value);
          }
          else
          {
            if (Inv.Assert.IsEnabled)
              Inv.Assert.Check(value == 0, "Capacity must not be set to below zero.");

            this.ItemArray = DistinctList<T>.EmptyArray;
          }
        }
      }
    }
    /// <summary>
    /// Gets the number of elements actually stored in the <see cref="DistinctList{T}"/>.
    /// </summary>
    /// <value>The number of elements actually stored in the <see cref="DistinctList{T}"/>.</value>
    /// <remarks>
    /// <para>Capacity is the number of elements that the <see cref="DistinctList{T}"/> can hold before resizing is required, while <see cref="Count"/> is the number of elements
    /// that are actually in the <see cref="DistinctList{T}"/>.</para>
    /// <para>Capacity is always greater than or equal to <see cref="Count"/>.</para>
    /// </remarks>
    public int Count
    {
      get
      {
#if DEBUG
        VerifyReadAccess();
#endif

        return this.ItemCount;
      }
    }
    /// <summary>
    /// Gets or sets the element at the specified index.
    /// </summary>
    /// <param name="Index">The zero-based index of the element to get or set.</param>
    /// <returns>The element at the specified index.</returns>
    /// <remarks>
    /// <see cref="DistinctList{T}"/> accepts null as a valid value for reference types and does not allow duplicate elements.
    /// </remarks>
    public T this[int Index]
    {
      get
      {
#if DEBUG
        VerifyReadAccess();
#endif

        if (Inv.Assert.IsEnabled)
          VerifyIndex(Index);

        return this.ItemArray[Index];
      }
      set
      {
#if DEBUG
        VerifyWriteAccess();
#endif

        if (Inv.Assert.IsEnabled)
        {
          VerifyIndex(Index);

          if (!this.ItemArray[Index].Equals(value))
            VerifyDistinctItem(value);

          VerifyExcludeItem(this.ItemArray[Index]);
        }

        this.ItemArray[Index] = value;

        if (Inv.Assert.IsEnabled)
          VerifyIncludeItem(value);
      }
    }

    /// <summary>
    /// Removes all elements from the <see cref="DistinctList{T}"/>.
    /// </summary>
    /// <remarks>
    /// <para><see cref="Count"/> is set to 0, and references to other objects from elements of the collection are also released.</para>
    /// <para><see cref="Capacity"/> is unchanged. To reset the capacity of the <see cref="DistinctList{T}"/>, set the <see cref="Capacity"/> property directly.</para>
    /// </remarks>
    public void Clear()
    {
#if DEBUG
      VerifyWriteAccess();
#endif

      if (this.ItemCount > 0)
      {
        if (ItemReferenceCount > 0)
        {
          this.ItemArray = DistinctList<T>.EmptyArray;
          this.ItemReferenceCount = 0;
        }
        else
        {
          Array.Clear(this.ItemArray, 0, this.ItemCount);
        }

        this.ItemCount = 0;

        if (Inv.Assert.IsEnabled)
          VerifyItemSet = null;
      }
    }
    /// <summary>
    /// Determines whether an element is in the <see cref="DistinctList{T}"/>.
    /// </summary>
    /// <param name="Item">The object to locate in the <see cref="DistinctList{T}"/>.</param>
    /// <returns>True if the item is found in the <see cref="DistinctList{T}"/>, false otherwise.</returns>
    public bool Contains(T Item)
    {
      var Comparer = EqualityComparer ?? EqualityComparer<T>.Default;

      for (var ItemIndex = 0; ItemIndex < this.ItemCount; ItemIndex++)
      {
        if (Comparer.Equals(this.ItemArray[ItemIndex], Item))
          return true;
      }

      return false;
    }
    /// <summary>
    /// Inserts an element into the <see cref="DistinctList{T}"/> at the specified location.
    /// </summary>
    /// <param name="Index">The zero-based index at which the item should be inserted.</param>
    /// <param name="Item">The object to insert.</param>
    /// <remarks>
    /// If <see cref="Count"/> already equals <see cref="Capacity"/>, the capacity of the <see cref="DistinctList{T}"/> is increased.
    /// </remarks>
    public void Insert(int Index, T Item)
    {
#if DEBUG
      VerifyWriteAccess();
#endif

      if (Inv.Assert.IsEnabled)
      {
        if (Index < 0 || Index > this.ItemCount)
          throw new ArgumentOutOfRangeException("Index");

        VerifyDistinctItem(Item);
      }

      if (!this.EnsureCapacity(this.ItemCount + 1))
        ProtectItemArray();

      if (Index < this.ItemCount)
        Array.Copy(this.ItemArray, Index, this.ItemArray, Index + 1, this.ItemCount - Index);

      this.ItemArray[Index] = Item;
      this.ItemCount++;

      if (Inv.Assert.IsEnabled)
        VerifyIncludeItem(Item);
    }
    /// <summary>
    /// Insert an item after another item.
    /// </summary>
    /// <param name="After"></param>
    /// <param name="Item"></param>
    public void InsertAfter(T After, T Item)
    {
      var Index = IndexOf(After) + 1;
      if (Index <= 0)
        Index = Count;

      Insert(Index, Item);
    }
    /// <summary>
    /// Insert an item before another item.
    /// </summary>
    /// <param name="Before"></param>
    /// <param name="Item"></param>
    public void InsertBefore(T Before, T Item)
    {
      var Index = IndexOf(Before);
      if (Index < 0)
        Index = 0;

      Insert(Index, Item);
    }
    /// <summary>
    /// Inserts the elements of a collection into the <see cref="DistinctList{T}"/> at the specified index.
    /// </summary>
    /// <param name="Index">The zero-based index at which the new elements should be inserted.</param>
    /// <param name="Range">The collection whose elements should be inserted into the <see cref="DistinctList{T}"/>. The collection itself cannot be null,
    /// but it can contain elements that are null, if type T is a reference type.</param>
    public void InsertRange(int Index, IEnumerable<T> Range)
    {
#if DEBUG
      VerifyWriteAccess();
#endif
      if (Inv.Assert.IsEnabled)
      {
        if (Index < 0 || Index > this.ItemCount)
          throw new ArgumentOutOfRangeException("Index");

        VerifyDistinctRange(Range);
      }

      var Collection = Range as ICollection<T>;

      if (Collection != null)
      {
        var CollectionCount = Collection.Count;

        if (CollectionCount > 0)
        {
          if (!this.EnsureCapacity(this.ItemCount + CollectionCount))
            ProtectItemArray();

          if (Index < this.ItemCount)
            Array.Copy(this.ItemArray, Index, this.ItemArray, Index + CollectionCount, this.ItemCount - Index);

          if (this == Collection)
          {
            Array.Copy(this.ItemArray, 0, this.ItemArray, Index, Index);
            Array.Copy(this.ItemArray, Index + CollectionCount, this.ItemArray, Index * 2, this.ItemCount - Index);
          }
          else
          {
            // NOTE: this appears to be an unnecessary copy to RangeArray.
            //var RangeArray = new T[CollectionCount];
            //Collection.CopyTo(RangeArray, 0);
            //RangeArray.CopyTo(this.ItemArray, Index);

            Collection.CopyTo(this.ItemArray, Index);
          }

          this.ItemCount += CollectionCount;

#if DEBUG
          VerifyIncludeRange(Range);
#endif
        }
      }
      else
      {
        using (var Enumerator = Range.GetEnumerator())
        {
          while (Enumerator.MoveNext())
            this.Insert(Index++, Enumerator.Current);
        }
      }
    }
    /// <summary>
    /// Adds an object to the end of the <see cref="DistinctList{T}"/>.
    /// </summary>
    /// <param name="Item">The object to be added to the end of the <see cref="DistinctList{T}"/>.</param>
    public void Add(T Item)
    {
#if DEBUG
      VerifyWriteAccess();
#endif

      if (Inv.Assert.IsEnabled)
        VerifyDistinctItem(Item);

      var Resized = this.EnsureCapacity(this.ItemCount + 1);

      this.ItemArray[this.ItemCount++] = Item;

      if (!Resized)
        ProtectItemArray();

      if (Inv.Assert.IsEnabled)
        VerifyIncludeItem(Item);
    }

    /// <summary>
    /// Add an array to the list.
    /// </summary>
    /// <param name="Array"></param>
    public void AddArray(params T[] Array)
    {
      AddRange(Array);
    }
    /// <summary>
    /// Adds the elements of the specified collection to the end of the <see cref="DistinctList{T}"/>.
    /// </summary>
    /// <param name="Enumerable">The collection whose elements should be added to the end of the <see cref="DistinctList{T}"/>. The collection itself cannot be null,
    /// but it can contain elements that are null, if type T is a reference type.</param>
    public void AddRange(IEnumerable<T> Enumerable)
    {
      this.InsertRange(this.ItemCount, Enumerable);
    }
    /// <summary>
    /// Remove the first occurrence of a specific object from the <see cref="DistinctList{T}"/>.
    /// </summary>
    /// <param name="Item">The object to remove from the <see cref="DistinctList{T}"/>. The value can be null for reference types.</param>
    /// <returns>True if the item is successfully removed, false otherwise.</returns>
    public bool Remove(T Item)
    {
#if DEBUG
      VerifyWriteAccess();
#endif

      var Index = this.IndexOf(Item);

      if (Index >= 0)
      {
        this.RemoveAt(Index);

        return true;
      }

      return false;
    }
    /// <summary>
    /// Removes all the elements that match the conditions defined by the specified predicate.
    /// </summary>
    /// <param name="Match">The <see cref="Predicate{T}"/> delegate that defines the conditions of the elements to remove.</param>
    /// <returns>The number of elements removed from the <see cref="DistinctList{T}"/>.</returns>
    public int RemoveAll(Predicate<T> Match)
    {
#if DEBUG
      VerifyWriteAccess();
#endif
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Match, nameof(Match));

      int Result;

      ProtectItemArray();

      var FirstIndex = 0;
      while ((FirstIndex < this.ItemCount) && !Match(this.ItemArray[FirstIndex]))
        FirstIndex++;

      if (FirstIndex < this.ItemCount)
      {
        Result = FirstIndex;

        var LastIndex = FirstIndex + 1;

        while (LastIndex < this.ItemCount)
        {
          while (LastIndex < this.ItemCount && Match(this.ItemArray[LastIndex]))
            LastIndex++;

          if (LastIndex < this.ItemCount)
          {
            if (Inv.Assert.IsEnabled)
              VerifyExcludeItem(this.ItemArray[FirstIndex]);

            this.ItemArray[FirstIndex++] = this.ItemArray[LastIndex++];

            Result++;
          }
        }

        var TrailingCount = this.ItemCount - FirstIndex;

        if (Inv.Assert.IsEnabled)
          VerifyExcludeRange(FirstIndex, TrailingCount);

        // Null trailing entries.
        Array.Clear(this.ItemArray, FirstIndex, TrailingCount);

        this.ItemCount = FirstIndex;

        Result += TrailingCount;
      }
      else
      {
        Result = 0;
      }

      return Result;
    }
    /// <summary>
    /// Removes the element at the specified index of the <see cref="DistinctList{T}"/>.
    /// </summary>
    /// <param name="Index">The zero-based index of the element to remove.</param>
    public void RemoveAt(int Index)
    {
#if DEBUG
      VerifyWriteAccess();
#endif

      if (Inv.Assert.IsEnabled)
      {
        VerifyIndex(Index);
        VerifyExcludeItem(this.ItemArray[Index]);
      }

      ProtectItemArray();

      this.ItemCount--;

      if (Index < this.ItemCount)
        Array.Copy(this.ItemArray, Index + 1, this.ItemArray, Index, this.ItemCount - Index);

      this.ItemArray[this.ItemCount] = default;
    }
    /// <summary>
    /// Removes a range of elements from the <see cref="DistinctList{T}"/>.
    /// </summary>
    /// <param name="Index">The zero-based starting index of the range of elements to remove.</param>
    /// <param name="Length">The number of elements to remove.</param>
    public void RemoveRange(int Index, int Length)
    {
#if DEBUG
      VerifyWriteAccess();
#endif
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Index >= 0, "Index must be greater than zero.");
        Inv.Assert.Check(Length >= 0, "Length must be greater than zero.");
        Inv.Assert.Check(Index + Length <= this.ItemCount, "Index plus length must not be more than size.");
      }

      if (Length > 0)
      {
        ProtectItemArray();

        if (Inv.Assert.IsEnabled)
          VerifyExcludeRange(Index, Length);

        this.ItemCount -= Length;

        if (Index < this.ItemCount)
          Array.Copy(this.ItemArray, Index + Length, this.ItemArray, Index, this.ItemCount - Index);

        Array.Clear(this.ItemArray, this.ItemCount, Length);
      }
    }
    /// <summary>
    /// Copies the entire <see cref="DistinctList{T}"/> to a compatible one-dimensional array, starting at the beginning of the target array.
    /// </summary>
    /// <param name="TargetArray">The one-dimensional <see cref="Array"/> that is the destination of the elements copied from <see cref="DistinctList{T}"/>.
    /// The <see cref="Array"/> must have zero-based indexing.</param>
    /// <param name="SourceIndex">The starting index in the target array, defaults to 0.</param>
    public void CopyTo(T[] TargetArray, int SourceIndex = 0)
    {
#if DEBUG
      VerifyReadAccess();
#endif
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.CheckNotNull(TargetArray, nameof(TargetArray));
        Inv.Assert.Check(SourceIndex >= 0 && SourceIndex < this.ItemCount, "Source index must valid.");
      }

      Array.Copy(this.ItemArray, SourceIndex, TargetArray, 0, this.ItemCount - SourceIndex);
    }
    /// <summary>
    /// Searches for an element that matches the conditions defined by the specified predicate, and returns the first occurrence within the entire <see cref="DistinctList{T}"/>.
    /// </summary>
    /// <param name="Match">The <see cref="Predicate{T}"/> delegate that defines the conditions of the element to search for.</param>
    /// <returns>The first element that matches the conditions defined by the specified predicate, if found; otherwise, the default value for type T.</returns>
    public T Find(Predicate<T> Match)
    {
#if DEBUG
      VerifyReadAccess();
#endif
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Match, nameof(Match));

      for (var i = 0; i < this.ItemCount; i++)
      {
        var Item = this.ItemArray[i];

        if (Match(Item))
          return Item;
      }

      return default;
    }
    /// <summary>
    /// Determines whether the <see cref="DistinctList{T}"/> contains elements that match the conditions defined by the specified predicate.
    /// </summary>
    /// <param name="Match">The <see cref="Predicate{T}"/> delegate that defines the conditions of the elements to search for.</param>
    /// <returns>True if the <see cref="DistinctList{T}"/> contains one or more elements that match the conditions defined by the specified predicate; false otherwise.</returns>
    public bool Exists(Predicate<T> Match)
    {
      return FindIndex(Match) != -1;
    }
    /// <summary>
    /// Determines whether the <see cref="DistinctList{T}"/> contains an element at the specified index.
    /// </summary>
    /// <param name="Index">The zero-based index to check for contents.</param>
    /// <returns>True if an element exists at the specified index, otherwise false.</returns>
    public bool ExistsAt(int Index)
    {
#if DEBUG
      VerifyReadAccess();
#endif

      return Index >= 0 && Index < this.ItemCount;
    }
    /// <summary>
    /// Searches for an element that matches the conditions defined by the specified predicate, and returns the zero-based index of the first occurrence
    /// within the entire <see cref="DistinctList{T}"/>.
    /// </summary>
    /// <param name="Match">The <see cref="Predicate{T}"/> delegate that defines the conditions of the elements to search for.</param>
    /// <returns>The zero-based index of the first occurrence of an element that matches the conditions defined by <paramref name="Match"/>, if found; otherwise -1.</returns>
    public int FindIndex(Predicate<T> Match)
    {
      return this.FindIndex(0, this.ItemCount, Match);
    }
    /// <summary>
    /// Searches for an element that matches the conditions defined by the specified predicate, and returns the zero-based index of the first occurrence
    /// within the range of elements in the <see cref="DistinctList{T}"/> that extends from the specified index to the last element.
    /// </summary>
    /// <param name="StartIndex">The zero-based starting index of the search.</param>
    /// <param name="Match">The <see cref="Predicate{T}"/> delegate that defines the conditions of the elements to search for.</param>
    /// <returns>The zero-based index of the first occurrence of an element that matches the conditions defined by <paramref name="Match"/>, if found; otherwise -1.</returns>
    public int FindIndex(int StartIndex, Predicate<T> Match)
    {
      return this.FindIndex(StartIndex, this.ItemCount - StartIndex, Match);
    }
    /// <summary>
    /// Searches for an element that matches the conditions defined by the specified predicate, and returns the zero-based index of the first occurrence
    /// within the range of elements in the <see cref="DistinctList{T}"/> that starts at the specified index and contains the specified number of elements.
    /// </summary>
    /// <param name="Index">The zero-based starting index of the search.</param>
    /// <param name="Length">The number of elements in the section to search.</param>
    /// <param name="Match">The <see cref="Predicate{T}"/> delegate that defines the conditions of the elements to search for.</param>
    /// <returns>The zero-based index of the first occurrence of an element that matches the conditions defined by <paramref name="Match"/>, if found; otherwise -1.</returns>
    public int FindIndex(int Index, int Length, Predicate<T> Match)
    {
#if DEBUG
      VerifyReadAccess();
#endif
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Index >= 0, "Index must be greater than zero.");
        Inv.Assert.Check(Length >= 0, "Length must be greater than zero.");
        Inv.Assert.Check(Index + Length <= this.ItemCount, "Index plus length must not be more than size.");
        Inv.Assert.Check(Match != null, "Match must be specified.");
      }

      var Current = Index + Length;

      for (var i = Index; i < Current; i++)
      {
        if (Match(this.ItemArray[i]))
          return i;
      }

      return -1;
    }
    /// <summary>
    /// Binary search to find an item.
    /// </summary>
    /// <param name="Item"></param>
    /// <param name="Comparer"></param>
    /// <returns></returns>
    public int BinarySearch(T Item, IComparer<T> Comparer)
    {
#if DEBUG
      VerifyReadAccess();
#endif

      return Array.BinarySearch<T>(this.ItemArray, 0, this.ItemCount, Item, Comparer);
    }
    /// <summary>
    /// Searches for the specified object and returns the zero-based index of the first occurrence within the entire <see cref="DistinctList{T}"/>.
    /// </summary>
    /// <param name="Item">The object to locate in the <see cref="DistinctList{T}"/>.</param>
    /// <returns>The zero-based index of the first occurrence of <paramref name="Item"/> within the entire <see cref="DistinctList{T}"/>.</returns>
    public int IndexOf(T Item)
    {
#if DEBUG
      VerifyReadAccess();
#endif
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Item, nameof(Item));

      return Array.IndexOf<T>(this.ItemArray, Item, 0, this.ItemCount);
    }
    /// <summary>
    /// Sorts the elements in the entire <see cref="DistinctList{T}"/> using the default comparer.
    /// </summary>
    public void Sort()
    {
      Sort(0, this.Count, null);
    }
    /// <summary>
    /// Sorts the elements in the entire <see cref="DistinctList{T}"/> using the specified <see cref="IComparer{T}"/>.
    /// </summary>
    /// <param name="Comparer">The <see cref="IComparer{T}"/> implementation to use when comparing elements, or null to use the default comparer <see cref="Comparer{T}.Default"/>.</param>
    public void Sort(IComparer<T> Comparer)
    {
#if DEBUG
      VerifyWriteAccess();
#endif
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Comparer, nameof(Comparer));

      if (this.ItemCount > 1)
      {
        ProtectItemArray();

        Array.Sort(this.ItemArray, 0, this.ItemCount, Comparer);
      }
    }
    /// <summary>
    /// Sorts the elements in the entire <see cref="DistinctList{T}"/> using the specified <see cref="System.Comparison{T}"/>.
    /// </summary>
    /// <param name="Comparison">The <see cref="System.Comparison{T}"/> to use when comparing elements.</param>
    public void Sort(System.Comparison<T> Comparison)
    {
#if DEBUG
      VerifyWriteAccess();
#endif
      if (Inv.Assert.IsEnabled)
        Inv.Assert.CheckNotNull(Comparison, nameof(Comparison));

      if (this.ItemCount > 1)
      {
        ProtectItemArray();

        Array.Sort(this.ItemArray, 0, this.ItemCount, new DelegateComparer<T>(Comparison));
      }
    }
    /// <summary>
    /// Sorts the elements in a range of elements in <see cref="DistinctList{T}"/> using the specified comparer.
    /// </summary>
    /// <param name="Index">The zero-based starting index of the range to sort.</param>
    /// <param name="Length">The length of the range to sort.</param>
    /// <param name="Comparer">The <see cref="IComparer{T}"/> implementation to use when comparing elements, or null to use the default comparer <see cref="Comparer{T}.Default"/>.</param>
    public void Sort(int Index, int Length, IComparer<T> Comparer)
    {
#if DEBUG
      VerifyWriteAccess();
#endif
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(Index >= 0, "Index must be greater or equal to than zero.");
        Inv.Assert.Check(Length >= 0, "Length must be greater or equal to than zero.");
        Inv.Assert.Check(Index + Length <= this.ItemCount, "Index plus length must not be more than size.");
      }

      if (Length > 1)
      {
        ProtectItemArray();

        Array.Sort(this.ItemArray, Index, Length, Comparer);
      }
    }
    /// <summary>
    /// Truncate the list to the number of items specified.
    /// </summary>
    /// <param name="Limit"></param>
    public void Truncate(int Limit)
    {
#if DEBUG
      VerifyWriteAccess();
#endif
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(Limit >= 0, "Truncate limit must be zero or higher.");

      if (Count > Limit)
        RemoveRange(Limit, Count - Limit);
    }
    /// <summary>
    /// Move an item in the list to a new position.
    /// </summary>
    /// <param name="SourceIndex"></param>
    /// <param name="TargetIndex"></param>
    public void Move(int SourceIndex, int TargetIndex)
    {
#if DEBUG
      VerifyWriteAccess();
#endif
      if (Inv.Assert.IsEnabled)
      {
        VerifyIndex(SourceIndex);
        VerifyIndex(TargetIndex);
      }

      if (SourceIndex != TargetIndex)
      {
        var Item = ItemArray[SourceIndex];

        RemoveAt(SourceIndex);

        if (TargetIndex > SourceIndex)
          Insert(TargetIndex - 1, Item);
        else
          Insert(TargetIndex, Item);
      }
    }
    /// <summary>
    /// Randomly shuffle the items in the list.
    /// </summary>
    public void Shuffle()
    {
      // NOTE: there is an extension method in AdvSupport, but we can't use it
      // because the item exchange temporarily violates the distinct assertions.

      var Random = new Random();
      var n = Count;
      while (n > 1)
      {
        n--;
        var k = Random.Next(n + 1);
        var Swap = ItemArray[k];
        ItemArray[k] = ItemArray[n];
        ItemArray[n] = Swap;
      }
    }
    /// <summary>
    /// Replace an existing item in the list with another item.
    /// </summary>
    /// <param name="Old"></param>
    /// <param name="New"></param>
    public void Replace(T Old, T New)
    {
      var Index = IndexOf(Old);

      if (Index < 0)
      {
        if (Inv.Assert.IsEnabled)
          Inv.Assert.Fail("Old item does not exist in the list.");
        return;
      }

      this[Index] = New;
    }
    /// <summary>
    /// Returns an enumerator that iterates through the <see cref="DistinctList{T}"/>.
    /// </summary>
    /// <returns>An <see cref="IEnumerator{T}"/> for the <see cref="DistinctList{T}"/>.</returns>
    public IEnumerator<T> GetEnumerator()
    {
#if DEBUG
      VerifyReadAccess();
#endif

      return new Enumerator(this);
    }

    private void ProtectItemArray()
    {
      // NOTE: if enumerators are active, then we need make a copy of the itemarray so that
      // the enumerators won't fail.
      if (ItemReferenceCount > 0)
      {
        var ReplaceArray = this.ItemArray;

        this.ItemArray = new T[ReplaceArray.Length < DefaultCapacity ? DefaultCapacity : ReplaceArray.Length];

        Array.Copy(ReplaceArray, this.ItemArray, ItemCount);

        this.ItemReferenceCount = 0;
      }
    }
    private bool EnsureCapacity(int ExpectCapacity)
    {
      var Result = this.ItemArray.Length < ExpectCapacity;

      if (Result)
      {
        var NewCapacity = (this.ItemArray.Length == 0) ? DefaultCapacity : (this.ItemArray.Length * 2);

        if (NewCapacity < ExpectCapacity)
          NewCapacity = ExpectCapacity;

        ExpandCapacity(NewCapacity);
      }

      return Result;
    }
    private void ExpandCapacity(int ExpandCapacity)
    {
      var NewArray = new T[ExpandCapacity];

      if (this.ItemCount > 0)
        Array.Copy(this.ItemArray, 0, NewArray, 0, this.ItemCount);

      this.ItemArray = NewArray;
    }

    [DebuggerNonUserCode]
    private void VerifyIndex(int Index)
    {
      if (Index < 0 || Index >= this.ItemCount)
        throw new ArgumentOutOfRangeException("Index", string.Format("Attempted to access index {0} in [0..{1}].", Index, ItemCount - 1));
    }
    [DebuggerNonUserCode]
    private void VerifyIncludeItem(T Item)
    {
      if (VerifyItemSet != null)
        VerifyItemSet.Add(Item);
    }
    [DebuggerNonUserCode]
    private void VerifyIncludeRange(IEnumerable<T> Range)
    {
      if (VerifyItemSet != null)
        VerifyItemSet.AddRange(Range);
    }
    [DebuggerNonUserCode]
    private void VerifyExcludeItem(T Item)
    {
      if (VerifyItemSet != null)
      {
        if (VerifyItemSet.Count <= ShortCircuitCheck)
          VerifyItemSet = null;
        else
          VerifyItemSet.Remove(Item);
      }
    }
    [DebuggerNonUserCode]
    private void VerifyExcludeRange(int Index, int Length)
    {
      if (VerifyItemSet != null)
      {
        if (VerifyItemSet.Count - Length < ShortCircuitCheck)
        {
          VerifyItemSet = null;
        }
        else
        {
          for (var ItemIndex = Index; ItemIndex < Index + Length; ItemIndex++)
            VerifyItemSet.Remove(this.ItemArray[ItemIndex]);
        }
      }
    }
    [DebuggerNonUserCode]
    private void VerifyDistinctItem(T Item)
    {
      if (Item == null)
        throw new ArgumentNullException("Item");

      if (ItemCount < ShortCircuitCheck)
      {
        if (Contains(Item))
          throw new Exception("Item must not already be contained in the distinct list.");
      }
      else
      {
        if (VerifyItemSet == null)
          VerifyItemSet = this.ToHashSetX();

        if (VerifyItemSet.Contains(Item))
          throw new Exception("Item must not already be contained in the distinct list.");
      }
    }
    [DebuggerNonUserCode]
    private void VerifyDistinctRange(IEnumerable<T> Range)
    {
      if (Range == null)
        throw new ArgumentNullException("Range");

      if (!Range.IsDistinct())
        throw new Exception("Range must be distinct.");

      foreach (var Item in Range)
        VerifyDistinctItem(Item);
    }

#if DEBUG
    /// <summary>
    /// Gets or sets a value indicating whether the <see cref="DistinctList{T}"/> is shielded from reads.
    /// </summary>
    [DataMember]
    public bool ReadShielded { get; set; }
    /// <summary>
    /// Gets or sets a value indicating whether the <see cref="DistinctList{T}"/> is shielded from writes.
    /// </summary>
    [DataMember]
    public bool WriteShielded { get; set; }

    bool IProtectedCollection.IsRecordType
    {
      get { return typeof(IProtectedRecord).GetTypeInfo().IsAssignableFrom(typeof(T).GetTypeInfo()); }
    }

    [DebuggerNonUserCode]
    private void VerifyWriteAccess()
    {
      if (WriteShielded)
        throw new Exception("List is shielded from write access.");
    }
    [DebuggerNonUserCode]
    private void VerifyReadAccess()
    {
      if (ReadShielded)
        throw new Exception("List is shielded from read access.");
    }
#endif

    [DataMember]
    private readonly IEqualityComparer<T> EqualityComparer;
    [DataMember]
    private T[] ItemArray;
    [DataMember]
    private int ItemReferenceCount; // number of active enumerators referencing the ItemArray.
    [DataMember]
    private int ItemCount; // number of valid items in the ItemArray.
    [DataMember]
    private HashSet<T> VerifyItemSet; // distinct enforcement.

    static DistinctList()
    {
      DistinctList<T>.EmptyArray = new T[0];
    }

    private const int ShortCircuitCheck = 100;
    private const int DefaultCapacity = 4;
    private static readonly T[] EmptyArray;

    void IDistinctList.AddRange(System.Collections.IEnumerable Items)
    {
      AddRange(Items.OfType<T>());
    }

    int System.Collections.IList.Add(object value)
    {
      var Result = Count;

      Add((T)value);

      return Result;
    }
    void System.Collections.IList.Clear()
    {
      Clear();
    }
    bool System.Collections.IList.Contains(object value)
    {
      return Contains((T)value);
    }
    int System.Collections.IList.IndexOf(object value)
    {
      return IndexOf((T)value);
    }
    void System.Collections.IList.Insert(int index, object value)
    {
      Insert(index, (T)value);
    }
    bool System.Collections.IList.IsFixedSize
    {
      get { return false; }
    }
    bool System.Collections.IList.IsReadOnly
    {
#if DEBUG
      get { return WriteShielded; }
#else
      get { return false; }
#endif
    }
    void System.Collections.IList.Remove(object value)
    {
      Remove((T)value);
    }
    void System.Collections.IList.RemoveAt(int index)
    {
      RemoveAt(index);
    }
    object System.Collections.IList.this[int index]
    {
      get { return this[index]; }
      set { this[index] = (T)value; }
    }

    void System.Collections.ICollection.CopyTo(Array array, int index)
    {
      CopyTo(array.Convert<T>().ToArray(), index);
    }
    int System.Collections.ICollection.Count
    {
      get { return Count; }
    }
    bool System.Collections.ICollection.IsSynchronized
    {
      get { return false; }
    }
    object System.Collections.ICollection.SyncRoot
    {
      get { return this; }
    }

    int ICollection<T>.Count
    {
      get { return Count; }
    }
    void ICollection<T>.Add(T item)
    {
      Add(item);
    }
    void ICollection<T>.Clear()
    {
      Clear();
    }
    bool ICollection<T>.Contains(T item)
    {
      return Contains(item);
    }
    void ICollection<T>.CopyTo(T[] array, int arrayIndex)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(array != null, "Target array must be specified.");
        Inv.Assert.Check(arrayIndex >= 0, "Target array index must zero or more.");
      }

      Array.Copy(this.ItemArray, 0, array, arrayIndex, this.ItemCount);
    }
    bool ICollection<T>.IsReadOnly
    {
      get { return false; }
    }
    bool ICollection<T>.Remove(T item)
    {
      return Remove(item);
    }

    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      return new Enumerator(this);
    }

    /// <summary>
    /// An enumerator representing this <see cref="DistinctList{T}"/>.
    /// </summary>
    [DataContract, StructLayout(LayoutKind.Sequential)]
    public struct Enumerator : IEnumerator<T>
    {
      internal Enumerator(Inv.DistinctList<T> List)
      {
        Interlocked.Increment(ref List.ItemReferenceCount);

        this.List = List;
        this.ItemArray = List.ItemArray;
        this.ItemSize = List.ItemCount;
        this.ItemIndex = 0;
        this.Item = default;
      }
      /// <summary>
      /// Releases the resources held by the enumerator.
      /// </summary>
      public void Dispose()
      {
        if (List.ItemArray == this.ItemArray)
          Interlocked.Decrement(ref List.ItemReferenceCount);
      }

      /// <summary>
      /// The current item this enumerator points to.
      /// </summary>
      public T Current
      {
        get
        {
          return this.Item;
        }
      }

      /// <summary>
      /// Moves the enumerator to the next item in the list.
      /// </summary>
      /// <returns>True if the next item exists; false otherwise.</returns>
      public bool MoveNext()
      {
        if (this.ItemIndex < this.ItemSize)
        {
          this.Item = this.ItemArray[this.ItemIndex++];

          return true;
        }
        else
        {
          this.ItemIndex = this.ItemSize + 1;
          this.Item = default;

          return false;
        }
      }

      object System.Collections.IEnumerator.Current
      {
        get
        {
          return this.Item;
        }
      }
      void System.Collections.IEnumerator.Reset()
      {
        this.ItemIndex = 0;
        this.Item = default;
      }

      [DataMember]
      private readonly Inv.DistinctList<T> List;
      [DataMember]
      private readonly T[] ItemArray;
      [DataMember]
      private readonly int ItemSize;
      [DataMember]
      private int ItemIndex;
      [DataMember]
      private T Item;
    }
  }

  public interface IGrid
  {
    int Width { get; }
    int Height { get; }

    void Resize(int NewWidth, int NewHeight);

    object this[int X, int Y] { get; set; }
  }

  /// <summary>
  /// Represents a two-dimensional grid of elements.
  /// </summary>
  /// <typeparam name="T">The type of elements in the <see cref="Grid{T}"/>.</typeparam>
  public sealed class Grid<T> : IGrid, IEnumerable<T>
  {
    /// <summary>
    /// Create a new 2D grid with a starting width and height of zero.
    /// </summary>
    public Grid()
    {
      this.SquareArray = new T[] { };
      this.Width = 0;
      this.Height = 0;
    }
    /// <summary>
    /// Create a new 2D grid with the specified width and height.
    /// </summary>
    /// <param name="Width"></param>
    /// <param name="Height"></param>
    public Grid(int Width, int Height)
    {
      this.SquareArray = new T[Width * Height];
      this.Width = Width;
      this.Height = Height;
    }

    /// <summary>
    /// The read-only width of the <see cref="Grid{T}"/> in elements.
    /// </summary>
    /// <value>The width of the <see cref="Grid{T}"/> in elements.</value>
    public int Width { get; private set; }
    /// <summary>
    /// The read-only height of the <see cref="Grid{T}"/> in elements.
    /// </summary>
    /// <value>The height of the <see cref="Grid{T}"/> in elements.</value>
    public int Height { get; private set; }
    /// <summary>
    /// Gets or sets the element at the specified zero-based index coordinates in the <see cref="Grid{T}"/>.
    /// </summary>
    /// <param name="X">The zero-based index of the X coordinate.</param>
    /// <param name="Y">The zero-based index of the Y coordinate.</param>
    /// <returns>The element at the specified coordinates.</returns>
    public T this[int X, int Y]
    {
      [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.AggressiveInlining)]
      get
      {
        //Inv.Assert.Check(IsValid(X, Y), string.Format("Get access item out of bounds: [{0}, {1}]", X, Y));

        return this.SquareArray[Y * Width + X];
      }
      [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.AggressiveInlining)]
      set
      {
        //Inv.Assert.Check(IsValid(X, Y), string.Format("Set access item out of bounds: [{0}, {1}]", X, Y));

        this.SquareArray[Y * Width + X] = value;
      } 
    }

    /// <summary>
    /// Determine whether or not the grid coordinates specified are valid.
    /// </summary>
    /// <param name="X">The X coordinate to check.</param>
    /// <param name="Y">The Y coordinate to check.</param>
    /// <returns>True if (X, Y) is within the width/height bounds of the <see cref="Grid{T}"/>; false otherwise.</returns>
    public bool IsValid(int X, int Y)
    {
      return (X >= 0) && (X < Width) && (Y >= 0) && (Y < Height);
    }
    /// <summary>
    /// Ask if the X is within the grid.
    /// </summary>
    /// <param name="X"></param>
    /// <returns></returns>
    public bool IsValidX(int X)
    {
      return (X >= 0) && (X < Width);
    }
    /// <summary>
    /// Ask if the Y is within the grid.
    /// </summary>
    /// <param name="Y"></param>
    /// <returns></returns>
    public bool IsValidY(int Y)
    {
      return (Y >= 0) && (Y < Height);
    }
    /// <summary>
    /// Destructively resizes the <see cref="Grid{T}"/> to the specified new size.
    /// </summary>
    /// <remarks>
    /// Any elements currently in the <see cref="Grid{T}"/> will be removed.
    /// </remarks>
    /// <param name="NewWidth">The new width of the <see cref="Grid{T}"/>.</param>
    /// <param name="NewHeight">The new height of the <see cref="Grid{T}"/>.</param>
    public void Resize(int NewWidth, int NewHeight)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(NewWidth >= 0, "New width must be zero or more.");
        Inv.Assert.Check(NewHeight >= 0, "New height must be zero or more.");
      }

      if (Width != NewWidth || Height != NewHeight)
      {
        var NewArray = new T[NewWidth * NewHeight];
        var OldArray = this.SquareArray;
        var OldWidth = Width;
        var OldHeight = Height;

        var CopyWidth = Math.Min(OldWidth, NewWidth);
        var CopyHeight = Math.Min(OldHeight, NewHeight);

        for (var X = 0; X < CopyWidth; X++)
        {
          for (var Y = 0; Y < CopyHeight; Y++)
            NewArray[Y * NewWidth + X] = OldArray[Y * OldWidth + X];
        }

        this.SquareArray = NewArray;
        this.Width = NewWidth;
        this.Height = NewHeight;
      }
    }
    /// <summary>
    /// Fill the grid with the default value.
    /// </summary>
    public void FillDefault()
    {
      for (var X = 0; X < Width; X++)
      {
        for (var Y = 0; Y < Height; Y++)
          this[X, Y] = default;
      }
    }
    /// <summary>
    /// Populate the <see cref="Grid{T}"/> with default instances of T.
    /// </summary>
    public void Fill(T Value)
    {
      for (var X = 0; X < Width; X++)
      {
        for (var Y = 0; Y < Height; Y++)
          this[X, Y] = Value;
      }
    }
    /// <summary>
    /// Populate the <see cref="Grid{T}"/> with default instances of T.
    /// </summary>
    public void Fill(Func<int, int, T> Function)
    {
      for (var X = 0; X < Width; X++)
      {
        for (var Y = 0; Y < Height; Y++)
          this[X, Y] = Function(X, Y);
      }
    }
    /// <summary>
    /// Rotate the grid 90 degrees. Width becomes Height and vice versa.
    /// </summary>
    public void Rotate90Degrees()
    {
      var OldArray = SquareArray;
      var OldWidth = Width;
      var OldHeight = Height;
      var NewWidth = OldHeight;
      var NewHeight = OldWidth;

      var NewArray = new T[NewWidth * NewHeight];

      var NewRow = 0;
      for (var OldColumn = OldWidth - 1; OldColumn >= 0; OldColumn--)
      {
        var NewColumn = 0;

        for (var OldRow = 0; OldRow < OldHeight; OldRow++)
        {
          NewArray[NewRow * NewWidth + NewColumn] = OldArray[OldRow * OldWidth + OldColumn];
          NewColumn++;
        }

        NewRow++;
      }

      this.SquareArray = NewArray;
      this.Width = NewWidth;
      this.Height = NewHeight;
    }
    /// <summary>
    /// Enumerate the cells in the row.
    /// </summary>
    /// <param name="Row"></param>
    /// <returns></returns>
    public IEnumerable<T> GetRowCells(int Row)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(IsValidY(Row), "Row must be valid: {0}", Row);

      for (var Column = 0; Column < Width; Column++)
        yield return this[Column, Row];
    }
    /// <summary>
    /// Enumerate the cells in the column.
    /// </summary>
    /// <param name="Column"></param>
    /// <returns></returns>
    public IEnumerable<T> GetColumnCells(int Column)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(IsValidX(Column), "Column must be valid: {0}", Column);

      for (var Row = 0; Row < Height; Row++)
        yield return this[Column, Row];
    }
    /// <summary>
    /// Make a shallow copy of the grid.
    /// </summary>
    /// <returns></returns>
    public Inv.Grid<T> Copy()
    {
      var Result = new Inv.Grid<T>(Width, Height);

      for (var Y = 0; Y < Height; Y++)
      {
        for (var X = 0; X < Width; X++)
          Result[X, Y] = this[X, Y];
      }

      return Result;
    }

    IEnumerator<T> IEnumerable<T>.GetEnumerator()
    {
      return new GridEnumerator(SquareArray);
    }
    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      return new GridEnumerator(SquareArray);
    }

    private T[] SquareArray;

    private sealed class GridEnumerator : IEnumerator<T>
    {
      internal GridEnumerator(T[] SquareArray)
      {
        this.SquareArray = SquareArray;
        this.Index = -1;
        this.Length = SquareArray.Length;
      }

      public T Current
      {
        get { return SquareArray[Index]; }
      }
      public void Dispose()
      {
      }

      object System.Collections.IEnumerator.Current
      {
        get { return Current; }
      }
      bool System.Collections.IEnumerator.MoveNext()
      {
        Index++;

        return Index < Length;
      }
      void System.Collections.IEnumerator.Reset()
      {
        Index = 0;
      }

      private readonly T[] SquareArray;
      private int Index;
      private readonly int Length;
    }

    object IGrid.this[int X, int Y]
    {
      get { return this[X, Y]; }
      set { this[X, Y] = (T)value; }
    }
  }

  /// <summary>
  /// Represents a set of characters for string validity checking.
  /// </summary>
  public sealed class CharacterSet : IEnumerable<char>
  {
    /// <summary>
    /// Check to see if this <see cref="CharacterSet"/> contains the specified char.
    /// </summary>
    /// <param name="Value">The character to check.</param>
    /// <returns>True if this <see cref="CharacterSet"/> contains <paramref name="Value"/>, false otherwise.</returns>
    public bool ContainsValue(char Value)
    {
      return HashSet.Contains(Value);
    }
    /// <summary>
    /// Add the specified character to the <see cref="CharacterSet"/>.
    /// </summary>
    /// <param name="Value">The character to add.</param>
    public void Add(char Value)
    {
      HashSet.Add(Value);
    }
    /// <summary>
    /// Add a range of characters to the <see cref="CharacterSet"/>.
    /// </summary>
    /// <param name="FromValue">The first character to add.</param>
    /// <param name="ToValue">The last character to add.</param>
    public void AddRange(char FromValue, char ToValue)
    {
      for (var aIndex = FromValue; aIndex <= ToValue; aIndex++)
        HashSet.Add(aIndex);
    }
    /// <summary>
    /// Add the contents of another <see cref="CharacterSet"/> to this <see cref="CharacterSet"/>.
    /// </summary>
    /// <param name="CharacterSet">The <see cref="CharacterSet"/> to add to this <see cref="CharacterSet"/>.</param>
    public void AddSet(CharacterSet CharacterSet)
    {
      foreach (var Character in CharacterSet)
        HashSet.Add(Character);
    }
    /// <summary>
    /// Remove the characters contained in another <see cref="CharacterSet"/> from this <see cref="CharacterSet"/>.
    /// </summary>
    /// <param name="CharacterSet">The <see cref="CharacterSet"/> to remove from this <see cref="CharacterSet"/>.</param>
    public void RemoveSet(Inv.CharacterSet CharacterSet)
    {
      HashSet.RemoveRange(CharacterSet.HashSet);
    }
    /// <summary>
    /// Check to see if this <see cref="CharacterSet"/> is equivalent to another <see cref="CharacterSet"/>.
    /// </summary>
    /// <param name="CharacterSet">The <see cref="CharacterSet"/>to compare.</param>
    /// <returns>True if the two <see cref="CharacterSet"/>s are equivalent; false otherwise.</returns>
    public bool EqualTo(Inv.CharacterSet CharacterSet)
    {
      return HashSet.Equals(CharacterSet.HashSet);
    }
    /// <summary>
    /// Check to see if a given <see cref="String"/> conforms to this <see cref="CharacterSet"/>.
    /// </summary>
    /// <param name="Value">The string value to check.</param>
    /// <returns>True if every character of <paramref name="Value"/> is contained within this <see cref="CharacterSet"/>, false otherwise.</returns>
    public bool Conforms(string Value)
    {
      foreach (var Character in Value)
      {
        if (!ContainsValue(Character))
          return false;
      }

      return true;
    }

    IEnumerator<char> IEnumerable<char>.GetEnumerator()
    {
      return HashSet.GetEnumerator();
    }

    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      return HashSet.GetEnumerator();
    }

    private readonly HashSet<char> HashSet = new HashSet<char>();
  }

  /// <summary>
  /// A bidirectional dictionary of elements.
  /// </summary>
  /// <remarks>
  /// <para><see cref="Bidictionary{TLeft,TRight}"/> is conceptually a matched pair of <see cref="Dictionary{TKey,TValue}"/>s to facilitate retrieval by either the key (the 'left side')
  /// or the value (the 'right side'); by comparison, <see cref="Dictionary{TKey,Tvalue}"/> has no methods for fetching a key given a value.</para>
  /// </remarks>
  /// <typeparam name="TLeft">The type of the elements on the left side of the dictionary.</typeparam>
  /// <typeparam name="TRight">The type of the elements on the right side of the dictionary.</typeparam>
  public sealed class Bidictionary<TLeft, TRight> : IEnumerable<KeyValuePair<TLeft, TRight>>
  {
    public Bidictionary(IEqualityComparer<TLeft> LeftComparer, IEqualityComparer<TRight> RightComparer)
    {
      this.LeftRightDictionary = new Dictionary<TLeft, TRight>(LeftComparer);
      this.RightLeftDictionary = new Dictionary<TRight, TLeft>(RightComparer);
    }
    public Bidictionary()
    {
      this.LeftRightDictionary = new Dictionary<TLeft, TRight>();
      this.RightLeftDictionary = new Dictionary<TRight, TLeft>();
    }

    /// <summary>
    /// Remove all entries from the <see cref="Bidictionary{TLeft,TRight}"/>.
    /// </summary>
    public void Clear()
    {
      LeftRightDictionary.Clear();
      RightLeftDictionary.Clear();
    }
    /// <summary>
    /// Add a pair of entries to the <see cref="Bidictionary{TLeft,TRight}"/>.
    /// </summary>
    /// <param name="Left">The left side value.</param>
    /// <param name="Right">The right side value.</param>
    public void Add(TLeft Left, TRight Right)
    {
      LeftRightDictionary.Add(Left, Right);
      RightLeftDictionary.Add(Right, Left);
    }
    /// <summary>
    /// Remove a pair of entries from the <see cref="Bidictionary{TLeft,TRight}"/>.
    /// </summary>
    /// <param name="Left">The left side value to remove.</param>
    /// <param name="Right">The right side value to remove.</param>
    /// <returns>True if the remove succeeded.</returns>
    public bool Remove(TLeft Left, TRight Right)
    {
      // This returns TRUE only when both dictionaries have their values removed. It is spread out this way to avoid C# lazy &&.
      var Result = true;
      Result = LeftRightDictionary.Remove(Left) && Result;
      Result = RightLeftDictionary.Remove(Right) && Result;
      return Result;
    }
    /// <summary>
    /// Remove a pair of values from the <see cref="Bidictionary{TLeft,TRight}"/> given the left side value.
    /// </summary>
    /// <param name="Left">The left side value to remove from the <see cref="Bidictionary{TLeft,TRight}"/>.</param>
    /// <returns>True if the remove succeeded.</returns>
    public bool RemoveByLeft(TLeft Left)
    {
      return LeftRightDictionary.TryGetValue(Left, out TRight Right) && Remove(Left, Right);
    }
    /// <summary>
    /// Remove a pair of values from the <see cref="Bidictionary{TLeft,TRight}"/> given the right side value.
    /// </summary>
    /// <param name="Right">The right side value to remove from the <see cref="Bidictionary{TLeft,TRight}"/>.</param>
    /// <returns>True if the remove succeeded.</returns>
    public bool RemoveByRight(TRight Right)
    {
      return RightLeftDictionary.TryGetValue(Right, out TLeft Left) && Remove(Left, Right);
    }
    /// <summary>
    /// Add every entry from another <see cref="Bidictionary{TLeft,TRight}"/> to this <see cref="Bidictionary{TLeft,TRight}"/>.
    /// </summary>
    /// <param name="Index">The <see cref="Bidictionary{TLeft,TRight}"/> to add to this <see cref="Bidictionary{TLeft,TRight}"/>.</param>
    public void AddAll(Bidictionary<TLeft, TRight> Index)
    {
      foreach (var Entry in Index.LeftRightDictionary)
        Add(Entry.Key, Entry.Value);
    }
    public void RemoveAll(Func<KeyValuePair<TLeft, TRight>, bool> Function)
    {
      var RemoveArray = this.Where(Function).ToArray();

      if (RemoveArray.Any(R => !Remove(R.Key, R.Value)))
        throw new Exception("RemoveAll equality failure.");
    }
    /// <summary>
    /// Get the right hand value for the given left hand value.
    /// </summary>
    /// <param name="Left">The left hand value.</param>
    /// <returns>The right hand value corresponding to the specified left hand value.</returns>
    /// <exception cref="Exception">Thrown if no matching right hand value could be found.</exception>
    public TRight GetByLeft(TLeft Left)
    {
      if (LeftRightDictionary.TryGetValue(Left, out TRight result))
        return result;
      else
        throw new Exception(string.Format(CultureInfo.InvariantCulture, "No value found left {0}", Left));
    }
    /// <summary>
    /// Get the left hand value for the given right hand value.
    /// </summary>
    /// <param name="Right">The right hand value.</param>
    /// <returns>The left hand value corresponding to the specified right hand value.</returns>
    /// <exception cref="Exception">Thrown if no matching left hand value could be found.</exception>
    public TLeft GetByRight(TRight Right)
    {
      if (RightLeftDictionary.TryGetValue(Right, out TLeft Result))
        return Result;
      else
        throw new Exception(string.Format(CultureInfo.InvariantCulture, "No value found right {0}", Right));
    }
    public TRight GetByLeftOrDefault(TLeft Left, TRight DefaultRight = default)
    {
      if (LeftRightDictionary.TryGetValue(Left, out TRight result))
        return result;
      else
        return DefaultRight;
    }
    public TLeft GetByRightOrDefault(TRight Right, TLeft DefaultLeft = default)
    {
      if (RightLeftDictionary.TryGetValue(Right, out TLeft Result))
        return Result;
      else
        return DefaultLeft;
    }
    /// <summary>
    /// Gets the right hand value for the given left hand value if possible.
    /// </summary>
    /// <param name="Left">The left hand value.</param>
    /// <param name="Right">When this method returns, contains the right hand value corresponding to the specified left hand value, if that value exists; otherwise
    /// returns the default value for the TRight type.</param>
    /// <returns>True if the <see cref="Bidictionary{TLeft,TRight}"/> contains a right hand value corresponding to the specified left hand value, false otherwise.</returns>
    public bool TryGetByLeft(TLeft Left, out TRight Right)
    {
      return LeftRightDictionary.TryGetValue(Left, out Right);
    }
    /// <summary>
    /// Gets the left hand value for the given right hand value if possible.
    /// </summary>
    /// <param name="Right">The right hand value.</param>
    /// <param name="Left">When this method returns, contains the left hand value corresponding to the specified right hand value, if that value exists; otherwise
    /// returns the default value for the TLeft type.</param>
    /// <returns>True if the <see cref="Bidictionary{TLeft,TRight}"/> contains a left hand value corresponding to the specified right hand value, false otherwise.</returns>
    public bool TryGetByRight(TRight Right, out TLeft Left)
    {
      return RightLeftDictionary.TryGetValue(Right, out Left);
    }
    /// <summary>
    /// Check to see if the <see cref="Bidictionary{TLeft,TRight}"/> contains the specified right hand value.
    /// </summary>
    /// <param name="Right">The right hand value to check</param>
    /// <returns>True if the <see cref="Bidictionary{TLeft,TRight}"/> contains the specified right hand value</returns>
    public bool ContainsByRight(TRight Right)
    {
      return RightLeftDictionary.ContainsKey(Right);
    }
    /// <summary>
    /// Check to see if the <see cref="Bidictionary{TLeft,TRight}"/> contains the specified left hand value.
    /// </summary>
    /// <param name="Left">The left hand value to check</param>
    /// <returns>True if the <see cref="Bidictionary{TLeft,TRight}"/> contains the specified left hand value</returns>
    public bool ContainsByLeft(TLeft Left)
    {
      return LeftRightDictionary.ContainsKey(Left);
    }
    /// <summary>
    /// Gets a <see cref="Dictionary{TLeft,TRight}.KeyCollection"/> of the left hand values of this <see cref="Bidictionary{TLeft,TRight}"/>.
    /// </summary>
    public Dictionary<TLeft, TRight>.KeyCollection Lefts
    {
      get
      {
        return LeftRightDictionary.Keys;
      }
    }
    /// <summary>
    /// Gets a <see cref="Dictionary{TRight,TLeft}.KeyCollection"/> of the right hand values of this <see cref="Bidictionary{TLeft,TRight}"/>.
    /// </summary>
    public Dictionary<TRight, TLeft>.KeyCollection Rights
    {
      get
      {
        return RightLeftDictionary.Keys;
      }
    }

    IEnumerator<KeyValuePair<TLeft, TRight>> IEnumerable<KeyValuePair<TLeft, TRight>>.GetEnumerator()
    {
      return LeftRightDictionary.GetEnumerator();
    }
    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      return LeftRightDictionary.GetEnumerator();
    }

    private readonly Dictionary<TLeft, TRight> LeftRightDictionary;
    private readonly Dictionary<TRight, TLeft> RightLeftDictionary;
  }

  public sealed class OrderedDictionary<TKey, TValue> : IDictionary<TKey, TValue>
  {
    public OrderedDictionary()
    {
      Dictionary = new Dictionary<TKey, LinkedListNode<Entry>>();
    }

    public OrderedDictionary(IEqualityComparer<TKey> EqualityComparer)
    {
      Dictionary = new Dictionary<TKey, LinkedListNode<Entry>>(EqualityComparer);
    }

    public TValue this[TKey Key]
    {
      get
      {
        return Dictionary[Key].Value.Value;
      }
      set
      {
        if (Dictionary.TryGetValue(Key, out LinkedListNode<Entry> Node))
        {
          // move already-existing node to end of list (most recent)
          LinkedList.Remove(Node);
          LinkedList.AddLast(Node);
        }
        else
        {
          Add(Key, value);
        }
      }
    }
    public ICollection<TKey> Keys
    {
      get { return Dictionary.Keys; }
    }
    public ICollection<TValue> Values
    {
      get { return Dictionary.Values.Select(Index => Index.Value.Value).ToHashSetX(); }
    }

    public void Add(TKey Key, TValue Value)
    {
      var Node = LinkedList.AddLast(new Entry(Key, Value));
      Dictionary.Add(Key, Node);
    }
    public void Add(KeyValuePair<TKey, TValue> Item)
    {
      Add(Item.Key, Item.Value);
    }
    public void Clear()
    {
      Dictionary.Clear();
      LinkedList.Clear();
    }
    public int Count
    {
      get { return Dictionary.Count; }
    }
    public bool Contains(KeyValuePair<TKey, TValue> Item)
    {
      return Dictionary.TryGetValue(Item.Key, out LinkedListNode<Entry> Node) && Node.Value.Value.Equals(Item.Value);
    }
    public bool ContainsKey(TKey Key)
    {
      return Dictionary.ContainsKey(Key);
    }
    public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
    {
      throw new NotImplementedException();
    }
    public bool IsReadOnly
    {
      get { throw new NotImplementedException(); }
    }
    public bool Remove(TKey Key)
    {
      if (Dictionary.TryGetValue(Key, out LinkedListNode<Entry> Node))
      {
        LinkedList.Remove(Node);
        return Dictionary.Remove(Key);
      }
      else
        return false;
    }
    public bool Remove(KeyValuePair<TKey, TValue> Item)
    {
      return (Dictionary.TryGetValue(Item.Key, out LinkedListNode<Entry> Node)) && Node.Value.Value.Equals(Item.Value) && Remove(Item.Key);
    }
    public bool TryGetValue(TKey Key, out TValue Value)
    {
      if (Dictionary.TryGetValue(Key, out LinkedListNode<Entry> Node))
      {
        Value = Node.Value.Value;
        return true;
      }
      else
      {
        Value = default;
        return false;
      }
    }

    public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
    {
      foreach (var Entry in LinkedList)
        yield return new KeyValuePair<TKey, TValue>(Entry.Key, Entry.Value);
    }

    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      return GetEnumerator();
    }

    public void RemoveWhile(Predicate<TValue> Predicate)
    {
      var Current = LinkedList.First;
      while (Current != null && Predicate(Current.Value.Value))
      {
        var Next = Current.Next;
        LinkedList.Remove(Current);
        Dictionary.Remove(Current.Value.Key);
        Current = Next;
      }
    }

    private readonly Dictionary<TKey, LinkedListNode<Entry>> Dictionary; // Initialised in the constructor
    private readonly LinkedList<Entry> LinkedList = new LinkedList<Entry>();

    private sealed class Entry
    {
      internal Entry(TKey Key, TValue Value)
      {
        this.Key = Key;
        this.Value = Value;
      }
      public TKey Key;
      public TValue Value;
    }
  }

  public sealed class Pointer<T>
    where T : struct
  {
    public Pointer(T Value)
    {
      this.Value = Value;
    }

    public T Value;
  }

  public sealed class Pool<T>
    where T : class
  {
    public Pool(Func<T> CreateFunction, Action<T> CleanupFunction = null, TimeSpan? Timeout = null, int MinCapacity = 0)
    {
      this.CreationFunction = CreateFunction;
      this.DestructionFunction = CleanupFunction ?? (x => { });
      this.Timeout = Timeout ?? TimeSpan.MaxValue;
      this.Items = new Stack<PoolEntry>();
      this.MinCapacity = MinCapacity;
      EnsureCapacity();
    }

    public int Count
    {
      get { lock (_syncRoot) DrainExpiredEntries(); return Items.Count; }
    }

    public T Take()
    {
      return TryTake() ?? CreationFunction();
    }
    public T TryTake()
    {
      lock (_syncRoot)
      {
        if (Items.Count > 0)
        {
          var Result = Items.Pop().Item;

          DrainExpiredEntries();

          return Result;
        }
      }

      return null;
    }
    public IEnumerable<T> TakeAll()
    {
      lock (_syncRoot)
      {
        DrainExpiredEntries();

        var list = new List<T>();
        while (Items.Count > 0)
          list.Add(Items.Pop().Item);

        return list;
      }
    }
    public void Return(T Item)
    {
      lock (_syncRoot)
      {
        DrainExpiredEntries();

        if (Item != null)
          AddItemInternal(Item);
      }
    }
    public void ReturnMany(IEnumerable<T> Items)
    {
      lock (_syncRoot)
      {
        DrainExpiredEntries();

        foreach (var item in Items)
          AddItemInternal(item);
      }
    }
    public void EnsureCapacity(int RequiredCapacity = 0)
    {
      var capacity = (int)Math.Max(RequiredCapacity, MinCapacity);
      lock (_syncRoot)
      {
        DrainExpiredEntries();

        while (Items.Count < capacity)
          AddItemInternal(CreationFunction());
      }
    }
    public int Drain()
    {
      lock (_syncRoot)
      {
        var size = Items.Count;

        while (Items.Count > 0)
          DestructionFunction(Items.Pop().Item);

        return size;
      }
    }
    public void ForEach(Action<T> action)
    {
      lock (_syncRoot)
      {
        DrainExpiredEntries();
        foreach (var item in Items.Select(x => x.Item))
          action(item);
      }
    }
    public void ForEachParallel(Action<T> action)
    {
      ForEachParallel((item, token) => action(item));
    }
    public void ForEachParallel(Action<T, CancellationToken> action)
    {
      lock (_syncRoot)
      {
        DrainExpiredEntries();
        var token = new CancellationToken();
        Parallel.ForEach(Items, new ParallelOptions() { MaxDegreeOfParallelism = 10, CancellationToken = token }, x => action(x.Item, token));
      }
    }
    private void AddItemInternal(T Item)
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(System.Threading.Monitor.IsEntered(_syncRoot), "Called AddItemInternal while not owning the pool lock");

      var now = DateTimeOffset.Now;
      Items.Push(new PoolEntry() { Item = Item, Added = now });
      MaxUsed = (int)Math.Max(MaxUsed, Items.Count);
#if POOL_DEBUGGING
      Debug.WriteLine(string.Format("Pool<{0}> added entry at {1}, pool size {2}, peak usage {3}", typeof(T).Name, now, Items.Count, MaxUsed));
#endif
    }
    private void DrainExpiredEntries()
    {
      if (Inv.Assert.IsEnabled)
        Inv.Assert.Check(System.Threading.Monitor.IsEntered(_syncRoot), "Called DrainExpiredEntries while not owning the pool lock");

      var now = DateTimeOffset.Now;
      var toDestroy = Items.Where(x => now.Subtract(x.Added) > Timeout).ToArray();
      if (Items.Count > 0 && toDestroy.Length > 0)
      {
        // If we'd drop below minimum capacity, don't delete some or all of toDestroy
        if (Items.Count - toDestroy.Length < MinCapacity)
        {
          var k = MinCapacity - (Items.Count - toDestroy.Length);
          toDestroy = toDestroy.Skip(k).ToArray();
        }

        var toKeep = Items.Except(toDestroy).Reverse().ToArray();

        foreach (var item in toDestroy)
        {
#if POOL_DEBUGGING
          Debug.WriteLine(string.Format("Pool<{0}> expired entry, was unused for {1}", typeof(T).Name, now.Subtract(item.Added)));
#endif
          DestructionFunction(item.Item);
        }

#if POOL_DEBUGGING
        if (toDestroy.Length > 0)
          Debug.WriteLine(string.Format("Pool<{0}> expired {1} entries, kept {2}", typeof(T).Name, toDestroy.Length, toKeep.Length));
#endif

        Items = new Stack<PoolEntry>(toKeep);
      }
    }

    private readonly object _syncRoot = new object();
    private int MaxUsed = 0;
    private readonly int MinCapacity;
    private Stack<PoolEntry> Items;
    private readonly Func<T> CreationFunction;
    private readonly Action<T> DestructionFunction;
    private readonly TimeSpan Timeout;

    private struct PoolEntry
    {
      public T Item;
      public DateTimeOffset Added;
    }
  }

  internal sealed class HierarchyWriter<TNode>
  {
    public HierarchyWriter(Func<TNode, int> GetChildCount, Func<TNode, IEnumerable<TNode>> EnumerateChildren, Func<TNode, string> WriteNode = null)
    {
      if (Inv.Assert.IsEnabled)
      {
        Inv.Assert.Check(GetChildCount != null, "GetChildCount must not be null");
        Inv.Assert.Check(EnumerateChildren != null, "EnumerateChildren must not be null");
      }

      this.GetChildCount = GetChildCount;
      this.EnumerateChildren = EnumerateChildren;
      this.WriteNode = WriteNode;
    }

    public string WriteToString(TNode Node)
    {
      var Builder = new System.Text.StringBuilder();
      WriteToString(Builder, Node, 0, false, string.Empty, string.Empty);
      return Builder.ToString();
    }

    private void WriteToString(System.Text.StringBuilder Builder, TNode Node, int Level, bool LastChild, string CurrentPrefix, string NextPrefix)
    {
      Builder.AppendLine($"{CurrentPrefix + (Level > 0 ? $"{(LastChild ? BoxDrawingsLightUpAndRight : BoxDrawingsLightVerticalAndRight)}{BoxDrawingsLightHorizontal}" : string.Empty)}{(WriteNode == null ? Node.ToString() : WriteNode(Node))}");

      var Index = 0;
      var ChildCount = GetChildCount(Node);
      var Children = EnumerateChildren(Node);

      foreach (var Child in Children)
      {
        var IsLast = ChildCount == ++Index;
        WriteToString(Builder, Child, Level + 1, IsLast, NextPrefix, $"{NextPrefix}{(IsLast ? ' ' : BoxDrawingsLightVertical)} ");
      }
    }

    private readonly Func<TNode, int> GetChildCount;
    private readonly Func<TNode, IEnumerable<TNode>> EnumerateChildren;
    private readonly Func<TNode, string> WriteNode;

    private const char BoxDrawingsLightHorizontal = '\u2500';
    private const char BoxDrawingsLightVertical = '\u2502';
    private const char BoxDrawingsLightUpAndRight = '\u2514';
    private const char BoxDrawingsLightVerticalAndRight = '\u251c';
  }
}