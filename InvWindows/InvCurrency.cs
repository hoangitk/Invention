﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inv
{
  public enum Currency
  {
    CHF,	// Swiss franc.
    CAD,	// Canadian dollar.
    USD,	// US dollar.
    EUR,	// euro.
    GBP,	// British pound.
    JPY,	// Japanese yen.
    AUD,	// Australian dollar.
    INR,	// Indian rupee.
    RUB 	// Russian ruble.
  }
}
